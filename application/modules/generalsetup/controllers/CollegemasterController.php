<?php
class GeneralSetup_CollegemasterController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $_gobjlog;

	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}

	public function indexAction() { // action for search and view
        $pageCount = 50;
		$this->view->title=$this->view->translate("Department Setup");
		
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$lobjCollegemaster = new GeneralSetup_Model_DbTable_Collegemaster(); //user model object



		//echo $this->gobjsessionsis->UserCollegeId;
		if($this->gobjsessionsis->UserCollegeId == '0') {
			$larrresult = $lobjCollegemaster->fngetCollegemasterDetails(); //get user details
		} else {

			// list of faculties based on universityID(loggedIN)
			$idUniversity = $this->gobjsessionsis->idUniversity;
			$larrresult = $lobjCollegemaster->fngetCollegemasterDetailsByAffltId($idUniversity);
			//$larrresult = $lobjCollegemaster->fngetCollegemasterDetailsById($this->gobjsessionsis->UserCollegeId); //get user details
		}

		$lobjUniversity = new GeneralSetup_Model_DbTable_University(); //intialize user Model
		$lobjUniversity = $lobjUniversity->fnGetUniversityList();
		$lobjform->field5->addMultiOptions(array('All'=>'All')+$lobjUniversity);



		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->collegepaginatorresult);

		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->collegepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->collegepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}

        if ($this->_request->isPost() && $this->_request->getPost('Search')) {

			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {

			if($this->gobjsessionsis->UserCollegeId == '0') {

				$larrresult = $lobjCollegemaster->fnSearchCollege($larrformData);
			} else {
				//echo "ni;ak";
				$larrresult = $lobjCollegemaster->fnSearchCollege($larrformData);
				//why it show only college
				//$larrresult = $lobjCollegemaster->fnSearchUserCollege($lobjform->getValues (),$this->gobjsessionsis->UserCollegeId); //get user details
			}
			//print_r(($larrresult));
			$this->view->field5Val = $larrformData['field5'];
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
			$this->gobjsessionsis->collegepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'collegemaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/generalsetup/collegemaster/index');
		}

        $paginator = Zend_Paginator::factory($larrresult);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);

        $totalRecord = $paginator->getTotalItemCount();

        $this->view->paginator = $paginator;
        $this->view->totalRecord = $totalRecord;

	}

	public function newcollegeAction() { //Action for creating the new University
		$idUniversity =$this->gobjsessionsis->idUniversity;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;


		$lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
		$initialConfig = $lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);

	    $this->view->title=$this->view->translate("Add New Department");
		$lobjcollegeForm = new GeneralSetup_Form_Collegemaster (); //intialize user lobjuserForm
		$this->view->lobjcollegeForm = $lobjcollegeForm; //send the lobjuserForm object to the view
		$this->view->lobjcollegeForm->CollegeName->setAttrib('validator', 'validateCollegeName');

		if($initialConfig['CollegeCodeType'] == 1 ){
			$this->view->lobjcollegeForm->CollegeCode->setAttrib('readonly','true');
			$this->view->lobjcollegeForm->CollegeCode->setValue('xxx-xxx-xxx');
		}else{
			$this->view->lobjcollegeForm->CollegeCode->setAttrib('required','true');
			//$this->view->lobjcollegeForm->CollegeCode->setAttrib('validator', 'validateCollegeCode');
		}

		$lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjcollegeForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjcollegeForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$this->view->lobjcollegeForm->ToDate->setValue ( $ldtsystemDate);

		$lobjcountry = $lobjUser->fnGetCountryList();
		$lobjcollegeForm->Country->addMultiOptions($lobjcountry);
		$lobjcollegeForm->Country->setValue ($this->view->DefaultCountry);
		$lobjcollegeForm->Countrystaff->addMultiOptions($lobjcountry);
		
	
		if($this->locale == 'ar_YE')  {
			//$this->view->lobjcollegeForm->FromDate->setAttrib('datePackage',"dojox.date.islamic");
			//$this->view->lobjcollegeForm->ToDate->setAttrib('datePackage',"dojox.date.islamic");
		}

		$lobjUniversity = new GeneralSetup_Model_DbTable_University(); //intialize user Model
		$lobjUniversity = $lobjUniversity->fnGetUniversityList();
		$lobjcollegeForm->AffiliatedTo->addMultiOptions($lobjUniversity);
		
		$lobjStaffmaster = new GeneralSetup_Model_DbTable_Staffmaster();

		//$lobjLevelList = $lobjStaffmaster->fnGetLevelList();
		//$lobjcollegeForm->IdLevel->addMultiOptions($lobjLevelList);

		$lobjStafflist = $lobjStaffmaster->fngetStaffMasterListforDD();
		$lobjcollegeForm->IdStaff->addMultiOptions($lobjStafflist);

		if ($this->_request->isPost()) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			
			if ($lobjcollegeForm->isValid ( $larrformData )) {
				
				//add faculty
				$lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster ();
				$result = $lobjcollegemaster->fnaddCollege ( $larrformData ,$idUniversity,$initialConfig['CollegeCodeType'],$lobjinitialconfig); //instance for adding the lobjuserForm values to DB

				//add dean
				$lobjdeanmaster = new GeneralSetup_Model_DbTable_Deanmaster ();
				$lobjdeanmaster->fnaddDean($result,$larrformData ); //instance for adding the lobjuserForm values to DB

				//add branch address 23/5/2014 
				//add department address
		        $data['add_org_name'] = 'tbl_collegemaster';
				$data['add_org_id'] = $result;
		        $data['add_address_type'] = $larrformData['AddressType'];
				$data['add_country'] = $larrformData['Country'];
				$data['add_state'] = $larrformData['State'];
				$data['add_city'] = $larrformData['City'];
				$data['add_address1'] = $larrformData['Add1'];
				$data['add_address2'] = $larrformData['Add2'];
				$data['add_zipcode'] = $larrformData['Zip'];
				$data['add_phone'] = $larrformData['Phone1'];
				$data['add_email'] = $larrformData['Email'];
				$data['add_affiliate'] = $larrformData['AffiliatedTo'];	
				$data['add_createddt'] = date('Y-m-d H:i:s');
				$data['add_createdby'] = $auth->getIdentity()->iduser;
				$data['add_state_others']=$larrformData['State_Others'];
				$data['add_city_others']=$larrformData['City_Others'];

		        $addressDb = new GeneralSetup_Model_DbTable_Address();
		        $addressDb->addData($data);
		        
				if($larrformData['IsDepartment'] == 1) {
					$larrDeptformData['DepartmentType'] ='0';
					$larrDeptformData['IdCollege'] =$result;
					$larrDeptformData['DepartmentName'] =$larrformData['CollegeName'];
					$larrDeptformData['ArabicName'] =$larrformData['ArabicName'];
					$larrDeptformData['ShortName'] =$larrformData['ShortName'];
					$larrDeptformData['DeptCode'] =$larrformData['ShortName'].'-'.$larrformData['UpdDate'];
					$larrDeptformData['Active'] =$larrformData['Active'];
					$larrDeptformData['UpdDate'] =$larrformData['UpdDate'];
					$larrDeptformData['UpdUser'] =$larrformData['UpdUser'];

					$cdepartmentmodel = new GeneralSetup_Model_DbTable_Departmentmaster();
					$resultstaff = $cdepartmentmodel->fnaddCollegeDepartment($larrDeptformData,$idUniversity,$initialConfig['DepartmentCodeType']); //instance for adding the lobjuserForm values to DB
				}

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New Faculty Add',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/generalsetup/collegemaster/index/');
			}
		}

	}

	public function editcollegeAction(){

    	$this->view->title=$this->view->translate("Edit Department");  //title
    	
		$lobjcollegeForm = new GeneralSetup_Form_Collegemaster (); //intialize user lobjuserForm
		$this->view->lobjcollegeForm = $lobjcollegeForm; //send the lobjuserForm object to the view
		
		$lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster(); //user model object
		
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;

		$lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
		$idUniversity =$this->gobjsessionsis->idUniversity;
		$initialConfig = $lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);

		/*
		$lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
		$lobjcountry = $lobjUser->fnGetCountryList();		
		$lobjcollegeForm->Country->addMultiOptions($lobjcountry);
		$lobjcollegeForm->PermanentidCountry->addMultiOptions($lobjcountry);

		$lobjstate = $lobjUser->fnGetStateList();
		$lobjcollegeForm->State->addMultiOptions($lobjstate);
		*/

		if($this->locale == 'ar_YE')  {
			//$this->view->lobjcollegeForm->FromDate->setAttrib('datePackage',"dojox.date.islamic");
			//$this->view->lobjcollegeForm->ToDate->setAttrib('datePackage',"dojox.date.islamic");
		}

		$lobjUniversity = new GeneralSetup_Model_DbTable_University(); //intialize user Model
		$lobjUniversity = $lobjUniversity->fnGetUniversityList();
		$lobjcollegeForm->AffiliatedTo->addMultiOptions($lobjUniversity);
		
		$lobjstaffmaster = new GeneralSetup_Model_DbTable_Staffmaster(); //intialize user Model
		$lobjstaffmaster = $lobjstaffmaster->fngetStaffMasterListforDD();
		$lobjcollegeForm->IdStaff->addMultiOptions($lobjstaffmaster);
    	$IdCollege = $this->_getParam('id', 0);
    	$this->view->idCollege = $IdCollege;


	    $larrDetails = $lobjcollegemaster->fnGetListofCollege();
	    $lobjcollegeForm->Idhead->addMultiOptions($larrDetails);

	    $lobjStaffmaster = new GeneralSetup_Model_DbTable_Staffmaster();
		$lobjLevelList = $lobjStaffmaster->fnGetLevelList();
		$lobjcollegeForm->IdLevel->addMultiOptions($lobjLevelList);

		//$this->view->lobjcollegeForm->CollegeCode->setAttrib('readonly','true');

		if($initialConfig['CollegeCodeType'] == 1 ){
			$this->view->lobjcollegeForm->CollegeCode->setAttrib('readonly','true');
			$this->view->lobjcollegeForm->CollegeCode->setValue('xxx-xxx-xxx');
		}else{
			$this->view->lobjcollegeForm->CollegeCode->setAttrib('required','true');
		}



		$larrdeanlist = $lobjcollegemaster->fneditDeanDetails($IdCollege);
		foreach($larrdeanlist as $larrdeanlistresult){
		}

		if ($larrdeanlist) {
			$lobjcollegeForm->IdStaff->setValue($larrdeanlistresult['IdStaff']);
			$lobjcollegeForm->FromDate->setValue($larrdeanlistresult['FromDate']);
		}


    	$result = $lobjcollegemaster->fneditCollege($IdCollege);

		foreach($result as $colresult){
		}
		$this->view->Isdepartment = isset($colresult['IsDepartment']) ? $colresult['IsDepartment']:'';
	
		//var_dump($result);
		if ($result) {
			$lobjcollegeForm->populate($colresult);
		}


		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjcollegeForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjcollegeForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

    	$lobjcollegeForm->CollegeName->removeValidator ('Db_NoRecordExists' );
    	
    	if ($this->getRequest()->isPost()) {
    		
    		$formData = $this->getRequest()->getPost();
    		//var_dump($formData); exit;
	    	if ($lobjcollegeForm->isValid($formData)) {
	    		
	   			$lintIdCollege = $formData['IdCollege'];
	   			
				$college= new GeneralSetup_Model_DbTable_Collegemaster();
				$college->fnupdateCollege($formData,$lintIdCollege);

				$lobjldeanistmodel = new GeneralSetup_Model_DbTable_Deanmaster();
				$lobjldeanistmodel->fnupdateDeanList($formData,$lintIdCollege);//update registrar

				if ($colresult['IdStaff'] != $formData['IdStaff'] && $colresult['IdStaff'] != null){
					$dataDeanHistory = array(
						'tdh_IdCollege'=>$lintIdCollege,
						'tdh_IdStaff'=>$colresult['IdStaff'],
						'tdh_fromDate'=>$colresult['FromDate'],
						'tdh_toDate'=>$colresult['ToDate'],
						'tdh_updUser'=>$auth->getIdentity()->iduser,
						'tdh_updDate'=>date('Y-m-d H:i:s')
					);
					$lobjldeanistmodel->storeDeanHistory($dataDeanHistory);
				}else{
					$lobjdeanmaster = new GeneralSetup_Model_DbTable_Deanmaster ();
					$lobjdeanmaster->fnaddDean($lintIdCollege,$formData);
				}
                                
				if($formData ['IsDepartment'] == "1") {
					$cdepartmentmodel = new GeneralSetup_Model_DbTable_Departmentmaster();
					$cdepartmentmodel->fnupdateDepartmentMaster($formData,$lintIdCollege);//update registrar
				}

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Faculty Edit Id=' . $IdCollege,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

				$this->_redirect( $this->baseUrl . '/generalsetup/collegemaster/editcollege/id/'.$IdCollege);
			}
    	}
    }

	public function getcollegelistAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$lobjCommonModel = new App_Model_Common();
    	$lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster(); //user model object
		//Get Country Id
		$lintidCollege = $this->_getParam('coltype');
		if($lintidCollege == '1') {
			$larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($lobjcollegemaster->fnGetListofCollege());
			echo Zend_Json_Encoder::encode($larrDetails);
		}else if($lintidCollege == '0') {
			echo '[{"name":"Select","key":"0"}]';
		}
	}
	public function getcollegenameAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$college= new GeneralSetup_Model_DbTable_Collegemaster();
		$CollegeName = $this->_getParam('CollegeName');
		$larrDetails = $college->fnValidateCollegeName($CollegeName);
		echo $larrDetails['CollegeName'];

	}

	public function getcollegecodeAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$college= new GeneralSetup_Model_DbTable_Collegemaster();
		$CollegeCode = $this->_getParam('CollegeCode');
		$regex="[$^?+*()|\\]";
		$lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
		$initialConfig = $lobjinitialconfig->fnGetInitialConfigDetails($this->gobjsessionsis->idUniversity);
		$sep= $initialConfig['CollegeSeparator'];
		$CollegeCode=preg_replace($regex,$sep,$CollegeCode);
		$larrDetails = $college->fnValidateCollegeCode($CollegeCode);
		echo $larrDetails['CollegeCode'];

	}
	
	public function deleteCollegeAction(){		
		$id = $this->_getParam('id', 0);
		
		$college= new GeneralSetup_Model_DbTable_Collegemaster();
		$college->deleteCollege($id);
		
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		$addressDb->deleteOrgAddress($id,'tbl_collegemaster');
		
		$this->_redirect( $this->baseUrl . '/generalsetup/collegemaster');
	}
	
public function collegeAddressAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_college = $this->_getParam('id', 0);		
		$this->view->idCollege = $id_college;
		
		//get list college address
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		$this->view->address_list = $addressDb->getDataByOrgId($id_college,'tbl_collegemaster');		
		
	}
	
	public function addAddressAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_college = $this->_getParam('id', 0);			
		
		$form= new GeneralSetup_Form_AddressForm(array('idBranch'=>$id_college)); //refer to college use same form as branch
		$this->view->addressForm = $form;
		
		if ($this->_request->isPost ()) {			
		
			$formData = $this->_request->getPost ();
			
			if ($form->isValid ( $formData )) {
				
				//save address				
				$auth = Zend_Auth::getInstance();
				
				$data['add_org_name'] = 'tbl_collegemaster';
				$data['add_org_id'] = $formData['idBranch']; //is refer to id department. Use the same form as branch
				$data['add_address_type'] = $formData['address_type'];
				$data['add_country'] = $formData['country'];
				$data['add_state'] = $formData['state'];
				$data['add_address1'] = $formData['address1'];
				$data['add_address2'] = $formData['address2'];
				$data['add_zipcode'] = $formData['zipcode'];
				$data['add_phone'] = $formData['phone'];
				$data['add_email'] = $formData['email'];
				$data['add_city'] = $formData['city'];
				$data['add_state_others'] = $formData['state_others'];
				$data['add_city_others'] = $formData['city_others'];
				$data['add_createddt'] = date('Y-m-d H:i:s');
				$data['add_createdby'] = $auth->getIdentity()->iduser;				
				
				$addressDb = new GeneralSetup_Model_DbTable_Address();
				$addressDb->addData($data);
				
				$this->_redirect( $this->baseUrl . '/generalsetup/collegemaster/editcollege/id/'.$formData['idBranch'].'#tab2');
			}
		}
	}
	
	public function editAddressAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_college = $this->_getParam('id', 0);		
		$add_id = $this->_getParam('add_id', 0);
						
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		
		$form= new GeneralSetup_Form_AddressForm(array('idBranch'=>$id_college,'addId'=>$add_id));
		
		if ($this->_request->isPost ()) {			
		
			$formData = $this->_request->getPost ();

			if ($form->isValid ( $formData )) {
				
				
				
				$auth = Zend_Auth::getInstance();
				
				//get old (address before change)				
				$address = $addressDb->getDatabyId($formData['add_id']);
				
				//insert into history
				$olddata['add_id'] = $address['add_id'];
				$olddata['addh_org_name'] = $address['add_org_name'];
				$olddata['addh_org_id'] = $address['add_org_id'];
				$olddata['addh_address_type'] = $address['add_address_type'];
				$olddata['addh_country'] = $address['add_country'];
				$olddata['addh_state'] = $address['add_state'];
				$olddata['addh_address1'] = $address['add_address1'];
				$olddata['addh_address2'] = $address['add_address2'];
				$olddata['addh_zipcode'] = $address['add_zipcode'];
				$olddata['addh_phone'] = $address['add_phone'];
				$olddata['addh_email'] = $address['add_email'];
				
				$olddata['addh_city'] = $address['add_city'];
				$olddata['addh_state_others'] = $address['add_state_others'];
				$olddata['addh_city_others'] = $address['add_city_others'];

				$olddata['addh_createddt'] = $address['add_createddt'];
				$olddata['addh_createdby'] = $address['add_createdby'];
				$olddata['addh_changedt'] = date('Y-m-d H:i:s');
				$olddata['addh_changeby'] = $auth->getIdentity()->iduser;
				
				$addHistoryDb = new GeneralSetup_Model_DbTable_AddressHistory();	
				$addHistoryDb->addData($olddata);
				
				//edit
				$data['add_address_type'] = $formData['address_type'];
				$data['add_country'] = $formData['country'];
				$data['add_state'] = $formData['state'];
				$data['add_address1'] = $formData['address1'];
				$data['add_address2'] = $formData['address2'];
				$data['add_zipcode'] = $formData['zipcode'];
				$data['add_phone'] = $formData['phone'];
				$data['add_email'] = $formData['email'];
				
				$data['add_city'] = $formData['city'];
				$data['add_state_others'] = $formData['state_others'];
				$data['add_city_others'] = $formData['city_others'];
				$data['add_modifydt'] = date('Y-m-d H:i:s');
				$data['add_modifyby'] = $auth->getIdentity()->iduser;				
				
				$addressDb->updateData($data,$formData['add_id']);

				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
				
				$this->_redirect( $this->baseUrl . '/generalsetup/collegemaster/editcollege/id/'.$formData['idBranch'].'#tab2');
				
			}
			
		}else{
			
			
			$add = $addressDb->getDatabyId($add_id);
			
			//get state to populate state from country
			$stateDB = new App_Model_General_DbTable_State();
			
			//$state_data = $stateDB->getState($add['add_country']);		
			//$element = $form->getElement('state');
			//foreach($state_data as $s){
				//$element->addMultiOption($s['idState'],$s['StateName']);
			//}
		
			$address_data['address_type']=$add['add_address_type'];
			$address_data['country']=$add['add_country'];
			$address_data['state']=$add['add_state'];
			$address_data['address1']=$add['add_address1'];
			$address_data['address2']=$add['add_address2'];
			$address_data['zipcode']=$add['add_zipcode'];
			$address_data['phone']=$add['add_phone'];
			$address_data['email']=$add['add_email'];
			
			$address_data['city']=$add['add_city'];
			$address_data['state_others']=$add['add_state_others'];
			$address_data['city_others']=$add['add_city_others'];
			
			//print_r($address_data);

			$this->view->address_data = $address_data;
		
			$form->populate($address_data);
			$this->view->addressForm = $form;		
		}
		
	}
	
	public function deleteAddressAction(){
		
		$id_college = $this->_getParam('id', 0);		
		$add_id = $this->_getParam('add_id', 0);
		
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		$addressDb->deleteData($add_id);
		
		$this->_helper->flashMessenger->addMessage(array('success' => "Address deleted"));
				
		$this->_redirect( $this->baseUrl . '/generalsetup/collegemaster/editcollege/id/'.$id_college.'#tab2');

		exit;
	}
	
	public function viewHistoryAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_college = $this->_getParam('id', 0);
		
		//get list branch address
		$addressDb = new GeneralSetup_Model_DbTable_AddressHistory();
		$this->view->address_list = $addressDb->getDataByOrgId($id_college,'tbl_collegemaster');
                
                $deanDb = new GeneralSetup_Model_DbTable_Deanmaster();
                $this->view->dean_list = $deanDb->getDeanHistory($id_college);
	}
}