<?php
class GeneralSetup_BatchController extends Base_Base {
    private $_gobjlog;
    protected $major_tbl = 'tbl_programmajoring';
    private $lobjprogramForm;

    public function init() {
        $this->fnsetObj();
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        Zend_Form::setDefaultTranslator($this->view->translate);

    }

    public function fnsetObj(){
        $this->lobjRegistry = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $this->batchProgramDB = new GeneralSetup_Model_DbTable_BatchProgram();
        $this->batchSubjectDB = new GeneralSetup_Model_DbTable_BatchSubject();
        $this->lobjprogramForm = new GeneralSetup_Form_BatchProgram();
    }

    public function indexAction()
    {
        $this->view->title = "Batch Setup";

        $form = new GeneralSetup_Form_Batch();

        $batchDB= new GeneralSetup_Model_DbTable_Batch();
        $query = $batchDB->getInfo();

        $pageCount = 25;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);
            $query = $batchDB->getBatchData($formData);

            $this->gobjsessionsis->batchQuery = $query;
            $form->populate($formData);
            $this->_redirect($this->baseUrl . '/generalsetup/batch/index/search/1');
        }

        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->batchQuery);
        }else{
            $query = $this->gobjsessionsis->batchQuery;
        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);
        $totalRecord = $paginator->getTotalItemCount();

        $this->view->form = $form;
        $this->view->totalRecord = $totalRecord;
        $this->view->paginator = $paginator;
    }

    public function editAction()
    {
        $id = $this->_getParam('id', null);

        if ($id == null) {
            $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'batch', 'action' => 'index'), 'default', true));
        }

        $this->view->title = "Edit Batch";

        $form = new GeneralSetup_Form_BatchSubmit();

        $batchDb = new GeneralSetup_Model_DbTable_Batch();
        $auth = Zend_Auth::getInstance();

        $BatchprogDb =  new GeneralSetup_Model_DbTable_BatchProgram();
        $this->view->Batchprog = $BatchprogDb->getData($id);

        $BatchSubjectDb =  new GeneralSetup_Model_DbTable_BatchSubject();
        $this->view->BatchSubject = $BatchSubjectDb->getData($id);

        $programDb =  new GeneralSetup_Model_DbTable_Program();
        $this->view->programList = $programDb->list_all();

        $PgStructureList = $this->lobjRegistry->getDataByCodeType('pg-structure');
        $this->view->PgStructureList = $PgStructureList;

        $BatchTypeList = $this->lobjRegistry->getDataByCodeType('batch-type');
        $this->view->BatchTypeList = $BatchTypeList;

        $courseDb =  new GeneralSetup_Model_DbTable_Subjectmaster();
        $this->view->subjectList = $courseDb->fnGetSubjectMasterList();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $data = array(
                    'BatchCode'           => strtoupper( $formData['BatchCode']),
                    'BatchStatus'         => $formData['BatchStatus'],
                    'modifydt'            => date('Y-m-d H:i:s'),
                    'modifyby'            => $auth->getIdentity()->iduser,
                );

                $batchDb->update($data, array('IDBatch=?' => $id));

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully updated"));

                $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'batch', 'action' => 'edit'), 'default', true));
                //$this->_redirect( $this->baseUrl . '/generalsetup/batch/edit/id/'.$id);

            } else {
                $form->populate($formData);
            }
        } else {
            $data = $batchDb->fetchRow(array('IDBatch=?' => $id))->toArray();
            $form->populate($data);
        }

        $this->view->form = $form;
    }

    public function editProgramAction(){

        $this->view->title=$this->view->translate("Batch - Program & Subject");
        $this->view->lobjprogramForm = $this->lobjprogramForm;

        $id = $this->_getParam('id', 0);
        $this->view->idMajor =  $id;
        $this->view->LintIdBatch=$id;

        $MajorModel = new GeneralSetup_Model_DbTable_Batch();
        $MajorInfo = $MajorModel->getBatchInfo($id);
        $this->view->majorInfo = $MajorInfo;

        $BatchprogDb =  new GeneralSetup_Model_DbTable_BatchProgram();
        $this->view->Batchprog = $BatchprogDb->getData($id);

        $BatchSubjectDb =  new GeneralSetup_Model_DbTable_BatchSubject();
        $this->view->BatchSubject = $BatchSubjectDb->getData($id);

        $programDb =  new GeneralSetup_Model_DbTable_Program();
        $this->view->programList = $programDb->list_all();

        $PgStructureList = $this->lobjRegistry->getDataByCodeType('pg-structure');
        $this->view->PgStructureList = $PgStructureList;

        $BatchTypeList = $this->lobjRegistry->getDataByCodeType('batch-type');
        $this->view->BatchTypeList = $BatchTypeList;

        $courseDb =  new GeneralSetup_Model_DbTable_Subjectmaster();
        $this->view->subjectList = $courseDb->fnGetSubjectMasterList();


        //$this->view->majorIntakelist = $this->lobjProgMajorIntake->getMajorIntakeList($idMajor);

        //$this->view->intakelist = $this->lobjIntake->fngetallIntake();


    }

    public function addAction()
    {
        $this->view->title = "Add Batch";

        $form = new GeneralSetup_Form_BatchSubmit();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $batchDb = new GeneralSetup_Model_DbTable_Batch();
                $auth = Zend_Auth::getInstance();

                $data = array(
                    'BatchCode'           => strtoupper( $formData['BatchCode'] ),
                    'BatchStatus'         => $formData['BatchStatus'],
                    'modifydt'            => date('Y-m-d H:i:s'),
                    'modifyby'            => $auth->getIdentity()->iduser,
                );

                $batchDb->insert($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully added"));

                $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'batch', 'action' => 'index'), 'default', true));

            } else {
                $form->populate($formData);
            }
        }

        $this->view->form = $form;
    }

    public function ajaxGetBatchAction(){

        $idProgram = $this->_getParam('idProgram', 0);
        $type = $this->_getParam('type', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        if($type=='program'){

            $IDBatchProgram = $this->_getParam('IDBatchProgram', 0);
            $programMinorDB = new GeneralSetup_Model_DbTable_BatchProgram();
            $row = $programMinorDB->getDetails($IDBatchProgram);
        }

        if($type=='subject'){

            $IDBatchSubject = $this->_getParam('IDBatchSubject', 0);
            $batchSubjectDB = new GeneralSetup_Model_DbTable_BatchSubject();
            $row = $batchSubjectDB->getDetails($IDBatchSubject);
        }

        if ($type=='major'){
            $idProgram = $this->_getParam('idProgram', 0);
            $majorDb =  new GeneralSetup_Model_DbTable_ProgramMajoring();
            $this->view->majorList = $majorDb->getData($idProgram);
        }

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();


        $json = Zend_Json::encode($row);

        echo $json;
        exit();
    }

    public function getMajorAction(){

        $idProgram = $this->_getParam('idProgram', 0);

        //echo $idProgram;exit;

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('s'=>'tbl_programmajoring'),array('IDProgramMajoring','IdMajor'))
            ->where('s.idProgram = ?', $idProgram)
            ->order('s.IdMajor ASC');

        $stmt = $db->query($select);
        $row = $stmt->fetchAll();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($row);

        echo $json;
        exit();
    }

    public function saveEditBatchAction(){

        $auth = Zend_Auth::getInstance();

        if ($this->_request->isPost() ) {

            $formData = $this->_request->getPost ();
            $type = $this->_getParam('type', 0);

            if($type=='program'){

                $data['idProgram']=$formData['EditidProgram'];

                if ($formData['EditidStruc'] == '0'){
                    $data['idStruc']=null;
                } else{
                    $data['idStruc']=$formData['EditidStruc'];
                }

                if ($formData['EditidMajor'] =='0'){
                    $data['idMajor']= null;
                } else {
                    $data['idMajor']=$formData['EditidMajor'];
                }

                $data['modifyby']=$auth->getIdentity()->iduser;
                $data['modifydt']=date('Y-m-d H:i:s');

                $MajorIntakeDB = new GeneralSetup_Model_DbTable_BatchProgram();
                $MajorIntakeDB->updateData($data,$formData["IDBatchProgram"]);

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->baseUrl . '/generalsetup/batch/edit-program/id/'.$formData['IDBatch']);
            }

            if($type=='subject'){

                $data['idSubject']=$formData['EditidSubject'];
                $data['idType']=$formData['EditidType'];

                $data['modifyby']=$auth->getIdentity()->iduser;
                $data['modifydt']=date('Y-m-d H:i:s');

                $MajorIntakeDB = new GeneralSetup_Model_DbTable_BatchSubject();
                $MajorIntakeDB->updateData($data,$formData["IDBatchSubject"]);

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->baseUrl . '/generalsetup/batch/edit-program/id/'.$formData['IDBatchS']);
            }

        }

    }

    public function deleteBatchAction(){

        $IDBatchProgram= $this->_getParam('IDBatchProgram', 0);
        $IDBatchSubject= $this->_getParam('IDBatchSubject', 0);
        $IDBatch= $this->_getParam('IDBatch', 0);
        $type = $this->_getParam('type', 0);

        if($type=='program'){

            $this->batchProgramDB->deleteData($IDBatchProgram);

            $this->_helper->flashMessenger->addMessage(array('success' => "Batch Program deleted"));

            $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'batch', 'action'=>'edit-program','id'=>$IDBatch),'default',true));
        }

        if($type=='subject'){

            $this->batchSubjectDB->deleteData($IDBatchSubject);

            $this->_helper->flashMessenger->addMessage(array('success' => "Batch Program deleted"));

            $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'batch', 'action'=>'edit-program','id'=>$IDBatch),'default',true));
        }


    }

    public function saveBatchAction(){
        $auth = Zend_Auth::getInstance();

        if ($this->_request->isPost() ) {

            $type = $this->_getParam('type', 0);
            $IDBatch = $this->_getParam('IDBatch', null);

            if($type=='program'){

                $data['IDBatch'] = $IDBatch;
                $data['idProgram'] = $this->_getParam('idProgram', null);

                $dataStruc =  $this->_getParam('idStruc', null);
                if ($dataStruc == '0') {
                    $data['idStruc'] = null;
                } else {
                    $data['idStruc'] =  $dataStruc;
                }

                $dataMajor =  $this->_getParam('idMajor', null);
                if ($dataMajor == '0') {
                    $data['idMajor'] = null;
                } else{
                    $data['idMajor'] = $dataMajor;
                }

                $data['modifyby'] = $auth->getIdentity()->iduser;;
                $data['modifydt'] = date ( 'Y-m-d H:i:s');

                $MajorIntakeDB = new GeneralSetup_Model_DbTable_BatchProgram();
                $MajorIntakeDB->addData($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->baseUrl . '/generalsetup/batch/edit-program/id/'.$IDBatch);
            }

            if($type=='subject'){

                $data['IDBatch'] = $IDBatch;
                $data['idSubject'] = $this->_getParam('idSubject', null);
                $data['idType'] = $this->_getParam('idType', null);
                $data['modifyby'] = $auth->getIdentity()->iduser;;
                $data['modifydt'] = date ( 'Y-m-d H:i:s');

                $MajorIntakeDB = new GeneralSetup_Model_DbTable_BatchSubject();
                $MajorIntakeDB->addData($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->baseUrl . '/generalsetup/batch/edit-program/id/'.$IDBatch);
            }

        }
    }

}