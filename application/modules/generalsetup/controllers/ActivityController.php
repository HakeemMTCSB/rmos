<?php

class GeneralSetup_ActivityController extends Base_Base
{
    private $lobjActivity;
    private $_gobjlog;
    private $lobjActivityForm;

    public function init()
    {
        $this->fnsetObj();
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        Zend_Form::setDefaultTranslator($this->view->translate);

    }

    public function fnsetObj()
    {
        $this->lobjform = new App_Form_Search ();
        $this->lobjActivityForm = new GeneralSetup_Form_Activity();
        $this->lobjActivity = new GeneralSetup_Model_DbTable_Activity();
    }

    //Index Action
    public function indexAction()
    {

        $auth = Zend_Auth::getInstance();

        $this->view->lobjform = $this->lobjform;
        $this->view->session = new Zend_Session_Namespace('sis');
        $this->view->iduser = $auth->getIdentity()->iduser;

        $defDb = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $activity = $defDb->getListDataByCodeType('activity-type');

        $this->lobjform->field5->addMultiOption('', $this->view->translate('All'));
        foreach ($activity as $act) {
            $this->lobjform->field5->addMultiOption($act['id'], $act['name']);
        }

        $query = $this->lobjActivity->fngetActivityDetails(); //get Activity Details

        if ($this->_request->isPost() && $this->_request->getPost('Search')) {
            $larrformData = $this->_request->getPost();
            if ($this->lobjform->isValid($larrformData)) {
                $query = $this->lobjActivity->fnSearchActivity($this->lobjform->getValues()); //searching the values for the user
                $this->gobjsessionsis->activitypaginatorresult = $query;
            }
        }

        if (!$this->_getParam('search')) {
            unset($this->gobjsessionsis->activitypaginatorresult);
        } else {
            $query = $this->gobjsessionsis->activitypaginatorresult;
        }

        $pageCount = 50;
        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);

        $this->view->paginator = $paginator;

    }

    //Action to add new activity
    public function newactivityAction()
    {
        $this->view->title = $this->view->translate('Add Activity');

        $auth = Zend_Auth::getInstance();

        $ldtsystemDate = date('Y-m-d H:i:s');
        $this->view->lobjActivityForm = $this->lobjActivityForm;

        $this->view->lobjActivityForm->UpdDate->setValue($ldtsystemDate);
        $this->view->lobjActivityForm->UpdUser->setValue($auth->getIdentity()->iduser);
        $this->view->errorMsg = '';

        if ($this->_request->isPost() && $this->_request->getPost('Save')) {
            $larrformData = $this->_request->getPost(); //getting the values of lobjactivityForm data from post
            unset ($larrformData ['Save']);

            if ($this->lobjActivityForm->isValid($larrformData)) {
                if ($this->lobjActivity->checkColor($larrformData['ActivityColorCode']) == false) {
                    $this->view->errorMsg = $this->view->translate('Calendar color already in used');
                    $this->lobjActivityForm->populate($larrformData);
                } else {
                    $result = $this->lobjActivity->fnaddActivity($larrformData); //instance for adding the lobjactivityForm values to DB

                    // Write Logs
                    $priority = Zend_Log::INFO;
                    $larrlog = array('user_id'     => $auth->getIdentity()->iduser,
                                     'level'       => $priority,
                                     'hostname'    => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                                     'time'        => date('Y-m-d H:i:s'),
                                     'message'     => 'New Activity Add',
                                     'Description' => Zend_Log::DEBUG,
                                     'ip'          => $this->getRequest()->getServer('REMOTE_ADDR'));
                    $this->_gobjlog->write($larrlog); //insert to tbl_log

                    $this->gobjsessionsis->flash = array('message' => 'New activity successfully added', 'type' => 'success');
                    $this->_redirect($this->baseUrl . '/generalsetup/activity/index');
                }
            }
        }
    }

    public function editactivityAction()
    {

        $this->view->title = $this->view->translate('Edit Activity');

        $this->view->lobjActivityForm = $this->lobjActivityForm; //send the lobjActivityForm object to the view
        $IdActivity = $this->_getParam('id', 0);
        $result = $this->lobjActivity->fetchAll('IdActivity =' . $IdActivity);
        $result = $result->toArray();
        foreach ($result as $activityresult) {
            $this->lobjActivityForm->populate($activityresult);
        }
        //$this->lobjActivityForm->populate($activityresult);
        $ldtsystemDate = date('Y-m-d H:i:s');
        $this->view->lobjActivityForm->IdActivity->setValue($result[0]['idActivity']);
        $this->view->lobjActivityForm->UpdDate->setValue($ldtsystemDate);
        $auth = Zend_Auth::getInstance();
        $this->view->lobjActivityForm->UpdUser->setValue($auth->getIdentity()->iduser);
        $this->view->errorMsg = '';

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($this->lobjActivityForm->isValid($formData)) {
                $lintIdActivity = $formData ['IdActivity'];
                $larrformData = $this->getRequest()->getPost();

                if ($this->lobjActivity->checkColor($formData['ActivityColorCode'], $lintIdActivity) == false) {
                    $this->view->errorMsg = $this->view->translate('Calendar color already in used');
                    $this->lobjActivityForm->populate($larrformData);
                } else {
                    $this->lobjActivity->fnupdateActivity($formData, $lintIdActivity);//update Activity

                    // Write Logs
                    $priority = Zend_Log::INFO;
                    $larrlog = array('user_id'     => $auth->getIdentity()->iduser,
                                     'level'       => $priority,
                                     'hostname'    => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                                     'time'        => date('Y-m-d H:i:s'),
                                     'message'     => 'Activity Edit Id=' . $IdActivity,
                                     'Description' => Zend_Log::DEBUG,
                                     'ip'          => $this->getRequest()->getServer('REMOTE_ADDR'));
                    $this->_gobjlog->write($larrlog); //insert to tbl_log

                    $this->gobjsessionsis->flash = array('message' => 'Activity successfully updated', 'type' => 'success');
                    $this->_redirect($this->baseUrl . '/generalsetup/activity/index');
                }
            }
        }
    }

    public function deleteactivityAction()
    {
        $this->view->lobjActivityForm = $this->lobjActivityForm;

        $idActivity = $this->_getParam('id', 0);
        $this->lobjActivity->fnDeleteActivity($idActivity);

        $this->gobjsessionsis->flash = array('message' => 'Activity has been deleted', 'type' => 'success');
        $this->_redirect($this->baseUrl . '/generalsetup/activity/index');
    }

    public function viewAction()
    {

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $this->view->title = "View Activities";

        $semesterId = $this->_getParam('semester_id', 0);

        $query = $this->lobjActivity->fngetActivityDetails($semesterId); //get Activity Details

        $this->view->paginator = $query;

    }

}


	