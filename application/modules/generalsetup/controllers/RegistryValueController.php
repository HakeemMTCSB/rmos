<?php

class Generalsetup_RegistryValueController extends Base_Base
{

    public function viewAction()
    {

        $id = $this->_getParam('id', null);

        $form = new GeneralSetup_Form_Registry_Search();

        $registryTypeDb = new GeneralSetup_Model_DbTable_Registry_RegistryType();
        $registryValueDb = new GeneralSetup_Model_DbTable_Registry_RegistryValue();

        $result = $registryValueDb->getList($id);
        $dataType = $registryTypeDb->getData($id);

        $this->view->title = "Registry Values - " . $dataType['name'];

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);
            $result = $registryValueDb->getList($id, $formData);

        }

        $this->view->id = $id;
        $this->view->form = $form;
        $this->view->formData = $formData ?? null;
        $this->view->data = $result;

    }

    public function addAction()
    {

        $this->view->title = "Add Registry Value";

        $id = $this->_getParam('id', null);

        $form = new GeneralSetup_Form_Registry_RegistryValue();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $registryValueDb = new GeneralSetup_Model_DbTable_Registry_RegistryValue();

                $data = array(
                    'type_id'     => $id,
                    'code'        => $formData['code'],
                    'name'        => $formData['name'],
                    'name_malay'  => $formData['name_malay'],
                    'description' => $formData['description'],
                    'active'      => $formData['active'],
                    'default'     => $formData['default'],
                );

                $registryValueDb->insert($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully added"));

                $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'registry-value', 'action' => 'view', 'id' => $id), 'default', true));

            } else {
                $form->populate($formData);
            }
        }

        $this->view->id = $id;
        $this->view->form = $form;
    }

    public function editAction()
    {

        $id = $this->_getParam('id', null);

        if ($id == null) {
            $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'registry-type', 'action' => 'index'), 'default', true));
        }

        $this->view->title = "Edit Registry Type";

        $form = new GeneralSetup_Form_Registry_RegistryValue();

        $registryValueDb = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $getData = $registryValueDb->getData($id);

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $data = array(
                    'code'        => $formData['code'],
                    'name'        => $formData['name'],
                    'name_malay'  => $formData['name_malay'],
                    'description' => $formData['description'],
                    'active'      => $formData['active'],
                    'default'     => $formData['default'],
                );

                $registryValueDb->update($data, array('id=?' => $id));

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully updated"));

                $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'registry-value', 'action' => 'view', 'id' => $getData['type_id']), 'default', true));

            } else {
                $form->populate($formData);
            }
        } else {
            $data = $registryValueDb->fetchRow(array('id=?' => $id))->toArray();
            $form->populate($data);
        }

        $this->view->typeId = $getData['type_id'];
        $this->view->id = $id;
        $this->view->form = $form;
    }
}