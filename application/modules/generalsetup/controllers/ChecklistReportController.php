<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 4/8/2016
 * Time: 10:31 AM
 */
class GeneralSetup_ChecklistReportController extends Base_Base {
    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new GeneralSetup_Model_DbTable_ChecklistReport();

        set_time_limit(0);
        ini_set('memory_limit', '-1');
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Checklist Report');
        $sofferedDB = new GeneralSetup_Model_DbTable_Subjectsoffered();
        $courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
        $courseProgramDb = new GeneralSetup_Model_DbTable_CourseGroupProgram();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $form = new GeneralSetup_Form_ChecklistReport();
            $form->populate($formData);
            $this->view->form = $form;
            $this->view->formData = $formData;

            $semester = $this->model->getSemesterById($formData['semester']);

            $calendar = $this->model->getCalendar($formData['semester']);
            $this->view->calendar = $calendar;

            $subOffer = $sofferedDB->getSubjectOffered($formData['semester']);

            if ($subOffer){
                foreach ($subOffer as $key => $subOfferLoop){
                    $subOffer[$key]['groupssection'] = $courseGroupDb->getGroupList($subOfferLoop['IdSubject'], $formData['semester']);

                    if (count($subOffer[$key]['groupssection'])){
                        foreach ($subOffer[$key]['groupssection'] as $key2=>$group){
                            $subOffer[$key]['groupssection'][$key2]['program'] = $courseProgramDb->getGroupData($group['IdCourseTaggingGroup']);
                        }
                    }

                    $subOffer[$key]['examschedule'] = $this->model->getExamSchedule($formData['semester'], $subOfferLoop['IdSubject']);
                }
            }

            $this->view->subOffer = $subOffer;

            $program = $this->model->getProgram($semester['IdScheme']);

            if ($program){
                foreach ($program as $programKey => $programLoop){
                    $programScheme = $this->model->getProgramScheme($programLoop['IdProgram']);

                    if ($programScheme){
                        $program[$programKey]['program_scheme'] = $programScheme;

                        foreach ($programScheme as $programSchemeKey => $programSchemeLoop){
                            $regItem = $this->model->getRegistrationItem($formData['semester'], $programLoop['IdProgram'], $programSchemeLoop['IdProgramScheme']);

                            if ($regItem){
                                $program[$programKey]['program_scheme'][$programSchemeKey]['item'] = $regItem;

                                if ($regItem){
                                    foreach ($regItem as $regItemKey => $regItemLoop){
                                        $activity = $this->model->getActivity($regItemLoop['ri_id']);

                                        if ($activity){
                                            $program[$programKey]['program_scheme'][$programSchemeKey]['item'][$regItemKey]['activity'] = $activity;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $this->view->program = $program;
        }else{
            $form = new GeneralSetup_Form_ChecklistReport();
            $this->view->form = $form;
        }
    }

    public function printAction(){
        $sofferedDB = new GeneralSetup_Model_DbTable_Subjectsoffered();
        $courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
        $courseProgramDb = new GeneralSetup_Model_DbTable_CourseGroupProgram();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $form = new GeneralSetup_Form_ChecklistReport();
            $form->populate($formData);
            $this->view->form = $form;

            $semester = $this->model->getSemesterById($formData['semester']);

            $calendar = $this->model->getCalendar($formData['semester']);
            $this->view->calendar = $calendar;

            $subOffer = $sofferedDB->getSubjectOffered($formData['semester']);

            if ($subOffer){
                foreach ($subOffer as $key => $subOfferLoop){
                    $subOffer[$key]['groupssection'] = $courseGroupDb->getGroupList($subOfferLoop['IdSubject'], $formData['semester']);

                    if (count($subOffer[$key]['groupssection'])){
                        foreach ($subOffer[$key]['groupssection'] as $key2=>$group){
                            $subOffer[$key]['groupssection'][$key2]['program'] = $courseProgramDb->getGroupData($group['IdCourseTaggingGroup']);
                        }
                    }

                    $subOffer[$key]['examschedule'] = $this->model->getExamSchedule($formData['semester'], $subOfferLoop['IdSubject']);
                }
            }

            $this->view->subOffer = $subOffer;

            $program = $this->model->getProgram($semester['IdScheme']);

            if ($program){
                foreach ($program as $programKey => $programLoop){
                    $programScheme = $this->model->getProgramScheme($programLoop['IdProgram']);

                    if ($programScheme){
                        $program[$programKey]['program_scheme'] = $programScheme;

                        foreach ($programScheme as $programSchemeKey => $programSchemeLoop){
                            $regItem = $this->model->getRegistrationItem($formData['semester'], $programLoop['IdProgram'], $programSchemeLoop['IdProgramScheme']);

                            if ($regItem){
                                $program[$programKey]['program_scheme'][$programSchemeKey]['item'] = $regItem;

                                if ($regItem){
                                    foreach ($regItem as $regItemKey => $regItemLoop){
                                        $activity = $this->model->getActivity($regItemLoop['ri_id']);

                                        if ($activity){
                                            $program[$programKey]['program_scheme'][$programSchemeKey]['item'][$regItemKey]['activity'] = $activity;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $this->view->program = $program;
        }

        $content = '';

        if ($calendar) {
            $content .= '<h3>Calendar</h3>';
            $content .= '<hr>';
            $content .= '<table class="table" width="100%">';
            $content .= '<tbody>';
            $content .= '<tr>';
            $content .= '<th>Activity Type</th>';
            $content .= '<th>Date From</th>';
            $content .= '<th>Date To</th>';
            $content .= '</tr>';
            $content .= '</tbody>';
            $content .= '<tbody>';

            if ($calendar) {
                foreach ($calendar as $calendar) {

                    $content .= '<tr>';
                    $content .= '<td>'.$calendar['ActivityName'].'</td>';
                    $content .= '<td>'.($calendar['StartDate'] != null && $calendar['StartDate'] != '' ? date('d-m-Y', strtotime($calendar['StartDate'])):'').'</td>';
                    $content .= '<td>'.($calendar['EndDate'] != null && $calendar['EndDate'] != '' ? date('d-m-Y', strtotime($calendar['EndDate'])):'').'</td>';
                    $content .= '</tr>';
                }
            }

            $content .= '</tbody>';
            $content .= '</table>';
            $content .= '<br/>';
            $content .= '<div style="page-break-after: always;"></div>';
        }

        if ($subOffer) {

            $content .= '<h3>Course Offered</h3>';
            $content .= '<hr>';
            $content .= '<table class="table" width="100%">';
            $content .= '<thead>';
            $content .= '<tr>';
            $content .= '<th>No.</th>';
            $content .= '<th>Course</th>';
            $content .= '<th>Course Code</th>';
            $content .= '<th>Course Section</th>';
            $content .= '<th>Exam Schedule</th>';
            $content .= '</tr>';
            $content .= '</thead>';
            $content .= '<tbody>';

            if ($subOffer) {
                $no = 0;
                foreach ($subOffer as $subOffer) {
                    $no++;

                    $content .= '<tr>';
                    $content .= '<td valign="top">'.$no.'</td>';
                    $content .= '<td valign="top">'.$subOffer['SubjectName'].'</td>';
                    $content .= '<td valign="top">'.$subOffer['SubCode'].'</td>';
                    $content .= '<td valign="top">';
                    $content .= '<table class="table" width="100%">';
                    $content .= '<tr>';
                    $content .= '<th>Section Name</th>';
                    $content .= '<th>Section Code</th>';
                    $content .= '<th>Semester</th>';
                    $content .= '<th>Lecturer</th>';
                    $content .= '<th>Programme</th>';
                    $content .= '<th>Max Occupancy</th>';
                    $content .= '</tr>';
                                
                    if (isset($subOffer['groupssection']) && count($subOffer['groupssection']) > 0) {
                        foreach ($subOffer['groupssection'] as $group) {

                            $content .= '<tr>';
                            $content .= '<td>'.$group["GroupName"].'</td>';
                            $content .= '<td align="center">'.$group["GroupCode"].'</td>';
                            $content .= '<td align="center">'.$group["semester_name"].'</td>';
                            $content .= '<td align="center">'.($group["IdLecturer"] != 0 ? $group["FullName"] : $this->translate("-")).'</td>';
                            $content .= '<td>';
                            //$content .= '<ul class="prog-list">';

                            if (sizeof($group['program']) > 0) {
                                foreach ($group['program'] as $pro) {
                                    $content .= '-' . $pro['ProgramCode'] . ' - ' . $pro['mop'] . ' ' . $pro['mos'] . ' ' . $pro['pt'] . '<br />';
                                }
                            }

                            //$content .= '</ul>';
                            $content .= '</td>';
                            $content .= '<td align="center">'.$group["maxstud"].'</td>';
                            $content .= '</tr>';

                        }
                    }

                    $content .= '</table>';
                    $content .= '</td>';
                    $content .= '<td valign="top">';
                    $content .= '<table class="table" width="100%">';
                    $content .= '<tr>';
                    $content .= '<th>Exam Date</th>';
                    $content .= '<th>Exam Start Time</th>';
                    $content .= '<th>Exam End Time</th>';
                    $content .= '<th>Exam Center</th>';
                    $content .= '</tr>';
                                
                    if (isset($subOffer['examschedule']) && count($subOffer['examschedule']) > 0) {
                        foreach ($subOffer['examschedule'] as $examschedule) {

                            $content .= '<tr>';
                            $content .= '<td align="center">'.($examschedule['es_date'] != null ? date('d-m-Y', strtotime($examschedule['es_date'])):'').'</td>';
                            $content .= '<td align="center">'.$examschedule['es_start_time'].'</td>';
                            $content .= '<td align="center">'.$examschedule['es_end_time'].'</td>';
                            $content .= '<td>'.$examschedule['ec_name'].'</td>';
                            $content .= '</tr>';
                        }
                    }

                    $content .= '</table>';
                    $content .= '</td>';
                    $content .= '</tr>';
                    
                }
            }

            $content .= '</tbody>';
            $content .= '</table>';
            $content .= '<br />';
            $content .= '<div style="page-break-after: always;"></div>';
        }

        if ($program) {

            $content .= '<h3>Registration Item</h3>';
            $content .= '<hr>';
            
            foreach ($program as $program){
                if (isset($program['program_scheme']) && count($program['program_scheme']) > 0){
                    foreach ($program['program_scheme'] as $programScheme){

                        $content .= '<fieldset>';
                        $content .= '<legend>'.$program['ProgramName'].' ('.$programScheme['mop'].' '.$programScheme['mos'].' '.$programScheme['pt'].')</legend>';
                        $content .= '<table class="table" width="100%">';
                        $content .= '<thead>';
                        $content .= '<tr>';
                        $content .= '<th>Item</th>';
                        $content .= '<th>Chargeable</th>';
                        $content .= '<th>Mandatory</th>';
                        $content .= '<th>Refund Policy</th>';
                        $content .= '</tr>';
                        $content .= '</thead>';
                        $content .= '<tbody>';
                                
                        if (isset($programScheme['item']) && count($programScheme['item']) > 0) {
                            foreach ($programScheme['item'] as $item) {

                                $content .= '<tr>';
                                $content .= '<td valign="top">'.$item['item_name'].'</td>';
                                $content .= '<td align="center" valign="top">'.($item['chargable']==1 ? 'Yes':'No').'</td>';
                                $content .= '<td align="center" valign="top">'.($item['mandatory']==1 ? 'Yes':'No').'</td>';
                                $content .= '<td valign="top">';

                                if (isset($item['activity']) && count($item['activity']) > 0){
                                    foreach ($item['activity'] as $activity) {

                                        $content .= '<table width="100%" class="btable">';
                                        $content .= '<tr>';
                                        $content .= '<td>Activity</td>';
                                        $content .= '<td width="1%">:</td>';
                                        $content .= '<td>'.$activity['ActivityName'].'</td>';
                                        $content .= '</tr>';
                                        $content .= '<tr>';
                                        $content .= '<td>Start/End Date</td>';
                                        $content .= '<td width="1%">:</td>';
                                        $content .= '<td>'.date('d-m-Y', strtotime($activity['StartDate'])).' - '.date('d-m-Y', strtotime($activity['EndDate'])).'</td>';
                                        $content .= '</tr>';
                                        $content .= '<tr>';
                                        $content .= '<td>Calculation Type</td>';
                                        $content .= '<td width="1%">:</td>';
                                        $content .= '<td>'.($activity['ria_calculation_type']==0 ? 'Percentage':'Amount').'</td>';
                                        $content .= '</tr>';
                                        $content .= '<tr>';
                                        $content .= '<td>Amount</td>';
                                        $content .= '<td width="1%">:</td>';
                                        $content .= '<td>'.$activity['ria_amount'].'</td>';
                                        $content .= '</tr>';
                                        $content .= '</table>';
                                        $content .= '<br />';

                                    }
                                }

                                $content .= '</td>';
                                $content .= '</tr>';

                            }
                        }

                        $content .= '</tbody>';
                        $content .= '</table>';
                        $content .= '</fieldset>';
                        $content .= '<br/>';
                        $content .= '<div style="page-break-after: always;"></div>';
                    }
                }
            }
        }

        //$content = '';

        $fieldValues = array(
            '$[LOGO]'=> "images/logo_text_high.jpg",
            '$[CONTENT]'=> $content
        );

        $html_template_path = DOCUMENT_PATH."/template/checklist_report.html";
        $output_filename = "checklist_report.pdf";

        require_once 'dompdf_config.inc.php';
        error_reporting(0);

        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        $html = file_get_contents($html_template_path);

        //replace variable
        foreach ($fieldValues as $key=>$value){
            $html = str_replace($key,$value,$html);
        }

        //echo $html; exit;

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'potrait');
        $dompdf->render();

        $dompdf->stream($output_filename);

        exit;
    }
}