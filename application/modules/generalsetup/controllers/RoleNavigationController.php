<?php

class GeneralSetup_RoleNavigationController extends Base_Base {
	
	public function indexAction(){
		
		$this->view->title = $this->view->translate("Role Navigation - Role List");
		
		$definitionDb = new App_Model_Definitiontype();
		
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		$roleList = $definitionDb->fnGetDefinations('Role');
		$this->view->paginator = $this->lobjCommon->fnPagination($roleList,$lintpage,$lintpagecount);	
		
	}
	
	public function roleAccessAction(){
		
		$this->view->title = $this->view->translate("Role Navigation - Assign Role Accessibility");
		
		$role_id = $this->_getParam('role_id',null);
		$this->view->role_id = $role_id;
		
		$topMenuDb = new GeneralSetup_Model_DbTable_TopMenu();
		$sidebarDb = new GeneralSetup_Model_DbTable_SidebarMenu();
		$resnavDB = new GeneralSetup_Model_DbTable_RoleResourcesNav();
		
		$top_menu_list = $topMenuDb->getMenuList();
		
		$menu_arr = array();
		$group_arr = array('1'=>'View','2'=>'Add','3'=>'Edit','4'=>'Delete','5'=>'Approve');
		
		if(count($top_menu_list)>0){
			
			//rename top menu
			foreach($top_menu_list as $tm){
				$tmenu['id']='m'.$tm['tm_id'];
				$tmenu['parent']='#';
				$tmenu['text']=$tm['tm_name'];
				array_push($menu_arr,$tmenu);
			}			
					
			//get list sidebar
			foreach($top_menu_list as $menu){				
				//get Sidebar
				$sidebar_list = $sidebarDb->getSidebar($menu["tm_id"]);
				
				if(count($sidebar_list)>0){
					foreach($sidebar_list as $sb){
						
						if($sb['sm_parentid']!=0){//screen
							$sidebar['parent']='t'.$sb['sm_parentid'];
							$sidebar['id']='s'.$sb['sm_id'];
							
							for($i=1; $i<=5; $i++){
								
								//check from dbase
								$allow_access = $resnavDB->checkRoleNav($role_id,$menu["tm_id"],$sb['sm_parentid'],$sb['sm_id'],$i);
								
								$group['id']='g'.$i.'-'.$sidebar['id'].'-'.$sidebar['parent'].'-m'.$menu['tm_id'];
								$group['parent']=$sidebar['id'];
								$group['text']=$group_arr[$i];
								$group['icon']=false ;
								
								if($allow_access){
									$group['state']=array('opened'=>true,'selected' => true);									
								}else{
									$group['state']=array();									
								}
								array_push($menu_arr,$group);
							}					
							
							
						}else{ //title (parent to screen)
							$sidebar['parent']='m'.$sb['sm_tm_id'];
							$sidebar['id']='t'.$sb['sm_id'];
							
						}	
						$sidebar['text']=$sb['sm_name'];
						array_push($menu_arr,$sidebar);
					}
				}
			}			
			
		}//end if count topmenu*/
			
			
		$json = Zend_Json::encode($menu_arr);
		$this->view->menu_a = $menu_arr;
		
		//print_r($menu_arr);
		//print_r($menu_arr);		
		
	}
	
	public function ajaxSaveDataAction(){
		
		$role_id = $this->_getParam('role_id',null);
		$elements = $this->_getParam('elements',null);
		
		$auth = Zend_Auth::getInstance();
		
		$this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        		    
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();
          
        $resNavDB = new GeneralSetup_Model_DbTable_RoleResourcesNav();
        //1) Delete modified role 
        $resNavDB->deleteData($role_id);
        
        foreach($elements as $element){

			//string substr ( string $string , int $start [, int $length ] )
        	$first_element = substr($element,0,1);   
        	     	
        	if($first_element=='g'){
        		//ok
        		$element_arr = explode("-", $element);
        		
        		$data['rrn_role_id'] = $role_id;
        		$data['rrn_menu_id'] = substr($element_arr[3],1);
        		$data['rrn_title_id'] = substr($element_arr[2],1);
        		$data['rrn_screen_id'] = substr($element_arr[1],1);
        		$data['rrn_res_group'] = substr($element_arr[0],1);
        		$data['rrn_createddt'] = date('Y-m-d H:i:s');
        		$data['rrn_createdby'] = $auth->getIdentity()->iduser;

        		$a = $resNavDB->addData($data);
        		
        	}
       	}
       	
       	//hardcode add home resource
       	$data['rrn_role_id'] = $role_id;
       	$data['rrn_menu_id'] = 1;
       	$data['rrn_title_id'] = 0;
       	$data['rrn_screen_id'] = 0;
       	$data['rrn_res_group'] = 1;
       	$data['rrn_createddt'] = date('Y-m-d H:i:s');
       	$data['rrn_createdby'] = $auth->getIdentity()->iduser;
       	$resNavDB->addData($data);
       	
			
		$json = Zend_Json::encode(array('0'=>null));
		
		echo $json;
		exit();
	}
	
	
	
}

?>