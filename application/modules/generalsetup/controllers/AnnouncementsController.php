<?php

class GeneralSetup_AnnouncementsController extends Base_Base 
{	
	public function init() 
	{
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		

		//models
		$this->annModel = new App_Model_General_DbTable_Announcements();
		$this->defModel = new App_Model_General_DbTable_Definationms();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');

		$this->annTypes = $this->view->annTypes = $this->announcementTypes('DefinitionCode');

		$this->uploadDir = DOCUMENT_PATH.'/announcements';
		$this->auth = Zend_Auth::getInstance();
	}
	
	public function announcementTypes($by='idDefinition')
	{
		$get = $this->defModel->getByCode('Announcement Types');
		$data = array();
		
		foreach ( $get as $row )
		{
			$data[$row[$by]] = ($this->currLocale == 'en_US' ? $row['DefinitionDesc'] : $row['BahasaIndonesia']);
		}

		return $data;
	}

	//Index Action
	public function indexAction() 
	{
		$this->view->search = 0;

		//title
    	$this->view->title = "Announcements";

		$form = new GeneralSetup_Form_SearchAnnouncement();
		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$this->view->search = 1;
			$formData = $this->getRequest()->getPost();
			
			$searchData = array(
									'title'			=>	$formData['title'],
									'program_id'	=>	$formData['program_id'],
									'semester_id'	=>	$formData['semester_id'],
									'intake_id'		=>	$formData['intake_id'],
									'types'			=>	$formData['types']
								);
			
			$form->populate( $searchData );


			$p_data = $this->annModel->getData(0,1, $searchData);
		}
		else
		{
			$p_data = $this->annModel->getData(0,1);
		}
		
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
		$this->view->form = $form;
    	$this->view->paginator = $paginator;
	}

	//Add
	public function addAction()
	{
		$this->view->title = "New Announcement";
	

		//program
		$progModel = new Application_Model_DbTable_ProgramScheme();
        $progList = $progModel->fnGetProgramList();
		
		//intake
		$intakeDb = new GeneralSetup_Model_DbTable_Intake();
		$intakeList = $intakeDb->fngetIntakeList();

		//semester_id
		$semDB = new GeneralSetup_Model_DbTable_Semestermaster ();
		$semList = $semDB->fngetSemestermainDetails();

		//views
		$this->view->intakeList = $intakeList;
		$this->view->progList = $progList;
		$this->view->semList = $semList;

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
                        //var_dump($formData); exit;
			if ( count($formData['type']) == 0 )
			{
				throw new Exception('You must select atleast one type');
			}
			
			$data = array(
							'title'						=> $formData['title'], 
							'type'						=> implode(',',$formData['type']), 
							'content'					=> $formData['content'], 
							'program_id'					=> ifexists($formData['program_id'],0), 
							'intake_id'					=> ifexists($formData['intake_id'],0),
							'semester_id'					=> ifexists($formData['semester_id'],0),
							'tags'						=> '', 
							'sticky'					=> ifexists($formData['sticky'],0), 
							'active'					=> ifexists($formData['active'],0),
							'html'						=> ifexists($formData['html'],0),
							'created_by'				=> $this->auth->getIdentity()->iduser,
							'created_date'				=> new Zend_Db_Expr('NOW()')
						);

			$announcement_id = $this->annModel->add($data);
			
			$this->uploadFiles($announcement_id);
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New announcement added"));
		 
			$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'announcements', 'action'=>'index'),'default',true));

		}
	}
	
	//Edit
	public function editAction()
	{
		$this->view->title = "Edit Announcement";
		
		$id = $this->_getParam('id');

		$info = $this->annModel->getData($id);

		$this->view->info = $info;

		if ( empty($info) )
		{
			throw new Exception($this->view->translate('Invalid Announcement ID'));
		}

		//program
		$progModel = new Application_Model_DbTable_ProgramScheme();
        $progList = $progModel->fnGetProgramList();
		
		//intake
		$intakeDb = new GeneralSetup_Model_DbTable_Intake();
		$intakeList = $intakeDb->fngetIntakeList();

		//semester_id
		$semDB = new GeneralSetup_Model_DbTable_Semestermaster ();
		$semList = $semDB->fngetSemestermainDetails();

		//views
		$this->view->intakeList = $intakeList;
		$this->view->progList = $progList;
		$this->view->semList = $semList;

		//getfiles

		$files = $this->annModel->getFiles($id);

		//views
		$this->view->intakeList = $intakeList;
		$this->view->progList = $progList;
		$this->view->files = $files;

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
                        //var_dump($formData); exit;
			if ( count($formData['type']) == 0 )
			{
				throw new Exception('You must select atleast one type');
			}
			
			$data = array(
							'title'						=> $formData['title'], 
							'type'						=> implode(',',$formData['type']), 
							'content'					=> $formData['content'], 
							'program_id'				=> ifexists($formData['program_id'],0), 
							'intake_id'					=> ifexists($formData['intake_id'],0),
							'semester_id'				=> ifexists($formData['semeter_id'],0),
							'tags'						=> '', 
							'sticky'					=> ifexists($formData['sticky'],0), 
							'active'					=> ifexists($formData['active'],0),
							'html'						=> ifexists($formData['html'],0),
							'updated_by'				=> $this->auth->getIdentity()->iduser,
							'updated_date'				=> new Zend_Db_Expr('NOW()')
						);
                                                //var_dump($data); exit;
			$this->annModel->updateAnnouncement($data, $id);
			
			
			$this->uploadFiles($id);
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Announcement updated"));
		 
			$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'announcements', 'action'=>'edit', 'id'=>$id),'default',true));

		}
	}

	//upload file
	public function uploadFiles($announcement_id)
	{
		try 
		{
			$uploadDir = $this->uploadDir;

			if ( !is_dir( $uploadDir ) )
			{
				if ( mkdir_p($uploadDir) === false )
				{
					throw new Exception('Cannot create attachment upload folder ('.$uploadDir.')');
				}
			}
				


			$adapter = new Zend_File_Transfer_Adapter_Http();


		
			
			$adapter->addValidator('NotExists', false, $uploadDir );

			$adapter->setDestination( $uploadDir );
			
			//$adapter->addValidator('Count', false, 5);
			$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
			$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx,zip,xls,xlsx', 'case' => false));
			

$files = $adapter->getFileInfo();



			foreach ($files as $no => $fileinfo) 
			{
				if ($adapter->isUploaded($no))
				{
					$ext = getext($fileinfo['name']);

					$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
					$fileUrl = $uploadDir.'/'.$fileName;

					$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
					$adapter->setOptions(array('useByteString' => false));
					
					$size = $adapter->getFileSize($no);
				
					if( !$adapter->receive($no))
					{
						$errmessage = array();
						if ( is_array($adapter->getMessages() ))
						{	
							foreach(  $adapter->getMessages() as $errtype => $errmsg )
							{
								$errmessage[] = $errmsg;
							}
							
							throw new Exception(implode('<br />',$errmessage));
						}
					}
					
					//save file into db, , , 
					$data = array(
									'announcement_id'			=> $announcement_id,
									'filename'					=> $fileinfo['name'],
									'fileurl'					=> '/announcements/'.$fileName,
									'filesize'					=> $size
								);
						
					$this->annModel->addFile($data);
					
				} //isuploaded
				
			} //foreach


		}
		catch (Exception $e) 
		{
			throw new Exception( $e->getMessage() );
		}
	}

	public function deleteFileAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$file = $this->annModel->getFile($id);

			if ( empty($file) )
			{
				$data = array('msg' => 'Invalid ID');

				echo Zend_Json::encode($data);
			}

			$this->annModel->deleteFile($id);

			//unlink
			@unlink(DOCUMENT_PATH.'/'.$file['fileurl']);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
		}
	}
	
}
?>