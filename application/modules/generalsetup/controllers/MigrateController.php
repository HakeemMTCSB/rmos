<?php
ini_set('max_execution_time', -1); //forever
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class GeneralSetup_MigrateController extends Base_Base 
{	
	public function init() 
	{
		$this->fnsetObj();
		$this->view->translate = Zend_Registry::get('Zend_Translate');
		Zend_Form::setDefaultTranslator($this->view->translate);

		$this->lobjdeftype = new App_Model_Definitiontype();
	}

	public function fnsetObj() 
	{
		$this->migrateFolder = APPLICATION_PATH.'/migrate/';
	
		require_once APPLICATION_PATH.'/../library/Others/parsecsv.lib.php';
		
		$this->migrate = new GeneralSetup_Model_DbTable_Migrate();
		
		$this->view->migrateFolder = $this->migrateFolder;
		$this->countries = $files = $this->view->files = $this->view->filesizes = array();
		
		$auth = Zend_Auth::getInstance();
		$this->userId = $auth->getIdentity()->iduser;
	
		$this->view->title = 'Migrate';

	}

	//Index Action
	public function indexAction() 
	{
		//listing
		$this->view->files_general = $this->get_files($this->migrateFolder.'/general/','general');
		$this->view->filesizes_general = $this->filesizes['general'];

		$this->view->files_applicant = $this->get_files($this->migrateFolder.'/applicant/','applicant');
		$this->view->filesizes_applicant = $this->filesizes['applicant'];

		$this->view->files_student = $this->get_files($this->migrateFolder.'/student/','student');
		$this->view->filesizes_student = $this->filesizes['student'];

		$this->view->files_fin = $this->get_files($this->migrateFolder.'/finance/','fin');
		$this->view->filesizes_fin = $this->filesizes['fin'];
	}
	
	public function studentAction()
	{
		
		$branchDB = new GeneralSetup_Model_DbTable_Branchofficevenue();
		$appProfileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		$appTransactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
		$objdeftypeDB = new App_Model_Definitiontype();
		$countryDB = new App_Model_General_DbTable_Country();
		$entryReqDB= new App_Model_Application_DbTable_EntryRequirement();
		$appQualificationDB  = new App_Model_Application_DbTable_ApplicantQualification();
		$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
		$appVisaDB = new App_Model_Application_DbTable_ApplicantVisa();
		
		//scholarship
		if ($this->_request->isPost () && $this->_request->getPost ( 'scholarship' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$csv->auto($this->migrateFolder.'/student/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = $this->error_log = $this->programdata = $this->cache = array();

			$db = getDB();
			
			foreach ( $csv->data as $row )
			{
				$sql = $db->select()->from(array('p'=>'tbl_studentregistration'))->where('p.registrationId = ?', $row['Matric No'])->where('profileStatus = ?', 92);
				$student = $db->fetchRow($sql);

				echo $student['registrationId'] . '. ....';
				// ------------

				$data = array(
								'sa_cust_type'	=> 1,
								'sa_status'		=> 2,
								'sa_scholarship_type'	=> $row['Scholarship ID'],
								'sa_cust_id'	=> $student['IdStudentRegistration'],
								'sa_start_date' => date('Y-m-d', strtotime('1-'.$row['Start date']))
							);

				$db->insert('tbl_scholarship_studenttag', $data);

				// -------------
				echo 'OK <br />';
			}
		
			echo 'done';
			exit;
		}

		//email3
		if ( $this->_request->isPost() && $this->_request->getPost('mail3') )
		{
			/*
			SELECT p.appl_email_personal,r.registrationID FROM tbl_studentregistration r JOIN student_profile p ON (r.sp_id=p.id) 
			JOIN tbl_studentregsubjects s ON ( s.IdStudentRegistration = r.IdStudentRegistration )
			WHERE s.IdSemesterMain IN (6,11)
			AND r.profileStatus = 92
			GROUP BY r.IdStudentRegistration*/

			$db = getDb();
			$sql = $db->select()
						 ->from(array('p'=>'student_profile'),array('p.appl_email_personal','p.id','p.appl_fname','p.appl_lname'))
						 ->join(array('r'=>'tbl_studentregistration'),'r.sp_id=p.id',array())
						 ->join(array('s'=>'tbl_studentregsubjects'),'s.IdStudentRegistration=r.IdStudentRegistration', array())
						->where('s.IdSemesterMain IN (6,11)')
						->where('p.email_blast IS NULL')
						->where('r.profileStatus = ?', 92)
						->where('p.appl_email_personal != ?','')
						->group('r.IdStudentRegistration')
						->limit(30);

			$students = $db->fetchAll($sql);			
			define('CRON', true);
			$mail = new Cms_SendMail();
			$total = 0;
			foreach ( $students as $app )
			{
				$total++;
				
				$message = 'Dear All Students,'."<br /><br />";


 

				$message .= 'We would like to remind the students to constantly login into the eUniversity portal. In order to access the eUniversity portal, please click on the link below and enter your username and password to login: <br />';

				$message .= 'URL: <a href="http://e-university.inceif.org/">http://e-university.inceif.org/</a> <br /><br />';

				$message .= 'This is to ensure that all the students are updated with the latest information. Should you encounter any issues, please do redirect your queries to <a href="mailto:helpdesk@inceif.org">helpdesk@inceif.org</a> or contact them at +603 7651 4100.  Our Helpdesk operating hours are as below: <br /><br />';

				$message .='<table border="0" cellpadding="4"><tr><th width="35%" align="left">Day</th><th align="left" width="65%">Time</th></tr><tr><td>Monday to Thursday</td><td>8:30 am to 8.30 pm (+8.00GMT)</td></tr><tr><td>Friday</td><td>8.30 am to 12.30 pm, 2.30pm to 8.30 pm (+8.00GMT)</td></tr><tr><td colspan="2">Helpdesk services is not available on Saturday, Sunday and Public Holidays.</td></tr></table><br /><br />';

				$message .= 'Thank you and your cooperation is highly appreciated.<br />Regards,<br /><br />';

				$message .= '<b>Admission and Student Affairs Department</b><br />INCEIF<br /><br />';

				$message .= 'This is computer generated email. Please DO NOT REPLY to this email.';

				
				
				//send email
				$mail->fnSendMail($app['appl_email_personal'], $this->view->translate('Reminder: Please login to INCEIF eUniversity Portal'), $message);
				//$mail->fnSendMail('munzir@gmail.com', $this->view->translate('Reminder: Please login to INCEIF eUniversity Portal '), $message);
				//exit;
				
				$db->update('student_profile', array('email_blast'=>1), $db->quoteInto('id=?',$app['id']));

				
				$log[] = array('Student Name' => $app['appl_fname'].' '.$app['appl_lname'], 'Email' => $app['appl_email_personal']);
				
				echo 'Email sent to '.$app['appl_email_personal'].' <br />';
				//sleep(1);
			}
			
			if ( !empty($log) )
			{
				//$this->migrate->addLog('Student Email Blast', $log, $this->userId, '' );
			}
			
			if ( $total > 0 )
			{
				echo '<br />Total Sent: '.$total.'<br />';
				?>
				<form method="post" action="" id="myform">
					<input type="hidden" name="mail3" value="Mail #3" />
				</form>
				<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
				<script type="text/javascript">
				  $(document).ready(function() {
					$("#myform").submit();
				  });
				</script>
				<?
			}
			else
			{
				echo '<br />Total Sent: '.$total.'<br/ >';
				echo 'done';
			}

			exit;
		}

		//email2
		if ( $this->_request->isPost() && $this->_request->getPost('mail2') )
		{
			/*
			SELECT p.appl_email_personal,r.registrationID FROM tbl_studentregistration r JOIN student_profile p ON (r.sp_id=p.id) 
			JOIN tbl_studentregsubjects s ON ( s.IdStudentRegistration = r.IdStudentRegistration )
			WHERE s.IdSemesterMain IN (6,11)
			AND r.profileStatus = 92
			GROUP BY r.IdStudentRegistration*/

			$db = getDb();
			$sql = $db->select()
						 ->from(array('p'=>'student_profile'),array('p.appl_email_personal','p.id','p.appl_fname','p.appl_lname'))
						 ->join(array('r'=>'tbl_studentregistration'),'r.sp_id=p.id',array())
						 ->join(array('s'=>'tbl_studentregsubjects'),'s.IdStudentRegistration=r.IdStudentRegistration', array())
						->where('s.IdSemesterMain IN (6,11)')
						->where('p.email_blast IS NULL')
						->where('r.profileStatus = ?', 92)
						->where('p.appl_email_personal != ?','')
						->group('r.IdStudentRegistration')
						->limit(30);

			$students = $db->fetchAll($sql);			
			define('CRON', true);
			$mail = new Cms_SendMail();
			$total = 0;
			foreach ( $students as $app )
			{
				$total++;
				
				$message = 'Dear All Students,'."<br /><br />";

				$message .= 'INCEIF has embarked into a new learning management system called Moodle Learning Management System (MLMS). You have been given the username and password for you to access to MLMS via Student Portal upon registration.'."<br /><br />";

				$message .= 'Your respective lecturers have set up the online learning within the Moodle environment. Hence, you need to login to your Student Portal ----> MLMS on daily basis so you may access your assignments, quizzes, forum, interact with the lecturers in your course, upload assignments, access resources and much more.'."<br /><br />";

				$message .= 'If you need help, please contact the helpdesk (helpdesk@inceif.org).'."<br /><br />";

				$message .= 'Happy learning!'."<br /><br />";

				
				
				//send email
				$mail->fnSendMail($app['appl_email_personal'], $this->view->translate('INCEIF Learning Management System'), $message);
				//$mail->fnSendMail('munzir@gmail.com', $this->view->translate('INCEIF Learning Management System'), $message);
				//exit;
				
				$db->update('student_profile', array('email_blast'=>1), $db->quoteInto('id=?',$app['id']));

				
				$log[] = array('Student Name' => $app['appl_fname'].' '.$app['appl_lname'], 'Email' => $app['appl_email_personal']);
				
				echo 'Email sent to '.$app['appl_email_personal'].' <br />';
				//sleep(1);
			}
			
			if ( !empty($log) )
			{
				//$this->migrate->addLog('Student Email Blast', $log, $this->userId, '' );
			}
			
			if ( $total > 0 )
			{
				echo '<br />Total Sent: '.$total.'<br />';
				?>
				<form method="post" action="" id="myform">
					<input type="hidden" name="mail2" value="Mail #2" />
				</form>
				<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
				<script type="text/javascript">
				  $(document).ready(function() {
					$("#myform").submit();
				  });
				</script>
				<?
			}
			else
			{
				echo '<br />Total Sent: '.$total.'<br/ >';
				echo 'done';
			}

			exit;
		}

		//email
		if ( $this->_request->isPost() && $this->_request->getPost('mail') )
		{
			$db = getDb();
			$sql = $db->select()
						 ->from(array('a'=>'student_profile'))
						 ->join(array('b'=>'tbl_studentregistration'),'b.sp_id=a.id',array())
						->where('a.migrate_email IS NULL')
						->where('b.MigrateCode != ?','')
						->where('b.profileStatus = ?', 92)
						->where('b.IdIntake != ?', 18)
						->where('a.appl_email_personal != ?','')
						->limit(30);

			$students = $db->fetchAll($sql);			
			define('CRON', true);
			$mail = new Cms_SendMail();
			$total = 0;
			foreach ( $students as $app )
			{
				$total++;
				$key = generateRandomString(16);
				//$keylink = APPLICATION_URL."/online-application/reset/id/".$larrResult['appl_id']."/key/".$key;
				
				/*
				Dear <name student>,

				Kindly be informed that we have enhanced our eUniversity portal in order to provide a better 

				service and experience. In order to access the new eUniversity portal, please click on the 

				link below and enter the following username and password to login:

				URL:

				Username : xxxxxx

				Password : xxxxxxxx

				Once you have logged in, you may change your password at http://xxxxxxxxxxxxxx. 

				1. If you need help to navigate the new eUniversity portal, you can download the navigation 

				guide here http://xxxxxxxxxxxxxx

				2. If you need further technical assistance, please contact our helpdesk at 

				helpdesk@inceif.org. 

				3. If you wish to get in touch with our registration team, please contact us at 

				admission@inceif.org. 

				Thank you.

				Admission and Student Affairs Department

				INCEIF
				*/

				$message = 'Dear '.$app['appl_fname'].' '.$app['appl_lname'].','."<br /><br />";

				$message .= 'Kindly be informed that we have enhanced our eUniversity portal in order to provide a better service and experience. In order to access the new eUniversity portal, please click on the link below and enter the following username and password to login: '."<br /><br />";

				$message .= 'URL: http://e-university.inceif.org'."<br />";

				$message .= 'Username : '.$app['appl_username']."<br />";

				$message .= 'Password : '.$app['clear_pass']."<br /><br />";

				$message .= 'Once you have logged in, you may change your password at http://e-university.inceif.org/portal/profile/changepassword'."<br /><br />";

				$message .= '1. If you need help to navigate the new eUniversity portal, you can download the navigation 

				guide here <a href="http://cdn.inceif.org/documents/INCEIF-euni-manual.pdf">http://cdn.inceif.org/documents/INCEIF-euni-manual.pdf</a>'."<br /><br />";

				$message .= '2. If you need further technical assistance, please contact our helpdesk at <a href="mailto:helpdesk@inceif.org">helpdesk@inceif.org</a>.'."<br /><br />";

				$message .= '3. If you wish to get in touch with our registration team, please contact us at admission@inceif.org.'."<br /><br />";

				$message .= 'Thank you.'."<br /><br />";

				$message .= '<strong>Admission and Student Affairs Department</strong><br /><br />';

				//$message .= 'INCEIF';

				
				//send email
				$mail->fnSendMail($app['appl_email_personal'], $this->view->translate('INCEIF e-University - New Login Information'), $message);
				//$mail->fnSendMail('munzir@gmail.com', $this->view->translate('INCEIF e-University - New Login Information'), $message);
				
				$db->update('student_profile', array('migrate_email'=>1), $db->quoteInto('id=?',$app['id']));

				
				$log[] = array('Student Name' => $app['appl_fname'].' '.$app['appl_lname'], 'Email' => $app['appl_email_personal']);
				
				echo 'Email sent to '.$app['appl_email_personal'].' <br />';
				//sleep(1);
			}
			
			if ( !empty($log) )
			{
				$this->migrate->addLog('Student Email', $log, $this->userId, '' );
			}
			
			if ( $total > 0 )
			{
				echo '<br />Total Sent: '.$total.'<br />';
				?>
				<form method="post" action="" id="myform">
					<input type="hidden" name="mail" value="Mail" />
				</form>
				<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
				<script type="text/javascript">
				  $(document).ready(function() {
					$("#myform").submit();
				  });
				</script>
				<?
			}
			else
			{
				echo '<br />Total Sent: '.$total.'<br/ >';
				echo 'done';
			}

			exit;
		}
		
		//profile
		if ($this->_request->isPost () && $this->_request->getPost ( 'update' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$csv->auto($this->migrateFolder.'/student/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = $this->error_log = $this->programdata = $this->cache = array();

			$db = getDB();
			
			foreach ( $csv->data as $row )
			{
				$student_program = $this->getProgramData($row);
				$student = $this->getStudent($row['Student ID'].'-'.$student_program['IdProgram']);
				

				if ( !empty($student) )
				{
					$select = $db->select()
								 ->from(array('s'=>'student_qualification'))
								 ->where('s.sp_id = ?',$student['IdStudentRegistration']);

					$qual = $db->fetchRow($select);
					
					if ( preg_match("/00000/", $qual['ae_year_graduate']) )
					{
						$yeargrad = str_pad($row['Month of Graduation'], 2, '0', STR_PAD_LEFT).'/'.$row['Year of Graduation'];
						
						$db->update('student_qualification', array('ae_year_graduate' => $yeargrad), array('ae_id = ?'=> $qual['ae_id'] ));
					}
				}
			}
		
			echo 'done';
			exit;
		}

		//profile
		if ($this->_request->isPost () && $this->_request->getPost ( 'profile' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$csv->auto($this->migrateFolder.'/student/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = $this->error_log = $this->programdata = $this->cache = array();

			$db = getDB();
			 $this->hasappend = 0;
			 $count=0;
			$first = $last = '';
			foreach ( $csv->data as $row )
			{
				$student_program = $this->getProgramData($row);

				if ( $this->checkStudent($row['Student ID'].'-'.$student_program['IdProgram']) == false )
				{
					$count++;
					if ( $count == 1 ) { $first = $row['Student ID']; } 
					$last = $row['Student ID'];
					
					$this->processStudentData($row);
					
					if ( $count == 1000 )
					{
						break;
					}
					
					$log[] = $row;
				}
			}

			

			if ( !empty($this->error_log) )
			{
				$this->migrate->addLog('Student Profile '.$first.'-'.$last.' - Error', $this->error_log, $this->userId, $formData['file'] );
			}

			if ( !empty($log) )
			{
				$this->migrate->addLog('Student Profile '.$first.'-'.$last, $log, $this->userId, $formData['file'] );
			}
			
			echo 'Total Added:'.$count.'<br />';
			echo 'done';
			exit;
		}
	
		
		//profile
		if ($this->_request->isPost () && $this->_request->getPost ( 'visiting' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$csv->auto($this->migrateFolder.'/student/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = $this->error_log = $this->programdata = $this->cache = array();

			$db = getDB();
			 $this->hasappend = 0;
			 $count=0;
			$first = $last = '';
			foreach ( $csv->data as $row )
			{
				if ( strtolower($row['Student ID'][0]) == 'v' )
				{
					//$student_program = $this->getProgramData($row);
					
					if ( $this->checkStudent($row['Student ID']) == false )
					{
						$count++;
						if ( $count == 1 ) { $first = $row['Student ID']; } 
						$last = $row['Student ID'];
						
						$this->processStudentData($row,1);
						
						if ( $count == 1000 )
						{
							break;
						}
						
						$log[] = $row;

					}
				}
			}


			if ( !empty($this->error_log) )
			{
				$this->migrate->addLog('Visiting Student Profile '.$first.'-'.$last.' - Error', $this->error_log, $this->userId, $formData['file'] );
			}

			if ( !empty($log) )
			{
				$this->migrate->addLog('Visiting Student Profile '.$first.'-'.$last, $log, $this->userId, $formData['file'] );
			}
			
			echo 'Total Added:'.$count.'<br />';
			echo 'done';
			exit;
		}
	}
	
	public function processStudentData($row, $visiting=0)
	{	
		$studentid = $ref_id = $row['Student ID'];
		
		$branchDB = new GeneralSetup_Model_DbTable_Branchofficevenue();
		$appProfileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		$appTransactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
		$objdeftypeDB = new App_Model_Definitiontype();
		$countryDB = new App_Model_General_DbTable_Country();
		$entryReqDB= new App_Model_Application_DbTable_EntryRequirement();
		$appQualificationDB  = new App_Model_Application_DbTable_ApplicantQualification();
		$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
		$appVisaDB = new App_Model_Application_DbTable_ApplicantVisa();

		$db = getDB();
		$i = 0;
		foreach ( $row as $field => $value )
		{
			$i++;
			switch ( $field ) 
			{
				case 'Programme':
					$student_program = array();
					if ( $visiting == 0 ) 
					{
						$student_program = $this->getProgramData($row);
					}
				break;

				case 'Idtype':
					$row['Idtype'] = str_replace('Military Number', 'Military ID/Police ID', $row['Idtype']);
					$row['Idtype'] = str_replace('Malaysian IC', 'MyKad', $row['Idtype']);

					if ( !isset($this->cache['id_type'][$row['Idtype']]) )
					{
						$id_type = $this->getIdByDef($row['Idtype'],$objdeftypeDB->fnGetDefinations('ID Type'));
					}
					else
					{
						$id_type = $this->cache['id_type'][$row['Idtype']];
					}

					if ( empty($id_type ) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Session Intake':

					if ( $visiting == 0 )
					{
						$getintake = explode(' ', str_replace(' Semester','', $row['Session Intake']));
						$intakename = $getintake[1].' '.$getintake[0];

						$row['Session Intake'] = $intakename;

						if ( !isset($this->cache['intake'][$row['Session Intake']]) )
						{
							$intake_id = $this->getIntake($row['Session Intake']);
						}
						else
						{
							$intake_id = $this->cache['intake'][$row['Session Intake']];
						}

						if ( empty($intake_id) ) $this->logItem($field, $row, $ref_id);
					}
				break;

				case 'Nationality':
				
					//values i dont like
					$row['Nationality'] = str_replace('SOUTH KOREAN','KOREAN', $row['Nationality']);
					$row['Nationality'] = str_replace('FILIPINA', 'FILIPINO', $row['Nationality']);
					
					if ( !isset($this->cache['countries']) )
					{
						$countries = $this->cache['countries'] = $countryDB->getData();
					}
					else
					{
						$countries = $this->cache['countries'];
					}

					$nationality = $this->getIdByDef($row['Nationality'],$countries,array('k'=>'Nationality','v'=>'idCountry'));
					if ( empty($nationality) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Religion':

					if ( !isset($this->cache['religion']) )
					{
						$religionlist = $this->cache['religion'] = $objdeftypeDB->fnGetDefinations('Religion');
					}
					else
					{
						$religionlist = $this->cache['religion'];
					}

					$religion = $this->getIdByDef($row['Religion'],$religionlist);
					if ( empty($religion) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Permanent State':

					$row['Permanent State'] = $this->filter($row['Permanent State'], 'states');
					
					$perm_state = $this->getState($row['Permanent State']);
					if ( empty($perm_state) ) $this->logItem($field, $row, $ref_id);
				break;
				
				case 'Permanent Country':
					$row['Permanent Country'] = $this->filter($row['Permanent Country'], 'country');
					$perm_country = $this->getIdByDef($row['Permanent Country'],$countries,array('k'=>'CountryName','v'=>'idCountry'));
					if ( empty($perm_country) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Correspondence State':
					$row['Correspondence State'] = $this->filter($row['Correspondence State'], 'states');
					$corr_state = $this->getState($row['Correspondence State']);
					if ( empty($corr_state) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Correspondence Country':
					$row['Correspondence Country'] = $this->filter($row['Correspondence Country'], 'country');
					$corr_country = $this->getIdByDef($row['Correspondence Country'],$countries,array('k'=>'CountryName','v'=>'idCountry'));
					if ( empty($corr_country) ) $this->logItem($field, $row, $ref_id);
				break;
				
				case 'Race':
					
					$row['Race'] = $this->filter($row['Race'], 'race');
						
					if ( !isset($this->cache['race']) )
					{
						$racelist = $this->cache['race'] = $objdeftypeDB->fnGetDefinations('Race');
					}
					else
					{
						$racelist = $this->cache['race'];
					}


					$race = $this->getIdByDef($row['Race'],$racelist);
					if ( empty($race) ) $this->logItem($field, $row, $ref_id);
				break;
				
				case 'Marital Status':
					$row['Marital Status'] = $this->filter($row['Marital Status'], 'marital');

					if ( !isset($this->cache['marital']) )
					{
						$maritallist = $this->cache['marital'] = $objdeftypeDB->fnGetDefinations('Marital Status');
					}
					else
					{
						$maritallist = $this->cache['marital'];
					}

					$marital_status = $this->getIdByDef($row['Marital Status'],$maritallist);

					if ( empty($marital_status) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Status':
					if ( !isset($this->cache['status']) )
					{
						$statuslist = $this->cache['status'] = $objdeftypeDB->fnGetDefinations('Profile Status');
					}
					else
					{
						$statuslist = $this->cache['status'];
					}

					$row['Status'] = $this->filter($row['Status'], 'student_status');
					if ( $row['Status'] == 'Active (Special Case)')
					{
						$row['Status'] = str_replace('Active (Special Case)','Active', $row['Status']);
					}
				
					$status = $this->getIdByDef($row['Status'], $statuslist);
					
					if ( empty($status) ) 
					{
						$this->logItem($field, $row, $ref_id);
					}
				break;

				case 'Highest Level':
					$row['Highest Level'] = $this->filter($row['Highest Level'],'highestlevel');
					$highest_level = $this->getIdByDef($row['Highest Level'],$entryReqDB->getListGeneralReq(),array('k'=>'QualificationLevel','v'=>'IdQualification'));
					if ( empty($highest_level) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Class':
					$row['Class'] = $this->filter($row['Class'],'classdegree');
					$class_degree = $this->getIdByDef($row['Class'],$objdeftypeDB->fnGetDefinations('Class Degree'));
					if ( empty($class_degree) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'English Proficiency':
					$row ['English Proficiency'] = $this->filter($row['English Proficiency'], 'proficiency');
					
					$qualificationDB = new App_Model_General_DbTable_Qualificationmaster();
					if ( !isset($this->cache['english']))
					{
						$quallist = $this->cache['english'] = $qualificationDB->getDataTest();
					}
					else
					{
						$quallist = $this->cache['english'];
					}
					$proficiency_english = $this->getIdByDef($row['English Proficiency'],$quallist,array('k'=>'QualificationLevel','v'=>'IdQualification'));
					
					if ( empty($proficiency_english) )  $this->logItem($field, $row, $ref_id);
				break;
				

			}//switch
		}//foreach
		
		$names = explode(' ',strtoupper($row['Student Name']));
		if ( $names[0] == 'MD.' )
		{
			$row['fname'] = $names[0].' '.$names[1];
			unset($names[0], $names[1]);

			$row['lname'] = trim( implode(' ', $names) );
		}
		else
		{
			$row['fname'] = $names[0];
			unset($names[0]);
			$row['lname'] = trim(implode(' ', $names));
		}
		
		/*
		`appl_address1`, 
		`appl_address2`, 
		`appl_address3`, 
		`appl_postcode`, 
		`appl_city`, 
		`appl_state`, 
		`appl_country`, 
		`appl_cstate_others`,
		`appl_ccity_others`, 
		`appl_state_others`, 
		`appl_city_others`, 
		`appl_prefer_lang`, 
		`appl_nationality`, 
		`appl_type_nationality`, 
		`appl_category`, 
		`appl_admission_type`, 
		`appl_religion`, 
		`appl_religion_others`, 
		`appl_race`, 
		`appl_race_others`, 
		`appl_marital_status`, 
		`appl_bumiputera`, 
		`appl_no_of_child`, 
		`appl_phone_home`, 
		`appl_phone_mobile`, 
		`appl_phone_office`, 
		`appl_fax`, 
		`appl_caddress1`, 
		`appl_caddress2`, 
		`appl_caddress3`, 
		`appl_ccity`, 
		`appl_cstate`, 
		`appl_ccountry`
		`appl_cpostcode`, 
		`appl_cphone_home`, 
		`appl_cphone_mobile`, 
		`appl_cphone_office`,
		`appl_cfax`, 
		`appl_contact_home`, 
		`appl_contact_mobile`, 
		`appl_contact_office`, 
		`change_passwordby`, 
		`change_passworddt`, 
		`appl_role`, 
		`create_date`, 
		`upd_date`, 
		`migrate_date`, 
		`migrate_by`, 
		`sp_repository`
		*/
		
		//student_profile
		if ( empty( $student_program ) && $visiting == 0 )
		{
			return;
		}

		$appl_info['appl_salutation'] = $row['Sal.'] == 'Ms.' ? 86:85;
		$appl_info['appl_fname'] = $row['fname'];
		$appl_info['appl_lname'] = $row['lname'];
		$appl_info["appl_email"]= $row['Student ID'].'@inceif.org';
		$appl_info['appl_email_personal'] = $row['Email Id'];
		$appl_info["create_date"]=date("Y-m-d H:i:s", strtotime('Applied Date'));
		$appl_info['appl_dob']=date('Y-m-d',strtotime($row['Date of Birth']));
		$appl_info['appl_idnumber'] = $row['Idnumber'];
		$appl_info['appl_idnumber_type'] = $id_type;
		$appl_info['appl_nationality'] = $nationality;

		$appl_info['appl_religion'] = $religion;
		$appl_info['appl_address1'] = $row['Permanent Address'];
		$appl_info['appl_city'] = 99;
		$appl_info['appl_city_others'] = $row['Permanent City'];
		
		if ( empty($perm_state) && $row['Permanent State'] != '' )
		{
			$appl_info['appl_state'] = 99;
			$appl_info['appl_state_others'] = $row['Permanent State'];
		}
		else
		{
			$appl_info['appl_state'] = $perm_state;
		}

		$appl_info['appl_country'] = $perm_country;
		$appl_info['appl_postcode'] = $row['Permanent Postcode'];
		
		$appl_info['appl_caddress1'] = $row['Correspondence Address'];
		
		if ( $row['Correspondence City'] != '' )
		{
			$appl_info['appl_ccity'] = 99;
			$appl_info['appl_ccity_others'] = $row['Correspondence City'];
		}

		if ( empty($corr_state) && $row['Correspondence State'] != '' )
		{
			$appl_info['appl_cstate'] = 99;
			$appl_info['appl_cstate_others'] = $row['Correspondence State'];
		}
		else
		{
			$appl_info['appl_cstate'] = $corr_state;
		}
		
		$appl_info['appl_cpostcode'] = $row['Correspondence Postcode'];
		$appl_info['appl_ccountry'] = $corr_country;

		$appl_info['appl_phone_home'] = $row['Contact No'];
		$appl_info['appl_fax'] = $row['Fax'];
		$appl_info['appl_phone_mobile'] = $row['Mobile No'];
		$appl_info['appl_cphone_home'] = '';
		$appl_info['appl_cphone_mobile'] = '';
		$appl_info['appl_cfax'] = '';

		$appl_info['appl_race'] = $race;
		$appl_info['appl_gender'] = $row['Sal.'] == 'Ms.' ? 2 : 1;
		$appl_info['appl_marital_status'] = $marital_status;
		
		$appl_info['MigrateCode'] = date('dmy');
		$appl_info['migrate_date'] = new Zend_Db_Expr('NOW()');
		$appl_info['migrate_by'] = 1; //system
		//
		
		if ( $id_type == 487 )
		{
			$appl_info['appl_type_nationality'] = 547;
			$appl_info['appl_category'] = 579;
		}
		else
		{
			$appl_info['appl_type_nationality'] = 549;
			$appl_info['appl_category']=580;
		}
	
		$studentDB = new Records_Model_DbTable_Studentprofile();
		$student_id = $studentDB->addData($appl_info);

		//Add into student_profile
		$student_path = DOCUMENT_PATH.'/student/'.date('Ym');
									
		//create directory to locate fisle			
		if (!is_dir($student_path)) {
			mkdir_p($student_path, 0775);
		}    			
				
		$txn_path = $student_path."/".$student_id;
		
		//create directory to locate file			
		if (!is_dir($txn_path)) {
			mkdir_p($txn_path, 0775);
		}							
		
		//update repository info	
		$rep['sp_repository']='/student/'.date('Ym').'/'.$student_id;								
		$studentDB->updateData($rep,$student_id);
		
		// ---------------------------
		// tbl_studentregistration
		// ---------------------------

		//programs
		$data = array();
		

		$data['IdProgram'] = $visiting == 1 ? 0 : $student_program['IdProgram'];
		$data['sp_id'] = $student_id;
		$data["IdApplication"]  = 0;	
		$data["transaction_id"] = 0;
		
		$appendx=0;
	
		$data['registrationId'] = $visiting == 1 ? strtoupper($row['Student ID']) : $row['Student ID'];

		echo $row['Student ID'].' ('.$row['Status'].' - '.$status.')<br />';

		$data['IdSemestersyllabus'] = 0;						
		$data['IdSemester'] = 0;											
		$data['IdSemesterMain'] = 0;
		$data['IdSemesterDetails'] = 0;
		$data['IdIntake'] = $visiting == 1 ? 0 : $intake_id;
		$data['IdLandscape'] = 0;
		$data['IdBranch'] = 1;					
		$data['IdProgramScheme'] = $visiting == 1 ? 0 : $student_program['IdProgramScheme'];
		$data['student_type'] = $visiting == 1 ? 741 : 740; //normal
		$data['senior_student'] = 0;					
		$data['IdLandscape']=0;
		$data['IdSemestersyllabus']=0;
		$data['IdSemesterMain']=0;
		$data['IdSemesterDetails']=0;
														
		$data['Status'] = 198; // tbl_definationms Semester Status Active
		$data['profileStatus'] = $status; //tbl_definationms idDefType = 20 ( Student Status )
		$data['UpdUser'] = $this->userId;
		$data['UpdDate'] = date ( 'Y-m-d H:i:s' );
		$data['MigrateCode'] = date('dmy');
		$data['ref_id'] = $visiting == 1 ? $row['Student ID'] : $row['Student ID'].'-'.$student_program['IdProgram'];
		
		$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
		$IdStudentRegistration = $studentRegistrationDb->addData($data);
		
	
		if ( $visiting == 0 )
		{
			//qualification 
			$data = array(
							'sp_id'							=>	$IdStudentRegistration, 
							'ae_appl_id'					=>	0, 
							'ae_transaction_id'				=>	0, 
							'ae_qualification'				=> $highest_level,
							'ae_degree_awarded'				=> $row['Degree'],
							'ae_class_degree'				=> $class_degree,
							'ae_result'						=> $row['cgpa'],
							'ae_year_graduate'				=> sprintf('%08d', $row['Month of Graduation']).'/'.$row['Year of Graduation'],
							'ae_institution_country'		=> '',
							'ae_institution'				=> 999,
							'ae_medium_instruction'			=> '',	
							'others'						=> $row['University'],	    		  	    		
							'createDate'					=> date("Y-m-d H:i:s")
					);

			$db->insert('student_qualification', $data);

			//english proficiency
			$data4 = array(   
							'sp_id'							=> $IdStudentRegistration,
							'ep_transaction_id'				=> 0,
							'ep_test'						=> $proficiency_english,
							'ep_test_detail'				=> '',
							'ep_date_taken'					=> '0000-00-00',
							'ep_score'						=> $row['Result for English Proficiency'] == '' ? 0 : $row['Result for English Proficiency'],
							'ep_updatedt'					=> date("Y-m-d H:i:s"),
							'ep_updateby'					=> 1
			);
			$db->insert('student_english_proficiency', $data4);

			//employment
			$data8 = array(	    
							'sp_id'	 => $IdStudentRegistration,
							'ae_appl_id' => 0,
							'ae_trans_id' => 0,	 
							'ae_status' => '',
							'ae_comp_name' => $row['Company'],
							'ae_comp_address' => '',
							'ae_comp_phone' => '',
							'ae_comp_fax' => '',
							'ae_designation' => '',
							'ae_position' => '',
							'ae_from' => '',
							'ae_to' => '',	    		  
							'emply_year_service' => $row['Years of Service'],
							'ae_industry' => '',	 
							'ae_job_desc' => '',	    		   	    		
							'upd_date' => date("Y-m-d H:i:s")
			);
			$db->insert('student_employment', $data8);
		}
		
		//photo
		$photo = $this->getPhoto($ref_id,'/student/'.date('Ym').'/'.$student_id);
		if ( !empty($photo) )
		{
			if ( isset($photo['fullpath']) && $photo['fullpath'] != '' )
			{
				$photo_data = array(
										'sp_id'			=> $IdStudentRegistration, 
										'ad_dcl_id'		=> 0,
										'ad_ads_id'		=> 0,
										'ad_section_id'		=> 2,
										'ad_type'		=> 67,
										'ad_table_name'		=> '',
										'ad_table_id'		=> 0, 
										'ad_filepath'		=> $photo['fullpath'], 
										'ad_filename'		=> $photo['filename'],
										'ad_ori_filename'	=> $photo['filename'], 
										'ad_createddt'		=> new Zend_Db_Expr('NOW()'), 
										'ad_createdby'		=> $this->userId
									);

				$db->insert('student_documents', $photo_data);
			}
			
		}
		
			
	}	


	function getProgramData($row)
	{
		$objdeftypeDB = new App_Model_Definitiontype();
		
		if ( isset($this->programdata[$row['Programme'].'-'.$row['Program Status'].'-'.$row['Program Mode']]) )
		{
			return $this->programdata[$row['Programme'].'-'.$row['Program Status'].'-'.$row['Program Mode']];
		}

		$getprog = explode('(', $row['Programme']);
		$program_name = trim($getprog[0]);
		$row['Program Option'] = trim(str_replace(')','',$getprog[1]));

		$program =$this->getProgram($program_name);

		if ( !empty($program) )
		{
			
			//proceed
			$info3 = array();
			
			$mode_study = $mode_program = $program_type = 0;
		
			switch ( $program['ProgramCode'] )
			{
				case 'PhD':

					//PhD in Islamic Finance (PhD by Coursework and Dissertation)
					$bytype = explode('by', str_replace('Coursework and Dissertation','Coursework & Dissertation', $row['Program Option']));
					$progtype = 'By '.trim($bytype[1]);

					$row['Program Mode']	= $row['Program Mode'];
					$row['Mode of program'] = str_replace('-','',$row['Program Status']);
					$row['Program Type']	= $progtype;

					$row['Mode of program'] = $row['Mode of program'] == '' ? 'Face to Face' : $row['Mode of program'];
				break;

				case 'MSc': case 'MIF': 
					
					$bytype = explode('by', str_replace('Coursework and Dissertation','Coursework & Dissertation', $row['Program Option']));
					$progtype = 'By '.trim($bytype[1]);

					$row['Program Mode']	= $row['Program Mode'];
					$row['Mode of program'] = $row['Program Status'] == '-' ? 'Face to Face' : $row['Program Status']; 
					$row['Program Type']	= $progtype;

				break;
				
				case 'CIFP':
					$row['Program Mode']	= $row['Program Mode'];
					$row['Mode of program'] = $row['Program Status'];
					$row['Program Type']	= '';
				break;

				case 'MIFP':
					$row['Program Mode']	= $row['Program Mode'];
					$row['Mode of program'] = $row['Program Status'];
					$row['Program Type']	= '';
				break;
			}


			
			//Program Mode = Mode of Study
			//Mode of Program = 
			$mode_study = $this->getIdByDef(str_replace('-',' ',$row['Program Mode']),$objdeftypeDB->fnGetDefinations('Mode of Study'));
			$mode_program = $this->getIdByDef($row['Mode of program'],$objdeftypeDB->fnGetDefinations('Mode of Program'));
			$program_type = $this->getIdByDef($row['Program Type'],$objdeftypeDB->fnGetDefinations('Program Type',0));
			
		
			if ( ( $program['ProgramCode'] == 'CIFP' || $program['ProgramCode'] == 'MIFP' ) || $row['Program Type'] == '' )
			{
				$program_type = 627;
			}
			
			
		
			//get program scheme
			$appProgramDB  = new App_Model_Application_DbTable_ApplicantProgram();	
			
			$scheme = $appProgramDB->getProgramScheme($program['IdProgram'],$mode_study,$mode_program,$program_type);
			
			if ( !empty( $scheme) ) 
			{
				$IdProgramScheme = $scheme['IdProgramScheme'];
				
				//echo $mode_study.'-'.$mode_program.'-'.$program_type;
				//echo 'Scheme:'.$IdProgramScheme;
				
				$this->programdata[$row['Programme'].'-'.$row['Program Status'].'-'.$row['Program Mode']] = array('IdProgram' => $program['IdProgram'],
																												  'IdProgramScheme' => $IdProgramScheme);

				return $this->programdata[$row['Programme'].'-'.$row['Program Status'].'-'.$row['Program Mode']];
			}
			else
			{
				print_R($row);
				echo $mode_study.'-'.$mode_program.'-'.$program_type;
				exit;

				$this->error_log[] = array('App ID' => $row['Student ID'], 'ErrorType' => 'Invalid Program Scheme', 'Value' => $row['Programme'].'-'.$row['Program Mode'].'-'.$row['Mode of program']);
			}
		}
		else
		{
			$this->error_log[] = array('App ID' => $row['Student ID'], 'ErrorType' => 'Invalid Program', 'Value' => $row['Programme']);
		}

	}
	
	public function financeAction()
	{
		
		$branchDB = new GeneralSetup_Model_DbTable_Branchofficevenue();
		$appProfileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		$appTransactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
		$objdeftypeDB = new App_Model_Definitiontype();
		$countryDB = new App_Model_General_DbTable_Country();
		$entryReqDB= new App_Model_Application_DbTable_EntryRequirement();
		$appQualificationDB  = new App_Model_Application_DbTable_ApplicantQualification();
		$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
		$appVisaDB = new App_Model_Application_DbTable_ApplicantVisa();
		
		$currencyDb	= new Studentfinance_Model_DbTable_Currency();

		$currencies = $currencyDb->fetchAll()->toArray();
		$this->currencies = array();
		
		foreach ( $currencies as $curr )
		{
			$this->currencies[$curr['cur_id']] = $curr;
			$this->currencies[$curr['cur_code']] = $curr;
		}

		
		
		//stuff
		if ($this->_request->isPost () && $this->_request->getPost ( 'resetinv' ))
		{
			/*SELECT * FROM `proforma_invoice_main` WHERE bill_number NOT LIKE 'P%'*/

			$db = getDB();
			$select = $db->select()
	                 ->from(array('s'=>'proforma_invoice_main'))
	                 ->where("bill_number NOT LIKE 'P%'")
					 //->where("SUBSTR(bill_number,6,1) = ?",'/');
					 ->orWhere('MigrateCode IS NOT NULL');
			$rows = $db->fetchAll($select);
			$deleted=0;
			foreach ( $rows as $row )
			{
				$db->delete('proforma_invoice_main','id = '.(int)$row['id']);
				$db->delete('proforma_invoice_detail','proforma_invoice_main_id = '.(int)$row['id']);
				$deleted++;

				$this->log[] = array(
							
							'Invoice No'			=> $row['bill_number']
						);
			}
			
			//delete invoices
			$select = $db->select()
	                 ->from(array('s'=>'invoice_main'))
	                 //->where("bill_number NOT LIKE 'P%'");
					 ->where("SUBSTR(bill_number,5,1) = ?",'/')
					 ->orWhere('MigrateCode IS NOT NULL');
			$rows = $db->fetchAll($select);
			
			foreach ( $rows as $row )
			{
				$db->delete('invoice_main','id = '.(int)$row['id']);
				$db->delete('invoice_detail','invoice_main_id = '.(int)$row['id']);
			}

			if ( !empty($this->log) )
			{
				$this->migrate->addLog('Invoice Deleted', $this->log, $this->userId,'' );
			}

			echo 'Total Deleted: '.$deleted.'<br />';
			echo 'done';
			exit;
		}


		//stuff
		if ($this->_request->isPost () && $this->_request->getPost ( 'reset' ))
		{
			$db = getDB();
			$select = $db->select()
	                 ->from(array('s'=>'receipt'))
	                 ->where('MigrateCode IS NOT NULL');
			$rows = $db->fetchAll($select);
			$deleted=0;
			foreach ( $rows as $row )
			{
				$db->delete('receipt','rcp_id = '.(int)$row['rcp_id']);
				$db->delete('payment','p_rcp_id = '.(int)$row['rcp_id']);
				$deleted++;

				$this->log[] = array(
							
							'Receipt No'			=> $row['rcp_no'],
							'Description'			=> $row['rcp_description']
						);
			}
			
			if ( !empty($this->log) )
			{
				$this->migrate->addLog('Receipts Deleted', $this->log, $this->userId,'' );
			}

			echo 'Total Deleted: '.$deleted.'<br />';
			echo 'done';
			exit;
		}


		//receipts
		if ($this->_request->isPost () && $this->_request->getPost ( 'receipts' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$csv->auto($this->migrateFolder.'/finance/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$this->log = $this->error_log = $this->trans = array();

			$db = getDB();
			foreach ( $csv->data as $row )
			{
				$this->processReceiptsData($row);				
			}

			if ( !empty($this->error_log) )
			{
				$this->migrate->addLog('Receipts - Error', $this->error_log, $this->userId, $formData['file'] );
			}

			if ( !empty($this->log) )
			{
				$this->migrate->addLog('Receipts', $this->log, $this->userId, $formData['file'] );
			}
	
		}


		//invoice
		if ($this->_request->isPost () && $this->_request->getPost ( 'invoice' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$csv->auto($this->migrateFolder.'/finance/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$this->log = $this->error_log = $this->trans = array();

			$db = getDB();
			foreach ( $csv->data as $row )
			{
				$this->processInvoiceData($row);
			}
			
			$this->processKnockoff();
			

			if ( !empty($this->error_log) )
			{
				$this->migrate->addLog('Invoice - Error', $this->error_log, $this->userId, $formData['file'] );
			}

			if ( !empty($this->log) )
			{
				$this->migrate->addLog('Invoice', $this->log, $this->userId, $formData['file'] );
			}
	
		}



		echo 'done';
		exit;
	}

	public function processKnockoff()
	{
		$db = getDB();
	  	$select = $db->select()
	                 ->from(array('s'=>'receipt'))
					 ->where('s.MigrateCode IS NOT NULL');

		$receipts = $db->fetchAll($select);

		foreach ( $receipts as $receipt )
		{
			//get default currency
			$currencyDb = new Studentfinance_Model_DbTable_Currency();
			$currencyData = $currencyDb->getDefaultCurrency();
			$defaultCurrency = $currencyData['cur_id'];
				
			$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
			$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
			$invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();
			
			$totalAmount = 0.00;
			$total_advance_payment_amount = 0;
			$total_payment_amount = $receipt['rcp_amount'];
			$total_paid_receipt = 0;
			$paidReceiptInvoice = 0;
			
			//get receipt invoice
			$receiptInvDb = new Studentfinance_Model_DbTable_ReceiptInvoice();
			$rcpinv = $receiptInvDb->getDataFromReceipt($receipt['rcp_id']);
			foreach ($rcpinv as $inv)
			{
				$invID = $inv['rcp_inv_invoice_id'];
				$invDetID = $inv['rcp_inv_invoice_dtl_id'];
				$rcpCurrency = $inv['rcp_inv_cur_id'];
				
				$invDetData = $invoiceDetailDb->getData($invDetID);
			
				$invCurrency = $invDetData['cur_id'];
				if ( empty($invCurrency) )
				{
					echo $invDetID;
					print_R($inv);
					exit;
				}

				$currencyDb = new Studentfinance_Model_DbTable_Currency();
				$currency = $currencyDb->fetchRow(array('cur_id = ?'=>$invCurrency))->toArray();

				$amountDefault = $invDetData['balance'];
				
				$paidReceiptInvoice = $inv['rcp_inv_amount'];
				
				if($invCurrency == $rcpCurrency){
					$paidReceipt = $inv['rcp_inv_amount'];
					
				}else{
					$paidReceipt = $inv['rcp_inv_amount_default_currency'];
				}
						
				$balance  = $amountDefault  - $paidReceipt;
				
				//update invoice details
				$dataInvDetail = array(
					'paid'=> $paidReceipt +  $invDetData['paid'] ,
					'balance' => $balance,
				);
				
				$invoiceDetailDb->update($dataInvDetail, array('id =?'=>$invDetID) );
				
				$totalAmount += $paidReceiptInvoice;
				
				//update invoice main
				$invMainData = $invoiceMainDb->getData($invID);
					
				$balanceMain = ($invMainData['bill_balance']) - ($paidReceipt);
				$paidMain = ($invMainData['bill_paid']) + ($paidReceipt);
				
				//update invoice main
				$dataInvMain = array(
					'bill_paid'=> $paidMain,
					'bill_balance' => $balanceMain,
					'upd_by'=>1,
					'upd_date'=>date('Y-m-d H:i:s')
				);
				
				$invoiceMainDb->updateTableData('invoice_main', $dataInvMain, array('id =?'=>$invID) );	
			}

		}//foreach
	}

	public function processReceiptsData($row=array())
	{
		$db = getDB();
		$i = 0;

		$applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
		$txnData = $applicantTxnDB->getData($row['applicant id'], 'at_pes_id');

		if ( empty($txnData) )
		{
			$this->error_log[] =  array('App ID' => $row['applicant id'], 'ErrorType' => 'Invalid Applicant', 'Value' => $row['applicant id']);
			return false;
		}
		
		else
		{

			foreach ( $row as $field => $value )
			{
				$i++;
				
				switch ( $field ) 
				{
					case 'applicant id':
						
					break;

					case 'name':

					break;

					case 'semester';

					break;

					case 'invoice date':
					break;

					case 'invoice No':

					break;

					case 'fee code':
						
					break;

					case 'amt (rm)':
						$row['amt (rm)'] = $this->cleanAmt($row['amt (rm)']);
					break;

					case 'amt(usd)':
						$row['amt(usd)'] = $this->cleanAmt($row['amt(usd)']);
					break;

					case 7:

					break;
					
				}//switch

			}//foreach

			
			//add receipt
			//add payment details

			$receiptDb = new Studentfinance_Model_DbTable_Receipt();
			
			$receipt_data = array(
				'rcp_no' => $row['receipt no'],
				'rcp_account_code' => 7,	//bank islam
				'rcp_date' => date('Y-m-d H:i:s', strtotime($row['receipt date'])),
				'rcp_receive_date' => date('Y-m-d', strtotime($row['receipt date'])),
				'rcp_payee_type' => 645, //applicant
				'rcp_appl_id' => $txnData['at_appl_id'],
				'rcp_trans_id' => $txnData['at_trans_id'],
				'rcp_idStudentRegistration' => 0,
				'rcp_description' => $row['type'].' ('.$row['invoice no'].')',
				'rcp_amount' => $row['amt(usd)']=='' ? $row['amt (rm)'] : $row['amt(usd)'],
				'rcp_amount_default_currency' => $row['amt (rm)'],
				'rcp_adv_payment_amt' => 0.00,
				'rcp_gainloss' => 0,
				'rcp_gainloss_amt' => 0,
				'rcp_cur_id' => $row['amt(usd)']=='' ? 1 : 2,
				'rcp_status'=>'APPROVE',
				'UpdDate' =>date('Y-m-d H:i:s'),
				'migs_id'=> '',
				'MigrateCode' => date('dmy')
			);
		
			$db->insert('receipt', $receipt_data);
			$receipt_id = $db->lastInsertId();

			//$receipt_id = $receiptDb->insert($receipt_data);
			
			//add payment
			$paymentDb = new Studentfinance_Model_DbTable_Payment();
			$payment_data = array(
				'p_rcp_id' => $receipt_id,
				'p_payment_mode_id' => 0,
				'p_migs' => 0,
				'p_cheque_no' => null,
				'p_doc_bank' => null,
				'p_doc_branch' => null,
				'p_terminal_id' => null,
				'p_card_no' => null,
				'p_cur_id' => $row['amt(usd)']=='' ? 1 : 2, //always MYR
				'p_status'=>'APPROVE',
				'p_migs'=> 0 ,
				'p_amount' => $row['amt(usd)']=='' ? $row['amt (rm)'] : $row['amt(usd)'],
				'p_amount_default_currency' => $row['amt (rm)'],
				'created_date' =>date('Y-m-d H:i:s', strtotime($row['receipt date'])),
				'created_by'=>1
			);

			//print_R($payment_data);
			$paymentDb->insert($payment_data);
		
		}
		
		$this->log[] = array(
							
							'Applicant Id'			=>  $row['applicant id'],
							'Name'					=>	$row['applicant name'],
							'Receipt No'			=> $row['receipt no'],
							'Invoice No'			=>  $row['invoice no'],
							'Description'			=>	$row['inv desc'],
							'Amount RM'				=>	$row['amt (rm)'],
							'Amount USD'			=>	$row['amt(usd)']
						);
	}
	
	public function processInvoiceData($row=array())
	{
		$db = getDB();
		$i = 0;
		foreach ( $row as $field => $value )
		{
			$i++;
			//echo $i.'. '.$field.' - '.$value.' <br />';
			
			switch ( $field ) 
			{
				case 'applicant id':
					
				break;

				case 'name':

				break;

				case 'semester';

				break;

				case 'invoice date':
				break;

				case 'invoice No':

				break;

				case 'fee code':
					
				break;

				case 'amount (rm)':
					$row['amount (rm)'] = $this->cleanAmt($row['amount (rm)']);
				break;

				case 'amount (usd)':
					$row['amount (usd)'] = $this->cleanAmt($row['amount (usd)']);
				break;
				
			}//switch

		}//foreach
		
		$row['Program'] = $row[6];

		$program =$this->getProgram($row[5],'ProgramCode');

		if ( empty($program) )
		{
			$this->logItem('Program', $row, $row['applicant id']);
		}
		else
		{

			$app = $this->getTransByProgram($row['applicant id'], $program['IdProgram']);
			if ( empty($app) )
			{
				$this->error_log[] =  array('App ID' => $row['applicant id'], 'ErrorType' => 'Invalid Applicant', 'Value' => $row['applicant id']);
			}
			else
			{
				if ( $app['at_processing_fee'] == 'ISSUED' )
				{
					if ( !in_array($app['at_appl_id'], $this->trans) )
					{
						//delete
						$db->delete('proforma_invoice_detail', new Zend_Db_Expr('proforma_invoice_main_id IN ( SELECT id FROM proforma_invoice_main WHERE appl_id = '.$app['at_appl_id'].' )'));
						$db->delete('proforma_invoice_main', 'appl_id='.$app['at_appl_id']);
						
						$txnUpdData = array('at_processing_fee' => null);
						$n = $db->update('applicant_transaction', $txnUpdData, 'at_trans_id = '.$app['at_trans_id']);
						
						$this->trans[] = $app['at_appl_id'];
					}
					
				}

				$proformaInvoiceMainDb 		= new Studentfinance_Model_DbTable_ProformaInvoiceMain();
				$proformaInvoiceDetailDb 	= new Studentfinance_Model_DbTable_ProformaInvoiceDetail();
				$feeItemDb					= new Studentfinance_Model_DbTable_FeeItem();
				
				$feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
				if($app['at_fs_id']==null || $app['at_fs_id']==0 )
				{
					$feeStructureData = $feeStructureDb->getApplicantFeeStructure($app['ap_prog_id'], $app['ap_prog_scheme'], $app['at_intake'], $app['appl_category'],0);
				}
				else
				{
					$feeStructureData = $feeStructureDb->fetchRow(array('fs_id = ?' => $app['at_fs_id']));
				}

				if ( empty($feeStructureData) )
				{
					$this->error_log[] =  array('App ID' => $row['applicant id'], 'ErrorType' => 'Invalid Fee Structure For Program / Scheme / Intake /Category ', 'Value' => $app['ap_prog_id'].'/'.$app['ap_prog_scheme'].'/'.$app['at_intake'].'/'.$app['appl_category']);
					return false;
				}


				
				if ( $row['amount (usd)'] != '' && $row['amount (rm)'] != '' )
				{
					$currency_id = $this->currencies['USD']['cur_id'];
					$total_invoice_amount = $row['amount (usd)'];
					$total_invoice_amount_default_currency = $row['amount (rm)'];
				}
				else
				{
					
					$currency_id = $this->currencies['MYR']['cur_id'];
					$total_invoice_amount = $row['amount (rm)'];
					$total_invoice_amount_default_currency = $row['amount (rm)'];
				}
					
				
				
				$invoice_desc = $row[8];
				
				$fi = $feeItemDb->fetchRow(array('fi_id=?'=>$row['fee code']))->toArray();
				if ( empty($app) )
				{
					$this->error_log[] =  array('App ID' => $row['applicant id'], 'ErrorType' => 'Invalid Fee Code', 'Value' => $row['fee code']);
				}
				else
				{
					//
					$data = array(
										'bill_number' => 'P'.$row['invoice no'],
										'appl_id' => $app['at_appl_id'],
										'trans_id' => $app['at_trans_id'],
										'bill_amount' => $total_invoice_amount,
										'bill_description' => $invoice_desc,
										'program_id' => $app['ap_prog_id'],
										'creator' => '-1',
										'fs_id' => $feeStructureData['fs_id'],
										'status' => 'A',
										'currency_id' => $currency_id,
										'bill_amount_default_currency' => $total_invoice_amount_default_currency,
										'invoice_type' => 'PROCESSING',
										'fee_category' => $fi['fi_fc_id'],
										'branch_id'=>  $app['branch_id'],
										'date_create' => date('Y-m-d H:i:s', strtotime($row['invoice date'])),
										'MigrateCode' => date('dmy')
								);
					
					//$main_id = $proformaInvoiceMainDb->insert($data);

					$db->insert('proforma_invoice_main', $data);
					$main_id = $db->lastInsertId();

					//detail
					$data_detail = array(
											'proforma_invoice_main_id' => $main_id,
											'fi_id' => $fi['fi_id'],
											'fee_item_description' => $fi['fi_name'],
											'cur_id' => $currency_id,
											'amount' => $total_invoice_amount,
											'amount_default_currency' => $total_invoice_amount_default_currency
										);
						
					$invoice_detail_id = $proformaInvoiceDetailDb->insert($data_detail);

					//update txn
					$txnUpdData = array('at_processing_fee' => 'ISSUED');
					$n = $db->update('applicant_transaction', $txnUpdData, array('at_trans_id = ?'=>$app['at_trans_id']));

					$this->log[] = array(
											'Applicant Id'			=>  $row['applicant id'],
											'Name'					=>	$row['name'],
											'Invoice No'			=>  $row['invoice no'],
											'Fee Code'				=>	$row['fee code'],
											'Description'			=>	$invoice_desc,
											'Amount RM'				=>	$row['amount (rm)'],
											'Amount USD'			=>	$row['amount (usd)'],
											'Date'					=>  date('Y-m-d H:i:s', strtotime($row['invoice date'])),
											'Data'					=>  json_encode($data)
										);

					//generate invoice		
					
					//check payment
					$select = $db->select()
								 ->from(array('s'=>'receipt'))
								 ->where('s.rcp_trans_id = ?',$app['at_trans_id'])
								 ->where('s.MigrateCode IS NOT NULL')
								 ->where('s.rcp_description LIKE ?', '%'.$row['invoice no'].'%');

					$receipt = $db->fetchRow($select);

					if ( !empty($receipt) )
					{
						//get proforma data
						$proformaInvoiceMainDb = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
						$proforma_invoice = $proformaInvoiceMainDb->fetchRow(array('id = ?'=>$main_id));
						
						if($proforma_invoice==null){
							throw new Exception('Unknown proforma invoice');
						}else{
							$proforma_invoice = $proforma_invoice->toArray();

						
								
							//get current currency rate 
							$currencyRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
							$currencyRate = $currencyRateDB->getCurrentExchangeRate($proforma_invoice['currency_id']);
							$proforma_invoice['exchange_rate'] = $currencyRate['cr_id'];
								
							//proforma invoice detail
							$proformaInvoiceDetailDb = new Studentfinance_Model_DbTable_ProformaInvoiceDetail();
							$proforma_invoice_detail = $proformaInvoiceDetailDb->fetchAll(array('proforma_invoice_main_id = ?'=>$proforma_invoice['id']));
								
							if($proforma_invoice_detail){
								$invoice_detail = $proforma_invoice_detail->toArray();
								
								foreach ($invoice_detail as $index=>&$fee_item)
								{
									$currencyRate = $currencyRateDB->getCurrentExchangeRate($fee_item['cur_id']);
									$fee_item['exchange_rate'] = $currencyRate['cr_id'];
								}

								$proforma_invoice['fee_item'] = $invoice_detail;
							}else{
								$proforma_invoice['fee_item'] = null;
							} 
							

							$db = Zend_Db_Table::getDefaultAdapter();
						
								//insert invoice main
								$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
								
								$data_invoice = array(
									'bill_number' => $row['invoice no'],
									'appl_id' => $proforma_invoice['appl_id'],
									'trans_id' => $proforma_invoice['trans_id'],
									'IdStudentRegistration' => $proforma_invoice['IdStudentRegistration'],
									'academic_year' => $proforma_invoice['academic_year'],
									'semester' => $proforma_invoice['semester'],
									'bill_amount' => $proforma_invoice['bill_amount'],
									//'bill_amount_default_currency' => $proforma_invoice['bill_amount_default_currency'],
									'bill_paid' => 0,
									'bill_balance' => $proforma_invoice['bill_amount'],
									'bill_description' => $proforma_invoice['bill_description'],
									'program_id' => $proforma_invoice['program_id'],
									'fs_id' => $proforma_invoice['fs_id'],
									'currency_id' => $proforma_invoice['currency_id'],
									'exchange_rate' => $proforma_invoice['exchange_rate'],
									'proforma_invoice_id' => $proforma_invoice['id'],
									'date_create'=>date('Y-m-d H:i:s', strtotime($row['invoice date'])),
									'MigrateCode' => date('dmy')
									
								);
								
								//admin
								$data_invoice['creator'] = 1;
								
														
								$invoice_id = $invoiceMainDb->insert($data_invoice);
								
								//update proforma invoice id
								$db->update('proforma_invoice_main', array('invoice_id'=>$invoice_id), array('id = ?'=>$proforma_invoice['id']));
								
								//insert invoice detail
								$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
								
								for($a=0; $a<sizeof($proforma_invoice['fee_item']); $a++){
									$fee_item_data = $proforma_invoice['fee_item'][$a];
									
									if ( empty($fee_item_data['exchange_rate']) )
									{
										print_R($fee_item_data);
										exit;
									}

									$invoiceDetailData = array(
										'invoice_main_id' => $invoice_id,
										'fi_id' =>  $fee_item_data['fi_id'],
										'fee_item_description' =>  $fee_item_data['fee_item_description'],
										'cur_id' =>  $fee_item_data['cur_id'],
										'amount' => $fee_item_data['amount'],
										'balance' => $fee_item_data['amount'],
										'exchange_rate' => $fee_item_data['exchange_rate']
										//'amount_default_currency' => $fee_item_data['amount_default_currency']
									);
									
									$invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);
								}
								
							
						}


							
						//add receipt-invoice detail
						$receiptInvoiceDb = new Studentfinance_Model_DbTable_ReceiptInvoice();
						$invdets = $invoiceDetailDb->getDetail($invoice_id);
						foreach ( $invdets as $invoiceData )
						{
							$currencyItem = $invoiceData['cur_id'];
							$currencyPaid = $receipt['rcp_cur_id'];
							
							$amountPaid = $invoiceData['amount'];
							$amountNew = $invoiceData['amount'];
							
							if($currencyPaid == $currencyItem){
								$amountNew = $invoiceData['amount'];
							}else{
								if($currencyPaid == 2){
									$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
									$dataCur = $curRateDB->getCurrentExchangeRate(2);
									$amountNew = round($invoiceData['amount'] * $dataCur['cr_exchange_rate'],2);
								}else if($currencyPaid == 1){
									$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
									$dataCur = $curRateDB->getData($invoiceData['exchange_rate']);
									
									if ( empty($dataCur)  || empty($invoiceData['amount']) )
									{
										print_R($invoiceData);
										exit;
									}

									$amountNew = round($invoiceData['amount'] / $dataCur['cr_exchange_rate'],2);
								}
							}
							
		//					echo $amountNew;
							$data_rcp_inv = array(
								'rcp_inv_rcp_id' => $receipt['rcp_id'],
								'rcp_inv_rcp_no' => $receipt['rcp_no'],
								'rcp_inv_invoice_id' => $invoice_id,
								'rcp_inv_invoice_dtl_id' => $invoiceData['id'],
								'rcp_inv_amount' => $amountPaid,
								'rcp_inv_amount_default_currency' => $amountNew,
								'rcp_inv_cur_id' => $currencyPaid //$fee_item['invoice_fi_currency']
							);
							$receiptInvoiceDb->insert($data_rcp_inv);
						}
												
					}//has receipt
				}
			}

		}
	
	}

	function cleanAmt($what)
	{
		$what = str_replace('-','',$what);
		$what = trim($what);

		return $what;
	}

	function getTransByProgram($refid, $prog_id)
	{
		$db = getDB();
	  	$select = $db->select()
	                 ->from(array('s'=>'applicant_transaction'))
					 ->join(array('sp'=>'applicant_profile'),'sp.appl_id=s.at_appl_id',array('sp.appl_category'))
					 ->join(array('p'=>'applicant_program'),'p.ap_at_trans_id=s.at_trans_id')
	                 ->where('s.at_ref_id = ?', $refid)
					 ->where('p.ap_prog_id = ?',$prog_id);


        $row = $db->fetchRow($select);

		return $row;
	}

	public function applicantAction()
	{
		
		$branchDB = new GeneralSetup_Model_DbTable_Branchofficevenue();
		$appProfileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		$appTransactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
		$objdeftypeDB = new App_Model_Definitiontype();
		$countryDB = new App_Model_General_DbTable_Country();
		$entryReqDB= new App_Model_Application_DbTable_EntryRequirement();
		$appQualificationDB  = new App_Model_Application_DbTable_ApplicantQualification();
		$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
		$appVisaDB = new App_Model_Application_DbTable_ApplicantVisa();
			

		//email
		if ( $this->_request->isPost() && $this->_request->getPost('mail') )
		{
			//intake = 
			$db = getDb();

			$sql =  $db->select()
						 ->from(array('a'=>'tbl_intake'),array('IdIntake'))
						 ->where('a.IntakeDesc LIKE ?','%2015%');

			$result = $db->fetchAll($sql);
			
			$intakeid = array();
			foreach ( $result as $intake )
			{
				$intakeid[] = $intake['IdIntake'];
			}

			$db = getDb();
			$sql = $db->select()
						 ->from(array('a'=>'applicant_profile'))
						 ->joinLeft(array('b'=>'applicant_transaction'),'a.appl_id=b.at_appl_id',array())
						->where('b.at_intake IN ('.implode(',',$intakeid).')')
						->where('a.migrate_email = 0')
						->where('a.MigrateCode != ?','')
						->limit(30);

			$applicants = $db->fetchAll($sql);
			
		
			$mail = new Cms_SendMail();
			$total = 0;
			foreach ( $applicants as $app )
			{
				$total++;
				$key = generateRandomString(16);
				//$keylink = APPLICATION_URL."/online-application/reset/id/".$larrResult['appl_id']."/key/".$key;
				
				/*
				Dear xxxx,

				Kindly be informed that we have enhanced our online application portal in order to provide a 

				better service and experience. In order to complete the application form or check on your 

				application status, and remit the necessary payment, you are required to reset your 

				password. 

				Please click the link below to reset your password

				http://apply.inceif.org/online-application/reset/id/2963/key/evhb3UJ0oqDeSPRN 

				Once you have reset your password, you may login to the application portal at 

				http://apply.inceif.org by using the email registered together with your new password.

				If you need further technical assistance, please contact our helpdesk at 

				helpdesk@inceif.org. 

				If you wish to get in touch with our admission team, please contact us at 

				admission@inceif.org. 

				Thank you.

				Admission and Student Affairs Department

				INCEIF
				*/

				$message = 'Dear '.$app['appl_fname'].' '.$app['appl_lname'].','."<br /><br />";

				$message .= 'Kindly be informed that we have enhanced our online application portal in order to provide a  better service and experience. In order to complete the application form or check on your  application status, and remit the necessary payment, you are required to reset your  password. '."<br /><br />";

				$message .= 'Please click the link below to set a new password'."<br /><br />";

				$message .= 'http://apply.inceif.org/online-application/reset/id/'.$app['appl_id'].'/key/'.$key."<br /><br />";

				$message .= 'Once you have reset your password, you may login to the application portal at http://apply.inceif.org by using the email registered together with your new password.'."<br /><br />";

				$message .= 'If you need further technical assistance, please contact our helpdesk at <a href="mailto:helpdesk@inceif.org">helpdesk@inceif.org</a>.'."<br /><br />";

				$message .= 'If you wish to get in touch with our admission team, please contact us at <a href="mailto:admission@inceif.org">admission@inceif.org</a>.'."<br /><br />";

				$message .= 'Thank you.'."<br /><br />";

				$message .= '<strong>Admission and Student Affairs Department</strong><br /><br />';

				//$message .= 'INCEIF';

				$db->update('applicant_profile', array('userKey'=>$key,'migrate_email'=>1), $db->quoteInto('appl_id=?',$app['appl_id']));

				//send email
				$mail->fnSendMail($app['appl_email'], $this->view->translate('INCEIF e-University - New Login Information'), $message);
				//$mail->fnSendMail('munzir@gmail.com', $this->view->translate('INCEIF e-University - New Login Information'), $message);
				
				$log[] = array('App Name' => $app['appl_fname'].' '.$app['appl_lname'], 'Email' => $app['appl_email']);
				
				echo 'Email sent to '.$app['appl_email'].' <br />';
				//sleep(1);
			}
			
			if ( !empty($log) )
			{
				$this->migrate->addLog('Applicant Email', $log, $this->userId, '' );
			}
			
			echo '<br />Total Sent: '.$total.'<br/ >';
			echo 'done';
			exit;
		}
		
		//stuff
		if ($this->_request->isPost () && $this->_request->getPost ( 'reset' ))
		{
			$db = getDB();
			$select = $db->select()
	                 ->from(array('s'=>'applicant_profile'))
	                 ->where('MigrateCode IS NOT NULL');
			$rows = $db->fetchAll($select);
			$deleted=0;
			foreach ( $rows as $row )
			{
				$select2 = $db->select()
	                 ->from(array('s'=>'applicant_transaction'))
	                 ->where('at_appl_id = ?',$row['appl_id']);
				$trans = $db->fetchAll($select2);

				$db->delete('applicant_transaction','at_appl_id = '.$row['appl_id']);
				$db->delete('applicant_profile','appl_id = '.$row['appl_id']);
				$deleted++;
			}

			echo 'Total Deleted: '.$deleted.'<br />';
			echo 'done';
			exit;
		}

		//applicant checklist
		$checklistDB = new Application_Model_DbTable_ChecklistVerification();
		if ($this->_request->isPost () && $this->_request->getPost ( 'applicantchecklist' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$csv->auto($this->migrateFolder.'/applicant/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = $this->error_log = array();

			$db = getDB();
			foreach ( $csv->data as $row )
			{
				$ref_id = $row['Ref. Id'];
				$app = $this->getApplicant($ref_id);
				/*
					at_appl_id
					at_trans_id
				*/
				if ( !empty($app) )
				{
					$appProgram = $appProfileDB->getProfileProgram($app['at_trans_id']);
					
					$db->delete('applicant_document_status', array('ads_txn_id' => $app['at_trans_id']));

					if ( !empty($appProgram) )
					{
						$docChecklist = $checklistDB->fnGetDocCheckList($appProgram['ap_prog_scheme'], $appProgram['appl_category'], $app['at_trans_id']);
						
						foreach ( $docChecklist as $checklist )
						{

							$status = $app['at_status'] == 593 ? 4 : 1;
							$status = $status == 4 & $checklist['dcl_finance'] == 1 ? 1 :$status;
							
							$list = Application_Model_DbTable_ChecklistVerification::universalQueryAll($checklist['table_name'], '*', $checklist['table_id_column'].' = '.$app['at_trans_id'], $checklist['table_join'], $checklist['join_column1'], $checklist['join_column2']);
							

							$data = array(
									'ads_txn_id'=>$app['at_trans_id'],
									'ads_dcl_id'=>$checklist['dcl_Id'],
									'ads_ad_id'=> 0,
									//'ads_appl_id'=>'',
									//'ads_confirm'=>$check,
									
									'ads_section_id'=>$checklist['dcl_sectionid'],
									'ads_table_name'=>$checklist['table_name'],
									'ads_table_id'=>empty($list[0][$checklist['table_primary_id']]) ? 0 : $list[0][$checklist['table_primary_id']],
									'ads_status'=>$status,
									'ads_comment'=>'migrated checklist',
									'ads_createBy'=>$this->userId,
									'ads_createDate'=>date('Y-m-d H:i:s')
								);

								
							$checklistDB->insert($data);
						}
					}
					else
					{
						$this->error_log[] = array('App ID' => $row['Ref. Id'], 'ErrorType' => 'Invalid Applicant Program', 'Value' => $row['Program']);
					}
				
				}
			}

			if ( !empty($this->error_log) )
			{
				$this->migrate->addLog('Applicant Checklist - Error', $this->error_log, $this->userId, $formData['file'] );
			}

			if ( !empty($log) )
			{
				$this->migrate->addLog('Applicant Checklist', $log, $this->userId, $formData['file'] );
			}
			
			echo 'done';
			exit;
		}

		//applicant details
		if ($this->_request->isPost () && $this->_request->getPost ( 'applicantdet' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$csv->auto($this->migrateFolder.'/applicant/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = array();

			$db = getDB();
			foreach ( $csv->data as $row )
			{
				$ref_id = $row['Ref. Id'];
				$app = $this->getApplicant($ref_id);
				/*
					at_appl_id
					at_trans_id
				*/

				if ( !empty($app) )
				{
					//programs
					$program =$this->getProgram($row["Program"]);
				

					if ( !empty($program) )
					{
						//proceed
						$info3 = array();
						
						$mode_study = $mode_program = $program_type = 0;
						
						switch ( $program['ProgramCode'] )
						{
							case 'PhD':
								$row['Program Mode']	= $row['Program Mode'];
								$row['Mode of program'] = 'Face to Face';
								$row['Program Type']	= $this->filter($row['Program Option'],'modeofprogram');
							break;

							case 'MSc': case 'MIF':
								
								$bytype = explode('by', str_replace('Coursework and Dissertation','Coursework & Dissertation', $row['Program Option']));
								$progtype = 'By '.trim($bytype[1]);

								$row['Program Mode']	= $row['Program Mode'];
								$row['Mode of program'] = 'Face to Face';
								$row['Program Type']	= $progtype;
							break;

							case 'MIFP':
								$row['Program Mode']	= $row['Program Mode'];
								$row['Mode of program'] = str_replace('-',' ',$row['Program Option']);
								$row['Program Type']	= '';
							break;
						}
						
						//Program Mode = Mode of Study
						//Mode of Program = 
						$mode_study = $this->getIdByDef(str_replace('-',' ',$row['Program Mode']),$objdeftypeDB->fnGetDefinations('Mode of Study'));
						$mode_program = $this->getIdByDef($row['Mode of program'],$objdeftypeDB->fnGetDefinations('Mode of Program'));
						$program_type = $this->getIdByDef($row['Program Type'],$objdeftypeDB->fnGetDefinations('Program Type',0));
						
						if ( $program['ProgramCode'] == 'MIFP' )
						{
							$program_type = 627;
						}
					
						//get program scheme
						$appProgramDB  = new App_Model_Application_DbTable_ApplicantProgram();	

						$scheme = $appProgramDB->getProgramScheme($program['IdProgram'],$mode_study,$mode_program,$program_type);
						if ( !empty( $scheme) ) 
						{
							$info3['mode_study'] = $mode_study;
							$info3['program_mode'] = $mode_program;
							$info3['program_type'] = $program_type;
							$info3['ap_prog_scheme'] = $scheme['IdProgramScheme'];
							$info3["ap_at_trans_id"]=$app['at_trans_id'];
							$info3["ap_prog_code"]=$program["ProgramCode"];
							$info3["ap_prog_id"]=$program['IdProgram'];
									
							$appProgramDB->addData($info3);
						}
						else
						{
							
							$this->error_log[] = array('App ID' => $row['Ref. Id'], 'ErrorType' => 'Invalid Program Scheme', 'Value' => $row['Program'].' '.$row['Program Mode'].' '.$row['Program Option']);
						}
					}
					else
					{
						$this->error_log[] = array('App ID' => $row['Ref. Id'], 'ErrorType' => 'Invalid Program', 'Value' => $row['Program']);
					}
					
					//fix values
					foreach ( $row as $field => $value )
					{
						switch ($field)
						{
							case 'Highest Level':
							
							$row['Highest Level'] = $this->filter($row['Highest Level'], 'highestlevel');

							$highest_level = $this->getIdByDef($row['Highest Level'],$entryReqDB->getListGeneralReq(),array('k'=>'QualificationLevel','v'=>'IdQualification'));
							if ( empty($highest_level) ) $this->logItem($field, $row, $ref_id);
							break;

							case 'Name of University':
								$name_of_university = $row['Name of University'];
								if ( empty($name_of_university) )  $this->logItem($field, $row, $ref_id);
							break;

							case 'Class Degree':
								$row['Class Degree'] = $this->filter($row['Class Degree'],'classdegree');
								$class_degree = $this->getIdByDef($row['Class Degree'],$objdeftypeDB->fnGetDefinations('Class Degree'));
								if ( empty($class_degree) ) $this->logItem($field, $row, $ref_id);
							break;

							case 'Proficiency in English':
								$row ['Proficiency in English'] = $this->filter($row['Proficiency in English'], 'proficiency');
								$qualificationDB = new App_Model_General_DbTable_Qualificationmaster();
								if ( !isset($this->cache['english']))
								{
									$quallist = $this->cache['english'] = $qualificationDB->getDataTest();
								}
								else
								{
									$quallist = $this->cache['english'];
								}
								$proficiency_english = $this->getIdByDef($row['Proficiency in English'],$quallist,array('k'=>'QualificationLevel','v'=>'IdQualification'));

								if ( empty($proficiency_english) )  $this->logItem($field, $row, $ref_id);
							break;

							case 'Employment Status':
								$employement_status = $this->getIdByDef($row['Employment Status'],$objdeftypeDB->fnGetDefinations('Employment Status'));
								if ( empty($employement_status) ) $this->logItem($field, $row, $ref_id);
							break;

							case 'Visa Status':
								$visa_status = $this->getIdByDef($row['Visa Status'],$objdeftypeDB->fnGetDefinations('Visa Status'));
								if ( empty($visa_status) ) $this->logItem($field, $row, $ref_id);
							break;

							case 'Employment Status':
								$row['Employment Status'] = $this->filter($row['Employment Status'], 'empstatus');
								$employement_status = $this->getIdByDef($row['Employment Status'],$objdeftypeDB->fnGetDefinations('Employment Status'));
								if ( empty($employement_status) ) $this->logItem($field, $row, $ref_id);
							break;

							case 'Position':
								$row['Position'] = $this->filter($row['Position'], 'position');
								$position_id = $this->getIdByDef($row['Position'],$objdeftypeDB->fnGetDefinations('Position Level'));
								if ( empty($position_id) ) if ( empty($employement_status) ) $this->logItem($field, $row, $ref_id);
							break;

							case 'Industry':
								$row['industry'] = $this->filter($row['Industry'], 'industry');
								$industry = $this->getIdByDef($row['Industry'],$objdeftypeDB->fnGetDefinations('Industry Working Experience'));
								if ( empty($industry) ) $this->logItem($field, $row, $ref_id);
							break;

							case 'Health condition':
								$health_name = ucfirst(strtolower($row['Health condition']));

								$health_condition = $this->getIdByDef($health_name,$objdeftypeDB->fnGetDefinations('Health Condition'));

								if ( empty($health_condition) ) $this->logItem($field, $row, $ref_id);
							break;

							case 'Financial Support':
								$financial_support = $this->getIdByDef($row['Financial Support'],$objdeftypeDB->fnGetDefinations('Funding Method'));
								if ( empty($financial_support) ) $this->logItem($field, $row, $ref_id);
							break;
						}
					}

					//qualification
					
					$data6 = array(	    		   
						'ae_appl_id' => $app['at_appl_id'],
						'ae_transaction_id' => $app['at_trans_id'],
						'ae_qualification' => $highest_level,
						'ae_degree_awarded' => $row['Diploma/Degree Awarded'],
						'ae_majoring' => isset($row['Major/Specialisation']) ? $row['Major/Specialisation'] : '',
						'ae_class_degree' => $class_degree,
						'ae_result' => $row['Result/CGPA'],
						'ae_year_graduate' => $row['Year Graduated'],
						'ae_institution_country' => '',
						'ae_institution' => 999,
						'ae_medium_instruction' => $row['Medium of Instruction'],	
						'others' => $name_of_university,	    		  	    		
						'createDate' => date("Y-m-d H:i:s")
					);

					$ae_id = $appQualificationDB->addData($data6);

					//health condition
					$appHealthDB = new App_Model_Application_DbTable_ApplicantHealthCondition();
					$data9 = array(    				   
						'ah_appl_id' => $app['at_appl_id'],
						'ah_trans_id' => $app['at_trans_id'],
						'ah_status' => $health_condition,
						'upd_date' => date("Y-m-d H:i:s")
					);
								
					$appHealthDB->addData($data9);

					//financial support
					$appFinancial = new App_Model_Application_DbTable_ApplicantFinancial();

					$data10 = array(
						'af_appl_id' => $app['at_appl_id'],
						'af_trans_id' => $app['at_trans_id'],
						'af_method' => $financial_support,
						'af_sponsor_name' => $row['Name of Sponsor'],
						'af_type_scholarship' => '',
						'af_scholarship_secured' => $row['Scholarship Plan'],
						'af_scholarship_apply' => '',
						'upd_date' => date("Y-m-d H:i:s")
					);

					$af_id = $appFinancial->addData($data10);

					//english
					$data4 = array(    			 
						'ep_transaction_id' => $app['at_trans_id'],
						'ep_test' => $proficiency_english,
						'ep_test_detail' => isset($row['english_test_detail']) ? $row['english_test_detail'] : '',
						'ep_date_taken' => isset($row['English Language Proficiency  - Date Taken']) ? date('Y-m-d',strtotime($row['English Language Proficiency  - Date Taken'])) : null,
						'ep_score' => $row['Result for English proficiency'] == '' ? 0 : $row['Result for English proficiency'],
						'ep_updatedt' => date("Y-m-d H:i:s"),
						'ep_updateby'=>$app['at_appl_id']
					);
					
					$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
					$ep_id=$appEngProfDB->addData($data4);

					//employment info
					$data8 = array(	    	
						'ae_appl_id' => $app['at_appl_id'],
						'ae_trans_id' => $app['at_trans_id'],	 
						'ae_status' => $employement_status,
						'ae_comp_name' => $row['Company Name'],
						'ae_comp_address' => $row['Company Address'],
						'ae_comp_phone' => $row['Company Telephone Number'],
						'ae_comp_fax' => $row['Company Fax Number'],
						'ae_designation' => $row['Designation'],
						'ae_position' => $position_id,
						'ae_from' => '',
						'ae_to' => '',	    		  
						'emply_year_service' => '',
						'ae_industry' => $industry,	 
						'ae_job_desc' => '',	    		   	    		
						'upd_date' => date("Y-m-d H:i:s")
					);
									
					$appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();
					$ae_id = $appEmploymentDB->addData($data8);
				
					//visa
					if ( $row['Malaysian Visa'] == 1 )
					{
						$data11 = array(
							'av_appl_id' => $app['at_appl_id'],
							'av_trans_id' => $app['at_trans_id'],
							'av_malaysian_visa' => $row['Malaysian Visa'] == 'Yes' ? 1 : 0,
							'av_status' => $visa_status,
							'av_expiry' => date('Y-m-d',strtotime($row['Visa Expiry Date'])),
							'upd_date' => date("Y-m-d H:i:s")
						);
						
						$appVisaDB->insert($data11);
					}
				}

				$log[] = array('App ID' => $row['Ref. Id']);
			}

			if ( !empty($this->error_log) )
			{
				$this->migrate->addLog('Applicant Details - Error', $this->error_log, $this->userId, $formData['file'] );
			}

			if ( !empty($log) )
			{
				$this->migrate->addLog('Applicant Details', $log, $this->userId, $formData['file'] );
			}
			
			echo 'done';
			exit;
		}

		//intake
		if ($this->_request->isPost () && $this->_request->getPost ( 'intake' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$csv->auto($this->migrateFolder.'/applicant/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = $this->error_log = $this->cache = array();

			$db = getDB();
			foreach ( $csv->data as $row )
			{
				$app = $this->getApplicant($row['Ref. Id']);

				if ( !empty($app) )
				{
					$ref_id = $row['Ref. Id'];

					$getintake = explode(' ', str_replace(' Semester','', $row['Intake Session']));
					$intakename = $getintake[1].' '.$getintake[0];
	
					$row['Intake Session'] = $intakename;
					$intakelist = $this->getIntake($row['Intake Session'],1);

					$intake_id = $intakelist['IdIntake'];
					if ( empty($intake_id) ) $this->logItem($field, $row, $ref_id);
				
					//echo $row['Intake Session'].' ('.$app['at_intake'].' | '.$intake_id.')<br />';
					//echo $row['Intake Session'].'<br />';

					$data = array();
					$data['at_intake'] = $intake_id;

					echo $app['at_intake'].'-'.$intake_id.'<br />';

					$db->update("applicant_transaction", $data,'at_trans_id='.$app['at_trans_id']);

					/*$row['Proficiency in English'] = $this->filter($row['Proficiency in English'], 'proficiency');
					$qualificationDB = new App_Model_General_DbTable_Qualificationmaster();
					if ( !isset($this->cache['english']))
					{
						$quallist = $this->cache['english'] = $qualificationDB->getDataTest();
					}
					else
					{
						$quallist = $this->cache['english'];
					}

					$proficiency_english = $this->getIdByDef($row['Proficiency in English'],$quallist,array('k'=>'QualificationLevel','v'=>'IdQualification'));

					if ( empty($proficiency_english) )  $this->logItem('Proficiency in English', $row, $ref_id);

					$highest_level = $this->getIdByDef($row['Highest Level'],$entryReqDB->getListGeneralReq(),array('k'=>'QualificationLevel','v'=>'IdQualification'));
					if ( empty($highest_level) ) $this->logItem('Highest Level', $row, $ref_id);


					$name_of_university = $row['Name of University'];
					if ( empty($name_of_university) )  $this->logItem('Name of University', $row, $ref_id);

					$row['Class Degree'] = $this->filter($row['Class Degree'],'classdegree');
					$class_degree = $this->getIdByDef($row['Class Degree'],$objdeftypeDB->fnGetDefinations('Class Degree'));
					if ( empty($class_degree) ) $this->logItem('Class Degree', $row, $ref_id);


					//english
					$data4 = array(    			 
						'ep_transaction_id' => $app['at_trans_id'],
						'ep_test' => $proficiency_english,
						'ep_test_detail' => isset($row['english_test_detail']) ? $row['english_test_detail'] : '',
						'ep_date_taken' => isset($row['English Language Proficiency  - Date Taken']) ? date('Y-m-d',strtotime($row['English Language Proficiency  - Date Taken'])) : null,
						'ep_score' => $row['Result for English proficiency'] == '' ? 0 : $row['Result for English proficiency'],
						'ep_updatedt' => date("Y-m-d H:i:s"),
						'ep_updateby'=>$app['at_appl_id']
					);

					
					$db->delete('applicant_english_proficiency', 'ep_transaction_id='.$app['at_trans_id']);
					
					$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
					$ep_id=$appEngProfDB->addData($data4);

					//specialization
					
					$data6 = array(	    		   
						'ae_appl_id' => $app['at_appl_id'],
						'ae_transaction_id' => $app['at_trans_id'],
						'ae_qualification' => $highest_level,
						'ae_degree_awarded' => $row['Diploma/Degree Awarded'],
						'ae_majoring' => isset($row['Major/Specialisation']) ? $row['Major/Specialisation'] : '',
						'ae_class_degree' => $class_degree,
						'ae_result' => $row['Result/CGPA'],
						'ae_year_graduate' => $row['Year Graduated'],
						'ae_institution_country' => '',
						'ae_institution' => 999,
						'ae_medium_instruction' => $row['Medium of Instruction'],	
						'others' => $name_of_university,	    		  	    		
						'createDate' => date("Y-m-d H:i:s")
					);
					$db->delete('applicant_qualification', 'ae_transaction_id='.$app['at_trans_id']);
					$ae_id = $appQualificationDB->addData($data6);

					*/
				}
					else
				{
					//die ('Invalid Applicant '.$row['Ref. Id']);
				}
			}
			

			if ( !empty($this->error_log) )
			{
				$this->migrate->addLog('Applicant - Error', $this->error_log, $this->userId, $formData['file'] );
			}

			if ( !empty($log) )
			{
				$this->migrate->addLog('Applicant', $log, $this->userId, $formData['file'] );
			}
			
			echo 'done';
			exit;
		}

		//applicant
		if ($this->_request->isPost () && $this->_request->getPost ( 'applicant' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$csv->auto($this->migrateFolder.'/applicant/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = $this->error_log = $this->cache = array();
			
			$count = 0;
			$db = getDB();
			foreach ( $csv->data as $row )
			{
				if ( $this->checkExists($row['Ref. Id']) == false )
				{
					$count++;

					$this->processApplicantData($row);

					$log[] = $row;
				}
			}
			

			if ( !empty($this->error_log) )
			{
				$this->migrate->addLog('Applicant - Error', $this->error_log, $this->userId, $formData['file'] );
			}

			if ( !empty($log) )
			{
				$this->migrate->addLog('Applicant', $log, $this->userId, $formData['file'] );
			}
			
			echo 'done';
			exit;
		}
	}

	function checkStudent($studentid)
	{
		$row = $this->getStudent($studentid);

		return !empty($row) ? true : false;
	}

	function getStudent($refid)
	{
		$db = getDB();
	  	$select = $db->select()
	                 ->from(array('s'=>'tbl_studentregistration'))
	                 ->where('ref_id = ?', $refid);
        $row = $db->fetchRow($select);

		return $row;
	}

	function getPhoto($refid,$student_path)
	{
		$db = getDB();
	  	$select = $db->select()
	                 ->from(array('s'=>'migrate_applicant_mapping'))
	                 ->where('metric_no = ?', '0'.$refid);
        $row = $db->fetchRow($select);


		if ( empty($row) )
		{
			return false;
		}

		$file = '/data/cdn/migrate/';
		$types = array('jpg','JPG','png','gif');

			
		$found=0;
		foreach ($types as $type)
		{
			if ( file_exists($file.$row['user_id'].'.'.$type) )
			{
				$found=1;
				$photo_name = $row['user_id'].'.'.$type;
				$photo_url = $file.$photo_name;
			}
		}

		if ($found)
		{
			$newloc = DOCUMENT_PATH.$student_path.'/'.$photo_name;
//echo $photo_url.'<br />'.$newloc; exit;

			if ( !copy($photo_url, $newloc) )
			{
				throw new Exception('Invalid Photo URL '.$photo_url.' - '.$newloc);
			}

			return array('fullpath' => $student_path.'/'.$photo_name, 'filename' => $photo_name);
		}

		return $row;
	}
	
	function checkExists($refid)
	{
		$row = $this->getApplicant($refid);

		return !empty($row) ? true : false;
	}
	
	function getApplicant($refid)
	{
		$db = getDB();
	  	$select = $db->select()
	                 ->from(array('s'=>'applicant_transaction'))
	                 ->where('at_ref_id = ?', $refid);
        $row = $db->fetchRow($select);

		return $row;
	}

	public function getProgram($name='',$by='ProgramName')
	{
		//$name = str_replace('MSc in Islamic Finance','MASTERS IN ISLAMIC FINANCE', $name);
		$name = str_replace('MSc in Islamic Finance','MASTER OF SCIENCE (MSc) IN ISLAMIC FINANCE', $name);
		$name = str_replace('PIF','PhD',$name);

		$db = getDb();

		$sql =  $db->select()
	                 ->from(array('a'=>'tbl_program'),array('a.*'))
	                 ->where('LOWER(a.'.$by.') = ?', strtolower($name));
	
		$result = $db->fetchRow($sql);

		if ( empty($result) )
		{
			return '';
		}
		else
		{
			return $result;
		}
	}

	protected function processApplicantData($row=array())
	{
		$branchDB = new GeneralSetup_Model_DbTable_Branchofficevenue();
		$appProfileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		$appTransactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
		$objdeftypeDB = new App_Model_Definitiontype();
		$countryDB = new App_Model_General_DbTable_Country();
		$entryReqDB= new App_Model_Application_DbTable_EntryRequirement();
		$appQualificationDB  = new App_Model_Application_DbTable_ApplicantQualification();
		$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
		$appVisaDB = new App_Model_Application_DbTable_ApplicantVisa();
		
		$ref_id = $row['Ref. Id'];

		$db = getDB();
		$i = 0;
		foreach ( $row as $field => $value )
		{
			$i++;
			//echo $i.'. '.$field.' - '.$value.' - ';
			
			
			switch ( $field ) 
			{
				case 'Id type':
					$row['Id type'] = str_replace('Military Number', 'Military ID/Police ID', $row['Id type']);
					$row['Id type'] = str_replace('Malaysian IC', 'MyKad', $row['Id type']);
				
					if ( !isset($this->cache['idtype']) )
					{
						$idlist = $this->cache['idtype'] = $objdeftypeDB->fnGetDefinations('ID Type');
					}
					else
					{
						$idlist = $this->cache['idtype'];
					}

					$id_type = $this->getIdByDef($row['Id type'],$idlist);
					if ( empty($id_type ) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Intake Session':

					$getintake = explode(' ', str_replace(' Semester','', $row['Intake Session']));
					$intakename = $getintake[1].' '.$getintake[0];
	
					$row['Intake Session'] = $intakename;
					$intakelist = $this->getIntake($row['Intake Session'],1);

					$intake_id = $intakelist['IdIntake'];
					if ( empty($intake_id) ) $this->logItem($field, $row, $ref_id);

				break;

				case 'Nationality':
				
					//values i dont like
					$row['Nationality'] = str_replace('SOUTH KOREAN','KOREAN', $row['Nationality']);
					$row['Nationality'] = str_replace('FILIPINA', 'FILIPINO', $row['Nationality']);
					
					if ( !isset($this->cache['countries']) )
					{
						$countries = $this->cache['countries'] = $countryDB->getData();
					}
					else
					{
						$countries = $this->cache['countries'];
					}

					$nationality = $this->getIdByDef($row['Nationality'],$countries,array('k'=>'Nationality','v'=>'idCountry'));
					if ( empty($nationality) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Religion':

					if ( !isset($this->cache['religion']) )
					{
						$religionlist = $this->cache['religion'] = $objdeftypeDB->fnGetDefinations('Religion');
					}
					else
					{
						$religionlist = $this->cache['religion'];
					}

					$religion = $this->getIdByDef($row['Religion'],$religionlist);
					if ( empty($religion) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Permanent State':

					$row['Permanent State'] = $this->filter($row['Permanent State'], 'states');
					
					$perm_state = $this->getState($row['Permanent State']);
					if ( empty($perm_state) ) $this->logItem($field, $row, $ref_id);
				break;
				
				case 'Permanent Country':
					$row['Permanent Country'] = $this->filter($row['Permanent Country'], 'country');
					$perm_country = $this->getIdByDef($row['Permanent Country'],$countries,array('k'=>'CountryName','v'=>'idCountry'));
					if ( empty($perm_country) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Correspondence State':
					$row['Correspondence State'] = $this->filter($row['Correspondence State'], 'states');
					$corr_state = $this->getState($row['Correspondence State']);
					if ( empty($corr_state) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Correspondence Country':
					$row['Correspondence Country'] = $this->filter($row['Correspondence Country'], 'country');
					$corr_country = $this->getIdByDef($row['Correspondence Country'],$countries,array('k'=>'CountryName','v'=>'idCountry'));
					if ( empty($corr_country) ) $this->logItem($field, $row, $ref_id);
				break;
				
				case 'Race':
					
					$row['Race'] = $this->filter($row['Race'], 'race');
						
					if ( !isset($this->cache['race']) )
					{
						$racelist = $this->cache['race'] = $objdeftypeDB->fnGetDefinations('Race');
					}
					else
					{
						$racelist = $this->cache['race'];
					}


					$race = $this->getIdByDef($row['Race'],$racelist);
					if ( empty($race) ) $this->logItem($field, $row, $ref_id);
				break;
				
				case 'Marital Status':
					$row['Marital Status'] = $this->filter($row['Marital Status'], 'marital');

					if ( !isset($this->cache['marital']) )
					{
						$maritallist = $this->cache['marital'] = $objdeftypeDB->fnGetDefinations('Marital Status');
					}
					else
					{
						$maritallist = $this->cache['marital'];
					}

					$marital_status = $this->getIdByDef($row['Marital Status'],$maritallist);

					if ( empty($marital_status) ) $this->logItem($field, $row, $ref_id);
				break;

				case 'Status for new system':

					
					if ( !isset($this->cache['status']) )
					{
						$statuslist = $this->cache['status'] = $objdeftypeDB->fnGetDefinations('Status');
					}
					else
					{
						$statuslist = $this->cache['status'];
					}

					$status = $this->getIdByDef($row['Status for new system'], $statuslist);
				break;

				/*
				Name
				Id type
				Id Number
				Dob
				Intake Session
				Nationality
				Religion
				Email Id
				Permanent Address
				Permanent City
				Permanent State
				Permanent Country
				Permanent Postcode
				Permanent Contact No.
				Permanent Fax
				Permanent Moblie
				Correspondence Address
				Correspondence City
				Correspondence State
				Correspondence Country
				Correspondence Postcode
				Correspondence Contact No.
				Correspondence Fax
				Correspondence Moblie
				Employment Status
				Company Name
				Company Address
				Company Telephone Number
				Company Fax Number
				Designation
				Position
				Years of service
				Industry
				Health condition
				Financial Support
				Name of Sponsor
				Sponsor Code
				Scholarship Plan
				Previous Student
				Previous Student ID
				Program
				Program Mode
				Program Option
				Race
				Gender
				Age
				Marital Status
				Ref. Id
				Application Status
				Status for new system
				Applied Date
				Highest Level
				Name of University
				Diploma/Degree Awarded
				Major/Specialisation
				Class Degree
				Result/CGPA
				Year Graduated
				Proficiency in English
				Result for English proficiency
				Medium of Instruction
				Malaysian Visa
				Visa Status
				Visa Expiry Date
				University
				Course
				Joining Date
				Leaving Date
				Reason of Leaving
				Duration
				Remark
				*/
			}//switch
		}//foreach
		
		$names = explode(' ',strtoupper($row['Name']));
		if ( $names[0] == 'MD.' )
		{
			$row['fname'] = $names[0].' '.$names[1];
			unset($names[0], $names[1]);

			$row['lname'] = trim( implode(' ', $names) );
		}
		else
		{
			$row['fname'] = $names[0];
			unset($names[0]);
			$row['lname'] = trim(implode(' ', $names));
		}

		//$row['fname'] = $names[0] == 'MD.' ? $names[0].' '.$names[1] : $names[0];
		//$row['lname'] = $names[0] == 'MD.' ? $names[2] : $names[1];
		
		//applicant_profile
		$appl_info['appl_fname'] = $row['fname'];
		$appl_info['appl_lname'] = $row['lname'];
		//$appl_info["branch_id"] = $branchID;            
		$appl_info["appl_email"]= $row['Email Id'];
		$appl_info["appl_password"]= '';				
		$appl_info["appl_role"]=0;
		$appl_info["create_date"]=date("Y-m-d H:i:s");
		$appl_info['appl_dob']=date('Y-m-d',strtotime($row['Dob']));
		//$appl_info['appl_salutation']= $salutation;
		$appl_info['appl_idnumber'] = $row['Id Number'];
		$appl_info['appl_idnumber_type'] = $id_type;
		$appl_info['appl_nationality'] = $nationality;
		$appl_info['appl_religion'] = $religion;
		$appl_info['appl_address1'] = $row['Permanent Address'];
		$appl_info['appl_city'] = 99;
		$appl_info['appl_city_others'] = $row['Permanent City'];

		if ( empty($perm_state) && $row['Permanent State'] != '' )
		{
			$appl_info['appl_state'] = 99;
			$appl_info['appl_state_others'] = $row['Permanent State'];
		}
		else
		{
			$appl_info['appl_state'] = $perm_state;
		}

		$appl_info['appl_country'] = $perm_country;
		$appl_info['appl_postcode'] = $row['Permanent Postcode'];
		
		$appl_info['appl_caddress1'] = $row['Correspondence Address'];
		
		if ( $row['Correspondence City'] != '' )
		{
			$appl_info['appl_ccity'] = 99;
			$appl_info['appl_ccity_others'] = $row['Correspondence City'];
		}

		if ( empty($corr_state) && $row['Correspondence State'] != '' )
		{
			$appl_info['appl_cstate'] = 99;
			$appl_info['appl_cstate_others'] = $row['Correspondence State'];
		}
		else
		{
			$appl_info['appl_cstate'] = $corr_state;
		}
		
		$appl_info['appl_cpostcode'] = $row['Correspondence Postcode'];
		$appl_info['appl_ccountry'] = $corr_country;

		$appl_info['appl_phone_home'] = $row['Permanent Contact No.'];
		$appl_info['appl_fax'] = $row['Permanent Fax'];
		$appl_info['appl_phone_mobile'] = $row['Permanent Moblie'];
		$appl_info['appl_cphone_home'] = $row['Correspondence Contact No.'];
		$appl_info['appl_cphone_mobile'] = $row['Correspondence Moblie'];
		$appl_info['appl_cfax'] = $row['Correspondence Fax'];


		$appl_info['prev_student'] = $row['Previous Student'] == 'yes' ? 1 : 0;
		$appl_info['prev_studentID'] = $row['Previous Student ID'];
		$appl_info['appl_race'] = $race;
		$appl_info['appl_gender'] = $row['Gender'] == 'Female' ? 2 : 1;
		$appl_info['appl_salutation'] = $row['Gender'] == 'Female' ? 86:85;
		$appl_info['appl_marital_status'] = $marital_status;
		//
		
		if ( $id_type == 487 )
		{
			//$appl_info['appl_type_nationality'] = 547;
			$appl_info['appl_category'] = 579;
		}
		else
		{
			//$appl_info['appl_type_nationality'] = 549;
			$appl_info['appl_category']=580;
		}

		$appl_info['MigrateCode'] = date('dmy');
		
		$applicant_id = $appProfileDB->addData($appl_info);
		
		//generate applicant ID
		//$gen_applicantId =  $this->generateApplicantID();

		//applicant_transaction
		$info2["at_appl_id"]=$applicant_id;
		$info2["at_pes_id"]=$ref_id;
		$info2["at_create_by"]=$applicant_id;
		$info2["at_status"]=$status; //offered
		$info2["at_create_date"]=date("Y-m-d H:i:s",strtotime($row['Applied Date']));
		$info2["entry_type"]=0;//manual			
		$info2["at_default_qlevel"]=null;	
		$info2["at_intake"] = $intake_id;
		$info2['at_remarks'] = isset($row['Remark']) ? $row['Remark'] : '';
		$info2['at_ref_id'] = $row['Ref. Id'];
	
		//trans ID		    					
		$at_trans_id = $appTransactionDB->addData($info2);

		echo $intake_id.'<br />';


		//make directory for repository purpose				   
		$applicant_path = DOCUMENT_PATH.'/applicant/'.date('Ym');
		
		//create directory to locate file			
		if (!is_dir($applicant_path)) {
			mkdir_p($applicant_path, 0775);
		}    			
				
		$txn_path = $applicant_path."/".$at_trans_id;
		
		//create directory to locate file			
		if (!is_dir($txn_path)) {
			mkdir_p($txn_path, 0775);
		}							
		
		//update repository info	
		$rep['at_repository']='/applicant/'.date('Ym').'/'.$at_trans_id;								
		$appTransactionDB->updateData($rep,$at_trans_id);
		
		$appTransactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
		$dataHistory = array(
                            'ash_trans_id'=>$at_trans_id,
                            'ash_status'=>$status,
                            'ash_oldStatus'=>0,
                            'ash_changeType'=>1,
                            'ash_updUser'=> $applicant_id,
                            'ash_updDate'=> date("Y-m-d H:i:s",strtotime($row['Applied Date']))
                        );
		
		$appTransactionDB->storeStatusUpdateHistory($dataHistory);
	
	}

	function generateApplicantID(){
		
		//generate applicant ID
		$applicantid_format = array();
		
		$seqnoDb = new App_Model_Application_DbTable_ApplicantSeqno();
		$sequence = $seqnoDb->getData(date('Y'));
		
		$tblconfigDB = new App_Model_General_DbTable_TblConfig();
		$config = $tblconfigDB->getConfig(1);
		
		//format
		 $format_config = explode('|',$config['ApplicantIdFormat']);
                 //var_dump($format_config);
		for($i=0; $i<count($format_config); $i++){
			$format = $format_config[$i];
			if($format=='px'){ //prefix
				$result = $config['ApplicantPrefix'];
			}elseif($format=='yyyy'){
				$result = date('Y');
			}elseif($format=='yy'){
				$result = date('y');
			}elseif($format=='seqno'){				
				$result = sprintf("%03d", $sequence['seq_no']);
			}
                        //var_dump($result);
			array_push($applicantid_format,$result);
		}
	
		//var_dump(implode("-",$applicantID));
		$applicantID = implode("",$applicantid_format);
		
		//update sequence		
		$seqnoDb->updateData(array('seq_no'=>$sequence['seq_no']+1),$sequence['seq_id']);
                
                //var_dump($applicantID); exit;
		
		return	$applicantID;
	}

	protected function filter($value, $type)
	{
		//highest level
		if ($type == 'highestlevel') 
		{
			$value = $value == 'Master' ? 'Masters or Equivalent' : $value;
			$value = $value == 'Bachelor' ? 'Bachelor or Equivalent' : $value;
			$value = $value == 'Professional Qualification' ? 'Masters or Equivalent' : $value;
		}

		//employment status
		if ($type == 'empstatus') 
		{
			
				$replace = array(
									'Self-employed'				=> 'Self employed'
							);

			foreach ( $replace as $search => $return )
			{
				$value = str_replace($search, $return, $value);
			}
		}
		//country
		if ( $type == 'country' )
		{
			$replace = array(
								'Korea, South'				=> 'REPUBLIC OF KOREA',
								'UNITED STATES OF AMERICA'	=> 'UNITED STATES',
								'ENGLAND'					=> 'UNITED KINGDOM'
							);

			foreach ( $replace as $search => $return )
			{
				$value = str_replace($search, $return, $value);
			}
		}

		//marital
		if ( $type == 'marital' )
		{
			$value = str_replace('Unmarried', 'Single', $value);
		}


		//race
		if ( $type == 'race' )
		{
			$value = strtolower($value);
			$value = str_replace('melayu', 'Malay', $value);
			$value = str_replace('cina', 'Chinese', $value);
		}
		
		//states
		if ( $type == 'states' )
		{
			$replace = array(
								'WP K LUMPUR'				=> 'WP Kuala Lumpur',
								'N SEMBILAN'				=>	'Negeri Sembilan',
								'P PINANG'					=>	'Pulau Pinang',
								'WP SELANGOR'				=> 'Selangor'
							);

			foreach ( $replace as $search => $return )
			{
				$value = str_replace($search, $return, $value);
			}
		}
		
		//modeofprogram
		if ( $type == 'modeofprogram' )
		{
			$replace = array(
								'PhD by Coursework and Dissertation' => 'By Coursework & Dissertation',
								'PhD by Research'					 => 'By Research',
								'MSc by Coursework'					 => 'By Coursework',
								'MIF by Coursework'					 => 'By Coursework',
								'MIF by Coursework and Dissertation' => 'By Coursework & Dissertation'
							);

			foreach ( $replace as $search => $return )
			{
				$value = str_replace($search, $return, $value);
			}
		}

		//classdegree
		if ( $type == 'classdegree' )
		{

			$replace = array( 	 		'2nd Class Upper/Division 1'	=> '2nd Class Upper / Division 1',
										'2nd Class Upper/Division 2'	=> '2nd Class Upper / Division 2',
										'Pass/Distinction'				=> 'Pass / Distinction'
							);

			foreach ( $replace as $search => $return )
			{
				$value = str_replace($search, $return, $value);
			}
		}

		//proficiency
		if ( $type == 'proficiency' )
		{
			$replace = array(
								'TOEFL - Test of English as a Foreign Language'			=> 'Test of English as a Foreign Language (TOEFL)',
								'IELTS - International English Language Testing System'	=> 'International English Language Testing System (IELTS)',
								'MUET - Malaysian University English Test'				=> 'Malaysian University English Test (MUET)'
							);

			foreach ( $replace as $search => $return )
			{
				$value = str_replace($search, $return, $value);
			}
			
			$value = $value == 'IETLS' ? 'International English Language Testing System (IELTS)' : $value;
			$value = $value == 'IELTS' ? 'International English Language Testing System (IELTS)' : $value;
			$value = $value == 'MUET' ? 'Malaysian University English Test (MUET)' : $value;
			$value = $value == 'TOEFL' ? 'Test of English as a Foreign Language (TOEFL)' : $value;
			$value = $value == 'Other' ? 'Others':$value;
		}
		
		
		
		//position
		if ( $type == 'position' )
		{

		}

		//industry
		if ( $type == 'industry' )
		{
			
			$replace = array(
							
								'Management, Services & Trustee'				=> 'Management. Service & Trustee',
								'Islamic Banking'								=> 'Accounting/Banking/Finance',
								'Marketing/Sales'								=> 'Marketing /Sales',
								'Education'										=> 'Education/Training'

						);

			foreach ( $replace as $search => $return )
			{
				$value = str_replace($search, $return, $value);
			}
		}

		//student_status
		if ( $type == 'student_status' )
		{

			$replace = array(
							
								'Active (Special Case)'								=> 'Active',
								'Suspension: Disciplinary'							=> 'Active',
								'Suspension: Outstanding payment'					=> 'Active',
								'0301 - Deferred: Maternity'						=> 'Active',
								'0302 - Deferred: Personal Matters'					=> 'Active',
								'0303 - Deferred: Under Probation (Disciplinary)'	=> 'Active',
								'0304 - Deferred: Medical Condition	Active'			=> 'Active',
								'0305 - Deferred: Financial Matters	Active'			=> 'Active',
								'0401 - Withdraw: Withdrawn from the Program'		=> 'Quit',
								'0402 - Withdraw: Deceased'							=> 'Quit',
								'0501 - Dismissed: Failed Exams'					=> 'Terminated',
								'0502 - Dismissed: Disciplinary'					=> 'Terminated',
								'0600 - Graduated'									=> 'Graduated',
								'0700 - Inactive'									=> 'Active'			
						);

			foreach ( $replace as $search => $return )
			{
				$value = preg_replace('/'.trim($search).'/', $return, $value);
			}
		}
		


		return $value;
	}

	protected function logItem($field, $row, $ref_id)
	{
		$this->error_log[] =  array('App ID' => $ref_id, 'ErrorType' => 'Invalid '.$field, 'Value' => $row[$field]);
	}

	public function getIntake($name='', $returnall=0)
	{
		$db = getDb();

		$sql =  $db->select()
	                 ->from(array('a'=>'tbl_intake'),array('IdIntake'))
	                 ->where('a.IntakeDesc = ?', $name);

		$result = $db->fetchRow($sql);

		if ( empty($result) )
		{
			$getname = explode(' ',$name);
			$intake_name = $getname[0];
			$intake_year = $getname[1];
			$nameId = str_replace(array('January','June','September'), array('JAN','JUNE','SEPT'),$intake_name);
			$nameMalay = str_replace(array('January','June','September'),array('Januari','Jun','September'),$intake_name);
			$data = array(
							'IntakeId'					=> $intake_year.' '.$nameId,
							'IntakeDesc'				=> $name,
							'IntakeDefaultLanguage'		=> $nameMalay.' '.$intake_year,
							'sem_seq'					=> substr($nameId,0,3),
							'apply_online'				=> 0,
							'sem_year'					=> $intake_year,
							'UpdDate'					=> new Zend_Db_Expr('NOW()'),
							'UpdUser'					=> 1,
							'MigratedCode'				=> date('dmy')
						);
			
			$db->insert('tbl_intake', $data);
			$intake_id = $db->lastInsertId();
			
			if ( $returnall )
			{
				return $data;
			}
			else
			{
				return $intake_id;
			}
		}
		else
		{
			if ( $returnall )
			{
				return $result;
			}
			else
			{
				return $result['IdIntake'];
			}
		}
	}
	
	function getState($state)
	{
		$db = getDB();
	  	$select = $db->select()
	                 ->from(array('s'=>'tbl_state'))
	                 ->where('LOWER(s.StateName) = ?', strtolower($state));
        $row = $db->fetchRow($select);
		
		if ( !empty($row) )
		{
			return $row['idState'];
		}
		else
		{
			return '';
		}
	}

	public function getIdByDef($id, $data, $map = array('k'=>'DefinitionDesc','v'=>'idDefinition') )
	{
		if ( !empty($data) )
		{
			
			foreach ( $data as $row )
			{
				if ( strtolower($row[$map['k']]) == strtolower($id) )
				{
					$val = $row[$map['v']];
				}
			}
		}

		if ( empty($val) )
		{
			//return 0;
			return '';
		}
		else
		{
			return $val;
		}
	}

	/* -----------
	*	General
	* ------------ */
	
	public function semseq($seq)
	{
		$data = array(
						'JUNE'		=> 'JUN',
						'SEPTEMBER'	=> 'SEP',
						'JANUARY'	=> 'JAN'
				);

		return $data[$seq];
	}

	public function getScheme($val)
	{
		$schemeDbObject = new GeneralSetup_Model_DbTable_Schemesetup();
		$schemeList = $schemeDbObject->fngetSchemes();
		
		$data = array();
		foreach ( $schemeList as $scheme )
		{
			$data[$scheme['value']] = $scheme['key'];
		}

		if ( preg_match('/(PS)/', $val) )
		{
			return $data['Professional Studies'];
		}
		else if ( preg_match('/(GS)/', $val) )
		{
			return $data['Graduate Studies'];
		}
	}

	public function generalAction()
	{
		//examiner
		if ($this->_request->isPost () && $this->_request->getPost ( 'examiner' ))
		{
			$db = getDB();
			
			$select2 = $db->select()
     				->from(array('a'=>'thesis_examiner_user'),array('staff_id'))
					->where('a.examiner_type = 0');


			$select = $db->select()
     				->from(array('user'=>'tbl_user'))
     				->join(array('staff'=>'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff')
     				->where('staff.StaffAcademic = 0')
					->where('user.iduser NOT IN ( ? )', $select2)
     				->order('staff.FullName ASC');

			$results = $db->fetchAll($select);

			$log = array();

			foreach ( $results as $row )
			{
				$check = $db->fetchRow( $db->select()
     				->from(array('a'=>'thesis_examiner_user'),array('staff_id'))
					->where('a.staff_id = ?',$row['iduser']));
				
				if ( empty($check) )
				{
					$data = array(
									'examiner_type'				=> 0,
									'staff_id'					=> $row['iduser'],
									'semester_id'				=> 0,
									'fullname'					=> $row['FullName'],
									'email'						=> $row['FullName'],
									'password'					=> '',
									'contact_no'				=> '',
									'created_by'				=> 1,
									'created_date'				=> new Zend_Db_Expr('NOW()'),
									'start_date'				=> null,
									'end_date'					=> null
								);


					$db->insert('thesis_examiner_user', $data);

					$log[] = array(
											'staff_id'					=>  $row['IdStaff'],
											'username'					=>	$row['loginName']
									);
				}
			}

			if ( !empty($log) )
			{
				$this->migrate->addLog('Thesis Examiner', $log, $this->userId ,'' );
			}

			
			echo 'done';
			$this->delayredirect();
			exit;
		}

		//lecturer
		if ($this->_request->isPost () && $this->_request->getPost ( 'lecturer' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$checkdupe = 1;
	
			$csv->auto($this->migrateFolder.'/general/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);
			$db = getDB();

			foreach ( $csv->data as $row )
			{
				$data =	array(
								'IdStaff' => $row['IdStaff'],
								'loginName' => $row['loginName'],
								'passwd' => md5('!inceif123'),
								'userArabicName' => $row['userArabicName'],
								'lName' => $row['lName'],
								'mName' => $row['mName'],
								'fName' => $row['fName'],
								'NameField1' => $row['NameField1'],
								'NameField2' => $row['NameField2'],
								'NameField3' => $row['NameField3'],
								'NameField4' => $row['NameField4'],
								'NameField5' => $row['NameField5'],
								'DOB' => $row['DOB'],
								'gender' => $row['gender'],
								'addr1' => $row['addr1'],
								'addr2' => $row['addr2'],
								'city' => $row['city'],
								'state' => $row['state'],
								'country' => $row['country'], 
								'zipCode' => $row['zipCode'],
								'homePhone' => $row['homePhone'],
								'workPhone' => $row['workPhone'],
								'cellPhone' => $row['cellPhone'],
								'fax' => $row['fax'],
								'email' => $row['email'],
								'notes' => $row['notes'],
								'IdRole' => 400,
								'UpdUser' => 4,
								'UpdDate' => new Zend_Db_Expr('NOW()'),
								'UserStatus' => 1,
								'LockStatus' => 0
						);

				$db->insert('tbl_user', $data);

				$log[] = array(
										'loginName'					=>  $row['loginName'],
										'email'						=>	$row['email']
								);
				
			}

			if ( !empty($log) )
			{
				$this->migrate->addLog('Lecturer', $log, $this->userId, $formData['file'] );
			}

			
			echo 'done';
			$this->delayredirect();
			exit;
		}

		//Semester Master
		if ($this->_request->isPost () && $this->_request->getPost ( 'semestermaster' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$checkdupe = 1;
	
			$csv->auto($this->migrateFolder.'/general/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);
			


			$this->Staff = $this->Faculty = $this->Department = $log = $department_log= array();

			foreach ( $csv->data as $row )
			{
				$semcode = str_replace( array(' (PS)', ' (GS)'), '', $row['Academic Session Code']);
				$scheme = $this->getScheme($row['Academic Session Code']);

				$data		= array(
									 'SemesterMainName'				=> $row['semester name'],
									 'SemesterMainDefaultLanguage'	=> $row['semester name (malay)'],
									 'SemesterMainCode'				=> $semcode,
									 'sem_seq'						=> $this->semseq($row['sem sequence']),
									 'AcademicYear'					=> $row['Academic Year'],
									 'SemesterType'					=> $row['Session Type'] == 'Short' ? 171 : 172,
									 'SemesterMainStartDate'		=> date('Y-m-d', strtotime($row['Start date'])),
									 'SemesterMainEndDate'			=> date('Y-m-d', strtotime($row['End Date'])),
									 'IdScheme'						=> $scheme,
									 'ismigrate'					=> 1
									);
				$db = getDB();
				$db->insert('tbl_semestermaster', $data);

				$log[] = array(
										'Semester'					=>  $row['semester name'],
										'sem_seq'					=>	$this->semseq($row['sem sequence']),
										'Type'						=>	$row['Session Type'],
										'semestercode'				=>	$semcode
								);
				
			}
			
			
					
			if ( !empty($log) )
			{
				$this->migrate->addLog('Semester Master', $log, $this->userId, $formData['file'] );
			}

			
			echo 'done';
			$this->delayredirect();
			exit;
		}

		//feecategory
		if ($this->_request->isPost () && $this->_request->getPost ( 'mapping' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
	
			$csv->auto($this->migrateFolder.'/general/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = array();

			$db = getDB();
			foreach ( $csv->data as $row )
			{
					
					$data = array(
									'metric_no'					=> $row['Metric No.'], 
									'ref_id'					=> $row['Ref Id'], 
									'applicant_id'				=> $row['Applicant Id'], 
									'user_id'					=> $row['Userid'],
									'program_id'				=> 0,
									'program_name'				=> $row['Program']
								);
					
					$db->insert('migrate_applicant_mapping', $data);

					$log[] = array(
										'Metric No'				=>  $row['Metric No.'],
										'Ref Id'				=>	$row['Ref Id'],
										'Applicant Id'			=>  $row['Applicant Id'],
										'User Id'				=>	$row['Userid'],
										'Program'				=>  $row['Program']
									);
			
			}

			if ( !empty($log) )
			{
				$this->migrate->addLog('Mapping Applicant', $log, $this->userId, $formData['file'] );
			}

			
			echo 'done';
			$this->delayredirect();
			exit;
		}

		//feeitemamount
		if ($this->_request->isPost () && $this->_request->getPost ( 'feeitemamount' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
	
			$csv->auto($this->migrateFolder.'/general/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = array();
			
			

			$db = getDB();

			$select = $db->select()
	                ->from(array('a'=>'fee_item') ) 
	                ->where("a.fi_code IN ('EMGS - VS','PB')");			                     
				
			$feeItems = $db->fetchAll($select);
			$feeItemIdByName = array();
			foreach ( $feeItems as $feeitem)
			{
				$feeItemIdByName[strtoupper($feeitem['fi_name'])]=  $feeitem['fi_id'];
			}


			//countries
			$select = $db->select()
	                ->from(array('a'=>'tbl_countries') );			                     
				
			$countries = $db->fetchAll($select);
			
			$countryIdByName = array();
			foreach ( $countries as $country )
			{
				$countryIdByName[$country['CountryName']] = $country['idCountry'];
			}

			
			$amountdata = array();
			foreach ( $csv->data as $row )
			{
				if ( isset($countryIdByName[$row['COUNTRY']] ) )
				{
					$amountdata[ $countryIdByName[$row['COUNTRY']] ][ $feeItemIdByName['EMGS - STUDENT VISA'] ] = $row['STUDENT VISA'];
					$amountdata[ $countryIdByName[$row['COUNTRY']] ][ $feeItemIdByName['PERSONAL BOND'] ] = $row['PERSONAL BOND'];
				}
			}
		
			//start adding amount
			foreach ( $countries as $country )
			{
				foreach ($feeItems as $feeitem)
				{
					if ( isset($amountdata[$country['idCountry']][$feeitem['fi_id']]) )
					{
						$data = array(
										'fic_fi_id'		=>	$feeitem['fi_id'], 
										'fic_idCountry'	=>	$country['idCountry'],
										'fic_cur_id'	=>	1,
										'fic_amount'	=>	$amountdata[$country['idCountry']][$feeitem['fi_id']]
									);



						$db->insert('fee_item_country', $data);

						$log[] = array(
											'Fee Item'					=>  $feeitem['fi_name'],
											'Country'					=>	$country['CountryName'],
											'Amount'					=>	$amountdata[$country['idCountry']][$feeitem['fi_id']]
									);
					}
				}
			}

			
			

			if ( !empty($log) )
			{
				$this->migrate->addLog('Fee Item Amount', $log, $this->userId, $formData['file'] );
			}

			
			echo 'done';
			$this->delayredirect();
			exit;
		}


		//feeitem
		if ($this->_request->isPost () && $this->_request->getPost ( 'feeitem' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
	
			$csv->auto($this->migrateFolder.'/general/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = array();

			$feeCategoryDb = new Studentfinance_Model_DbTable_FeeCategory();
			$feecatList = $feeCategoryDb->fetchAll()->toArray();
			$feecatByCode = $calcTypes = $freqModes = $accCodes = array();
			foreach ( $feecatList as $fee )
			{
				$feecatByCode[$fee['fc_code']] = $fee['fc_id'];
			}
			
			$calctypeList = $this->lobjdeftype->fnGetDefinationMs('Fee Item Calculation Type');
			foreach ( $calctypeList as $calc )
			{
				$calcTypes[$calc['value']] = $calc['key'];
			}

		
			$freqmodeList = $this->lobjdeftype->fnGetDefinationMs('Fee Item Frequency Mode');
			foreach ( $freqmodeList as $freq )
			{
				$freqModes[$freq['value']] = $freq['key'];
			}

			//acccode
			$accDB = new Studentfinance_Model_DbTable_AccountCode();
			$accList = $accDB->getAccountCode();
			foreach ( $accList as $acc )
			{
				$accCodes[$acc['ac_code']] = $acc['ac_id'];
			}

			$db = getDB();
			foreach ( $csv->data as $row )
			{
				//Array ( [Fee Name] => CIFP - Application Fee [Code] => CIFP - AF [Amount Calculation] => Fix Amount [Frequency Mode] => Application [Account Code] => 5100/600 [Refundable] => N [Non-invoice] => Y )
				

				$data = array(
								'fi_name'						=> $row['Fee Name'],
								'fi_code'						=> $row['Fee Code'], 
								'fi_amount_calculation_type'	=> $calcTypes[$row['Calculation Type']],
								'fi_frequency_mode'				=> ifexists($freqModes[$row['Frequency Mode']],0),
								'fi_fc_id'						=> ifexists($feecatByCode[$row['Fee Category']],0), 
								'fi_ac_id'						=> ifexists($accCodes[$row['Account Code']],0),		
								'fi_refundable'					=> $row['Refundable'] == 'Y' ? 1:0,	 
								'fi_non_invoice'				=> $row['Non-invoice'] == 'Y' ? 1:0, 
								'fi_active'						=> 1
							);

				$db->insert('fee_item', $data);

				$log[] = array(
										'Fee Name'				=>  $row['Fee Name'],
										'Fee Code'				=>	$row['Fee Code'],
										'Fee Category'			=> $row['Fee Category'],
										'Calculation Type'		=>	$row['Calculation Type'],
										'Frequency Mode'		=>	$row['Frequency Mode'],
										'Account Code'			=>	$row['Account Code'],
										'Refundable'			=>  $row['Refundable'],
										'Non-invoice'			=>  $row['Non-invoice']
								);
		
			}

			if ( !empty($log) )
			{
				$this->migrate->addLog('Fee Item', $log, $this->userId, $formData['file'] );
			}

			
			echo 'done';
			$this->delayredirect();
			exit;
		}

		//feecategory
		if ($this->_request->isPost () && $this->_request->getPost ( 'feecategory' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
	
			$csv->auto($this->migrateFolder.'/general/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = array();
	
			$db = getDB();
			foreach ( $csv->data as $row )
			{
					
					$data = array(
									'fc_code'					=> $row['Fee Category Code'], 
									'fc_desc'					=> $row['Description'], 
									'fc_desc_second_language'	=> $row['Description (Second Language)'], 
									'fc_seq'					=> $row['Sequence']
								);
					
					$db->insert('tbl_fee_category', $data);

					$log[] = array(
											'Fee Code'				=>  $row['Fee Category Code'],
											'Description'			=>	$row['Description'],
											'Description (Malay)'	=>  $row['Description (Second Language)'],
											'Sequence'				=>	$row['Sequence']
									);
			
			}

			if ( !empty($log) )
			{
				$this->migrate->addLog('Fee Category', $log, $this->userId, $formData['file'] );
			}

			
			echo 'done';
			$this->delayredirect();
			exit;
		}
		
		//Account Code
		if ($this->_request->isPost () && $this->_request->getPost ( 'accountcode' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
	
			$csv->auto($this->migrateFolder.'/general/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);

			$log = $error_log = array();
				
			$accounttypes = $this->lobjdeftype->fnGetDefinationMs('Account Type');
			foreach ( $accounttypes as $type )
			{
				$accTypes[$type['value']] = $type['key'];
			}

			$db = getDB();
			foreach ( $csv->data as $row )
			{
				if ( !empty( $accTypes[$row['Account Type']] ) )
				{
					$data = array(
									'ac_code'			=> $row['Account Code'],
									'ac_desc'			=> $row['Description'],
									'ac_acc_type'		=> $accTypes[$row['Account Type']],
									'ac_status'			=> 1,
									'ac_last_edit_by'	=> 1,
									'ac_last_edit_date' => new Zend_Db_Expr('NOW()')
								);
					
					$db->insert('tbl_account_code', $data);

					$log[] = array(
											'Account Code'				=>  $row['Account Code'],
											'Description'				=>	$row['Description'],
											'Account Type'				=>	$row['Account Type'],
											'Account Type ID'			=>  $accTypes[$row['Account Type']]
									);
				}
				else
				{
					$error_log[] = array(
											'Account Type'				=>	$row['Account Type']
										);
				}
			}

			if ( !empty($log) )
			{
				$this->migrate->addLog('Account Code', $log, $this->userId, $formData['file'] );
			}

			if ( !empty($error_log) )
			{
				$this->migrate->addLog('Account Code - Error', $error_log, $this->userId );
			}

			
			echo 'done';
			$this->delayredirect();
			exit;
		}

		//Course Master
		if ($this->_request->isPost () && $this->_request->getPost ( 'coursemaster' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$checkdupe = 1;
	
			$csv->auto($this->migrateFolder.'/general/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);
			


			$this->Staff = $this->Faculty = $this->Department = $log = $department_log= array();

			foreach ( $csv->data as $row )
			{
					
				$flag = true;
				
				$checkflag = $this->migrate->checkSubject($row['Module Code']);
		
				if ( $checkflag == 0 )
				{
					$row['Module Name'] = str_replace($row['Module Code'].' - ','', $row['Module Name']);

					$data = array(
									'IdFaculty'						=> 19,
									'IdDepartment'					=> 0,
									'SubjectName'					=> $row['Module Name'],
									'ShortName'						=> '',
									'subjectMainDefaultLanguage'	=> '',
									'courseDescription'				=> '',
									'BahasaIndonesia'				=> '',
									'ArabicName'					=> '',
									'SubCode'						=> $row['Module Code'],
									'Active'						=> 1,
									'CreditHours'					=> $row['Credit Hours'],
									'AmtPerHour'					=> '0.00',
									'CourseType'					=> 1,
									'MinCreditHours'				=> 0,
									'IdDefaultSyllabus'				=> null,
									'ClassTimeTable'				=> 0,
									'ExamTimeTable'					=> 0,
									'ReligiousSubject'				=> 0,
									'IdReligion'					=> 0,
									'Group'							=> 0,
									'SubCodeFormat'					=> null,
									'audit'							=> 0,
									'UpdDate' 						=> new Zend_Db_Expr('NOW()'),
									'UpdUser'						=> 1,
									
								);
					
					$subject_id = $this->migrate->insertCoursemaster($data);

					$coordinator = array('IdSubject' => $subject_id,
										 'IdStaff' => 3213,
										 'Active' => 1,
										 'UpdDate' => new Zend_Db_Expr('NOW()'),
										 'UpdUser' => 1
										);
					$db = getDB();
					$db->insert('tbl_subjectcoordinatorlist', $coordinator);
	
					$log[] = array(
											'SubjectName'				=>  $row['Module Name'],
											'SubjectNameMalay'			=>	'',
											'SubCode'					=>	$row['Module Code'],
											'CreditHours'				=>	$row['Credit Hours']
									);
				}
			}
			
			
					
			if ( !empty($log) )
			{
				$this->migrate->addLog('Course Master', $log, $this->userId, $formData['file'] );
			}

			
			echo 'done';
			$this->delayredirect();
			exit;
		}

		//Course Master #2
		if ($this->_request->isPost () && $this->_request->getPost ( 'coursemaster2' ))
		{
			$formData = $this->_request->getPost ();
			$csv = new parseCSV();
			
			$checkdupe = 1;
	
			$csv->auto($this->migrateFolder.'/general/'.$formData['file']);
	
			$totalfields = count($csv->data[0]);
			


			$this->Staff = $this->Faculty = $this->Department = $log = $department_log= array();

			foreach ( $csv->data as $row )
			{
					
				$flag = true;
				
				$checkflag = $this->migrate->checkSubject($row['Course Code']);
		
				if ( $checkflag == 0 )
				{
					$row['Course Name'] = str_replace($row['Course Code'].' - ','', $row['Course Name']);

					$data = array(
									'IdFaculty'						=> 20,
									'IdDepartment'					=> 0,
									'SubjectName'					=> $row['Course Name'],
									'ShortName'						=> '',
									'subjectMainDefaultLanguage'	=> '',
									'courseDescription'				=> '',
									'BahasaIndonesia'				=> '',
									'ArabicName'					=> '',
									'SubCode'						=> $row['Course Code'],
									'Active'						=> 1,
									'CreditHours'					=> $row['Credit Hours'],
									'AmtPerHour'					=> '0.00',
									'CourseType'					=> 1,
									'MinCreditHours'				=> 0,
									'IdDefaultSyllabus'				=> null,
									'ClassTimeTable'				=> 0,
									'ExamTimeTable'					=> 0,
									'ReligiousSubject'				=> 0,
									'IdReligion'					=> 0,
									'Group'							=> 0,
									'SubCodeFormat'					=> null,
									'audit'							=> 0,
									'UpdDate' 						=> new Zend_Db_Expr('NOW()'),
									'UpdUser'						=> 1,
									
								);
					
					$subject_id = $this->migrate->insertCoursemaster($data);

					$coordinator = array('IdSubject' => $subject_id,
										 'IdStaff' => 3229,
										 'Active' => 1,
										 'UpdDate' => new Zend_Db_Expr('NOW()'),
										 'UpdUser' => 1
										);
					$db = getDB();
					$db->insert('tbl_subjectcoordinatorlist', $coordinator);
										
	
					$log[] = array(
											'SubjectName'				=>  $row['Course Name'],
											'SubjectNameMalay'			=>	'',
											'SubCode'					=>	$row['Course Code'],
											'CreditHours'				=>	$row['Credit Hours']
									);
				}
			}
			
			
					
			if ( !empty($log) )
			{
				$this->migrate->addLog('Course Master', $log, $this->userId, $formData['file'] );
			}

			
			echo 'done';
			$this->delayredirect();
			exit;
		}
	
	}

	public function reportAction()
	{
		$this->view->title = 'Migration Report';

		// see report
		$logs = $this->migrate->getLogs();
		
		$this->view->logs = $logs;
	}
	
	public function viewreportAction()
	{
		$this->view->title = 'Migration Report';

		$id = $this->_getParam('id');

		$logdata = $this->migrate->getLogsById($id);
		
		if ( empty($logdata) )
		{
			throw new Exception($this->view->translate('Invalid Report Id'));
		}
		
		$this->view->logdata = $logdata;
	}
	
	
	/*
	 *  OTHER FUNCTIONS BELOW
	 */
	
	private function csvtotable($data, $table='')
	{
		if ( !empty($data) )
		{
			$fields = array();
			foreach ( $data[0] as $field => $val )
			{
				$fields[] = $field;
			}
			
			if ( $this->migrate->createTable($fields, $table) !=  count($data) )
			{
				foreach ( $data as $row)
				{
					$this->migrate->insertData($table, $row);
				}
			}
		}
	}
	
	private function get_files($folder,$type='')
	{
				
		if ($handle = opendir($folder))
		{
		    while (false !== ($entry = readdir($handle))) 
		    {
		        if ($entry != "." && $entry != "..") 
		        {
		           $files[] = $entry;
		           $this->filesizes[$type][$entry] = $this->filesize(filesize($folder.$entry));
		        }
		    }
		    
		    closedir($handle);
		}
		
		return $files;
	}

	
	public function country($val='')
	{
		if ( !isset($this->countries[$val]) )
		{
			$this->countries[$val] = $this->migrate->getCountry($val);
		}
		
		return $this->countries[$val] == '' ? 0 : $this->countries[$val];
	}
	
	public function gender($val='')
	{
		$genders = array('Male' => 1,
						 'Female' => 0);
		
		return $genders[$val];
	}
	
	public function checknull($val='')
	{
		return strtolower($val) == 'null' ? '' : $val;
	}
	

	//better filesize
	function filesize($size)
	{
		$unit=array('b','kb','mb','gb','tb','pb');
		return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
	}

	
	
	public function nationalityAction()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        // -- data
        
        $nationality = array (
							    'AF' => 'Afghan',
							    'AL' => 'Albanian',
							    'DZ' => 'Algerian',
							    'AS' => 'American Samoan',
							    'AD' => 'Andorran',
							    'AO' => 'Angolan',
							    'AI' => 'Anguillan',
							    'AG' => 'Antiguan/Barbudan',
							    'AR' => 'Argentinean',
							    'AM' => 'Armenian',
							    'AW' => 'Aruban',
							    'AU' => 'Australian',
							    'AT' => 'Austrian',
							    'AZ' => 'Azerbaijani',
							    'BS' => 'Bahamian',
							    'BH' => 'Bahraini',
							    'BD' => 'Bangladeshi',
							    'BB' => 'Barbadian',
							    'BY' => 'Belarusian',
							    'BE' => 'Belgian',
							    'BZ' => 'Belizean',
							    'BJ' => 'Beninese',
							    'BM' => 'Bermudian',
							    'BT' => 'Bhutanese',
							    'BA' => 'Bosnian/Herzegovinian',
							    'BW' => 'Motswana',
							    'BR' => 'Brazilian',
							    'BG' => 'Bulgarian',
							    'BI' => 'Burundian',
							    'KH' => 'Cambodian',
							    'CM' => 'Cameroonian',
							    'CA' => 'Canadian',
							    'CV' => 'Cape Verdean',
							    'KY' => 'Caymanian',
							    'CF' => 'Central African',
							    'TD' => 'Chadian',
							    'CL' => 'Chilean',
							    'CX' => 'Christmas Island',
							    'CC' => 'Cocos Island',
							    'CO' => 'Colombian',
							    'KM' => 'Comorian',
							    'CK' => 'Cook Island',
							    'CR' => 'Costa Rican',
							    'CI' => 'Ivorian',
							    'HR' => 'Croatian',
							    'CU' => 'Cuban',
							    'CY' => 'Cypriot',
							    'CZ' => 'Czech',
							    'DK' => 'Danish',
							    'DJ' => 'Djiboutian',
							    'DM' => 'Dominicand',
							    'DO' => 'Dominicane',
							    'EC' => 'Ecuadorian',
							    'EG' => 'Egyptian',
							    'SV' => 'Salvadoran',
							    'GQ' => 'Equatorial Guinean',
							    'ER' => 'Eritrean',
							    'EE' => 'Estonian',
							    'ET' => 'Ethiopian',
							    'FO' => 'Faroese',
							    'FJ' => 'Fijian',
							    'FI' => 'Finnish',
							    'FR' => 'French',
							    'GF' => 'French Guianese',
							    'PF' => 'French Polynesian',
							    'GA' => 'Gabonese',
							    'GM' => 'Gambian',
							    'GE' => 'Georgian',
							    'DE' => 'German',
							    'GH' => 'Ghanaian',
							    'GI' => 'Gibraltar',
							    'GR' => 'Greek',
							    'GL' => 'Greenlandic',
							    'GD' => 'Grenadian',
							    'GP' => 'Guadeloupe',
							    'GU' => 'Guamanian',
							    'GT' => 'Guatemalan',
							    'GN' => 'Guinean',
							    'GW' => 'Guinean',
							    'GY' => 'Guyanese',
							    'HT' => 'Haitian',
							    'HN' => 'Honduran',
							    'HK' => 'Hongkongese',
							    'HU' => 'Hungarian',
							    'IS' => 'Icelandic',
							    'IN' => 'Indian',
							    'ID' => 'Indonesian',
							    'IQ' => 'Iraqi',
							    'IE' => 'Irish',
							    'IM' => 'Manx',
							    'IL' => 'Israeli',
							    'IT' => 'Italian',
							    'JM' => 'Jamaican',
							    'JP' => 'Japanese',
							    'JO' => 'Jordanian',
							    'KZ' => 'Kazakh',
							    'KE' => 'Kenyan',
							    'KI' => 'I-Kiribati',
							    'KW' => 'Kuwaiti',
							    'KG' => 'Kyrgyzstani',
							    'LV' => 'Latvian',
							    'LB' => 'Lebanese',
							    'LS' => 'Basotho',
							    'LR' => 'Liberian',
							    'LY' => 'Libyan',
							    'LI' => 'Liechtenstein',
							    'LT' => 'Lithuanian',
							    'LU' => 'Luxembourgish',
							    'MG' => 'Malagasy',
							    'MW' => 'Malawian',
							    'MY' => 'Malaysian',
							    'MV' => 'Maldivian',
							    'ML' => 'Malian',
							    'MT' => 'Maltese',
							    'MH' => 'Marshallese',
							    'MQ' => 'Martiniquais',
							    'MR' => 'Mauritanian',
							    'MU' => 'Mauritian',
							    'YT' => 'Mahoran',
							    'MX' => 'Mexican',
							    'FM' => 'Micronesian',
							    'MC' => 'Monégasque, Monacan',
							    'MN' => 'Mongolian',
							    'ME' => 'Montenegrin',
							    'MS' => 'Montserratian',
							    'MA' => 'Moroccan',
							    'MZ' => 'Mozambican',
							    'NA' => 'Namibian',
							    'NR' => 'Nauruan',
							    'NP' => 'Nepali',
							    'NL' => 'Dutch',
							    'NC' => 'New Caledonian',
							    'NZ' => 'New Zealand',
							    'NI' => 'Nicaraguan',
							    'NE' => 'Nigerien',
							    'NG' => 'Nigerian',
							    'NU' => 'Niuean',
							    'NO' => 'Norwegian',
							    'OM' => 'Omani',
							    'PK' => 'Pakistani',
							    'PW' => 'Palauan',
							    'PA' => 'Panamanian',
							    'PG' => 'Papua New Guinean',
							    'PY' => 'Paraguayan',
							    'PE' => 'Peruvian',
							    'PH' => 'Filipino',
							    'PL' => 'Polish',
							    'PT' => 'Portuguese',
							    'PR' => 'Puerto Rican',
							    'QA' => 'Qatari',
							    'RE' => 'Réunionese',
							    'RO' => 'Romanian',
							    'RW' => 'Rwandan',
							    'WS' => 'Samoan',
							    'SM' => 'Sammarinese',
							    'SA' => 'Saudi Arabian',
							    'SN' => 'Senegalese',
							    'RS' => 'Serbian',
							    'SC' => 'Seychellois',
							    'SL' => 'Sierra Leonean',
							    'SG' => 'Singapore',
							    'SK' => 'Slovak',
							    'SI' => 'Slovenian',
							    'SB' => 'Solomon Island',
							    'SO' => 'Somalian',
							    'ZA' => 'South African',
							    'SS' => 'South Sudanese',
							    'ES' => 'Spanish',
							    'LK' => 'Sri Lankan',
							    'SD' => 'Sudanese',
							    'SZ' => 'Swazi',
							    'SE' => 'Swedish',
							    'CH' => 'Swiss',
							    'TJ' => 'Tajikistani',
							    'TH' => 'Thai',
							    'TG' => 'Togolese',
							    'TO' => 'Tongan',
							    'TT' => 'Trinidadian',
							    'TN' => 'Tunisian',
							    'TR' => 'Turkish',
							    'TM' => 'Turkmen',
							    'TC' => 'none',
							    'TV' => 'Tuvaluan',
							    'UG' => 'Ugandan',
							    'UA' => 'Ukrainian',
							    'AE' => 'Emirati',
							    'GB' => 'British',
							    'US' => 'American',
							    'UY' => 'Uruguayan',
							    'UZ' => 'Uzbekistani',
							    'VU' => 'Ni-Vanuatu',
							    'WF' => 'Wallisian/Futunan',
							    'EH' => 'Sahrawian',
							    'YE' => 'Yemeni',
							    'ZM' => 'Zambian',
							    'ZW' => 'Zimbabwean',
							);
        
        // --
        $db = getDB();
        
        $select = $db->select()->from(array('c'=>'tbl_countries'));
        $countries = $db->fetchAll($select);
        
        foreach ( $countries as $row )
        {
        	if ( $row['Nationality'] == '' )
        	{
        		$natname = $nationality[$row['CountryIso']];
        		
        		$db->update('tbl_countries', array('Nationality' => $natname), $db->quoteInto('CountryIso=?',$row['CountryIso']));
        	}
        }
	}
	
	public function delayredirect()
	{
		echo '<script type="text/javascript">setTimeout(function(){ window.location=\''.$this->view->baseUrl().'/generalsetup/migrate/report/\' }, 1000);</script>';
	}
	
	function _start()
	{
		$starttime = microtime();	
		$starttime = explode(" ",$starttime);
		$starttime = $starttime[1] + $starttime[0];

		$this->_TIMESTART = $starttime;
	}

	function _end()
	{
		$starttime =  $this->_TIMESTART;

		$endtime = microtime();
		$endtime = explode(" ",$endtime);
		$endtime = $endtime[1] + $endtime[0];
		$stime = $endtime - $starttime;
		return round($stime,4);
	}
	
}
?>