<?php
class GeneralSetup_UniversityController extends Base_Base 
{
	private $lobjuniversity;
	private $lobjuniversityForm;
	private $lobjUser;
	private $lobjStaffForm;
	private $lobjdeftype;
	private $lobjcommonmodel;
	private $lobjstaffmodel;
	private $lobjreglistmodel;
	private $registry;
	private $locale;
	private $_gobjlog;
	
	public function init() 
	{
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->view->translate =Zend_Registry::get('Zend_Translate');
		$this->gstrsessionSIS = new Zend_Session_Namespace('sis'); 
		Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->fnsetObj();
   	    if(!$this->_getParam('search')) 
			unset($this->gobjsessionstudent->universitypaginatorresult);
	}
	
	public function fnsetObj()
	{
		$this->lobjform = new App_Form_Search ();
		$this->lobjPaginator = new App_Model_Definitiontype();
		$this->lobjuniversity = new GeneralSetup_Model_DbTable_University();
		$this->lobjuniversityForm = new GeneralSetup_Form_University (); //intialize user lobjuniversityForm
		$this->lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
		$this->lobjStaffForm = new GeneralSetup_Form_Staffmaster();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjcommonmodel = new App_Model_Common();
		$this->lobjstaffmodel = new GeneralSetup_Model_DbTable_Staffmaster();
		$this->lobjreglistmodel = new GeneralSetup_Model_DbTable_RegistrarList();
		$this->registry = Zend_Registry::getInstance();
		
		
	}
	
	public function indexAction() {
        $pageCount = 50;
		$this->view->title= $this->view->translate("University Setup");
		
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;

        //intialize search lobjuniversityForm
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		 
		//user model object
		$larrresult = $this->lobjuniversity->fngetUniversityDetails (); //get user details
		$lintpagecount = $this->gintPageCount;

		// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionstudent->universitypaginatorresult)) {
			$this->view->paginator = $this->lobjPaginator->fnPagination($this->gobjsessionstudent->universitypaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjuniversity->fnSearchUniversity ( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->universitypaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'university', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/generalsetup/university/index');
		}

        $paginator = Zend_Paginator::factory($larrresult);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);

        $totalRecord = $paginator->getTotalItemCount();

        $this->view->paginator = $paginator;
        $this->view->totalRecord = $totalRecord;


	}
	
	public function newuniversityAction() { //title
    	$this->view->title="Add New University";
		
		$this->view->lobjuniversityForm = $this->lobjuniversityForm; //send the lobjuniversityForm object to the view
		$this->view->lobjstaffmasterForm = $this->lobjStaffForm;
		$this->view->lobjuniversityForm->Univ_Name->setAttrib('validator', 'validateUniversityName');
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		
		if($this->locale == 'ar_YE') 
		{
			$this->lobjuniversityForm->FromDate->setAttrib('datePackage',"dojox.date.islamic");
			$this->lobjuniversityForm->ToDate->setAttrib('datePackage',"dojox.date.islamic");
		}
		
		$this->lobjStaffForm->StaffType->setValue(0);
		$this->lobjStaffForm->StaffType->setAttrib('readonly','readonly');
		
		
		
		
			if($this->view->DefaultDropDownLanguage==0){
				$this->lobjuniversityForm->IdStaff->addMultiOptions($this->lobjstaffmodel->fngetStaffMasterListforDD());
			}else{
				
				$BahasStaffMasterList=$this->lobjstaffmodel->fngetBahasStaffMasterListforDD();
				foreach($BahasStaffMasterList as $BahasStaffMasterListDtls){
				if($BahasStaffMasterListDtls ['ArabicName'] == NULL ) {
					$BahasStaffMasterListDtls ['ArabicName'] = $BahasStaffMasterListDtls ['EngName'];
				}
					$this->lobjuniversityForm->IdStaff->addMultiOption($BahasStaffMasterListDtls ['IdStaff'],$BahasStaffMasterListDtls ['ArabicName']);
					
				}
				
				
			}
		
		$lobjcountry = $this->lobjUser->fnGetCountryList();
		$this->lobjuniversityForm->Country->addMultiOptions($lobjcountry);
		$this->view->lobjuniversityForm->Country->setValue ($this->view->DefaultCountry);
		$larrcountrylist = $this->lobjcommonmodel->fnResetArrayFromValuesToNames($lobjcountry);
		$this->view->jsondata = 
				 '{
    				"label":"name",
					"identifier":"key",
					"items":'.Zend_Json_Encoder::encode($larrcountrylist).
				  '}';
		
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjuniversityForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjuniversityForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
//			$languageId = $this->lobjUser->fngetDefaultlanguage($formData['Country']);
//			$languageName = $this->lobjUser->fngetLanguageName($languageId[0]['value']);
//			echo "<pre>";
//			print_r($languageId);
//			print_r($languageName[0]['value']);die;
//			print_r($formData);die;
			//if ($this->lobjuniversityForm->isValid($formData)) {//process form 
				//unset ( $formData ['Save'] );
				//unset ( $formData ['Back'] );
				
				$id_university = $this->lobjuniversity->fnaddUniversity($formData);
				
					// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New University Add',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				//$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'university', 'action'=>'index'),'default',true));	//redirect
				$this->_redirect( $this->baseUrl . '/generalsetup/university/edituniversity/id/'.$id_university);	
			//}  	
        }     
    }
    
	public function edituniversityAction(){
    	
		$this->view->title="Edit University";  //title
    	
		$this->view->lobjuniversityForm = $this->lobjuniversityForm; //send the lobjuniversityForm object to the view
		
		if($this->locale == 'ar_YE') 
		{
			$this->lobjuniversityForm->FromDate->setAttrib('datePackage',"dojox.date.islamic");
			$this->lobjuniversityForm->ToDate->setAttrib('datePackage',"dojox.date.islamic");
		}
		
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		
		$this->view->lobjuniversityForm->UpdDate->setValue ( $ldtsystemDate );		
		
		$lobjcountry = $this->lobjUser->fnGetCountryList();
		$this->lobjuniversityForm->Country->addMultiOptions($lobjcountry);
	
    	$IdUniversity = $this->_getParam('id', 0);
		//var_dump($IdUniversity); exit;
    	$this->view->id_university = $this->view->escape(strip_tags($IdUniversity));
    	$this->lobjuniversityForm->IdStaff->addMultiOptions($this->lobjstaffmodel->fngetStaffMasterListforDD());
    	$result = $this->lobjuniversity->fneditUniversity($IdUniversity);
    	
    	$language = $this->lobjCommon->fngetLanguage($result[0]['Country']);
    	if(isset($language[0]['DefinitionDesc'])){
    	$this->view->defaultlanguage = $language[0]['DefinitionDesc'];
    	}
    	else{
    		$language[0]['DefinitionDesc'] = "Arabic";
    		$this->view->defaultlanguage = $language[0]['DefinitionDesc'];
    	}
    	foreach($result as $unvresult){
		}
		$lobjstate = $this->lobjUser->fnGetStateListcountry($unvresult['Country']);
		$this->lobjuniversityForm->State->addMultiOptions($lobjstate);

		$lobjCommonModel = new App_Model_Common();		
		$this->lobjuniversityForm->populate($unvresult);
		
		$this->view->lobjuniversityForm->ToDate->setValue ($ldtsystemDate );		
		$this->view->lobjuniversityForm->UpdDate->setValue ( $ldtsystemDate );
		
		$auth = Zend_Auth::getInstance();
		$this->view->lobjuniversityForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
				
    	if ($this->getRequest()->isPost()) {
    		
    		$formData = $this->getRequest()->getPost();
                    //var_dump($formData); exit;
    		if($this->gobjsessionsis->idUniversity == $formData['IdUniversity']){
				$language = $this->lobjCommon->fngetLanguage($formData['Country']);
				
				if(isset($language[0]['DefinitionDesc'])){
	    			$this->gobjsessionsis->UniversityLanguage = $language[0]['DefinitionDesc'];
	    		}
	    		else{
	    			$language[0]['DefinitionDesc'] = "Arabic";
	    			$this->gobjsessionsis->UniversityLanguage = $language[0]['DefinitionDesc'];
	    		}	
    			
			}

	    	if ($this->lobjuniversityForm->isValid($formData)) {
	   			$lintIdUniversity = $formData ['IdUniversity'];
				
				$this->lobjuniversity->fnupdateUniversity($formData,$lintIdUniversity);//update university
				
				$this->lobjreglistmodel->fnupdateRegistrarList($formData,$lintIdUniversity);//update registrar
				
				$this->lobjreglistmodel->fninsertRegistrarList($formData,$lintIdUniversity);//insert new registrar
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Universit Edit Id=' . $IdUniversity,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				
				//$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'university', 'action'=>'index'),'default',true));
				$this->_redirect( $this->baseUrl . '/generalsetup/university/edituniversity/id/'.$IdUniversity);
			}
    	}
    }
    
	public function getuniversitynameAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$UniversityName = $this->_getParam('UniversityName');	
		$larrDetails = $this->lobjuniversity->fngetValidateUniversityName($UniversityName);
		echo $larrDetails['Univ_Name'];
		
	}
	
////Action To Set the label of Arabic Caption
//	public function setlabelAction(){
//		$this->_helper->layout->disableLayout();
//		$this->_helper->viewRenderer->setNoRender();
//		//Get Country Id
//		$lintIdCountry = $this->_getParam('idCountry');
//		$languageId = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjUser->fngetDefaultlanguage($lintIdCountry));
////		$language = $this->lobjUser->fngetLanguageName($languageId[0]['value']);
////		//echo "<pre>";
////		//print_r($language);
//	}

	
public function universityAddressAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_univ = $this->_getParam('id', 0);		
		$this->view->id_univ = $id_univ;
		
		//get list branch address
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		$this->view->address_list = $addressDb->getDataByOrgId($id_univ,'tbl_university');		
		
	}
	
	public function addAddressAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_univ = $this->_getParam('id', 0);
		
		$form= new GeneralSetup_Form_AddressForm(array('idBranch'=>$id_univ));
		$this->view->addressForm = $form;
		
		if ($this->_request->isPost ()) {			
		
			$formData = $this->_request->getPost ();
			
			if ($form->isValid ( $formData )) {
				
				//save address				
				$auth = Zend_Auth::getInstance();
				
				$data['add_org_name'] = 'tbl_university';
				$data['add_org_id'] = $formData['idBranch'];
				$data['add_address_type'] = $formData['address_type'];
				$data['add_country'] = $formData['country'];
				$data['add_state'] = $formData['state'];
				$data['add_address1'] = $formData['address1'];
				$data['add_address2'] = $formData['address2'];
				$data['add_zipcode'] = $formData['zipcode'];
				$data['add_phone'] = $formData['phone'];
				$data['add_email'] = $formData['email'];
				$data['add_createddt'] = date('Y-m-d H:i:s');
				$data['add_createdby'] = $auth->getIdentity()->iduser;
				$data['add_city'] = $formData['city'];
				$data['add_state_others'] = $formData['state_others'];
				$data['add_city_others'] = $formData['city_others'];
				
				$addressDb = new GeneralSetup_Model_DbTable_Address();
				$addressDb->addData($data);
				
				$this->_redirect( $this->baseUrl . '/generalsetup/university/edituniversity/id/'.$formData['idBranch'].'#tab2');
			}
		}
	}
	
	public function editAddressAction(){
		
		if( $this->getRequest()->isXmlHttpRequest() )
		{
			$this->_helper->layout->disableLayout();	
		}

		$id_univ = $this->_getParam('id', 0);		
		$add_id = $this->_getParam('add_id', 0);
						
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		
		$form= new GeneralSetup_Form_AddressForm(array('idBranch'=>$id_univ,'addId'=>$add_id));
		
		if ($this->_request->isPost ()) {			
		
			$formData = $this->_request->getPost ();
			
	                   //var_dump($formData); exit;
			if ($form->isValid ( $formData )) {
			
				$auth = Zend_Auth::getInstance();
                                
                $address = $addressDb->getDatabyId($add_id);
				
				//insert into history
				$olddata['add_id'] = $address['add_id'];
				$olddata['addh_org_name'] = $address['add_org_name'];
				$olddata['addh_org_id'] = $address['add_org_id'];
				$olddata['addh_address_type'] = $address['add_address_type'];
				$olddata['addh_country'] = $address['add_country'];
				$olddata['addh_state'] = $address['add_state'];
				$olddata['addh_address1'] = $address['add_address1'];
				$olddata['addh_address2'] = $address['add_address2'];
				$olddata['addh_zipcode'] = $address['add_zipcode'];
				$olddata['addh_phone'] = $address['add_phone'];
				$olddata['addh_email'] = $address['add_email'];
				$olddata['addh_createddt'] = $address['add_createddt'];
				$olddata['addh_createdby'] = $address['add_createdby'];	
				$olddata['addh_changedt'] = date('Y-m-d H:i:s');
				$olddata['addh_changeby'] = $auth->getIdentity()->iduser;
				$olddata['addh_city'] = $address['add_city'];
				$olddata['addh_state_others'] = $address['add_state_others'];
				$olddata['addh_city_others'] = $address['add_city_others'];
                                
                                //var_dump($olddata); exit;
				
				$addHistoryDb = new GeneralSetup_Model_DbTable_AddressHistory();	
				$addHistoryDb->addData($olddata);
				
				//edit				
				$data['add_address_type'] = $formData['address_type'];
				$data['add_country'] = $formData['country'];
				$data['add_state'] = $formData['state'];
				$data['add_address1'] = $formData['address1'];
				$data['add_address2'] = $formData['address2'];
				$data['add_zipcode'] = $formData['zipcode'];
				$data['add_phone'] = $formData['phone'];
				$data['add_email'] = $formData['email'];
				$data['add_modifydt'] = date('Y-m-d H:i:s');
				$data['add_modifyby'] = $auth->getIdentity()->iduser;
				$data['add_city'] = $formData['city'];
				$data['add_state_others'] = $formData['state_others'];
				$data['add_city_others'] = $formData['city_others'];
				
				$addressDb->updateData($data,$formData['add_id']);
				
				$this->_redirect( $this->baseUrl . '/generalsetup/university/edituniversity/id/'.$formData['idBranch'].'#tab2');
				
			}
			
		}else{
			
			
			$add = $addressDb->getDatabyId($add_id);
			
			/*//get state to populate state from country
			$stateDB = new App_Model_General_DbTable_State();
			$state_data = $stateDB->getState($add['add_country']);		
			$element = $form->getElement('state');
			foreach($state_data as $s){
			$element->addMultiOption($s['idState'],$s['StateName']);
			}*/
		
			$address_data['address_type']=$add['add_address_type'];
			$address_data['country']=$add['add_country'];
			$address_data['state']=$add['add_state'];
			$address_data['address1']=$add['add_address1'];
			$address_data['address2']=$add['add_address2'];
			$address_data['zipcode']=$add['add_zipcode'];
			$address_data['phone']=$add['add_phone'];
			$address_data['email']=$add['add_email'];
			$address_data['city']=$add['add_city'];
			$address_data['state_others']=$add['add_state_others'];
			$address_data['city_others']=$add['add_city_others'];
			
			//print_r($address_data);

			$this->view->address_data = $address_data;
		
			$form->populate($address_data);
			$this->view->addressForm = $form;		
		}
		
	}
	
	public function deleteAddressAction(){
		
		$id_univ = $this->_getParam('id', 0);		
		$add_id = $this->_getParam('add_id', 0);
		
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		$addressDb->deleteData($add_id);

		$this->_helper->flashMessenger->addMessage(array('success' => "Address deleted"));
		
		$this->_redirect( $this->baseUrl . '/generalsetup/university/edituniversity/id/'.$id_univ.'#tab2');
		exit;
	}
	
    public function viewHistoryAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_univ = $this->_getParam('id', 0);
		
		//get list branch address
		$addressDb = new GeneralSetup_Model_DbTable_AddressHistory();
		$this->view->address_list = $addressDb->getDataByOrgId($id_univ,'tbl_university');		
	}
	
}