<?php
class GeneralSetup_CourseGroupController extends Base_Base { 

	
	public function indexAction(){
	
		$this->view->title = $this->view->translate("Course Section : Subject List");
		
		$form = new GeneralSetup_Form_CourseSearch(array('course_group'=>true));
		$this->view->form = $form;
		
		$auth = Zend_Auth::getInstance(); 
		$this->view->role=$auth->getIdentity()->IdRole;
		
        if($this->getRequest()->getPost() || $this->_getParam('from', 0)=="pull" )
        //if($this->getRequest()->getPost() )
		{		
						
                $formData2 =$this->getRequest()->getPost();	
                
                if (isset($formData2['Search'])){
                	
				if($form->isValid($_POST)){
				
				$formData =$this->getRequest()->getPost();	
							
				//var_dump($formData); exit;
				
				$idsemester = $formData["IdSemester"];
				$idcollege = $formData["IdCollege"];
                $SubjectName = isset($formData["SubjectName"])?$formData["SubjectName"]:NULL;
				$SubjectCode = isset($formData["SubjectCode"])? $formData["SubjectCode"] : 0;
							
				//$form->populate(array("IdSemester"=>$idsemester,"IdCollege"=>$idcollege));
                $form->populate($formData);
	
				$sofferedDB = new GeneralSetup_Model_DbTable_Subjectsoffered();
				$progDB= new GeneralSetup_Model_DbTable_Program();
				$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
				$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
				$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
	
				$this->view->idsem = $idsemester;
				$this->view->idcol = $idcollege;
				//Subject list base on Program Landscape from the selected faculty 
	
				//$programs = $progDB->fngetProgramDetails($idcollege);
				$programs = $progDB->getProgramDetailsByCollege($idcollege);
	
				//print_r($programs);
				
				$i=0;
				$j=0;
				$allblocklandscape=null;
				$allsemlandscape=null;
			
				if (count($programs))
				{
					foreach ($programs as $key => $program)
					{
						$activeLandscape=$landscapeDB->getAllLandscape($program["IdProgram"]);
						
						foreach($activeLandscape as $actl)
						{
							if($actl["LandscapeType"]!=44)
							{
								$allsemlandscape[$i] = $actl["IdLandscape"];						
								$i++;
							}
							elseif($actl["LandscapeType"]==44)
							{
								$allblocklandscape[$j] = $actl["IdLandscape"];
								$j++;
							}
						}
					}
				}
				else 
				{
					$subjects = FALSE;
				}
	
				if(is_array($allsemlandscape))
				{
					$subjectsem=$sofferedDB->getMultiLandscapeCourseOffer($allsemlandscape, $formData,$idsemester);
				}
	
				if(is_array($allblocklandscape))
				{
					$subjectblock=$sofferedDB->getMultiBlockLandscapeCourseOffer($allblocklandscape, $formData,$idsemester);
				}
	
				if(is_array($allsemlandscape) && is_array($allblocklandscape))
				{
					$subjects=array_merge( $subjectsem , $subjectblock );
				}
				else
				{
					if(is_array($allsemlandscape) && !is_array($allblocklandscape))
					{
						$subjects=$subjectsem;
					}
					elseif(!is_array($allsemlandscape) && is_array($allblocklandscape))
					{
						$subjects=$subjectblock;
					}
					else
					{
						$subjects=FALSE;
					}
				}			
	                        
	                $i=0;
					foreach($subjects as $subject){		
                                            
                                                $subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
                
                                                $semesterList = $subjectRegDB->getSemesterInfo($formData["IdSemester"]);
                                                $semesterRelated = $subjectRegDB->getSemesterRelated($semesterList['AcademicYear'], $semesterList['sem_seq']);
                                                
                                                $semArr = array($formData["IdSemester"]);
                
                                                if ($semesterRelated){
                                                    foreach ($semesterRelated as $semesterRelatedLoop){
                                                        array_push($semArr, $semesterRelatedLoop['IdSemesterMaster']);
                                                    }
                                                }
							
						//get total student register this subject
						//$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
						$total_student = $subjectRegDB->getTotalRegister($subject["IdSubject"],$formData["IdSemester"]);
						$subject["total_student"] = $total_student;				
						
						//get total group creates
						$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
						$total_group = $courseGroupDb->getTotalGroupByCourse($subject["IdSubject"],$formData["IdSemester"]);
						$info_group = $courseGroupDb->getGroupInfoByCourse($subject["IdSubject"],$formData["IdSemester"]);

						if ($info_group){
							$maxquota = 0;
							foreach($info_group as $info_group_loop){
								$maxquota = $info_group_loop['maxstud'] + $maxquota;
							}
						}else{
							$maxquota = 0;
						}

						$subject["maxquota"] = $maxquota;
						$subject["total_group"] = $total_group;
						$subject["IdSemester"] = $formData["IdSemester"];
						
						//get total no of student has been assigned
						$total_assigned = $subjectRegDB->getTotalAssigned($subject["IdSubject"],$formData["IdSemester"]);
						$total_unassigned = $subjectRegDB->getTotalUnAssigned($subject["IdSubject"],$formData["IdSemester"]);
						$subject["total_assigned"] = $total_assigned;
						$subject["total_unassigned"] = $total_unassigned;
						
						$subjects[$i]=$subject;
						
					$i++;	
					}
					;
					//var_dump($subjects); exit;
					$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($subjects));
					$paginator->setItemCountPerPage(1000);
					$paginator->setCurrentPageNumber($this->_getParam('page',1));
					
					$this->view->list_subject = $paginator;
			}
                    }    
		}
		
	}
	
	
	public function createGroupAction(){
		
		$auth = Zend_Auth::getInstance(); 
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			//print_r($formData);
			
			if($formData["generate_group_type"]==2){ //auto create group

				for($i=1; $i<=$formData["no_of_group"]; $i++){
					
					//create group
					$data["IdSemester"] = $formData["idSemester"];
					$data["IdSubject"]  = $formData["IdSubject"];
					$data["GroupCode"]  = "Group ".$i;
					$data["IdUniversity"]  = 1;
					$data["UpdUser"]  = $auth->getIdentity()->id;
					$data["UpdDate"]  = date("Y-m-d H:i:s");
					$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
					$idGroup = $courseGroupDb->addData($data);
					
					
					/*echo '<pre>';
					print_r($data);
					echo '</pre>';*/
					
					
					if($formData["assign_student_type"]==2){ //auto assign student to group
						
						//check how many student 
						if($formData["generate_group_type"]>0){
							
							$student_per_group = abs($formData["total_student"])/abs($formData["no_of_group"]);
						    $student_per_group = ceil($student_per_group);
							
							//query student register order by registrationID
							$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
							$list_student = $subjectRegDB->getStudents($formData["IdSubject"],$formData["idSemester"],$student_per_group);
							
							
							foreach($list_student as $student){
								
								//Update Studenr Register Subject
								$subjectRegDB->updateData(array('IdCourseTaggingGroup'=>$idGroup),$student["IdStudentRegSubjects"]);
								
								//insert dalam student mapping
								$info["IdCourseTaggingGroup"]=$idGroup;
								$info["IdStudent"]=$student["IdStudentRegistration"];
								$info['IdSubject']= $formData["IdSubject"];
								$info['UpdDate'] = date("Y-m-d H:i:s");
								
								$mappingDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
								$mappingDb->addData($info);
							}//end foreach							
							
						}//end if
						
					}//end if
				}//end for
			}//end if
			
			
			$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'course-group', 'action'=>'group-list','idSubject'=>$formData["IdSubject"],'idSemester'=>$formData["idSemester"]),'default',true));
		}//end if post
	}
	
	public function groupListAction(){
		
		$this->view->title = $this->view->translate("Course Section : Section List");
		
		$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
		$idSubject = $this->_getParam('idSubject', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		
		$this->view->idSemester = $idSemester;
		$this->view->idSubject = $idSubject;
		
		//semester
		$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
		$this->view->semester = $semesterDb->getData($idSemester);
		
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($idSubject);
		$this->view->subject = $subject;
				
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$groups = $courseGroupDb->getGroupList($idSubject,$idSemester);
		
		$courseProgramDb = new GeneralSetup_Model_DbTable_CourseGroupProgram();
		
		$i=0;
		foreach($groups as $group){
			
			//get verify by name
			if($group["VerifyBy"]){
			$staffDB = new GeneralSetup_Model_DbTable_Staffmaster();
			$verifyBy = $staffDB->getData($group["VerifyBy"]);
			$group['VerifyByName']=$verifyBy["FullName"];
			}
			
			$courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();
			//$total_student = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"]);
			$total_student = $courseGroupStudent->getTotalStudentViaSubReg($group["IdCourseTaggingGroup"]);
			$group["total_student"] = $total_student;
			
			
			//program
			$group["program"] = $courseProgramDb->getGroupData($group['IdCourseTaggingGroup']);
			
			
			$groups[$i]=$group;
			
		$i++;
		}
		
		$this->view->list_groups = $groups;

	}
	
	public function deleteGroupAction(){
		
		$idGroup = $this->_getParam('idGroup', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		
		if($idGroup){
			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			$courseGroupDb->deleteData($idGroup);
		}
		
		$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'course-group', 'action'=>'group-list','idSubject'=>$idSubject,'idSemester'=>$idSemester),'default',true));
		
	}
	
	public function assignStudentAction(){
		
		$this->view->title = "Assign Student to Section";
		
		$idGroup = $this->_getParam('idGroup', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		
		$this->view->idGroup = $idGroup;
		$this->view->idSemester = $idSemester;
		$this->view->idSubject = $idSubject;
                
                $subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
                
                $semesterList = $subjectRegDB->getSemesterInfo($idSemester);
                $semesterRelated = $subjectRegDB->getSemesterRelated($semesterList['AcademicYear'], $semesterList['sem_seq']);
                
                $semArr = array($idSemester);
                
                if ($semesterRelated){
                    foreach ($semesterRelated as $semesterRelatedLoop){
                        array_push($semArr, $semesterRelatedLoop['IdSemesterMaster']);
                    }
                }
		
		//get list student yg belum di assign ke any group
		$list_student = $subjectRegDB->getUnTagGroupStudents($idSubject,$idSemester,$semArr);
		$this->view->list_student = $list_student;
		
		//get group info
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($idGroup);
		$this->view->group = $group;
		
		$form = new GeneralSetup_Form_SearchGroupStudent();
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) {
    		
    		$formData = $this->getRequest()->getPost();
    		
    		if(isset($formData["Search"])){
    			if(isset($formData["bil_student"])){
    				$limit = $formData["bil_student"];
    				
    			}
				$list_student = $subjectRegDB->getUnTagGroupStudents($idSubject,$idSemester,$limit);
				$this->view->list_student = $list_student;
    		}else{
	    		for($i=0; $i<count($formData["IdStudentRegistration"]); $i++){
	    			
	    			$idStudentRegistration = $formData["IdStudentRegistration"][$i];
	    			$IdStudentRegSubjects = $formData["IdStudentRegSubjects"][$idStudentRegistration];
	    			
	    			$data["IdCourseTaggingGroup"] = $formData["idGroup"];
	    			$data["IdStudent"] = $idStudentRegistration;
	    			$data['IdSubject']= $idSubject;
					$data['UpdDate'] = date("Y-m-d H:i:s");

		    		$studentGroupDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
		    		$studentGroupDb->addData($data);
		    			    		
		    		//update student reg subject table
		    		$subjectRegDB->updateData(array('IdCourseTaggingGroup'=>$formData["idGroup"]),$IdStudentRegSubjects);
	    		}
    		
    		$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'course-group', 'action'=>'group-list','idSubject'=>$idSubject,'idSemester'=>$idSemester,'msg'=>1),'default',true));
    		}
    	}
	}
	
	
	public function removeStudentAction(){
		
		$this->view->title = "View & Remove Student from Section";
		
		
		$idGroup = $this->_getParam('idGroup', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		
		$this->view->idGroup = $idGroup;
		$this->view->idSemester = $idSemester;
		$this->view->idSubject = $idSubject;
		
		//get list student yg dah di assign ke  group
		$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
//		$list_student = $subjectRegDB->getTaggedGroupStudents($idGroup);	
		$list_student = $subjectRegDB->getTaggedGroupStudents($idGroup);	
		$this->view->list_student = $list_student;
		
		
		
		//get group info
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($idGroup);
		$this->view->group = $group;
		
		$form = new GeneralSetup_Form_SearchGroupStudent();
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) {
    		
    		$formData = $this->getRequest()->getPost();
    		//print_r($formData);
    		
    		for($i=0; $i<count($formData["IdStudentRegSubjects"]); $i++){
    			
    			$IdStudentRegSubjects = $formData["IdStudentRegSubjects"][$i];
    			
    			$IdStudentRegistration = $formData["IdStudentRegistration"][$IdStudentRegSubjects];
    		
	    		//remove dari student mapping
	    		$studentGroupDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();	    	
		    	$studentGroupDb->removeStudent($idGroup,$IdStudentRegistration);
		    	
		    	//update reg subject to remove tagging to group
		    	$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
		    	$subjectRegDB->updateData(array('IdCourseTaggingGroup'=>0),$IdStudentRegSubjects);
    		}
    	
    		$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'course-group', 'action'=>'group-list','idSubject'=>$idSubject,'idSemester'=>$idSemester,'msg'=>2),'default',true));
    		
		}
		
		
	}
	
	
	public function addGroupAction(){
	  
	  $this->view->title = $this->view->translate("Course Section : Add New Section");
        	  
	  $registry = Zend_Registry::getInstance();
	  $this->view->locale = $registry->get('Zend_Locale');
		
	  $idSubject = $this->_getParam('idSubject', 0);
	  $idSemester = $this->_getParam('idSemester', 0);
	  
	  $this->view->idSemester = $idSemester;
	  $this->view->idSubject = $idSubject;
	  
	  //semester
	  $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
	  $semester = $semesterDb->getData($idSemester);
	  $this->view->semester = $semester;
	 
	  //get Subject Info
	  $subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
	  $subject = $subjectDb->getData($idSubject);
	  $this->view->subject = $subject;
	  
	  //faculty list
	  $facultyDb = new App_Model_General_DbTable_Collegemaster();
	  $this->view->faculty_list = $facultyDb->getFaculty();
	  
	  //program list
	  $programDb = new App_Model_Record_DbTable_Program();
	  $program = array();
	  foreach ($this->view->faculty_list as $faculty){
	    $where = array(
	        'IdCollege = ?' => $faculty['IdCollege'],
	        'Active = ?' => 1
	        );
	    $programList = $programDb->fetchAll($where);
	    
	    $program[] = array(
	          'faculty' => $faculty,
	          'program' => $programList->toArray()
	        );
	  }
	  $this->view->program_list = $program;
	  
	  
	  //form
      $form = new GeneralSetup_Form_CourseGroup(array('idSubject'=>$idSubject,'IdSemester'=>$idSemester,'idFaculty'=>$subject["IdFaculty"],'subCode'=>$subject["SubCode"],'semester'=> $semester));
      $this->view->form = $form;
      
      if ($this->getRequest()->isPost()) {
      	
      	$formData = $this->getRequest()->getPost();
      
      	if($form->isValid($formData)){
      	
        	$auth = Zend_Auth::getInstance();
        	
        	
        	//create group
        	$data["IdSemester"] = $formData["idSemester"];
        	$data["IdSubject"]  = $formData["IdSubject"];
        	$data["GroupName"]  = $formData["GroupName"];
        	$data["GroupCode"]  = $formData["GroupCode"];
        	$data["maxstud"]  = $formData["maxstud"];
        	$data["remark"]  = $formData["remark"];
        	$data["IdLecturer"]  = isset($formData["IdLecturer"])?$formData["IdLecturer"]:null;
        	//$data["VerifyBy"]  = isset($formData["VerifyBy"])?$formData["VerifyBy"]:null;
			$data["reservation"]  = $formData["reservation"];
			$data["IdBranch"]  = $formData["IdBranch"];
        	$data["IdUniversity"]  = 1;
        	$data["UpdUser"]  = $auth->getIdentity()->id;
        	$data["UpdDate"]  = date("Y-m-d H:i:s");
        	
        	if(isset($formData["class_start_date"]) && $formData["class_start_date"]!=''){
        		$data["class_start_date"]  = date("Y-m-d",strtotime($formData["class_start_date"]));
        	}
        	
        	if(isset($formData["class_end_date"]) && $formData["class_end_date"]!=''){
        		$data["class_end_date"]  = date("Y-m-d",strtotime($formData["class_end_date"]));
        	}
        	
        	$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
        	$idGroup = $courseGroupDb->addData($data);
        	
        	//course group program
        	if( isset($formData['program']) ){
        	  $courseGroupProgramDb = new GeneralSetup_Model_DbTable_CourseGroupProgram();
                  $i=0;
        	  foreach($formData['program'] as $program){
        	    $dt = array(
        	          'group_id' => $idGroup,
        	          'program_id' => $program,
                          'program_scheme_id' => $formData['programscheme'][$i]
        	        );
        	    
        	    $courseGroupProgramDb->insert($dt);
                    $i++;
        	  }
        	}
        	
        	
        	$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'course-group', 'action'=>'group-list','idSubject'=>$formData["IdSubject"],'idSemester'=>$formData["idSemester"]),'default',true));
      	}else{
      	  $form->populate($formData);
      	  
      	}
      }
      
      $this->view->form = $form;
		
	}
	
	
	public function editGroupAction(){
		
	  $this->view->title = $this->view->translate("Course Section : Edit Section");
		
	  $registry = Zend_Registry::getInstance();
	  $this->view->locale = $registry->get('Zend_Locale');
	  
	  $idGroup = $this->_getParam('idGroup', 0);
	  $idSubject = $this->_getParam('idSubject', 0);
	  $idSemester = $this->_getParam('idSemester', 0);
	  
	  $this->view->idSemester = $idSemester;
	  $this->view->idSubject = $idSubject;
	  $this->view->idGroup = $idGroup;
	  
	  
	  //semester
	  $semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
	  $semester = $semesterDb->getData($idSemester);
	  $this->view->semester = $semester;
	  
	  //get Subject Info
	  $subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
	  $subject = $subjectDb->getData($idSubject);
	  $this->view->subject = $subject;
	  
	  //faculty list
	  $facultyDb = new App_Model_General_DbTable_Collegemaster();
	  $this->view->faculty_list = $facultyDb->getFaculty();
	  
	  //program list
	  $programDb = new App_Model_Record_DbTable_Program();
	  $program = array();
	  foreach ($this->view->faculty_list as $faculty){
	    $where = array(
	        'IdCollege = ?' => $faculty['IdCollege'],
	        'Active = ?' => 1
	        );
	    $programList = $programDb->fetchAll($where);
	    
	    $program[] = array(
	          'faculty' => $faculty,
	          'program' => $programList->toArray()
	        );
	  }
	  $this->view->program_list = $program;
	  
	  //group
	  $courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
	  $data = $courseGroupDb->getInfo($idGroup);
          //var_dump($data);
	  //group program
	  $courseGroupProgramDb = new GeneralSetup_Model_DbTable_CourseGroupProgram();
	  $data_program = $courseGroupProgramDb->getGroupData($idGroup);
	  $this->view->data_program = $data_program;
	  
	  //form
      $form = new GeneralSetup_Form_CourseGroup(array('idSubject'=>$idSubject,'IdSemester'=>$idSemester,'idGroup'=>$idGroup,'semester'=> $semester));
      $this->view->form = $form;
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
                        //var_dump($formData); exit;
			if($form->isValid($formData)){
			  
			  
			    //course group
    			//$data_upd["GroupCode"]  = $formData["GroupCode"];
    			$data_upd["GroupName"]  = $formData["GroupName"];
    			$data_upd["IdLecturer"]  = isset($formData["IdLecturer"]) && $formData["IdLecturer"]!=0?$formData["IdLecturer"]:null;
    			//$data_upd["VerifyBy"]  = isset($formData["VerifyBy"]) && $formData["VerifyBy"]!=0?$formData["VerifyBy"]:null;
    			$data_upd["reservation"]  = $formData["reservation"];
                        $data_upd["IdBranch"]  = $formData["IdBranch"];
                        $data_upd["maxstud"]  = $formData["maxstud"];
    			$data_upd["remark"]  = $formData["remark"];
    			
				if(isset($formData["class_start_date"]) && $formData["class_start_date"]!=''){
	        		$data_upd["class_start_date"]  = date("Y-m-d",strtotime($formData["class_start_date"]));
	        	}
	        	
	        	if(isset($formData["class_end_date"]) && $formData["class_end_date"]!=''){
	        		$data_upd["class_end_date"]  = date("Y-m-d",strtotime($formData["class_end_date"]));
	        	}
    			
    			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
    			$courseGroupDb->updateData($data_upd, $formData["IdCourseTaggingGroup"]);
    			
    			
    			$courseGroupProgramDb = new GeneralSetup_Model_DbTable_CourseGroupProgram();
    			//group program
    			if(isset($formData['program_remove'])){
    			  foreach ($formData['program_remove'] as $id_to_remove){
    			    $courseGroupProgramDb->delete('id = '.$id_to_remove);
    			  }
    			}
    			
    			if(isset($formData['program_add'])){
                            $i=0;
    			  foreach ($formData['program_add'] as $id_program_to_add){
    			    
    			    $dt = array(
        	          'group_id' => $formData["IdCourseTaggingGroup"],
        	          'program_id' => $id_program_to_add,
                          'program_scheme_id' => $formData['programscheme'][$i]
        	        );
        	    
        	        $courseGroupProgramDb->insert($dt);
                        $i++;
    			  }
    			}
    			
    			
    			$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'course-group', 'action'=>'group-list','idSubject'=>$formData["IdSubject"],'idSemester'=>$formData["idSemester"]),'default',true));
			}else{
			  $form->populate($formData);
			}
		}else{
			$form->populate($data);
		}
			
		$this->view->form = $form;
			
		
	}
	
	public function scheduleAction(){
		
		$this->view->title = "Schedule";
		
		$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
				
		$idGroup = $this->_getParam('idGroup', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		
		$this->view->idGroup = $idGroup;
		$this->view->idSemester = $idSemester;
		$this->view->idSubject = $idSubject;
		
		$form = new GeneralSetup_Form_ScheduleForm(array('idSubject'=>$idSubject,'IdSemester'=>$idSemester,'IdGroup'=>$idGroup));
		$this->view->form = $form;
		
		$groupSchdeleDb = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
		$schedule = $groupSchdeleDb->getSchedule($idGroup);
		$this->view->schedule = $schedule;		
		
		//get group info
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($idGroup);
		$this->view->group = $group;
		
	}
	
	public function addScheduleAction(){
	  
        if($this->_request->isXmlHttpRequest()){
          $this->_helper->layout->disableLayout();
        }
		
		$auth = Zend_Auth::getInstance();
		 
		$idGroup = $this->_getParam('idGroup', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		
		//get group info
		$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
		$group = $courseGroupDb->getInfo($idGroup);
		$this->view->group = $group;
		
		$form = new GeneralSetup_Form_ScheduleForm(array('idSubject'=>$idSubject,'IdSemester'=>$idSemester,'IdGroup'=>$idGroup,'idLect'=>$group['IdLecturer']));

		//branch
		$branchDB = new GeneralSetup_Model_DbTable_Branchofficevenue();
		$branchList = $branchDB->fnGetBranchList();

		/*$form->idBranch->addMultiOption('0','All');
		foreach ( $branchList as $branch )
		{
			$form->idBranch->addMultiOption($branch['id'], $branch['value']);
		}*/
		
		//deftype
		
		
		if ($this->getRequest()->isPost()) {
		  
		    $formData = $this->getRequest()->getPost();

		    if($form->isValid($formData)){
			
                        if(isset($formData["sc_date"]) && $formData["sc_date"]!=''){    			
                            $data["sc_date"]=date('Y-m-d',strtotime($formData["sc_date"]));
                        }else{
                            $data["sc_date"]=null;
                        }
		      
    			$data["idGroup"]=$formData["idGroup"];    			
    			$data["sc_day"]=$formData["sc_day"];
    			$data["sc_start_time"]=$formData["sc_start_time"];
    			$data["sc_end_time"]=$formData["sc_end_time"];
    			$data["sc_venue"]=$formData["sc_venue"];
    			if(isset($formData["idCollege"])&&$formData["idCollege"]!=""&&$formData["idCollege"]!=0){
    				$data["idCollege"]=$formData["idCollege"];
    			}
    			if(isset($formData["idLecturer"])&&$formData["idLecturer"]!=0&&$formData["idLecturer"]!=""){
    				$data["IdLecturer"]=$formData["idLecturer"];
    			}
    			$data["sc_class"]=$formData["sc_class"];
    			$data["sc_createdby"]=$auth->getIdentity()->id;
    			$data["sc_createddt"]=date("Y-m-d H:i:s");
    			
    			$data["sc_remark"]=$formData["sc_remark"];
                        $data['idBranch'] = $formData['idBranch'];
                        $data['idClassType'] = $formData['idClassType'];
    
    			$scheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
    			$scid = $scheduleDB->addData($data);
                        
                        //todo insert qp_event
                        $classstarttime = $formData["sc_start_time"];
                        $classendtime = $formData["sc_end_time"];
                        
                        $class_start_time = ($formData["sc_start_time"]*60);
                        $class_end_time = ($formData["sc_end_time"]*60);

                        if ($data["sc_date"]=='0000-00-00' || $data["sc_date"]==null){
                            $start_day = 0;
                            $end_day = 0;
                            $program_mode = 2;
                        }else{
                            $start_day = strtotime($data["sc_date"].' '.$classstarttime);
                            $end_day = strtotime($data["sc_date"].' '.$classendtime);
                            $program_mode = 1;
                        }

                        $start_time = strtoupper(date('h:i a', strtotime($classstarttime)));
                        $end_time = strtoupper(date('h:i a', strtotime($classendtime)));

						if (isset($formData["sc_venue"])) {
							$venue = $scheduleDB->getDefination($formData["sc_venue"]);
						}

						if (isset($formData["idClassType"])) {
							$classtype = $scheduleDB->getDefination($formData['idClassType']);
						}

						if (isset($formData["idLecturer"])) {
							$lecturer = $scheduleDB->getLecturer($formData["idLecturer"]);
						}
                        
                        $semesterInfo = $scheduleDB->getSemesterById($group['IdSemester']);
                        
                        $programList = $scheduleDB->getGroupProgram2($idGroup);
                        
                        foreach ($programList as $programLoop){
                        
                            $qpData = array(
                                'IdCourseTaggingGroup'=>$idGroup,
                                'sc_id'=>$scid,
                                'title'=>$group['GroupName'], 
                                'description'=>'', 
                                'venue'=>isset($venue['DefinitionDesc']) ? $venue['DefinitionDesc']:'',
                                'all_day'=>0, 
                                'start_day'=>$start_day, 
                                'end_day'=>$end_day, 
                                'start_time'=>$start_time, 
                                'end_time'=>$end_time, 
                                'background_color'=>'', 
                                'border_color'=>'', 
                                'text_color'=>'', 
                                'date_created'=>strtotime(date('Y-m-d H:i:s')), 
                                'last_updated'=>strtotime(date('Y-m-d H:i:s')), 
                                'access'=>0, 
                                'category'=>1, 
                                'status'=>0, 
                                'repeat'=>0, 
                                'program_mode'=>$program_mode, 
                                'course_code'=>$group['subject_code'], 
                                'course_name'=>$group['subject_name'], 
                                'merge_group'=>null,//to be declare 
                                'section'=>null,//to be declare 
                                'class_size'=>$group['maxstud'], 
                                'class_duration'=>($class_start_time-$class_end_time), 
                                'lecturer_code'=>isset($lecturer['IdStaff']) ? $lecturer['IdStaff']:0,
                                'lecturer_name'=>isset($lecturer['FullName']) ? $lecturer['FullName']:'',
                                'lecturer_email'=>isset($lecturer['Email']) ? $lecturer['Email']:'',
                                'class_id'=>$group['GroupName'], 
                                'class_type'=>isset($classtype['DefinitionDesc']) ? $classtype['DefinitionDesc']:'',
                                'class_number'=>0,//to be declare 
                                'class_date'=>$start_day, 
                                'class_day'=>$formData["sc_day"], 
                                'class_start_time'=>$class_start_time, 
                                'class_end_time'=>$class_end_time, 
                                'in_workload'=>0, 
                                'ordering'=>1, 
                                'tag1_title'=>'Programme', 
                                'tag1_value'=>$programLoop['ProgramName'], 
                                'tag2_title'=>'Programme Mode', 
                                'tag2_value'=>$programLoop['mop'], 
                                'tag3_title'=>'Programme Status', 
                                'tag3_value'=>$programLoop['mos'], 
                                'tag4_title'=>'Branch', 
                                'tag4_value'=>$group['BranchName'], 
                                'tag5_title'=>'Study Mode', 
                                'tag5_value'=>$programLoop['mos'], 
                                'tag6_title'=>'Department', 
                                'tag6_value'=>$programLoop['EnglishDescription'], 
                                'tag7_title'=>'Collaborative Partner', 
                                'tag7_value'=>$group['BranchName'], 
                                'export_date'=>strtotime(date('Y-m-d H:i:s')), 
                                'schedule_id'=>0, 
                                'term'=>$semesterInfo['SemesterMainName'].' Semester', 
                                'session'=>0, 
                                'version'=>0, 
                                'field_change1'=>0, 
                                'field_change2'=>0, 
                                'field_change3'=>0, 
                                'field_change4'=>0, 
                                'field_change5'=>0, 
                                'field_change6'=>0, 
                                'field_change7'=>0
                            );
                            $scheduleDB->insertQpEvent($qpData);
                        }
    			
    			$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'course-group', 'action'=>'schedule','idSubject'=>$formData["IdSubject"],'idSemester'=>$formData["idSemester"],'idGroup'=>$formData["idGroup"]),'default',true));
			
		    }else{
		      $form->populate($formData);
		    }
		}
		
		$this->view->form = $form;
		
		
	}
	
	
	public function editScheduleAction(){
		
        if($this->_request->isXmlHttpRequest()){
          $this->_helper->layout->disableLayout();
        }
		
		
		$this->view->title = "Edit Schedule";
		
		//setlocale(LC_TIME, 'id_ID');
				
		$idGroup = $this->_getParam('idGroup', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		$idSchedule = $this->_getParam('idSchedule', 0);
					
		//get data
		$scheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
		$data = $scheduleDB->getData($idSchedule);
		$this->view->data = $data;
		
		$form = new GeneralSetup_Form_ScheduleForm(array('idSubject'=>$idSubject,'IdSemester'=>$idSemester,'IdGroup'=>$idGroup,'IdSchedule'=>$idSchedule));		

		//branch
		$branchDB = new GeneralSetup_Model_DbTable_Branchofficevenue();
		$branchList = $branchDB->fnGetBranchList();

		/*$form->idBranch->addMultiOption('0','All');
		foreach ( $branchList as $branch )
		{
			$form->idBranch->addMultiOption($branch['id'], $branch['value']);
		}*/
		
		
		
		if ($this->getRequest()->isPost()) {
			
		     $formData = $this->getRequest()->getPost();
		    //var_dump($formData); exit;
		    if($form->isValid($formData)){
                        $scdata = $scheduleDB->getData($idSchedule);
                        
		      	if(isset($formData["sc_date"]) && $formData["sc_date"]!=''){    			
                            $data["sc_date"]=date('Y-m-d',strtotime($formData["sc_date"]));
                        }else{
                            $data["sc_date"]=null;
                        }
			    
    			$data["sc_day"]=$formData["sc_day"];
    			$data["sc_start_time"]=$formData["sc_start_time"];
    			$data["sc_end_time"]=$formData["sc_end_time"];
    			$data["sc_venue"]=$formData["sc_venue"];
    			$data["sc_class"]=$formData["sc_class"];
    			$data["idLecturer"]=$formData["idLecturer"];
    			$data["sc_remark"]=$formData["sc_remark"];
                        $data['idBranch'] = $formData['idBranch'];
                        $data['idClassType'] = $formData['idClassType'];
			//var_dump($data); exit;	
    			$scheduleDB->updateData($data, $idSchedule);
                        
                        //insert history
                        $scdata["sc_createddt"]=date('Y-m-d H:i:s');
                        $scheduleDB->insertScheduleHistory($scdata);
                        
                        $attModel = new GeneralSetup_Model_DbTable_Attendance();
                        if ($data["sc_date"] != null){
                            $scheduleData = array(
                                'ctd_scdate'=>date('Y-m-d', strtotime($formData['date']))
                            );
                            $attModel->updateAttendanceDate($scheduleData, $formData['idGroup'], $scdata['sc_date']);
                        }
                        
                        //update qp event
                        $getQpEvents = $scheduleDB->getQpEvent($idGroup, $idSchedule);
                        
                        if ($getQpEvents){
                            foreach($getQpEvents as $getQpEvent){
                            //insert qp_event history
                                $qpHistoryData = array(
                                    'event_id'=>$getQpEvent['id'], 
                                    'venue'=>$getQpEvent['venue'], 
                                    'start_day'=>$getQpEvent['start_day'], 
                                    'end_day'=>$getQpEvent['end_day'], 
                                    'start_time'=>$getQpEvent['start_time'], 
                                    'end_time'=>$getQpEvent['end_time'], 
                                    'date_created'=>$getQpEvent['date_created'], 
                                    'last_updated'=>$getQpEvent['last_updated'], 
                                    'status'=>$getQpEvent['status'], 
                                    'class_duration'=>$getQpEvent['class_duration'], 
                                    'lecturer_code'=>$getQpEvent['lecturer_code'], 
                                    'lecturer_name'=>$getQpEvent['lecturer_name'], 
                                    'lecturer_email'=>$getQpEvent['lecturer_email'], 
                                    'class_date'=>$getQpEvent['class_date'], 
                                    'class_day'=>$getQpEvent['class_day'], 
                                    'class_start_time'=>$getQpEvent['class_start_time'], 
                                    'class_end_time'=>$getQpEvent['class_end_time'], 
                                    'sequence_no'=>$getQpEvent['sequence_no'], 
                                    'field_change1'=>$getQpEvent['field_change1'], 
                                    'field_change2'=>$getQpEvent['field_change2'], 
                                    'field_change3'=>$getQpEvent['field_change3'], 
                                    'field_change4'=>$getQpEvent['field_change4'], 
                                    'field_change5'=>$getQpEvent['field_change5'], 
                                    'field_change6'=>$getQpEvent['field_change6'], 
                                    'field_change7'=>$getQpEvent['field_change7']
                                );
                                $scheduleDB->insertQpEventHistory($qpHistoryData);

                                $classstarttime = $formData["sc_start_time"];
                                $classendtime = $formData["sc_end_time"];

                                if ($data["sc_date"]=='0000-00-00' || $data["sc_date"]==null){
                                    $start_day = 0;
                                    $end_day = 0;
                                }else{
                                    $start_day = strtotime($data["sc_date"].' '.$classstarttime);
                                    $end_day = strtotime($data["sc_date"].' '.$classendtime);
                                }

                                if ($getQpEvent['program_mode']=='Full Time'){
                                    $program_mode = 2;
                                }else{
                                    $program_mode = 1;
                                }

                                $start_time = strtoupper(date('h:i a', strtotime($classstarttime)));
                                $end_time = strtoupper(date('h:i a', strtotime($classendtime)));

                                $venue = $scheduleDB->getDefination($formData["sc_venue"]);

                                $lecturer = $scheduleDB->getLecturer($formData["idLecturer"]);

                                //status update
                                $field_change1 = 0;
                                $field_change2 = 0;
                                $field_change3 = 0;
                                $field_change4 = 0;
                                $field_change5 = 0;
                                $cstatus = 0;

                                if ($start_day != $getQpEvent['start_day']){
                                    $field_change1 = 1;
                                    $cstatus = 1;
                                }

                                if ($data["sc_day"] != $getQpEvent['class_day']){
                                    $field_change2 = 1;
                                    $cstatus = 1;
                                }

                                if ($start_time != $getQpEvent['start_time'] || $end_time != $getQpEvent['end_time']){
                                    $field_change3 = 1;
                                    $cstatus = 1;
                                }

                                if ($venue['DefinitionDesc'] != $getQpEvent['venue']){
                                    $field_change4 = 1;
                                    $cstatus = 1;
                                }

                                if ($formData["idLecturer"] != $getQpEvent['lecturer_code']){
                                    $field_change5 = 1;
                                    $cstatus = 1;
                                }

                                $qpData = array(
                                    'start_day'=>$start_day,
                                    'end_day'=>$end_day,
                                    'class_date'=>$start_day,
                                    'class_day'=>$data["sc_day"],
                                    'start_time'=>$start_time,
                                    'end_time'=>$end_time,
                                    'class_start_time'=>($classstarttime*60),
                                    'class_end_time'=>($classendtime*60),
                                    'venue'=>$venue['DefinitionDesc'],
                                    'lecturer_code'=>$formData["idLecturer"],
                                    'lecturer_name'=>$lecturer['FullName'],
                                    'lecturer_email'=>$lecturer['Email'],
                                    'status'=>$cstatus,
                                    'last_updated'=>date('Y-m-d H:i:s'),
                                    'field_change1'=>$field_change1, 
                                    'field_change2'=>$field_change2, 
                                    'field_change3'=>$field_change3, 
                                    'field_change4'=>$field_change4, 
                                    'field_change5'=>$field_change5 
                                );
                                $scheduleDB->updateQpEvent($qpData, $getQpEvent['id']);
                            }
                        }
    			
    			$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'course-group', 'action'=>'schedule','idSubject'=>$formData["IdSubject"],'idSemester'=>$idSemester,'idGroup'=>$idGroup),'default',true));
			
			}else{
			  $form->populate($formData);
			}
		}
		if($data["sc_date"]!=null){
		  $data['sc_date'] = date('d-m-Y',strtotime($data['sc_date']));
		}
		$form->populate($data);
		$this->view->form = $form;
	}
	
	
	public function deleteScheduleAction(){
		
			$idGroup = $this->_getParam('idGroup', 0);
			$idSemester = $this->_getParam('idSemester', 0);
			$idSubject = $this->_getParam('idSubject', 0);
			$idSchedule = $this->_getParam('idSchedule', 0);
		
			$scheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
			$scheduleDB->deleteData($idSchedule);
			
			$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'course-group', 'action'=>'schedule','idSubject'=>$idSubject,'idSemester'=>$idSemester,'idGroup'=>$idGroup),'default',true));
			
	}
	
	public function attendanceListAction(){
		
		$this->_helper->layout->setLayout('preview');
		
		$this->view->title = "Daftar Hadir Mahasiswa";
		
		$idSchedule = $this->_getParam('idSchedule', 0);
		$this->view->idSchedule = $idSchedule;
		
		//get schedule info
		$scheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
		$schedule = $scheduleDB->getDetailsInfo($idSchedule);
		$this->view->schedule = $schedule;
		
		//get list student yg dah di assign ke  group
		$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
		$list_student = $subjectRegDB->getTaggedGroupStudents($schedule["idGroup"]);		
		$this->view->list_student = $list_student;
		
		
	}
	
	public function attendanceListPdfAction(){
            
                //set unlimited
                set_time_limit(0);
                ini_set('memory_limit', '-1');
		
		$this->view->title = "Daftar Hadir Mahasiswa";
		
		$idSchedule = $this->_getParam('idSchedule', 0);
		
		//get schedule info
		$scheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
		$schedule_data = $scheduleDB->getDetailsInfo($idSchedule);
                $programList = $scheduleDB->getGroupProgram($schedule_data["idGroup"]);
                
		//get list student yg dah di assign ke  group
		$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
		$list_student = $subjectRegDB->getTaggedGroupStudents($schedule_data["idGroup"]);
                
                $classList = array();
                if ($schedule_data['scdate'] != null && $schedule_data['scdate'] != '0000-00-00'){
                    $classList = $scheduleDB->getScheduleByGroup($schedule_data["idGroup"]);
                }else{
                    $date = $schedule_data['class_start_date'];
                    $i = 0;
                    while ($date <= $schedule_data['class_end_date']){
                        $classList[$i]['sc_date'] = $date;
                        $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                        $i++;
                        /*if ($i==3){
                            break;
                        }*/
                    }
                }
		
		global $schedule;
		$schedule = $schedule_data;
		
		global $data;
		$data = $list_student;
                
                global $class_list;
                $class_list = $classList;
                
                global $program_list;
                $program_list = $programList;
		
		/*
		 * PDF Generation
		 */
                
                $fieldValues = array(
                    '$[LOGO]'=> APPLICATION_URL."/images/logo_text.png"
                );
		
		require_once 'dompdf_config.inc.php';
		
		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
		
		//$html_template_path = DOCUMENT_PATH."/template/CourseGroupingAttendance__.html";
                $html_template_path = DOCUMENT_PATH."/template/CourseGroupingAttendance.html";
		
		$html = file_get_contents($html_template_path);
                //exit;
                //replace variable
		foreach ($fieldValues as $key=>$value){
			$html = str_replace($key,$value,$html);	
		}
		
		/*echo $html;
		exit;*/
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		//$dompdf->set_paper('a4', 'potrait');
                $dompdf->set_paper('a4', 'landscape');
		$dompdf->render();
		
		
		$dompdf->stream("Attendance.pdf");
		exit;
	}
	
	public function ajaxGetLecturerAction(){
		
    	$idCollege = $this->_getParam('idCollege',null);
    	$idLecturer = $this->_getParam('IdLecturer',null);
    	//var_dump($idCollege);
     	$this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$staffDb = new GeneralSetup_Model_DbTable_Staffmaster();
	  	
	  	if($idLecturer!=null){
	  	  $staff = $staffDb->getData($idLecturer);
	  	}else{
	  	  $staff = $staffDb->getAcademicStaff($idCollege);
	  	}        
	    
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($staff);
		
		echo $json;
		exit();
    }
    
    
    public function updateGroupDataMigrationAction(){
    	
    	$studentGroupDb = new GeneralSetup_Model_DbTable_CourseGroupStudent();
    	$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
    	
    	$student = $subjectRegDB->getStudentWithGroup();
    	
    	foreach($student as $s){
    		
    		//cek dah ada lom?
    		$return_row = $studentGroupDb->checkStudentMappingGroup($s['IdCourseTaggingGroup'],$s["IdStudentRegistration"]);
    		
    		if(!$return_row){
		    	$data["IdCourseTaggingGroup"]=$s['IdCourseTaggingGroup'];
		    	$data["IdStudent"]=$s['IdStudentRegistration'];   	
	    	echo '<br>add  => '.$s['IdCourseTaggingGroup'].' | '.$s["IdStudentRegistration"] .'<br>';
	    	//$studentGroupDb->addData($data);
    		}else{
    			
    			echo '<br>exist<br>';
    		}
    	
    		
    	}exit;
    }
    
    
	public function ajaxValidateCodeAction(){
		
    	$code= $this->_getParam('code',null);
    	
     	$this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
	  	$result = $courseGroupDb->validateCodeGroup($code);
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
	
	public function coordinatorListAction(){
		
		$this->view->title = $this->view->translate("Course Section : Coordinator List");
		
		$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
	
		$idSubject = $this->_getParam('idSubject', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idCollege = $this->_getParam('idCollege', 0);
		
		$this->view->idSemester = $idSemester;
		$this->view->idSubject = $idSubject;
		$this->view->idCollege = $idCollege;
		
		//semester
		$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
		$this->view->semester = $semesterDb->getData($idSemester);
		
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($idSubject);
		$this->view->subject = $subject;
		
	    $coordinatorDb = new GeneralSetup_Model_DbTable_CourseCoordinator();
		$this->view->coordinator = $coordinatorDb->getData($idSemester,$idSubject);
				
	}
	
	public function addCoordinatorAction(){
		
		$this->view->title = $this->view->translate("Add Coordinator");
		
		$auth = Zend_Auth::getInstance();
	
		$registry = Zend_Registry::getInstance();
		$this->view->locale = $registry->get('Zend_Locale');
		
		$idSubject = $this->_getParam('idSubject', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idCollege = $this->_getParam('idCollege', 0);
		
		$this->view->idSemester = $idSemester;
		$this->view->idSubject = $idSubject;
		$this->view->idCollege = $idCollege;
		
		//semester
		$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
		$this->view->semester = $semesterDb->getData($idSemester);
		
		//get Subject Info
		$subjectDb = new GeneralSetup_Model_DbTable_Subjectmaster();
		$subject = $subjectDb->getData($idSubject);
		$this->view->subject = $subject;
		
		//get list of lecturer under x yh control by faculty
		$staffDB = new GeneralSetup_Model_DbTable_Staffmaster();
		$staff_list = $staffDB->getAcademicStaffNotCoordinator(null,$idSemester,$idSubject);
		$this->view->staff_list = $staff_list;
		
		
		if ($this->getRequest()->isPost()) {
			
		     $formData = $this->getRequest()->getPost();		   	
		     
		     $coordinatorDb = new GeneralSetup_Model_DbTable_CourseCoordinator();
		     
		     $data['IdSemester'] = $formData['idSemester'];
		     $data['IdSubject'] = $formData['idSubject'];		     
		     $data['UpdDate'] = date("Y-m-d H:i:s");
		     $data['UpdUser'] = $auth->getIdentity()->id;
		    
		     for($i=0; $i<count($formData['idStaff']); $i++){
		     	
		     	$data['IdStaff'] = $formData['idStaff'][$i];
		     	$coordinatorDb->addData($data);
		     }
		     
		    $this->gobjsessionsis->flash = array('message' => 'Coordinator has been assigned successfully', 'type' => 'success');
		    $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'course-group', 'action'=>'coordinator-list','idSubject'=>$formData['idSubject'],'idSemester'=>$formData['idSemester'],'idCollege'=>$formData['idCollege']),'default',true));	
		}
	
	}
	
	public function delCoordinatorAction(){
		
		$idSubject = $this->_getParam('idSubject', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idCollege = $this->_getParam('idCollege', 0);
		$id = $this->_getParam('id', 0);
				
		$coordinatorDb = new GeneralSetup_Model_DbTable_CourseCoordinator();
		$coordinatorDb->deleteData($id);
		 		
		$this->gobjsessionsis->flash = array('message' => 'Remove coordinator success', 'type' => 'success');
		$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'course-group', 'action'=>'coordinator-list','idSubject'=>$idSubject,'idSemester'=>$idSemester,'idCollege'=>$idCollege),'default',true));
			
		
	}
	
	public function mlmspushAction(){
		/*if($this->_getParam('migrate', 0)==1){
			$moodledb = new GeneralSetup_Model_DbTable_Moodle();
			$sodb=new GeneralSetup_Model_DbTable_Subjectsoffered();
			$cgdb = new GeneralSetup_Model_DbTable_CourseGroup();
			$subjects = $moodledb->getAllSubject();
			$i=1;
			foreach($subjects as $subject){
				if($subject["idnumber"]==""){
					$subject["idnumber"]=$subject["shortname"];
				}				
				$idnumber = explode("/",$subject["idnumber"]);
				$idx = count($idnumber);

				if($idnumber[0]=="JAN15"){					
					
					//daptkan idstaff from username
					$usrdb = new GeneralSetup_Model_DbTable_User();
					$userdata=$usrdb->fngetIdStaff($subject["username"]);
					$idnumber[$idx+1]=$userdata["IdStaff"];
					$sinfo = $sodb->checkOfferedSem($idnumber[1],array(6,11));
					
					
					$newidnumber[0]=$sinfo["IdSemester"];
					$newidnumber[1]=$sinfo["IdSubject"];
					$newidnumber[2]=$userdata["IdStaff"];
					
					if($newidnumber[0]!=""){
						$newid = implode("/",$newidnumber);
						$data["idnumber"]=$newid;
						//$moodledb->update('mdl_course', $data, "idnumber = '".$subject["idnumber"]."'");
						echo "<pre>";echo($newid);echo"</pre>";
					}//
					$i++;
				}
			}
			$groups = $moodledb->getAllGroup();
			foreach ($groups as $group){
				if($group["cidnumber"]!="" && is_numeric($group["idnumber"])){
					$cidnumber = explode("/",$group["cidnumber"]);
					if(is_numeric($cidnumber[1])){
						$smsgrp = $cgdb->getInfo($group["idnumber"]);
						if($smsgrp["IdSubject"] == $cidnumber[1]){
							$data["idnumber"]=$group["idnumber"]."/".$cidnumber[1];
						}else{
							$data["idnumber"]=$group["idnumber"]."/D";
						}
						echo "<pre>";print_r($data);echo"</pre>";
						$moodledb->update('mdl_groups', $data, "id= '".$group["id"]."'");
					}
				}
			}
			
			$groups = $moodledb->getAllGrouping();
			foreach ($groups as $group){
				if($group["cidnumber"]!="" && is_numeric($group["idnumber"])){
					$cidnumber = explode("/",$group["cidnumber"]);
					if(is_numeric($cidnumber[1])){
						$smsgrp = $cgdb->getInfo($group["idnumber"]);
						if($smsgrp["IdSubject"] == $cidnumber[1]){
							$data["idnumber"]=$group["idnumber"]."/".$cidnumber[1];
						}else{
							$data["idnumber"]=$group["idnumber"]."/D";
						}
						echo "<pre>";print_r($data);echo"</pre>";
						$moodledb->update('mdl_groupings', $data, "id= '".$group["id"]."'");
					}
				}
			}			
			echo $i;
			exit;
			
		}*/
		$this->view->title = $this->view->translate("MLMS Sync - Course/Section");
		$idSubject = $this->_getParam('idSubject', 0);
		$idSemester = $this->_getParam('idSemester', 0);
		$idCollege = $this->_getParam('idCollege', 0);
		
		$this->view->idSubject = $idSubject;
		$this->view->idSemester = $idSemester;
		$this->view->idCollege = $idCollege;
		
		$semesterDb = new GeneralSetup_Model_DbTable_Semestermaster();
		$semester = $semesterDb->getData($idSemester);
		$activity = $semesterDb->getActivity($idSemester, 39);

	    $moodledb = new GeneralSetup_Model_DbTable_Moodle();
		
	    //Create semester in moodle via category
		$sem["name"] = $semester["SemesterMainCode"];
		$sem["idnumber"] = $idSemester;
		$sem["description"] = $semester["SemesterMainName"];
		$sem["descriptionformat"] = 1;
		$sem["parent"] = 0;
		$sem["sortorder"] = '90000';
		$sem["visible"] = 1;
		$sem["visibleold"] = 1;
		$sem["timemodified"] = time();
		$sem["depth"] = 1;
		
		$msemester = $moodledb->isSemesterExist($idSemester);
		if(!$msemester){
			//echo "takdak";
			Cms_MoodleLog::add('New Semester', json_encode($sem));
			$msemester = $moodledb->addSemester($sem);
		}else{			
			//echo "Semester already exist<hr>";
		}
		//echo $idSemester;
		//print_r($msemester);exit;
		//echo "SEMESTER DONE<hr>";
		$coursedb = new GeneralSetup_Model_DbTable_Subjectmaster();
		
		$subject = $coursedb->getData($idSubject);
		
		//dapatkan course info 
		//create course as category under semester in moodle
		$course["name"] = $subject["SubCode"];
		$course["idnumber"] = $idSemester."/".$idSubject;
		$course["description"] = $subject["SubjectName"];
		$course["descriptionformat"] = 1;
		$course["parent"] = $msemester["id"];
		$course["sortorder"] = '660000';
		$course["visible"] = 1;
		$course["visibleold"] = 1;
		$course["timemodified"] = time();
		$course["depth"] = 1;
		
		$msubject = $moodledb->isSubjectSemesterExist($idSemester,$idSubject);
		if(!$msubject){
			//echo "takdak $idSemester/$idSubject";
			Cms_MoodleLog::add('New Subject Semester', json_encode($course));
			$msubject = $moodledb->addSubjectSemester($course);
		}else{			
			//echo "Subject-Semester already exist<hr>";
		}
		
		//echo "Subject Done<hr>";
		//dapat lecturer dalam course section 
		$cgdb = new GeneralSetup_Model_DbTable_CourseGroup();
		$courseProgramDb = new GeneralSetup_Model_DbTable_CourseGroupProgram();
		
		$lecturers = $cgdb->getLectCourseGroup($idSubject,$idSemester);
		//var_dump($lecturers); exit;
		if(is_array($lecturers)){
			foreach ($lecturers as $lect){
				$lect["roleid"]=3; //moodle role id for lecturer
						
				$lcourse["category"]=$msubject["id"];
				$lcourse["fullname"]=$subject["SubjectName"];
				$lcourse["shortname"]=$subject["SubCode"]."-".$lect["FullName"];;
				$lcourse["idnumber"]=$idSemester."/".$idSubject."/".$lect["IdLecturer"];
				$lcourse["summary"]=$subject["SubjectName"];
				$lcourse["summaryformat"]=1;
				$lcourse["format"]='weeks';
				$lcourse["groupmode"] = 1;
    			$lcourse["groupmodeforce"] = 1;
				$lcourse["showgrades"]=1;
				$lcourse["maxbytes"] =52428800;
				$lcourse["showreports"] = 1;
				$lcourse["enablecompletion"] = 1;
				$lcourse["newsitems"]=5;
				//$lcourse["startdate"]=time();
				$lcourse["startdate"]=strtotime($activity['StartDate']);
				$lcourse["timecreated"]=time();
				$lcourse["timemodified"]=time();
				$lcourse["cacherev"]=time();
				
				$mcourse = $moodledb->isLectSubjectSemesterExist($idSemester,$idSubject,$lect["IdLecturer"]);
				if(!$mcourse){
					//echo "takdk";
					Cms_MoodleLog::add('New Lecturer Subject Semester', json_encode($lcourse));
					$mcourse = $moodledb->addLectSubSem($lcourse,$msubject);
					//echo "<pre>";print_r($mcourse);print_r($lect);echo "</pre>";exit;
				}else{			
					//echo "Moodle Course  already exist - By Lect<hr>";
				}	
				//echo "<pre>";print_r($mcourse);print_r($lect);echo "</pre>";//exit;
				//echo "Subject Lect Done<hr>";
				
				//Assign Lecture To moodle course
				$moodledb->assignUserToSubject($lect,$mcourse);
				//echo "Assign Lect Done<hr>";
				
				//Dapat list of group for this lecturer
				$grouplect = $cgdb->getGroupListLect($idSubject,$idSemester,$lect["IdLecturer"]);
				
				foreach ($grouplect as $key=>$group){
					//$grpprog = $courseProgramDb->getGroupData($grp['IdCourseTaggingGroup']);
					$grp = $moodledb->isGroupExist($group["IdCourseTaggingGroup"],$idSubject);
					if(!$grp){
						//echo "Moodle Group not exist";
						$moodledb->addGroup($grouplect);
						//assign lect to group
						
						$lectgroup = $moodledb->assignLectSingle($group);
					}else{
						//echo "Moodle Group  exist";
						//assign lect to group
						$lectgroup = $moodledb->assignLectSingle($group);
					}
				}
				
			}
		}
		
		
		//Get All assigned student  to subject
		$rgsdb = new Registration_Model_DbTable_Studentregsubjects();
		$studlist = $rgsdb->getRegStudents($idSubject,$idSemester,"","","yes");
		//echo "<pre>";print_r($studlist);
		//exit;
		$this->view->total = $moodledb->assignStudBatch($studlist);
		//echo "<pre>";print_r($studlist);echo "</pre>";
	}

	public function updateWeekMlmsAction(){
		exit;
		$model = new GeneralSetup_Model_DbTable_Moodle();
		$list = $model->getClass();
		var_dump($list);

		if ($list){
			foreach ($list as $loop){
				$cformat = $model->getCourseFormat($loop['id']);

				if (!$cformat){
					$data = array(
						'courseid'=>$loop['id'],
						'format'=>'weeks',
						'sectionid'=>0,
						'name'=>'coursedisplay',
						'value'=>0
					);
					$model->insertCourseFormat($data);

					$data2 = array(
						'courseid'=>$loop['id'],
						'format'=>'weeks',
						'sectionid'=>0,
						'name'=>'hiddensections',
						'value'=>0
					);
					$model->insertCourseFormat($data2);

					$data3 = array(
						'courseid'=>$loop['id'],
						'format'=>'weeks',
						'sectionid'=>0,
						'name'=>'numsections',
						'value'=>7
					);
					$model->insertCourseFormat($data3);
				}
			}
		}
		exit;
	}
}
?>