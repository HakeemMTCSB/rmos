<?php
class GeneralSetup_ExamCenterController extends Base_Base {
	
	public function init() {
		
	}
	
	public function indexAction() {
        $pageCount = 50;
		$this->view->title = $this->view->translate('Partner Setup');
		
		$session = Zend_Registry::get('sis');
		$this->view->defaultlanguage = $session->UniversityLanguage;
		
		$page_number = $this->_getParam('page',1); // Paginator instance
		
		$form = new GeneralSetup_Form_ExamCenter();
		$this->view->form = $form;
		
		
		$examcenterdb = new GeneralSetup_Model_DbTable_ExamCenter();
		
		//get exam center list
		if ($this->_request->isPost ()) {			
			$formData = $this->_request->getPost ();
			$this->view->examcenter_list = $examcenterdb->getDataInfo($formData);
		}else{
			$this->view->examcenter_list = $examcenterdb->getDataInfo();
		}

        $paginator = Zend_Paginator::factory($this->view->examcenter_list);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);

        $totalRecord = $paginator->getTotalItemCount();

        $this->view->paginator = $paginator;
        $this->view->totalRecord = $totalRecord;
		
 	}
 	
 	
 	public function addDataAction(){
 		
 		$this->view->title = $this->view->translate('Add Partner');
 		
 		$session = Zend_Registry::get('sis');
		$this->view->defaultlanguage = $session->UniversityLanguage;
		
 		$form = new GeneralSetup_Form_ExamCenter();
		$this->view->form = $form;
		
		if ($this->_request->isPost ()) {
			
			$auth = Zend_Auth::getInstance();
			$formData = $this->_request->getPost ();
			
			//save exam center info
			$data['ec_name']=$formData['ec_name'];
			$data['ec_short_name']='';
			$data['ec_code']=$formData['ec_code'];
            $data['ec_type']=$formData['ec_type'];
			$data['ec_default_language']=$formData['ec_default_language'];
			$data['ec_start_date']=($formData['ec_start_date'] == '' ? null:date('Y-m-d',strtotime($formData['ec_start_date'])));
			$data['ec_end_date']=($formData['ec_end_date'] == '' ? null:date('Y-m-d',strtotime($formData['ec_end_date'])));
			$data['ec_active']=$formData['ec_active'];
			$data['createddt']=date('Y-m-d H:i:s');
			$data['ec_coordinatorname']=$formData['ec_coordinatorname'];
			$data['ec_coordinatoremail']=$formData['ec_coordinatoremail'];
			$data['ec_coordinatorphone']=$formData['ec_coordinatorphone'];
			$data['ec_coordinatormobile']=$formData['ec_coordinatormobile'];
			$data['ec_coordinatorfax']= '';
			$data['ec_timezone'] = $formData['ec_timezone'];
			$data['ec_capacity'] = $formData['ec_capacity'];
			$data['createdby']=$auth->getIdentity()->iduser;
			
			$examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
			$ec_id = $examCenterDB->addData($data);
			
			//save address info
			$address['add_org_name'] = 'tbl_exam_center';
			$address['add_org_id'] = $ec_id;
	        $address['add_address_type'] = $formData['AddressType'];
			$address['add_country'] = $formData['ec_country'];
			$address['add_state'] = $formData['ec_state'];
			$address['add_city'] = $formData['ec_city'];
			$address['add_address1'] = $formData['ec_address1'];
			$address['add_address2'] = $formData['ec_address2'];
			$address['add_zipcode'] = $formData['ec_zipcode'];
			$address['add_phone'] = $formData['ec_phone'];
			$address['add_email'] = $formData['ec_email'];			
			$address['add_createddt'] = date('Y-m-d H:i:s');
			$address['add_createdby'] = $auth->getIdentity()->iduser;	
			$address['add_state_others']=$formData['State_Others'];
			$address['add_city_others']=$formData['City_Others'];
			$address['add_fax'] = $formData['ec_fax'];
			
	        $addressDb = new GeneralSetup_Model_DbTable_Address();
	        $addressDb->addData($address);
	        
			
	        $this->_redirect($this->baseUrl . '/generalsetup/exam-center/index');
			
 		}
 	}
 	
	public function editDataAction(){
 		
		$this->view->title = $this->view->translate('Edit Partner');
		
		$session = Zend_Registry::get('sis');
		$this->view->defaultlanguage = $session->UniversityLanguage;
		
		$ec_id = $this->_getParam('id');
		$this->view->ec_id = $ec_id;
		 
		//get exam center info
		$examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
		$exam_center = $examCenterDB->getDatabyId($ec_id);
		 
 		$form = new GeneralSetup_Form_ExamCenter();
 		$form->populate($exam_center);
		$this->view->form = $form;
		
		if ($this->_request->isPost ()) {
			
			$auth = Zend_Auth::getInstance();
			$formData = $this->_request->getPost ();

			//save exam center info
			$data['ec_name']=$formData['ec_name'];
			$data['ec_short_name']='';
			$data['ec_code']=$formData['ec_code'];
            $data['ec_type']=$formData['ec_type'];
			$data['ec_default_language']=$formData['ec_default_language'];
			$data['ec_start_date']=($formData['ec_start_date']!='') ? date('Y-m-d',strtotime($formData['ec_start_date'])):null;
			$data['ec_end_date']= ($formData['ec_end_date']!='') ? date('Y-m-d',strtotime($formData['ec_end_date'])):null;
			$data['ec_active']=$formData['ec_active'];
			$data['createddt']=date('Y-m-d H:i:s');
			$data['ec_coordinatorname']=$formData['ec_coordinatorname'];
			$data['ec_coordinatoremail']=$formData['ec_coordinatoremail'];
			$data['ec_coordinatorphone']=$formData['ec_coordinatorphone'];
			$data['ec_coordinatormobile']=$formData['ec_coordinatormobile'];
			$data['ec_coordinatorfax']=$formData['ec_coordinatorfax'];
			$data['ec_timezone'] = $formData['ec_timezone'];
			$data['ec_capacity'] = $formData['ec_capacity'];
			
			
			
			$examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
			$examCenterDB->updateData($data, $formData['ec_id']);

			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

			
			$this->_redirect($this->baseUrl . '/generalsetup/exam-center/edit-data/id/'.$formData['ec_id']);
		}
 	}
 	
	public function deleteDataAction(){
 		$id = $this->_getParam('id', 0);
 		
 		$examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
		$examCenterDB->deleteData($id);
		
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		$addressDb->deleteOrgAddress($id,'tbl_exam_center');
		
		$this->_redirect( $this->baseUrl . '/generalsetup/exam-center');
 	}
 	
	public function examCenterAddressAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_ec = $this->_getParam('id', 0);		
		$this->view->id_ec = $id_ec;
		
		//get list branch address
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		$this->view->address_list = $addressDb->getDataByOrgId($id_ec,'tbl_exam_center');		
		
	}
	
	public function addAddressAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_ec = $this->_getParam('id', 0);			
		
		$form= new GeneralSetup_Form_AddressForm(array('idBranch'=>$id_ec));
		$this->view->addressForm = $form;
		
		if ($this->_request->isPost ()) {			
		
			$formData = $this->_request->getPost ();
			
			if ($form->isValid ( $formData )) {
				
				//save address				
				$auth = Zend_Auth::getInstance();
				
				$data['add_org_name'] = 'tbl_exam_center';
				$data['add_org_id'] = $formData['idBranch']; //is refer to id department. Use the same form as branch
				$data['add_address_type'] = $formData['address_type'];
				$data['add_country'] = $formData['country'];
				$data['add_state'] = $formData['state'];
				$data['add_address1'] = $formData['address1'];
				$data['add_address2'] = $formData['address2'];
				$data['add_zipcode'] = $formData['zipcode'];
				$data['add_phone'] = $formData['phone'];
				$data['add_email'] = $formData['email'];
				$data['add_createddt'] = date('Y-m-d H:i:s');
				$data['add_createdby'] = $auth->getIdentity()->iduser;	
				$data['add_city'] = $formData['city'];
				$data['add_state_others'] = $formData['state_others'];
				$data['add_city_others'] = $formData['city_others'];
				
				$addressDb = new GeneralSetup_Model_DbTable_Address();
				$addressDb->addData($data);
				
				$this->_redirect( $this->baseUrl . '/generalsetup/exam-center/edit-data/id/'.$formData['idBranch']);
			}
		}
	}
	
	public function editAddressAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_ec = $this->_getParam('id', 0);		
		$add_id = $this->_getParam('add_id', 0);
						
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		
		$form= new GeneralSetup_Form_AddressForm(array('idBranch'=>$id_ec,'addId'=>$add_id));

		//add fax field
		$form->zipcode->setRequired(false);
		$form->addElement('text','fax', array(
			'label'=>$this->view->translate('Fax'),
			'class'=>'input-txt',
			'required'=>false	    
		));
		
		if ($this->_request->isPost ()) {			
		
			$formData = $this->_request->getPost ();
				
			if ($form->isValid ( $formData )) {
			
				
				$auth = Zend_Auth::getInstance();
				
				//get old (address before change)				
				$address = $addressDb->getDatabyId($formData['add_id']);
				
				//insert into history
				$olddata['add_id'] = $address['add_id'];
				$olddata['addh_org_name'] = $address['add_org_name'];
				$olddata['addh_org_id'] = $address['add_org_id'];
				$olddata['addh_address_type'] = $address['add_address_type'];
				$olddata['addh_country'] = $address['add_country'];
				$olddata['addh_state'] = $address['add_state'];
				$olddata['addh_address1'] = $address['add_address1'];
				$olddata['addh_address2'] = $address['add_address2'];
				$olddata['addh_zipcode'] = $address['add_zipcode'];
				$olddata['addh_phone'] = $address['add_phone'];
				$olddata['addh_email'] = $address['add_email'];
				$olddata['addh_createddt'] = $address['add_createddt'];
				$olddata['addh_createdby'] = $address['add_createdby'];	
				$olddata['addh_changedt'] = date('Y-m-d H:i:s');
				$olddata['addh_changeby'] = $auth->getIdentity()->iduser;
				$olddata['addh_city'] = $address['add_city'];
				$olddata['addh_state_others'] = $address['add_state_others'];
				$olddata['addh_city_others'] = $address['add_city_others'];
				$olddata['addh_fax'] = $address['add_fax'];

				$addHistoryDb = new GeneralSetup_Model_DbTable_AddressHistory();	
				$addHistoryDb->addData($olddata);
				
				//edit
				$data['add_address_type'] = $formData['address_type'];
				$data['add_country'] = $formData['country'];
				$data['add_state'] = $formData['state'];
				$data['add_address1'] = $formData['address1'];
				$data['add_address2'] = $formData['address2'];
				$data['add_zipcode'] = $formData['zipcode'];
				$data['add_phone'] = $formData['phone'];
				$data['add_email'] = $formData['email'];
				$data['add_modifydt'] = date('Y-m-d H:i:s');
				$data['add_modifyby'] = $auth->getIdentity()->iduser;
				$data['add_city'] = $formData['city'];
				$data['add_state_others'] = $formData['state_others'];
				$data['add_city_others'] = $formData['city_others'];
				$data['add_fax'] = $formData['fax'];
				
				$addressDb->updateData($data,$formData['add_id']);

				$this->_helper->flashMessenger->addMessage(array('success' => "Address updated"));
				
				$this->_redirect( $this->baseUrl . '/generalsetup/exam-center/edit-data/id/'.$formData['idBranch'].'#tab2');
				
			}
			
		}else{
			
			
			$add = $addressDb->getDatabyId($add_id);
			
			//get state to populate state from country
			$stateDB = new App_Model_General_DbTable_State();
			$state_data = $stateDB->getState($add['add_country']);		
			$element = $form->getElement('state');
			foreach($state_data as $s){
			$element->addMultiOption($s['idState'],$s['StateName']);
			}
		
			$address_data['address_type']=$add['add_address_type'];
			$address_data['country']=$add['add_country'];
			$address_data['state']=$add['add_state'];
			$address_data['address1']=$add['add_address1'];
			$address_data['address2']=$add['add_address2'];
			$address_data['zipcode']=$add['add_zipcode'];
			$address_data['phone']=$add['add_phone'];
			$address_data['email']=$add['add_email'];
			$address_data['city']=$add['add_city'];
			$address_data['state_others']=$add['add_state_others'];
			$address_data['city_others']=$add['add_city_others'];
			$address_data['fax'] = $add['add_fax'];
			//print_r($address_data);

			$this->view->address_data = $address_data;
		
			$form->populate($address_data);
			$this->view->addressForm = $form;		
		}
		
	}
	
	public function deleteAddressAction(){
		
		$id_ec = $this->_getParam('id', 0);		
		$add_id = $this->_getParam('add_id', 0);


		$addressDb = new GeneralSetup_Model_DbTable_Address();
		$addressDb->deleteData($add_id);

        $this->_helper->flashMessenger->addMessage(array('success' => "Address deleted"));

		$this->_redirect( $this->baseUrl . '/generalsetup/exam-center/edit-data/id/'.$id_ec);

	}
	
	public function viewHistoryAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_ec = $this->_getParam('id', 0);
		
		//get list branch address
		$addressDb = new GeneralSetup_Model_DbTable_AddressHistory();
		$this->view->address_list = $addressDb->getDataByOrgId($id_ec,'tbl_exam_center');		
	}
	
	
}
?>