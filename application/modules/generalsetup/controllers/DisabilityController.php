<?php
class GeneralSetup_DisabilityController extends Base_Base {

    public function indexAction()
    {

        $this->view->title = "Disability Setup";

        $form = new GeneralSetup_Form_Disability();

        $disableDB= new GeneralSetup_Model_DbTable_Disability();
        $query = $disableDB->getDisableData();

        $pageCount = 25;

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);
            $query = $disableDB->getDisableData($formData);

            $this->gobjsessionsis->raceQuery = $query;
            $form->populate($formData);
            $this->_redirect($this->baseUrl . '/generalsetup/disability/index/search/1');
        }

        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->raceQuery);
        }else{
            $query = $this->gobjsessionsis->raceQuery;
        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);
        $totalRecord = $paginator->getTotalItemCount();

        $this->view->form = $form;
        $this->view->totalRecord = $totalRecord;
        $this->view->paginator = $paginator;
    }

    public function addAction()
    {
        $this->view->title = "Add Disability";

        $form = new GeneralSetup_Form_DisabilitySubmit();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $disabilityDb = new GeneralSetup_Model_DbTable_Disability();

                $data = array(
                    'disable_id'           => $formData['disable_id'],
                    'disable_desc'         => $formData['disable_desc'],
                    'disable_desc_eng'     => $formData['disable_desc_eng'],
                    'disable_condition'    => $formData['disable_condition'],
                    'active'               => $formData['active'],
                );

                $disabilityDb->insert($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully added"));

                $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'disability', 'action' => 'index'), 'default', true));

            } else {
                $form->populate($formData);
            }
        }

        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->_getParam('id', null);

        if ($id == null) {
            $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'disability', 'action' => 'index'), 'default', true));
        }

        $this->view->title = "Edit Disability";

        $form = new GeneralSetup_Form_DisabilitySubmit();

        $disabilityDb = new GeneralSetup_Model_DbTable_Disability();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $data = array(
                    'disable_id'           => $formData['disable_id'],
                    'disable_desc'         => $formData['disable_desc'],
                    'disable_desc_eng'     => $formData['disable_desc_eng'],
                    'disable_condition'    => $formData['disable_condition'],
                    'active'               => $formData['active'],
                );

                $disabilityDb->update($data, array('id=?' => $id));

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully updated"));

                $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'disability', 'action' => 'index'), 'default', true));

            } else {
                $form->populate($formData);
            }
        } else {
            $data = $disabilityDb->fetchRow(array('id=?' => $id))->toArray();

            $form->populate($data);
        }

        $this->view->form = $form;
    }
 
}