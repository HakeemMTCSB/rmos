<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class GeneralSetup_HighschoolSubjectController extends Zend_Controller_Action {
	
	private $_DbObj;
	
	public function init(){
        $this->fnsetObj();
		$this->_DbObj = new GeneralSetup_Model_DbTable_SchoolSubject();
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        Zend_Form::setDefaultTranslator($this->view->translate);
	}

    public function fnsetObj(){
        $this->lobjform = new App_Form_Search ();
        $this->lobjHighschoolSubjectForm = new GeneralSetup_Form_HighschoolSubject();
        $this->lobjEditSchoolSubject = new GeneralSetup_Form_EditSchoolSubject();
        $this->lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
        $this->lobjqualificationsubmap = new Application_Model_DbTable_QualificationsubjectMapping();
        $this->lobjRegistryvalue = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $this->lobjQua = new Application_Model_DbTable_Qualificationsetup();

    }

	
	public function indexAction() {
		//title
    	$this->view->title= $this->view->translate("Subject Set-up");
    	
    	$this->view->searchForm = new GeneralSetup_Form_SchoolSubject();
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			//paginator
			$data = $this->_DbObj->getPaginateData($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
			$paginator->setItemCountPerPage(PAGINATION_SIZE);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
            $totalRecord = $paginator->getTotalItemCount();
			
			$this->view->paginator = $paginator;
            $this->view->totalRecord = $totalRecord;
		
    	}else{
    		//paginator
			$data = $this->_DbObj->getPaginateData();
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
			$paginator->setItemCountPerPage(PAGINATION_SIZE);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
            $totalRecord = $paginator->getTotalItemCount();
			
			$this->view->paginator = $paginator;
            $this->view->totalRecord = $totalRecord;
    	}
    	
	}


	public function detailAction(){
    	$this->view->title= $this->view->translate("Subject Set-up")." - ".$this->view->translate("Detail");

        $this->view->searchForm = new GeneralSetup_Form_SchoolSubject();
    	
		$id = $this->_getParam('id', null);
		$this->view->id = $id;


    	if($id) {
            //subject data
            $result = $this->_DbObj->getData($id);
            $this->view->paginator = array($result);

            if ($this->getRequest()->isPost()) {

                $formData = $this->getRequest()->getPost();

                //paginator
                $data = $this->_DbObj->getPaginateData($formData);

                $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
                $paginator->setItemCountPerPage(PAGINATION_SIZE);
                $paginator->setCurrentPageNumber($this->_getParam('page',1));
                $totalRecord = $paginator->getTotalItemCount();

                $this->view->paginator = $paginator;
                $this->view->totalRecord = $totalRecord;


            }
    					
		}

		else{
			$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'highschool-subject', 'action'=>'index'),'default',true));
		}
    }
	
	public function addoldAction()
    {
    	//title
    	$this->view->title= $this->view->translate("Subject Set-up")." - ".$this->view->translate("Add");
    	    	
    	$form = new GeneralSetup_Form_SchoolSubject();
    	
    	$form->cancel->onClick = "window.location ='".$this->view->url(array('module'=>'generalsetup','controller'=>'highschool-subject', 'action'=>'index'),'default',true)."'; return false;";
    	    	
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				
				$id = $this->_DbObj->addData($formData);
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'highschool-subject', 'action'=>'detail', 'id'=>$id),'default',true));	
			}else{
				$form->populate($formData);
			}
        	
        }
    	
        $this->view->form = $form;
    }

    public function addAction() {


        $this->view->lobjHighschoolSubjectForm = $this->lobjHighschoolSubjectForm;
        ///$lobjcountry = $this->lobjUser->fnGetCountryList();
        ///$this->lobjHighschoolSubjectForm->Country->addMultiOptions($lobjcountry);
        ///$lobjinstype = $this->lobjRegistryvalue->getList('7');
        ///$lobjinstype = $this->lobjUser->fnGetInstitutionType();
        ///$this->lobjHighschoolSubjectForm->QualificationType->addMultiOptions($lobjinstype);
        $ldtsystemDate = date ( 'Y-m-d H:i:s' );
        $this->view->lobjHighschoolSubjectForm->UpdDate->setValue ( $ldtsystemDate );
        $auth = Zend_Auth::getInstance();
        $this->view->lobjHighschoolSubjectForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

        $this->view->lobjHighschoolSubjectForm->ss_core_subject->setValue ( '0' );

        $this->view->lobjHighschoolSubjectForm->ss_subject->addValidator('Db_NoRecordExists', true, array(
                'table' => 'school_subject',
                'field' => 'ss_subject'
            )
        );
        $this->view->lobjHighschoolSubjectForm->ss_subject->getValidator('Db_NoRecordExists')->setMessage("Record already exists");

        $this->view->lobjHighschoolSubjectForm->ss_subject_code->addValidator('Db_NoRecordExists', true, array(
                'table' => 'school_subject',
                'field' => 'ss_subject_code'
            )
        );
        $this->view->lobjHighschoolSubjectForm->ss_subject_code->getValidator('Db_NoRecordExists')->setMessage("Record already exists");


        if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
            $larrformData = $this->_request->getPost (); //getting the values of lobjInstitutionsetupForm data from post
            //dd($larrformData);
            //exit;
            unset ( $larrformData ['Save'] );
            if ($this->lobjHighschoolSubjectForm->isValid ( $larrformData )) {

                //$IdInstitution = $this->lobjHighschoolSubjectForm->fnaddInstitution($larrformData); //instance for adding the lobjInstitutionsetupForm values to DB

                $schoolsubMasterDb = new GeneralSetup_Model_DbTable_SchoolSubject();
                $id = $schoolsubMasterDb->addData($larrformData);

                $rowcount = count($larrformData['IdSubject']);



                    for ($i = 0; $i < $rowcount; $i++) {
                        $mapping = array();
                        $mapping['IdSubject'] = $id;
                        $mapping['IdQualification'] = $larrformData['IdSubject'][$i];
                        if ($larrformData['IdSubject'][$i]!='0') {
                            $this->lobjqualificationsubmap->fnadd($mapping);
                        }
                    }


                // Write Logs
                $priority=Zend_Log::INFO;
                $larrlog = array ('user_id' => $auth->getIdentity()->iduser,
                    'level' => $priority,
                    'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'time' => date ( 'Y-m-d H:i:s' ),
                    'message' => 'School Subject Add',
                    'Description' =>  Zend_Log::DEBUG,
                    'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
                $this->_gobjlog->write ( $larrlog ); //insert to tbl_log

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully added"));
                $this->_redirect( $this->baseUrl . '/generalsetup/highschool-subject/index');
            }
        }
    }
    
	public function editAction(){

        $this->view->lobjEditSchoolSubject = $this->lobjEditSchoolSubject;


        //$this->view->form = new GeneralSetup_Form_EditSchoolSubject();

        //$this->view->lobjEditSchoolSubject = $this->lobjEditSchoolSubject;

        //$lobjquatype = $this->lobjQua->fngetQualificationList();
        //$this->lobjEditSchoolSubject->Qualification->addMultiOptions($lobjquatype);

		$id = $this->_getParam('id', 0);
        $this->view->id = $id;
		
		//title
    	$this->view->title= $this->view->translate("Subject Set-up")." - ".$this->view->translate("Edit");
    	
    	//$form = new GeneralSetup_Form_EditSchoolSubject();

       /* $lobjqua = new Application_Model_DbTable_Qualificationsetup();
        $lobjqualist = $lobjqua->fngetQualificationList();
        $this->view->lobjqualist = $lobjqualist;*/


    	if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($this->lobjEditSchoolSubject->isValid($formData)) {
                //dd($formData);
                // exit;

                $idsub = $formData["idsub"];

                $this->_DbObj->updateData($formData, $idsub);
                $this->lobjqualificationsubmap->fndeletemappings($idsub);

                $rowcount = count($formData['IdSubject']);

                for ($i = 0; $i < $rowcount; $i++) {
                    $mapping = array();
                    $mapping['IdSubject'] = $idsub;
                    $mapping['IdQualification'] = $formData['IdSubject'][$i];

                    //dd($mapping);
                    //exit;


                    //if ($formData['IdSubject']!='0' || $formData['IdSubject']!='') {
                    if (!empty($formData['IdSubject'])) {


                        $this->lobjqualificationsubmap->fnadd($mapping);
                    }
                }


                $this->_redirect($this->view->url(array('module' => 'generalsetup', 'controller' => 'highschool-subject', 'action' => 'detail', 'id' => $idsub), 'default', true));
                /*}else{
                    $this->lobjEditSchoolSubject->populate($formData); */
                }
            } else {
                if ($id > 0) {

                    //$form->populate($this->_DbObj->getData($id));
                    //$this->view->acadecmisetup = $larrresult;
                    $this->lobjEditSchoolSubject->populate($this->_DbObj->getData($id));

                    $quaresultDetails = $this->lobjqualificationsubmap->fngetmappings($id);
                    $this->view->quaresultDetails = $quaresultDetails;
                }
            }

    	//$this->view->form = $form;
    }
    
	public function deleteAction($id = null){
    	$id = $this->_getParam('id', 0);
    	
    	if($id>0){
    		$db = new GeneralSetup_Model_DbTable_SchoolSubject();
    		$db->deleteData($id);
    	}
    		
    	$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'highschool-subject', 'action'=>'index'),'default',true));
    	
    }
}

