<?php

class GeneralSetup_MenuController extends Base_Base {
	
	public function init(){
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
	}
	
	public function indexAction(){
		
		
		
		$this->view->title = $this->view->translate("Navigation Setup - Role Selection");
		
		$definitionDb = new App_Model_Definitiontype();
		
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		$roleList = $definitionDb->fnGetDefinations('Role');
		$this->view->paginator = $this->lobjCommon->fnPagination($roleList,$lintpage,$lintpagecount);	
	}
	
	public function menuAction(){
		
		$migrateDb = new GeneralSetup_Model_DbTable_RoleResourcesNav();
		//$migrateDb->migrate();
		
		$this->view->title = $this->view->translate("Menu Setup - Top Menu List");		
		
		$topMenuDb = new GeneralSetup_Model_DbTable_TopMenu();
		$this->view->menuList = $topMenuDb->getMenuList();	
				
	}
	
	public function addMenuAction(){
		
		
		
		$this->view->title = $this->view->translate("Navigation Setup - Top Menu Navigation : ADD");
			
		$this->view->backURL = $this->view->url(array('module'=>'generalsetup','controller'=>'menu', 'action'=>'menu'),'default',true);
		
		$form = new GeneralSetup_Form_TopMenu();
		
		$form->cancel->onClick = "window.location ='".$this->view->backURL."'; return false; ";
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData)) {
				$menuDb = new GeneralSetup_Model_DbTable_TopMenu();
				
				$data = array(							
							'tm_name' => $formData['tm_name'],
							'tm_desc' => $formData['tm_desc'],
							'tm_module' => $formData['tm_module'],
							'tm_controller' => $formData['tm_controller'],
							'tm_action' => $formData['tm_action'],
							'tm_seq_order' => $formData['tm_seq_order']							
						);
				
				$menuDb->insert($data);
				
				//redirect
				$this->_redirect($this->view->backURL);	
			}else{
				$form->populate($formData);
			}
			
		}
		
		$this->view->form = $form;
	}
	
	public function menuAjaxAction(){
		
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	
	    
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

        $navigationMenuDb = new GeneralSetup_Model_DbTable_TopMenu();
		$menuList = $navigationMenuDb->getMenuList();
		
		$json = Zend_Json::encode($menuList);
		
		echo $json;
		exit();
	}
	
	public function editMenuAction(){		
		
		$menu_id = $this->_getParam('id',null);
		
		$menuDb = new GeneralSetup_Model_DbTable_TopMenu();
		
		$data = $menuDb->getData($menu_id);
		
		$form = new GeneralSetup_Form_TopMenu();
		
		$this->view->title = $this->view->translate("Navigation Setup - Edit Top Menu");
		$this->view->backURL = $this->view->url(array('module'=>'generalsetup','controller'=>'menu', 'action'=>'menu'),'default',true);
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData)) {

                $data = [
                    'tm_name'       => $formData['tm_name'],
                    'tm_desc'       => $formData['tm_desc'],
                    'tm_module'     => $formData['tm_module'],
                    'tm_controller' => $formData['tm_controller'],
                    'tm_action'     => $formData['tm_action'],
                    'tm_seq_order'  => $formData['tm_seq_order'] ?? 0,
                    'tm_visibility' => $formData['tm_visibility'] ?? 0
                ];

				$menuDb->update($data,'tm_id = '.$menu_id);
				
				//redirect
				$this->redirect($this->view->backURL);
			}else{
				$form->populate($formData);
			}
			
		}else{
			$form->populate($data);
		}
        
		$this->view->form = $form;
	}
	
	public function deleteMenuAction(){
		
		$menu_id = $this->_getParam('id',null);
		
		$menuDb = new GeneralSetup_Model_DbTable_TopMenu();
		$menuDb->deleteData($menu_id);
		
		$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'menu', 'action'=>'menu'),'default',true));
	}
	
	public function moveMenuAction(){
		
		$direction = $this->_getParam('direction',null);		
		$menu_id = $this->_getParam('menu_id',null);
		$total = $this->_getParam('total',null);	
		$sidebar_id = $this->_getParam('sidebar_id',null);	
		
		$topMenuDb = new GeneralSetup_Model_DbTable_TopMenu();
		$sidebarDb = new GeneralSetup_Model_DbTable_SidebarMenu();
		
				
		if($direction=='up'){
			
			//check seq for current id
			$menu_info = $topMenuDb->getData($menu_id);
			
			if($menu_info['tm_seq_order']==1){
				return false;
			}else{
				$seq_order = $menu_info['tm_seq_order']-1;
							
				//update above selected seq to +1 from current seq
				$topMenuDb->update(array('tm_seq_order'=>$seq_order+1),"tm_seq_order='$seq_order'");
				
				//update sequence for selected order minus -1 from current seq
				$topMenuDb->update(array('tm_seq_order'=>$menu_info['tm_seq_order']-1),"tm_id='$menu_id'");
			}
								
			
		}else
		if($direction=='down'){

			
			//check seq for current id
			$menu_info = $topMenuDb->getData($menu_id);
		
			if($menu_info['tm_seq_order']==$total){
				return false;
			}else{
				$seq_order = $menu_info['tm_seq_order']+1;
							
				//update below selected seq to -1 from current seq
				$topMenuDb->update(array('tm_seq_order'=>$seq_order-1),"tm_seq_order='$seq_order'");
				
				//update sequence for selected order add +1 from current seq
				$topMenuDb->update(array('tm_seq_order'=>$menu_info['tm_seq_order']+1),"tm_id='$menu_id'");
			}
			
		}else 
		if($direction=='s_up'){
			
			//check seq for current id
			$menu_info = $sidebarDb->getData($sidebar_id);
						
			if($menu_info['sm_parentid']==0){ //parent
			
				if($menu_info['sm_seq_order']==1){
					return false;
				}else{
					$seq_order = $menu_info['sm_seq_order']-1;
								
					//update above selected seq to +1 from current seq
					$sidebarDb->update(array('sm_seq_order'=>$seq_order+1),"sm_seq_order='$seq_order' AND sm_parentid=0");
					
					//update sequence for selected order minus -1 from current seq
					$sidebarDb->update(array('sm_seq_order'=>$menu_info['sm_seq_order']-1),"sm_id='$sidebar_id'");
				}
				
			}else{ //child
				
				if($menu_info['sm_seq_order']==1){
					return false;
				}else{
					$seq_order = $menu_info['sm_seq_order']-1;
					$parentid = $menu_info['sm_parentid'];
								
					//update above selected seq to +1 from current seq
					$sidebarDb->update(array('sm_seq_order'=>$seq_order+1),"sm_seq_order='$seq_order' AND sm_parentid='$parentid'");
					
					//update sequence for selected order minus -1 from current seq
					$sidebarDb->update(array('sm_seq_order'=>$menu_info['sm_seq_order']-1),"sm_id='$sidebar_id'");
				}
			
			}
							
			
		}else
		if($direction=='s_down'){
			
			//check seq for current id
			$menu_info = $sidebarDb->getData($sidebar_id);
						
			if($menu_info['sm_parentid']==0){ //parent
			
				if($menu_info['sm_seq_order']==$total){
					return false;
				}else{
					$seq_order = $menu_info['sm_seq_order']+1;
								
					//update below selected seq to -1 from current seq
					$sidebarDb->update(array('sm_seq_order'=>$seq_order-1),"sm_seq_order='$seq_order' AND sm_parentid=0");
					
					//update sequence for selected order add +1 from current seq
					$sidebarDb->update(array('sm_seq_order'=>$menu_info['sm_seq_order']+1),"sm_id='$sidebar_id'");
				}
				
			}else{ //child
			
				//get total child 
				$child = $sidebarDb->getChildSidebar($menu_info['sm_parentid']);
							
				if($menu_info['sm_seq_order']==count($child)){
					return false;
				}else{
					$seq_order = $menu_info['sm_seq_order']+1;
					$parentid = $menu_info['sm_parentid'];
					
					//update below selected seq to -1 from current seq
					$sidebarDb->update(array('sm_seq_order'=>$seq_order-1),"sm_seq_order='$seq_order' AND sm_parentid='$parentid'");
					
					//update sequence for selected order add +1 from current seq
					$sidebarDb->update(array('sm_seq_order'=>$menu_info['sm_seq_order']+1),"sm_id='$sidebar_id'");
				}
			
			}							
			
		}
				
		exit;
	}

	public function downloadAction()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

		$menu_id = $this->_getParam('id',0);
		$navigationMenuDb = new GeneralSetup_Model_DbTable_TopMenu();
		$sidebarDb = new GeneralSetup_Model_DbTable_SidebarMenu();

		$parent = $navigationMenuDb->getData($menu_id);
		$child = $sidebarDb->getSidebarList($menu_id);

		$output = array('menu'		=> $parent,
						'submenu'	=> $child);

		$json = Zend_Json::encode($output);
		
		//header('Content-Type: application/txt');
		header('Content-disposition: attachment; filename='.$parent['tm_name'].'.ini');
		//header('Content-Length: ' . filesize($file));

		echo $json;
		exit();
	}

	public function uploadAction()
	{
		$this->view->title = "Menu Setup - Upload Menu";

		if ($this->_request->isPost() && $this->_request->getPost('save')) 
		{
			$larrformData = $this->_request->getPost();
			
			$total = 0;
			
			$this->uploadDir = DOCUMENT_PATH.'/menu';


			try 
			{
				if ( !is_dir( $this->uploadDir ) )
				{
					if ( mkdir_p($this->uploadDir) === false )
					{
						throw new Exception('Cannot create document folder ('.$this->uploadDir.')');
					}
				}

				$adapter = new Zend_File_Transfer_Adapter_Http();
				
			
				$files = $adapter->getFileInfo();
				$adapter->addValidator('NotExists', false, $this->uploadDir );
				$adapter->setDestination( $this->uploadDir );
				$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
				$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

				foreach ($files as $no => $fileinfo) 
				{
					$adapter->addValidator('Extension', false, array('extension' => 'ini', 'case' => false));
				
					if ($adapter->isUploaded($no))
					{
						$ext = getext($fileinfo['name']);
						$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
						
						$fileUrl = $this->uploadDir.'/'.$fileName;

						$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
						$adapter->setOptions(array('useByteString' => false));
						
						$size = $adapter->getFileSize($no);
					
						if( !$adapter->receive($no))
						{
							$errmessage = array();
							if ( is_array($adapter->getMessages() ))
							{	
								foreach(  $adapter->getMessages() as $errtype => $errmsg )
								{
									$errmessage[] = $errmsg;
								}
								
								throw new Exception(implode('<br />',$errmessage));
							}
						}

						//PROCESS INI
						$data = file_get_contents($fileUrl);

						$results = json_decode($data, true);

						if ( empty($results) )
						{
							throw new Exception('Invalid Menu File');
						}
						else
						{
							//insert into tbl_top_menu the parent
							$db = getDB();
							
							$data_parent = array(
													'tm_name' => $results['menu']['tm_name'],
													'tm_desc' => $results['menu']['tm_desc'], 
													'tm_module' => $results['menu']['tm_module'], 
													'tm_controller' => $results['menu']['tm_controller'], 
													'tm_action' => $results['menu']['tm_action'], 
													'tm_seq_order' => $results['menu']['tm_seq_order'], 
													'tm_visibility' => $results['menu']['tm_visibility']
											);
							
							
							
							$db->insert("tbl_top_menu", $data_parent);
							$menu_id = $db->lastInsertId();

							//loop through the children and insert into tbl_sidebar_menu
							foreach ( $results['submenu'] as $children )
							{
								$subdata = array(
													'sm_tm_id' => $menu_id,
													'sm_name' => $children['sm_name'],
													'sm_desc' => $children['sm_desc'],
													'sm_parentid' => $children['sm_parentid'],
													'sm_module' => $children['sm_module'],
													'sm_controller' => $children['sm_controller'],
													'sm_action' => $children['sm_action'],
													'sm_seq_order' => $children['sm_seq_order'],
													'sm_visibility' => $children['sm_visibility']
												);
								$db->insert("tbl_sidebar_menu", $subdata);
								$submenu_id = $db->lastInsertId();

								if ( is_array($children['child']) && !empty($children['child']) )
								{
									foreach ( $children['child'] as $subchild ) 
									{
										$subdata = array(
													'sm_tm_id' => $menu_id,
													'sm_name' => $subchild['sm_name'],
													'sm_desc' => $subchild['sm_desc'],
													'sm_parentid' => $submenu_id,
													'sm_module' => $subchild['sm_module'],
													'sm_controller' => $subchild['sm_controller'],
													'sm_action' => $subchild['sm_action'],
													'sm_seq_order' => $subchild['sm_seq_order'],
													'sm_visibility' => $subchild['sm_visibility']
												);

										$db->insert("tbl_sidebar_menu", $subdata);
									}
								}
							}
						}

						$this->_helper->flashMessenger->addMessage(array('success' => "Menu successfully imported"));

					} //isuploaded
					
				} //foreach

				
				
				$this->_redirect('/generalsetup/menu/menu');	

			}
			catch (Exception $e) 
			{
				throw new Exception( $e->getMessage() );
			}

			

		} // if 

	}


	
	
	public function sidebarAction(){
		$this->view->title = $this->view->translate("Menu Setup - Sidebar Menu");
		
				
		$menu_id = $this->_getParam('menu_id',0);
		$this->view->menu_id = $menu_id;
		
		if($menu_id==0){
			$this->_redirect( $this->view->url(array('module'=>'generalsetup','controller'=>'menu', 'action'=>'menu'),'default',true) );
		}else{
			
			$navigationMenuDb = new GeneralSetup_Model_DbTable_TopMenu();
			$this->view->menu = $navigationMenuDb->getData($menu_id);

			$sidebarDb = new GeneralSetup_Model_DbTable_SidebarMenu();
			$this->view->sidebarList = $sidebarDb->getSidebarList($menu_id);

		}
	}
	
	public function sidebarAjaxAction(){
		
		$main_menu_id = $this->_getParam('main_menu_id',0);
		
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	
	    
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

        $sidebarDb = new GeneralSetup_Model_DbTable_SidebarMenu();
		$sidebarList = $sidebarDb->getSidebarList($main_menu_id);
			
		$json = Zend_Json::encode($sidebarList);
		
		echo $json;
		exit();
	}
	
	public function addSidebarAction(){		
		
		$menu_id = $this->_getParam('menu_id',null);
		
		$this->view->title = $this->view->translate("Menu Setup - Add Sidebar Menu");
		
		$this->view->backURL = $this->view->url(array('module'=>'generalsetup','controller'=>'menu', 'action'=>'sidebar', 'menu_id'=>$menu_id),'default',true);
		
		$form = new GeneralSetup_Form_SidebarMenu(array('menuid'=>$menu_id,'parentid'=>'xx'));
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData)) {
				$sidebarDb = new GeneralSetup_Model_DbTable_SidebarMenu();
				
				$data = array(
							'sm_tm_id' => $menu_id,
				            'sm_parentid' => $formData['sm_parentid'],
							'sm_name' => $formData['sm_name'],
							'sm_desc' => $formData['sm_desc'],
							'sm_module' => $formData['sm_module'],
							'sm_controller' => $formData['sm_controller'],
							'sm_action' => $formData['sm_action'],
							'sm_visibility' => $formData['sm_visibility'],
                            'sm_status' => $formData['sm_status']
						);
				
				$sidebarDb->insert($data);
				
				//redirect
				$this->_redirect($this->view->backURL);	
			}else{
				$form->populate($formData);
			}
			
		}
		
		$this->view->form = $form;
		
	}
	
	public function editSidebarAction(){
		
		$menu_id = $this->_getParam('menu_id',null);
		$sidebar_id = $this->_getParam('id',null);
		
		$sidebarDb = new GeneralSetup_Model_DbTable_SidebarMenu();
		
		$data = $sidebarDb->getData($sidebar_id);
		$this->view->data = $data;
		
		$form = new GeneralSetup_Form_EditSidebarMenu(array('menuid'=>$data['sm_tm_id'],'parentid'=>$data['sm_parentid']));
		
		$this->view->title = $this->view->translate("Menu Setup - Edit Sidebar Menu");
		$this->view->backURL = $this->view->url(array('module'=>'generalsetup','controller'=>'menu', 'action'=>'sidebar','menu_id'=>$menu_id),'default',true);
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData)) {
		
				$sidebarDb = new GeneralSetup_Model_DbTable_SidebarMenu();
				
				$info = array(	
							'sm_tm_id' => $menu_id,
							'sm_name' => $formData['sm_name'],
							'sm_desc' => $formData['sm_desc'],
							'sm_module' => $formData['sm_module'],
							'sm_controller' => $formData['sm_controller'],
							'sm_action' => $formData['sm_action'],
							'sm_visibility' => $formData['sm_visibility'],
							'sm_status' => $formData['sm_status']
						);
						
				if(isset($formData['sm_parentid'])){
					$info['sm_parentid']=$formData['sm_parentid'];
				}
				$sidebarDb->updateSidebar($info,$sidebar_id,$formData['old_parent_id']);
				
				//redirect
				$this->_redirect($this->view->backURL);	
			}else{
				$form->populate($formData);
			}
			
		}else{
			$form->populate($data);
		}
		
		$this->view->form = $form;
		
	}
	
	public function deleteSidebarAction(){		
		
		$menu_id = $this->_getParam('menu_id',null);
		$sidebar_id = $this->_getParam('id',null);
		
		$sidebarDb = new GeneralSetup_Model_DbTable_SidebarMenu();
		$sidebarDb->deleteData($sidebar_id);
		
		$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'menu', 'action'=>'sidebar', 'menu_id'=>$menu_id),'default',true));
	}
	
	
}
?>