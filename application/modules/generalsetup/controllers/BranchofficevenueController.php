<?php
class GeneralSetup_BranchofficevenueController extends Base_Base {
	private $locale;
	private $registry;
	private $lobjBranchofficevenueform;
	private $Branchofficevenue;
	private $lobjuser;
	private $_gobjlog;
	private $lintidbrc;

	public function init() {
		$this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
		$this->locale = $this->registry->get('Zend_Locale');
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->view->lintidbrc = $lintidbrc = ( int ) $this->_getParam ( 'idbrc' );
		if($lintidbrc ==1){
			$this->view->idbrch='Branch';
		}
		if($lintidbrc ==2){
			$this->view->idbrch='Office';
		}
		if($lintidbrc ==3){
			$this->view->idbrch='Venue';
		}
	}

	public function fnsetObj(){
		$this->lobjform = new App_Form_Search ();
		$this->lobjBranchofficevenueform = new GeneralSetup_Form_Branchofficevenue();
		$this->Branchofficevenue = new GeneralSetup_Model_DbTable_Branchofficevenue();
		$this->lobjsubjectsofferedmodel = new GeneralSetup_Model_DbTable_Subjectsoffered();
		$this->lobjuser = new GeneralSetup_Model_DbTable_User(); //user model object
	}

	public function indexAction() {
        $pageCount = 50;
		$this->view->title = $this->view->translate("Branch Setup");
		
		$this->view->lobjform = $this->lobjform;
		// Function	to get Country List
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$lobjCountryNameList = $this->Branchofficevenue->fnGetCountryList();
		$this->lobjform->field5->addMultiOptions($lobjCountryNameList);
		$lobjStateNameList = $this->Branchofficevenue->fnGetStateList();
		$this->lobjform->field8->addMultiOptions($lobjStateNameList);
		$this->view->lintidbrc = $lintidbrc = ( int ) $this->_getParam ( 'idbrc' );
		$larrresult = $this->Branchofficevenue->fngetBranchDetails($lintidbrc);
		if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->Branchofficevenuepaginatorresult);
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1);// Paginator instance
		if(isset($this->gobjsessionsis->Branchofficevenuepaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->Branchofficevenuepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->Branchofficevenue->fnSearchBranchDetails( $this->lobjform->getValues (),$lintidbrc ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Branchofficevenuepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/index');
			$this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/index/idbrc/'.$lintidbrc);
		}

        $paginator = Zend_Paginator::factory($larrresult);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);

        $totalRecord = $paginator->getTotalItemCount();

        $this->view->paginator = $paginator;
        $this->view->totalRecord = $totalRecord;
	}

	public function newbranchofficevenueAction() {
		
		$this->view->title = $this->view->translate("Add Branch");
		
		$this->view->lobjBranchofficevenueform = $this->lobjBranchofficevenueform;
		$this->view->generated_code = generateRandomString(6);
		
		$lobjUniversity = new GeneralSetup_Model_DbTable_University(); //To get university details
		$lobjUniversity = $lobjUniversity->fnGetUniversityList();
		$this->lobjBranchofficevenueform->AffiliatedTo->addMultiOptions($lobjUniversity);
		
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		
		$lobjProgramme = new GeneralSetup_Model_DbTable_Program(); //To get programme list
		$lobjProgramme = $lobjProgramme->fnGetProgramList();
		if(count($lobjProgramme) > 0) {
			//$lobjProgramme[] = array('key' => 'all', 'value' => 'All');
		}
		$this->lobjBranchofficevenueform->Programme->addMultiOptions($lobjProgramme);

		$larrresult = $this->Branchofficevenue->fnGetRegistrationlocList();
		$this->lobjBranchofficevenueform->RegistrationLoc->addMultiOptions($larrresult);
		
		$this->view->lintidbrc = $lintidbrc = ( int ) $this->_getParam ( 'idbrc' );
		if ($lintidbrc == 2){
			$larrbranchset = $this->lobjsubjectsofferedmodel->fngetAllBranchset(1);
			$this->lobjBranchofficevenueform->Branch->addMultiOptions($larrbranchset);
		}
		else if ($lintidbrc == 3){
			$larrbranchset = $this->lobjsubjectsofferedmodel->fngetAllBranchset(2);
			$this->lobjBranchofficevenueform->Branch->addMultiOptions($larrbranchset);
		}
		
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		
		$this->view->lobjBranchofficevenueform->UpdDate->setValue( $ldtsystemDate );
		
		$auth = Zend_Auth::getInstance();
		$this->view->lobjBranchofficevenueform->UpdUser->setValue( $auth->getIdentity()->iduser);
		
		$lobjcountry = $this->lobjuser->fnGetCountryList();
		$this->lobjBranchofficevenueform->idCountry->addMultiOptions($lobjcountry);
		$this->view->lobjBranchofficevenueform->idCountry->setValue ($this->view->DefaultCountry);
			
		$this->view->lobjBranchofficevenueform->IdType->setValue ($this->view->lintidbrc);
		
		if ($this->_request->isPost ()) {
			
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post

			if ($this->lobjBranchofficevenueform->isValid ( $larrformData )) {
				
				$larrregistration = $larrformData;
                                //var_dump($larrregistration); exit;
				//add branch info
				$lintIdBranch= $this->Branchofficevenue->fnaddBranchDetails($larrformData);
			
				//add reg location (BUT IT WAS COMMENTED ON FORM) 23/2/2014 so i disbaled the add function
				//$this->Branchofficevenue->fninsertBranchregistration($larrregistration,$lintIdBranch);
				
				//add branch address 23/5/2014 
				//add department address
		        $data['add_org_name'] = 'tbl_branchofficevenue';
				$data['add_org_id'] = $lintIdBranch;
		        $data['add_address_type'] = $larrformData['AddressType'];
				$data['add_country'] = $larrformData['idCountry'];
				$data['add_state'] = $larrformData['idState'];
				$data['add_address1'] = $larrformData['Addr1'];
				$data['add_address2'] = $larrformData['Addr2'];
				$data['add_zipcode'] = $larrformData['zipCode'];
				$data['add_phone'] = $larrformData['Phone'];
				$data['add_email'] = $larrformData['Email'];
				$data['add_affiliate'] = '';	
				$data['add_createddt'] = date('Y-m-d H:i:s');
				$data['add_createdby'] = $auth->getIdentity()->iduser;
				$data['add_state_others']=$larrformData['State_Others'];
				$data['add_city_others']=$larrformData['City_Others'];
				$data['add_city']=$larrformData['City'];

		        $addressDb = new GeneralSetup_Model_DbTable_Address();
		        $addressDb->addData($data);
				
				if (isset($larrformData['programme']) && count($larrformData['programme']) > 0){
					foreach($larrformData['programme'] as $programLoop){
						$dataProg = array(
							'bp_IdBranch' => $lintIdBranch,
							'bp_IdProgram' => $programLoop,
							'bp_updUser' => $auth->getIdentity()->iduser,
							'bp_updDate' => date('Y-m-d H:i:s')
						);
						$this->Branchofficevenue->insertProgram($dataProg);
					}
				}
				
				/*if (isset($larrformData['mouname']) && count($larrformData['mouname']) > 0){
					$i=0;
					foreach($larrformData['mouname'] as $mouLoop){
						$dataMou = array(
							'mou_IdBranch' => $lintIdBranch,
							'mou_name' => $mouLoop,
							'mou_desc' => $larrformData['moudesc'][$i],
							'mou_startDate' => date('Y-m-d',strtotime($larrformData['moustartdate'][$i])),
							'mou_endDate' => date('Y-m-d',strtotime($larrformData['mouenddate'][$i])),
							'mou_updUser' => $auth->getIdentity()->iduser,
							'mou_updDate' => date('Y-m-d H:i:s')
						);
						$this->Branchofficevenue->insertMou($dataMou);
						$i++;
					}
				}*/
				
				if($lintidbrc ==1){
					$idbrch='Internal Branch';
				}
				if($lintidbrc ==2){
					$idbrch='External Branch';
				}
				
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'New'.' '.$idbrch.' '.'Add',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/index/idbrc/1');
			}
		}
	}

	public function branchofficevenuelistAction() { //Action for the updation and view of the  details

		$this->view->title = $this->view->translate("Edit Branch");
		
		$this->view->lobjBranchofficevenueform= $this->lobjBranchofficevenueform;
		
		$lintIdBranch = ( int ) $this->_getParam ( 'id' );
		
		$this->view->idBranch = $lintIdBranch;
		$this->view->lobjBranchofficevenueform->IdBranch->setValue( $lintIdBranch );
		$this->view->lintidbrc = $lintidbrc = ( int ) $this->_getParam ( 'idbrc' );
		if ($lintidbrc == 2){
			$larrbranchset = $this->lobjsubjectsofferedmodel->fngetAllBranchset(1);
			$this->lobjBranchofficevenueform->Branch->addMultiOptions($larrbranchset);
		}
		else if ($lintidbrc == 3){
			$larrbranchset = $this->lobjsubjectsofferedmodel->fngetAllBranchset(2);
			$this->lobjBranchofficevenueform->Branch->addMultiOptions($larrbranchset);
		}
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjBranchofficevenueform->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjBranchofficevenueform->UpdUser->setValue( $auth->getIdentity()->iduser);
		$lobjcountry = $this->lobjuser->fnGetCountryList();
		$this->lobjBranchofficevenueform->idCountry->addMultiOptions($lobjcountry);
		$this->view->lobjBranchofficevenueform->idCountry->setValue ($this->view->DefaultCountry);
                
                $currencyModel = new Studentfinance_Model_DbTable_Currency();
		$currencyList = $currencyModel->fetchAll();
                $currlist = array();
                $i = 0;
                if (count($currencyList)){
                    foreach ($currencyList as $currencyLoop){
                    $currlist[$i]['value']=$currencyLoop['cur_code'];
                    $currlist[$i]['key']=$currencyLoop['cur_id'];
                    $i++;
                    }
                }else{
                    $currlist[$i]['value']='other';
                    $currlist[$i]['key']='0';
                }
                $this->lobjBranchofficevenueform->Currency->addMultiOptions($currlist);
		
		$lobjUniversity = new GeneralSetup_Model_DbTable_University(); //intialize user Model
		$lobjUniversity = $lobjUniversity->fnGetUniversityList();
		$this->lobjBranchofficevenueform->AffiliatedTo->addMultiOptions($lobjUniversity);
	
		$lobjProgramme = new GeneralSetup_Model_DbTable_Program(); //To get programme list
		$lobjProgramme = $lobjProgramme->fnGetProgramList();
		if(count($lobjProgramme) > 0) {
			$lobjProgramme[] = array('key' => 'all', 'value' => 'All');
		}
		$this->lobjBranchofficevenueform->Programme->addMultiOptions($lobjProgramme);

		$larrResult = $this->Branchofficevenue->fnviewBranchofficevenueDtls($lintIdBranch);
		
		$lobjCommonModel = new App_Model_Common();
		$larrCountyrStatesList = $lobjCommonModel->fnGetStateList();
		$this->lobjBranchofficevenueform->idState->addMultiOptions($larrCountyrStatesList);
		//$this->view->lobjBranchofficevenueform->idState->setValue ($larrResult['idState']);
		
		$this->lobjBranchofficevenueform->populate($larrResult);
						
		if ($this->_request->isPost()) {
						
			$larrformData = $this->_request->getPost();
			
			
				if ($this->lobjBranchofficevenueform->isValid ( $larrformData )) {
					
					$lintIdBranch = $larrformData ['IdBranch'];
					$result=$this->Branchofficevenue->fnupdateBranchofficevenueDtls($lintIdBranch, $larrformData );
					
					if($lintidbrc ==1){
						$idbrch='Internal Branch';
					}
					if($lintidbrc ==2){
						$idbrch='External Branch';
					}
					
					$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

					// Write Logs
					$priority=Zend_Log::INFO;
					$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => $idbrch.' '.'Edit Id=' . $lintIdBranch,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
					$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

					$this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/branchofficevenuelist/id/'.$lintIdBranch.'/idbrc/'.$lintidbrc);
				}
			}
	}

	public function deleteregistrationlocationAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidbrc =1;
		$idRegLoc = $this->_getParam('id', 0);
		$larrDelete = $this->Branchofficevenue->fnDeleteRegLoc($idRegLoc);
		$this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/index/idbrc/'.$lintidbrc);
	}
	
	public function deleteBranchAction(){		
		$id = $this->_getParam('id', 0);
		$this->Branchofficevenue->deleteBranch($id);
		
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		$addressDb->deleteOrgAddress($id,'tbl_branchofficevenue');
		
		$this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/index/idbrc/1');
	}
	
	public function branchAddressAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_branch = $this->_getParam('id', 0);
		$idbrc = $this->_getParam('idbrc', 0);
		$this->view->idBranch = $id_branch;
		
		//get list branch address
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		$this->view->address_list = $addressDb->getDataByOrgId($id_branch,'tbl_branchofficevenue');		
		
	}
	
	public function addAddressAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_branch = $this->_getParam('id', 0);
		$idbrc = $this->_getParam('idbrc', 0);		
		
		$form= new GeneralSetup_Form_AddressForm(array('idBranch'=>$id_branch,'idBrc'=>$idbrc));
		$this->view->addressForm = $form;
		
		if ($this->_request->isPost ()) {			
		
			$formData = $this->_request->getPost ();
			
			if ($form->isValid ( $formData )) {
				
				//save address				
				$auth = Zend_Auth::getInstance();
				
				$data['add_org_name'] = 'tbl_branchofficevenue';
				$data['add_org_id'] = $formData['idBranch'];
				$data['add_address_type'] = $formData['address_type'];
				$data['add_country'] = $formData['country'];
				$data['add_state'] = $formData['state'];
				$data['add_address1'] = $formData['address1'];
				$data['add_address2'] = $formData['address2'];
				$data['add_zipcode'] = $formData['zipcode'];
				$data['add_phone'] = $formData['phone'];
				$data['add_email'] = $formData['email'];
				$data['add_createddt'] = date('Y-m-d H:i:s');
				$data['add_createdby'] = $auth->getIdentity()->iduser;
				$data['add_city'] = $formData['city'];
				$data['add_state_others'] = $formData['state_others'];
				$data['add_city_others'] = $formData['city_others'];
				
				$addressDb = new GeneralSetup_Model_DbTable_Address();
				$addressDb->addData($data);
				
				$this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/branchofficevenuelist/id/'.$formData['idBranch'].'/idbrc/1');
			}
		}
	}
	
	public function editAddressAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_branch = $this->_getParam('id', 0);
		$idbrc = $this->_getParam('idbrc', 0);
		$add_id = $this->_getParam('add_id', 0);
						
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		
		
		$form= new GeneralSetup_Form_AddressForm(array('idBranch'=>$id_branch,'idBrc'=>$idbrc,'addId'=>$add_id));
		
		if ($this->_request->isPost ()) {			
		
			$formData = $this->_request->getPost ();
			$auth = Zend_Auth::getInstance();
				
			if ($form->isValid ( $formData )) {
			
				//get old (address before change)				
				$address = $addressDb->getDatabyId($formData['add_id']);
				
				//insert into history
				$olddata['add_id'] = $address['add_id'];
				$olddata['addh_org_name'] = $address['add_org_name'];
				$olddata['addh_org_id'] = $address['add_org_id'];
				$olddata['addh_address_type'] = $address['add_address_type'];
				$olddata['addh_country'] = $address['add_country'];
				$olddata['addh_state'] = $address['add_state'];
				$olddata['addh_address1'] = $address['add_address1'];
				$olddata['addh_address2'] = $address['add_address2'];
				$olddata['addh_zipcode'] = $address['add_zipcode'];
				$olddata['addh_phone'] = $address['add_phone'];
				$olddata['addh_email'] = $address['add_email'];
				$olddata['addh_createddt'] = $address['add_createddt'];
				$olddata['addh_createdby'] = $address['add_createdby'];	
				$olddata['addh_changedt'] = date('Y-m-d H:i:s');
				$olddata['addh_changeby'] = $auth->getIdentity()->iduser;
				$olddata['addh_city'] = $address['add_city'];
				$olddata['addh_state_others'] = $address['add_state_others'];
				$olddata['addh_city_others'] = $address['add_city_others'];
				
				$addHistoryDb = new GeneralSetup_Model_DbTable_AddressHistory();	
				$addHistoryDb->addData($olddata);
				
				
				//edit
				
				$data['add_address_type'] = $formData['address_type'];
				$data['add_country'] = $formData['country'];
				$data['add_state'] = $formData['state'];
				$data['add_address1'] = $formData['address1'];
				$data['add_address2'] = $formData['address2'];
				$data['add_zipcode'] = $formData['zipcode'];
				$data['add_phone'] = $formData['phone'];
				$data['add_email'] = $formData['email'];
				$data['add_modifydt'] = date('Y-m-d H:i:s');
				$data['add_modifyby'] = $auth->getIdentity()->iduser;		
				$data['add_city'] = $formData['city'];
				$data['add_state_others'] = $formData['state_others'];
				$data['add_city_others'] = $formData['city_others'];
				
				$addressDb->updateData($data,$formData['add_id']);
				
				$this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/branchofficevenuelist/id/'.$formData['idBranch'].'/idbrc/1');
				
			}
			
		}else{
			
			
			$add = $addressDb->getDatabyId($add_id);
			
			//get state to populate state from country
			/*$stateDB = new App_Model_General_DbTable_State();
			$state_data = $stateDB->getState($add['add_country']);		
			$element = $form->getElement('state');
			foreach($state_data as $s){
			$element->addMultiOption($s['idState'],$s['StateName']);
			}*/
		
			$address_data['address_type']=$add['add_address_type'];
			$address_data['country']=$add['add_country'];
			$address_data['state']=$add['add_state'];
			$address_data['address1']=$add['add_address1'];
			$address_data['address2']=$add['add_address2'];
			$address_data['zipcode']=$add['add_zipcode'];
			$address_data['phone']=$add['add_phone'];
			$address_data['email']=$add['add_email'];
			$address_data['city']=$add['add_city'];
			$address_data['state_others']=$add['add_state_others'];
			$address_data['city_others']=$add['add_city_others'];
			//print_r($address_data);

			$this->view->address_data = $address_data;
		
			$form->populate($address_data);
			$this->view->addressForm = $form;		
		}
		
	}
	
	public function deleteAddressAction(){
		
		$id_branch = $this->_getParam('id', 0);
		$idbrc = $this->_getParam('idbrc', 0);
		$add_id = $this->_getParam('add_id', 0);
		
		$addressDb = new GeneralSetup_Model_DbTable_Address();
		$addressDb->deleteData($add_id);
		
		$this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/branchofficevenuelist/id/'.$id_branch.'/idbrc/1');
		exit;
	}
	
	public function viewHistoryAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_branch = $this->_getParam('id', 0);
		$idbrc = $this->_getParam('idbrc', 0);		
		
		//get list branch address
		$addressDb = new GeneralSetup_Model_DbTable_AddressHistory();
		$this->view->address_list = $addressDb->getDataByOrgId($id_branch,'tbl_branchofficevenue');		
	}
	
        public function getcurrencylistAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
                
                $currencyModel = new Studentfinance_Model_DbTable_Currency();

		$currencyList = $currencyModel->fetchAll();
                //var_dump($currencyList); exit;
                $currlist = array();
                $i = 0;
                if (count($currencyList)){
                    foreach ($currencyList as $currencyLoop){
                    $currlist[$i]['name']=$currencyLoop['cur_code'];
                    $currlist[$i]['key']=$currencyLoop['cur_id'];
                    $i++;
                    }
                }else{
                    $currlist[$i]['name']='other';
                    $currlist[$i]['key']='0';
                }
		
		echo Zend_Json_Encoder::encode($currlist);
	}
        
        public function viewProgramAction(){
		
		$this->_helper->layout->disableLayout();	
		$id_branch = $this->_getParam('id', 0);
		$idbrc = $this->_getParam('idbrc', 0);		
		
		//get list branch program
		$this->view->program_list = $this->Branchofficevenue->getBranchProgramList($id_branch);	
                $this->view->idBranch = $id_branch;
	}
        
        public function addProgramAction(){
            $this->_helper->layout->disableLayout();	
            $id_branch = $this->_getParam('id', 0);
            $idbrc = $this->_getParam('idbrc', 0);		

            $progModel = new Application_Model_DbTable_ProgramScheme();
            $progList = $progModel->fnGetProgramList();

            $this->view->programList = $progList;
            $this->view->id_branch = $id_branch;
            $this->view->idbrc = $idbrc;

            if ($this->_request->isPost ()) {			
                $formData = $this->_request->getPost();
                //var_dump($formData); exit;
                $auth = Zend_Auth::getInstance();
                if (isset($formData['cprogram']) && count($formData['cprogram']) > 0){
                    foreach($formData['cprogram'] as $programLoop){
                        $dataProg = array(
                            'bp_IdBranch' => $formData['idBranch'],
                            'bp_IdProgram' => $programLoop,
                            'bp_updUser' => $auth->getIdentity()->iduser,
                            'bp_updDate' => date('Y-m-d H:i:s')
                        );
                        $this->Branchofficevenue->insertProgram($dataProg);
                    }
                    $this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/branchofficevenuelist/id/'.$formData['idBranch'].'/idbrc/1');
                }
            }
	}
        
        public function deleteProgramAction(){
            $id_branch = $this->_getParam('id', 0);
            $idbrc = $this->_getParam('idbrc', 0);
            $bp_id = $this->_getParam('bp_id', 0);

            $this->Branchofficevenue->deleteBranchProgram($bp_id);

            $this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/branchofficevenuelist/id/'.$id_branch.'/idbrc/1');
            exit;
        }
        
        public function viewMouAction(){
            $this->_helper->layout->disableLayout();	
            $id_branch = $this->_getParam('id', 0);
            $idbrc = $this->_getParam('idbrc', 0);
            
            $this->view->id_branch = $id_branch;
            $this->view->idbrc = $idbrc;
            
            $this->view->lobjBranchofficevenueform = $this->lobjBranchofficevenueform;
            //get list branch program
            $this->view->mou_list = $this->Branchofficevenue->getMou($id_branch);	
            $this->view->idBranch = $id_branch;
        }
        
        public function addMouAction(){
            $id_branch = $this->_getParam('id', 0);
            $idbrc = $this->_getParam('idbrc', 0);	
            
            $this->view->lobjBranchofficevenueform = $this->lobjBranchofficevenueform;
            $this->view->id_branch = $id_branch;
            $this->view->idbrc = $idbrc;

            if ($this->_request->isPost ()) {			
                $formData = $this->_request->getPost();
                //var_dump($formData); exit;
                $auth = Zend_Auth::getInstance();
                if($this->view->lobjBranchofficevenueform->isValid($formData)){
                    
                    $dir = APP_DOC_PATH.'/mou/'.$formData['idBranch'];
            
                    if ( !is_dir($dir) ){
                        if ( mkdir_p($dir) === false )
                        {
                            throw new Exception('Cannot create attachment folder ('.$dir.')');
                        }
                    }
                    
                    //upload file proses
                    $adapter = new Zend_File_Transfer_Adapter_Http();
                    $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 1)); //maybe soon we will allow more?
                    $adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
                    $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
                    $adapter->addValidator('NotExists', false, $dir);
                    $adapter->setDestination($dir);

                    $files = $adapter->getFileInfo();
                    //var_dump($files); exit;
                    //insert upload file in db
                    if ($files){
                        $fileOriName = $files['file']['name'];
                        $fileRename = date('YmdHis').'_'.$fileOriName;
                        $filepath = $files['file']['destination'].'/'.$fileRename;

                        $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true));

                        $adapter->isUploaded();
                        $adapter->receive();    

                        $data = array(
                            'mou_IdBranch' => $formData['idBranch'],
                            'mou_name' => $formData['mou_name'],
                            'mou_desc' => $formData['mou_desc'],
                            'mou_startDate' => date('Y-m-d',strtotime($formData['mou_startDate'])),
                            'mou_endDate' => date('Y-m-d',strtotime($formData['mou_endDate'])),
                            'mou_filepath' => '/mou/'.$formData['idBranch'].'/'.$fileRename,
                            'mou_updUser' => $auth->getIdentity()->iduser,
                            'mou_updDate' => date('Y-m-d H:i:s')
                        );
                        //var_dump($data); exit;
                        $this->Branchofficevenue->insertMou($data);
                        $this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/branchofficevenuelist/id/'.$formData['idBranch'].'/idbrc/1#tab5');
                    }
                }
            }
        }
        
        public function deleteMouAction(){
            $id_branch = $this->_getParam('id', 0);
            $idbrc = $this->_getParam('idbrc', 0);
            $mou_id = $this->_getParam('mou_id', 0);

            $this->Branchofficevenue->deleteBranchMou($mou_id);

			$this->_helper->flashMessenger->addMessage(array('success' => "Agreement deleted"));

            $this->_redirect( $this->baseUrl . '/generalsetup/branchofficevenue/branchofficevenuelist/id/'.$id_branch.'/idbrc/1#tab5');
            exit;
        }
}