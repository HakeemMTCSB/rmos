<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_AttendanceReportController extends Base_Base { 
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new GeneralSetup_Model_DbTable_AttendanceReport();
    }
    
    public function indexAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->view->title = $this->view->translate('Attendance Report (By Course Section)');
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $form = new GeneralSetup_Form_AttendanceReportCourseSection(array('formData'=>$formData));
            $this->view->form = $form;
            $form->populate($formData);
            
            $groupInfo = $this->model->getGroupInfo($formData['coursesection']);
            $studentList = $this->model->getGroupStudent($formData['coursesection']);
            $groupSchedule = $this->model->getGroupSchedule($formData['coursesection'], $formData);
            $sdate = $this->model->getAttendanceDate($formData['coursesection']);
            
            if ($groupSchedule){
                $classList = $groupSchedule;
            }else{
                throw new Exception('Group Schedule Not Found');
            }
            
            $weekArr = array();
            $monthArr = array();

            if ($classList){
                //$monthCount = 0;
                //$weekCount = 0;
                $i = 0;
                foreach ($classList as $key => $dateLoop){

                    if (isset($dateLoop['sc_day'])){
                        switch($dateLoop['sc_day']){
                            case 'Monday':
                                $d[$key] = 1;
                                break;
                            case 'Tuesday':
                                $d[$key] = 2;
                                break;
                            case 'Wednesday':
                                $d[$key] = 3;
                                break;
                            case 'Thursday':
                                $d[$key] = 4;
                                break;
                            case 'Friday':
                                $d[$key] = 5;
                                break;
                            case 'Saturday':
                                $d[$key] = 6;
                                break;
                            case 'Sunday':
                                $d[$key] = 7;
                                break;
                        }
                    }

                    if ($dateLoop['sc_date']==null || $dateLoop['sc_date']==''){
                        if ($sdate){
                            $i = 0;
                            foreach ($sdate as $sdateLoop){
                                if ($sdateLoop['ctd_class_status']==1) {
                                    $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                                    $i++;

                                    $month = date('F', strtotime($sdateLoop['ctd_scdate']));
                                    $week = date('W', strtotime($sdateLoop['ctd_scdate']));

                                    if (!in_array($month, $monthArr)) {
                                        array_push($monthArr, $month);
                                    }

                                    if (!in_array($week, $weekArr)) {
                                        array_push($weekArr, $week);
                                    }
                                }
                            }
                        }else{
                            if ($key > 0){
                                $dkey = $d[$key]-$d[($key-1)];
                                $plusdate = $dkey.' day';
                                $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                            }else{
                                $start_day = date('l', strtotime($groupInfo['class_start_date']));

                                switch($start_day){
                                    case 'Monday':
                                        $start_day_no = 1;
                                        break;
                                    case 'Tuesday':
                                        $start_day_no = 2;
                                        break;
                                    case 'Wednesday':
                                        $start_day_no = 3;
                                        break;
                                    case 'Thursday':
                                        $start_day_no = 4;
                                        break;
                                    case 'Friday':
                                        $start_day_no = 5;
                                        break;
                                    case 'Saturday':
                                        $start_day_no = 6;
                                        break;
                                    case 'Sunday':
                                        $start_day_no = 7;
                                        break;
                                }
                                $dkey = $d[$key]-$start_day_no;
                                //var_dump($dkey);
                                $plusdate = $dkey.' day';
                                $groupInfo['class_start_date'] = $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                                //$date = $groupInfo['class_start_date'];
                            }

                            while ($date <= $groupInfo['class_end_date']){
                                $classList[$i]['sc_date'] = $date;

                                $month = date('F', strtotime($date));
                                $week = date('W', strtotime($date));

                                $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                                $i++;

                                if (!in_array($month, $monthArr)){
                                    array_push($monthArr, $month);
                                }

                                if (!in_array($week, $weekArr)){
                                    array_push($weekArr, $week);
                                }
                            }
                        }
                        //redirect here
                        //$this->_helper->flashMessenger->addMessage(array('error' => 'Please Set Schedule Date For Section "'.$groupInfo['GroupName'].'" Before View Attendance'));
                        //$this->_redirect($this->baseUrl . '/generalsetup/course-group/group-list/idSubject/'.$groupInfo['IdSubject'].'/idSemester/'.$groupInfo['IdSemester']);
                    }

                    $month = date('F', strtotime($dateLoop['sc_date']));
                    $week = date('W', strtotime($dateLoop['sc_date']));

                    if (!in_array($month, $monthArr)){
                        array_push($monthArr, $month);
                    }

                    if (!in_array($week, $weekArr)){
                        array_push($weekArr, $week);
                    }

                    unset($classList[$key]['sc_id']);
                    unset($classList[$key]['idGroup']);
                    unset($classList[$key]['IdLecturer']);
                    unset($classList[$key]['idCollege']);
                    unset($classList[$key]['idBranch']);
                    unset($classList[$key]['idClassType']);
                    unset($classList[$key]['sc_day']);
                    unset($classList[$key]['sc_start_time']);
                    unset($classList[$key]['sc_end_time']);
                    unset($classList[$key]['sc_venue']);
                    unset($classList[$key]['sc_class']);
                    unset($classList[$key]['sc_remark']);
                    unset($classList[$key]['sc_createdby']);
                    unset($classList[$key]['sc_createddt']);
                }
            }

            $this->aasort($classList, 'sc_date');
            $this->view->groupInfo = $groupInfo;
            $this->view->studentList = $studentList;
            $this->view->classList = $classList;
            $this->view->monthArr = $monthArr;
            $this->view->weekArr = $weekArr;
        }else{
            $form = new GeneralSetup_Form_AttendanceReportCourseSection();
            $this->view->form = $form;
        }
    }

    public function clearAction(){
        //redirect here
        $this->_redirect($this->baseUrl . '/generalsetup/attendance-report/index/');
        exit;
    }
    
    public function printAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->_helper->layout->disableLayout();
        $this->view->role = $this->auth->getIdentity()->IdRole;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $form = new GeneralSetup_Form_AttendanceReportCourseSection(array('formData'=>$formData));
            $this->view->form = $form;
            $form->populate($formData);
            
            $groupInfo = $this->model->getGroupInfo($formData['coursesection']);
            $studentList = $this->model->getGroupStudent($formData['coursesection']);
            $groupSchedule = $this->model->getGroupSchedule($formData['coursesection'], $formData);
            $sdate = $this->model->getAttendanceDate($formData['coursesection']);
            
            if ($groupSchedule){
                $classList = array();
                /*if (count($groupSchedule)==1){
                    if ($sdate){
                        $i = 0;
                        foreach ($sdate as $sdateLoop){
                            $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                            $i++;
                        }
                    }else{
                        $date = $groupInfo['class_start_date'];
                        $i = 0;
                        while ($date <= $groupInfo['class_end_date']){
                            $classList[$i]['sc_date'] = $date;
                            $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                            $i++;
                        }
                    }
                }else{
                    $classList = $groupSchedule;
                }*/
                $classList = $groupSchedule;
            }else{
                throw new Exception('Group Schedule Not Found');
            }
            
            $weekArr = array();
            $monthArr = array();

            if ($classList){
                //$monthCount = 0;
                //$weekCount = 0;
                $i = 0;
                foreach ($classList as $key => $dateLoop){

                    if (isset($dateLoop['sc_day'])){
                        switch($dateLoop['sc_day']){
                            case 'Monday':
                                $d[$key] = 1;
                                break;
                            case 'Tuesday':
                                $d[$key] = 2;
                                break;
                            case 'Wednesday':
                                $d[$key] = 3;
                                break;
                            case 'Thursday':
                                $d[$key] = 4;
                                break;
                            case 'Friday':
                                $d[$key] = 5;
                                break;
                            case 'Saturday':
                                $d[$key] = 6;
                                break;
                            case 'Sunday':
                                $d[$key] = 7;
                                break;
                        }
                    }

                    if ($dateLoop['sc_date']==null || $dateLoop['sc_date']==''){
                        if ($sdate){
                            $i = 0;
                            foreach ($sdate as $sdateLoop){
                                if ($sdateLoop['ctd_class_status']==1) {
                                    $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                                    $i++;

                                    $month = date('F', strtotime($sdateLoop['ctd_scdate']));
                                    $week = date('W', strtotime($sdateLoop['ctd_scdate']));

                                    if (!in_array($month, $monthArr)) {
                                        array_push($monthArr, $month);
                                    }

                                    if (!in_array($week, $weekArr)) {
                                        array_push($weekArr, $week);
                                    }
                                }
                            }
                        }else{
                            if ($key > 0){
                                $dkey = $d[$key]-$d[($key-1)];
                                $plusdate = $dkey.' day';
                                $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                            }else{
                                $start_day = date('l', strtotime($groupInfo['class_start_date']));

                                switch($start_day){
                                    case 'Monday':
                                        $start_day_no = 1;
                                        break;
                                    case 'Tuesday':
                                        $start_day_no = 2;
                                        break;
                                    case 'Wednesday':
                                        $start_day_no = 3;
                                        break;
                                    case 'Thursday':
                                        $start_day_no = 4;
                                        break;
                                    case 'Friday':
                                        $start_day_no = 5;
                                        break;
                                    case 'Saturday':
                                        $start_day_no = 6;
                                        break;
                                    case 'Sunday':
                                        $start_day_no = 7;
                                        break;
                                }
                                $dkey = $d[$key]-$start_day_no;
                                //var_dump($dkey);
                                $plusdate = $dkey.' day';
                                $groupInfo['class_start_date'] = $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                                //$date = $groupInfo['class_start_date'];
                            }

                            while ($date <= $groupInfo['class_end_date']){
                                $classList[$i]['sc_date'] = $date;

                                $month = date('F', strtotime($date));
                                $week = date('W', strtotime($date));

                                $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                                $i++;

                                if (!in_array($month, $monthArr)){
                                    array_push($monthArr, $month);
                                }

                                if (!in_array($week, $weekArr)){
                                    array_push($weekArr, $week);
                                }
                            }
                        }
                        //redirect here
                        //$this->_helper->flashMessenger->addMessage(array('error' => 'Please Set Schedule Date For Section "'.$groupInfo['GroupName'].'" Before View Attendance'));
                        //$this->_redirect($this->baseUrl . '/generalsetup/course-group/group-list/idSubject/'.$groupInfo['IdSubject'].'/idSemester/'.$groupInfo['IdSemester']);
                    }

                    $month = date('F', strtotime($dateLoop['sc_date']));
                    $week = date('W', strtotime($dateLoop['sc_date']));

                    if (!in_array($month, $monthArr)){
                        array_push($monthArr, $month);
                    }

                    if (!in_array($week, $weekArr)){
                        array_push($weekArr, $week);
                    }

                    unset($classList[$key]['sc_id']);
                    unset($classList[$key]['idGroup']);
                    unset($classList[$key]['IdLecturer']);
                    unset($classList[$key]['idCollege']);
                    unset($classList[$key]['idBranch']);
                    unset($classList[$key]['idClassType']);
                    unset($classList[$key]['sc_day']);
                    unset($classList[$key]['sc_start_time']);
                    unset($classList[$key]['sc_end_time']);
                    unset($classList[$key]['sc_venue']);
                    unset($classList[$key]['sc_class']);
                    unset($classList[$key]['sc_remark']);
                    unset($classList[$key]['sc_createdby']);
                    unset($classList[$key]['sc_createddt']);
                }
            }

            $this->aasort($classList, 'sc_date');
            $this->view->groupInfo = $groupInfo;
            $this->view->studentList = $studentList;
            $this->view->classList = $classList;
            $this->view->monthArr = $monthArr;
            $this->view->weekArr = $weekArr;
        }
        
        $this->view->filename = date('Ymd').'_attendance.xls';
    }
    
    public function studentAttendanceAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->view->title = $this->view->translate('Attendance Report (By Student)');
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $form = new GeneralSetup_Form_AttendanceReportStudent(array('formData'=>$formData));
            $this->view->form = $form;
            $form->populate($formData);
            
            $list = $this->model->getReportAtttendanceReport($formData);
            
            if ($list){
                foreach ($list as $key => $loop){
                    $result = $this->calculateAttendance($loop['IdCourseTaggingGroup'], $loop['IdStudentRegistration']);
                    $list[$key]['calculation'] = $result;
                }
            }
            
            $this->view->list = $list;
        }else{
            $form = new GeneralSetup_Form_AttendanceReportStudent();
            $this->view->form = $form;
        }
    }
    
    public function printStudentAttendanceAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->_helper->layout->disableLayout();
        $this->view->role = $this->auth->getIdentity()->IdRole;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $form = new GeneralSetup_Form_AttendanceReportStudent(array('formData'=>$formData));
            $this->view->form = $form;
            $form->populate($formData);
            
            $list = $this->model->getReportAtttendanceReport($formData);
            
            if ($list){
                foreach ($list as $key => $loop){
                    $result = $this->calculateAttendance($loop['IdCourseTaggingGroup'], $loop['IdStudentRegistration']);
                    $list[$key]['calculation'] = $result;
                }
            }
            
            $this->view->list = $list;
        }
        
        $this->view->filename = date('Ymd').'_student_attendance.xls';
    }
    
    public function getProgramAction(){
        $semesterid = $this->_getParam('id', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $semesterInfo = $this->model->getSemesterById($semesterid);
        
        $programList = $this->model->getProgramByScheme($semesterInfo['IdScheme']);
        
        $json = Zend_Json::encode($programList);
		
	echo $json;
	exit();
    }
    
    public function getCourseAction(){
        $programid = $this->_getParam('id', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $courseList = $this->model->getSubjectFromLandscape($programid);
        
        $json = Zend_Json::encode($courseList);
		
	echo $json;
	exit();
    }
    
    public function getCourseSectionAction(){
        $courseid = $this->_getParam('id', 0);
        $semesterid = $this->_getParam('semid', 0);
        $programid = $this->_getParam('progid', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $courseSectionList = $this->model->getCourseSection($programid, $semesterid, $courseid);
        
        $json = Zend_Json::encode($courseSectionList);
		
	echo $json;
	exit();
    }
    
    public function getLecturerAction(){
        $id = $this->_getParam('id', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $lecturerList = $this->model->getLecturer($id);
        
        $json = Zend_Json::encode($lecturerList);
		
	echo $json;
	exit();
    }
    
    public function getRegistrationItemAction(){
        $id = $this->_getParam('id', 0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $registrationItemList = $this->model->getRegistrationItem($id);
        
        $json = Zend_Json::encode($registrationItemList);
		
	echo $json;
	exit();
    }
    
    public function calculateAttendance($groupid, $studentid){
    	
    	//kalo ubah kat sini peles ubah kat attendance model dalam registration module..
        $groupInfo = $this->model->getGroupInfo($groupid);
        //$studentList = $this->model->getGroupStudent($groupid);
        $groupSchedule = $this->model->getGroupSchedule($groupid);
        $sdate = $this->model->getAttendanceDate($groupid);
        $presentAtt = $this->model->getPresentAttendanceDate($groupid, $studentid);
        
        if ($groupSchedule){
            $classList = array();
            /*if (count($groupSchedule)==1){
                if ($sdate){
                    $i = 0;
                    foreach ($sdate as $sdateLoop){
                        $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                        $i++;
                    }
                }else{
                    $date = $groupInfo['class_start_date'];
                    $i = 0;
                    while ($date <= $groupInfo['class_end_date']){
                        $classList[$i]['sc_date'] = $date;
                        $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                        $i++;
                    }
                }
            }else{
                $classList = $groupSchedule;
            }*/
            $classList = $groupSchedule;
        }else{
            
        }

        $weekArr = array();
        $monthArr = array();

        if (isset($classList) && $classList){
            //$monthCount = 0;
            //$weekCount = 0;
            $i = 0;
            foreach ($classList as $key => $dateLoop){

                if (isset($dateLoop['sc_day'])){
                    switch($dateLoop['sc_day']){
                        case 'Monday':
                            $d[$key] = 1;
                            break;
                        case 'Tuesday':
                            $d[$key] = 2;
                            break;
                        case 'Wednesday':
                            $d[$key] = 3;
                            break;
                        case 'Thursday':
                            $d[$key] = 4;
                            break;
                        case 'Friday':
                            $d[$key] = 5;
                            break;
                        case 'Saturday':
                            $d[$key] = 6;
                            break;
                        case 'Sunday':
                            $d[$key] = 7;
                            break;
                    }
                }

                if ($dateLoop['sc_date']==null || $dateLoop['sc_date']==''){
                    if ($sdate){
                        $i = 0;
                        foreach ($sdate as $sdateLoop){
                            if ($sdateLoop['ctd_class_status']==1) {
                                $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                                $i++;

                                $month = date('F', strtotime($sdateLoop['ctd_scdate']));
                                $week = date('W', strtotime($sdateLoop['ctd_scdate']));

                                if (!in_array($month, $monthArr)) {
                                    array_push($monthArr, $month);
                                }

                                if (!in_array($week, $weekArr)) {
                                    array_push($weekArr, $week);
                                }
                            }
                        }
                    }else{
                        if ($key > 0){
                            $dkey = $d[$key]-$d[($key-1)];
                            $plusdate = $dkey.' day';
                            $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                        }else{
                            $start_day = date('l', strtotime($groupInfo['class_start_date']));

                            switch($start_day){
                                case 'Monday':
                                    $start_day_no = 1;
                                    break;
                                case 'Tuesday':
                                    $start_day_no = 2;
                                    break;
                                case 'Wednesday':
                                    $start_day_no = 3;
                                    break;
                                case 'Thursday':
                                    $start_day_no = 4;
                                    break;
                                case 'Friday':
                                    $start_day_no = 5;
                                    break;
                                case 'Saturday':
                                    $start_day_no = 6;
                                    break;
                                case 'Sunday':
                                    $start_day_no = 7;
                                    break;
                            }
                            $dkey = $d[$key]-$start_day_no;
                            //var_dump($dkey);
                            $plusdate = $dkey.' day';
                            $groupInfo['class_start_date'] = $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                            //$date = $groupInfo['class_start_date'];
                        }

                        while ($date <= $groupInfo['class_end_date']){
                            $classList[$i]['sc_date'] = $date;

                            $month = date('F', strtotime($date));
                            $week = date('W', strtotime($date));

                            $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                            $i++;

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }
                    //redirect here
                    //$this->_helper->flashMessenger->addMessage(array('error' => 'Please Set Schedule Date For Section "'.$groupInfo['GroupName'].'" Before View Attendance'));
                    //$this->_redirect($this->baseUrl . '/generalsetup/course-group/group-list/idSubject/'.$groupInfo['IdSubject'].'/idSemester/'.$groupInfo['IdSemester']);
                }

                $month = date('F', strtotime($dateLoop['sc_date']));
                $week = date('W', strtotime($dateLoop['sc_date']));

                if (!in_array($month, $monthArr)){
                    array_push($monthArr, $month);
                }

                if (!in_array($week, $weekArr)){
                    array_push($weekArr, $week);
                }

                unset($classList[$key]['sc_id']);
                unset($classList[$key]['idGroup']);
                unset($classList[$key]['IdLecturer']);
                unset($classList[$key]['idCollege']);
                unset($classList[$key]['idBranch']);
                unset($classList[$key]['idClassType']);
                unset($classList[$key]['sc_day']);
                unset($classList[$key]['sc_start_time']);
                unset($classList[$key]['sc_end_time']);
                unset($classList[$key]['sc_venue']);
                unset($classList[$key]['sc_class']);
                unset($classList[$key]['sc_remark']);
                unset($classList[$key]['sc_createdby']);
                unset($classList[$key]['sc_createddt']);
            }
        }

        /*$this->view->groupInfo = $groupInfo;
        $this->view->studentList = $studentList;
        $this->view->classList = $classList;
        $this->view->monthArr = $monthArr;
        $this->view->weekArr = $weekArr;*/
        if ($groupSchedule){
            $classesAtt = count($presentAtt).'/'.count($classList);
            $percentageAtt = number_format(((count($presentAtt)/count($classList))*100), 0, '.', ',');
        }else{
            $classesAtt = '0/0';
            $percentageAtt = 0;
        }
        
        $returnArr = array(
            'classesnAtt' => $classesAtt,
            'percentage' => $percentageAtt,
            'sectionname' => $groupInfo['GroupName'],
        );
        
        return $returnArr;
    }

    private function aasort(&$array, $key){
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
    }
}