<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_ErpUserSyncController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $model;
    private $conn;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->conn = Zend_Db_Table::getDefaultAdapter();
        $this->model = new GeneralSetup_Model_DbTable_ErpUserSync();
    }
    
    public function indexAction(){
        $this->view->title = $this->view->translate('Import ERP User Data');
        
        //$this->truncateTempTable();
        $fileList = $this->model->getFileName();
        
        $fileArr = array('.', '..');
        if ($fileList){
            foreach ($fileList as $fileLoop){
                array_push($fileArr, $fileLoop['erp_log_filename']);
            }
        }

        $this->view->fileArr = $fileArr;
        
        //scan file in folder
        $files = scandir(ERP_URL.'/Staff');
        $this->view->file = $files;
    }
    
    public function importFileAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $fileList = $this->model->getFileName();
        
        $fileArr = array('.', '..');
        if ($fileList){
            foreach ($fileList as $fileLoop){
                array_push($fileArr, $fileLoop['erp_log_filename']);
            }
        }
        
        //scan file in folder
        $files = scandir(ERP_URL.'/Staff');
        
        $logArr = array();
        $logfilename = array('');
        if ($files){
            foreach ($files as $fileLoop){
                if (is_dir($fileLoop)==false){
                    if ($fileLoop != '.' && $fileLoop != '..'){
                        if (in_array($fileLoop, $fileArr)==false){
                            $log = $this->process($fileLoop);

                            if ($log){
                                foreach ($log as $logLoop){
                                    array_push($logArr, $logLoop);
                                }
                            }
                        }
                    }
                }
            }
        }

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Import Success'));
        $this->_redirect($this->baseUrl.'/generalsetup/erp-user-sync/log/max/'.max($logArr).'/min/'.min($logArr));
    }
    
    public function logAction(){
        $this->view->title = $this->view->translate('Import ERP User Data Log');
        $max = $this->_getParam('max', 0);
        $min = $this->_getParam('min', 0);
        //var_dump($filename);
        $log = $this->model->getLog($max, $min);
        $this->view->log = $log;
    }
    
    public function logAllAction(){
        $this->view->title = $this->view->translate('Import ERP User Data Log Overall');
        
        $log = $this->model->getLogAll();
        $this->view->log = $log;
    }
    
    public function process($filesname){
        $logArr = array();
        $this->truncateTempTable();
        
        $content = file_get_contents(ERP_URL.'/Staff/'.$filesname);
        $lines = explode("\n", $content);
        
        if ($lines){
            foreach ($lines as $key => $line){
                
                if ($key > 0){
                    $lineArr = explode(',', $line);

                    $temp = array(
                        'Salutation'=>strip_tags(str_replace('"', '', utf8_decode($lineArr[0]))), 
                        'Staff_ID'=>strip_tags(str_replace('"', '', $lineArr[1])), 
                        'First_Name'=>strip_tags(str_replace('"', '', $lineArr[2])),
                        'Last_Name'=>strip_tags(str_replace('"', '', $lineArr[3])), 
                        'Email_Address'=>strip_tags(str_replace('"', '', $lineArr[4])), 
                        'Phone_No_Mobile'=>strip_tags(str_replace('"', '', $lineArr[5])), 
                        'Phone_No_Office'=>strip_tags(str_replace('"', '', $lineArr[6])), 
                        'Department_Code'=>strip_tags(str_replace('"', '', $lineArr[7])), 
                        'Department_Description'=>strip_tags(str_replace('"', '', $lineArr[8])), 
                        'Area_of_Interest'=>strip_tags(str_replace('"', '', $lineArr[9])), 
                        'Staff_type'=>strip_tags(str_replace('"', '', $lineArr[10])), 
                        'Status'=>strip_tags(str_replace('"', '', $lineArr[11]))
                    );
                    $this->insertDataToTempTable($temp);
                }
            }
        }

        /*$this->conn->beginTransaction();
        try{
            $this->importDataToTempTable($filesname);
            $this->conn->commit();
            
            //todo insert log
            $logData = array(
                'erp_log_filename'=>$filesname,
                'erp_log_process'=>'import data to temp table', 
                'erp_log_message'=>'Success', 
                'erp_log_date'=>date('Y-m-d h:i:s')
            );
            $logId = $this->insertLog($logData);
            array_push($logArr, $logId);
        } catch (Exception $ex) {
            $this->conn->rollBack();

            //todo insert log
            $logData = array(
                'erp_log_filename'=>$filesname,
                'erp_log_process'=>'import data to temp table', 
                'erp_log_message'=>$ex, 
                'erp_log_date'=>date('Y-m-d h:i:s')
            );
            $logId = $this->insertLog($logData);
            array_push($logArr, $logId);
        }*/
        
        $this->conn->beginTransaction();
        try{
            $data = $this->getTempData();
            $this->conn->commit();
            
            //todo insert log
            $logData = array(
                'erp_log_filename'=>$filesname,
                'erp_log_process'=>'get data from temp table', 
                'erp_log_message'=>'Success', 
                'erp_log_date'=>date('Y-m-d h:i:s')
            );
            $logId = $this->insertLog($logData);
            array_push($logArr, $logId);
        } catch (Exception $ex) {
            $this->conn->rollBack();
            
            //todo insert log
            $logData = array(
                'erp_log_filename'=>$filesname,
                'erp_log_process'=>'get data from temp table', 
                'erp_log_message'=>$ex, 
                'erp_log_date'=>date('Y-m-d h:i:s')
            );
            $logId = $this->insertLog($logData);
            array_push($logArr, $logId);
        }

        if ($data){
            foreach ($data as $dataLoop){
                //check if staff already exist
                $checkStaff = $this->checkStaff(str_replace('"', '', $dataLoop['Staff_ID']));

                //define department
                $departmentid = 0;
                switch (trim($dataLoop['Department_Description'])){
                    case "INC- Quality Assurance";
                        $departmentid = 22;
                        break;
                    case "INC-Admission and Student Affairs";
                        $departmentid = 23;
                        break;
                    case "INC-CAO's office";
                        $departmentid = 24;
                        break;
                    case "INC-CEP";
                        $departmentid = 25;
                        break;
                    case "INC-CIFP";
                        $departmentid = 26;
                        break;
                    case "INC-COO's Office";
                        $departmentid = 27;
                        break;
                    case "INC-Finance";
                        $departmentid = 28;
                        break;
                    case "INC-FMP&HSE";
                        $departmentid = 29;
                        break;
                    case "INC-FPCHI Officer";
                        $departmentid = 30;
                        break;
                    case "INC-Governance";
                        $departmentid = 31;
                        break;
                    case "INC-Human Capital";
                        $departmentid = 32;
                        break;
                    case "INC-ICT";
                        $departmentid = 33;
                        break;
                    case "INC-Knowledge Management Centre";
                        $departmentid = 34;
                        break;
                    case "INC-PCEO's Office";
                        $departmentid = 35;
                        break;
                    case "INC-Research and Publication";
                        $departmentid = 36;
                        break;
                    case "INC-School of Graduate Studies";
                        $departmentid = 20;
                        break;
                    case "INC-School of Professional Studies";
                        $departmentid = 19;
                        break;
                    case "INC-Strategic Planning & Corporate Affairs";
                        $departmentid = 39;
                        break;
                    case "ISRA-Consultancy";
                        $departmentid = 40;
                        break;
                    case "ISRA-Executive Director Officer";
                        $departmentid = 41;
                        break;
                    case "ISRA-General Affairs";
                        $departmentid = 42;
                        break;
                    case "ISRA-Research Affairs";
                        $departmentid = 43;
                        break;
                }

                if ($checkStaff){
                    $status = preg_replace("/[\n\r]/","", str_replace('"', '', $dataLoop['Status']));
                    $staffType = preg_replace("/[\n\r]/","",str_replace('"', '', $dataLoop['Staff_type']));
                    
                    $userData = array(
                        'StaffId'=>str_replace('"', '', $dataLoop['Staff_ID']),
                        'FirstName'=>str_replace('"', '', $dataLoop['Salutation'].' '.$dataLoop['First_Name'].' '.$dataLoop['Last_Name']), 
                        'FullName'=>str_replace('"', '', $dataLoop['Salutation'].' '.$dataLoop['First_Name'].' '.$dataLoop['Last_Name']), 
                        'Phone'=>str_replace('"', '', $dataLoop['Phone_No_Office']), 
                        'Mobile'=>str_replace('"', '', $dataLoop['Phone_No_Mobile']), 
                        'Email'=>str_replace('"', '', $dataLoop['Email_Address']),
                        'Active'=>$status=='Active' ? 1:0,
                        'IdCollege'=>$departmentid,
                        'UpdDate'=>date('Y-m-d h:i:s'), 
                        'UpdUser'=>1, 
                        'StaffAcademic'=>$staffType=='Academician' ? 0:1,
                    );
                    $this->updateUserData($userData, $checkStaff['IdStaff']);
                    
                    //todo insert log
                    $logData = array(
                        'erp_log_filename'=>$filesname,
                        'erp_log_process'=>'update data '.$dataLoop['Staff_ID'], 
                        'erp_log_message'=>'Success', 
                        'erp_log_date'=>date('Y-m-d h:i:s')
                    );
                    $logId = $this->insertLog($logData);
                    array_push($logArr, $logId);
                }else{
                    $status = preg_replace("/[\n\r]/","", str_replace('"', '', $dataLoop['Status']));
                    $staffType = preg_replace("/[\n\r]/","",str_replace('"', '', $dataLoop['Staff_type']));
                    
                    $userData = array(
                        'StaffId'=>str_replace('"', '', $dataLoop['Staff_ID']),
                        'FirstName'=>str_replace('"', '', $dataLoop['Salutation'].' '.$dataLoop['First_Name'].' '.$dataLoop['Last_Name']), 
                        'FullName'=>str_replace('"', '', $dataLoop['Salutation'].' '.$dataLoop['First_Name'].' '.$dataLoop['Last_Name']), 
                        'Phone'=>str_replace('"', '', $dataLoop['Phone_No_Office']), 
                        'Mobile'=>str_replace('"', '', $dataLoop['Phone_No_Mobile']), 
                        'Email'=>str_replace('"', '', $dataLoop['Email_Address']), 
                        'Active'=>$status=='Active' ? 1:0,
                        'IdCollege'=>$departmentid,
                        'UpdDate'=>date('Y-m-d h:i:s'), 
                        'UpdUser'=>1, 
                        'StaffAcademic'=>$staffType=='Academician' ? 0:1,
                    );
                    $this->insertUserData($userData);
                    
                    //todo insert log
                    $logData = array(
                        'erp_log_filename'=>$filesname,
                        'erp_log_process'=>'insert data '.$dataLoop['Staff_ID'], 
                        'erp_log_message'=>'Success', 
                        'erp_log_date'=>date('Y-m-d h:i:s')
                    );
                    $logId = $this->insertLog($logData);
                    array_push($logArr, $logId);
                }
            }
        }else{
            //todo insert log
            $logData = array(
                'erp_log_filename'=>$filesname,
                'erp_log_process'=>'empty data', 
                'erp_log_message'=>'-', 
                'erp_log_date'=>date('Y-m-d h:i:s')
            );
            $logId = $this->insertLog($logData);
            array_push($logArr, $logId);
        }
        
        $this->conn->beginTransaction();
        try{
            $this->truncateTempTable();
            $this->conn->commit();
            
            //todo insert log
            $logData = array(
                'erp_log_filename'=>$filesname,
                'erp_log_process'=>'truncate temp table', 
                'erp_log_message'=>'Success', 
                'erp_log_date'=>date('Y-m-d h:i:s')
            );
            $logId = $this->insertLog($logData);
            array_push($logArr, $logId);
            
        } catch (Exception $ex) {
            $this->conn->rollBack();
            
            //todo insert log
            $logData = array(
                'erp_log_filename'=>$filesname,
                'erp_log_process'=>'truncate temp table', 
                'erp_log_message'=>$ex, 
                'erp_log_date'=>date('Y-m-d h:i:s')
            );
            $logId = $this->insertLog($logData);
            array_push($logArr, $logId);
        }
        
        unlink(ERP_URL.'/Staff/'.$filesname);
        
        return $logArr;
    }
    
    public function importDataToTempTable($filesname){
        
        $sql = "LOAD DATA INFILE '".ERP_URL."/Staff/".$filesname."' ";
        $sql .= "INTO TABLE erp_user_sync ";
        $sql .= "FIELDS TERMINATED BY ',' ";
        $sql .= "ENCLOSED BY ";
        $sql .= "'";
        $sql .= '"';
        $sql .= "' ";
        $sql .= "LINES TERMINATED BY '\n'";
        $sql .= "IGNORE 1 ROWS";
        
        $execute = $this->conn->query($sql);
        return $execute;
    }
    
    public function insertDataToTempTable($data){
        $insert = $this->conn->insert('erp_user_sync', $data);
        return $insert;
    }
    
    public function truncateTempTable(){
        $sql = "TRUNCATE TABLE erp_user_sync";
        $execute = $this->conn->query($sql);
        return $execute;
    }
    
    public function getTempData(){
        $sql = $this->conn->select()->from(array('a'=>'erp_user_sync'));
        $execute = $this->conn->fetchAll($sql);
        return $execute;
    }
    
    public function insertUserData($data){
        $this->conn->insert('tbl_staffmaster', $data);
        $id = $this->conn->lastInsertId('tbl_staffmaster', 'IdStaff');
        return $id;
    }
    
    public function updateUserData($data, $id){
        $update = $this->conn->update('tbl_staffmaster', $data, 'IdStaff = '.$id);
        return $update;
    }
    
    public function insertLog($data){
        $this->conn->insert('erp_user_sync_log', $data);
        $id = $this->conn->lastInsertId('erp_user_sync_log', 'erp_log_id');
        return $id;
    }
    
    public function checkStaff($id){
        $sql = $this->conn->select()->from(array('a'=>'tbl_staffmaster'))
            ->where('a.StaffId = ?', $id);
        $execute = $this->conn->fetchRow($sql);
        return $execute;
    }
}
?>