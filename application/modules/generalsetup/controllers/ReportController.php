<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 24/1/2017
 * Time: 11:54 AM
 */
class GeneralSetup_ReportController extends Base_Base {

    private $_gobjlog;
    private $locale;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
    }

    public function loginReportAction(){
        $this->view->title = $this->view->translate('Login Report');

        $form = new GeneralSetup_Form_LoginReport();
        $this->view->form = $form;

        $model = new GeneralSetup_Model_DbTable_Report();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);

            $list = $model->getLoginInformation($formData);
            $this->view->list = $list;
        }
    }

    public function printLoginReportAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $this->_helper->layout->disableLayout();
        $model = new GeneralSetup_Model_DbTable_Report();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $list = $model->getLoginInformation($formData);
            $this->view->list = $list;
        }

        $this->view->filename = date('Ymd').'_login_report.xls';
    }
}