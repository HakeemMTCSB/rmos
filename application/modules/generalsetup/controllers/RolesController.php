<?php
class GeneralSetup_RolesController extends Base_Base {
	private $lobjprogrammaster;
	private $lobjcoursemasterForm;
	private $lobjRoles;
	private $lobjdeftype;
	private $_gobjlog;
	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	       
	}
	
	public function fnsetObj(){
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjcoursemaster = new GeneralSetup_Model_DbTable_Coursemaster();
		$this->lobjcoursemasterForm = new GeneralSetup_Form_Coursemaster (); 
		$this->lobjRoles = new GeneralSetup_Model_DbTable_Roles ();
	}
	
	public function indexAction() {
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
    	$this->view->title="Roles Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjdeftype->fnGetDefinations('Role');

		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->rolepaginatorresult);
   	    	
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->rolepaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->rolepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjRoles->fnSearchRoles ( $this->lobjform->getValues (),'Role' ); //searching the values for the user
				//$this->view->paginator = $this->Paginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->rolepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'coursemaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/generalsetup/roles/index');
		}
		
	}
	
	public function addroleAction(){
		$this->view->title="Add Role";  //title
		
		$id = $this->_getParam('id');
		
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		 
		$lobjMaintainanceForm = new GeneralSetup_Form_Maintenance();		 
		
	    $this->view->lobjMaintainanceForm = $lobjMaintainanceForm;
		
		if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($lobjMaintainanceForm->isValid($formData)) {
	    		
	    		$formData['idDefType']=1;	    		
	    		$definationDB = new App_Model_General_DbTable_Definationms();
	    		$definationDB->insert($formData);
	    		
	    		$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'roles', 'action'=>'index'),'default',true));
	    	}
		}
		
	}
 
	public function editroleAction(){
    	$this->view->title="Edit Role";  //title
    	
    	$id = $this->_getparam('id');
    	
    	$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		 
		$lobjMaintainanceForm = new GeneralSetup_Form_Maintenance();		 
		
	   $definationDB = new App_Model_General_DbTable_Definationms();
	   $role = $definationDB->getData($id);
	
	   $lobjMaintainanceForm->populate($role);
	   $this->view->lobjMaintainanceForm = $lobjMaintainanceForm;
		
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($lobjMaintainanceForm->isValid($formData)) {
	    		
	    		$definationDB = new App_Model_General_DbTable_Definationms();
	    		
	    		$data=array(
							'DefinitionCode' => $formData['DefinitionCode'],
							'DefinitionDesc' => $formData['DefinitionDesc'],
							'Status' 		 => $formData['Status'],
							'BahasaIndonesia' => $formData['BahasaIndonesia'],
							'Description' => $formData['Description']
						);
										
	    		$definationDB->update($data,"idDefinition = '".$formData['idDefinition'] . " ' ");
	    		$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'roles', 'action'=>'index'),'default',true));
	    	}
	    	
    	}
    }
}