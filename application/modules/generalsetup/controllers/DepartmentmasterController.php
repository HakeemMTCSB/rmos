<?php

class GeneralSetup_DepartmentmasterController extends Base_Base
{ //Controller for the User Module

    private $_gobjlog;

    public function init()
    {
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        $this->fnsetObj();
        $this->gstrsessionSIS = new Zend_Session_Namespace('sis');
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        Zend_Form::setDefaultTranslator($this->view->translate);
    }

    public function fnsetObj()
    {
        $this->lobjstaffmodel = new GeneralSetup_Model_DbTable_Staffmaster();
        $this->lobjhodmodel = new GeneralSetup_Model_DbTable_Headofdepartment();
        $this->lobjDepartmentmaster = new GeneralSetup_Model_DbTable_Departmentmaster();
    }

    public function indexAction()
    { // action for search and view
        $pageCount = 50;
        $this->view->title = $this->view->translate('Unit Setup');
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $lobjform = new App_Form_Search (); //intialize search lobjuserForm
        $this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
        $lobjDepartmentmaster = new GeneralSetup_Model_DbTable_Departmentmaster(); //user model object
        if ($this->gobjsessionsis->UserCollegeId == '0') {
            $larrresult = $lobjDepartmentmaster->fngetDepartmentDetails(); //get user details
        } else {
            $larrresult = $lobjDepartmentmaster->fngetDepartementmasterDetailsById($this->gobjsessionsis->UserCollegeId); //get user details
        }

        $lobjColgBranchList = $lobjDepartmentmaster->fnGetColgBranchList();
        $lobjform->field5->addMultiOptions($lobjColgBranchList);

        if (!$this->_getParam('search'))
            unset($this->gobjsessionsis->departmentpaginatorresult);

        $lintpagecount = $this->gintPageCount;
        $lobjPaginator = new App_Model_Common(); // Definitiontype model
        $lintpage = $this->_getParam('page', 1); // Paginator instance

        if (isset($this->gobjsessionsis->departmentpaginatorresult)) {
            $this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->departmentpaginatorresult, $lintpage, $lintpagecount);
        } else {
            $this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
        }


        if ($this->_request->isPost() && $this->_request->getPost('Search')) {

            $larrformData = $this->_request->getPost();
            if ($lobjform->isValid($larrformData)) {


                if ($this->gobjsessionsis->UserCollegeId == '0') {
                    $larrresult = $lobjDepartmentmaster->fnSearchDepartment($lobjform->getValues()); //searching the values for the user
                } else {
                    $larrresult = $lobjDepartmentmaster->fnSearchUserDepartment($lobjform->getValues(), $this->gobjsessionsis->UserCollegeId); //get user details
                }

                $this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
                $this->gobjsessionsis->departmentpaginatorresult = $larrresult;
            }
        }


        if ($this->_request->isPost() && $this->_request->getPost('Clear')) {


            //$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'departmentmaster', 'action'=>'index'),'default',true));
            $this->_redirect($this->baseUrl . '/generalsetup/departmentmaster/index');
        }

        $paginator = Zend_Paginator::factory($larrresult);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);

        $totalRecord = $paginator->getTotalItemCount();

        $this->view->paginator = $paginator;
        $this->view->totalRecord = $totalRecord;
    }

    public function newdepartmentmasterAction()
    { //Action for creating the new user
        $this->view->title = $this->view->translate('Add Unit');

        $lobjDepartmentmasterForm = new GeneralSetup_Form_Departmentmaster(); //intialize user lobjuserForm
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;

        $this->view->lobjdepartmentmasterForm = $lobjDepartmentmasterForm; //send the lobjuserForm object to the view
        //$this->view->lobjdepartmentmasterForm->DepartmentName->setAttrib('validator', 'validateDepartmentname');

        $lobjDepartmentmaster = new GeneralSetup_Model_DbTable_Departmentmaster(); //intialize user Model

        $idUniversity = $this->gobjsessionsis->idUniversity;
        $lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
        $initialConfig = $lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);

        if ($initialConfig['DepartmentCodeType'] == 1) {
            $this->view->lobjdepartmentmasterForm->DeptCode->setAttrib('readonly', 'true');
            $this->view->lobjdepartmentmasterForm->DeptCode->setValue('xxx-xxx-xxx');
        } else {
            $this->view->lobjdepartmentmasterForm->DeptCode->setAttrib('required', 'true');
            //$this->view->lobjdepartmentmasterForm->DeptCode->setAttrib('validator', 'validateDepartmentCode');
        }

        $ldtsystemDate = date('Y-m-d H:i:s');
        $this->view->lobjdepartmentmasterForm->UpdDate->setValue($ldtsystemDate);
        $auth = Zend_Auth::getInstance();
        $this->view->lobjdepartmentmasterForm->UpdUser->setValue($auth->getIdentity()->iduser);
        $this->view->lobjdepartmentmasterForm->IdStaff->addMultiOptions($this->lobjstaffmodel->fngetStaffMasterListforDD());
        if ($this->gobjsessionsis->UserCollegeId == '0') {
            $lobjCollegeList = $lobjDepartmentmaster->fnGetCollegeList();
        } else {
            $lobjCollegeList = $lobjDepartmentmaster->fnGetUserCollegeList($this->gobjsessionsis->UserCollegeId);
        }
        $lobjDepartmentmasterForm->IdCollege->addMultiOptions($lobjCollegeList);

        //Country List
        $lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
        $lobjcountry = $lobjUser->fnGetCountryList();
        $lobjDepartmentmasterForm->idCountry->addMultiOptions($lobjcountry);


        //Affiliate
        //$lobjUniversity = new GeneralSetup_Model_DbTable_University(); //intialize user Model
        //$lobjUniversity = $lobjUniversity->fnGetUniversityList();
        //$lobjDepartmentmasterForm->AffiliatedTo->addMultiOptions($lobjUniversity);
        $lobjCollegeMaster = new GeneralSetup_Model_DbTable_Collegemaster();
        $larrDeptList = $lobjCollegeMaster->fnGetCollegeList();
        $lobjDepartmentmasterForm->AffiliatedTo->addMultiOptions($larrDeptList);


        if ($this->_request->isPost()) {

            $larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post
            $larrheadofdept = $larrformData;

            if ($lobjDepartmentmasterForm->isValid($larrformData)) {

                //add department info
                $result = $lobjDepartmentmaster->fnaddDepartment($larrformData, $idUniversity, $initialConfig['DepartmentCodeType']); //instance for adding the lobjuserForm values to DB

                //add head of department info
                $this->lobjhodmodel->fninsertHeadofDepartment($larrheadofdept, $result);

                //add department address
                $data['add_org_name'] = 'tbl_departmentmaster';
                $data['add_org_id'] = $result;
                $data['add_address_type'] = $larrformData['AddressType'];
                $data['add_country'] = $larrformData['idCountry'];
                $data['add_state'] = $larrformData['idState'];
                $data['add_address1'] = $larrformData['Addr1'];
                $data['add_address2'] = $larrformData['Addr2'];
                $data['add_zipcode'] = $larrformData['zipCode'];
                $data['add_phone'] = $larrformData['Phone'];
                $data['add_email'] = $larrformData['Email'];
                $data['add_affiliate'] = $larrformData['AffiliatedTo'];
                $data['add_createddt'] = date('Y-m-d H:i:s');
                $data['add_createdby'] = $auth->getIdentity()->iduser;
                $data['add_state_others'] = $larrformData['State_Others'];
                $data['add_city_others'] = $larrformData['City_Others'];
                $data['add_city'] = $larrformData['City'];

                $addressDb = new GeneralSetup_Model_DbTable_Address();
                $addressDb->addData($data);

                // Write Logs
                $priority = Zend_Log::INFO;
                $larrlog = array('user_id' => $auth->getIdentity()->iduser,
                    'level' => $priority,
                    'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'time' => date('Y-m-d H:i:s'),
                    'message' => 'New Department Add',
                    'Description' => Zend_Log::DEBUG,
                    'ip' => $this->getRequest()->getServer('REMOTE_ADDR'));

                $this->_gobjlog->write($larrlog); //insert to tbl_log

                $this->_redirect($this->baseUrl . '/generalsetup/departmentmaster/index/');
            }
        }
    }

    public function departmentmasterlistAction()
    { //Action for the updation and view of the user details

        $this->view->title = $this->view->translate('Edit Unit');

        $lobjDepartmentmasterForm = new GeneralSetup_Form_Departmentmaster(); //intialize user lobjuserForm
        $this->view->lobjdepartmentmasterForm = $lobjDepartmentmasterForm; //send the lobjuserForm object to the view
        $lobjDepartmentmaster = new GeneralSetup_Model_DbTable_Departmentmaster(); //intialize user Model
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $idUniversity = $this->gobjsessionsis->idUniversity;
        $lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
        $initialConfig = $lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);

        if ($initialConfig['DepartmentCodeType'] == 1) {
            $this->view->lobjdepartmentmasterForm->DeptCode->setAttrib('readonly', 'true');
            $this->view->lobjdepartmentmasterForm->DeptCode->setValue('xxx-xxx-xxx');
        } else {
            $this->view->lobjdepartmentmasterForm->DeptCode->setAttrib('required', 'true');
        }

        $lintidepartment = (int)$this->_getParam('id');
        $this->view->iddepartment = $lintidepartment;

        $lintidbrc = (int)$this->_getParam('idbrc');
        $this->view->lintidbrc = $lintidbrc;

        //$this->view->lobjdepartmentmasterForm->DeptCode->setAttrib('readonly','true');
        $this->view->lobjdepartmentmasterForm->IdStaff->addMultiOptions($this->lobjstaffmodel->fngetStaffMasterListforDD());
        $larrhoddetails = $this->lobjhodmodel->fngethodList($lintidepartment);
        $this->view->lobjdepartmentmasterForm->FromDate->setValue($larrhoddetails['FromDate']);
        $this->view->lobjdepartmentmasterForm->IdStaff->setValue($larrhoddetails['IdStaff']);
        $larrresult = $lobjDepartmentmaster->fnviewDepartment($lintidepartment); //getting user details based on userid

        if ($larrresult['DepartmentType'] == '0') {
            if ($this->gobjsessionsis->UserCollegeId == '0') {
                $lobjCollegeList = $lobjDepartmentmaster->fnGetCollegeList();
            } else {
                $lobjCollegeList = $lobjDepartmentmaster->fnGetUserCollegeList($this->gobjsessionsis->UserCollegeId);
//					echo "<pre>";
//					print_r($lobjCollegeList);die;
            }
            $lobjDepartmentmasterForm->IdCollege->addMultiOptions($lobjCollegeList);
        } else {

            if ($this->gobjsessionsis->UserCollegeId == '0') {
                $lobjBranchList = $lobjDepartmentmaster->fnGetBranchList();
            } else {
                $lobjBranchList = $lobjDepartmentmaster->fnGetUserBranchList($this->gobjsessionsis->UserCollegeId);
            }
            $lobjDepartmentmasterForm->IdCollege->addMultiOptions($lobjBranchList);
        }

        //Country List
        $lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
        $lobjcountry = $lobjUser->fnGetCountryList();
        $lobjDepartmentmasterForm->idCountry->addMultiOptions($lobjcountry);

        //Affiliate
        $lobjUniversity = new GeneralSetup_Model_DbTable_University(); //intialize user Model
        $lobjUniversity = $lobjUniversity->fnGetUniversityList();
        $lobjDepartmentmasterForm->AffiliatedTo->addMultiOptions($lobjUniversity);

        $lobjDepartmentmasterForm->populate($larrresult);


        $ldtsystemDate = date('Y-m-d H:i:s');
        $this->view->lobjdepartmentmasterForm->UpdDate->setValue($ldtsystemDate);
        $auth = Zend_Auth::getInstance();
        $this->view->lobjdepartmentmasterForm->UpdUser->setValue($auth->getIdentity()->iduser);

        if ($this->_request->isPost()) {

            $larrformData = $this->_request->getPost();

            if ($lobjDepartmentmasterForm->isValid($larrformData)) {

                $lintIdDepartment = $larrformData ['IdDepartment'];
                $this->lobjhodmodel->fnupdatehodList($larrformData, $lintIdDepartment);
                $this->lobjhodmodel->fninsertHeadofDepartment($larrformData, $lintIdDepartment);
                $lobjDepartmentmaster->fnupdateDepartment($larrformData, $lintIdDepartment);

                // Write Logs
                $priority = Zend_Log::INFO;
                $larrlog = array('user_id' => $auth->getIdentity()->iduser,
                    'level' => $priority,
                    'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'time' => date('Y-m-d H:i:s'),
                    'message' => 'Department Edit Id=' . $lintidepartment,
                    'Description' => Zend_Log::DEBUG,
                    'ip' => $this->getRequest()->getServer('REMOTE_ADDR'));
                $this->_gobjlog->write($larrlog); //insert to tbl_log

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->baseUrl . '/generalsetup/departmentmaster/departmentmasterlist/id/' . $lintidepartment);
            }
        }
        $this->view->lobjdepartmentmasterForm = $lobjDepartmentmasterForm;
    }

    public function getunivesitycolglistAction()
    {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $lobjCommonModel = new App_Model_Common();
        $lobjDepartmentModel = new GeneralSetup_Model_DbTable_Departmentmaster();
        $lintvalue = $this->_getParam('value');
        if ($lintvalue == '0') {
            $larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($lobjDepartmentModel->fnGetUniversityList());
        } else {
            if ($this->gobjsessionsis->UserCollegeId == '0') {
                $larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($lobjDepartmentModel->fnGetCollegeList());
            } else {
                $larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($lobjDepartmentModel->fnGetUserCollegeList($this->gobjsessionsis->UserCollegeId));
            }
        }
        echo Zend_Json_Encoder::encode($larrDetails);
    }

    public function getcolgbranchlistAction()
    {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $lobjCommonModel = new App_Model_Common();
        $lobjDepartmentModel = new GeneralSetup_Model_DbTable_Departmentmaster();
        $lintvalue = $this->_getParam('value');


        if ($lintvalue == '0') {

            if ($this->gobjsessionsis->UserCollegeId == '0') {
                $larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($lobjDepartmentModel->fnGetCollegeList());
            } else {
                $larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($lobjDepartmentModel->fnGetUserCollegeList($this->gobjsessionsis->UserCollegeId));
            }
        } else {

            if ($this->gobjsessionsis->UserCollegeId == '0') {
                $larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($lobjDepartmentModel->fnGetBranchList());
            } else {
                $larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($lobjDepartmentModel->fnGetUserBranchList($this->gobjsessionsis->UserCollegeId));
            }
        }
        echo Zend_Json_Encoder::encode($larrDetails);
    }

    public function getdepartmentnameAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $DeptName = $this->_getParam('DeptName');
        $larrDetails = $this->lobjDepartmentmaster->fngetDeptname($DeptName);
        echo $larrDetails['DepartmentName'];
    }

    public function getdepartmentcodeAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $DeptCode = $this->_getParam('DeptCode');
        $larrDetails = $this->lobjDepartmentmaster->fngetDeptCode($DeptCode);
    }

    public function deleteDepartmentAction()
    {
        $id = $this->_getParam('id', 0);
        $this->lobjDepartmentmaster->deleteDepartment($id);

        $addressDb = new GeneralSetup_Model_DbTable_Address();
        $addressDb->deleteOrgAddress($id, 'tbl_departmentmaster');

        $this->_redirect($this->baseUrl . '/generalsetup/departmentmaster');
    }

    public function departmentAddressAction()
    {

        $this->_helper->layout->disableLayout();
        $id_dept = $this->_getParam('id', 0);
        $this->view->idDepartment = $id_dept;

        //get list branch address
        $addressDb = new GeneralSetup_Model_DbTable_Address();
        $this->view->address_list = $addressDb->getDataByOrgId($id_dept, 'tbl_departmentmaster');

    }

    public function addAddressAction()
    {

        $this->_helper->layout->disableLayout();
        $id_dept = $this->_getParam('id', 0);

        $form = new GeneralSetup_Form_AddressForm(array('idBranch' => $id_dept));
        $this->view->addressForm = $form;

        if ($this->_request->isPost()) {

            $formData = $this->_request->getPost();

            if ($form->isValid($formData)) {

                //save address
                $auth = Zend_Auth::getInstance();

                $data['add_org_name'] = 'tbl_departmentmaster';
                $data['add_org_id'] = $formData['idBranch']; //is refer to id department. Use the same form as branch
                $data['add_address_type'] = $formData['address_type'];
                $data['add_country'] = $formData['country'];
                $data['add_state'] = $formData['state'];
                $data['add_address1'] = $formData['address1'];
                $data['add_address2'] = $formData['address2'];
                $data['add_zipcode'] = $formData['zipcode'];
                $data['add_phone'] = $formData['phone'];
                $data['add_email'] = $formData['email'];
                $data['add_createddt'] = date('Y-m-d H:i:s');
                $data['add_createdby'] = $auth->getIdentity()->iduser;
                $data['add_city'] = $formData['city'];
                $data['add_state_others'] = $formData['state_others'];
                $data['add_city_others'] = $formData['city_others'];

                $addressDb = new GeneralSetup_Model_DbTable_Address();
                $addressDb->addData($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "Address successfully added"));

                $this->_redirect($this->baseUrl . '/generalsetup/departmentmaster/departmentmasterlist/id/' . $formData['idBranch'] . '#tab2');
            }
        }
    }

    public function editAddressAction()
    {

        $this->_helper->layout->disableLayout();
        $id_dept = $this->_getParam('id', 0);
        $add_id = $this->_getParam('add_id', 0);

        $addressDb = new GeneralSetup_Model_DbTable_Address();

        $form = new GeneralSetup_Form_AddressForm(array('idBranch' => $id_dept, 'addId' => $add_id));

        if ($this->_request->isPost()) {

            $formData = $this->_request->getPost();

            if ($form->isValid($formData)) {

                $auth = Zend_Auth::getInstance();

                //get old (address before change)
                $address = $addressDb->getDatabyId($formData['add_id']);

                //insert into history
                $olddata['add_id'] = $address['add_id'];
                $olddata['addh_org_name'] = $address['add_org_name'];
                $olddata['addh_org_id'] = $address['add_org_id'];
                $olddata['addh_address_type'] = $address['add_address_type'];
                $olddata['addh_country'] = $address['add_country'];
                $olddata['addh_state'] = $address['add_state'];
                $olddata['addh_address1'] = $address['add_address1'];
                $olddata['addh_address2'] = $address['add_address2'];
                $olddata['addh_zipcode'] = $address['add_zipcode'];
                $olddata['addh_phone'] = $address['add_phone'];
                $olddata['addh_email'] = $address['add_email'];
                $olddata['addh_createddt'] = $address['add_createddt'];
                $olddata['addh_createdby'] = $address['add_createdby'];
                $olddata['addh_changedt'] = date('Y-m-d H:i:s');
                $olddata['addh_changeby'] = $auth->getIdentity()->iduser;
                $olddata['addh_city'] = $address['add_city'];
                $olddata['addh_state_others'] = $address['add_state_others'];
                $olddata['addh_city_others'] = $address['add_city_others'];

                $addHistoryDb = new GeneralSetup_Model_DbTable_AddressHistory();
                $addHistoryDb->addData($olddata);

                //edit
                $data['add_address_type'] = $formData['address_type'];
                $data['add_country'] = $formData['country'];
                $data['add_state'] = $formData['state'];
                $data['add_address1'] = $formData['address1'];
                $data['add_address2'] = $formData['address2'];
                $data['add_zipcode'] = $formData['zipcode'];
                $data['add_phone'] = $formData['phone'];
                $data['add_email'] = $formData['email'];
                $data['add_modifydt'] = date('Y-m-d H:i:s');
                $data['add_modifyby'] = $auth->getIdentity()->iduser;
                $data['add_city'] = $formData['city'];
                $data['add_state_others'] = $formData['state_others'];
                $data['add_city_others'] = $formData['city_others'];

                $addressDb->updateData($data, $formData['add_id']);

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->baseUrl . '/generalsetup/departmentmaster/departmentmasterlist/id/' . $formData['idBranch'] . '#tab2');

            }

        } else {


            $add = $addressDb->getDatabyId($add_id);

            //get state to populate state from country
            /*$stateDB = new App_Model_General_DbTable_State();
            $state_data = $stateDB->getState($add['add_country']);
            $element = $form->getElement('state');
            foreach($state_data as $s){
            $element->addMultiOption($s['idState'],$s['StateName']);
            }*/

            $address_data['address_type'] = $add['add_address_type'];
            $address_data['country'] = $add['add_country'];
            $address_data['state'] = $add['add_state'];
            $address_data['address1'] = $add['add_address1'];
            $address_data['address2'] = $add['add_address2'];
            $address_data['zipcode'] = $add['add_zipcode'];
            $address_data['phone'] = $add['add_phone'];
            $address_data['email'] = $add['add_email'];
            $address_data['city'] = $add['add_city'];
            $address_data['state_others'] = $add['add_state_others'];
            $address_data['city_others'] = $add['add_city_others'];
            //print_r($address_data);

            $this->view->address_data = $address_data;

            $form->populate($address_data);
            $this->view->addressForm = $form;
        }

    }

    public function deleteAddressAction()
    {

        $id_dept = $this->_getParam('id', 0);
        $add_id = $this->_getParam('add_id', 0);

        $addressDb = new GeneralSetup_Model_DbTable_Address();
        $addressDb->deleteData($add_id);

        $this->_helper->flashMessenger->addMessage(array('success' => "Address deleted"));

        $this->_redirect($this->baseUrl . '/generalsetup/departmentmaster/departmentmasterlist/id/' . $id_dept . '#tab2');
        exit;
    }

    public function viewHistoryAction()
    {

        $this->_helper->layout->disableLayout();
        $id_dept = $this->_getParam('id', 0);

        //get list branch address
        $addressDb = new GeneralSetup_Model_DbTable_AddressHistory();
        $this->view->address_list = $addressDb->getDataByOrgId($id_dept, 'tbl_departmentmaster');
    }
}