<?php
class Application_SpecializationController extends Base_Base {
	//private $lobjspecializationmodel;
	//private $lobjspecializationform;
    private $_DbObj;

	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();
        $this->_DbObj = new Application_Model_DbTable_Specialization();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');

	}

	private function fnsetObj() {
		$this->lobjform = new App_Form_Search ();
		$this->lobjspecializationmodel = new Application_Model_Specialization();
		$this->lobjspecializationform = new Application_Form_Specialization();
        $this->lobjstudyfieldsubmap = new Application_Model_DbTable_SpecializationfieldMapping();
        $this->lobjEditSpecialization = new Application_Form_EditSpecialization();
        //$this->lobjspecializationsfmodel = new Application_Model_DbTable_Specializationstudyfield();
	}

	public function indexAction() {
        //title
        $this->view->title= $this->view->translate("Specialization");

        $this->view->searchForm = new Application_Form_SpecializationSearch();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            //paginator
            $data = $this->_DbObj->getPaginateData($formData);

            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
            $paginator->setItemCountPerPage(PAGINATION_SIZE);
            $paginator->setCurrentPageNumber($this->_getParam('page',1));
            $totalRecord = $paginator->getTotalItemCount();

            $this->view->paginator = $paginator;
            $this->view->totalRecord = $totalRecord;

        }else{
            //paginator
            $data = $this->_DbObj->getPaginateData();

            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
            $paginator->setItemCountPerPage(PAGINATION_SIZE);
            $paginator->setCurrentPageNumber($this->_getParam('page',1));
            $totalRecord = $paginator->getTotalItemCount();

            $this->view->paginator = $paginator;
            $this->view->totalRecord = $totalRecord;
        }
	}

    public function addAction() {


        $this->view->lobjspecializationform = $this->lobjspecializationform;
        ///$lobjcountry = $this->lobjUser->fnGetCountryList();
        ///$this->lobjHighschoolSubjectForm->Country->addMultiOptions($lobjcountry);
        ///$lobjinstype = $this->lobjRegistryvalue->getList('7');
        ///$lobjinstype = $this->lobjUser->fnGetInstitutionType();
        ///$this->lobjHighschoolSubjectForm->QualificationType->addMultiOptions($lobjinstype);
        $ldtsystemDate = date ( 'Y-m-d H:i:s' );
        $this->view->lobjspecializationform->UpdDate->setValue ( $ldtsystemDate );
        $auth = Zend_Auth::getInstance();
        $this->view->lobjspecializationform->UpdUser->setValue ( $auth->getIdentity()->iduser);

        //$this->view->lobjspecializationform->ss_core_subject->setValue ( '0' );

        $this->view->lobjspecializationform->sft_id->addValidator('Db_NoRecordExists', true, array(
                'table' => 'tbl_studyfield_type',
                'field' => 'sft_id'
            )
        );
        $this->view->lobjspecializationform->sft_id->getValidator('Db_NoRecordExists')->setMessage("Record already exists");

        $this->view->lobjspecializationform->sft_eng_desc->addValidator('Db_NoRecordExists', true, array(
                'table' => 'tbl_studyfield_type',
                'field' => 'sft_eng_desc'
            )
        );
        $this->view->lobjspecializationform->sft_eng_desc->getValidator('Db_NoRecordExists')->setMessage("Record already exists");


        if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
            $larrformData = $this->_request->getPost (); //getting the values of lobjInstitutionsetupForm data from post
            //dd($larrformData);
            //exit;
            unset ( $larrformData ['Save'] );
            if ($this->lobjspecializationform->isValid ( $larrformData )) {

                //$IdInstitution = $this->lobjHighschoolSubjectForm->fnaddInstitution($larrformData); //instance for adding the lobjInstitutionsetupForm values to DB

                $specializationMasterDb = new Application_Model_DbTable_Specialization();
                $id = $specializationMasterDb->addData($larrformData);

                $rowcount = count($larrformData['IdSubject']);


                for ($i = 0; $i < $rowcount; $i++) {
                    $mapping = array();
                    $mapping['IdFieldType'] = $id;
                    $mapping['IdField'] = $larrformData['IdSubject'][$i];
                    if ($larrformData['IdSubject'][$i]!='0') {
                        $this->lobjstudyfieldsubmap->fnadd($mapping);
                    }
                }


                // Write Logs
                $priority=Zend_Log::INFO;
                $larrlog = array ('user_id' => $auth->getIdentity()->iduser,
                    'level' => $priority,
                    'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'time' => date ( 'Y-m-d H:i:s' ),
                    'message' => 'Specialization Add',
                    'Description' =>  Zend_Log::DEBUG,
                    'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
                $this->_gobjlog->write ( $larrlog ); //insert to tbl_log

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully added"));
                $this->_redirect( $this->baseUrl . '/application/specialization/index');
            }
        }
    }

    public function detailAction(){
        $this->view->title= $this->view->translate("Specialization")." - ".$this->view->translate("Detail");

        $this->view->searchForm = new Application_Form_SpecializationSearch();

        $id = $this->_getParam('id', null);
        $this->view->id = $id;


        if($id) {
            //subject data
            $result = $this->_DbObj->getData($id);
            $this->view->paginator = array($result);

            if ($this->getRequest()->isPost()) {

                $formData = $this->getRequest()->getPost();

                //paginator
                $data = $this->_DbObj->getPaginateData($formData);

                $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
                $paginator->setItemCountPerPage(PAGINATION_SIZE);
                $paginator->setCurrentPageNumber($this->_getParam('page',1));
                $totalRecord = $paginator->getTotalItemCount();

                $this->view->paginator = $paginator;
                $this->view->totalRecord = $totalRecord;


            }

        }

        else{
            $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'highschool-subject', 'action'=>'index'),'default',true));
        }
    }

    public function editAction(){

        $this->view->lobjEditSpecialization = $this->lobjEditSpecialization;

        $ldtsystemDate = date ( 'Y-m-d H:i:s' );
        $this->view->lobjEditSpecialization->UpdDate->setValue ( $ldtsystemDate );
        $auth = Zend_Auth::getInstance();
        $this->view->lobjEditSpecialization->UpdUser->setValue ( $auth->getIdentity()->iduser);


        //$this->view->form = new GeneralSetup_Form_EditSchoolSubject();

        //$this->view->lobjEditSchoolSubject = $this->lobjEditSchoolSubject;

        //$lobjquatype = $this->lobjQua->fngetQualificationList();
        //$this->lobjEditSchoolSubject->Qualification->addMultiOptions($lobjquatype);

        $id = $this->_getParam('id', 0);
        $this->view->id = $id;

        //title
        $this->view->title= $this->view->translate("Subject Set-up")." - ".$this->view->translate("Edit");

        //$form = new GeneralSetup_Form_EditSchoolSubject();

        /* $lobjqua = new Application_Model_DbTable_Qualificationsetup();
         $lobjqualist = $lobjqua->fngetQualificationList();
         $this->view->lobjqualist = $lobjqualist;*/


        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($this->lobjEditSpecialization->isValid($formData)) {
                //dd($formData);
                 //exit;

                $idsub = $formData["idsub"];

                $this->_DbObj->updateData($formData, $idsub);
                $this->lobjstudyfieldsubmap->fndeletemappings($idsub);

                $rowcount = count($formData['IdSubject']);

                for ($i = 0; $i < $rowcount; $i++) {
                    $mapping = array();
                    $mapping['IdFieldType'] = $idsub;
                    $mapping['IdField'] = $formData['IdSubject'][$i];

                    //dd($mapping);
                    //exit;


                    //if ($formData['IdSubject']!='0' || $formData['IdSubject']!='') {
                    if (!empty($formData['IdSubject'])) {


                        $this->lobjstudyfieldsubmap->fnadd($mapping);
                    }
                }


                $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'specialization', 'action' => 'detail', 'id' => $idsub), 'default', true));
                /*}else{
                    $this->lobjEditSchoolSubject->populate($formData); */
            }
        } else {
            if ($id > 0) {

                //$form->populate($this->_DbObj->getData($id));
                //$this->view->acadecmisetup = $larrresult;
                $this->lobjEditSpecialization->populate($this->_DbObj->getData($id));



                $sfresultDetails = $this->lobjstudyfieldsubmap->fngetmappings($id);
                $this->view->sfresultDetails = $sfresultDetails;
            }
        }

        //$this->view->form = $form;
    }

	public function newspecializationAction() {
		$this->view->lobjspecializationform = $this->lobjspecializationform;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjspecializationform->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjspecializationform->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$this->view->lobjspecializationform->Specialization->addValidator('Db_NoRecordExists', true, array(
						'table' => 'tbl_specialization', 
						'field' => 'Specialization'
				)
		);
		$this->view->lobjspecializationform->Specialization->getValidator('Db_NoRecordExists')->setMessage("Record already exists");
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			unset($larrformData['Save']);
			if ($this->lobjspecializationform->isValid ( $larrformData )) {
				$this->lobjspecializationmodel->fnaddSpecialization($larrformData);
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
									  'level' => $priority,
									  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					                  'time' => date ( 'Y-m-d H:i:s' ),
					   				  'message' => 'New Specialization Add',
									  'Description' =>  Zend_Log::DEBUG,
									  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				$this->_redirect( $this->baseUrl . '/application/specialization/index');
			}
		}

	}

	
	public function editspecializationAction() {
		$this->view->lobjspecializationform = $this->lobjspecializationform;
		
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjspecializationform->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjspecializationform->UpdUser->setValue ( $auth->getIdentity()->iduser);
		
		$this->view->lobjspecializationform->Specialization->addValidator('Db_NoRecordExists', true, array(
						'table' => 'tbl_specialization', 
						'field' => 'Specialization',
						'exclude' => array(
								'field' => 'IdSpecialization',
								'value' => $this->_getParam('id', 0)
						)
				)
		);
		$this->view->lobjspecializationform->Specialization->getValidator('Db_NoRecordExists')->setMessage("Record already exists");
		
		$Idspecialization = $this->_getParam('id', 0);
		$result = $this->lobjspecializationmodel->fetchAll('IdSpecialization ='.$Idspecialization);
		$result = $result->toArray();
		foreach($result as $specialization){}
		$this->view->lobjspecializationform->populate($specialization);
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$formData = $this->_request->getPost ();
			unset($formData['Save']);
			if ($this->lobjspecializationform->isValid($formData)) {
				$this->lobjspecializationmodel->fnupdateSpecialization($formData, $Idspecialization);
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
									  'level' => $priority,
									  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					                  'time' => date ( 'Y-m-d H:i:s' ),
					   				  'message' => 'Update Specialization Id'.$Idspecialization,
									  'Description' =>  Zend_Log::DEBUG,
									  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				$this->_redirect( $this->baseUrl . '/application/specialization/index');
			}
		}
		
	}

	public function deletespecializationAction() {
		$this->view->lobjspecializationform = $this->lobjspecializationform;
		$IdSpecialization = $this->_getParam('id', 0);
		$this->lobjspecializationmodel->fndeleteSpecialiation($IdSpecialization);
		$this->_redirect( $this->baseUrl . '/application/specialization/index');
	}
}