<?php
class Application_placementestnopesController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjdeftype;
	private $lobformulir;
	private $_gobjlog;
	private $lobjformplnopes;

	public function init() { //initialization function
		$this->view->translate =Zend_Registry::get('Zend_Translate');
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();

	}

	public function fnsetObj() {
		//$this->lobformulir = new GeneralSetup_Form_Formulir(); //formulir model object
		$this->lobformulir= new App_Form_Search();
		$this->lobjformplnopes = new GeneralSetup_Form_placementtestnopes();

		$this->registry = Zend_Registry::getInstance();
		$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
		$this->lobjdeftype = new App_Model_Definitiontype();
	}


	public function indexAction() { // action for search and view
		//$lobjform=$this->view->lobjformplnopes = $this->lobjformplnopes;
		$lobjPlacementtestnopesModel = New GeneralSetup_Model_DbTable_Placementtestnopes();
		$this->view->lobjform = $this->lobformulir; //send the lobformulir object to the view

		$applicationtype = $this->lobjdeftype->fnGetDefinations('Application Type');


		$semester = new GeneralSetup_Model_DbTable_Semester();
		$semarray = $semester->fngetlandscapeSemesterDetails();



		$result=$lobjPlacementtestnopesModel->fnGetPlacementtestnopesDetails();

		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$this->view->paginator =  $this->lobjCommon->fnPagination($result,$lintpage,$lintpagecount);


		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			$larrresult =$lobjPlacementtestnopesModel->fnSearchPlacementestname($larrformData); //searching the values for the user
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
			$this->gobjsessionsis->userpaginatorresult = $larrresult;

		}
	}
	public function newAction() {
		$lintiduser = ( int ) $this->_getParam ( 'id' );
		//echo $lintiduser;die();
		$lobjPlacementtestnopesModel=New GeneralSetup_Model_DbTable_Placementtestnopes();
		$result=$lobjPlacementtestnopesModel->fnGetPlacementtestnopesDetailsyear($lintiduser);
		//echo "<pre>";
		//print_r($result);die();
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$this->view->paginator =  $this->lobjCommon->fnPagination($result,$lintpage,$lintpagecount);

	}

}