<?php

class Application_QuitController extends Base_Base {
	/**
	 * The default action - show the home page
	 */
	
	public function applicantListAction() {
		
		
		//title
    	$this->view->title="Quit Application List";
    	
    	
    	$applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
		
	   // $form = new Application_Form_Quit();
	    		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			/*
			$txnList = $applicantTxnDB->getQuitData($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($txnList));			
			$paginator->setItemCountPerPage(20);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));			
			$form->populate($formData);
			*/
			$db = Zend_Db_Table::getDefaultAdapter();
			
			$search_query = $db->select()
							->from(array('at'=>'applicant_transaction') )
							->join(array('ap'=>'applicant_profile'),'ap.appl_id=at.at_appl_id')
							->join(array('aq'=>'applicant_quit'),'aq.aq_trans_id=at.at_trans_id')
							->joinleft(array('rchq'=>'refund_cheque'),'rchq.rchq_id = aq.aq_cheque_id')
							->joinLeft(array('u'=>'tbl_user'), 'u.iduser = rchq.rchq_collector_update_by', array())
							->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('rchq_collector_update_by_name'=>'Fullname'))
							->joinLeft(array('i'=>'tbl_intake'),'i.IdIntake = at.at_intake')
							->joinLeft(array('aprd'=>'tbl_academic_period'),'aprd.ap_id = at.at_period')
							->where("at_quit_status != 0")
							->order('aq.aq_createddt desc');
			
			if( isset($formData['name']) &&  $formData['name']!=""){
				$this->view->search_name = $formData['name'];
				$search_query->where("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) like ?", '%'.$formData['name'].'%');
			}
			
			if( isset($formData['nofom']) &&  $formData['nofom']!=""){
				$this->view->search_nofom = $formData['nofom'];
				$search_query->where("at.at_pes_id like '%".$formData['nofom']."%'");
			}
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($search_query));
			$paginator->setItemCountPerPage(999999999999999999);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$this->view->search = true;
			
			
			
		}else{
			$txnList = $applicantTxnDB->getQuitPaginateData();
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($txnList));		
			$paginator->setItemCountPerPage(20);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
		}
		
		//$this->view->form = $form;		
		$this->view->paginator = $paginator;
	}
	
	public function quitApprovalAction() {
		
		 
		//title
    	$this->view->title="Quit Approval";
    	
    	$txnId = $this->_getParam('id', 0);
		$this->view->txn_id = $txnId;
		
		$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction(); 
    	$applicant = $transactionDb->getQuitProfile($txnId);
    	$this->view->applicant = $applicant;
    	
    	
    	$applicantProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
    	$txnProgram = $applicantProgramDb->getOfferProgram($txnId,$applicant["at_appl_type"]);
    	$this->view->program = $txnProgram;
    	
    	//get payment status
    	$financeDb =  new Studentfinance_Model_DbTable_PaymentMain();
    	$payment = $financeDb->getApplicantPaymentTotalAmount($applicant["at_pes_id"]);
    	//$payment = $financeDb->getApplicantPaymentTotalAmount('13380008');     	
    	$this->view->payment = $payment;

    	//advance payment
    	$avdPaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
    	$advPayment = $avdPaymentDb->getApplicantBalanceAvdPayment($applicant["at_appl_id"]);
    	
    	$adv_amt = 0;
    	for($i=0;$i<sizeof($advPayment);$i++){
    		$adv_amt += $advPayment[$i]['advpy_total_balance'];
    	}
    	$this->view->adv_payment = $adv_amt;
    	
    	//get realtionship info
    	$setupDb = new App_Model_General_DbTable_SisSetupDetail();
    	$this->view->relationship = $setupDb->getData($applicant['aq_relationship']);
    	
    	//get reason
    	$definationDB = new App_Model_General_DbTable_Definationms();    	
    	$this->view->reason = $definationDB->getData($applicant['aq_reason']);
    	$this->view->identity_type = $definationDB->getData($applicant['aq_identity_type']);
    	
	    //get KARTU PESERTA UJIAN
    	$documentDB = new App_Model_Application_DbTable_ApplicantDocument();
    	$kartu = $documentDB->getData($txnId,30); 
    	if($kartu["ad_filename"]!=''){
    		$this->view->url_kartu = 'http://'.ONNAPP_HOSTNAME.'/documents/'.$kartu["ad_filepath"].'/'.$kartu["ad_filename"];
    	}
    	
    	//get document uploaded
    	$uploadfileDB = new App_Model_Application_DbTable_ApplicantUploadFile();  
    	
    	//Slip Pembayaran
    	$payment_slip = $uploadfileDB->getTxnFile($txnId,46);
		if($payment_slip["auf_file_name"]!=''){
    		$this->view->url_payment_slip = 'http://'.ONNAPP_HOSTNAME.'/documents'.$payment_slip["pathupload"].'/'.$txnId.'/'.$payment_slip["auf_file_name"];
    	}
    	
    	//Identity Card
    	$identity_card = $uploadfileDB->getTxnFile($txnId,48);
		if($identity_card["auf_file_name"]!=''){
    		$this->view->url_identity_card = 'http://'.ONNAPP_HOSTNAME.'/documents'.$identity_card["pathupload"].'/'.$txnId.'/'.$identity_card["auf_file_name"];
    	}
    	
    	//Surat Pernyataan Kesediaan
    	$surat_pernyataan = $uploadfileDB->getTxnFile($txnId,47);
		if($surat_pernyataan["auf_file_name"]!=''){
    		$this->view->url_surat_pernyataan = 'http://'.ONNAPP_HOSTNAME.'/documents'.$surat_pernyataan["pathupload"].'/'.$txnId.'/'.$surat_pernyataan["auf_file_name"];
    	}
    	
    	
    	//quit charges
    	$quitFeeFacultyDb = new Studentfinance_Model_DbTable_FeeQuitFaculty();
		$quitCharges = $quitFeeFacultyDb->getQuitCharges($txnProgram['faculty_id']);
		$this->view->quit_charges = $quitCharges;
    	
    	$form = new Application_Form_QuitApproval(array('transid'=>$txnId,'payment'=>$payment["id"]));
    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost()) {
    		
			$formData = $this->getRequest()->getPost();
			
			if($form->isValid($formData)){
				
				
				$auth = Zend_Auth::getInstance();
							
				if($formData["aq_status"]==2) { //approve
					
					$data["aq_approvaldt"]=date("Y-m-d",strtotime($formData["aq_approveddt"]));
					$data["aq_approvalby"]= $auth->getIdentity()->iduser;
					//$data["aq_issueddt"]=date("Y-m-d",strtotime($formData["aq_issueddt"]));
					//$data["aq_bank"]=$formData["aq_bank"];
					//$data["aq_cheque_no"]=$formData["aq_cheque_no"];
					
				}else if($formData["aq_status"]==3){ //reject
					
					$data["aq_approvaldt"]=date("Y-m-d",strtotime($formData["aq_approveddt"]));
					$data["aq_approvalby"]= $auth->getIdentity()->iduser;
					
				}else if($formData["aq_status"]==4){ //incomplete docs
					
					$data["aq_processdt"]=date("Y-m-d H:i:s");
					$data["aq_processby"]=$auth->getIdentity()->iduser;
				}
				
				$data["aq_remarks"]=$formData["aq_remarks"];				
				
				
				
				
				/***********************************
    			 * STUDENT FINANCE
    			 ***********************************/
    			if($formData['aq_status'] == 2){
    				
    				$db = Zend_Db_Table::getDefaultAdapter();
	    	    	$db->beginTransaction();
	    	   		try {
	    	   			
	    	   			//update quit table
	    	   			$quitDb = new App_Model_Application_DbTable_ApplicantQuit();
						$quitDb->updateQuit($data,$formData["aq_trans_id"]);
				
						//update transaction			
						$transactionDb->updateData(array('at_quit_status'=>$formData["aq_status"]),$formData["aq_trans_id"]);
					
		    			//get issued invoice
		    			$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		    			$invoice_list = $invoiceMainDb->getIssuedInvoiceData($applicant['at_pes_id']);
		    					    			
		    					    			
		    			//create credit note
		    			$creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
		    			
		    			for($i=0; $i<sizeof($invoice_list); $i++){
		    				$invoice = $invoice_list[$i];
		    				
		    				//exclude invoice for USM test
		    				if( $invoice['bill_number'] != $applicant['at_pes_id']){
			    				//variable for update invoice
			    				$data_invoice_update = array();
			    		
								$data = array(
											'cn_billing_no' => $invoice['bill_number'],
											'cn_fomulir' => $invoice['no_fomulir'],
											'appl_id' => $invoice['appl_id'],
											'cn_amount' => $invoice['bill_amount'],
											'cn_description' => 'Quit Program',
											'cn_creator' => -1,
											'cn_approver' => -1
										);
								
								$data_invoice_update['cn_amount'] = $invoice['bill_amount'];
								$data_invoice_update['bill_balance'] = 0;
								
								$creditNoteDb->insert($data);
								
			    				//create advance payment on paid invoice
			    			 	if( isset($invoice['bill_paid']) && $invoice['bill_paid']>0 ){
			    			 		
						    	    //transfer payment to advance payment
						    	    $advPaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
						    	    $data2 = array(
						    	    	'advpy_appl_id' => $invoice['appl_id'],
						    	    	'advpy_acad_year_id' => $invoice['academic_year'],
						    	    	'advpy_sem_id' => $invoice['semester'],
						    	    	'advpy_prog_code' => $invoice['program_code'],
						    	    	'advpy_fomulir' => $invoice['no_fomulir'],
						    	    	'advpy_invoice_no' =>$invoice['bill_number'],
						    	    	'advpy_invoice_id' =>$invoice['id'],
						    	    	'advpy_description' => 'Quit Application - '.$applicant['at_pes_id'],
						    	    	'advpy_amount' => $invoice['bill_paid'],
						    	    	'advpy_total_paid' => 0,
						    	    	'advpy_total_balance' => $invoice['bill_paid'],
						    	    	'advpy_status' => 'A',
						    	    	'advpy_creator' => -1
						    	    );
						    	    
						    	   $advPaymentDb->insert($data2);
						    	   
						    	   //update invoice bill paid = 0.00
						    	   $data_invoice_update['bill_paid'] = 0.00;
		
					    	    }
					    	    
					    	    //update invoice paid, balance and CN value
					    	    $invoiceMainDb->update($data_invoice_update,'id = '.$invoice['id']);
		    				}
		    			}
		    			
		    			
		    			
		    			//create invoice for quit charges
		    			$quitFeeFacultyDb = new Studentfinance_Model_DbTable_FeeQuitFaculty();
		    			$quitCharges = $quitFeeFacultyDb->getQuitCharges($txnProgram['faculty_id']);
		    			
		    			$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		    				
		    			//get bill seq
		    			$stmt = $db->query("SELECT seq('bill_seq')bill");
		    			$seq_bill = $stmt->fetch();
		    		
		    			$data = array(
		    						'bill_number' => $seq_bill['bill'],
		    						'appl_id' => $invoice['appl_id'],
		    						'no_fomulir' => $invoice['no_fomulir'],
		    						'academic_year' => $invoice['academic_year'],
		    						'semester' => $invoice['semester'],
		    						'bill_amount' => $quitCharges,
		    						'bill_description' => 'Application Quit Charges ('.$invoice['no_fomulir'].')',
		    						'college_id' => $txnProgram['faculty_id'],
		    						'program_code' => $txnProgram['program_code'],
		    						'creator' => -1
		    					);
		    				
		    			$charges_invoice_id =  $invoiceMainDb->insert($data);
		    			
		    			
				    	//knock-off charges invoice using advance payment
				    	$invoice_charges = $invoiceMainDb->getData($charges_invoice_id);
				    	
				    	$avdPaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
				    	$avdPaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
				    	
				    	
				    	$avdPaymentList = $avdPaymentDb->getBalanceAvdPayment($invoice['no_fomulir']);
				    	
				    	$charges = $quitCharges;
				    	if($avdPaymentList){
				    		
				    		$i=0;
				    		while ($charges > 0 && $i<sizeof($avdPaymentList) ){
				    			
				    			if( $avdPaymentList[$i]['advpy_total_balance'] >=  $charges){
				    				$invoice_balance = (float)$charges - (float)$charges;
				    				$avdPaymentBalance = $avdPaymentList[$i]['advpy_total_balance'] - $charges;
				    				$amount_use = $charges;
				    			}else{
				    				$invoice_balance = - $avdPaymentList[$i]['advpy_total_balance'];
				    				$avdPaymentBalance = 0;	
				    				$amount_use = $avdPaymentList[$i]['advpy_total_balance'];
				    			}
				    			
				    			$charges = $charges - $amount_use;
				    			
				    			//update advance payment
				    			$avdData = array(
				    							'advpy_total_paid' => $avdPaymentList[$i]['advpy_total_paid'] + $amount_use,
				    							'advpy_total_balance' => $avdPaymentList[$i]['advpy_total_balance'] - $amount_use
				    						);
				    			
				    			$avdPaymentDb->update($avdData, 'advpy_id = '.$avdPaymentList[$i]['advpy_id']);
				    			
				    						
				    			//insert advance payment detail
				    			$avdDtlData = array(
				    							'advpydet_advpy_id' => $avdPaymentList[$i]['advpy_id'],
				    							'advpydet_bill_no' => $seq_bill['bill'],
				    							'advpydet_total_paid' => $amount_use
				    						  );
				    			$avdPaymentDetailDb->insert($avdDtlData);
				    			
				    			//update quit charges invoice
				    			$invoice_to_update = $invoiceMainDb->getData($charges_invoice_id);
				    			
				    			$upd_data = array(
				    							'bill_paid'=> $invoice_to_update['bill_paid']+$amount_use,
				    							'bill_balance' => $invoice_to_update['bill_amount'] - ($invoice_to_update['bill_paid']+$amount_use)
				    						);
				    						
				    			$invoiceMainDb->update($upd_data, 'id = '.$charges_invoice_id);
				    			
				    			$i++;
				    		}
				    		
				    	}
				    	
    	   				//create refund on advance payment
			    		//$avdPaymentList = $avdPaymentDb->getBalanceAvdPayment($invoice['no_fomulir']);
				    	$avdPaymentList = $avdPaymentDb->getApplicantBalanceAvdPayment($invoice['appl_id']);
						
			    		if($avdPaymentList){
			    			
				    		$refundDb = new Studentfinance_Model_DbTable_Refund();
				    		$refundDetailDb = new Studentfinance_Model_DbTable_RefundDetail();
				    		
				    		//get all advance payment and process as 1 refund
				    		$total_adv_pymt_amount = 0;
				    		foreach ($avdPaymentList as $advPaymentRecord){
				    			$total_adv_pymt_amount += $advPaymentRecord['advpy_total_balance'];
				    		}
				    		
				    		//refund
				    		$data_refund = array(
				    				'rfd_appl_id' => $applicant['at_appl_id'],
				    				'rfd_acad_year_id' => $applicant['at_academic_year'],
				    				'rfd_fomulir' => $applicant['at_pes_id'],
				    				'rfd_desc' => 'Quit Refund ('.$applicant['at_pes_id'].')',
				    				'rfd_amount' => $total_adv_pymt_amount,
				    				'rfd_creator' => '-1',
				    		);
				    		$id_refund = $refundDb->insert($data_refund);
				    		
				    		//Update Advance Payment and insert advance payment detail
				    		foreach ($avdPaymentList as $advPaymentRecord){
				    		
				    			//insert advance paymet detail 
				    			$advPmtDtlDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
				    			$data_advance_payment_detail = array(
				    					'advpydet_advpy_id' => $advPaymentRecord['advpy_id'],
				    					'advpydet_refund_id' => $id_refund,
				    					'advpydet_total_paid' => $advPaymentRecord['advpy_total_balance']
				    			);
				    			$advPymtDtlId = $advPmtDtlDb->insert($data_advance_payment_detail);
				    			
				    			 
				    			//update advance payment main balance amount
				    			$dataAdvancePaymentUpd = array(
				    					'advpy_total_paid' => ($advPaymentRecord['advpy_total_paid'] + $advPaymentRecord['advpy_total_balance']),
				    					'advpy_total_balance' => ($advPaymentRecord['advpy_total_balance'] - $advPaymentRecord['advpy_total_balance'])
				    			);
				    			$avdPaymentDb->update($dataAdvancePaymentUpd, 'advpy_id = '.$advPaymentRecord['advpy_id']);
				    			
				    			
				    			//refund detail
				    			$data_refund_detail = array(
				    					'rfdd_refund_id' => $id_refund,
				    					'rfdd_advpydet_id' => $advPymtDtlId,
				    					'rfdd_amount' => $advPaymentRecord['advpy_total_balance']
				    			);
				    			$refundDetailDb->insert($data_refund_detail);
				    			
				    		}
				    		
				    		
				    			
				    		//create refund cheque
				    		$refundChequeDb = new Studentfinance_Model_DbTable_RefundCheque();
				    		$data_cheque = array(
				    				'rchq_cheque_no' => $formData['aq_cheque_no'],
				    				'rchq_bank_name' => $formData['aq_bank'],
				    				'rchq_amount' => $total_adv_pymt_amount,
				    				'rchq_issue_date' => date('Y-m-d',strtotime($formData['aq_issueddt'])),
				    		);
				    		$cheque_id = $refundChequeDb->insert($data_cheque);
				    		
				    		
				    		//update refund with cheque id
							$data_refund_upd = array(
										'rfd_approver_id' => $auth->getIdentity()->iduser,
										'rfd_approve_date' => date('Y-m-d H:i:s'),
										'rdf_refund_cheque_id' => $cheque_id
										);
							$refundDb->update($data_refund_upd, 'rfd_id = '.$id_refund);
							
							
							//update applicant_quit with cheque id
							$data_applicant_quit_upd = array('aq_cheque_id'=>$cheque_id);
							$quitDb->updateQuit($data_applicant_quit_upd,$formData["aq_trans_id"]);
				    			
				    		
			    		}
			    		
			    		
			    		
				    	//$db->rollback();
				    	//exit;
				    	$db->commit();
				    	    
	    	    	}catch (exception $e) {
					    $db->rollback();
					    echo "<pre>";
					    echo $e->getMessage();
						print_r($e->getTrace());
						echo "</pre>";
						exit;
					}

    			/************************************
    			 * END STUDENT FINANCE
    			 ************************************/
				}else{
    				$quitDb = new App_Model_Application_DbTable_ApplicantQuit();
					$quitDb->updateQuit($data,$formData["aq_trans_id"]);
				
					//update transaction			
					$transactionDb->updateData(array('at_quit_status'=>$formData["aq_status"]),$formData["aq_trans_id"]);
    			}
    			
    			//yati update applicant_quit table (refund_id)
				
				$this->_redirect($this->view->url(array('module'=>'application','controller'=>'quit', 'action'=>'view-details','id'=>$formData["aq_trans_id"],'msg'=>'success'),'default',true));
			}
			
    	}
	}
	
	public function viewDetailsAction() {
		//title
    	$this->view->title="Quit Application Details";
    	
    	$msg = $this->_getParam('msg', 0);
    	if($msg) $this->view->noticeSuccess=$this->view->translate("Information has been saved");
    	
    	
    	$txnId = $this->_getParam('id', 0);
		$this->view->txn_id = $txnId;
		
		$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction(); 
    	$applicant = $transactionDb->getQuitProfile($txnId);
    	$this->view->applicant = $applicant;
    	
    	//get program info
    	$applicantProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
    	$txnProgram = $applicantProgramDb->getOfferProgram($txnId,$applicant["at_appl_type"]);
    	$this->view->program = $txnProgram;
    	
    	//get cheque info
    	$quitDb = new App_Model_Application_DbTable_ApplicantQuit();
    	$cheque = $quitDb->getChequeInfo($txnId);  
    	$this->view->cheque = $cheque;	
    	
    	
    	//get payment status
    	$financeDb =  new Studentfinance_Model_DbTable_PaymentMain();
    	$payment = $financeDb->getApplicantPaymentTotalAmount($applicant["at_pes_id"]); //    	
    	$this->view->payment = $payment;  
    	
    	
    	//get realtionship info
    	$setupDb = new App_Model_General_DbTable_SisSetupDetail();
    	$this->view->relationship = $setupDb->getData($applicant['aq_relationship']);
    	
    	
    	//get reason
    	$definationDB = new App_Model_General_DbTable_Definationms();    	
    	$this->view->reason = $definationDB->getData($applicant['aq_reason']);
    	    	
    	
    	//get staff name
    	$userDb = new App_Model_System_DbTable_User();
    	$this->view->processby = $userDb->getData($applicant['aq_processby']);
    	$this->view->staffinchange = $userDb->getData($cheque['rchq_collector_update_by']);
    	
    	
    	
    	//get KARTU PESERTA UJIAN
    	$documentDB = new App_Model_Application_DbTable_ApplicantDocument();
    	$kartu = $documentDB->getData($txnId,30); 
    	if($kartu["ad_filename"]!=''){
    		$this->view->url_kartu = 'http://'.ONNAPP_HOSTNAME.'/documents/'.$kartu["ad_filepath"].'/'.$kartu["ad_filename"];
    	}
    	
    	//------get document uploaded-------
    	$uploadfileDB = new App_Model_Application_DbTable_ApplicantUploadFile();  
    	
    	//Slip Pembayaran
    	$payment_slip = $uploadfileDB->getTxnFile($txnId,46);
		if($payment_slip["auf_file_name"]!=''){
    		$this->view->url_payment_slip = 'http://'.ONNAPP_HOSTNAME.'/documents'.$payment_slip["pathupload"].'/'.$txnId.'/'.$payment_slip["auf_file_name"];
    	}
    	
    	//Identity Card
    	$identity_card = $uploadfileDB->getTxnFile($txnId,48);
		if($identity_card["auf_file_name"]!=''){
    		$this->view->url_identity_card = 'http://'.ONNAPP_HOSTNAME.'/documents'.$identity_card["pathupload"].'/'.$txnId.'/'.$identity_card["auf_file_name"];
    	}
    	
    	//Surat Pernyataan Kesediaan
    	$surat_pernyataan = $uploadfileDB->getTxnFile($txnId,47);
		if($surat_pernyataan["auf_file_name"]!=''){
    		$this->view->url_surat_pernyataan = 'http://'.ONNAPP_HOSTNAME.'/documents'.$surat_pernyataan["pathupload"].'/'.$txnId.'/'.$surat_pernyataan["auf_file_name"];
    	}
	}
	
	
	public function ajaxGetQuitAction(){
    	$txn_id = $this->_getParam('txn_id', null);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
        $transactionDb = new App_Model_Application_DbTable_ApplicantTransaction(); 
    	$row = $transactionDb->getQuitProfile($txn_id);
    		  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
    public function confirmCollectionAction(){
    	
    	if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			//applicant quit data
			$quitDb = new App_Model_Application_DbTable_ApplicantQuit();
			$quitData = $quitDb->getData($formData['aq_id']);
			
			$auth = Zend_Auth::getInstance();
			
			$data = array(
						'rchq_collector_name' => $formData['collector_name'],
						'rchq_collector_date' => date('Y-m-d H:i:s'),
						'rchq_collector_id' => $formData['collector_id'],
						'rchq_collector_update_by' => $auth->getIdentity()->iduser
					);
			
			$refundChecqueDb = new Studentfinance_Model_DbTable_RefundCheque();
			$refundChecqueDb->update($data, 'rchq_id = '.$quitData['aq_cheque_id']);
			
			//redirect
			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'quit', 'action'=>'applicant-list'),'default',true));
			
		}
    }
    
    
	public  function downloadAction(){
    	
    	$this->view->title=$this->view->translate("Download Quit Form");    	 

    	$txnId = $this->_getParam('id', 0);
		$this->view->txn_id = $txnId;    	    	
    	    
    	$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();  
    	$applicant = $transactionDb->getTransaction($txnId);
		$this->view->applicant = $applicant;
		
		$appl_id = $applicant["at_appl_id"];
		
		$applicantProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
		$program_data = $applicantProgramDb->getProgramOffered($txnId,$applicant["at_appl_type"]);
    	
	    	//cek document dah create ke belum
	    	$applicantDocumentDb = new App_Model_Application_DbTable_ApplicantDocument();    		
	    	$docData = $applicantDocumentDb->getData($txnId,49);
	    		    	
	    	//generate each time user download
    		if($docData & 1==0){
    			echo "Already Generated";
    			exit;
    			$this->view->file_path = DOCUMENT_PATH.DIRECTORY_SEPARATOR.$docData['ad_filepath'].DIRECTORY_SEPARATOR.$docData['ad_filename'];
    		}else{		
    			
		    			/*
		    			 * Start GENERATE
		    			 */
		    			
		    			
		    			// ------- create PDF File section	--------  
		    			
		    			setlocale(LC_MONETARY, 'id_ID');
		    			setlocale(LC_TIME, 'id_ID');
		    			
		    			global $payment;
		    			global $quit_charges;
		    			global $invoice;
		    			
    					$quitDb = new App_Model_Application_DbTable_ApplicantQuit();
						$quit = $quitDb->getInfo($txnId);
						
						$definationDB = new App_Model_General_DbTable_Definationms();
						$defination = $definationDB->getData($quit["aq_identity_type"]);
						
						//get list payment made by applicant
						$paymentDb = new App_Model_Finance_DbTable_PaymentMain();	
					    $payment = $paymentDb->getPaymentDetails($applicant["at_pes_id"]);
						
						//get invoice 
						$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
					    $invoice = $invoiceMainDb->getInvoicedProformaData($applicant["at_pes_id"]);
						
					    //mark paid if having transfer to advance payment
					    $advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
					    for($i=0; $i<sizeof($invoice); $i++){
					    	
					    	//get advance payment transfered
					    	$advPymt = $advancePaymentDb->getAdvancePaymentFromInvoice($invoice[$i]['id']);
					    	
					    	if($advPymt){
					    		$invoice[$i]['paid_transfer_to_advpymt'] = $advPymt['advpy_amount'];
					    	}
					    	
					    	//remove
					    }
					    
						//get Paket info
						$feeStructureDB = new App_Model_Finance_DbTable_FeeStructurePlan();	
						$paket = $feeStructureDB->getStructurePlan($invoice[0]["fs_id"],$invoice[0]["fsp_id"]);
						
						//get quit charges
						$quitFeeDb = new Studentfinance_Model_DbTable_FeeQuitFaculty();
						$quit_charges = $quitFeeDb->getQuitCharges($program_data["faculty_id"], $applicant["at_intake"],date("Y-m-d",strtotime($quit["aq_createddt"])));

						
				
						$collection_date = strftime('%e-%B-%Y', strtotime('+4 days', strtotime(date("Y-m-d",strtotime($quit["aq_approvaldt"])))));
						$approve_date    = strftime("%e-%B-%Y",strtotime($quit["aq_approvaldt"]));
						
							
						
		    			$fieldValues = array (
						    
		    			     '$[Academicyear]' => $applicant["ay_code"],		    			 	
		    			 	 '$[Period]' => $applicant["ap_number"],	
		    			
						 	 '$[Applicantname]' => $applicant["appl_fname"].' '.$applicant["appl_mname"].' '.$applicant["appl_lname"],
		    			     '$[Applicantid]' => $applicant["at_pes_id"],
							 '$[Faculty]' => $program_data["faculty_indonesia"],				    
							 '$[Programme]' => $program_data["program_name_indonesia"],
		    			     '$[Paket]' => $paket["fsp_name"],	
		    				 '$[Address1]' => $applicant["appl_address1"],
						     '$[Address2]' => $applicant["appl_address2"],
		    			     '$[Phone]' => $applicant["appl_phone_mobile"],	

		    			 	 '$[PersonalAuthorised]' => $quit["aq_authorised_personnel"],		    			 	
		    			 	 '$[PersonalAddress]' => $quit["aq_address"],		    				
		    				 '$[IdentityType]' => $defination["DefinitionDesc"],	
		    				 '$[IdentityNo]' => $quit["aq_identity_no"],
		    			    // '$[Perincian]' => $perincian,
		    				
		    				 '$[CollectionDate]' => $collection_date,
		    			     '$[ApprovedDate]' => $approve_date
						);		
						    	
						
						
		    			$monthyearfolder=date("mY");
		    			
				    	//directory to locate file
						$app_directory_path = DOCUMENT_PATH."/applicant/".$monthyearfolder;	
		
						//create directory to locate file			
						if (!is_dir($app_directory_path)) {
						    mkdir($app_directory_path, 0775);
						}
						
						$output_directory_path = DOCUMENT_PATH."/applicant/".$monthyearfolder."/".$txnId;
							
		    			//create directory to locate file			
						if (!is_dir($output_directory_path)) {
						    mkdir($output_directory_path, 0775);
						}				
														
						//$location_path
					    $location_path = "applicant/".$monthyearfolder."/".$txnId;								
						
						//filename
						$output_filename = $applicant["at_pes_id"]."_quitform.pdf";
						
						try{
							require_once 'dompdf_config.inc.php';
							
							$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
							$autoloader->pushAutoloader('DOMPDF_autoload');
							
							//template path	 
							$html_template_path = DOCUMENT_PATH."/template/quitform.html";
							
							$html = file_get_contents($html_template_path);
							
							//replace variable
							foreach ($fieldValues as $key=>$value){
								$html = str_replace($key,$value,$html);	
							}
								
							
							$dompdf = new DOMPDF();
							$dompdf->load_html($html);
							$dompdf->set_paper('a4', 'potrait');
							$dompdf->render();
							
							$dompdf = $dompdf->stream($output_filename);
							//$dompdf = $dompdf->output();
							
							
							
							//to rename output file			
							$output_file_path = $output_directory_path."/".$output_filename;
							
					
							file_put_contents($output_file_path, $dompdf);
									
							// ------- End PDF File section	--------
							
							$status = true;
						
						}catch (Exception $e) {
							$status = false;	
						}
		
		    			//return $status;				
				    	//save file info
						/*$documentDB = new App_Model_Application_DbTable_ApplicantDocument();
						$doc["ad_appl_id"]=$txnId;
						$doc["ad_type"]=49; //QUIT FORM
						$doc["ad_filepath"]=$location_path;
						$doc["ad_filename"]=$output_filename;
						$doc["ad_createddt"]=date("Y-m-d");
						//$documentDB->addData($doc);	*/
		    	
						$this->view->file_path = DOCUMENT_PATH.DIRECTORY_SEPARATOR.$location_path.DIRECTORY_SEPARATOR.$output_filename;
		    	       
    			
    			/*
    			 * END GENERATE
    			 */
    				
		   }//end cek document
	
		       			
    }
    
    public function preRevertQuitAction(){
    	$quit_id = $this->_getParam('id', 0);
    	$this->view->quit_id = $quit_id;
    	
    	$this->view->title=$this->view->translate("Revert Quit");
    	
    	//quit data
    	$quitDb = new App_Model_Application_DbTable_ApplicantQuit();
    	$quitData = $quitDb->getData($quit_id);
    	$this->view->quit = $quitData;
    	
    	//profile
    	$applicantProfileDb = new App_Model_Application_DbTable_ApplicantProfile();
    	$profile = $applicantProfileDb->getData($quitData['at_appl_id']);
    	$this->view->profile = $profile;
    	
    	//offered program
    	$applicantProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
    	$programOffered = $applicantProgramDb->getOfferProgram($quitData['aq_trans_id'], $quitData['at_appl_type'] );
    	$this->view->program_offered = $programOffered;
    	
    	//refund data
    	$refundDb = new Studentfinance_Model_DbTable_Refund();
    	
    	if( isset($quitData['aq_refund_id']) ){
	    	$refundList = $refundDb->getData($quit_id);
    	}else{ //get refund data from no fom
    		$refundList = $refundDb->getQuitRefund($quitData['at_pes_id']);
    	}
    	
    	//cheque data
    	$chequeDb = new Studentfinance_Model_DbTable_RefundCheque();
    	foreach ($refundList as $index=>$refund){
    		$cheque = $chequeDb->getData($refund['rdf_refund_cheque_id']);
    		$refundList[$index]['cheque_info'] = $cheque;
    	}
    	$this->view->refund = $refundList;
    	
    	
    	//invoice from proforma
    	$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
    	$invoice_list = $invoiceMainDb->getInvoicedProformaData($quitData['at_pes_id']);
    	$this->view->invoice_list = $invoice_list;

    	
    	//quit charges invoice
    	$id_proforma_invoice_list = "";
    	foreach ($invoice_list as $invoice){
    		if($id_proforma_invoice_list!=""){
    			$id_proforma_invoice_list = $id_proforma_invoice_list.",".$invoice['id'];
    		}else{
    			$id_proforma_invoice_list = $invoice['id'];
    		}	
    	}
    	
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$selectData = $db->select()
    					->from(array('im'=>'invoice_main'))
    					->where("im.no_fomulir = '".$quitData['at_pes_id']."'")
    					->where('im.bill_number != im.no_fomulir')
    					->where('im.id not in('.$id_proforma_invoice_list.')');
    		
    	
    	$row = $db->fetchAll($selectData);
    	
    	if($row){
    		$this->view->quit_charges = $row;
    	}else{
    		$this->view->quit_charges = null;
    	}
    	
    	
    	
    }
    
    public function revertQuitAction(){
    	
    	$quit_id = $this->_getParam('id', 0);
    	
    	if ($this->getRequest()->isPost()) {
    		
    		$auth = Zend_Auth::getInstance();
    		
    		$formData = $this->getRequest()->getPost();
    		
    		$db = Zend_Db_Table::getDefaultAdapter();
    		$db->beginTransaction();
    		try {
    			
    			//get quit data and remove
    			$quitDb = new App_Model_Application_DbTable_ApplicantQuit();
    			$quitData = $quitDb->getData($quit_id);
    			$quitDb->delete('aq_id = '.$quitData['aq_id']);
    			
    			
    			//add applicant quit revert
    			$quitRevertDb = new App_Model_Application_DbTable_ApplicantQuitRevert();
    			
    			$quitRevertData['aq_id'] = $quitData['aq_id'];
    			$quitRevertData['aq_trans_id'] = $quitData['aq_trans_id'];
    			$quitRevertData['aq_reason'] = $quitData['aq_reason'];
    			$quitRevertData['aq_authorised_personnel'] = $quitData['aq_authorised_personnel'];
    			$quitRevertData['aq_relationship'] = $quitData['aq_relationship'];
    			$quitRevertData['aq_address'] = $quitData['aq_address'];
    			$quitRevertData['aq_identity_type'] = $quitData['aq_identity_type'];
    			$quitRevertData['aq_identity_no'] = $quitData['aq_identity_no'];
    			$quitRevertData['aq_createddt'] = $quitData['aq_createddt'];
    			$quitRevertData['aq_approvaldt'] = $quitData['aq_approvaldt'];
    			$quitRevertData['aq_approvalby'] = $quitData['aq_approvalby'];
    			$quitRevertData['aq_processdt'] = $quitData['aq_processdt'];
    			$quitRevertData['aq_processby'] = $quitData['aq_processby'];
    			$quitRevertData['aq_remarks'] = $quitData['aq_remarks'];
    			$quitRevertData['aq_cheque_id'] = $quitData['aq_cheque_id'];
    			$quitRevertData['aqr_remark'] = $formData['remark'];
    			$quitRevertData['aqr_by'] = $auth->getIdentity()->iduser;
    			$quitRevertData['aqr_date'] = date("Y-m-d H:i:s");
    			
    			$quitRevertDb->insert($quitRevertData);
    			
    			
    			//update applicant_transaction table
    			$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
    			$data = array(
    				'at_quit_status' => 0	
    			);
    			$txnDb->update($data,'at_trans_id = '.$quitData['aq_trans_id']);
    			
    			//get refund and cancel
    			$refundDb = new Studentfinance_Model_DbTable_Refund();
    			$rfndChqDb = new Studentfinance_Model_DbTable_RefundCheque();
    			$avdPaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
    			
    			$refundList = $refundDb->getQuitRefund($quitData['at_pes_id']);
    			
    			
    			if($refundList){
    				
    				foreach($refundList as $refund){
       					
    					//cancel refund
    					$cnl_refund_data = array(
    						'rfd_status'	=> 'X',
    						'rfd_cancel_by' => $auth->getIdentity()->iduser,
    						'rfd_cancel_date' => date("Y-m-d H:i:s")
    					);
    					$refundDb->update($cnl_refund_data,'rfd_id = '.$refund['rfd_id'] );
    					
    					
    					//cancel refund_cheque
    					if( isset($refund['rdf_refund_cheque_id']) ){
	    					
    						//get cheque info
    						$refund_cheque_data = $rfndChqDb->getData($refund['rdf_refund_cheque_id']);
    						
	    					$data=array(
	    							'rchq_status' => 'X',
	    							'rchq_cancel_by' => $auth->getIdentity()->iduser,
	    							'rchq_cancel_date' => date("Y-m-d H:i:s")
	    					);
	    					$rfndChqDb->update($data,'rchq_id = '.$quitData['aq_cheque_id']);
    					}
    					
    					//insert advance payment
    					$data_advance_payment = array(
    							'advpy_appl_id' => $refund['rfd_appl_id'],
    							'advpy_acad_year_id' => $refund['rfd_acad_year_id'],
    							'advpy_sem_id' => $refund['rfd_sem_id'],
    							'advpy_prog_code' => '',
    							'advpy_fomulir' => $refund['rfd_fomulir'],
    							'advpy_refund_id' =>$refund['rfd_id'],
    							'advpy_description' => 'Revert Quit Application (Refund) - '.$refund['rfd_fomulir'],
    							'advpy_amount' => $refund['rfd_amount'],
    							'advpy_total_paid' => 0,
    							'advpy_total_balance' => $refund['rfd_amount'],
    							'advpy_status' => 'A',
    							'advpy_creator' => -1
    					);
    					$avdPaymentDb->insert($data_advance_payment);
    					
    				}
    			}
    			
    			
    			//get fomulir invoice
    			$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
    			$invoice_list = $invoiceMainDb->getInvoicedProformaData($quitData['at_pes_id']);
    			
    			//cancel credit note and update invoice balance
    			for($i=0; $i<sizeof($invoice_list);$i++){
    				
    				if($invoice_list[$i]['cn_amount']>0){
    					
		    			
		    			$creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
		    			$invoiceCn = $creditNoteDb->getDataByInvoice($invoice_list[$i]['bill_number']);
		    			
		    			if($invoiceCn){
		    				
		    				//cancel credit note
			    			$data_cancel_cn = array(
			    				'cn_cancel_by' => $auth->getIdentity()->iduser,
			    				'cn_cancel_date' =>	date("Y-m-d H:i:s")	
			    			);
			    			
			    			$creditNoteDb->update($data_cancel_cn, 'cn_id = '.$invoiceCn['cn_id']);
			    			
			    			//update invoice bill balance
			    			$data_upd_invoice = array(
			    				'bill_balance' => $invoice_list[$i]['cn_amount'],
			    				'cn_amount' => 0.00	
			    			);
			    			$invoiceMainDb->update($data_upd_invoice, 'id = '.$invoice_list[$i]['id']);
			    			
		    			}
		    			
    				}
    			}
    			
    			
    			//cancel invoice on quit charges//quit charges invoice and transfer to advance payment
		    	$id_proforma_invoice_list = "";
		    	foreach ($invoice_list as $invoice){
		    		if($id_proforma_invoice_list!=""){
		    			$id_proforma_invoice_list = $id_proforma_invoice_list.",".$invoice['id'];
		    		}else{
		    			$id_proforma_invoice_list = $invoice['id'];
		    		}	
		    	}
		    	
		    	$db = Zend_Db_Table::getDefaultAdapter();
		    	$selectData = $db->select()
		    					->from(array('im'=>'invoice_main'))
		    					->where("im.no_fomulir = '".$quitData['at_pes_id']."'")
		    					->where('im.bill_number != im.no_fomulir')
		    					->where('im.id not in('.$id_proforma_invoice_list.')');
		    		
		    	
		    	$row = $db->fetchAll($selectData);
		    	
		    	if($row){
		    		
		    		foreach ($row as $inv_quit_charges){
		    			//cancel invoice
		    			$inv_cnl_data = array(
		    				'bill_paid' => 0.00,
		    				'bill_balance' => 0.00,
		    				'status' => 'X',
		    				'cancel_by' =>$auth->getIdentity()->iduser,
		    				'cancel_date' =>date("Y-m-d H:i:s")	
		    			);
		    			
		    			$invoiceMainDb->update($inv_cnl_data,'id = '.$inv_quit_charges['id']);
		    			
		    			//transfer to advance payment
		    			$data_inv_to_advpymt = array(
		    					'advpy_appl_id' => $inv_quit_charges['appl_id'],
		    					'advpy_acad_year_id' => $inv_quit_charges['academic_year'],
		    					'advpy_sem_id' => $inv_quit_charges['semester'],
		    					'advpy_prog_code' => '',
		    					'advpy_fomulir' => $inv_quit_charges['no_fomulir'],
		    					'advpy_invoice_id' =>$inv_quit_charges['id'],
		    					'advpy_description' => 'Revert Quit Application (Quit charges) - '.$inv_quit_charges['no_fomulir'],
		    					'advpy_amount' => $inv_quit_charges['bill_paid'],
		    					'advpy_total_paid' => 0,
		    					'advpy_total_balance' => $inv_quit_charges['bill_paid'],
		    					'advpy_status' => 'A',
		    					'advpy_creator' => -1
		    			);
		    			
		    			$avdPaymentDb->insert($data_inv_to_advpymt);	
		    		}
		    	}
		    	
    			//$db->rollback();
    			$db->commit();
    			
    			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'advance-payment-invoice-pay', 'action'=>'payment','appl_id'=>$quitData['at_appl_id']),'default',true));
    			
				    	    
			}catch (exception $e) {
			    $db->rollback();
			    echo "<pre>";
			    echo $e->getMessage();
				print_r($e->getTrace());
				echo "</pre>";
				exit;
			}
    	}
    }
}





?>