<?php

class Application_ChangeProgramController extends Base_Base{
	
		
	public function indexAction()
    {
    	$this->view->title=$this->view->translate("Change Program");
    	
    	$changeProgramDB = new App_Model_Application_DbTable_ApplicantChangeProgram();
		
	   // $form = new Application_Form_Quit();
	    		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			$txnList = $changeProgramDB->geListApplication($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($txnList));			
			$paginator->setItemCountPerPage(100);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));			
			//$form->populate($formData);
			
		}else{
			$list = $changeProgramDB->gePaginateListApplication();
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($list));		
			$paginator->setItemCountPerPage(100);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
		}
		
		//$this->view->form = $form;		
		$this->view->list = $paginator;
    }
    
    
}

























