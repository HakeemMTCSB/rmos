<?php
class Application_TpaSelectionController extends Zend_Controller_Action {
	
	public function indexAction(){
	
		$this->view->title = "TPA - Selection Process";
	
		$msg = $this->_getParam('msg', null);
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Information has been save successfully");
		}
		 
		$intake_id = $this->_getParam('intake_id', null);
		$this->view->intake_id = $intake_id;
		 
		$period_id = $this->_getParam('period_id', null);
		$this->view->period_id = $period_id;
		 
		$load_previous_period = $this->_getParam('load_previous_period', 0);
		$this->view->load_previous_period = $load_previous_period;
		 
		$ptest = $this->_getParam('ptest', null);
		$this->view->ptest = $ptest;
		 
		$aps_test_date = $this->_getParam('aps_test_date', null);
		$this->view->aps_test_date = $aps_test_date;
		 
		$aps_id = $this->_getParam('aps_id', null);
		$this->view->aps_id = $aps_id ;
		 
		$faculty = $this->_getParam('faculty', null);
		$this->view->faculty = $faculty;
		 
		$programme = $this->_getParam('programme', null);
		$this->view->programme = $programme;
		 
		$preference = $this->_getParam('preference', null);
		$this->view->preference = $preference;
		 
		$quota = $this->_getParam('quota',1);
		$this->view->quota = $quota;
		 
		$limit = $this->_getParam('limit',null);
		$this->view->limit = $limit;
		 
		$attendance = $this->_getParam('aps_usm_attendance',null);
		$this->view->attendance = $attendance;
		 
		$form = new Application_Form_SearchTpa(array('ptest'=>$ptest,'ptest_date'=>$aps_test_date,'quota'=>$quota,'limitt'=>$limit));
		
		$form->setAction("/application/tpa-selection/index");
		$this->view->form = $form;
	
			
		if ($this->getRequest()->isPost()) {
			if ($form->isValid ( $_POST )) {
				$formData = $this->getRequest()->getPost();
				$form->populate($formData);
				$this->view->form = $form;
	
				$tpaDb = new Application_Model_DbTable_TpaSelection();
				//$passingmark = $transDB->getPassingMark($programme,$ptest);
				$this->view->passingmark=$formData["passmark"];
				$applicant = $tpaDb->getTpaCandidate($ptest,$intake_id,$period_id,0,$load_previous_period,$programme,$aps_id,$aps_test_date,$limit,$formData["passmark"],'',$attendance);
				$this->view->applicant = $applicant;
	
			}
		}elseif($programme && $aps_test_date){
				
			$formData = array('intake_id'=>$intake_id,'ptest'=>$ptest,'aps_test_date'=>$aps_test_date,'aps_id'=>$aps_id,'programme'=>$programme,'preference'=>$preference,'faculty'=>$faculty,'quota'=>1);
				
			$form->populate($formData);
			$this->view->form = $form;
	
			$transDB = new App_Model_Application_DbTable_ApplicantTransaction();
			$applicant = $transDB->getApplicantPassNotInPoolUSM($ptest,$preference,$intake_id,$period_id,0,$load_previous_period,$programme,$aps_id,$aps_test_date,$limit);
			$this->view->applicant = $applicant;
		}
	}
	
	public function saveSelectionAction(){
		
		$msg = "";
		
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
				
			for($i=0; $i<count($formData["transaction_id"]); $i++){
					
				 $txn_id =  $formData["transaction_id"][$i];
				$status =  $formData["status"][$i];

				//lulus tahap ke 2
				if($status=="6"){
				
					$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
					$txnData = $transactionDB->getTransactionData($txn_id);
					
					//type USM push to pool
					if($txnData['at_appl_type']==1){
						
						//program offered
						$appl_programDb = new App_Model_Application_DbTable_ApplicantProgram();
						$program_offered = $appl_programDb->getOfferProgram($txn_id, 1);
						
						//save to pool
						$data["ats_transaction_id"] =  $formData["transaction_id"][$i];
						$data["ats_ap_id"]          =  $formData["ap_id"][$i];
						$data["ats_program_code"]   =  $program_offered['ap_prog_code'];
						$data["ats_preference"]     =  $program_offered['ap_preference'];
						$tempSelectionDB =  new App_Model_Application_DbTable_ApplicantTempUsmSelection();
						$tempSelectionDB->addData($data);
						
						//update selection status
						$transactionDB->updateData(array('at_selection_status'=>4),$txn_id);
					}else{
						//update selection status
						$transactionDB->updateData(array('at_selection_status'=>0),$txn_id);
					}
					
					
							
				}else
				if($status==3){ //reject application
				
					$appl_programDb = new App_Model_Application_DbTable_ApplicantProgram();
					$program_offered = $appl_programDb->getOfferProgram($applicant['at_trans_id'], $applicant['at_appl_type']);
					
					$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
					$appProgramDB->update(array("ap_usm_status"=>2),"ap_at_trans_id = ".$txn_id. " and ap_ptest_prog_id = '".$program_offered['ap_prog_code']."'");
						
					//reject application
					$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
					$transactionDB->updateData(array('at_status'=>'REJECT','at_selection_status'=>3),$txn_id);//completed
				}	
			}
			
			$msg = $this->view->translate("Success Saving TPA Selection");
			
		}else{
			$msg = "Unknown request";
		}
		
		$this->_redirect($this->view->url(array('module'=>'application','controller'=>'tpa-selection','action'=>'index','msg'=>$msg),'default',true));
	}
}
?>
