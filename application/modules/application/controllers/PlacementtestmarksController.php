<?php

/**
 * PlacementtestmarksController
 *
 * @author
 * @version
 */


class Application_PlacementtestmarksController extends Base_Base {
    private $_gobjlog;
    private $locale;
    private $dclModel;
    private $defModel;
    private $lobjAppItem;
    private $lobjAppConfig;
    private $stpModel;
    private $tplModel;
    private $sthModel;
    private $checklistVerification;
    private $appAprove;
    private $placementTestMark;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->dclModel = new Application_Model_DbTable_DocumentChecklist();
        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->lobjAppItem = new Application_Model_DbTable_ApplicationItem();
        $this->lobjAppConfig = new Application_Model_DbTable_ApplicationInitialConfig();
        $this->stpModel = new Application_Model_DbTable_StatusTemplate();
        $this->tplModel = new Communication_Model_DbTable_Template();
        $this->sthModel = new Application_Model_DbTable_StatusAttachment();
        $this->checklistVerification = new Application_Model_DbTable_ChecklistVerification();
        $this->appAprove = new Application_Model_DbTable_ApplicantApproval();
        $this->placementTestMark = new Application_Model_DbTable_Placementtestmarks();
    }
    
    public function indexAction(){
        $this->view->title=$this->view->translate("Placement Test Marks Entry");
        $getUserIdentity = $this->auth->getIdentity();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if (isset($formData['Search']) && $formData['Search']=='Search'){
                $placementTestStudentSearchForm = new Application_Form_Placementtestmarksentry(array('IdProgram'=>$formData['Program']));
                $placementTestStudentSearchForm->populate($formData);
                $this->view->placementTestList = $this->placementTestMark->getPlacementMarksEntryList($formData);
            }else if(isset($formData['Save']) && $formData['Save']=='Save'){
                $i=0;
                foreach ($formData['at_trans_id'] as $marksLoop){
                    $cekPlacementTestMarks = $this->placementTestMark->cekPlacemntTestMark($formData['IdPlacementTest'][$i], $formData['IdPlacementTestComponent'][$i], $marksLoop);
                    
                    if ($cekPlacementTestMarks == TRUE){
                        $data = array(
                            'Marks' => $formData['Marks'][$i],
                            'UpdDate' => date('Y-m-d H:i:s'),
                            'UpdUser' => $getUserIdentity->id
                        );
                        $this->placementTestMark->update($data, 'IdPlacementTestMarks = '.$cekPlacementTestMarks['IdPlacementTestMarks']);
                    }else{
                        $data = array(
                            'IdPlacementTest' => $formData['IdPlacementTest'][$i],
                            'IdPlacementTestComponent' => $formData['IdPlacementTestComponent'][$i],
                            'IdApplication' => $marksLoop,
                            'Marks' => $formData['Marks'][$i],
                            'UpdDate' => date('Y-m-d H:i:s'),
                            'UpdUser' => $getUserIdentity->id
                        );
                        $this->placementTestMark->insert($data);
                    }
                    $i++;
                }
                $placementTestStudentSearchForm = new Application_Form_Placementtestmarksentry();
            }else{
                $placementTestStudentSearchForm = new Application_Form_Placementtestmarksentry();
            }
        }else{
            $placementTestStudentSearchForm = new Application_Form_Placementtestmarksentry();
        }
        
        $this->view->placementTestStudentSearchForm = $placementTestStudentSearchForm;
    }
}