<?php
class Application_BulkAdmissionController extends Base_Base
{
	protected $itemLog = array();

	public function init()
	{
		$this->locale = Zend_Registry::get('Zend_Locale');
		$this->bulkDB = new Application_Model_DbTable_Bulk();
		$this->uploadDir = DOCUMENT_PATH.'/bulk';

		require_once APPLICATION_PATH.'/../library/Others/parsecsv.lib.php';
	}

	public function indexAction()
	{
	
		$this->view->title = "Bulk Admission";
		
		$p_data = $this->bulkDB->getPaginateData();

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
    	
	}

	public function uploadAction()
	{
		/*	
			problem/missing
			- Designation
			- program_scheme
			- Medium of Instruction
			- Proficiency in English -- kena filter by code
		*/
		$this->view->title = "Bulk Admission - Upload";
		
		if ($this->_request->isPost() && $this->_request->getPost('save')) 
		{
			$larrformData = $this->_request->getPost();
			
			$total = 0;

			try 
			{
				if ( !is_dir( $this->uploadDir ) )
				{
					if ( mkdir_p($this->uploadDir) === false )
					{
						throw new Exception('Cannot create document folder ('.$this->uploadDir.')');
					}
				}

				$adapter = new Zend_File_Transfer_Adapter_Http();
				
			
				$files = $adapter->getFileInfo();
				$adapter->addValidator('NotExists', false, $this->uploadDir );
				$adapter->setDestination( $this->uploadDir );
				$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
				$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

				foreach ($files as $no => $fileinfo) 
				{
					$adapter->addValidator('Extension', false, array('extension' => 'csv', 'case' => false));
				
					if ($adapter->isUploaded($no))
					{
						$ext = getext($fileinfo['name']);
						$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
						
						$fileUrl = $this->uploadDir.'/'.$fileName;

						$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
						$adapter->setOptions(array('useByteString' => false));
						
						$size = $adapter->getFileSize($no);
					
						if( !$adapter->receive($no))
						{
							$errmessage = array();
							if ( is_array($adapter->getMessages() ))
							{	
								foreach(  $adapter->getMessages() as $errtype => $errmsg )
								{
									$errmessage[] = $errmsg;
								}
								
								throw new Exception(implode('<br />',$errmessage));
							}
						}

						
						//process - CSV
						$csv = new parseCSV();

						$csv->auto($fileUrl);
						
						

						$totalfields = count($csv->data[0]);
						
						if ( $totalfields != 63 )
						{
							throw new Zend_Exception('Invalid CSV format');
						}

						//save into db
						if ( !isset($this->bulk_id) )
						{
							$auth = Zend_Auth::getInstance(); 
							$data = array(
											'b_filename'		=> $fileinfo['name'],
											'b_fileurl'			=> '/bulk/'.$fileName,
											'b_status'			=> 'NEW',
											'b_totalapp'		=> count($csv->data),
											'b_created_by'		=> $auth->getIdentity()->iduser,
											'b_created_date'	=> date("Y-m-d H:i:s")
										);
							
							$this->bulk_id = $this->bulkDB->addData($data);
						}

						

					} //isuploaded
					
				} //foreach
				
				//$this->bulkDB->updateData(array('b_totalapp' => $total), $this->bulk_id);

				$this->_redirect('/application/bulk-admission/review/id/'.$this->bulk_id);	

			}
			catch (Exception $e) 
			{
				throw new Exception( $e->getMessage() );
			}

			

		} // if 
	}
	
	protected function getItemID($row)
	{
		return clean_string($row['First Name'].' '.$row['Last Name']);
	}

	public function reviewAction()
	{
		$this->view->title = "Bulk Admission - Review";

		$id = $this->_getParam('id',null);

		$bulkinfo = $this->bulkDB->getData($id);


		if ( empty($bulkinfo) )
		{
			throw new Exception('Invalid Bulk ID');
		}
		
		
		$this->bulk_id = $bulkinfo['b_id'];
		$importedItems = $this->bulkDB->getItems($id);
		$importedBID = array();
		foreach ( $importedItems as $iitem ) 
		{
			$importedBID[] = $iitem['bi_bid'];
		}

		//parse CSV
		$csv = new parseCSV();
		$csv->auto(DOCUMENT_PATH.$bulkinfo['b_fileurl']);
		
	
		//start
		$items = array();

		foreach ( $csv->data as $row_id => $row )
		{
			$bid = $this->getItemID($row);

			$items[$bid] = array('BID' => $bid) + $row;

			$this->processItem(array('BID' => $bid) + $row);
		}

		//post
		if ($this->_request->isPost() && $this->_request->getPost('save')) 
		{
			$formData = $this->_request->getPost();

			$count = $formData['item'];
	
			if ( $count > 0 )
			{
				$total = 0;
				foreach ( $formData['item'] as $BID )
				{
					$this->processItem($items[$BID], 1);
					$total++;
				}
				
				$this->bulkDB->updateData(array('b_status' => 'IMPORTED', 'b_totaladded' => new Zend_Db_Expr('b_totaladded+'.$total)), $this->bulk_id);

				$this->_helper->flashMessenger->addMessage(array('success' => $total." Applicant imported"));
				$this->_redirect('/application/bulk-admission/review/id/'.$this->bulk_id);
			}
			else
			{
				//error
			}
		}
		

			
		

		//views
		$this->view->importedBID = $importedBID;
		$this->view->items = $items;
		$this->view->itemLog = $this->itemLog;
		
	}


	protected function logItem($bid, $field='', $msg='' )
	{
		$this->itemLog[$bid][] = array('field'	=> $field,
										'msg'	=> $msg);
	}

	protected function processItem($row, $insert=0)
	{
		//Models
		$branchDB = new GeneralSetup_Model_DbTable_Branchofficevenue();
		$appProfileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		$appTransactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
		$objdeftypeDB = new App_Model_Definitiontype();
		$countryDB = new App_Model_General_DbTable_Country();
		$entryReqDB= new App_Model_Application_DbTable_EntryRequirement();
		$appQualificationDB  = new App_Model_Application_DbTable_ApplicantQualification();
		$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
		$appVisaDB = new App_Model_Application_DbTable_ApplicantVisa();
		
		
		//BID
		$bid = $row['BID'];

		/* some remapping */
		$row['Nationality'] = $row['Nationality'] == 'MALAYSIAN' ? 'MALAYSIA':$row['Nationality'];
		$row['Race'] = $row['Race'] == 'MELAYU' ? 'Malay' : $row['Race'];
		$row['Class Degree'] = str_replace('/',' / ',$row['Class Degree']);
		$row['Proficiency in English'] = $row['Proficiency in English'] == 'MUET' ? 'Malaysian University English Test (MUET)' : $row['Proficiency in English'];

		/* process fields */
		foreach ( $row as $field => $value )
		{
			switch ($field)
			{
				case 'CP ID':
					$branch = $branchDB->getData($row['CP ID'], 'BranchCode');
					$branch['IdBranch'];
					
					if ( empty($branch ) ) $this->logItem($bid,$field, 'Empty');
				
				break;
				
				case 'Salutation':
					$salutation = $this->getIdByDef($row['Salutation'], $objdeftypeDB->fnGetDefinationsByLocale('Salutation'));
					if ( empty($salutation ) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'First Name': 
				case 'Last Name':
				case 'Id Number':
				case 'Dob':
				case 'Email Id':
				case 'Address (Permanent)':
				case 'City (Permanent)':
				case 'Postcode (Permanent)':
				case 'Address (Correspondance)':
				case 'City (Correspondance)':
				case 'Postcode (Correspondance)':
				case 'Fax':
				case 'Moblie':
				case 'Company Name':
				case 'Company Address':
				case 'Company Telephone Number':
				case 'Company Fax Number':
				case 'Designation':
				case 'Duration of service (Start Date)':
				case 'Duration of service (End Date)':
				case 'Industry':
				case 'Years in Industry (Working Experience)':
				case 'Name of Sponsor':
				case 'Sponsor Code':
				case 'Scholarship Plan':
				case 'Previous Student':
				case 'Student ID':
				case 'Gender':
				case 'Diploma/Degree Awarded':
				case 'Result/CGPA':
				case 'Year Graduated':
				case 'Medium of Instruction':
				case 'English Language Proficiency  - Date Taken':
				case 'Result for English proficiency':
				case 'Malaysian Visa':

					if ( $value == '' ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Id type':
					$id_type = $this->getIdByDef($row['Id type'],$objdeftypeDB->fnGetDefinations('ID Type'));
					if ( empty($id_type ) ) $this->logItem($bid,$field, 'Empty');
				break;

				
				case 'Intake Session':
					$intake_id = $this->getIntake($row['Intake Session']);
					if ( empty($intake_id) ) $this->logItem($bid,$field, 'Empty');
				break;
				
				case 'Nationality':
					$nationality = $this->getIdByDef($row['Nationality'],$countryDB->getData(),array('k'=>'CountryName','v'=>'idCountry'));
					if ( empty($nationality) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Religion':
					$religion = $this->getIdByDef($row['Religion'],$objdeftypeDB->fnGetDefinations('Religion'));
					if ( empty($religion) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'State (Permanent)':
					$perm_state = $this->getState($row['State (Permanent)']);
					if ( empty($perm_state) ) $this->logItem($bid,$field, 'Empty');
				break;
				
				case 'Country (Permanent)':
					$perm_country = $this->getIdByDef($row['Country (Permanent)'],$countryDB->getData(),array('k'=>'CountryName','v'=>'idCountry'));
					if ( empty($perm_country) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'State (Correspondance)':
					$corr_state = $this->getState($row['State (Correspondance)']);
					if ( empty($corr_state) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Country (Correspondance)':
					$corr_country = $this->getIdByDef($row['Country (Correspondance)'],$countryDB->getData(),array('k'=>'CountryName','v'=>'idCountry'));
					if ( empty($corr_country) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Employment Status':
					$employement_status = $this->getIdByDef($row['Employment Status'],$objdeftypeDB->fnGetDefinations('Employment Status'));
					if ( empty($employement_status) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Position':
					$position_id = $this->getIdByDef($row['Position'],$objdeftypeDB->fnGetDefinations('Position Level'));
					if ( empty($position_id) ) $this->logItem($bid,$field, 'Empty');
				break;
				
				case 'Industry':
					$industry = $this->getIdByDef($row['Industry'],$objdeftypeDB->fnGetDefinations('Industry Working Experience'));
					if ( empty($position_id) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Health condition':
					$health_name = ucfirst(strtolower($row['Health condition']));

					$health_condition = $this->getIdByDef($health_name,$objdeftypeDB->fnGetDefinations('Health Condition'));

					if ( empty($health_condition) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Financial Support':
					$financial_support = $this->getIdByDef($row['Financial Support'],$objdeftypeDB->fnGetDefinations('Funding Method'));
					if ( empty($financial_support) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Program':
					$program =$this->getProgram($row["Program"]);
					if ( empty($program) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Mode of study':

					
					$mode_study = $this->getIdByDef(str_replace('-',' ',$row['Mode of study']),$objdeftypeDB->fnGetDefinations('Mode of Study'));

					if ( empty($mode_study) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Mode of program':
					$mode_program = $this->getIdByDef($row['Mode of program'],$objdeftypeDB->fnGetDefinations('Mode of Program'));
					if ( empty($mode_program) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Programme Type':
					$program_type = $this->getIdByDef($row['Mode of program'],$objdeftypeDB->fnGetDefinations('Program Type'));
					if ( empty($program_type) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Race':
					$race_name = ucfirst(strtolower($row['Race']));
					$race = $this->getIdByDef($race_name,$objdeftypeDB->fnGetDefinations('Race'));
					if ( empty($race) ) $this->logItem($bid,$field, 'Empty');
				break;
				
				case 'Marital Status':
					$marital_status = $this->getIdByDef($row['Marital Status'],$objdeftypeDB->fnGetDefinations('Marital Status'));
					if ( empty($race) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Highest Level':
					$highest_level = $this->getIdByDef($row['Highest Level'],$entryReqDB->getListGeneralReq(),array('k'=>'QualificationLevel','v'=>'IdQualification'));
					if ( empty($highest_level) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Name of University/College':
					$name_of_university = $row['Name of University/College'];
					if ( empty($name_of_university) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Class Degree':
					$class_degree = $this->getIdByDef($row['Class Degree'],$objdeftypeDB->fnGetDefinations('Class Degree'));
					if ( empty($class_degree) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Proficiency in English':
					$proficiency_english = $this->getIdByDef($row['Proficiency in English'],$objdeftypeDB->fnGetDefinations('English Proficiency Test List'));
					if ( empty($proficiency_english) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Visa Status':
					$visa_status = $this->getIdByDef($row['Visa Status'],$objdeftypeDB->fnGetDefinations('Visa Status'));
					if ( empty($visa_status) ) $this->logItem($bid,$field, 'Empty');
				break;
			}
		}
		

		if ( $insert == 0 )
		{
			return false;
		}
		
		if ( $row['First Name'] != '' && $row['Last Name'] )
		{
			//applicant_profile
			$appl_info['appl_fname'] = $row['First Name'];
			$appl_info['appl_lname'] = $row['Last Name'];
			$appl_info["branch_id"] = $branchID;            
			$appl_info["appl_email"]= $formData['Email Id'];
			$appl_info["appl_password"]= '';				
			$appl_info["appl_role"]=0;
			$appl_info["create_date"]=date("Y-m-d H:i:s");
			$appl_info['appl_dob']=date('Y-m-d',strtotime($row['Dob']));
			$appl_info['appl_salutation']= $salutation;
			$appl_info['appl_idnumber'] = $row['Id Number'];
			$appl_info['appl_idnumber_type'] = $id_type;
			$appl_info['appl_nationality'] = $nationality;
			$appl_info['appl_religion'] = $religion;
			$appl_info['appl_address1'] = $row['Address (Permanent)'];
			$appl_info['appl_city'] = 99;
			$appl_info['appl_city_others'] = $row['City (Permanent)'];

			if ( empty($perm_state) && $row['State (Permanent)'] != '' )
			{
				$appl_info['appl_state'] = 99;
				$appl_info['appl_state_others'] = $row['State (Permanent)'];
			}
			else
			{
				$appl_info['appl_state'] = $perm_state;
			}

			$appl_info['appl_country'] = $perm_country;
			$appl_info['appl_postcode'] = $row['Postcode (Permanent)'];
			
			$appl_info['appl_caddress1'] = $row['Address (Correspondance)'];
			
			if ( $row['City (Correspondance)'] != '' )
			{
				$appl_info['appl_ccity'] = 99;
				$appl_info['appl_ccity_others'] = $row['City (Correspondance)'];
			}

			if ( empty($corr_state) && $row['State (Correspondance)'] != '' )
			{
				$appl_info['appl_cstate'] = 99;
				$appl_info['appl_cstate_others'] = $row['State (Correspondance)'];
			}
			else
			{
				$appl_info['appl_cstate'] = $corr_state;
			}

			$appl_info['appl_ccountry'] = $corr_country;

			$appl_info['appl_phone_home'] = $row['Contact No.'];
			$appl_info['appl_fax'] = $row['Fax'];
			$appl_info['appl_phone_mobile'] = $row['Moblie'];
			$appl_info['prev_student'] = $row['Previous Student'] == 'yes' ? 1 : 0;
			$appl_info['prev_studentID'] = $row['Student ID'];
			$appl_info['appl_race'] = $race;
			$appl_info['appl_gender'] = $row['Gender'] == 'Female' ? 2 : 1;
			$appl_info['appl_marital_status'] = $marital_status;
			//

			$applicant_id = $appProfileDB->addData($appl_info);
			
			//generate applicant ID
			$gen_applicantId =  $this->generateApplicantID();

			//applicant_transaction
			$info2["at_appl_id"]=$applicant_id;
			$info2["at_pes_id"]=$gen_applicantId;
			$info2["at_create_by"]=$applicant_id;
			$info2["at_status"]=592; //offered
			$info2["at_create_date"]=date("Y-m-d H:i:s");
			$info2["entry_type"]=0;//manual			
			$info2["at_default_qlevel"]=null;	
			$info2["at_intake"] = $intake_id;
		
		
			//trans ID		    					
			$at_trans_id = $appTransactionDB->addData($info2);
			
			//program
			
			$info3['mode_study'] = $mode_study;
			$info3['program_mode'] = $mode_program;
			$info3['program_type'] = $program_type;
			$info3['ap_prog_scheme'] = '';
			$info3["ap_at_trans_id"]=$at_trans_id;
			$info3["ap_prog_code"]=$program["ProgramCode"];
			$info3["ap_prog_id"]=$program['IdProgram'];		
			
			$appProgramDB  = new App_Model_Application_DbTable_ApplicantProgram();			
			$appProgramDB->addData($info3);
			
			//qualification
			$data6 = array(	    		   
				'ae_appl_id' => $applicant_id,
				'ae_transaction_id' => $at_trans_id,
				'ae_qualification' => $higest_level,
				'ae_degree_awarded' => $row['Diploma/Degree Awarded'],
				'ae_class_degree' => $class_degree,
				'ae_result' => $row['Result/CGPA'],
				'ae_year_graduate' => $row['Year Graduated'],
				'ae_institution_country' => '',
				'ae_institution' => 999,
				'ae_medium_instruction' => $row['Medium of Instruction'],	
				'others' => $name_of_university,	    		  	    		
				'createDate' => date("Y-m-d H:i:s")
			);

			$ae_id = $appQualificationDB->addData($data6);

			//english
			$data4 = array(    			 
				'ep_transaction_id' => $at_trans_id,
				'ep_test' => $proficiency_english,
				'ep_test_detail' => $formData['english_test_detail'],
				'ep_date_taken' => date('Y-m-d',strtotime($formData['English Language Proficiency  - Date Taken'])),
				'ep_score' => $formData['Result for English proficiency'] == '' ? 0 : $formData['Result for English proficiency'],
				'ep_updatedt' => date("Y-m-d H:i:s"),
				'ep_updateby'=>$applicant_id
			);
			
			$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
			$ep_id=$appEngProfDB->addData($data4);
			
		
			//employment info
			$data8 = array(	    	
				'ae_appl_id' => $applicant_id,
				'ae_trans_id' => $at_trans_id,	 
				'ae_status' => $employement_status,
				'ae_comp_name' => $row['Company Name'],
				'ae_comp_address' => $row['Company Address'],
				'ae_comp_phone' => $row['Company Telephone Number'],
				'ae_comp_fax' => $row['Company Fax Number'],
				'ae_designation' => $row['Designation'],
				'ae_position' => $position_id,
				'ae_from' => date('m/Y',strtotime($row['Duration of service (Start Date)'])),
				'ae_to' => date('m/Y',strtotime($row['Duration of service (End Date)'])),	    		  
				'emply_year_service' => '',
				'ae_industry' => $industry,	 
				'ae_job_desc' => '',	    		   	    		
				'upd_date' => date("Y-m-d H:i:s")
			);
							
			$appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();
			$ae_id = $appEmploymentDB->addData($data8);
			
			//health condition
			$appHealthDB = new App_Model_Application_DbTable_ApplicantHealthCondition();
			$data9 = array(    				   
				'ah_appl_id' => $applicant_id,
				'ah_trans_id' => $at_trans_id,
				'ah_status' => $health_condition,
				'upd_date' => date("Y-m-d H:i:s")
			);
						
			$appHealthDB->addData($data9);

			//financial support
			$appFinancial = new App_Model_Application_DbTable_ApplicantFinancial();

			$data10 = array(
				'af_appl_id' => $applicant_id,
				'af_trans_id' => $at_trans_id,
				'af_method' => $financial_support,
				'af_sponsor_name' => $row['Name of Sponsor'],
				'af_type_scholarship' => '',
				'af_scholarship_secured' => $row['Scholarship Plan'],
				'af_scholarship_apply' => '',
				'upd_date' => date("Y-m-d H:i:s")
			);

			$af_id = $appFinancial->addData($data10);

			//visa
			$data11 = array(
				'av_appl_id' => $applicant_id,
				'av_trans_id' => $at_trans_id,
				'av_malaysian_visa' => $row['Malaysian Visa'] == 'Yes' ? 1 : 0,
				'av_status' => $visa_status,
				'av_expiry' => date('Y-m-d',strtotime($row['Visa Expiry Date'])),
				'upd_date' => date("Y-m-d H:i:s")
			);
			
			$appVisaDB->insert($data11);

			//item
			$data = array(
							'b_id'				=> $this->bulk_id,
							'bi_trans_id'		=> $at_trans_id,
							'bi_appl_id'		=> $applicant_id,
							'bi_bid'			=> $bid
						);
			
			$bulk_id = $this->bulkDB->addItem($data);
		}
	}

	function backupprocess()
	{
		/*
		Array
		(
			[CP ID] => INCEIF-HQ
			[Salutation] => Ms
			[First Name] => AZILA
			[Last Name] => Ali
			[Id type] => MyKad
			[Id Number] => 850412-02-5812
			[Dob] => 04/12/1985
			[Intake Session] => 2014 June Semester
			[Nationality] => MALAYSIAN
			[Religion] => ISLAM
			[Email Id] => nnadianadzri@gmail.com
			[Address (Permanent)] => 37,JALAN BOLA LISUT, 13/17 SEKSYEN 13
			[City (Permanent)] => SHAH ALAM
			[State (Permanent)] => SELANGOR
			[Country (Permanent)] => Malaysia
			[Postcode (Permanent)] => 40100
			[Address (Correspondance)] => 
			[City (Correspondance)] => 
			[State (Correspondance)] => 
			[Country (Correspondance)] => 
			[Postcode (Correspondance)] => +60122733090
			[Fax] => 
			[Moblie] => +60123589936
			[Employment Status] => Self-employed
			[Company Name] => NADS MEDIA PRODUCTION SDN BHD
			[Company Address] => 2-01-2 PRESINT ALAMI, PERSIARAN AKUATIK SEKSYEN 13
			[Company Telephone Number] => +60355108236
			[Company Fax Number] => 
			[Designation] => EXECUTIVE PRODUCER
			[Position] => Officer
			[Duration of service (Start Date)] => 
			[Duration of service (End Date)] => 
			[Industry] => Marketing/Sales
			[Industry Working Experience] => 
			[Years in Industry (Working Experience)] => 
			[Health condition] => NORMAL
			[Financial Support] => Self-funding
			[Name of Sponsor] => 
			[Sponsor Code] => 
			[Scholarship Plan] => 
			[Previous Student] => yes
			[Student ID] => 1200133
			[Program] => Masters in Islamic Finance Practice
			[Mode of study] => Full-time
			[Mode of program] => Online
			[Programme Type] => 
			[Race] => MELAYU
			[Gender] => Female
			[Age] => 
			[Marital Status] => Married
			[Highest Level] => Bachelor
			[Name of University/College] => UNIVERSITI KEBANGSAAN MALAYSIA
			[Diploma/Degree Awarded] => BACHELOR OF SCIENCE (STATISTICS)
			[Class Degree] => 2nd Class Lower/Division 2
			[Result/CGPA] => 2.65
			[Year Graduated] => 2003
			[Medium of Instruction] => 
			[Proficiency in English] => MUET
			[English Language Proficiency  - Date Taken] => 
			[Result for English proficiency] => 
			[Malaysian Visa] => Yes
			[Visa Status] => Student
			[Visa Expiry Date] => 
		)*/

		$branch = $branchDB->getData($row['CP ID'], 'BranchCode');
			$branchID = $branch['IdBranch'];
			$row['Nationality'] = $row['Nationality'] == 'MALAYSIAN' ? 'MALAYSIA':$row['Nationality'];

			//applicant_profile
			$appl_info['appl_fname'] = $row['First Name'];
			$appl_info['appl_lname'] = $row['Last Name'];
			$appl_info["branch_id"] = $branchID;            
			$appl_info["appl_email"]= $formData['Email Id'];
			$appl_info["appl_password"]= '';				
			$appl_info["appl_role"]=0;
			$appl_info["create_date"]=date("Y-m-d H:i:s");
			$appl_info['appl_dob']=date('Y-m-d',strtotime($row['Dob']));
			$appl_info['appl_salutation']= $this->getIdByDef($row['Salutation'], $objdeftypeDB->fnGetDefinationsByLocale('Salutation'));
			$appl_info['appl_idnumber'] = $row['Id Number'];
			$appl_info['appl_idnumber_type'] = $this->getIdByDef($row['Id type'],$objdeftypeDB->fnGetDefinations('ID Type'));
			$appl_info['appl_nationality'] = $this->getIdByDef($row['Nationality'],$countryDB->getData(),array('k'=>'CountryName','v'=>'idCountry'));
			$appl_info['appl_religion'] = $this->getIdByDef($row['Religion'],$objdeftypeDB->fnGetDefinations('Religion'));
			$appl_info['appl_address1'] = $row['Address'];
			$appl_info['appl_city'] = $row['City'];
			$appl_info['appl_state'] = $this->getState($row['State']);
			$appl_info['appl_country'] = $this->getIdByDef($row['Country'],$countryDB->getData(),array('k'=>'CountryName','v'=>'idCountry'));
			$appl_info['appl_postcode'] = $row['Postcode'];
			$appl_info['appl_phone_home'] = $row['Contact No.'];
			$appl_info['appl_fax'] = $row['Fax'];
			$appl_info['appl_phone_mobile'] = $row['Moblie'];
			$appl_info['prev_student'] = $row['Previous Student'] == 'yes' ? 1 : 0;
			$appl_info['prev_studentID'] = $row['Student ID'];
			$appl_info['appl_race'] = $this->getIdByDef($row['Race'],$objdeftypeDB->fnGetDefinations('Race'));
			$appl_info['appl_gender'] = $row['Gender'] == 'Female' ? 2 : 1;
			$appl_info['appl_marital_status'] = $this->getIdByDef($row['Marital Status'],$objdeftypeDB->fnGetDefinations('Marital Status'));
			//

			$applicant_id = $appProfileDB->addData($appl_info);
			
			//generate applicant ID
			$gen_applicantId =  $this->generateApplicantID();

			//applicant_transaction
			$info2["at_appl_id"]=$applicant_id;
			$info2["at_pes_id"]=$gen_applicantId;
			$info2["at_create_by"]=$applicant_id;
			$info2["at_status"]=593; //offered
			$info2["at_create_date"]=date("Y-m-d H:i:s");
			$info2["entry_type"]=0;//manual			
			$info2["at_default_qlevel"]=null;	
			$info2["at_intake"] = $this->getIntake($row['Intake Session']);
		
		
			//trans ID		    					
			$at_trans_id = $appTransactionDB->addData($info2);
			
			//program
			
			$program =$this->getProgram($row["Program"]);

			
			$info3['mode_study'] = $this->getIdByDef(str_replace('-','',$row['Mode of study']),$objdeftypeDB->fnGetDefinations('Mode of Study'));
			$info3['program_mode'] = $this->getIdByDef($row['Mode of program'],$objdeftypeDB->fnGetDefinations('Mode of Program'));
			$info3['program_type'] = '';
			$info3['ap_prog_scheme'] = '';
			$info3["ap_at_trans_id"]=$at_trans_id;
			$info3["ap_prog_code"]=$program["ProgramCode"];
			$info3["ap_prog_id"]=$program['IdProgram'];		
			
			$appProgramDB  = new App_Model_Application_DbTable_ApplicantProgram();			
			$appProgramDB->addData($info3);
			
			//qualification
			$data6 = array(	    		   
				'ae_appl_id' => $applicant_id,
				'ae_transaction_id' => $at_trans_id,
				'ae_qualification' => $this->getIdByDef($row['Highest Level'],$entryReqDB->getListGeneralReq(),array('k'=>'QualificationLevel','v'=>'IdQualification')),
				'ae_degree_awarded' => $row['Diploma/Degree Awarded'],
				'ae_class_degree' => $this->getIdByDef($row['Class Degree'],$objdeftypeDB->fnGetDefinations('Class Degree')),
				'ae_result' => $row['Result/CGPA'],
				'ae_year_graduate' => $row['Year Graduated'],
				'ae_institution_country' => '',
				'ae_institution' => '',
				'ae_medium_instruction' => '',	
				'others' => '',	    		  	    		
				'createDate' => date("Y-m-d H:i:s")
			);

			$ae_id = $appQualificationDB->addData($data6);

			//english
			$data4 = array(    			 
				'ep_transaction_id' => $at_trans_id,
				'ep_test' => $this->getIdByDef($row['Proficiency in English'],$objdeftypeDB->fnGetDefinations('English Proficiency Test List')),
				'ep_test_detail' => $formData['english_test_detail'],
				'ep_date_taken' => date('Y-m-d',strtotime($formData['English Language Proficiency - Date Taken'])),
				'ep_score' => $formData['Result for English proficiency'] == '' ? 0 : $formData['Result for English proficiency'],
				'ep_updatedt' => date("Y-m-d H:i:s"),
				'ep_updateby'=>$applicant_id
			);
			
			$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
			$ep_id=$appEngProfDB->addData($data4);
			
		
			//employment info
			$data8 = array(	    	
				'ae_appl_id' => $applicant_id,
				'ae_trans_id' => $at_trans_id,	 
				'ae_status' => $this->getIdByDef($row['Employment Status'],$objdeftypeDB->fnGetDefinations('Employment Status')),
				'ae_comp_name' => $row['Company Name'],
				'ae_comp_address' => $row['Company Address'],
				'ae_comp_phone' => $row['Company Telephone Number'],
				'ae_comp_fax' => $row['Company Fax Number'],
				'ae_designation' => $row['Designation'],
				'ae_position' => $formData['Position'],
				'ae_from' => date('Y-m-d',strtotime($row['Duration of service (Start Date)'])),
				'ae_to' => date('Y-m-d',strtotime($row['Duration of service (End Date)'])),	    		  
				'emply_year_service' => '',
				'ae_industry' => $this->getIdByDef($row['Industry'],$objdeftypeDB->fnGetDefinations('Industry Working Experience')),	 
				'ae_job_desc' => '',	    		   	    		
				'upd_date' => date("Y-m-d H:i:s")
			);
							
			$appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();
			$ae_id = $appEmploymentDB->addData($data8);
			
			//health condition
			$appHealthDB = new App_Model_Application_DbTable_ApplicantHealthCondition();
			$data9 = array(    				   
				'ah_appl_id' => $applicant_id,
				'ah_trans_id' => $at_trans_id,
				'ah_status' => $this->getIdByDef($row['Health condition'],$objdeftypeDB->fnGetDefinations('Industry Working Experience')),
				'upd_date' => date("Y-m-d H:i:s")
			);
						
			$appHealthDB->addData($data9);

			//financial support
			$appFinancial = new App_Model_Application_DbTable_ApplicantFinancial();

			$data10 = array(
				'af_appl_id' => $applicant_id,
				'af_trans_id' => $at_trans_id,
				'af_method' => $this->getIdByDef($row['Financial Support'],$objdeftypeDB->fnGetDefinations('Funding Method')),
				'af_sponsor_name' => $row['Name of Sponsor'],
				'af_type_scholarship' => '',
				'af_scholarship_secured' => $row['Scholarship Plan'],
				'af_scholarship_apply' => '',
				'upd_date' => date("Y-m-d H:i:s")
			);

			$af_id = $appFinancial->addData($data10);

			//visa
			$data11 = array(
				'av_appl_id' => $applicant_id,
				'av_trans_id' => $at_trans_id,
				'av_malaysian_visa' => $row['Malaysian Visa'] == 'Yes' ? 1 : 0,
				'av_status' => $this->getIdByDef($row['Visa Status'],$objdeftypeDB->fnGetDefinations('Visa Status')),
				'av_expiry' => date('Y-m-d',strtotime($row['Visa Expiry Date'])),
				'upd_date' => date("Y-m-d H:i:s")
			);
			
			$appVisaDB->insert($data11);

			//item
			$data = array(
							'b_id'				=> $this->bulk_id,
							'bi_trans_id'		=> $at_trans_id,
							'bi_appl_id'		=> $applicant_id
						);
			
			$bulk_id = $this->bulkDB->addItem($data);

			$total++;
	}

	function getState($state)
	{
		$db = getDB();
	  	$select = $db->select()
	                 ->from(array('s'=>'tbl_state'))
	                 ->where('LOWER(s.StateName) = ?', strtolower($state));
        $row = $db->fetchRow($select);
		
		if ( !empty($row) )
		{
			return $row['idState'];
		}
		else
		{
			return '';
		}
	}
	

	public function getProgram($name='')
	{
		$db = getDb();

		$sql =  $db->select()
	                 ->from(array('a'=>'tbl_program'),array('a.*'))
	                 ->where('LOWER(a.ProgramName) = ?', strtolower($name));
	
		$result = $db->fetchRow($sql);

		if ( empty($result) )
		{
			return '';
		}
		else
		{
			return $result;
		}
	}
	
	public function getIntake($name='')
	{
		$db = getDb();

		$sql =  $db->select()
	                 ->from(array('a'=>'tbl_intake'),array('IdIntake'))
	                 ->where('a.IntakeDesc = ?', $name);

		$result = $db->fetchRow($sql);

		if ( empty($result) )
		{
			return '';
		}
		else
		{
			return $result['IdIntake'];
		}
	}

	public function getIdByDef($id, $data, $map = array('k'=>'DefinitionDesc','v'=>'idDefinition') )
	{
		if ( !empty($data) )
		{
			
			foreach ( $data as $row )
			{
				if ( strtolower($row[$map['k']]) == strtolower($id) )
				{
					$val = $row[$map['v']];
				}
			}
		}

		if ( empty($val) )
		{
			return 0;
		}
		else
		{
			return $val;
		}
	}

	function generateApplicantID(){
		
		//generate applicant ID
		$applicantid_format = array();
		
		$seqnoDb = new App_Model_Application_DbTable_ApplicantSeqno();
		$sequence = $seqnoDb->getData(date('Y'));
		
		$tblconfigDB = new App_Model_General_DbTable_TblConfig();
		$config = $tblconfigDB->getConfig(1);
		
		//format
		 $format_config = explode('|',$config['ApplicantIdFormat']);
                 //var_dump($format_config);
		for($i=0; $i<count($format_config); $i++){
			$format = $format_config[$i];
			if($format=='px'){ //prefix
				$result = $config['ApplicantPrefix'];
			}elseif($format=='yyyy'){
				$result = date('Y');
			}elseif($format=='yy'){
				$result = date('y');
			}elseif($format=='seqno'){				
				$result = sprintf("%05d", $sequence['seq_no']);
			}
                        //var_dump($result);
			array_push($applicantid_format,$result);
		}
	
		//var_dump(implode("-",$applicantID));
		$applicantID = implode("",$applicantid_format);
		
		//update sequence		
		$seqnoDb->updateData(array('seq_no'=>$sequence['seq_no']+1),$sequence['seq_id']);

		$checkDuplicate = $seqnoDb->checkduplicate($applicantID);

		if ($checkDuplicate){
			$applicantID = $this->generateApplicantID();
		}
		
		//echo $applicantID;
		return	$applicantID;
	}
	
	
	
}
?>