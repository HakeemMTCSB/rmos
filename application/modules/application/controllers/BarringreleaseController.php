<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Application_BarringreleaseController extends Zend_Controller_Action{
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Application_Model_DbTable_Barringrelease();
    }
    
    public function indexAction(){
        $this->view->title = $this->view->translate("Barring/Release");
        
        $barringList = $this->model->getBarringList();
        $this->view->barringList = $barringList;
        //var_dump($barringList);
    }
    
    public function addbarringAction(){
        $this->view->title = $this->view->translate("New Barring");
        $form = new Application_Form_BarringForm();
        $this->view->form = $form;
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)){
                $barringData = array(
                    'tbr_category'=>$formData['tbr_category'], 
                    'tbr_appstud_id'=>$formData['id'], 
                    'tbr_type'=>$formData['tbr_type'], 
                    'tbr_intake'=>$formData['tbr_intake'], 
                    'tbr_reason'=>$formData['tbr_reason'], 
                    'tbr_status'=>0, 
                    'tbr_createby'=>$userId, 
                    'tbr_createdate'=>date('Y-m-d'), 
                    'tbr_updby'=>$userId, 
                    'tbr_upddate'=>date('Y-m-d')
                );
                $barId = $this->model->insertBarring($barringData);
                
                //log history
                $barringHisData = array(
                    'tbr_id'=>$barId,
                    'tbr_category'=>$formData['tbr_category'], 
                    'tbr_appstud_id'=>$formData['id'], 
                    'tbr_type'=>$formData['tbr_type'], 
                    'tbr_intake'=>$formData['tbr_intake'], 
                    'tbr_reason'=>$formData['tbr_reason'], 
                    'tbr_status'=>0,
                    'tbr_changeType'=>1,
                    'tbr_createby'=>$userId, 
                    'tbr_createdate'=>date('Y-m-d'), 
                    'tbr_updby'=>$userId, 
                    'tbr_upddate'=>date('Y-m-d')
                );
                $barHisId = $this->model->insertBarringHistory($barringHisData);
            }
            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl . '/application/barringrelease/editbarring/id/'.$barId);
        }
    }
    
    public function editbarringAction(){
        $this->view->title = $this->view->translate("Edit Barring");
        $id = $this->_getParam('id', 0);
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($id != 0){
            $barringInfo =  $this->model->getBarring($id);
            $tbr_appstud_id = $barringInfo['tbr_appstud_id'];
            if ($barringInfo['tbr_category']==1){
                $barringInfo['tbr_appstud_id']=$barringInfo['appName'];
            }else{
                $barringInfo['tbr_appstud_id']=$barringInfo['studName'];
            }
            
            $form = new Application_Form_BarringForm(array('category'=>$barringInfo['tbr_category']));
            $form->populate($barringInfo);
            
            $this->view->form = $form;
            
            if ($this->getRequest()->isPost()){
                $formData = $this->getRequest()->getPost();
                
                if ($form->isValid($formData)){
                    $barringData = array(
                        //'tbr_category'=>$formData['tbr_category'], 
                        //'tbr_appstud_id'=>$formData['tbr_appstud_id'], 
                        'tbr_type'=>$formData['tbr_type'], 
                        'tbr_intake'=>$formData['tbr_intake'], 
                        'tbr_reason'=>$formData['tbr_reason'], 
                        'tbr_status'=>0, 
                        'tbr_updby'=>$userId, 
                        'tbr_upddate'=>date('Y-m-d')
                    );
                    $this->model->updateBarring($barringData, $id);
                    
                    //log history
                    $barringHisData = array(
                        'tbr_id'=>$id,
                        'tbr_category'=>$barringInfo['tbr_category'], 
                        'tbr_appstud_id'=>$tbr_appstud_id, 
                        'tbr_type'=>$barringInfo['tbr_type'], 
                        'tbr_intake'=>$barringInfo['tbr_intake'], 
                        'tbr_reason'=>$barringInfo['tbr_reason'], 
                        'tbr_status'=>$barringInfo['tbr_status'],
                        'tbr_changeType'=>2,
                        'tbr_createby'=>$userId, 
                        'tbr_createdate'=>date('Y-m-d'), 
                        'tbr_updby'=>$userId, 
                        'tbr_upddate'=>date('Y-m-d')
                    );
                    $barHisId = $this->model->insertBarringHistory($barringHisData);
                }
                //redirect
                $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
                $this->_redirect($this->baseUrl . '/application/barringrelease/editbarring/id/'.$id);
            }
        }else{
            //redirect
            $this->_redirect($this->baseUrl . '/application/barringrelease/index');
        }
    }
    
    public function addreleaseAction(){
        $this->view->title = $this->view->translate("New Release");
        $form = new Application_Form_BarringForm();
        $this->view->form = $form;
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)){
                $barringData = array(
                    'tbr_category'=>$formData['tbr_category'], 
                    'tbr_appstud_id'=>$formData['id'], 
                    'tbr_type'=>$formData['tbr_type'], 
                    'tbr_intake'=>$formData['tbr_intake'], 
                    'tbr_reason'=>$formData['tbr_reason'], 
                    'tbr_status'=>1, 
                    'tbr_createby'=>$userId, 
                    'tbr_createdate'=>date('Y-m-d'), 
                    'tbr_updby'=>$userId, 
                    'tbr_upddate'=>date('Y-m-d')
                );
                $barId = $this->model->insertBarring($barringData);
                
                //log history
                $barringHisData = array(
                    'tbr_id'=>$barId,
                    'tbr_category'=>$formData['tbr_category'], 
                    'tbr_appstud_id'=>$formData['id'], 
                    'tbr_type'=>$formData['tbr_type'], 
                    'tbr_intake'=>$formData['tbr_intake'], 
                    'tbr_reason'=>$formData['tbr_reason'], 
                    'tbr_status'=>1,
                    'tbr_changeType'=>1,
                    'tbr_createby'=>$userId, 
                    'tbr_createdate'=>date('Y-m-d'), 
                    'tbr_updby'=>$userId, 
                    'tbr_upddate'=>date('Y-m-d')
                );
                $barHisId = $this->model->insertBarringHistory($barringHisData);
            }
            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            $this->_redirect($this->baseUrl . '/application/barringrelease/editrelease/id/'.$barId);
        }
    }
    
    public function editreleaseAction(){
        $this->view->title = $this->view->translate("Edit Release");
        $id = $this->_getParam('id', 0);
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($id != 0){
            $barringInfo =  $this->model->getBarring($id);
            $tbr_appstud_id = $barringInfo['tbr_appstud_id'];
            if ($barringInfo['tbr_category']==1){
                $barringInfo['tbr_appstud_id']=$barringInfo['appName'];
            }else{
                $barringInfo['tbr_appstud_id']=$barringInfo['studName'];
            }
            
            $form = new Application_Form_BarringForm(array('category'=>$barringInfo['tbr_category']));
            $form->populate($barringInfo);
            
            $this->view->form = $form;
            
            if ($this->getRequest()->isPost()){
                $formData = $this->getRequest()->getPost();
                
                if ($form->isValid($formData)){
                    $barringData = array(
                        //'tbr_category'=>$formData['tbr_category'], 
                        //'tbr_appstud_id'=>$formData['tbr_appstud_id'], 
                        'tbr_type'=>$formData['tbr_type'], 
                        'tbr_intake'=>$formData['tbr_intake'], 
                        'tbr_reason'=>$formData['tbr_reason'], 
                        'tbr_status'=>1, 
                        'tbr_updby'=>$userId, 
                        'tbr_upddate'=>date('Y-m-d')
                    );
                    $this->model->updateBarring($barringData, $id);
                    
                    //log history
                    $barringHisData = array(
                        'tbr_id'=>$id,
                        'tbr_category'=>$barringInfo['tbr_category'], 
                        'tbr_appstud_id'=>$tbr_appstud_id, 
                        'tbr_type'=>$barringInfo['tbr_type'], 
                        'tbr_intake'=>$barringInfo['tbr_intake'], 
                        'tbr_reason'=>$barringInfo['tbr_reason'], 
                        'tbr_status'=>$barringInfo['tbr_status'],
                        'tbr_changeType'=>2,
                        'tbr_createby'=>$userId, 
                        'tbr_createdate'=>date('Y-m-d'), 
                        'tbr_updby'=>$userId, 
                        'tbr_upddate'=>date('Y-m-d')
                    );
                    $barHisId = $this->model->insertBarringHistory($barringHisData);
                }
                //redirect
                $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
                $this->_redirect($this->baseUrl . '/application/barringrelease/editrelease/id/'.$id);
            }
        }else{
            //redirect
            $this->_redirect($this->baseUrl . '/application/barringrelease/index');
        }
    }
    
    public function getstudappAction(){
        $searchElement = $this->_getParam('term', '');
        $id = $this->_getParam('id', 1);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        if ($id == 1){
            $list = $this->model->getApplicantList($searchElement);
        }else{
            $list = $this->model->getStudentList($searchElement); 
        }
        
        $json = Zend_Json::encode($list);
		
	echo $json;
	exit();
    }
    
    public function getsemesmerintakeAction(){
        $id = $this->_getParam('id', 1);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        if ($id == 1){
            $list = $this->model->getIntakeList();
        }else{
            $list = $this->model->getSemesterList(); 
        }
        
        $json = Zend_Json::encode($list);
		
	echo $json;
	exit();
    }
    
    public function deleteBarringAction(){
        $id = $this->_getParam('id', 0);
        
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        
        if ($id != 0){
            $barringInfo =  $this->model->getBarring($id);
            
            //log history
            $barringHisData = array(
                'tbr_id'=>$id,
                'tbr_category'=>$barringInfo['tbr_category'], 
                'tbr_appstud_id'=>$barringInfo['tbr_appstud_id'], 
                'tbr_type'=>$barringInfo['tbr_type'], 
                'tbr_intake'=>$barringInfo['tbr_intake'], 
                'tbr_reason'=>$barringInfo['tbr_reason'], 
                'tbr_status'=>$barringInfo['tbr_status'],
                'tbr_changeType'=>0,
                'tbr_createby'=>$userId, 
                'tbr_createdate'=>date('Y-m-d'), 
                'tbr_updby'=>$userId, 
                'tbr_upddate'=>date('Y-m-d')
            );
            $barHisId = $this->model->insertBarringHistory($barringHisData);
            
            //delete
            $this->model->deleteBarring($id);
            
            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => 'Delete Success'));
        }
        
        $this->_redirect($this->baseUrl . '/application/barringrelease/index');
        exit;
    }
}
?>

