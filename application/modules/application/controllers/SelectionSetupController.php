<?php
class Application_SelectionSetupController extends Base_Base
{

	public function indexAction(){
	
		$this->view->title = $this->view->translate("Nomor Setup");
	
    	$msg = $this->_getParam('msg', null);
    	$this->view->msg = $msg;
    	
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Data has been saved.");
		}
		
					    
		//get lock data
    	$usmDB =  new App_Model_Application_DbTable_ApplicantAssessmentUsmDetl();
    	$list_nomor = $usmDB->getPaginateNomor();	
    	$lockDate = $usmDB->getLockData();
    	
    	$l_date = array();
    	foreach ($lockDate as $date_l){
    		$l_date[] = date('n-j-Y', strtotime($date_l['date']));
    	}    	
    	$this->view->localDate = $l_date;
    	
    	//academic year for save
    	$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
    	$academicYearList = $academicYearDb->getAcademicYear(array('NEXT','FUTURE'));
    	$this->view->academicYearList = $academicYearList;
    	
		$data= array();
		$i=0;
		foreach($list_nomor as $list){
			
			$data[$i]["id"]=$list["aaud_id"];
			$data[$i]["nomor"]=$list["aaud_nomor"];
			$data[$i]["decree_date"]=$list["aaud_decree_date"];
			$data[$i]["academic_year"]=$list["ay_code"];
			$data[$i]["reg_start_date"]=$list["aaud_reg_start_date"];
			$data[$i]["reg_end_date"]=$list["aaud_reg_end_date"];
			$data[$i]["payment_start_date"]=$list["aaud_payment_start_date"];
			$data[$i]["payment_end_date"]=$list["aaud_payment_end_date"];
			$data[$i]["status"]=$list["aaud_lock_status"];
			
		$i++;}
				
		$this->view->data = $data;
		
	} 
	
	public function saveIndexAction(){
    	
		if ($this->getRequest()->isPost()) {
			
			    $formData = $this->getRequest()->getPost();	
			    $auth = Zend_Auth::getInstance(); 			    

			  
			    //save selection details
			    $info["aaud_type"]			=2;//rektor
			    $info["aaud_nomor"]			=$formData["dialog_nomor"];
			    $info["aaud_decree_date"]	=date("Y-m-d",strtotime($formData["dialog_decree_date"]));
			    $info["aaud_academic_year"]	=$formData["academic_year"];
			    $info["aaud_reg_start_date"]=date("Y-m-d",strtotime($formData["registration_start_date"]));
			    $info["aaud_reg_end_date"]	=date("Y-m-d",strtotime($formData["registration_end_date"]));
			    $info["aaud_payment_start_date"]=date("Y-m-d",strtotime($formData["first_payment_start_date"]));
			    $info["aaud_payment_end_date"]	=date("Y-m-d",strtotime($formData["first_payment_end_date"]));
			    $info["aaud_createddt"]	=date("Y-m-d H:m:s");
			    $info["aaud_createdby"]	=$auth->getIdentity()->id;
			    			  
		    	if(isset($formData['dialog_lock_decree_date']) && $formData['dialog_lock_decree_date']==1 ){
		    		  $info["aaud_lock_status"]	= 1;
		    		  $info["aaud_lock_date"]	= date("Y-m-d H:m:s");
			    	  $info["aaud_lock_by"]	    = $auth->getIdentity()->id;
		    	}
		    	
			    $assessmentDetlDb = new App_Model_Application_DbTable_ApplicantAssessmentUsmDetl();
			    $detl_id = $assessmentDetlDb->addData($info);
			    			    				
			    
			 $this->_redirect($this->view->url(array('module'=>'application','controller'=>'selection-setup','action'=>'index','msg'=>'success.'),'default',true));   
				
		}//en if post
		
		
    }
    
    
	public function setStatusAction(){
    	
    	$auth = Zend_Auth::getInstance(); 	
    	$nomor = $this->_getParam('nomor', null);
    	$status = $this->_getParam('status', null);
    	
    	$data = array(						
			'aaud_lock_status'=>$status,					 
			'aaud_lock_by'=>$auth->getIdentity()->iduser,
			'aaud_lock_date'=>date ('Y-m-d h:i:s')
		);
		
					
		 $assessmentDetlDb = new App_Model_Application_DbTable_ApplicantAssessmentUsmDetl();
   	     $assessmentDetlDb->updateStatus($data,$nomor);
    	 $this->_redirect($this->view->url(array('module'=>'application','controller'=>'selection-setup','action'=>'index','msg'=>'success.'),'default',true));
    }
}
?>