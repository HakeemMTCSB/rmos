<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_ChecklistVerificationController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $dclModel;
    private $defModel;
    private $lobjAppItem;
    private $lobjAppConfig;
    private $stpModel;
    private $tplModel;
    private $sthModel;
    private $checklistVerification;
    private $appAprove;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->dclModel = new Application_Model_DbTable_DocumentChecklist();
        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->lobjAppItem = new Application_Model_DbTable_ApplicationItem();
        $this->lobjAppConfig = new Application_Model_DbTable_ApplicationInitialConfig();
        $this->stpModel = new Application_Model_DbTable_StatusTemplate();
        $this->tplModel = new Communication_Model_DbTable_Template();
        $this->sthModel = new Application_Model_DbTable_StatusAttachment();
        $this->checklistVerification = new Application_Model_DbTable_ChecklistVerification();
        $this->appAprove = new Application_Model_DbTable_ApplicantApproval();
    }
    
    public function indexAction(){
        
        ini_set('memory_limit', '-1');
        
        $this->view->title=$this->view->translate("Checklist Verification");
        $where = 'a.at_status != 602 and at_status != 591';

        $cacheObj = new Cms_Cache();
        $cache = $cacheObj->get();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if (isset($formData['Search']) && $formData['Search']=='Search'){
                $searchForm = new Application_Form_ApplicantSearch(array('IdProgram'=>$formData['field3']));
                $searchForm->populate($formData);
                $applicantList = $this->checklistVerification->getApplicantSearchList($formData, $where);

                $cache->save($applicantList, 'checklistverificationpaginatorresult');
                //$this->gobjsessionsis->checklistverificationpaginatorresult = $applicantList;
                $this->_redirect( $this->baseUrl . '/application/checklist-verification/index/search/1');
            }else{
                $searchForm = new Application_Form_ApplicantSearch();
                $applicantList = $this->checklistVerification->fnGetApplicantList($where);
                $this->_redirect( $this->baseUrl . '/application/checklist-verification/index/');
            }
        }else{
            $searchForm = new Application_Form_ApplicantSearch();
            $applicantList = $this->checklistVerification->fnGetApplicantList($where);
        }
        
        if(!$this->_getParam('search')){
            //unset($this->gobjsessionsis->checklistverificationpaginatorresult);
            $cache->remove('checklistverificationpaginatorresult');
        }
            
        $lintpagecount = $this->gintPageCount;// Definitiontype model

        $lintpage = $this->_getParam('page',1); // Paginator instance

        $cached_results = $cache->load('checklistverificationpaginatorresult');
        if($cached_results != '') {
            $paginator = $this->lobjCommon->fnPagination($cached_results,$lintpage,$lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($applicantList,$lintpage,$lintpagecount);
        }
        
        $paginator->setItemCountPerPage(25);
        
        $this->view->paginator = $paginator;
        $this->view->searchForm = $searchForm;
    }
    
    public function checklistVerificationAction(){
        $this->view->title=$this->view->translate("Checklist Verification");
        $checklistVerificationForm = new Application_Form_ChecklistVerification();
        $appTrxId = $this->_getParam('id', null);
        $getUserIdentity = $this->auth->getIdentity();
        $this->view->role = $getUserIdentity->IdRole;
        $info = $this->checklistVerification->infoBox($appTrxId);
        $programSchemeId = $info['ap_prog_scheme'];
        $stdCtgy = $info['appl_category'];
        $docChecklist = $this->checklistVerification->fnGetDocCheckList($programSchemeId, $stdCtgy, $appTrxId);
        //var_dump($docChecklist); exit;
        $this->view->info = $info;
        $this->view->docChecklist = $docChecklist;
        $this->view->appTrxId = $appTrxId;
        $this->view->checklistVerificationForm = $checklistVerificationForm;
        
        $mop = $this->defModel->getData($info['mode_of_program']); 
        $mod = $this->defModel->getData($info['mode_of_study']);
        $programType = $this->defModel->getData($info['program_type']);
        $studentCategory = $this->defModel->getData($stdCtgy);
        
        //pass info to view
        $this->view->applicantName = $info['appl_fname'].' '.$info['appl_mname'].' '.$info['appl_lname'];
        $this->view->applicantId = $info['at_pes_id'];
        $this->view->programName = $info['ProgramName'];
        $this->view->programScheme = $programType['DefinitionDesc']." ".$mop['DefinitionDesc']." ".$mod['DefinitionDesc'];
        $this->view->studentCategory = $studentCategory['DefinitionDesc'];
        $this->view->intake = $info['IntakeDesc'];
        $this->view->status = $info['at_status'];
		
		$chkdocDb = new Application_Model_DbTable_ChecklistDocuments();
		$hasdocs = $chkdocDb->getData($info['at_pes_id']);
		
		$this->view->hasdocs = empty($hasdocs) ? false : true;
        
        $this->view->editSuccess = 0;
        /*$class = new icampus_Function_Studentfinance_Invoice();
        $proformaInfo = $class->getApplicantProformaInfo($appTrxId, 1);
        var_dump($proformaInfo);
        exit;*/
        
        //update payment
        $this->checkPaymentStatusAction($appTrxId);
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($checklistVerificationForm->isValid($formData)){
                
                $this->checklistVerification->deleteStatusByTrx($appTrxId);
                //var_dump($formData); exit;
                $adsarr = array();
                
                if (count($formData['dcl_Id']) > 0){
                    $i=0;
                    foreach ($formData['dcl_Id'] as $docStatusLoop){
                        $adsCheck = $this->checklistVerification->fnGetChecklistStatusByTxnId($appTrxId, $docStatusLoop, $formData['table_id'][$i], $formData['section_Id'][$i]);
                        $adId = Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($appTrxId, $docStatusLoop, 'ad_id', $formData['table_id'][$i], $formData['section_Id'][$i]);
                        $getdcl = $this->checklistVerification->getDclById($docStatusLoop);
                        //var_dump($getdcl);
                        if ($adsCheck){ //update
                            
                            if ($getdcl['dcl_finance']==0){
                                $data = array(
                                    'ads_ad_id'=>$adId,
                                    //'ads_appl_id'=>'',
                                    //'ads_confirm'=>$check,
                                    'ads_status'=>$formData['field2'][$i],
                                    'ads_comment'=>$formData['comment'][$i],
                                    'ads_table_name'=>$formData['table_name'][$i],
                                    'ads_table_id'=>$formData['table_id'][$i],
                                    //'ads_modBy'=>$getUserIdentity->id,
                                    //'ads_modDate'=>date('Y-m-d H:i:s')
                                );

                                if ($adsCheck[0]['ads_status']!=$formData['field2'][$i] && $formData['field2'][$i]!=0 && $formData['field2'][$i]!=1){
                                    $data['ads_modBy']=$getUserIdentity->id;
                                    $data['ads_modDate']=date('Y-m-d H:i:s');
                                }
                                
                                $this->checklistVerification->update($data, 'ads_txn_id = '.$appTrxId.' and ads_dcl_id = '.$docStatusLoop.' and ads_table_id='.$formData['table_id'][$i].' and ads_section_id='.$formData['section_Id'][$i]);
                            }
                            
                            array_push($adsarr, $adsCheck[0]['ads_id']);
                        }else{ //insert
                            $data = array(
                                'ads_txn_id'=>$appTrxId,
                                'ads_dcl_id'=>$docStatusLoop,
                                'ads_ad_id'=>$adId,
                                //'ads_appl_id'=>'',
                                //'ads_confirm'=>$check,
                                'ads_section_id'=>$formData['section_Id'][$i],
                                'ads_table_name'=>$formData['table_name'][$i],
                                'ads_table_id'=>$formData['table_id'][$i],
                                'ads_status'=>$formData['field2'][$i],
                                'ads_comment'=>$formData['comment'][$i],
                                'ads_createBy'=>$getUserIdentity->id,
                                'ads_createDate'=>date('Y-m-d H:i:s'),
                                /*'ads_modBy'=>$getUserIdentity->id,
                                'ads_modDate'=>date('Y-m-d H:i:s')*/
                            );
                            
                            if ($getdcl['dcl_finance']==0 && $formData['field2'][$i]!=0 && $formData['field2'][$i]!=1){
                                $data['ads_modBy']=$getUserIdentity->id;
                                $data['ads_modDate']=date('Y-m-d H:i:s');
                            }
                            
                            $newid = $this->checklistVerification->insert($data);
                            array_push($adsarr, $newid);
                        }
                        $i++;
                    }
                }
                
                //membasmi checklist yang sesat dan kotor.....
                $this->checklistVerification->deleteChecklistSesat($appTrxId, $adsarr);
                
                $docList = $this->checklistVerification->fnGetChecklistStatus($appTrxId);
                //var_dump($docList);
                if (count($docList) > 0){
                    $i=0;
                    foreach($docList as $docLoop){
                        if(($docLoop['ads_status']==4 || $docLoop['ads_status']==5) && $docLoop['dcl_mandatory']==1){
                            $i++;
                        }
                    }
                    //var_dump(count($docList));var_dump($i); exit;
                    if (count($docList)==$i){
                        
                        $getStatus = $this->defModel->getIdByDefType('Status', 'Complete');
                        
                        $oldStatus = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_status', 'at_trans_id = '.$appTrxId);
                        
                        if (($oldStatus == '592') || ($oldStatus == '595')){
                        
                            $data = array(
                                'at_status'=>$getStatus['key']
                            );
                            $this->checklistVerification->updateAppTrans($data, $appTrxId);

                            $dataHistory = array(
                                'ash_trans_id'=>$appTrxId,
                                'ash_status'=>$data['at_status'],
                                'ash_oldStatus'=>$oldStatus,
                                'ash_changeType'=>1,
                                'ash_updUser'=>$getUserIdentity->id,
                                'ash_userType'=>1,
                                'ash_updDate'=>date('Y-m-d H:i:s')
                            );

                            $this->checklistVerification->storeStatusUpdateHistory($dataHistory);
                            $this->view->status = $getStatus['key'];
                        }
                    }else{
                        
                        $getStatus = $this->defModel->getIdByDefType('Status', 'Incomplete');
                        
                        $oldStatus = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_status', 'at_trans_id = '.$appTrxId);
                        
                        if (($oldStatus == '592') || ($oldStatus == '595')){
                        
                            $data = array(
                                'at_status'=>$getStatus['key']
                            );
                            $this->checklistVerification->updateAppTrans($data, $appTrxId);

                            $dataHistory = array(
                                'ash_trans_id'=>$appTrxId,
                                'ash_status'=>$data['at_status'],
                                'ash_oldStatus'=>$oldStatus,
                                'ash_changeType'=>1,
                                'ash_updUser'=>$getUserIdentity->id,
                                'ash_userType'=>1,
                                'ash_updDate'=>date('Y-m-d H:i:s')
                            );

                            $this->checklistVerification->storeStatusUpdateHistory($dataHistory);
                            $this->view->status = $getStatus['key'];
                        }
                    }
                }
                $this->view->editSuccess = 1;
                //$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
            }
        }
    }
    
    public function downloadDocumentAction(){
        
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $getUserIdentity = $this->auth->getIdentity();
        
        $adId = $this->_getParam('id', null);
        $trxId = $this->_getParam('trxId', null);
        
        $downloadInfo = Application_Model_DbTable_ChecklistVerification::fnGetDownloadInfo($adId);
        //var_dump($downloadInfo); exit;
        $file = APP_DOC_PATH.$downloadInfo['ad_filepath'];

        // Ophalen bestand
        if (file_exists($file)) {
            
            $adsCheck = $this->checklistVerification->fnGetChecklistStatusByTxnId($trxId, $downloadInfo['ad_dcl_id'], $downloadInfo['ad_table_name'], $downloadInfo['ad_section_id']);
            
            if (empty($adsCheck)){ //insert status checklist
                //var_dump($adsCheck); exit;
                $data = array(
                    'ads_txn_id'=>$trxId,
                    'ads_dcl_id'=>$downloadInfo['ad_dcl_id'],
                    'ads_ad_id'=>$adId,
                    //'ads_appl_id'=>'',
                    'ads_confirm'=>0,
                    'ads_section_id'=>$downloadInfo['ad_section_id'],
                    'ads_table_name'=>$downloadInfo['ad_table_name'],
                    'ads_table_id'=>$downloadInfo['ad_table_id'],
                    'ads_status'=>2,
                    'ads_createBy'=>$getUserIdentity->id,
                    'ads_createDate'=>date('Y-m-d H:i:s')
                );
                $this->checklistVerification->insert($data);
            }
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            //header('Content-Transfer-Encoding: binary');
            //header('Expires: 0');
            //header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            //header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            //ob_clean();
            //flush();
            readfile($file);
            //$this->_redirect($downloadInfo['ad_filepath']);
        }else{
            throw new Exception("file not exist");
        }
        
        exit();
    }
    
	public function assigndocAction()
	{
		$chkdocDb = new Application_Model_DbTable_ChecklistDocuments();

        $txnId = $this->_getParam('txnId',null);
        $dclId = $this->_getParam('dclId',null);
		$tableId = $this->_getParam('tableId',null);
        $sectionId = $this->_getParam('sectionId',null);
        
        $this->_helper->layout->disableLayout();
		$getUserIdentity = $this->auth->getIdentity();

		$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
		
		//post

		 if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
			
			$txnData = $txnDb->getData($formData['txn_id'],'at_trans_id');
			if ( empty($txnData) )
			{
				throw new Exception('Invalid Applicant Txn ID');
			}
			
			$this->migrate_path = str_replace('/documents','', DOCUMENT_PATH);

			$files = count($formData['file']);
			if ( $files > 0 )
			{
				foreach ( $formData['file'] as $file )
				{
					
					$filedata = $chkdocDb->getSingle($file);

					$ori_filename = $filedata['filename'][8] == '_' ? substr($filedata['filename'],9) : substr($filedata['filename'],8);
					$filename= date('Ymd').'_'.$ori_filename;

					$ori_location = $this->migrate_path.'/migrate/AppDoc/'.$filedata['filename'];
					$destination = DOCUMENT_PATH.$txnData['at_repository'].'/'.$filename;

					//echo $ori_location.'<br />';
					//echo $destination;
					//exit;

					//copy file
					copy($ori_location, $destination);

					$data = array(
						'ad_txn_id' => $formData['txn_id'],
						'ad_dcl_id' => $formData['dcl_id'],
						'ad_table_id' => $formData['table_id'],
						'ad_section_id' => $formData['section_id'],
						'ad_table_name' => Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = '.$formData['section_id']),
						'ad_filepath' => $txnData['at_repository'].'/'.$filename,
						'ad_filename' => $filename,
						'ad_ori_filename' => $ori_filename,
						'ad_createddt'=> date('Y-m-d'),
						'ad_createdby'=> $getUserIdentity->id,
						'ad_role'=>'admin'
					);
					
					$upload_id = $this->checklistVerification->insertUploadData($data);

					//update
					$chkdocDb->updateData(array('assigned_to' => $upload_id), $filedata['id']);

					$this->_helper->flashMessenger->addMessage(array('success' => "Document checklist updated"));
					
					$info = $this->checklistVerification->infoBox($formData['txn_id']);

					$this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'checklist-verification', 'action'=>'checklist-verification', 'id'=>$formData['txn_id'], 'programScheme'=>$info['ap_prog_scheme'], 'stdCtgy'=>$info['appl_category']),'default',true);
					$this->_redirect($this->view->backURL);
				}
			}
		 }
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
		$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
		
		$txnData = $txnDb->getData($txnId,'at_trans_id');

		
		if ( empty($txnData) )
		{
			throw new Exception('Invalid Applicant Txn ID');
		}

		
		$hasdocs = $chkdocDb->getData($txnData['at_pes_id']);
		
		foreach ( $hasdocs as $key => $doc )
		{
			$hasdocs[$key]['real_filename'] = $doc['filename'][8] == '_' ? substr($doc['filename'],9) : substr($doc['filename'],8);
		}
		
        $json = Zend_Json::encode($hasdocs);
		
		echo $json;
		exit();
    }

    public function documentlistAction(){
        $trxId = $this->_getParam('trxId',null);
        $dclId = $this->_getParam('dclId',null);
        $tableId = $this->_getParam('tableId',null);
        $sectionId = $this->_getParam('sectionId',null);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $adId = Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($trxId, $dclId, 'ad_id', $tableId, $sectionId);
        
        if ($adId){
            $getUserIdentity = $this->auth->getIdentity();
            $downloadInfo = Application_Model_DbTable_ChecklistVerification::fnGetDownloadInfo($adId);

            $adsCheck = $this->checklistVerification->fnGetChecklistStatusByTxnId($trxId, $downloadInfo['ad_dcl_id'], $downloadInfo['ad_table_id'], $downloadInfo['ad_section_id']);

            if (empty($adsCheck)){ //insert status checklist
                //var_dump($adsCheck); exit;
                /*$data = array(
                    'ads_txn_id'=>$trxId,
                    'ads_dcl_id'=>$downloadInfo['ad_dcl_id'],
                    'ads_ad_id'=>$adId,
                    //'ads_appl_id'=>'',
                    'ads_confirm'=>0,
                    'ads_section_id'=>$downloadInfo['ad_section_id'],
                    'ads_table_name'=>$downloadInfo['ad_table_name'],
                    'ads_table_id'=>$downloadInfo['ad_table_id'],
                    'ads_status'=>2,
                    'ads_createBy'=>$getUserIdentity->id,
                    'ads_createDate'=>date('Y-m-d H:i:s')
                );
                $this->checklistVerification->insert($data);*/
            }
        }
        
        $docList = $this->checklistVerification->getDocumentUpload($trxId, $dclId, $sectionId, $tableId);

        if ($docList){
            foreach ($docList as $key => $docLoop){
                $docList[$key]['ad_filepath'] = htmlspecialchars($docLoop['ad_filepath'], ENT_QUOTES);
            }
        }

        $json = Zend_Json::encode($docList);
		
	echo $json;
	exit();
    }
    
    public function uploadFileAction(){
        
        $getUserIdentity = $this->auth->getIdentity();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $info = $this->checklistVerification->infoBox($formData['trx_id']);
            
            $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'checklist-verification', 'action'=>'checklist-verification', 'id'=>$formData['trx_id'], 'programScheme'=>$info['ap_prog_scheme'], 'stdCtgy'=>$info['appl_category']),'default',true);
            
            $dir = APP_DOC_PATH.$info['at_repository'];
            
            if ( !is_dir($dir) ){
                if ( mkdir_p($dir) === false )
                {
                    throw new Exception('Cannot create attachment folder ('.$dir.')');
                }
            }
            
            //upload file proses
            $adapter = new Zend_File_Transfer_Adapter_Http();
            //$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 2)); //maybe soon we will allow more?
            //$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
            //$adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx' , 'case' => false));
            $adapter->addValidator('NotExists', false, $dir);
            $adapter->setDestination($dir);

            $files = $adapter->getFileInfo();
            
            //insert upload file in db
            if ($files){
                $fileOriName = $files['file']['name'];
                $fileRename = date('YmdHis').'_'.$fileOriName;
                $filepath = $files['file']['destination'].'/'.$fileRename;
                
                $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true));
                
                //$adapter->isUploaded();
                //$adapter->receive();
                
                if ($adapter->isUploaded()){
                    if ($adapter->receive()){
                        $data = array(
                            'ad_txn_id' => $formData['trx_id'],
                            'ad_dcl_id' => $formData['dcl_id'],
                            'ad_table_id' => $formData['table_id'],
                            'ad_section_id' => $formData['section_id'],
                            'ad_table_name' => Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = '.$formData['section_id']),
                            'ad_filepath' => $info['at_repository'].'/'.$fileRename,
                            'ad_filename' => $fileRename,
                            'ad_ori_filename' => $fileOriName,
                            'ad_createddt'=> date('Y-m-d'),
                            'ad_createdby'=> $getUserIdentity->id,
                            'ad_role'=>'admin'
                        );

                        $adId = $this->checklistVerification->insertUploadData($data);
                        
                        //update status if incomplete
                        $checkstatus = $this->checklistVerification->getChecklistStatus($formData['trx_id'], $formData['dcl_id'], $formData['section_id'], $formData['table_id']);
                        
                        if ($checkstatus){
                            if ($checkstatus['ads_status'] == 3 || $checkstatus['ads_status'] == 2){
                                $data = array(
                                    'ads_ad_id'=>$adId,
                                    'ads_status'=>1,
                                );
                                
                                $this->checklistVerification->update($data, 'ads_id = '.$checkstatus['ads_id']);
                            }
                        }
                        
                        $this->_helper->flashMessenger->addMessage(array('success' => "Upload Success"));
                        $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'checklist-verification', 'action'=>'checklist-verification', 'id'=>$formData['trx_id'], 'programScheme'=>$info['ap_prog_scheme'], 'stdCtgy'=>$info['appl_category']),'default',true);
                        $this->_redirect($this->view->backURL);
                    }else{
                        $this->_helper->flashMessenger->addMessage(array('error' => "Upload Failed"));
                        $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'checklist-verification', 'action'=>'checklist-verification', 'id'=>$formData['trx_id'], 'programScheme'=>$info['ap_prog_scheme'], 'stdCtgy'=>$info['appl_category']),'default',true);
                        $this->_redirect($this->view->backURL);
                    }
                }else{
                    $this->_helper->flashMessenger->addMessage(array('error' => "Upload Failed"));
                    $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'checklist-verification', 'action'=>'checklist-verification', 'id'=>$formData['trx_id'], 'programScheme'=>$info['ap_prog_scheme'], 'stdCtgy'=>$info['appl_category']),'default',true);
                    $this->_redirect($this->view->backURL);
                }
            }else{
                throw new Exception("Upload Error (check zend file transfer)");
            }
        }else{
            throw new Exception("Upload Error (Doesn't have post method)");
        }
        
        $this->_redirect($this->view->backURL,array('prependBase'=>false));
        
	exit();
    }
    
    public function deleteUploadFileAction(){
        $adId = $this->_getParam('id',null);
        if ($adId){
            $info = $this->checklistVerification->deleteUploadFileInfo($adId);
            $info2 = $this->checklistVerification->infoBox($info['ad_txn_id']);
            $fileLocation = APP_DOC_PATH.$info['ad_filepath'];
            $uplInfo = $this->checklistVerification->getUplInfo($adId);
            
            //delete fizikal file
            unlink($fileLocation);
            
            //delete file data
            $this->checklistVerification->deleteUploadFile($adId);

			$db = getDB();

			$db->update('migrate_applicant_files', array('assigned_to' => null),  array('assigned_to = ?' => $adId));
            
            $checkUplFile = $this->checklistVerification->checkUplFile($uplInfo['ad_txn_id'], $uplInfo['ad_dcl_id'], $uplInfo['ad_section_id'], $uplInfo['ad_table_id']);
            //var_dump($checkUplFile); exit;
            if (!$checkUplFile){
                $checkStatus = $this->checklistVerification->getStatus($uplInfo['ad_txn_id'], $uplInfo['ad_dcl_id'], $uplInfo['ad_section_id'], $uplInfo['ad_table_id']);
                if ($checkStatus){
                    $this->checklistVerification->deleteStatusData($checkStatus['ads_id']);
                }
            }
            
            $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'checklist-verification', 'action'=>'checklist-verification', 'id'=>$info['ad_txn_id'], 'programScheme'=>$info2['ap_prog_scheme'], 'stdCtgy'=>$info2['appl_category']),'default',true);
            
            $this->_redirect($this->view->backURL);
        }else{
            throw new Exception("delete error");
        }
    }
    
    public function addChecklistAction(){
        $this->_helper->layout->disableLayout();
        $appTrxId = $this->_getParam('id', null);
        $info = $this->checklistVerification->infoBox($appTrxId);
        $form = new Application_Form_AddSpesificChecklist(array('std_Ctgy'=>$info['appl_category'], 'IdProgramScheme'=>$info['ap_prog_scheme']));
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            $formData['dcl_programScheme'] = $info['ap_prog_scheme'];
            $formData['dcl_stdCtgy'] = $info['appl_category'];
            $formData['dcl_sectionid'] = 23;
            //$formData['dcl_finance'] = 23;
            $formData['dcl_specific'] = $appTrxId;
            $formData['dcl_priority'] = 100;
            $this->checklistVerification->insertNewChecklist($formData);
            $this->_helper->flashMessenger->addMessage(array('success' => "Add Checklist Success"));
            $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'checklist-verification', 'action'=>'checklist-verification', 'id'=>$appTrxId, 'programScheme'=>$info['ap_prog_scheme'], 'stdCtgy'=>$info['appl_category']),'default',true);
            $this->_redirect($this->view->backURL);
        }
    }
    
    public function deleteChecklistAction(){
        $dclId = $this->_getParam('id', null);
        $trxId = $this->_getParam('trxId', null);
        
        $info = $this->checklistVerification->infoBox($trxId);
        
        if ($dclId != null){
            $this->_helper->flashMessenger->addMessage(array('success' => "Delete Checklist Success"));
            $this->dclModel->delete('dcl_Id='.$dclId);
        }else{
            $this->_helper->flashMessenger->addMessage(array('error' => "Delete Checklist Failed"));
        }
        $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'checklist-verification', 'action'=>'checklist-verification', 'id'=>$trxId, 'programScheme'=>$info['ap_prog_scheme'], 'stdCtgy'=>$info['appl_category']),'default',true);
        $this->_redirect($this->view->backURL);
    }
    
    public function refreshPaymentStatusAction(){
        $trxId = $this->_getParam('trxId', null);
        $getUserIdentity = $this->auth->getIdentity();
        
        $fsProgramDB = new Application_Model_DbTable_ApplicantDocumentStatus();
        $checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
        
        $info = $this->checklistVerification->infoBox($trxId);
        $this->checklistVerification->deleteStatusByTrx($trxId); //exit;
        //$checkDocExist = $fsProgramDB->getApplicantDocumentList($trxId);
        //$totalDocExist = count($checkDocExist);
        //var_dump($checkDocExist); //exit;

        //get applicant document status
        $docChecklist = $checklistVerificationDB->fnGetDocCheckList($info['ap_prog_scheme'], $info['appl_category'], $trxId);

        foreach($docChecklist as $dataDoc){

            $list = $checklistVerificationDB->universalQueryAllTable($dataDoc['table_name'], '*', $dataDoc['table_id_column'].' = '.$trxId, $dataDoc['table_join'], $dataDoc['join_column1'], $dataDoc['join_column2']);
            //var_dump($list);

            if($list){
                foreach($list as $listLoop){
                    $adsCheck = $checklistVerificationDB->fnGetChecklistStatusByTxnId($trxId, $dataDoc['dcl_Id'], $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);
                    $adId = Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($trxId, $dataDoc['dcl_Id'], 'ad_id', $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);

                    if (!$adsCheck){ //update
                        $data = array(
                            'ads_txn_id'=>$trxId,
                            'ads_dcl_id'=>$dataDoc['dcl_Id'],
                            'ads_ad_id'=>$adId,
                            //'ads_appl_id'=>'',
                            //'ads_confirm'=>$check,
                            'ads_section_id'=>$dataDoc['dcl_sectionid'],
                            'ads_table_name'=>$dataDoc['table_name'],
                            'ads_table_id'=>$listLoop[$dataDoc['table_primary_id']],
                            'ads_status'=>0,
                            'ads_comment'=>'',
                            'ads_createBy'=>$getUserIdentity->id,
                            'ads_createDate'=>date('Y-m-d H:i:s')
                        );
                        $checklistVerificationDB->insert($data);
                    }
                }
            }
        }
        
        //loop by transaction start
        $reciptId = $this->checklistVerification->getReceiptId($trxId);
        //var_dump($reciptId); exit;
        if ($reciptId){
            foreach ($reciptId as $reciptIdLoop){
                //invoice
                $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
                $invoice_list = $invoiceMainDb->getInvoiceFromReceipt($reciptIdLoop['rcp_id']);

                $inv = array();
                $total_payment = 0.00;
                $total_balance = 0.00;
                //var_dump($invoice_list); exit;
                if($invoice_list){
                    foreach ($invoice_list as $invoice){
                        $iv = array(
                            'invoice_no' => $invoice['bill_number'],
                            'amount' => 0,
                            'currency' => $invoice['cur_symbol_prefix']
                        );
                        //var_dump($invoice['receipt_invoice']); exit;
                        foreach ($invoice['receipt_invoice'] as $ri){
                            $iv['amount'] += $ri['rcp_inv_amount'];
                            $total_payment += $ri['rcp_inv_amount'];
                            $total_balance += $ri['balance'];

                            $feeID = $ri['fi_id'];
                            $receipt_no = $ri['rcp_inv_rcp_no'];
//                           $bill_paid = $invoice['bill_amount']; // updated by su 1/9/2015
                            $bill_paid = $ri['paid'];
                            $currencyPaid = $ri['cur_symbol_prefix'];
                            $currencyInv = $invoice['cur_symbol_prefix'];

                            /*
                            * get fee structure program item
                            * update docuemnt checklist status
                            */
                            $fsProgram = $fsProgramDB->getApplicantDocumentType($trxId,$feeID);
                            $newArray1 = array();	
                            $ads_id = $fsProgram['ads_id'];
                            if(isset($ads_id)){
                                $arrayAds [] = array(
                                    'id'=>$ads_id,
                                    'total'	=>$bill_paid,
                                    'balance'=>$total_balance,
                                    'cur'=>$currencyInv,
                                    'receipt_no'=>$receipt_no
                                );
                            }
                            array_push($newArray1,$arrayAds);
                        }

                        $inv[] = $iv;
                    }
                    
//                    echo "<pre>";
//                    print_r($arrayAds);
//                    exit;
                    //update checklist
                    $this->updateChecklist($arrayAds);
                }
            }
        }//loop by transaction end
        
        $this->_helper->flashMessenger->addMessage(array('success' => "Payment status updated!"));
        $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'checklist-verification', 'action'=>'checklist-verification', 'id'=>$trxId, 'programScheme'=>$info['ap_prog_scheme'], 'stdCtgy'=>$info['appl_category']),'default',true);
        $this->_redirect($this->view->backURL);
        
        exit;
    }
    
    public function checkPaymentStatusAction($trxId){
        //$trxId = $this->_getParam('trxId', null);
        $getUserIdentity = $this->auth->getIdentity();
        
        $fsProgramDB = new Application_Model_DbTable_ApplicantDocumentStatus();
        $checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
        
        $info = $this->checklistVerification->infoBox($trxId);
        $this->checklistVerification->deleteStatusByTrx($trxId); //exit;
        //$checkDocExist = $fsProgramDB->getApplicantDocumentList($trxId);
        //$totalDocExist = count($checkDocExist);
        //var_dump($checkDocExist); //exit;

        //get applicant document status
        $docChecklist = $checklistVerificationDB->fnGetDocCheckList($info['ap_prog_scheme'], $info['appl_category'], $trxId);

        foreach($docChecklist as $dataDoc){

            $list = $checklistVerificationDB->universalQueryAllTable($dataDoc['table_name'], '*', $dataDoc['table_id_column'].' = '.$trxId, $dataDoc['table_join'], $dataDoc['join_column1'], $dataDoc['join_column2']);
            //var_dump($list);

            if($list){
                foreach($list as $listLoop){
                    $adsCheck = $checklistVerificationDB->fnGetChecklistStatusByTxnId($trxId, $dataDoc['dcl_Id'], $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);
                    $adId = Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($trxId, $dataDoc['dcl_Id'], 'ad_id', $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);

                    if (!$adsCheck){ //update
                        $data = array(
                            'ads_txn_id'=>$trxId,
                            'ads_dcl_id'=>$dataDoc['dcl_Id'],
                            'ads_ad_id'=>$adId,
                            //'ads_appl_id'=>'',
                            //'ads_confirm'=>$check,
                            'ads_section_id'=>$dataDoc['dcl_sectionid'],
                            'ads_table_name'=>$dataDoc['table_name'],
                            'ads_table_id'=>$listLoop[$dataDoc['table_primary_id']],
                            'ads_status'=>0,
                            'ads_comment'=>'',
                            //'ads_createBy'=>$getUserIdentity->id,
                            //'ads_createDate'=>date('Y-m-d H:i:s')
                        );
                        $checklistVerificationDB->insert($data);
                    }
                }
            }
        }
        
        //loop by transaction start
        $reciptId = $this->checklistVerification->getReceiptId($trxId);
        //var_dump($reciptId); exit;
        if ($reciptId){
            foreach ($reciptId as $reciptIdLoop){
                //invoice
                $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
                $invoice_list = $invoiceMainDb->getInvoiceFromReceipt($reciptIdLoop['rcp_id']);

                $inv = array();
                $total_payment = 0.00;
                $total_balance = 0.00;
                //var_dump($invoice_list); exit;
                if($invoice_list){
                    foreach ($invoice_list as $invoice){
                        $iv = array(
                            'invoice_no' => $invoice['bill_number'],
                            'amount' => 0,
                            'currency' => $invoice['cur_symbol_prefix']
                        );
                        
                        //var_dump($invoice['receipt_invoice']); exit;
                        foreach ($invoice['receipt_invoice'] as $ri){
                            $iv['amount'] += $ri['rcp_inv_amount'];
                            $total_payment += $ri['rcp_inv_amount'];
                            $total_balance += $ri['balance'];

                            $feeID = $ri['fi_id'];
                            $receipt_no = $ri['rcp_inv_rcp_no'];
//                            $bill_paid = $invoice['bill_amount']; // updated by su 1/9/2015
                            $bill_paid = $ri['paid'];
                            $currencyPaid = $ri['cur_symbol_prefix'];
                            $currencyInv = $invoice['cur_symbol_prefix'];

                            /*
                            * get fee structure program item
                            * update docuemnt checklist status
                            */
                            $fsProgram = $fsProgramDB->getApplicantDocumentType($trxId,$feeID);
                            $newArray1 = array();	
                            $ads_id = $fsProgram['ads_id'];
                            if(isset($ads_id)){
                                $arrayAds [] = array(
                                    'id'=>$ads_id,
                                    'total'	=>$bill_paid,
                                    'balance'=>$total_balance,
                                    'cur'=>$currencyInv,
                                    'receipt_no'=>$receipt_no,
                                    'date'=>$reciptIdLoop['rcp_receive_date'],
                                    'creator'=>$reciptIdLoop['rcp_create_by']==0 ? 1:$reciptIdLoop['rcp_create_by']
                                );
                            }
                            array_push($newArray1,$arrayAds);
                        }

                        $inv[] = $iv;
                    }
                    
//                    echo "<pre>";
//                    print_r($arrayAds);
//                    exit;
                    //update checklist
                    $this->updateChecklist2($arrayAds);
                }
            }
        }//loop by transaction end
        
        //$this->_helper->flashMessenger->addMessage(array('success' => "Payment status updated!"));
        //$this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'checklist-verification', 'action'=>'checklist-verification', 'id'=>$trxId, 'programScheme'=>$info['ap_prog_scheme'], 'stdCtgy'=>$info['appl_category']),'default',true);
        //$this->_redirect($this->view->backURL);
        
        //exit;
        return true;
    }
    
    private function updateChecklist(&$arrayAds){
        //var_dump($arrayAds); exit;
        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        $tmp = array();
        $n=0;
        foreach($arrayAds as $arg){
            $tmp[$arg['id']]['tot'][$n] = $arg['total'];
            $tmp[$arg['id']]['bal'][$n] = $arg['balance'];
            $tmp[$arg['id']]['cur'] = $arg['cur'];
            $tmp[$arg['id']]['receipt_no'] = $arg['receipt_no'];
            $n++;
        }
        //var_dump($tmp); exit;
        $output = array();
        $m=0;
        foreach($tmp as $type => $labels){
            $output[$m] = array(
                'id' => $type,
                'total' => number_format(array_sum($labels['tot']),2),
                'balance' => number_format(array_sum($labels['bal']),2),
                'cur'=>$labels['cur'],
                'receipt'=>$labels['receipt_no'],
            );
            $m++;
        }
        //var_dump($output); exit;
        //update applicant document type checklist	
        foreach($output as $out){
            if($out['balance'] <= '0'){
                $docStatus = array(
                    'ads_status'=>4,//completed
                    'ads_comment'=> $out['receipt'].", ".$out['cur']."".$out['total'] ,
                    'ads_modBy'=>$getUserIdentity->id,
                    'ads_modDate'=>date('Y-m-d H:i:s'),
                );
                //var_dump($docStatus); exit;
                $appDocStatusDB = new Application_Model_DbTable_ApplicantDocumentStatus();
                $appDocStatusDB->update($docStatus, array('ads_id =?'=>$out['id']) );
            }
        }
    }
    
    private function updateChecklist2(&$arrayAds){
        //var_dump($arrayAds); exit;
        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        $tmp = array();
        $n=0;
        foreach($arrayAds as $arg){
            $tmp[$arg['id']]['tot'][$n] = $arg['total'];
            $tmp[$arg['id']]['bal'][$n] = $arg['balance'];
            $tmp[$arg['id']]['cur'] = $arg['cur'];
            $tmp[$arg['id']]['receipt_no'] = $arg['receipt_no'];
            $tmp[$arg['id']]['date'] = $arg['date'];
            $tmp[$arg['id']]['creator'] = $arg['creator'];
            $n++;
        }
        //var_dump($tmp); exit;
        $output = array();
        $m=0;
        foreach($tmp as $type => $labels){
            $output[$m] = array(
                'id' => $type,
                'total' => number_format(array_sum($labels['tot']),2),
                'balance' => number_format(array_sum($labels['bal']),2),
                'cur'=>$labels['cur'],
                'receipt'=>$labels['receipt_no'],
                'date'=>$labels['date'],
                'creator'=>$labels['creator']
            );
            $m++;
        }
        //var_dump($output); exit;
        //update applicant document type checklist	
        foreach($output as $out){
            if($out['balance'] <= 0){
                $docStatus = array(
                    'ads_status'=>4,//completed
                    'ads_comment'=> $out['receipt'].", ".$out['cur']."".$out['total'] ,
                    'ads_modBy'=>$out['creator'],
                    'ads_modDate'=>$out['date'],
                );
                //var_dump($docStatus); exit;
                $appDocStatusDB = new Application_Model_DbTable_ApplicantDocumentStatus();
                $appDocStatusDB->update($docStatus, array('ads_id =?'=>$out['id']) );
            }
        }
    }
    
    /*not use anymore - tp cater migration only */
    public function updatePaymentAction(){
        $this->view->title =  $this->view->translate("Update Payment Status");
        
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        if ($this->getRequest()->isPost()) {
            //get migrate applicant
            $migrateAppl = $this->checklistVerification->getMigrateApplicant();
            
            if ($migrateAppl){
                
                foreach ($migrateAppl as $migrateApplLoop){
                    if ($migrateApplLoop['at_status']!=591){
                        $trxId = $migrateApplLoop['at_trans_id'];
                        $getUserIdentity = $this->auth->getIdentity();

                        $fsProgramDB = new Application_Model_DbTable_ApplicantDocumentStatus();
                        $checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();

                        $info = $this->checklistVerification->infoBox($trxId);

                        $this->checklistVerification->deleteStatusByTrx($trxId);
                        //$checkDocExist = $fsProgramDB->getApplicantDocumentList($trxId);
                        //$totalDocExist = count($checkDocExist);

                        if ($info){
                            //get applicant document status
                            $docChecklist = $checklistVerificationDB->fnGetDocCheckList($info['ap_prog_scheme'], $info['appl_category'], $trxId);

                            foreach($docChecklist as $dataDoc){

                                $list = $checklistVerificationDB->universalQueryAllTable($dataDoc['table_name'], '*', $dataDoc['table_id_column'].' = '.$trxId, $dataDoc['table_join'], $dataDoc['join_column1'], $dataDoc['join_column2']);
                                //var_dump($list);

                                if($list){
                                    foreach($list as $listLoop){
                                        $adsCheck = $checklistVerificationDB->fnGetChecklistStatusByTxnId($trxId, $dataDoc['dcl_Id'], $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);
                                        $adId = Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($trxId, $dataDoc['dcl_Id'], 'ad_id', $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);

                                        if (!$adsCheck){ //update
                                            $data = array(
                                                'ads_txn_id'=>$trxId,
                                                'ads_dcl_id'=>$dataDoc['dcl_Id'],
                                                'ads_ad_id'=>$adId,
                                                //'ads_appl_id'=>'',
                                                //'ads_confirm'=>$check,
                                                'ads_section_id'=>$dataDoc['dcl_sectionid'],
                                                'ads_table_name'=>$dataDoc['table_name'],
                                                'ads_table_id'=>$listLoop[$dataDoc['table_primary_id']],
                                                'ads_status'=>0,
                                                'ads_comment'=>'',
                                                'ads_createBy'=>$getUserIdentity->id,
                                                'ads_createDate'=>date('Y-m-d H:i:s')
                                            );
                                            $checklistVerificationDB->insert($data);
                                        }
                                    }
                                }
                            }

                            //loop by transaction start
                            $reciptId = $this->checklistVerification->getReceiptId($trxId);
                            //var_dump($reciptId); exit;
                            if ($reciptId){
                                foreach ($reciptId as $reciptIdLoop){
                                    //invoice
                                    $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
                                    $invoice_list = $invoiceMainDb->getInvoiceFromReceipt($reciptIdLoop['rcp_id']);

                                    $inv = array();
                                    $total_payment = 0.00;
                                    $total_balance = 0.00;
                                    //var_dump($invoice_list); exit;
                                    if($invoice_list){
                                        foreach ($invoice_list as $invoice){
                                            $iv = array(
                                                'invoice_no' => $invoice['bill_number'],
                                                'amount' => 0,
                                                'currency' => $invoice['cur_symbol_prefix']
                                            );
                                            //var_dump($invoice['receipt_invoice']); exit;
                                            foreach ($invoice['receipt_invoice'] as $ri){
                                                $iv['amount'] += $ri['rcp_inv_amount'];
                                                $total_payment += $ri['rcp_inv_amount'];
                                                $total_balance += $ri['balance'];

                                                $feeID = $ri['fi_id'];
                                                $receipt_no = $ri['rcp_inv_rcp_no'];
                                                $bill_paid = $ri['rcp_inv_amount'];
                                                $currencyPaid = $ri['cur_symbol_prefix'];

                                                /*
                                                * get fee structure program item
                                                * update docuemnt checklist status
                                                */
                                                $fsProgram = $fsProgramDB->getApplicantDocumentType($trxId,$feeID);
                                                $newArray1 = array();	
                                                $ads_id = $fsProgram['ads_id'];
                                                if(isset($ads_id)){
                                                    $arrayAds [] = array(
                                                        'id'=>$ads_id,
                                                        'total'	=>$bill_paid,
                                                        'balance'=>$total_balance,
                                                        'cur'=>$currencyPaid,
                                                        'receipt_no'=>$receipt_no
                                                    );
                                                }
                                                array_push($newArray1,$arrayAds);
                                            }

                                            $inv[] = $iv;
                                        }

                                        //update checklist
                                        $this->updateChecklist($arrayAds);
                                    }
                                }
                            }//loop by transaction end
                        }
                    }
                }
            }
            
            $this->_helper->flashMessenger->addMessage(array('success' => "Payment status updated!"));
            $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'checklist-verification', 'action'=>'update-payment'),'default',true);
            $this->_redirect($this->view->backURL);
        }
    }
    
    public function updatePaymentStatus($trxId){
        $getUserIdentity = $this->auth->getIdentity();
        
        $fsProgramDB = new Application_Model_DbTable_ApplicantDocumentStatus();
        $checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
        
        $info = $this->checklistVerification->infoBox($trxId);
        $this->checklistVerification->deleteStatusByTrx($trxId); //exit;
        //$checkDocExist = $fsProgramDB->getApplicantDocumentList($trxId);
        //$totalDocExist = count($checkDocExist);
        //var_dump($checkDocExist); //exit;

        //get applicant document status
        $docChecklist = $checklistVerificationDB->fnGetDocCheckList($info['ap_prog_scheme'], $info['appl_category'], $trxId);

        foreach($docChecklist as $dataDoc){

            $list = $checklistVerificationDB->universalQueryAllTable($dataDoc['table_name'], '*', $dataDoc['table_id_column'].' = '.$trxId, $dataDoc['table_join'], $dataDoc['join_column1'], $dataDoc['join_column2']);
            //var_dump($list);

            if($list){
                foreach($list as $listLoop){
                    $adsCheck = $checklistVerificationDB->fnGetChecklistStatusByTxnId($trxId, $dataDoc['dcl_Id'], $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);
                    $adId = Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($trxId, $dataDoc['dcl_Id'], 'ad_id', $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);

                    if (!$adsCheck){ //update
                        $data = array(
                            'ads_txn_id'=>$trxId,
                            'ads_dcl_id'=>$dataDoc['dcl_Id'],
                            'ads_ad_id'=>$adId,
                            //'ads_appl_id'=>'',
                            //'ads_confirm'=>$check,
                            'ads_section_id'=>$dataDoc['dcl_sectionid'],
                            'ads_table_name'=>$dataDoc['table_name'],
                            'ads_table_id'=>$listLoop[$dataDoc['table_primary_id']],
                            'ads_status'=>0,
                            'ads_comment'=>'',
                            'ads_createBy'=>$getUserIdentity->id,
                            'ads_createDate'=>date('Y-m-d H:i:s')
                        );
                        $checklistVerificationDB->insert($data);
                    }
                }
            }
        }
        
        //loop by transaction start
        $reciptId = $this->checklistVerification->getReceiptId($trxId);
        //var_dump($reciptId); exit;
        if ($reciptId){
            foreach ($reciptId as $reciptIdLoop){
                //invoice
                $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
                $invoice_list = $invoiceMainDb->getInvoiceFromReceipt($reciptIdLoop['rcp_id']);

                $inv = array();
                $total_payment = 0.00;
                $total_balance = 0.00;
                //var_dump($invoice_list); exit;
                if($invoice_list){
                    foreach ($invoice_list as $invoice){
                        $iv = array(
                            'invoice_no' => $invoice['bill_number'],
                            'amount' => 0,
                            'currency' => $invoice['cur_symbol_prefix']
                        );
                        //var_dump($invoice['receipt_invoice']); exit;
                        foreach ($invoice['receipt_invoice'] as $ri){
                            $iv['amount'] += $ri['rcp_inv_amount'];
                            $total_payment += $ri['rcp_inv_amount'];
                            $total_balance += $ri['balance'];

                            $feeID = $ri['fi_id'];
                            $receipt_no = $ri['rcp_inv_rcp_no'];
                            $bill_paid = $ri['rcp_inv_amount'];
                            $currencyPaid = $ri['cur_symbol_prefix'];

                            /*
                            * get fee structure program item
                            * update docuemnt checklist status
                            */
                            $fsProgram = $fsProgramDB->getApplicantDocumentType($trxId,$feeID);
                            $newArray1 = array();	
                            $ads_id = $fsProgram['ads_id'];
                            if(isset($ads_id)){
                                $arrayAds [] = array(
                                    'id'=>$ads_id,
                                    'total'	=>$bill_paid,
                                    'balance'=>$total_balance,
                                    'cur'=>$currencyPaid,
                                    'receipt_no'=>$receipt_no
                                );
                            }
                            array_push($newArray1,$arrayAds);
                        }

                        $inv[] = $iv;
                    }
                    
                    //update checklist
                    $this->updateChecklist($arrayAds);
                }
            }
        }//loop by transaction end
        
        return true;
    }
    
    public function updateWaivedAction(){
        $trxId = $this->_getParam('trxId', null);
        $getUserIdentity = $this->auth->getIdentity();
        
        $checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();
        
        $info = $this->checklistVerification->infoBox($trxId);
        $this->checklistVerification->deleteStatusByTrx($trxId); //exit;

        //get applicant document status
        $docChecklist = $checklistVerificationDB->fnGetDocCheckList($info['ap_prog_scheme'], $info['appl_category'], $trxId);

        foreach($docChecklist as $dataDoc){

            $list = $checklistVerificationDB->universalQueryAllTable($dataDoc['table_name'], '*', $dataDoc['table_id_column'].' = '.$trxId, $dataDoc['table_join'], $dataDoc['join_column1'], $dataDoc['join_column2']);
            //var_dump($list);

            if($list){
                foreach($list as $listLoop){
                    $adsCheck = $checklistVerificationDB->fnGetChecklistStatusByTxnId($trxId, $dataDoc['dcl_Id'], $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);
                    $adId = Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($trxId, $dataDoc['dcl_Id'], 'ad_id', $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);

                    if (!$adsCheck){ //update
                        $data = array(
                            'ads_txn_id'=>$trxId,
                            'ads_dcl_id'=>$dataDoc['dcl_Id'],
                            'ads_ad_id'=>$adId,
                            //'ads_appl_id'=>'',
                            //'ads_confirm'=>$check,
                            'ads_section_id'=>$dataDoc['dcl_sectionid'],
                            'ads_table_name'=>$dataDoc['table_name'],
                            'ads_table_id'=>$listLoop[$dataDoc['table_primary_id']],
                            'ads_status'=>0,
                            'ads_comment'=>'',
                            'ads_createBy'=>$getUserIdentity->id,
                            'ads_createDate'=>date('Y-m-d H:i:s')
                        );
                        $checklistVerificationDB->insert($data);
                    }
                }
            }
        }
        
        //get proforma
        $proformas = $this->checklistVerification->getWaivedProforma($trxId);
        
        if ($proformas){
            foreach ($proformas as $proforma){
                $db = Zend_Db_Table::getDefaultAdapter();

                $select = $db->select()
                    ->from(array('a'=>'proforma_invoice_main'))
                    ->join(array('b'=>'proforma_invoice_detail'), 'b.proforma_invoice_main_id = a.id', array('*'))
                    ->where('a.id =?',$proforma['id']);

                $txnDataApp = $db->fetchAll($select);

                $trans_id = $txnDataApp[0]['trans_id'];

                $paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
                $paymentClass->checkingApplicantDocumentChecklist($trans_id);

                foreach($txnDataApp as $txnData){

                    $feeID = $txnData['fi_id'];

                    $fsProgramDB = new Application_Model_DbTable_ApplicantDocumentStatus();
                    $fsProgram = $fsProgramDB->getApplicantDocumentType($trans_id,$feeID);
                    $ads_id = $fsProgram['ads_id'];

                    if($ads_id){
                        $newStatus = '';
                        if($proforma['status'] == 'E'){
                            $newStatus = 'Exempted';
                        }else if($proforma['status'] == 'W'){
                            $newStatus = 'Waived';
                        }else if($proforma['status'] == 'S'){
                            $newStatus = 'Sponsorship';
                        }

                        $docStatus = array(
                            'ads_status'=>5,//not applicable
                            'ads_comment'=> $newStatus.', '.$proforma['remarks'] ,
                            'ads_modBy'=>$getUserIdentity->id,
                            'ads_modDate'=>date('Y-m-d H:i:s'),
                        );

                        $appDocStatusDB = new Application_Model_DbTable_ApplicantDocumentStatus();
                        $appDocStatusDB->update($docStatus, array('ads_id =?'=>$ads_id) );
                    }
                }
            }
        }
        
        $this->_helper->flashMessenger->addMessage(array('success' => "Waived status updated!"));
        $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'checklist-verification', 'action'=>'checklist-verification', 'id'=>$trxId, 'programScheme'=>$info['ap_prog_scheme'], 'stdCtgy'=>$info['appl_category']),'default',true);
        $this->_redirect($this->view->backURL);
        
        exit;
    }
}
?>