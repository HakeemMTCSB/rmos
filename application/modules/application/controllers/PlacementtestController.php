<?php
class Application_PlacementtestController extends Base_Base { //Controller for the User Module
	private $_gobjlog;
	private $lobjdeftype;
	public function init() { //initialization function
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->lobjdeftype = new App_Model_Definitiontype();

	}

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$lobjplacementtest = new Application_Model_DbTable_Placementtest(); //user model object
		$larrresult = $lobjplacementtest->fngetPlacementtestDetails (); //get user details
		$lobjProgramList = $lobjplacementtest->fnGetProgramMaterList();
		$lobjform->field5->addMultiOptions($lobjProgramList);
		$larrnewresult = array();
		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->placementpaginatorresult);

		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(count($larrresult) >0){
			$larrnewresults['test'] = array();
			$larrnewresultbranch = array();
			$s = 0;
			for($i=0;$i<count($larrresult);$i++){
				if(!in_array($larrresult[$i]['IdPlacementTest'],$larrnewresults['test'])){
					$larrnewresults['test'][] = $larrresult[$i]['IdPlacementTest'];
					$larrnewresult[$s]['IdPlacementTest'] = $larrresult[$i]['IdPlacementTest'];
					$larrnewresult[$s]['PlacementTestName'] = $larrresult[$i]['PlacementTestName'];
					$larrnewresult[$s]['PlacementTestDate'] = $larrresult[$i]['PlacementTestDate'];
					$larrnewresult[$s]['ProgramName'] = $larrresult[$i]['ProgramName'];
					$larrnewresult[$s]['PlacementTestTime'] = $larrresult[$i]['PlacementTestTime'];
					$s++;
				}
				$larrnewresultbranch[$larrresult[$i]['IdPlacementTest']][] = $larrresult[$i]['ProgramName'];
			}
			$this->view->resultbranch = $larrnewresultbranch;
		}
		if(isset($this->gobjsessionsis->placementpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->placementpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrnewresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $lobjplacementtest->fnSearchPlacementTest( $lobjform->getValues () ); //searching the values for the user

				if(count($larrresult) >0){
					$larrnewresults['test'] = array();
					$larrnewresultbranch = array();
					$s = 0;
					for($i=0;$i<count($larrresult);$i++){
						if(!in_array($larrresult[$i]['IdPlacementTest'],$larrnewresults['test'])){
							$larrnewresults['test'][] = $larrresult[$i]['IdPlacementTest'];
							$larrnewresult1[$s]['IdPlacementTest'] = $larrresult[$i]['IdPlacementTest'];
							$larrnewresult1[$s]['PlacementTestName'] = $larrresult[$i]['PlacementTestName'];
							$larrnewresult1[$s]['PlacementTestDate'] = $larrresult[$i]['PlacementTestDate'];
							$larrnewresult1[$s]['ProgramName'] = $larrresult[$i]['ProgramName'];
							$larrnewresult1[$s]['PlacementTestTime'] = $larrresult[$i]['PlacementTestTime'];
							$s++;
						}
						$larrnewresultbranch[$larrresult[$i]['IdPlacementTest']][] = $larrresult[$i]['ProgramName'];
					}
					$larrresult = $larrnewresult1;
					$this->view->resultbranch = $larrnewresultbranch;
				}
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->placementpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {


			//$this->_redirect($this->view->url(array('module'=>'application' ,'controller'=>'placementtest', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/application/placementtest/index');
		}

	}

	public function newplacementtestAction()
	{ 
		//Action for creating the new user
		$lobjplacementtestForm = new Application_Form_Placementtest(); //intialize user lobjuserForm
		$this->view->lobjplacementtestForm = $lobjplacementtestForm; //send the lobjuserForm object to the view
		$this->lobjuser = new GeneralSetup_Model_DbTable_User(); //user model object
		$lobjplacementtest = new Application_Model_DbTable_Placementtest(); //intialize user Model
		$this->lobjintake = new GeneralSetup_Model_DbTable_Intake();
		$larrintakelist = $this->lobjintake->fngetIntakes();
		$lobjplacementtestForm->Intake->addMultiOptions($larrintakelist);
		$lobjplacementtestForm->PlacementTestType->addMultiOptions(array(0=>"Normal",1=>"Interview",2=>"Placement Test"));
		$lobjplacementtestForm->MarkingType->addMultiOptions(array(0=>"Marks",1=>"Pass/Fail"));
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjplacementtestForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjplacementtestForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

		$lobjProgramMaterList = $lobjplacementtest->fnGetProgramMaterList();
		$lobjplacementtestForm->IdProgram->addMultiOptions($lobjProgramMaterList);

		$lobjcountry = $this->lobjuser->fnGetCountryList();
		$lobjplacementtestForm->idCountry->addMultiOptions($lobjcountry);

		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}";
		$lobjplacementtestForm->PlacementTestDate->setAttrib('constraints', "$dateofbirth");
		$lobjplacementtestForm->CopyPlacementTestDate->setAttrib('constraints', "$dateofbirth");

		$BranchList=$lobjplacementtest->fnGetBranchList();
		$lobjplacementtestForm->IdCollege->addMultiOptions($BranchList);

		$ExamVenue=$lobjplacementtest->fnGetIdVenu();
		$lobjplacementtestForm->IdVenue->addMultiOptions($ExamVenue);

		$semester = new GeneralSetup_Model_DbTable_Semester();
		$semarray = $semester->fngetlandscapeSemesterDetails();
		//		echo "<pre>";
		//		print_r($semarray);die;
		foreach($semarray as $semdata) 
		{
			$lobjplacementtestForm->Year->addMultiOption($semdata['SemesterMainName'],$semdata['SemesterMainName']);
		}

		$applicationtype = $this->lobjdeftype->fnGetDefinations('Application Type');
		foreach($applicationtype as $applicationtype) 
		{
			$lobjplacementtestForm->Apptype->addMultiOption($applicationtype['idDefinition'],$applicationtype['DefinitionDesc']);
		}


		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' ))
	 	{

			$larrformData = $this->_request->getPost ();
                        
			$testTimeValue = $larrformData['PlacementTestTime'];
			$larrreqhrs = explode('T',$larrformData['PlacementTestTime']);
			$larrformData['PlacementTestTime'] = $larrreqhrs[1];
			unset ( $larrformData ['Save'] );
	
			if ($lobjplacementtestForm->isValid ( $larrformData )) 
			{
				//starts checking for placement
				$checkIntake = $lobjplacementtest->fnGetPlacementDetails('pm.Intake = \''.$larrformData['Intake'].'\'');
				if ( !empty($checkIntake) )
				{
					foreach ( $checkIntake as $checkRow )
					{
						//not this one
						if ( $checkRow['IdPlacementTest'] != $IdPlacementTest )
						//if ( $checkRow['IdPlacementTest'] != 1 ) // for test
						{					
							//get program details for this placement test
							$progs = $lobjplacementtest->fnGetPlacementProgramDetails($checkRow['IdPlacementTest']);
							if ( !empty($progs) )
							{
								foreach ( $progs as $prog )
								{
									if ( in_array($prog['IdProgram'], $larrformData['IdProgramNamegrid']) )
									{
										$this->view->errorMsg1 = $this->view->translate('Placement Test with the same program(s) already exists for this Intake.');
										return false;
									}
								}
							}
						}
					}
				}
				
				// CHECK FOR SAME programtest name, time and date selected
				$this->view->errorMsg1 = '';
				$this->view->errorMsg2 = '';
				$testName = $larrformData['PlacementTestName'];
				$testTime = $larrreqhrs[1];
				$testDate = $larrformData['PlacementTestDate'];
				$testprogramIDs = $larrformData['IdProgramScheme']; // user selected programs
				//asd($testprogramIDs,false);

				$condition_forname = " PlacementTestName =  '".$testName."' ";

				
				$testResult = $lobjplacementtest->fnGetPlacementDetails($condition_forname);
				
				if (count($testResult)>0) 
				{
					$this->view->errorMsg1 = $this->view->translate('The Placement Test Name already exists. Please try different.');
					$this->view->lobjplacementtestForm = $lobjplacementtestForm;
				}
				else
				{
					$condition_fordatetime = " PlacementTestDate =  '".$testDate."' AND   PlacementTestTime =  '".$testTime."' ";
					$testResult_ForTD = $lobjplacementtest->fnGetPlacementDetails($condition_fordatetime);
					
					if (count($testResult_ForTD)>0)
					{
						$myArr = array();
						$i=0;
						
						foreach($testResult_ForTD as $values) 
						{
							$obtainedPID = $values['IdPlacementTest'];
							$testResultProgram = $lobjplacementtest->fnGetPlacementProgramDetails($obtainedPID);
							
							foreach ($testResultProgram as $values)
							{
								$myArr[$i] = $values['IdProgramScheme'];
								$i++;
							}
						}
						
					//asd($myArr,false);
					 $resultIntersect = array_intersect($testprogramIDs, $myArr);
					//asd($resultIntersect);
					$totalArrays = count($resultIntersect);
					
					if($totalArrays>0)
					{
						$this->view->errorMsg1 = '';
						$this->view->errorMsg2 = $this->view->translate('The selected Program for the DateTime already exists. Please try different.');
						$this->view->lobjplacementtestForm = $lobjplacementtestForm;
						$this->view->lobjplacementtestForm->PlacementTestTime->setValue ( $testTimeValue );
					}
					else
					{
						$result = $lobjplacementtest->fnaddplacementtest($larrformData); //instance for adding the lobjuserForm values to DB
						$resultDetail = $lobjplacementtest->fnaddplacementtestBranchDtls($larrformData,$result);
						$resultplacementtestComponentDetail = $lobjplacementtest->fnaddNewplacementtestComponentDtls($larrformData,$result);
						$this->view->errorMsg1 = '';
						$this->view->errorMsg2 = '';
					}

				}
				else
				{
					$result = $lobjplacementtest->fnaddplacementtest($larrformData); //instance for adding the lobjuserForm values to DB
					$resultDetail = $lobjplacementtest->fnaddplacementtestBranchDtls($larrformData,$result);
					$resultplacementtestComponentDetail = $lobjplacementtest->fnaddNewplacementtestComponentDtls($larrformData,$result);
					$this->view->errorMsg1 = '';
					$this->view->errorMsg2 = '';
				}
			}

			// ENDS CHECK FOR SAME programtest name, time and progrma selected
			

			//$maxid = $lobjplacementtest->fnGetmaxid();
			//				$year=explode("-",$larrformData['Year']);
			//				$yy=substr($year[1],2,5);
			//				$ApplicationType=substr($larrformData['Apptype'],0,1);
			//				$length = 5;
			//				for($i=0;$i<$larrformData['EnterNo'];$i++){
			//					@$num = $maxid['maxid']+$i;
			//				$FamulirNo =  $yy.$ApplicationType.str_pad($num, 5, "0", STR_PAD_LEFT);
			//				$password = $this->fnCreateRandPassword($length);
			//				 // $FamulirresultDetail = $lobjplacementtest->fnInsertintoplacementtestnopes($larrformData,$result,$FamulirNo,$password);
			//				}
			// Write Logs
			$priority=Zend_Log::INFO;
			$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
					'level' => $priority,
					'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					'time' => date ( 'Y-m-d H:i:s' ),
					'message' => 'New Placement Test Add',
					'Description' =>  Zend_Log::DEBUG,
					'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
			
			$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

			//$this->_redirect($this->view->url(array('module'=>'application' ,'controller'=>'placementtest', 'action'=>'index'),'default',true));
			if($this->view->errorMsg1=='' && $this->view->errorMsg2=='')
			{
				$this->_redirect( $this->baseUrl . '/application/placementtest/index');
			}
		}
	}

	}
	public function fnCreateRandPassword($length)
	{
		$chars = "234567890abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$i = 0;
		$password = "";
		while ($i <= $length)
		{
			@$password .= $chars{mt_rand(0,strlen($chars))};
			$i++;
		}
		return $password;

	}

	public function placementtestlistAction() {
		$lobjplacementtestForm = new Application_Form_Placementtest();
		$this->view->lobjplacementtestForm = $lobjplacementtestForm;
		$lobjplacementtest = new Application_Model_DbTable_Placementtest(); //intialize user Model
		$this->lobjuser = new GeneralSetup_Model_DbTable_User(); //user model object
		$lobjProgramMaterList = $lobjplacementtest->fnGetProgramMaterList();
		$lobjplacementtestForm->IdProgram->addMultiOptions($lobjProgramMaterList);
		$this->lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
		$lobjcountry = $this->lobjuser->fnGetCountryList();
		$lobjplacementtestForm->idCountry->addMultiOptions($lobjcountry);
		$this->lobjintake = new GeneralSetup_Model_DbTable_Intake();
		$larrintakelist = $this->lobjintake->fngetIntakes();
		$lobjplacementtestForm->Intake->addMultiOptions($larrintakelist);
		$lobjplacementtestForm->PlacementTestType->addMultiOptions(array(0=>"Normal",1=>"Interview",2=>"Placement Test"));
		$lobjplacementtestForm->MarkingType->addMultiOptions(array(0=>"Marks",1=>"Pass/Fail"));
		$IdPlacementTest = ( int ) $this->_getParam ( 'id' );
		$this->view->idplacementtest = $IdPlacementTest;
		$result = $lobjplacementtest->fnViewPlacementTest($IdPlacementTest);
		$this->view->programdetails = $lobjplacementtest->fngetPlacementTestProgramDetails($IdPlacementTest);
		//        echo "<pre>";
		//        var_dump($this->view->programdetails); exit;
		$this->view->resultPlacementComponent = $resultPlacementComponent = $lobjplacementtest->fnViewPlacementComponentDetails($IdPlacementTest);
		$SumComponentWeightage = 0;
		foreach($resultPlacementComponent as $resultPlacementComponentdtl){
			$SumComponentWeightage = $SumComponentWeightage + $resultPlacementComponentdtl['ComponentWeightage'];
		}
		$this->view->SumComponentWeightage =$SumComponentWeightage;
		foreach($result as $placementresult){
			$placementresult['PlacementTestTime'] = 'T'.$placementresult['PlacementTestTime'];
			$IdProgram[] = $placementresult['IdProgramScheme'];
		}
		$lobjstate = $this->lobjUser->fnGetStateListcountry($result[0]['idCountry']);
		//asd($result,false);
		$lobjplacementtestForm->idState->addMultiOptions($lobjstate);
		$lobjCommonModel = new App_Model_Common();
		$larrStateCityList = $lobjCommonModel->fnGetCityList($placementresult['idState']);
		$lobjplacementtestForm->City->addMultiOptions($larrStateCityList);
		$lobjplacementtestForm->populate($placementresult);
		$arrPhone = explode("-",$placementresult ['Phone']);
		$this->view->lobjplacementtestForm->countrycode->setValue ( $arrPhone [0] );
		$this->view->lobjplacementtestForm->statecode->setValue ( $arrPhone [1] );
		$this->view->lobjplacementtestForm->Phone1->setValue ( $arrPhone [2] );

		//$this->view->lobjplacementtestForm->IdProgram->setValue($IdProgram);

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjplacementtestForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjplacementtestForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$ExamVenue=$lobjplacementtest->fnGetIdVenu();
		$lobjplacementtestForm->IdVenue->addMultiOptions($ExamVenue);




		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
			$larrformData = $this->_request->getPost ();
			//            echo "<pre>";
			//            var_dump($larrformData);exit;
			$testTimeValue = $larrformData['PlacementTestTime'];
			$larrreqhrs = explode('T',$larrformData['PlacementTestTime']);
			$larrformData['PlacementTestTime'] = $larrreqhrs[1];
			unset ( $larrformData ['Save'] );

			if ($lobjplacementtestForm->isValid ( $larrformData )) 
			{
				//$IdPlacementTest = $larrformData['IdPlacementTest'];
				
							
				//starts checking for placement
				$checkIntake = $lobjplacementtest->fnGetPlacementDetails('pm.Intake = \''.$larrformData['Intake'].'\'');
				if ( !empty($checkIntake) )
				{
					foreach ( $checkIntake as $checkRow )
					{
						//not this one
						if ( $checkRow['IdPlacementTest'] != $IdPlacementTest )
						//if ( $checkRow['IdPlacementTest'] != 1 ) // for test
						{					
							//get program details for this placement test
							$progs = $lobjplacementtest->fnGetPlacementProgramDetails($checkRow['IdPlacementTest']);
							if ( !empty($progs) )
							{
								foreach ( $progs as $prog )
								{
									if ( in_array($prog['IdProgram'], $larrformData['IdProgramNamegrid']) )
									{
										$this->view->errorMsg1 = $this->view->translate('Placement Test with the same program(s) already exists for this Intake.');
										return false;
									}
								}
							}
						}
					}
				}

				// CHECK FOR SAME programtest name, time and date selected
				$testDateTime = FALSE;
				$this->view->errorMsg1 = '';
				$this->view->errorMsg2 = '';
				$testName = $larrformData['PlacementTestName'];
				$testTime = $larrreqhrs[1];
				$testDate = $larrformData['PlacementTestDate'];
				$testprogramIDs = $larrformData['IdScheme']; // user selected programs
				//asd($testprogramIDs,false);

				// CHECk if user changes the TESTNAME
				$originalTestName = $result[0]['PlacementTestName'];
				if($originalTestName!=$testName) {
					$condition_forname = " PlacementTestName =  '".$testName."' ";
					$testResult = $lobjplacementtest->fnGetPlacementDetails($condition_forname);
					if(count($testResult)>0) {
						$this->view->errorMsg1 = $this->view->translate('The Placement Test Name already exists. Please try different.');
						$this->view->lobjplacementtestForm = $lobjplacementtestForm;
						$testDateTime = FALSE;
					} else {
						$result = $lobjplacementtest->fnupdateplacementtest($IdPlacementTest,$larrformData);
						$testDateTime = TRUE;
					}
				} else {
					$result = $lobjplacementtest->fnupdateplacementtest($IdPlacementTest,$larrformData);
					$testDateTime = TRUE;
				}
				//CHECK ENDS
					

				//CHECK if user make changes in selecting program checkboxes
				$myArrAssoc = array();$j=0;
				$testAssociatedProgram = $lobjplacementtest->fnGetPlacementProgramDetails($IdPlacementTest);
				foreach ($testAssociatedProgram as $values)
				{
					$myArrAssoc[$j] = $values['IdProgram'];
					$j++;
				}
				$resultIntersectProgram = count(array_diff($testprogramIDs, $myArrAssoc)); // returns TRUE
				if($resultIntersectProgram=='0') {
					$testDateTime = FALSE;
					$this->view->errorMsg1 = '';
					$this->view->errorMsg2 = '';
				}
				//CHECK ENDS
				//asd($myArrAssoc,false);
				$resultDifference = array_diff($testprogramIDs, $myArrAssoc);
				//asd($resultDifference,false);


					
				//echo 'hhh'.$testDateTime; die;
				// CHECK FOR DUPLICATE PROGRAM ENTRY
				if($testDateTime) {
					$condition_fordatetime = " PlacementTestDate =  '".$testDate."' AND   PlacementTestTime =  '".$testTime."' ";
					$testResult_ForTD = $lobjplacementtest->fnGetPlacementDetails($condition_fordatetime);
					if(count($testResult_ForTD)>0)
					{
						$myArr = array();
						$i=0;
						foreach($testResult_ForTD as $values) {
							$obtainedPID = $values['IdPlacementTest'];
							$testResultProgram = $lobjplacementtest->fnGetPlacementProgramDetails($obtainedPID);
							foreach ($testResultProgram as $values)
							{
								$myArr[$i] = $values['IdProgram'];
								$i++;
							}
						}
						//asd($myArr,false);
						$resultIntersect = array_intersect($resultDifference, $myArr);
						//$resultIntersect22 = array_diff($testprogramIDs, $myArr);
						//asd($resultIntersect);
						$totalArrays = count($resultIntersect);
						if($totalArrays>0) {
							$this->view->errorMsg1 = '';
							$this->view->errorMsg2 = $this->view->translate('The selected Program for the DateTime already exists. Please try different.');
							$this->view->lobjplacementtestForm = $lobjplacementtestForm;
							$this->view->lobjplacementtestForm->PlacementTestTime->setValue ( $testTimeValue );
						} else {

							$lobjplacementtest->fndeleteplacementbranch($IdPlacementTest);
							$lobjplacementtest->fndeleteplacementtestComponent($IdPlacementTest);

							$resultDetail = $lobjplacementtest->fnaddplacementtestBranchDtls($larrformData,$IdPlacementTest);
							$resultplacementtestComponentDetail = $lobjplacementtest->fnaddplacementtestComponentDtls($larrformData,$IdPlacementTest);
							$this->view->errorMsg1 = '';
							$this->view->errorMsg2 = '';
						}

					} else {

						$lobjplacementtest->fndeleteplacementbranch($IdPlacementTest);
						$lobjplacementtest->fndeleteplacementtestComponent($IdPlacementTest);
							
						$resultDetail = $lobjplacementtest->fnaddplacementtestBranchDtls($larrformData,$IdPlacementTest);
						$resultplacementtestComponentDetail = $lobjplacementtest->fnaddplacementtestComponentDtls($larrformData,$IdPlacementTest);
						$this->view->errorMsg1 = '';
						$this->view->errorMsg2 = '';
					}

				}//CHECK ENDS
				else {

					$lobjplacementtest->fndeleteplacementbranch($IdPlacementTest);
					$lobjplacementtest->fndeleteplacementtestComponent($IdPlacementTest);

					$resultDetail = $lobjplacementtest->fnaddplacementtestBranchDtls($larrformData,$IdPlacementTest);
					$resultplacementtestComponentDetail = $lobjplacementtest->fnaddplacementtestComponentDtls($larrformData,$IdPlacementTest);
					$this->view->errorMsg1 = '';
					$this->view->errorMsg2 = '';
				}
					
			}

			// Write Logs
			$priority=Zend_Log::INFO;
			$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
					'level' => $priority,
					'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					'time' => date ( 'Y-m-d H:i:s' ),
					'message' => 'Placement Test Edit Id=' . $IdPlacementTest,
					'Description' =>  Zend_Log::DEBUG,
					'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
			$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
			//$this->_redirect($this->view->url(array('module'=>'application' ,'controller'=>'placementtest', 'action'=>'index'),'default',true));
			//$this->_redirect( $this->baseUrl . '/application/placementtest/index');
			if($this->view->errorMsg1=='' && $this->view->errorMsg2=='') {
				$this->_redirect( $this->baseUrl . '/application/placementtest/index');
			}
		}
	}


	public function deleteplacementcomponentAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Po details Id
		$idplacementtestcomponent = $this->_getParam('idplacementtestcomponent');
		$lobjplacementtest = new Application_Model_DbTable_Placementtest(); //user model object

		$larrDelete = $lobjplacementtest->fnUpdateTempprogramentrydetails($idplacementtestcomponent);
		echo "1";
	}


	public function copyplacementtestAction(){
		$newPlacementTestData = array();
		$auth = Zend_Auth::getInstance();
		$placementtestcomponent = array();
		if($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			$lobjplacementtest = new Application_Model_DbTable_Placementtest();
			$placementdetail = $lobjplacementtest->fngetPlacementTest($formData['FromPlacementTest']);

			$larrreqhrs = explode('T',$formData['CopyPlacementTestTime']);
			$newPlacementTestData['PlacementTestName'] = $formData['CopyPlacementTestName'];
			$newPlacementTestData['PlacementTestDate'] = $formData['CopyPlacementTestDate'];
			$newPlacementTestData['PlacementTestTime'] = $larrreqhrs[1];
			$newPlacementTestData['Addr1'] = $placementdetail[0]['Addr1'];
			$newPlacementTestData['Addr2'] = $placementdetail[0]['Addr2'];
			$newPlacementTestData['idCountry'] = $placementdetail[0]['idCountry'];
			$newPlacementTestData['idState'] = $placementdetail[0]['idState'];
			$newPlacementTestData['City'] = $placementdetail[0]['City'];
			$newPlacementTestData['Zip'] = $placementdetail[0]['Zip'];
			$newPlacementTestData['MarksBasedOn'] = $placementdetail[0]['MarksBasedOn'];
			$newPlacementTestData['Active'] = $placementdetail[0]['Active'];
			$newPlacementTestData['UpdDate'] = date ( 'Y-m-d H:i:s' );
			$newPlacementTestData['UpdUser'] = $auth->getIdentity()->iduser;
			$newPlacementTestData['MinMark'] = $placementdetail[0]['MinMark'];
			$newPlacementTestData['TotalMark'] = $placementdetail[0]['TotalMark'];
			$newPlacementTestData['PassingMark'] = $placementdetail[0]['PassingMark'];
			$newPlacementTestData['Phone'] = $placementdetail[0]['Phone'];

			$newIdplacementtest = $lobjplacementtest->fnaddcopyplacementTest($newPlacementTestData);

			$i = 0;
			foreach($placementdetail as $det){
				$placementtestcomponent[$i]['IdPlacementTest'] = $newIdplacementtest;
				$placementtestcomponent[$i]['ComponentName'] = $det['ComponentName'];
				$placementtestcomponent[$i]['ComponentWeightage'] = $det['ComponentWeightage'];
				$placementtestcomponent[$i]['ComponentTotalMarks'] = $det['ComponentTotalMarks'];
				$placementtestcomponent[$i]['MinimumMark'] = $det['MinimumMark'];
				$placementtestcomponent[$i]['UpdUser'] = $auth->getIdentity()->iduser;
				$placementtestcomponent[$i]['UpdDate'] = date ( 'Y-m-d H:i:s' );
				$i++;
			}

			foreach($placementtestcomponent as $rec){
				$lobjplacementtest->fnaddcopyplacementcomponent($rec);
			}
			$programtestdetail = $lobjplacementtest->getplcamenttestprogrammappingdet($formData['FromPlacementTest']);
			$mappingdet = array();
			$mappingdet['IdPlacementTest'] = $newIdplacementtest;
			$mappingdet['IdProgram'] = $programtestdetail['IdProgram'];
			$mappingdet['UpdDate'] = date ( 'Y-m-d H:i:s' );
			$mappingdet['UpdUser'] = $auth->getIdentity()->iduser;
			$lobjplacementtest->addprogramplacementtestmapping($mappingdet);
			$this->_redirect( $this->baseUrl . '/application/placementtest/index/');
		}
	}

	public function getschemelistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$IdProgram = $this->_getParam('IdProgram');
		$lobjprogramscheme = new GeneralSetup_Model_DbTable_Programscheme();
		$schemeList = $lobjprogramscheme->fngetprogramsschemelist($IdProgram);
                
                //var_dump($schemeList);
                
                if (count($schemeList)>0){
                    $i=0;
                    $this->defModel = new App_Model_General_DbTable_Definationms();
                    foreach ($schemeList as $schemeLoop){
                        $mop = $this->defModel->getData($schemeLoop['mode_of_program']);
                        $mod = $this->defModel->getData($schemeLoop['mode_of_study']);
                        $pt = $this->defModel->getData($schemeLoop['program_type']);

                        $schemeList[$i]['name'] = $mop['DefinitionDesc'].' '.$mod['DefinitionDesc'].' '.$pt['DefinitionDesc'];
                        
                        unset($schemeList[$i]['mode_of_program']);
                        unset($schemeList[$i]['mode_of_study']);
                        unset($schemeList[$i]['program_type']);
                        
                        $i++;
                    }   
                }
                
		echo Zend_Json_Encoder::encode($schemeList);
	}

}
