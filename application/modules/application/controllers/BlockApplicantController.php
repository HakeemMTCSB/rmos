<?php
class Application_BlockApplicantController extends Base_Base {
	
	private $locale;
	private $registry;
	private $Awardlevelform;
	private $lobjNewawardform;
	private $Awardlevel;
	private $_gobjlog;
	private $lobjvalidator;
	
	public function init() {
		$this->fnsetObj();
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');		
	}
	
	
	public function fnsetObj(){
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();		
		$this->lobjBlockAppForm = new Application_Form_BlockApplicant();
		$this->BlockAppDB = new Application_Model_DbTable_BlockApplicant();
	}

    public function indexAction(){

        $this->view->title = "Block Applicant";
        $form = new Application_Form_BlockApplicantSearch();
        $blockAppDb = new Application_Model_DbTable_BlockApplicant();
        $query = $blockAppDb->getBlockList(null,'sql');
        $pageCount = 50;

        //post
        if ($this->getRequest()->isPost() ) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);
            $query = $blockAppDb->getBlockList($formData,'sql');

        }

        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);
        $totalRecord = $paginator->getTotalItemCount();


        $this->view->form = $form;
        $this->view->totalRecord = $totalRecord;
        $this->view->paginator = $paginator;

    }

    public function editAction(){

        $id = $this->_getParam('id',null);

        if($id==null){
            $this->_redirect($this->view->url(array('module'=>'application','controller'=>'block-applicant', 'action'=>'index'),'default',true));
        }

        $this->view->title = "Edit Block Applicant";

        $form = new Application_Form_BlockApplicant();

        $blockAppDb = new Application_Model_DbTable_BlockApplicant();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $data = array(
                    'IcNo' => $formData['IcNo'],
                    'Name' => $formData['Name'],
                    'Remark' => $formData['Remark'],

                );



                $blockAppDb->update($data,array('Id=?'=>$id));

                $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success update applicant information');

                $this->_redirect($this->view->url(array('module'=>'application','controller'=>'block-applicant', 'action'=>'index'),'default',true));

            }else{
                $form->populate($formData);
            }
        }else{
            $data = $blockAppDb->fetchRow(array('Id=?'=>$id))->toArray();
            $form->populate($data);
        }

        $this->view->form = $form;
    }

	
	public function addAction(){
        $this->view->title = "Add Block Applicant";

        $form = new Application_Form_BlockApplicant();
        $auth = Zend_Auth::getInstance();


        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $blockAppDb = new Application_Model_DbTable_BlockApplicant();

                $data = array(
                    'IcNo' => $formData['IcNo'],
                    'Name' => $formData['Name'],
                    'Remark' => $formData['Remark'],
                    'createdby' => $auth->getIdentity()->iduser,
                    'createddt' => date('Y-m-d H:i:s')
                );

                $blockAppDb->addData($data);

                $this->gobjsessionsis->flash = array('type'=>'success','message'=>'Success add new Block Applicant');

                $this->_redirect($this->view->url(array('module'=>'application','controller'=>'block-applicant', 'action'=>'index'),'default',true));

            }else{
                $form->populate($formData);
            }
        }

        $this->view->form = $form;
	}
	

	
}




?>