<?php

class Application_IndexController extends Base_Base
{
    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
        $this->model = new App_Model_Application_DbTable_ApplicantTransaction();
    }

    public function indexAction()
    {

        $this->view->title = "Application List";


        $db = getDb();
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $objdeftypeDB = new GeneralSetup_Model_DbTable_Registry_RegistryValue();

        $form = new Application_Form_SearchApplicant();

        $session = new Zend_Session_Namespace('sis');
        $this->view->formData = array();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $formData['program_id'] = $this->view->escape(strip_tags($formData['program_id']));
            $formData['program_scheme_id'] = $this->view->escape(strip_tags($formData['program_scheme_id']));
            $formData['name'] = $this->view->escape(strip_tags($formData['name']));
            $formData['pes_no'] = $this->view->escape(strip_tags($formData['pes_no']));
            $formData['personal_id'] = $this->view->escape(strip_tags($formData['personal_id']));
            $formData['application_status'] = $this->view->escape(strip_tags($formData['application_status']));
            $formData['intake_id'] = $this->view->escape(strip_tags($formData['intake_id']));

            //FACULTY DEAN, FACULTY FINANCE atau FACULTY ADMIN nampak faculty dia sahaja
            if ($session->IdRole == 311 || $session->IdRole == 298 || $session->IdRole == 386) {
                $txnList = $applicantTxnDB->getSearchPaginateFaculty($formData, $session->idCollege);
            } else {
                $txnList = $applicantTxnDB->getSearchPaginate($formData);
            }

            $total = $db->query($applicantTxnDB->getSearchPaginate($formData))->rowCount();

            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($txnList));
            //$paginator->setItemCountPerPage($this->gintPageCount);
            $paginator->setItemCountPerPage(10000);
            $paginator->setCurrentPageNumber($this->_getParam('page', 1));

            $form->populate($formData);
            $this->view->formData = $formData;
        } else {

            //FACULTY DEAN, FACULTY FINANCE atau FACULTY ADMIN nampak faculty dia sahaja
            if ($session->IdRole == 311 || $session->IdRole == 298 || $session->IdRole == 386) {
                $txnList = $applicantTxnDB->getPaginateDataFaculty($session->idCollege);
            } else {
                $txnList = $applicantTxnDB->getPaginateData();
                $total = $db->query($applicantTxnDB->getPaginateData())->rowCount();
            }


            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($txnList));
            //$paginator->setItemCountPerPage($this->gintPageCount);
            $paginator->setItemCountPerPage(20);
            $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        }

        $this->view->form = $form;
        $this->view->paginator = $paginator;
        $this->view->total = $total;

        $auth = Zend_Auth::getInstance();
        $this->view->iduser = $auth->getIdentity()->iduser;

        //status
        $status = $objdeftypeDB->getListDataByCodeType('application-status');

        $this->view->statusCount = array();
        foreach ($status as $sts) {
            $getTotal = $db->query($db->quoteInto('SELECT at_trans_id FROM applicant_transaction WHERE at_status=?', $sts['idDefinition']))->rowCount();

            $this->view->statusCount[$sts['DefinitionDesc']] = $getTotal;

            //idDefinition, DefinitionDesc
        }

        //intake
        $this->view->intakeCount = array();
        $intakeDb = new GeneralSetup_Model_DbTable_Intake();
        $listData = $intakeDb->fngetIntakeList();
        foreach ($listData as $list) {
            $getTotal = $db->query($db->quoteInto('SELECT at_trans_id FROM applicant_transaction WHERE at_intake=?', $list['IdIntake']))->rowCount();
            $this->view->intakeCount[$list['IntakeDesc']] = $getTotal;
        }

    }

    public function homeAction()
    {

    }

    public function auditDetailsAction()
    {
        //models
        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $objdeftypeDB = new App_Model_Definitiontype();
        $appInitConfigDB = new Application_Model_DbTable_ApplicationInitialConfig();
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $programDB = new App_Model_Application_DbTable_ApplicantProgram();
        $appQualificationDB = new App_Model_Application_DbTable_ApplicantQualification();
        $countryDB = new App_Model_General_DbTable_Country();
        $entryReqDB = new App_Model_Application_DbTable_EntryRequirement();
        $institutionDB = new App_Model_Application_DbTable_Institution();
        $appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();
        $appHealthDB = new App_Model_Application_DbTable_ApplicantHealthCondition();
        $appFinanacialDB = new App_Model_Application_DbTable_ApplicantFinancial();
        $appvisaDB = new App_Model_Application_DbTable_ApplicantVisa();
        $auditDB = new App_Model_Audit();

        //title
        $this->view->title = "Applicant Profile History";
        $this->view->tab = 'audit';

        $id = $this->_getParam('id', 0);
        $txn_id = $this->_getParam('txn_id', 0);
        $sid = $this->_getParam('sid', 0);

        //txn
        $this->view->txn_id = $txn_id;
        $txnData = $applicantTxnDB->getTransaction($txn_id);

        //programme/sections
        $program = $programDB->getProgram($txn_id);
        $sectionDB = new Application_Model_DbTable_ApplicationSection();
        $section_list = $sectionDB->getSection($program['ap_prog_id'], $program['ap_prog_scheme'], $txnData['appl_category']);
        $sectionItems = array();

        array_unshift($section_list, array('sectionID' => 1, 'name' => 'Programme Details'));

        foreach ($section_list as $section) {
            if ($section['sectionID'] == 1) {
                $listItem = $appInitConfigDB->getItemBasedOnSection(1);
            } else {
                $listItem = $appInitConfigDB->getItem($program['ap_prog_scheme'], $section['id']);
            }

            $sectionItems[$section['sectionID']] = $this->cleanSectionItem($listItem, $section['sectionID']);
        }

        //format items into variable
        $varinfo = array();
        foreach ($sectionItems[$sid] as $item) {
            $varinfo[$this->remapVariable($item['variable'])] = $item['name'];
        }

        //hardcoded
        $varinfo['appl_type_nationality'] = 'Type of Nationality';
        $varinfo['branch_id'] = 'CP';
        $varinfo['ep_test_detail'] = 'Format';
        $varinfo['aw_industry_id'] = 'Industry Working Experience';
        $varinfo['at_intake'] = 'Intake';

        //audit log data
        $audit_info = $auditDB->getDataById($id);
        $log_data = json_decode($audit_info['log_data'], true);

        //populate values
        $this->view->values = array();

        foreach ($log_data as $row) {
            $this->view->values[$row['variable']]['value'] = $this->remapValues($row['variable'], $row['value']);
            $this->view->values[$row['variable']]['prev_value'] = $this->remapValues($row['variable'], $row['prev_value']);
        }

        //views
        $this->view->varinfo = $varinfo;
        $this->view->audit_info = $audit_info;
        $this->view->data = $log_data;
    }

    public function remapValues($variable, $value = '')
    {
        $objdeftypeDB = new App_Model_Definitiontype();
        $countryDB = new App_Model_General_DbTable_Country();
        $branchDB = new GeneralSetup_Model_DbTable_Branchofficevenue();
        $qualificationDB = new App_Model_General_DbTable_Qualificationmaster();
        $entryReqDB = new App_Model_Application_DbTable_EntryRequirement();
        $institutionDB = new App_Model_Application_DbTable_Institution();

        switch ($variable) {
            case "mode_study":
                return $this->getRealValueByDef($value, $objdeftypeDB->fnGetDefinationsByLocale('Mode of Study'));
                break;
            case "program_mode":
                return $this->getRealValueByDef($value, $objdeftypeDB->fnGetDefinationsByLocale('Mode of Program'));
                break;
            case "program_type":
                return $this->getRealValueByDef($value, $objdeftypeDB->fnGetDefinationsByLocale('Program Type'));
                break;
            case "at_intake":
                $db = getDb();
                $select = $db->select()->from('tbl_intake')->where('IdIntake=?', $value);
                $row = $db->fetchRow($select);
                return $row['IntakeId'];
                break;

            /*2 */
            case 'appl_salutation':
                return $this->getRealValueByDef($value, $objdeftypeDB->fnGetDefinationsByLocale('Salutation'));
                break;

            case "appl_nationality":
            case "appl_country":
            case "appl_ccountry":
            case "ae_institution_country":
                return $this->getValueById($value, $countryDB->getData(), array('k' => 'idCountry', 'v' => 'CountryName'));
                break;

            case "appl_type_nationality":
                return $this->getRealValueByDef($value, $objdeftypeDB->fnGetDefinationsByLocale('Type of Nationality'));
                break;

            case "appl_idnumber_type":
                return $this->getRealValueByDef($value, $objdeftypeDB->fnGetDefinationsByLocale('ID Type'));
                break;

            case "branch_id":
                return $this->getValueById($value, $branchDB->fnGetBranchList(), array('k' => 'id', 'v' => 'value'));
                break;

            case "appl_gender":
                return $value == '1' ? $this->view->translate('Male') : $this->view->translate('Female');
                break;

            case "appl_religion":
                return $this->getRealValueByDef($value, $objdeftypeDB->fnGetDefinationsByLocale('Religion'));
                break;

            case "appl_race":
                return $this->getRealValueByDef($value, $objdeftypeDB->fnGetDefinationsByLocale('Race'));
                break;

            case "appl_marital_status":
                return $this->getRealValueByDef($value, $objdeftypeDB->fnGetDefinationsByLocale('Marital Status'));
                break;

            case "appl_state":
            case "appl_cstate":
                return $this->getStateById($value);
                break;

            /*
			3
			*/
            case "ae_qualification":
                return $this->getValueById($value, $entryReqDB->getListGeneralReq(), array('k' => 'IdQualification', 'v' => 'QualificationLevel'));
                break;

            case "ae_class_degree":
                return $this->getRealValueByDef($value, $objdeftypeDB->fnGetDefinationsByLocale('Class Degree'));
                break;

            case "ae_institution":
                return $this->getValueById($value, $institutionDB->getData(), array('k' => 'idInstitution', 'v' => 'InstitutionName'));
                break;
            /*
			4
			*/
            case "ep_test":
                return $this->getValueById($value, $qualificationDB->getDataTest(), array('k' => 'IdQualification', 'v' => 'QualificationLevel'));
                break;
            case "ep_test_detail":
                return $this->getRealValueByDef($value, $objdeftypeDB->fnGetDefinationsByLocale('Format  TOEFL'));
                break;
            /*
			8
			*/
            case "aw_industry_id":
                $split = explode('-', $value);
                $value = $this->getRealValueByDef($split[0], $objdeftypeDB->fnGetDefinationsByLocale('Industry Working Experience'));
                $value .= ' - ' . $this->getRealValueByDef($split[1], $objdeftypeDB->fnGetDefinationsByLocale('Years of Industry'));

                return $value;
                break;
            /*
			9
			*/
            case "health_condition":
                $result = $objdeftypeDB->fnGetDefinationsByLocale('Health Condition');
                $defbyid = array();
                foreach ($result as $def) {
                    $defbyid[$def['idDefinition']] = $def['DefinitionDesc'];
                }

                $out = array();
                $values = explode(',', $value);
                foreach ($values as $val) {
                    $out[] = $defbyid[$val];
                }

                return implode(', ', $out);
                break;

            default;
                return $value;
        }
    }

    public function remapVariable($name)
    {
        /* why is it not the same... */
        $map = array(
            'ap_prog_scheme'         => 'ap_prog_scheme',

            /* section 2 */
            'appl_salutation'        => 'salutation',
            'appl_fname'             => 'first_name',
            'appl_lname'             => 'last_name',
            'appl_idnumber'          => 'idNo',
            'appl_idnumber_type'     => 'typeID',
            'appl_passport_expiry'   => 'passport_expiry',
            'appl_email'             => 'email',
            'appl_dob'               => 'dob',
            'appl_gender'            => 'gender',
            'appl_nationality'       => 'nationality',
            'appl_category'          => 'appl_category',
            'appl_religion'          => 'religion',
            'appl_race'              => 'race',
            'appl_marital_status'    => 'marital_status',
            'appl_bumiputera'        => 'bumiputra_eligibility',
            'appl_address1'          => 'address_perm1',
            'appl_address2'          => 'address_perm2',
            'appl_address3'          => 'address_perm3',
            'appl_city'              => 'city_perm',
            'appl_state'             => 'state_perm',
            'appl_country'           => 'country_perm',
            'appl_phone_home'        => 'home_perm',
            'appl_phone_mobile'      => 'mobile_perm',
            'appl_phone_office'      => 'office_perm',
            'appl_fax'               => 'fax_perm',
            'appl_caddress1'         => 'address_corr1',
            'appl_caddress2'         => 'address_corr2',
            'appl_caddress3'         => 'address_corr3',
            'appl_cpostcode'         => 'postcode_corr',
            'appl_ccity'             => 'city_corr',
            'appl_cstate'            => 'state_corr',
            'appl_ccountry'          => 'country_corr',
            'appl_cphone_home'       => 'home_corr',
            'appl_cphone_mobile'     => 'mobile_corr',
            'appl_cphone_office'     => 'office_corr',
            'appl_cfax'              => 'fax-corr',
            'appl_contact_home'      => 'phone_home',
            'appl_contact_mobile'    => 'phone_mobile',
            'appl_contact_office'    => 'phone_office',

            /* Section 3 */
            'ae_qualification'       => 'qualification_id',
            'ae_degree_awarded'      => 'degree_awarded',
            'ae_class_degree'        => 'class_degree',
            'ae_result'              => 'result',
            'ae_year_graduate'       => 'year_graduated',
            'ae_institution_country' => 'ae_institution_country',
            'ae_institution'         => 'institution_name',
            'ae_medium_instruction'  => 'medium_instruction',
            'others'                 => 'others',

            /* Section 4 */
            'ep_test'                => 'english_test',
            'ep_test_detail'         => 'english_test_detail',
            'ep_date_taken'          => 'english_date_taken',
            'ep_score'               => 'english_score',

            /* Section 5 */
            'qp_level'               => 'qp_level',
            'qp_grade_mark'          => 'qp_grade_mark',
            'qp_institution'         => 'qp_institution',

            /* Section 6 */
            'rpd_title'              => 'rpd_title',
            'rpd_date'               => 'rpd_date',
            'rpd_publisher'          => 'rpd_publisher',

        );

        foreach ($map as $key => $val) {
            if ($name == $val) {
                return $key;
            }
        }

        return $name;
    }

    public function viewAction()
    {

        $this->view->title = "Application Summary";

        $txnId = $this->_getParam('txn_id', 0);
        $this->view->txn_id = $txnId;

        //not required
        //$idSection = $this->_getParam('idSection', 0);
        //$this->view->idSection = $idSection;

        $this->view->tab = 'summary';

        $pid = $this->_getParam('pid', 0);
        $this->view->pid = $pid;

        //MODELS
        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $objdeftypeDB = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $appInitConfigDB = new Application_Model_DbTable_ApplicationInitialConfig();
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $programDB = new App_Model_Application_DbTable_ApplicantProgram();
        $appQualificationDB = new App_Model_Application_DbTable_ApplicantQualification();
        $countryDB = new App_Model_General_DbTable_Country();
        $entryReqDB = new App_Model_Application_DbTable_EntryRequirement();
        $institutionDB = new App_Model_Application_DbTable_Institution();
        $appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();
        $appHealthDB = new App_Model_Application_DbTable_ApplicantHealthCondition();
        $appFinanacialDB = new App_Model_Application_DbTable_ApplicantFinancial();
        $appvisaDB = new App_Model_Application_DbTable_ApplicantVisa();
        $commDB = new Communication_Model_DbTable_General();

        //txn data
        $txnData = $applicantTxnDB->getTransaction($txnId, 1);

        $appl_id = $txnData['at_appl_id'];
        $transID = $txnData['at_trans_id'];
        $IdConfig = $txnData['IdConfig'];
        $this->view->txnData = $txnData;

        $this->view->session_transactionID = $transID;


        //programme applied
        $program = $programDB->getProgram($txnId);
        $this->view->program = $program;

        $photo = $appDocumentDB->getDataArrayBySection($transID, 2, 'Passport sized photo');
        $app_photo = $this->view->app_photo_full = '';
        if (!empty($photo)) {
            $this->view->app_photo_full = DOCUMENT_URL . $photo[0]['ad_filepath'];
            $app_photo = $this->view->baseUrl() . '/thumb.php?s=1&w=100&f=' . DOCUMENT_PATH . $photo[0]['ad_filepath'];
        }
        $this->view->app_photo = $app_photo;


        //get sidebar list
        $sectionDB = new Application_Model_DbTable_ApplicationSection();
        //$section_list = $sectionDB->getSection($program['ap_prog_scheme'], $txnData['appl_category'], $program['program_id']);
        $section_list = $sectionDB->getSection($IdConfig);

        //sectionData
        //$sectionData = $appInitConfigDB->fnGetDefination($idSection);
        //$mainsectionid = $sectionData['sectionID'];


        //Section items
        $sectionItems = array();


        foreach ($section_list as $index => $section) {
            $listItem = $appInitConfigDB->getItem($IdConfig, $section['id']);

            //Applicant Documents
            $section_list[$index]['docs'] = $appDocumentDB->getDataArrayByTxn($transID, $section['sectionID']);

            $sectionItems[$section['sectionID']] = $this->cleanSectionItem($listItem, $section['sectionID']);

        }
        $this->view->section_list = $section_list;
        $this->view->sectionItems = $sectionItems;

        //echo '<pre>';
        //print_r($section_list);


        //-------------------
        // section 2
        // ------------------
        $applicant = $appProfileDB->getProfile($txnId);

        $applicant['prev_student'] = $this->getBooleanText($applicant['prev_student']);
        $applicant['studentID'] = $applicant['prev_studentID'];

        $applicant['salutation'] = $applicant['appl_salutation'];
        $applicant['salutation'] = $this->getRealValueByDef($applicant['salutation'], $objdeftypeDB->getListDataByCodeType('salutation'));
        $applicant['first_name'] = $applicant['appl_fname'];
        $applicant['last_name'] = $applicant['appl_lname'];
        $applicant['idNo'] = $applicant['appl_idnumber'];
        $applicant['typeID'] = $this->getRealValueByDef($applicant['appl_idnumber_type'], $objdeftypeDB->getListDataByCodeType('identity-card-type'));
        if ($applicant['appl_passport_expiry']) {
            $applicant['passport_expiry'] = date('d-m-Y', strtotime($applicant['appl_passport_expiry']));
        }
        $applicant['email'] = $applicant['appl_email'];
        $applicant['dob'] = date('d-m-Y', strtotime($applicant['appl_dob']));
        $applicant['gender'] = $applicant['appl_gender'] == '1' ? $this->view->translate('Male') : $this->view->translate('Female');
        $applicant['nationality'] = $this->getValueById($applicant['appl_nationality'], $countryDB->getData(), array('k' => 'idCountry', 'v' => 'CountryName'));
        $applicant['appl_category'] = $applicant['appl_category'];
        $applicant['age'] = $this->calcAge($applicant['dob']);

        $applicant['religion'] = $applicant['appl_religion'] == 99 ? $applicant['appl_religion_others'] : $this->getRealValueByDef($applicant['appl_religion'], $objdeftypeDB->getListDataByCodeType('religion'));
        $applicant['race'] = $this->ifEmpty($applicant['appl_race']);
        $applicant['marital_status'] = $this->getRealValueByDef($applicant['appl_marital_status'], $objdeftypeDB->getListDataByCodeType('marital-status'));
        $applicant['bumiputra_eligibility'] = $applicant['appl_bumiputera'];

        $applicant['address_perm1'] = $applicant['appl_address1'];
        $applicant['address_perm2'] = $applicant['appl_address2'];
        $applicant['address_perm3'] = $applicant['appl_address3'];
        $applicant['postcode_perm'] = $applicant['appl_postcode'];
        $applicant['city_perm'] = $applicant['appl_city'] == 99 ? $applicant['appl_city_others'] : $applicant['CityName'];
        $applicant['state_perm'] = ($applicant['appl_state'] == 99 || $applicant['appl_state'] == '') ? $applicant['appl_state_others'] : $this->getStateByCountryId($applicant['appl_state'], $applicant['appl_country'], $applicant['appl_state_others']);
        $applicant['country_perm'] = $applicant['appl_country'] == 99 ? $applicant['appl_country_others'] : $this->getValueById($applicant['appl_country'], $countryDB->getData(), array('k' => 'idCountry', 'v' => 'CountryName'));
        $applicant['home_perm'] = $applicant['appl_phone_home'];
        $applicant['mobile_perm'] = $applicant['appl_phone_mobile'];
        $applicant['office_perm'] = $applicant['appl_phone_office'];
        $applicant['fax_perm'] = $applicant['appl_fax'];

        $applicant['address_corr1'] = $applicant['appl_caddress1'];
        $applicant['address_corr2'] = $applicant['appl_caddress2'];
        $applicant['address_corr3'] = $applicant['appl_caddress3'];
        $applicant['postcode_corr'] = $applicant['appl_cpostcode'];
        $applicant['city_corr'] = $applicant['appl_ccity'] == 99 ? $applicant['appl_ccity_others'] : $applicant['CCityName'];
        $applicant['state_corr'] = ($applicant['appl_cstate'] == 99 || $applicant['appl_cstate'] == '') ? $applicant['appl_cstate_others'] : $this->getStateByCountryId($applicant['appl_cstate'], $applicant['appl_ccountry'], $applicant['appl_cstate_others']);
        $applicant['country_corr'] = $applicant['appl_ccountry'] == 99 ? $applicant['appl_ccountry_others'] : $this->getValueById($applicant['appl_ccountry'], $countryDB->getData(), array('k' => 'idCountry', 'v' => 'CountryName'));
        $applicant['home_corr'] = $applicant['appl_cphone_home'];
        $applicant['mobile_corr'] = $applicant['appl_cphone_mobile'];
        $applicant['office_corr'] = $applicant['appl_cphone_office'];
        $applicant['fax-corr'] = $applicant['appl_cfax'];

        $applicant['phone_home'] = $applicant['appl_contact_home'];
        $applicant['phone_mobile'] = $applicant['appl_contact_mobile'];
        $applicant['phone_office'] = $applicant['appl_contact_office'];

        $applicant['program'] = $applicant['ProgramName'];
        $applicant['program_mode'] = $applicant['mop'];
        $applicant['mode_study'] = $applicant['mos'];
        $applicant['program_type'] = $applicant['pt'];
        $applicant['level_of_study'] = $applicant['level_of_study'];
        $applicant['intake'] = $applicant['IntakeDesc'];
        $applicant['branch'] = $applicant['BranchName'];

        //IdSection = 26 ENTRY REQUIREMENT

        $programEntryDB = new Application_Model_DbTable_Programentry();
        $result = $programEntryDB->getProgramEntryList($applicant['IdScheme'], $applicant['ap_prog_id'], $applicant['ap_prog_scheme']);
        $table = '<table class="table"><tr><td>';
        $table .= '<table width="100%">';
        foreach ($result as $data) {

            $qualification = $data['qt_eng_desc'];
            $qualification .= ($data['subjects']) ? ' with minimum of ' . $data['subjects'] : '';
            $qualification .= ($data['Value']) ? ' with minimum CGPA of ' . $data['Value'] : '';
            $qualification .= (isset($data['sf_eng_desc'])) ? ' in ' . $data['sf_eng_desc'] : '';

            $table .= '<tr>
                            <td width="5px"><input type="checkbox" class="entryReq" name="entry_requirement[]" id="entry_requirement-' . $data["IdProgramEntryReq"] . '" value="' . $data["IdProgramEntryReq"] . '" onclick="displaySubRequirements(this)"></td>
                                <td> ' . $qualification . '</td>
                        </tr>';
        }

        $table .= '</table><br>';

        $resultOpenEntry = $programEntryDB->getProgramEntryList($applicant['IdScheme'], $applicant['ap_prog_id'], $applicant['ap_prog_scheme'], 1);

        if ($resultOpenEntry) {
            $table .= '<table>
                        <tr>
                            <td colspan="2" align="left">
                            <h4>OR <br><br>Re-apply through APEL for Admission or APEL (A)</h4>
                             
                            <font color="green"><span class="icon icon-info-circled"></span></font>
                             APEL for Admission or APEL (A) is a flexible pathway for adult learners to enter into OUM academic programmes. It employs minimal entry requirements while taking into consideration the applicant\'s prior learning and work experience for admission. Applicant must fulfill the entry requirements outlined for the programme level and pass for the APEL (A) Assessment.
        
                            </td>
                        </tr>
                        ';

            foreach ($resultOpenEntry as $data) {
                $table .= '<tr>
                                <td width="5px"><input type="checkbox" class="entryReq" name="entry_requirement[]" id="entry_requirement-' . $data["IdProgramEntryReq"] . '" value="' . $data["IdProgramEntryReq"] . '" onclick="displaySubRequirements(this)"></td>
                                <td> ' . $data["requirement"] . '</td>
                            </tr>';
            }
            $table .= '</table>	';
        }

        $table .= '<br><table width="100%">
                        <tr>
                            <td colspan="2" align="left">
                            <h4>OR <br><br>Choose another programme</h4>
                            
                        </td>
                        </tr>
                        <tr>
                            <td width="5px"><input type="checkbox" class="entryReq" name="entry_requirement[]" id="entry_requirement-0" value="0" onclick="displaySubRequirements(this)"></td>
                            <td>I do not have any of the qualifications</td>
                        </tr>
                    ';
        $table .= '</table>	';

        $table .= '</td></tr></table>';

        $this->view->tableRequirement = $table;

        //applicant entry req
        $applicantEntryReqDB = new Application_Model_DbTable_ApplicantEntryRequirement();
        $applicantEntry = $applicantEntryReqDB->getData($transID);

        if ($applicantEntry) {
            foreach ($applicantEntry as $entry) {
                $applicant['entry_requirement'][$entry['IdProgramEntryReq']] = $entry['status'];
            }

            $this->view->applicantEntryReq = $applicant['entry_requirement'];
        }


        //-------------------
        // section 3
        // ------------------

        //1st:track from current profile data exist or not
        $appQualification = $appQualificationDB->getTransData($appl_id, $txnId);

        //get documents
        if (count($appQualification) > 0) {
            foreach ($appQualification as $index => $q) {
                $documents = $appDocumentDB->getDataArray($txnId, $q['ae_id'], 'applicant_qualification');
                $appQualification[$index]['documents'] = $documents;

            }
        }
        $this->view->appQualification = $appQualification;

        /*if(isset($q['ae_id']))
		{
			$q = $appQualificationDB->getQualification($q['ae_id']);

			$applicant['qualification_id'] = $this->getValueById($q['ae_qualification'],$entryReqDB->getListGeneralReq(),array('k'=>'IdQualification','v'=>'QualificationLevel'));
			$applicant['degree_awarded'] = $q['ae_degree_awarded'];
			$applicant['class_degree'] = $this->getRealValueByDef($q['ae_class_degree'],$objdeftypeDB->fnGetDefinations('Class Degree'));
			$applicant['result'] = $q['ae_result'];
			$applicant['year_graduated'] = $q['ae_year_graduate'];
			$applicant['medium_instruction'] = $q['ae_medium_instruction'];
			$applicant['ae_institution_country'] = $q['CountryName'];

			if($q['ae_institution']==999){
				$applicant['institution_name'] = $q['others'];
			}else{
				$applicant['institution_name'] = $q['InstitutionName'];
				//$this->getValueById($q['ae_institution'],$institutionDB->getData(),array('k'=>'idInstitution', 'v'=>'InstitutionName'));
			}


		}else{
			//$applicant = array();
		}*/


        //-------------------
        // section 4
        // ------------------
        $appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
        $engprof = $appEngProfDB->getData($txnId);

        //$app_documents = $appDocumentDB->getDataArrayBySection($txnId,4);
        //$this->view->app_docs =$app_documents;

        $this->view->pid = @$engprof[0]['ep_id'];
        $applicant['english_test'] = @$engprof[0]['ep_test'] == 25 ? @$engprof[0]['ep_test_detail'] : @$engprof[0]['QualificationLevel'];
        $applicant['english_date_taken'] = @$engprof[0]['ep_date_taken'];
        $applicant['english_score'] = @$engprof[0]['ep_score'];


        //-------------------
        // section 5
        // ------------------

        $qpDB = new App_Model_Application_DbTable_ApplicantQuantitativeProficiency();
        $quanprof_list = $qpDB->getTransData($txnId);

        //get documents
        foreach ($quanprof_list as $index => $q) {

            $documents = $appDocumentDB->getDataArray($txnId, $q['qp_id'], 'applicant_quantitative_proficiency');
            $quanprof_list[$index]['documents'] = $documents;
        }
        $this->view->qp_list = $quanprof_list;


        //-------------------
        // section 6 (not used)
        // ------------------


        $rpdDB = new App_Model_Application_DbTable_ApplicantResearchPublication();
        $research_list = $rpdDB->getTransData($txnId);
        //get documents
        foreach ($research_list as $index => $r) {

            $documents = $appDocumentDB->getDataArray($txnId, $r['rpd_id'], 'applicant_research_publication');
            $research_list[$index]['documents'] = $documents;
        }
        $this->view->rpd_list = $research_list;
        $this->view->total_added = count($research_list);


        //-------------------
        // section 7 : Employment Details
        // ------------------

        $appEmployment = $appEmploymentDB->getTransData($appl_id, $txnId);

        if (count($appEmployment) > 0) {
            foreach ($appEmployment as $index => $emp) {
                $documents = $appDocumentDB->getDataArray($txnId, $emp['ae_id'], 'applicant_employment');
                $appEmployment[$index]['documents'] = $documents;
            }
        }
        $this->view->appEmployment = $appEmployment;
        $this->view->positionLevel = $objdeftypeDB->getListDataByCodeType('position-level');


        /*if(isset($emp['ae_id']))
		{
			$emp = $appEmploymentDB->getEmployement($emp['ae_id']);
			$applicant['emply_status']=$this->getRealValueByDef($emp['ae_status'],$objdeftypeDB->fnGetDefinations('Employment Status'));
			$applicant['emply_comp_name']=$emp['ae_comp_name'];
			$applicant['emply_comp_address']=$emp['ae_comp_address'];
			$applicant['emply_comp_phone']=$emp['ae_comp_phone'];
			$applicant['emply_comp_fax']=$emp['ae_comp_fax'];
			$applicant['emply_designation']=$emp['ae_designation'];
			$applicant['emply_position_level']=$emp['ae_position'];
			$applicant['emply_duration_from']=$emp['ae_from'];
			$applicant['emply_duration_to']=$emp['ae_to'];
			$applicant['emply_industry']=$emp['ae_industry'];
			$applicant['job_desc']=$emp['ae_job_desc'];
			$this->view->position = $emp['ae_position'];
		}  */


        //-------------------
        // section 8 : Working XP
        // ------------------


        $industryWorkingExperience = $objdeftypeDB->getListDataByCodeType('industry-working-experience');
        $this->view->industryWorkingExperience = $industryWorkingExperience;

        $this->view->yearofIndustry = $objdeftypeDB->getListDataByCodeType('years-of-industry');

        //-------------------
        // section 9 : Health
        // ------------------
        $result = $objdeftypeDB->getListDataByCodeType('health-condition');

        $applicant['health_condition'] = '';

        if ($result) {

            foreach ($result as $index => $r) {
                //get data
                $appHealth = $appHealthDB->getRowData($txnId, $r['idDefinition']);
                //var_dump($appHealth);
                if (isset($appHealth) && $appHealth['ah_status'] != '') {
                    $applicant['health_condition'] .= $r['DefinitionDesc'] . ($appHealth['ah_status'] == 796 ? ' (' . $appHealth['ah_others'] . ')' : '') . '<br />';
                }
            }
        }
        //-------------------
        // section 10 : Financial Particulars
        // ------------------

        $appFinancial = $appFinanacialDB->getDataByTxn($txnId);

        /*if(isset($appFinancial) && $appFinancial['af_id']!=''){
			//$this->view->app_docs = $appDocumentDB->getDataArray($txnId,$appFinancial['af_id'],'applicant_financial');
		}*/

        //dd($appFinancial); exit;

        $status = array(
            1  => 'APPLIED',
            2  => 'OFFERED',
            3  => 'REJECTED',
            4  => 'COMPLETED',
            5  => 'INCOMPLETE',
            6  => 'CANCEL',
            7  => 'OFFERED',
            8  => 'SHORTLISTED',
            9  => 'OFFERED-ACCEPTED',
            10 => 'OFFERED-DECLINED'
        );

        $applicant['fin_method'] = $appFinancial['financial_method'];
        $applicant['fin_sponsor_name'] = $appFinancial['af_sponsor_name'];
        $applicant['fin_type_scholarship'] = $appFinancial['financial_type_scholarship'];
        $applicant['fin_scholarship_secured'] = $appFinancial['af_scholarship_secured'];
        $applicant['fin_scholarship_apply'] = $appFinancial['af_scholarship_apply'];
//        $applicant['fin_apply'] = $status[$appFinancial['sa_status']] . ' - ' . $appFinancial['sch_name'];
        $applicant['file_fin_resume'] = $appFinancial['af_file'];


        //-------------------
        // section 11 : Visa Details
        // ------------------

        $visa = $appvisaDB->getTransData($txnId);
        $applicant['primary_id'] = $visa['av_id'];
        $applicant['visa_malaysian'] = $this->getBooleanText($visa['av_malaysian_visa']);
        $applicant['visa_status'] = $this->getRealValueByDef($visa['av_status'], $objdeftypeDB->getListDataByCodeType('visa-status'));
        $applicant['visa_expiry'] = $visa['av_expiry'];


        //-------------------
        // section 12 : Accomodation
        // ------------------


        $accDB = new App_Model_Application_DbTable_ApplicantAccomodation();
        $accomodation = $accDB->getDatabyTxn($txnId);

        $applicant['primary_id'] = $accomodation['acd_id'];
        $applicant['acd_assistance'] = $accomodation['acd_assistance'];
        //$form->acd_type->setValue($accomodation['acd_type']);

        $roomTypeDB = new Hostel_Model_DbTable_Hostelroom();
        $room_list = $roomTypeDB->getRoomList(263);
        //$form->acd_type->setAttrib('class','accomodationtype');
        $table = '<table border=0 width="80%">
					<tr>
					   <th>' . $this->view->translate("Occupancy Type") . '</th>
					   <th>' . $this->view->translate("Room Type") . '</th>
					   <th>' . $this->view->translate("Monthly Rate") . '<br>(RM)</th>
					   <th>' . $this->view->translate("Tick") . '</th>
					</tr>';

        foreach ($room_list as $room) {
            $table .= "<tr>
						<td align='center'>" . $room['OccupancyType'] . "</td>
						<td align='center'>" . $room['RoomType'] . "</td>
						<td align='center'>" . $room['Rate'] . "</td><td  align='center'>";
            //$table .= $form->acd_type->addMultiOption($room['IdHostelRoomType']);
            $table .= "</td></tr>";
        }
        $table .= '</table>';

        $this->view->table = $table;


        // -----------------------------------
        // section 13 : Initial Registration
        // -----------------------------------

        //get subject from landscape
        $landscapeDB = new App_Model_General_DbTable_Landscape();
        $subject_list = $landscapeDB->getLandscapeSubject($program['ap_prog_id'], $program['ap_prog_scheme'], $txnData['at_intake']);

        //dd($subject_list); exit;

        $landscapeInfo = $landscapeDB->getLandscapeInfo($program['ap_prog_id'], $program['ap_prog_scheme'], $txnData['at_intake']);

        $table = '';

        if (!empty($landscapeInfo)) {
            $tagInfo = $landscapeDB->getLandscapeSubjectTag($landscapeInfo['IdLandscape']);
            $tagEncode = json_decode($tagInfo['subjectdata'], true);

            $intakeInfo = $landscapeDB->getIntakeInfo($txnData['at_intake']);
            $semInfo = $landscapeDB->getSemesterInfo($intakeInfo['sem_year'], $intakeInfo['sem_seq']);

            $i = 0;

            //dd($tagEncode); exit;

            if (count($subject_list) > 0) {
                foreach ($subject_list as $loop) {
                    if (isset($tagEncode[$loop['IdSubject']])) {
                        //var_dump($tagEncode[$loop['IdSubject']]);
                        if (isset($tagEncode[$loop['IdSubject']][$semInfo['SemesterType']])) {
                            $subject_list2[$i] = $loop;
                            $i++;
                        }
                    }
                }


                $table = '<table border=0 width="100%" class="table">
				<tr>
				<th>' . $this->view->translate("No") . '</th>
				<th>' . $this->view->translate("Course Code") . '</th>
				<th align="left">' . $this->view->translate("Course Name") . '</th>
				<th>' . $this->view->translate("Credit Hour") . '</th>		        			
				</tr>';
                if (isset($subject_list2) && count($subject_list2) > 0) {
                    foreach ($subject_list2 as $index => $subject) {
                        $no = $index + 1;
                        $table .= "<tr>
						<td align='center'>" . $no . ".</td>
						<td>" . $subject['SubCode'] . "</td>
						<td>" . $subject['SubjectName'] . "</td>
						<td align='center'>" . $subject['CreditHours'] . "</td>";
                        $table .= "</tr>";
                    }
                } else {
                    $table .= "<tr><td colspan=4>No Initial Registration Course available</td></tr>";
                }
                $table .= '</table>';
            }
        }
        $this->view->initial_reg = $table;


        // -----------------------------------
        // section 16 : Referees
        // -----------------------------------

        $refereeDB = new App_Model_Application_DbTable_ApplicantReferees();
        $referee = $refereeDB->getData($txnId);

        if ($referee) {
            if ($referee['r_ref1_state'] == 99) {
                $referee['r_ref1_state'] = $referee['r_ref1_state_others'];
            }
            if ($referee['r_ref1_city'] == 99) {
                $referee['r_ref1_city'] = $referee['r_ref1_city_others'];
            }
            if ($referee['r_ref2_state'] == 99) {
                $referee['r_ref2_state'] = $referee['r_ref2_state_others'];
            }
            if ($referee['r_ref2_city'] == 99) {
                $referee['r_ref2_city'] = $referee['r_ref2_city_others'];
            }
            $referee['r_ref1_country'] = $referee['r_ref1_country_name'];
            $referee['r_ref2_country'] = $referee['r_ref2_country_name'];
            $applicant = array_merge($applicant, $referee);
        }

        //echo '<pre>';
        //print_r($applicant);

        // Spit them out
        $this->view->applicant = $applicant;
    }

    public function viewCommDetailAction()
    {
        $this->view->ajax = 0;

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->view->ajax = 1;
        }

        $this->view->title = "Application Summary - Communication Detail";

        $txnId = $this->_getParam('txn_id', 0);
        $this->view->txn_id = $txnId;


        $id = $this->_getParam('id', 0);
        $this->view->id = $id;

        $type = $this->_getParam('type', 0);
        $this->view->type = $id;


        $db = getDB();

        if ($type == 'comm') {
            $select = $db->select()
                ->from(array('a' => 'comm_compose'))
                ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
                ->joinLeft(array('ts' => 'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('created_by_name' => 'Fullname'))
                ->joinLeft(array('d' => 'tbl_definationms'), 'd.idDefinition=a.comp_type', array('comp_type_name' => 'd.DefinitionDesc'))
                ->where('a.comp_id = ?', $id);

            $row = $db->fetchRow($select);

            $this->view->message = ifexists($row['comp_content'], '-');
            $this->view->subject = ifexists($row['comp_subject'], '-');
        } else {
            $select = $db->select()
                ->from(array('a' => 'email_que'))
                ->where('a.id = ?', $id);

            $row = $db->fetchRow($select);

            $this->view->subject = ifexists($row['subject'], '-');
            $this->view->message = ifexists($row['content'], '-');
        }

    }

    public function viewCommunicationAction()
    {

        $this->view->title = "Application Summary - Communication History";

        $txnId = $this->_getParam('txn_id', 0);
        $this->view->txn_id = $txnId;

        $this->view->tab = 'comm';

        $pid = $this->_getParam('pid', 0);
        $this->view->pid = $pid;

        //MODELS
        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $commDB = new Communication_Model_DbTable_General();

        //txn data
        $txnData = $applicantTxnDB->getTransaction($txnId, 1);
        $appl_id = $txnData['at_appl_id'];
        $transID = $txnData['at_trans_id'];
        $this->view->txnData = $txnData;
        $this->view->session_transactionID = $transID;

        $applicant = $appProfileDB->getData($appl_id, 1);

        $photo = $appDocumentDB->getDataArrayBySection($transID, 2, 'Passport sized photo');
        $app_photo = $this->view->app_photo_full = '';
        if (!empty($photo)) {
            $this->view->app_photo_full = DOCUMENT_URL . $photo[0]['ad_filepath'];
            $app_photo = $this->view->baseUrl() . '/thumb.php?s=1&w=100&f=' . DOCUMENT_PATH . $photo[0]['ad_filepath'];
        }
        $this->view->app_photo = $app_photo;

        //COMMUNICATION HISTORY
        if ($applicant['appl_email'] == '') {
            throw new Zend_Exception('Applicant Email is empty');
        }

        $comm_history = $commDB->getAllCommunication($applicant['appl_email']);

        $this->view->comm_history = $comm_history;

        // Spit them out
        $this->view->applicant = $applicant;
    }

    public function viewAuditAction()
    {

        $this->view->title = "Application Summary - Applicant History";

        $txnId = $this->_getParam('txn_id', 0);
        $this->view->txn_id = $txnId;

        $this->view->tab = 'audit';

        $pid = $this->_getParam('pid', 0);
        $this->view->pid = $pid;

        //MODELS
        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $commDB = new Communication_Model_DbTable_General();

        //txn data
        $txnData = $applicantTxnDB->getTransaction($txnId, 1);
        $appl_id = $txnData['at_appl_id'];
        $transID = $txnData['at_trans_id'];
        $this->view->txnData = $txnData;
        $this->view->session_transactionID = $transID;

        $photo = $appDocumentDB->getDataArrayBySection($transID, 2, 'Passport sized photo');
        $app_photo = $this->view->app_photo_full = '';
        if (!empty($photo)) {
            $this->view->app_photo_full = DOCUMENT_URL . $photo[0]['ad_filepath'];
            $app_photo = $this->view->baseUrl() . '/thumb.php?s=1&w=100&f=' . DOCUMENT_PATH . $photo[0]['ad_filepath'];
        }
        $this->view->app_photo = $app_photo;

        $applicant = $appProfileDB->getData($appl_id, 1);

        //AUDIT LOG
        $auditDB = new App_Model_Audit();
        $audit_history = $auditDB->getDataByUser($txnId);
        $this->view->audit_history = $audit_history;


        // Spit them out
        $this->view->applicant = $applicant;
    }

    public function viewStatusAction()
    {

        $this->view->title = "Application Summary - Status History";

        $txnId = $this->_getParam('txn_id', 0);
        $this->view->txn_id = $txnId;

        $this->view->tab = 'status';

        $pid = $this->_getParam('pid', 0);
        $this->view->pid = $pid;

        //MODELS
        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $commDB = new Communication_Model_DbTable_General();

        //txn data
        $txnData = $applicantTxnDB->getTransaction($txnId, 1);
        $appl_id = $txnData['at_appl_id'];
        $transID = $txnData['at_trans_id'];
        $this->view->txnData = $txnData;
        $this->view->session_transactionID = $transID;

        $photo = $appDocumentDB->getDataArrayBySection($transID, 2, 'Passport sized photo');
        $app_photo = $this->view->app_photo_full = '';
        if (!empty($photo)) {
            $this->view->app_photo_full = DOCUMENT_URL . $photo[0]['ad_filepath'];
            $app_photo = $this->view->baseUrl() . '/thumb.php?s=1&w=100&f=' . DOCUMENT_PATH . $photo[0]['ad_filepath'];
        }
        $this->view->app_photo = $app_photo;

        $applicant = $appProfileDB->getData($appl_id, 1);

        //STATUS STUFF
        //ash_status, ash_oldStatus
        $db = getDb();

        $select = $db->select()
            ->from(array('a' => 'applicant_status_history'))
            ->joinLeft(array("as" => "registry_values"), 'as.id=a.ash_status', array('StatusName' => 'as.name'))
            ->joinLeft(array("aso" => "registry_values"), 'aso.id=a.ash_oldStatus', array('OldStatusName' => 'aso.name'))
            ->joinLeft(array("b" => "tbl_user"), "a.ash_updUser = b.iduser", array('User' => 'loginName'))
            ->joinLeft(array("c" => "applicant_profile"), "a.ash_updUser = c.appl_id", array('UserStudent' => 'concat(appl_fname, " ", appl_lname)'))
            ->where("a.ash_trans_id = ?", $txnId);
        $results = $db->fetchAll($select);
        //var_dump($results); exit;
        // Spit them out
        $this->view->results = $results;
        $this->view->applicant = $applicant;
    }

    public function viewChecklistAction()
    {
        $this->view->title = "Application Summary - Checklist History";

        $txnId = $this->_getParam('txn_id', 0);
        $this->view->txn_id = $txnId;

        $this->view->tab = 'checklist';

        $pid = $this->_getParam('pid', 0);
        $this->view->pid = $pid;

        //MODELS
        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $checklistHisDB = new Application_Model_DbTable_ChecklistHistory();

        //txn data
        $txnData = $applicantTxnDB->getTransaction($txnId, 1);
        $appl_id = $txnData['at_appl_id'];
        $transID = $txnData['at_trans_id'];
        $this->view->txnData = $txnData;
        $this->view->session_transactionID = $transID;

        $photo = $appDocumentDB->getDataArrayBySection($transID, 2, 'Passport sized photo');
        $app_photo = $this->view->app_photo_full = '';
        if (!empty($photo)) {
            $this->view->app_photo_full = DOCUMENT_URL . $photo[0]['ad_filepath'];
            $app_photo = $this->view->baseUrl() . '/thumb.php?s=1&w=100&f=' . DOCUMENT_PATH . $photo[0]['ad_filepath'];
        }
        $this->view->app_photo = $app_photo;

        $applicant = $appProfileDB->getData($appl_id, 1);

        $this->view->applicant = $applicant;

        //checklist history part
        $historyList = $checklistHisDB->getHistoryChecklist($txnId);
        $this->view->historyList = $historyList;
        //var_dump($historyList); exit;
    }

    public function loadDocumentChecklistHistoryAction()
    {
        $adhId = $this->_getParam('id', 0);
        $txnId = $this->_getParam('txnId', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $model = new Application_Model_DbTable_ChecklistHistory();

        $checklistList = $model->getViewChecklistHistory($adhId);
        //var_dump($checklistList);
        if ($checklistList) {
            $i = 0;
            foreach ($checklistList as $checklistLoop) {
                $getStatus = $model->getChecklistStatus($checklistLoop['adc_id']);

                if ($getStatus) {
                    $j = 0;
                    foreach ($getStatus as $statusLoop) {
                        if ($statusLoop['adt_comment'] == '' || $statusLoop['adt_comment'] == null) {
                            $statusLoop['adt_comment'] = '-';
                        }

                        $upload = $model->getUploadHistory($adhId, $statusLoop['adt_adc_id'], $statusLoop['adt_txn_id'], $statusLoop['adt_dcl_id'], $statusLoop['adt_section_id'], $statusLoop['adt_table_id']);

                        if ($upload) {
                            $checklistList[$i]['checklist_upload_status'][$j]['uploadstatus'] = 'Uploaded';
                            $k = 0;
                            foreach ($upload as $uploadLoop) {
                                $checklistList[$i]['checklist_upload_info'][$j]['uploadinfo'][$k] = $uploadLoop;
                                $k++;
                            }
                        } else {
                            $checklistList[$i]['checklist_upload_status'][$j]['uploadstatus'] = 'Not Upload';
                        }

                        $checklistList[$i]['checklist_status_info'][$j] = $statusLoop;

                        switch ($statusLoop['adt_status']) {
                            case 0:
                                $checklistList[$i]['checklist_status_info'][$j]['adt_status'] = '-';
                                break;
                            case 1:
                                $checklistList[$i]['checklist_status_info'][$j]['adt_status'] = 'New';
                                break;
                            case 2:
                                $checklistList[$i]['checklist_status_info'][$j]['adt_status'] = 'Viewed';
                                break;
                            case 3:
                                $checklistList[$i]['checklist_status_info'][$j]['adt_status'] = 'Incomplete';
                                break;
                            case 4:
                                $checklistList[$i]['checklist_status_info'][$j]['adt_status'] = 'Complete';
                                break;
                            case 5:
                                $checklistList[$i]['checklist_status_info'][$j]['adt_status'] = 'Not Applicable';
                                break;
                            default:
                                $checklistList[$i]['checklist_status_info'][$j]['adt_status'] = '-';
                                break;
                        }

                        $j++;
                    }
                }
                $i++;
            }
        }

        $json = Zend_Json::encode($checklistList);
        echo $json;
        exit;
    }

    public function accountAction()
    {
        $this->view->title = "Account Statement";

        $txnId = $this->view->txn_id = $this->_getParam('id', 0);
        $this->view->tab = 'account';

        //proforma invoice test
        //$invoiceClass = new icampus_Function_Studentfinance_Invoice();
        //$invoiceClass->generateApplicantProformaInvoice(1418,  $level=0, $semester_id=null, $invoice_desc=null);
        //echo 'test';
        //exit;

        $transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();

        //MODELS
        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $commDB = new Communication_Model_DbTable_General();

        //txn data
        $txnData = $applicantTxnDB->getTransaction($txnId, 1);
        $appl_id = $txnData['at_appl_id'];
        $transID = $txnData['at_trans_id'];

        $this->view->txnData = $txnData;
        $this->view->session_transactionID = $transID;
        //var_dump($txnData); exit;
        //applicant info

        $photo = $appDocumentDB->getDataArrayBySection($transID, 2, 'Passport sized photo');
        $app_photo = $this->view->app_photo_full = '';
        if (!empty($photo)) {
            $this->view->app_photo_full = DOCUMENT_URL . $photo[0]['ad_filepath'];
            $app_photo = $this->view->baseUrl() . '/thumb.php?s=1&w=100&f=' . DOCUMENT_PATH . $photo[0]['ad_filepath'];
        }
        $this->view->app_photo = $app_photo;

        $applicant = $appProfileDB->getData($appl_id, 1);

        //ACCOUNT STUFF
        //profile
        $applicantProfileDb = new App_Model_Application_DbTable_ApplicantProfile();
        $this->view->profile = $applicantProfileDb->getData($appl_id);

        //account
        $db = Zend_Db_Table::getDefaultAdapter();


        /*$select_currency = $db->select()
							->from(array('c'=>'tbl_currency'));
		$row_currency = $db->fetchAll($select_currency);
		$this->view->currency_list = $row_currency;		*/


        $select_invoice = $db->select()
            ->from(array('im' => 'proforma_invoice_main'), array(
                    'id'          => 'im.id',
                    'record_date' => 'im.date_create',
                    'description' => 'fc.fc_desc',
                    'txn_type'    => new Zend_Db_Expr ('"Invoice"'),
                    'debit'       => 'bill_amount',
                    'credit'      => new Zend_Db_Expr ('"0.00"'),
                    'document'    => 'bill_number',
                    'invoice_no'  => 'bill_number',
                    'receipt_no'  => new Zend_Db_Expr ('"-"'),
                    'fc_seq'      => 'fc.fc_seq'
                )
            )
            ->join(array('fc' => 'tbl_fee_category'), 'fc.fc_id=im.fee_category', array())
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=im.currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            //->where('im.currency_id= ?',$currency)
            ->where('im.appl_id = ?', $appl_id)
            ->where('im.trans_id = ?', $transID)
            ->where("im.status IN ('A','W')");


        $select_payment = $db->select()
            ->from(
                array('pm' => 'receipt'), array(
                    'id'          => 'pm.rcp_id',
                    'record_date' => 'pm.rcp_receive_date',
                    'description' => 'pm.rcp_description',
                    'txn_type'    => new Zend_Db_Expr ('"Payment"'),
                    'debit'       => new Zend_Db_Expr ('"0.00"'),
                    'credit'      => 'rcp_amount',
                    'document'    => 'rcp_no',
                    'invoice_no'  => 'rcp_no',
                    'receipt_no'  => 'rcp_no',
                    'fc_seq'      => 'c.cur_id'
                )
            )
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=pm.rcp_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
            //->where('pm.rcp_cur_id= ?',$currency)
            ->where("pm.rcp_status = 'APPROVE'")
            ->where('pm.rcp_trans_id = ?', $transID);


        //get array payment
        $select = $db->select()
            ->union(array($select_invoice, $select_payment), Zend_Db_Select::SQL_UNION_ALL)
            ->order("record_date asc")
            ->order("txn_type asc");

        $results = $db->fetchAll($select);

        if (!$results) {
            $results = null;
        }

        $this->view->account = $results;

        //process data
        $amountCurr = array();
        $curencyArray = array();
        $balance = 0;
        if ($results) {
            foreach ($results as $row) {
                $curencyArray[$row['cur_id']] = $row['cur_code'];

                $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
                //$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
                $dataCur = $curRateDB->getRateByDate(2, $row['record_date']);

                if ($row['cur_id'] == 2) {
                    $amountDebit = round($row['debit'] * $dataCur['cr_exchange_rate'], 2);
                    $amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'], 2);
                } else {
                    $amountDebit = $row['debit'];
                    $amountCredit = $row['credit'];
                }


                if ($row['txn_type'] == "Invoice") {
                    $balance = $balance + $amountDebit;
                } else
                    if ($row['txn_type'] == "Payment") {

                        if ($row['cur_id'] == 2) {
                            $amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'], 2);
                        } else {
                            $amountCredit = $row['credit'];
                        }

                        $balance = $balance - $amountCredit;

                    } else
                        if ($row['txn_type'] == "Credit Note") {
                            $balance = $balance - $row['credit'];
                        } else
                            if ($row['txn_type'] == "Debit Note") {
                                $balance = $balance + $row['debit'];
                            } else
                                if ($row['txn_type'] == "Refund") {
                                    $balance = $balance + $row['debit'];
                                }


                $amountCurr[$row['invoice_no']]['MYR']['debit'] = $amountDebit;
                $amountCurr[$row['invoice_no']]['MYR']['credit'] = $amountCredit;
                $amountCurr[$row['invoice_no']]['MYR']['balance'] = number_format($balance, 2, '.', ',');

            }
        }


//		    $curAvailable = array_unique($curencyArray);
//		    echo "<pre>";
//			print_r($curAvailable);

        $curAvailable = array('2' => 'USD', '1' => 'MYR');

        $this->view->availableCurrency = $curAvailable;
        $this->view->amountCurr = $amountCurr;
    }

    /*
		Random Function
	*/
    public function ifEmpty($val = '')
    {
        if ($val == '' || $val == 0) {
            return '';
        } else {
            return $val;
        }
    }

    public function calcAge($date)
    {
        $from = new DateTime($date);
        $to = new DateTime('today');

        return $from->diff($to)->y;
    }

    public function getValueById($id, $data, $map)
    {
        if (!empty($data)) {
            foreach ($data as $row) {
                if ($row[$map['k']] == $id) {
                    return $row[$map['v']];
                }
            }
        }
    }

    public function getRealValueByDef($id, $data)
    {
        //changed to use the function above, too lazy to remove
        return $this->getValueById($id, $data, array('k' => 'idDefinition', 'v' => 'DefinitionDesc'));
    }

    //amazing function
    public function getBooleanText($what)
    {
        return $what == 0 ? $this->view->translate('No') : $this->view->translate('Yes');
    }

    protected function cleanSectionItem($items, $sectionid)
    {
        $newitems = array();

        switch ($sectionid) {
            // Personal Details
            case 2:
                $ignorefields = array('studentID', 'password', 'replicate_address_option');

                foreach ($items as $item) {
                    if (!in_array($item['variable'], $ignorefields)) {
                        $newitems[] = $item;
                    }
                }

                $items = $newitems;
                break;

        }

        return $items;
    }

    function getStateByCountryId($state_id, $country_id, $others = '')
    {
        if (empty($country_id)) {
            return '';
        }

        if ($state_id == 99) {
            return $others;
        }

        $db = getDB();
        $select = $db->select()
            ->from(array('s' => 'tbl_state'))
            ->where('s.idCountry = ?', $country_id)
            ->where('s.idState = ?', $state_id)
            ->order('s.StateName ASC');
        $row = $db->fetchRow($select);

        if (!empty($row)) {
            return $row['StateName'];
        } else {
            return '';
        }
    }

    function getStateById($state_id)
    {

        $db = getDB();
        $select = $db->select()
            ->from(array('s' => 'tbl_state'))
            ->where('s.idState = ?', $state_id)
            ->order('s.StateName ASC');
        $row = $db->fetchRow($select);

        if (!empty($row)) {
            return $row['StateName'];
        } else {
            return '';
        }
    }

    public function txnDetailAction()
    {
        echo '<pre>';
        $this->_helper->layout->disableLayout();

        $txnId = $this->_getParam('txn_id', 0);
        $this->view->txn_id = $txnId;

        $pid = $this->_getParam('pid', 0);
        $this->view->pid = $pid;

        //echo 'section tagging:';
        $section_id = $this->_getParam('section_id', 0);
        $this->view->section_id = $section_id;


        $appInitConfigDB = new Application_Model_DbTable_ApplicationInitialConfig();
        $sectionData = $appInitConfigDB->fnGetDefination($section_id);
        $mainsectionid = $sectionData['sectionID'];
        $this->view->mainsectionid = $mainsectionid;
        $this->view->title = $sectionData['name'];

        $registry = Zend_Registry::getInstance();
        $locale = $registry->get('Zend_Locale');

        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $appTransactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
        $appQualificationDB = new App_Model_Application_DbTable_ApplicantQualification();
        $appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();
        $appHealthDB = new App_Model_Application_DbTable_ApplicantHealthCondition();
        $appFinanacialDB = new App_Model_Application_DbTable_ApplicantFinancial();
        $appWorkExpDB = new App_Model_Application_DbTable_ApplicantWorkingExperience();
        $appvisaDB = new App_Model_Application_DbTable_ApplicantVisa();

        $objdeftypeDB = new App_Model_Definitiontype();
        $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $entryReqDB = new App_Model_Application_DbTable_EntryRequirement();
        $programDB = new App_Model_Record_DbTable_Program();
        $intakeDB = new App_Model_Record_DbTable_Intake();
        $countryDB = new App_Model_General_DbTable_Country();
        $institutionDB = new App_Model_Application_DbTable_Institution();


        //form declaration
        $form = new Application_Form_Main(array('lang' => $locale, 'program' => $program['program_id'], 'section' => $section_id));

        if ($mainsectionid == 1) {
            $applicant = array();
        }

        //profile
        if ($mainsectionid == 2) {
            $profile = $appProfileDB->getData($appl_id);
            $this->view->profile = $profile;
            $this->view->pid = $appl_id;


            $applicant['prev_student'] = $profile['prev_student'];
            $applicant['salutation'] = $profile['appl_salutation'];
            $applicant['first_name'] = $profile['appl_fname'];
            $applicant['last_name'] = $profile['appl_lname'];
            $applicant['idNo'] = $profile['appl_idnumber'];
            $applicant['typeID'] = $profile['appl_idnumber_type'];
            $applicant['passport_expiry'] = $profile['appl_passport_expiry'];
            $applicant['email'] = $profile['appl_email'];
            $applicant['dob'] = $profile['appl_dob'];
            $applicant['gender'] = $profile['appl_gender'];
            $applicant['nationality'] = $profile['appl_nationality'];

            $applicant['religion'] = $profile['appl_religion'];
            $applicant['race'] = $profile['appl_race'];
            $applicant['marital_status'] = $profile['appl_marital_status'];
            $applicant['bumiputra_eligibility'] = $profile['appl_bumiputera'];

            $applicant['address_perm1'] = $profile['appl_address1'];
            $applicant['address_perm2'] = $profile['appl_address2'];
            $applicant['address_perm3'] = $profile['appl_address3'];
            $applicant['postcode_perm'] = $profile['appl_postcode'];
            $applicant['city_perm'] = $profile['appl_city'];
            $applicant['state_perm'] = $profile['appl_state'];
            $applicant['country_perm'] = $profile['appl_country'];
            $applicant['home_perm'] = $profile['appl_phone_home'];
            $applicant['mobile_perm'] = $profile['appl_phone_mobile'];
            $applicant['office_perm'] = $profile['appl_phone_office'];
            $applicant['fax_perm'] = $profile['appl_fax'];

            $applicant['address_corr1'] = $profile['appl_caddress1'];
            $applicant['address_corr2'] = $profile['appl_caddress2'];
            $applicant['address_corr3'] = $profile['appl_caddress3'];
            $applicant['postcode_corr'] = $profile['appl_cpostcode'];
            $applicant['city_corr'] = $profile['appl_ccity'];
            $applicant['state_corr'] = $profile['appl_cstate'];
            $applicant['country_corr'] = $profile['appl_ccountry'];
            $applicant['home_corr'] = $profile['appl_cphone_home'];
            $applicant['mobile_corr'] = $profile['appl_cphone_mobile'];
            $applicant['office_corr'] = $profile['appl_cphone_office'];
            $applicant['fax-corr'] = $profile['appl_cfax'];

            $applicant['phone_home'] = $profile['appl_contact_home'];
            $applicant['phone_mobile'] = $profile['appl_contact_mobile'];
            $applicant['phone_office'] = $profile['appl_contact_office'];


        }

        //education details :section 3
        if ($mainsectionid == 3) {

            //1st:track from current profile data exist or not
            $appQualification = $appQualificationDB->getTransData($appl_id, $txnId);

            //get documents
            if (count($appQualification) > 0) {
                foreach ($appQualification as $index => $q) {
                    $documents = $appDocumentDB->getDataArray($txnId, $q['ae_id'], 'applicant_qualification');
                    $appQualification[$index]['documents'] = $documents;
                }
            }

            $this->view->qualification = $appQualification;

            if ($pid) {
                $q = $appQualificationDB->getQualification($pid);

                $applicant['qualification'] = $q['ae_qualification'];
                $applicant['degree_awarded'] = $q['ae_degree_awarded'];
                $applicant['class_degree'] = $q['ae_class_degree'];
                $applicant['result'] = $q['ae_result'];
                $applicant['year_graduated'] = $q['ae_year_graduate'];
                $applicant['institution_name'] = $q['ae_institution'];
                $applicant['medium_instruction'] = $q['ae_medium_instruction'];

            } else {
                $applicant = array();
            }
        }


        //english proficiency : idsection 4
        if ($mainsectionid == 4) {

            $appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
            $engprof = $appEngProfDB->getData($txnId);

            $app_documents = $appDocumentDB->getDataArrayBySection($txnId, $section_id);
            $this->view->app_docs = $app_documents;

            $this->view->pid = $engprof['ep_id'];
            $applicant['english_test'] = $engprof['ep_test'];
            $applicant['english_test_detail'] = $engprof['ep_test_detail'];
            $applicant['english_date_taken'] = $engprof['ep_date_taken'];
            $applicant['english_score'] = $engprof['ep_score'];

        }


        //quantitative proficiency
        if ($mainsectionid == 5) {

            $qpDB = new App_Model_Application_DbTable_ApplicantQuantitativeProficiency();
            $quanprof_list = $qpDB->getTransData($txnId);

            //get documents
            foreach ($quanprof_list as $index => $q) {

                $documents = $appDocumentDB->getDataArray($txnId, $q['qp_id'], 'applicant_quantitative_proficiency');
                $quanprof_list[$index]['documents'] = $documents;
            }

            $this->view->qp_list = $quanprof_list;
            if ($pid) {
                $applicant = $qpDB->getData($pid);
            } else {
                $applicant = array();
            }

        }

        //research publication details
        if ($mainsectionid == 6) {


            $rpdDB = new App_Model_Application_DbTable_ApplicantResearchPublication();
            $research_list = $rpdDB->getTransData($txnId);
            //get documents
            foreach ($research_list as $index => $r) {

                $documents = $appDocumentDB->getDataArray($txnId, $r['rpd_id'], 'applicant_research_publication');
                $research_list[$index]['documents'] = $documents;
            }
            $this->view->rpd_list = $research_list;
            $this->view->total_added = count($research_list);


            if ($pid) {
                $applicant = $rpdDB->getData($pid);
            } else {
                $applicant = array();
            }
        }

        //employment
        if ($mainsectionid == 7) {

            $appEmployment = $appEmploymentDB->getTransData($appl_id, $txnId);

            if (count($appEmployment) > 0) {
                foreach ($appEmployment as $index => $emp) {
                    $documents = $appDocumentDB->getDataArray($txnId, $emp['ae_id'], 'applicant_employment');
                    $appEmployment[$index]['documents'] = $documents;
                }
            }
            $this->view->appEmployment = $appEmployment;
            $this->view->positionLevel = $objdeftypeDB->fnGetDefinations('Position Level');

            /*$table = "<table border='1'>
							<tr>
								<td colspan='2'>".$this->view->translate('Position Level')."</td>
								<td>".$this->view->translate('Description')."</td>
							</tr>";

							foreach($positionLevel as $pl){
							$table .=	"<tr><td>";
						    $table .=   $form->emp_position->addMultiOption($pl['idDefinition']);
							$table .=	"</td>
										<td>".$pl['DefinitionDesc']."</td>
										<td>".$pl['Description']."</td>
										</tr>";
									}
					$table .=	"</table>";
					$this->view->table = $table;*/

            if ($pid) {
                $emp = $appEmploymentDB->getEmployement($pid);
                $applicant['emply_status'] = $emp['ae_status'];
                $applicant['emply_comp_name'] = $emp['ae_comp_name'];
                $applicant['emply_comp_address'] = $emp['ae_comp_address'];
                $applicant['emply_comp_phone'] = $emp['ae_comp_phone'];
                $applicant['emply_comp_fax'] = $emp['ae_comp_fax'];
                $applicant['emply_designation'] = $emp['ae_designation'];
                $applicant['emply_position_level'] = $emp['ae_position'];
                $applicant['emply_duration_from'] = $emp['ae_from'];
                $applicant['emply_duration_to'] = $emp['ae_to'];
                $applicant['emply_industry'] = $emp['ae_industry'];
                $applicant['job_desc'] = $emp['ae_job_desc'];
                $this->view->position = $emp['ae_position'];
            } else {
                $applicant = array();
            }
        }

        //working experience
        if ($mainsectionid == 8) {

            $industryWorkingExperience = $objdeftypeDB->fnGetDefinations('Industry Working Experience');
            $this->view->industryWorkingExperience = $industryWorkingExperience;

            $this->view->yearofIndustry = $objdeftypeDB->fnGetDefinations('Years of Industry');
            $applicant = array();
        }


        //health condition
        if ($mainsectionid == 9) {

            $result = $objdeftypeDB->fnGetDefinations('Health Condition');

            foreach ($result as $index => $r) {
                //get data
                $appHealth = $appHealthDB->getRowData($txnId, $r['idDefinition']);

                if (isset($appHealth) && $appHealth['ah_status'] != '') {
                    $applicant['health_condition'][$index] = $appHealth['ah_status'];
                }
            }

        }

        //financial particulars :section 10
        if ($mainsectionid == 10) {
            $appFinancial = $appFinanacialDB->getDataByTxn($txnId);
            $this->view->appFinancial = $appFinancial;

            if (isset($appFinancial) && $appFinancial['af_id'] != '') {
                $this->view->app_docs = $appDocumentDB->getDataArray($txnId, $appFinancial['af_id'], 'applicant_financial');
            }

            $this->view->pid = $appFinancial['af_id'];
            $applicant['fin_method'] = $appFinancial['af_method'];
            $applicant['fin_sponsor_name'] = $appFinancial['af_sponsor_name'];
            $applicant['fin_type_scholarship'] = $appFinancial['af_type_scholarship'];
            $applicant['fin_scholarship_secured'] = $appFinancial['af_scholarship_secured'];
            $applicant['fin_scholarship_apply'] = $appFinancial['af_scholarship_apply'];
            $applicant['file_fin_resume'] = $appFinancial['af_file'];
        }


        //visa :section 11
        if ($mainsectionid == 11) {

            $visa = $appvisaDB->getTransData($txnId);
            $applicant['primary_id'] = $visa['av_id'];
            $applicant['visa_malaysian'] = $visa['av_malaysian_visa'];
            $applicant['visa_status'] = $visa['av_status'];
            $applicant['visa_expiry'] = $visa['av_expiry'];

        }

        //Accomodation
        if ($mainsectionid == 12) {
            exit;
            $accDB = new App_Model_Application_DbTable_ApplicantAccomodation();
            $accomodation = $accDB->getDatabyTxn($txnId);

            $applicant['primary_id'] = $accomodation['acd_id'];
            $applicant['acd_assistance'] = $accomodation['acd_assistance'];
            $form->acd_type->setValue($accomodation['acd_type']);

            $roomTypeDB = new Hostel_Model_DbTable_Hostelroom();
            $room_list = $roomTypeDB->getRoomList(263);
            $form->acd_type->setAttrib('class', 'accomodationtype');
            $table = '<table border=0 width="80%">
		        			<tr>
		        			   <th>' . $this->view->translate("Occupancy Type") . '</th>
		        			   <th>' . $this->view->translate("Room Type") . '</th>
		        			   <th>' . $this->view->translate("Monthly Rate") . '<br>(RM)</th>
		        			   <th>' . $this->view->translate("Tick") . '</th>
		        			</tr>';

            foreach ($room_list as $room) {
                $table .= "<tr>
		        				<td align='center'>" . $room['OccupancyType'] . "</td>
		        				<td align='center'>" . $room['RoomType'] . "</td>
		        				<td align='center'>" . $room['Rate'] . "</td><td  align='center'>";
                $table .= $form->acd_type->addMultiOption($room['IdHostelRoomType']);
                $table .= "</td></tr>";
            }
            $table .= '</table>';

            $this->view->table = $table;
        }


        //initial registration details
        if ($mainsectionid == 13) {

            //print_r($applicant);
            //get subject from landscape
            $landscapeDB = new GeneralSetup_Model_DbTable_Landscapesubject();
            $subject_list = $landscapeDB->getLandscapeSubject($program['program_id'], $program['ap_prog_scheme'], $txnData['at_intake']);

            $table = '<table border=0 width="80%" class="table">
		        			<tr>
		        			   <th>' . $this->view->translate("No") . '</th>
		        			   <th>' . $this->view->translate("Subject Code") . '</th>
		        			   <th>' . $this->view->translate("Subject Name") . '</th>
		        			   <th>' . $this->view->translate("Credit Hour") . '</th>		        			
		        			</tr>';
            foreach ($subject_list as $index => $subject) {
                $no = $index + 1;
                $table .= "<tr>
		        				<td>" . $no . "</td>
		        				<td align='center'>" . $room['SubCode'] . "</td>
		        				<td align='center'>" . $room['SubjectName'] . "</td>
		        				<td align='center'>" . $room['CreditHours'] . "</td>";
                $table .= "</tr>";
            }
            $table .= '</table>';

            $this->view->table = $table;
            $applicant = array();
        }

        //essay
        if ($mainsectionid == 14) {
            $this->view->app_docs = $appDocumentDB->getDataArrayBySection($txnId, $mainsectionid);
            $applicant = array();
        }

        //statement of research interest
        if ($mainsectionid == 15) {
            $this->view->app_docs = $appDocumentDB->getDataArrayBySection($txnId, $mainsectionid);
            $applicant = array();
        }

        //referee
        if ($mainsectionid == 16) {
            $refereeDB = new App_Model_Application_DbTable_ApplicantReferees();
            $referee = $refereeDB->getData($txnId);
            if ($referee) {
                $applicant = $referee;
                $this->view->pid = $applicant['r_id'];
            }
        }


        if ($mainsectionid == 17) { //declaration
            $applicant = $appProfileDB->getData($appl_id);
            $applicant['declaration_name'] = $applicant['appl_fname'] . ' ' . $applicant['appl_lname'];
            $applicant['declaration_date'] = date("d-m-Y");

        }


        $listItem = $appInitConfigDB->getItem($program['program_id'], $section_id);
        $this->view->onlineapplicationitem = $listItem;

        //print_r($listItem);
        foreach ($listItem as $list) {

            $disabled = $list['enable'];

            if ($disabled == 0) {
                $form->$list['variable']->setAttrib('disabled', array());
            }

            if ($list['variable'] == 'email') {
                //$form->$list['variable']->setValue($applicant['appl_email']);
            }

            if ($list['form_type'] == 'select' || $list['form_type'] == 'select_others') {

                if ($list['variable'] == 'program') {

                    /*$result = $programDB->getData();
						$form->$list['variable']->addMultiOption('','-- Please Select --');
						foreach($result as $data) {
							$form->$list['variable']->addMultiOption($data['IdProgram'],$data['ProgramName']);
						}

						$form->$list['variable']->setValue($appProgram['ap_prog_code']);
						$form->$list['variable']->setAttrib('disabled', array());*/


                } elseif ($list['variable'] == 'salutation') {
                    $result = $objdeftypeDB->fnGetDefinations('Salutation');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }

                    /*}elseif($list['variable'] == 'program_type'){
						$result = $objdeftypeDB->fnGetDefinations('Programme Type');
						$form->$list['variable']->addMultiOption('','-- Please Select --');
						foreach($result as $data) {
							$form->$list['variable']->addMultiOption($data['idDefinition'],$data['DefinitionDesc']);
						}*/

                } elseif ($list['variable'] == 'intake') {
                    /*$result = $intakeDB->getIntakeOffered($programID,$schemeID,$studentCategoryID);
						$form->$list['variable']->addMultiOption('','-- Please Select --');
						if(count($result)>0){
							foreach($result as $data) {
								$form->$list['variable']->addMultiOption($data['IdIntake'],$data['IntakeDesc']." (".$data['IntakeId'].")");
							}
						}

						$form->$list['variable']->setValue($transaction['at_intake']);*/

                } elseif ($list['variable'] == 'marital_status') {
                    $result = $objdeftypeDB->fnGetDefinations('Marital Status');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }

                } elseif ($list['variable'] == 'religion') {
                    $result = $objdeftypeDB->fnGetDefinations('Religion');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }

                } elseif ($list['variable'] == 'nationality' || $list['variable'] == 'country_perm' || $list['variable'] == 'country_corr' || $list['variable'] == 'r_ref1_country' || $list['variable'] == 'r_ref2_country') {
                    $result = $countryDB->getData();
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idCountry'], $data['CountryName']);
                    }
                    //$form->$list['variable']->setValue($applicant['appl_nationality']);
                    if ($list['variable'] == 'nationality') {
                        $form->$list['variable']->setAttrib('disabled', array());
                    }
                    if ($list['variable'] == 'country_perm') {
                        $form->$list['variable']->setAttrib('onchange', 'getState(this.value,"state_perm")');
                    }
                    if ($list['variable'] == 'country_corr') {
                        $form->$list['variable']->setAttrib('onchange', 'getState(this.value,"state_corr")');
                    }
                    if ($list['variable'] == 'r_ref1_country') {
                        $form->$list['variable']->setAttrib('onchange', 'getState(this.value,"r_ref1_state")');
                    }
                    if ($list['variable'] == 'r_ref2_country') {
                        $form->$list['variable']->setAttrib('onchange', 'getState(this.value,"r_ref2_state")');
                    }


                } elseif ($list['variable'] == 'race') {
                    $result = $objdeftypeDB->fnGetDefinations('Race');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }

                } elseif ($list['variable'] == 'bumiputra_eligibility') {
                    $result = $objdeftypeDB->fnGetDefinations('Bumiputera Eligibility');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }

                } elseif ($list['variable'] == 'class_degree') {
                    $result = $objdeftypeDB->fnGetDefinations('Class Degree');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }

                } elseif ($list['variable'] == 'english_test') {
                    $result = $objdeftypeDB->fnGetDefinations('English Proficiency Test List');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }
                    $form->$list['variable']->setAttrib('onchange', 'getDetail(this.value)');

                } elseif ($list['variable'] == 'emply_status') {
                    $result = $objdeftypeDB->fnGetDefinations('Employment Status');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }

                } elseif ($list['variable'] == 'fin_method') {
                    $result = $objdeftypeDB->fnGetDefinations('Funding Method');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }

                } elseif ($list['variable'] == 'fin_type_scholarship') {
                    $result = $objdeftypeDB->fnGetDefinations('Select Scholarship');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }

                } elseif ($list['variable'] == 'fin_scholarship_apply') {
                    $result = $objdeftypeDB->fnGetDefinations('Apply Scholarship');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }

                } elseif ($list['variable'] == 'visa_status') {
                    $result = $objdeftypeDB->fnGetDefinations('Visa Status');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }

                } elseif ($list['variable'] == 'institution_name') {
                    $result = $institutionDB->getData();
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idInstitution'], $data['InstitutionName']);
                    }

                } elseif ($list['variable'] == 'qualification_id') {
                    $result = $entryReqDB->getListGeneralReq();
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['IdQualification'], $data['QualificationLevel']);
                    }
                    //$form->$list['variable']->setValue($appQualification['ae_qualification']);
                    $form->$list['variable']->setAttrib('disabled', array());

                } elseif ($list['variable'] == 'emply_industry') {
                    $result = $objdeftypeDB->fnGetDefinations('Industry Working Experience');
                    $form->$list['variable']->addMultiOption('', '-- Please Select --');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }

                }


            } elseif ($list['form_type'] == 'radio') {


                if ($list['variable'] == 'prev_student') {

                    $form->$list['variable']->setMultiOptions(array(
                        '1' => ' ' . $this->view->translate('Yes'),
                        '0' => ' ' . $this->view->translate('No')));

                    $form->$list['variable']->setValue(0);


                } elseif ($list['variable'] == 'gender') {
                    $form->$list['variable']->setMultiOptions(array(
                        '1' => ' ' . $this->view->translate('Male'),
                        '2' => ' ' . $this->view->translate('Female')));

                } elseif ($list['variable'] == 'typeID') {
                    $result = $objdeftypeDB->fnGetDefinations('ID Type');
                    $dataArray = array();
                    foreach ($result as $data) {
                        $dataArray[$data['idDefinition']] = $data['DefinitionDesc'];
                    }
                    $form->$list['variable']->setMultiOptions($dataArray);

                } elseif ($list['variable'] == 'visa_malaysian') {
                    $form->$list['variable']->setMultiOptions(array(
                        '1' => ' ' . $this->view->translate('Yes'),
                        '0' => ' ' . $this->view->translate('No')));

                } elseif ($list['variable'] == 'acd_assistance') {

                    $form->$list['variable']->setMultiOptions(array(
                        '1' => ' ' . $this->view->translate('Yes'),
                        '0' => ' ' . $this->view->translate('No')));

                    $form->$list['variable']->setValue(0);
                    $form->$list['variable']->setAttrib('onclick', 'enableAccomodation(this.value)');


                }

                $form->$list['variable']->setSeparator('<br>');

            } elseif ($list['form_type'] == 'checkbox') {

                if ($list['variable'] == 'health_condition') {

                    $result = $objdeftypeDB->fnGetDefinations('Health Condition');
                    foreach ($result as $data) {
                        $form->$list['variable']->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                    }
                    $form->$list['variable']->setSeparator('<br>');
                }


            }
        }
        if (!$applicant) {
            $applicant = array();
        }
        $form->populate($applicant);

        $this->view->applicant = $applicant;
        $this->view->form = $form;

    }

    public function updateApplicationAction()
    {
        //not used
    }

    public function viewDocumentAction()
    {

        $this->view->title = "Document Checklist";

        $txnId = $this->_getParam('txn_id', 0);
        $this->view->txn_id = $txnId;

        $scheme = $this->_getParam('scheme', 0);
        $this->view->scheme = $scheme;

        $student_category = $this->_getParam('sc', 0);
        $this->view->student_category = $student_category;

        $documentChecklistDB = new Application_Model_DbTable_DocumentChecklist();
        $document_list = $documentChecklistDB->getDclSearchList(array('field2' => $scheme, 'field4' => $student_category));

        $appDocStatusDB = new Application_Model_DbTable_ApplicantDocumentStatus();
        foreach ($document_list as $index => $doc) {

            //get status document
            $doc_status = $appDocStatusDB->getApplicantDocumentStatus($txnId, $doc['dcl_Id']);
            $document_list[$index]['doc_status'] = $doc_status;
        }
        $this->view->document_list = $document_list;
        //echo '<pre>';
        //print_r($document_list);
    }

    function upload($file, $idSection, $tbl_id, $tbl_name = null, $txn_id)
    {

        $auth = Zend_Auth::getInstance();

        //get transaction repository path
        $appTransaction = new App_Model_Application_DbTable_ApplicantTransaction();
        $txn_data = $appTransaction->getTransactionData($txn_id);


        $files = (is_array($file)) ? $file : array();

        if (!is_dir(DOCUMENT_PATH . $txn_data['at_repository'])) {
            mkdir_p(DOCUMENT_PATH . $txn_data['at_repository']);
        }
        //var_dump($files); exit;
        foreach ($files as $name => $attributes) {
            list($var, $dcl_id) = explode("-", $name);
            for ($i = 0; $i < count($file[$name]); $i++) {
                //var_dump($file[$name]['tmp_name'][$i]);
                if (is_uploaded_file($file[$name]['tmp_name'][$i])) {

                    $ori_filename = str_replace(' ', '', strtolower($file[$name]["name"][$i]));
                    $rename_filename = date('Ymdhs') . "_" . $var . "_" . $ori_filename;
                    $path_file = $txn_data['at_repository'] . '/' . $rename_filename;
                    move_uploaded_file($file[$name]['tmp_name'][$i], DOCUMENT_PATH . $path_file);

                    $upload_file = array(
                        'ad_txn_id'       => $txn_id,
                        'ad_dcl_id'       => $dcl_id,
                        'ad_section_id'   => $idSection,
                        'ad_table_name'   => $tbl_name,
                        'ad_table_id'     => $tbl_id,
                        'ad_filename'     => $rename_filename,
                        'ad_ori_filename' => $ori_filename,
                        'ad_filepath'     => $path_file,
                        'ad_createddt'    => date("Y-m-d h:i:s"),
                        'ad_createdby'    => $auth->getIdentity()->iduser,
                        'ad_role'         => $auth->getIdentity()->IdRole
                    );
                    //print_r($upload_file);
                    $uploadfileDB = new App_Model_Application_DbTable_ApplicantDocument();
                    $uploadfileDB->addData($upload_file);

                    //audit compare
                    $auditDB = new App_Model_Audit();
                    $log_audit = array(array(
                        'type'       => 'upload',
                        'variable'   => '',
                        'value'      => $path_file,
                        'prev_value' => ''
                    ));

                    if (!empty($log_audit)) {
                        $auditDB->recordLog($this->section_name, $idSection, $txn_id, $log_audit, $auth->getIdentity()->iduser, 'application');
                    }

                }//end if is_uploaded

            }//end for

        }//enf oreach
        //exit;
    }


    public function listAction()
    {

        $this->view->title = "Online Application (Student Application)";

        $studentDB = new App_Model_Record_DbTable_Student();


        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $student_data = $studentDB->searchList($formData);

            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_data));
            $paginator->setItemCountPerPage(100);
            $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        } else {
            $student_data = $studentDB->getPaginateData(1);
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($student_data));
            $paginator->setItemCountPerPage(10);
            $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        }

        $this->view->paginator = $paginator;


    }

    public function addAction()
    {
        //title
        $this->view->title = "Add New Applicant";

        $form = new Application_Form_Manual();
        $emailDB = new App_Model_Email();

        //program
        $programDB = new App_Model_Record_DbTable_Program();
        $this->view->list_program = $list_program = $programDB->getActiveProgram();


        $elements = array(
            'firstname' => array(
                'form_type' => 'text',
                'variable'  => 'firstname',
                'class'     => 'input-txt reqfield',
                'label'     => 'First Name'
            ),

            'lastname' => array(
                'form_type' => 'text',
                'variable'  => 'lastname',
                'class'     => 'input-txt reqfield',
                'label'     => 'Last Name'
            ),
            'email'    => array(
                'form_type' => 'text',
                'variable'  => 'email',
                'class'     => 'input-txt reqfield',
                'label'     => 'Email'
            ),
            'password' => array(
                'form_type' => 'text',
                'variable'  => 'password',
                'class'     => 'input-txt reqfield',
                'label'     => 'Password'
            ),
            'program'  => array(
                'form_type' => 'select',
                'variable'  => 'program',
                'class'     => 'select',
                'label'     => 'Programme'
            ),
            'template' => array(
                'form_type' => 'select',
                'variable'  => 'template',
                'class'     => 'select',
                'label'     => 'Email Template'
            )


        );


        $applicant = array();

        foreach ($elements as $elem) {
            $form->addElement($elem['form_type'], $elem['variable'], array(
                'class'      => $elem['class'],
                'decorators' => array(
                    'ViewHelper',
                    'Description',
                    'Errors'),
            ));

            $form->$elem['variable']->removeDecorator('Errors');
            $form->$elem['variable']->removeDecorator('HtmlTag');
            $form->$elem['variable']->removeDecorator('Label');
        }


        //password
        $form->password->setValue($this->randomizer(6));


        //options
        foreach ($this->view->list_program as $data) {
            $form->program->addMultiOption($data['IdProgram'], $data['ProgramName']);
        }

        //template
        $commtplDB = new Communication_Model_DbTable_Template();
        $tpllist = $commtplDB->getTemplatesByCategory('application', 'newapplicant');
        foreach ($tpllist as $tpl) {
            $form->template->addMultiOption($tpl['tpl_id'], $tpl['tpl_name']);
        }

        //view
        $this->view->elements = $elements;
        $this->view->form = $form;
        $this->view->applicant = $applicant;

        //
        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {

                //check email

                //insert student profile into applicant_profile
                $appl_info['appl_fname'] = strtoupper($formData['firstname']);
                $appl_info['appl_lname'] = strtoupper($formData['lastname']);
                $appl_info["branch_id"] = 1;
                $appl_info["appl_email"] = $formData['email'];
                $appl_info["appl_password"] = md5($formData["password"]);
                $appl_info["appl_role"] = 0;
                $appl_info["create_date"] = date("Y-m-d H:i:s");

                $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();

                //check email
                $check = $appProfileDB->uniqueEmail($formData['email']);

                $studentExists = 0;
                if ($check === false) {
                    //$this->view->errorMsg = $this->view->translate('Email address already exists.');
                    //$form->populate($formData);
                    $studentExists = 1;
                }

                if (!defined('ONAPP_URL')) {
                    throw new Zend_Exception('You did not set ONAPP_URL in your config file');
                }

                //send email
                $fullname = $formData['firstname'] . ' ' . $formData['lastname'];

                foreach ($list_program as $prog) {
                    if ($prog['IdProgram'] == $formData['program']) {
                        $program_name = $prog['ProgramName'];
                    }
                }

                $tpl = $emailDB->getTemplate($formData['template'], 'tpl_id');
                $tpl_content = str_replace(
                    array('[Email]', '[Password]', '[Url]', '[Program]', '[Name]'),
                    array($formData['email'], $formData['password'], ONAPP_URL, $program_name, $fullname),
                    $tpl['tpl_content']
                );


                //create profile
                if ($studentExists == 0) {
                    $applicant_id = $appProfileDB->addData($appl_info);
                } else {
                    $profile = $appProfileDB->getDataBy($formData['email'], 'appl_email');
                    $applicant_id = $profile['appl_id'];
                }

                //generate applicant ID
                $gen_applicantId = $this->generateApplicantID();

                //create new txn
                $info2["at_appl_id"] = $applicant_id;
                $info2["at_pes_id"] = $gen_applicantId;
                $info2["at_create_by"] = $applicant_id;
                $info2["at_status"] = 591;
                $info2["at_create_date"] = date("Y-m-d H:i:s");
                $info2["entry_type"] = 0;//manual
                $info2["at_default_qlevel"] = null;

                $appTransactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
                $at_trans_id = $appTransactionDB->addData($info2);

                //make directory for repository purpose
                $applicant_path = DOCUMENT_PATH . '/applicant/' . date('Ym');


                //create directory to locate fisle
                if (!is_dir($applicant_path)) {
                    mkdir_p($applicant_path, 0775);
                }

                $txn_path = $applicant_path . "/" . $at_trans_id;

                //create directory to locate file
                if (!is_dir($txn_path)) {
                    mkdir_p($txn_path, 0775);
                }

                //insert applicant_program
                $programDB = new App_Model_Record_DbTable_Program();
                $program = $programDB->getData($formData["program"]);
                $info3["ap_at_trans_id"] = $at_trans_id;
                $info3["ap_prog_code"] = $program["ProgramCode"];
                $info3["ap_prog_id"] = $formData["program"];
                $appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
                $appProgramDB->addData($info3);

                //update repository info
                $rep['at_repository'] = '/applicant/' . date('Ym') . '/' . $at_trans_id;
                $appTransactionDB->updateData($rep, $at_trans_id);


                $dataEmail = array(
                    'recepient_email' => $formData['email'],
                    'subject'         => $tpl['tpl_name'],
                    'content'         => $tpl_content,
                    'date_que'        => date('Y-m-d H:i:s')
                );

                $emailDB->add($dataEmail);


                $content = $this->view->translate('Applicant successfully invited.');
                $redirect = $this->view->baseUrl() . '/application/index/';

                $this->SuccessPage($content, $redirect);
            }


        }

        //$this->_helper->layout->disableLayout();
        $this->view->form = $form;
    }

    function generateApplicantID()
    {

        //generate applicant ID
        $applicantid_format = array();

        $seqnoDb = new App_Model_Application_DbTable_ApplicantSeqno();
        $sequence = $seqnoDb->getData(date('Y'));

        $tblconfigDB = new App_Model_General_DbTable_TblConfig();
        $config = $tblconfigDB->getConfig(1);

        //format
        $format_config = explode('|', $config['ApplicantIdFormat']);
        //var_dump($format_config);
        for ($i = 0; $i < count($format_config); $i++) {
            $format = $format_config[$i];
            if ($format == 'px') { //prefix
                $result = $config['ApplicantPrefix'];
            } elseif ($format == 'yyyy') {
                $result = date('Y');
            } elseif ($format == 'yy') {
                $result = date('y');
            } elseif ($format == 'seqno') {
                $result = sprintf("%03d", $sequence['seq_no']);
            }
            //var_dump($result);
            array_push($applicantid_format, $result);
        }

        //var_dump(implode("-",$applicantID));
        $applicantID = implode("", $applicantid_format);

        //update sequence
        $seqnoDb->updateData(array('seq_no' => $sequence['seq_no'] + 1), $sequence['seq_id']);

        //var_dump($applicantID); exit;

        return $applicantID;
    }

    public function randomizer($length = '8')
    {
        $out = '';
        srand((double)microtime() * 1000000);
        $characters = "1,2,3,4,5,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,6,7,8,9,0,P,Q,R,S,T,U,V,W,X,Y,Z";
        $characters_length = (strlen($characters) - 1) / 2;
        $token = explode(",", $characters);
        $pass_length = $length;   // length of the password

        for ($i = 0; $i < $pass_length; $i++) {
            $rand = rand(0, $characters_length);
            $out .= $token[$rand];
        }
        return $out;
    }

    public function deleteAction()
    {
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();

        $txnId = $this->_getParam('id', 0);
        $this->view->txn_id = $txnId;

        $db = getDB();

        $auth = Zend_Auth::getInstance();
        $iduser = $auth->getIdentity()->iduser;

        if ($iduser == 1) {
            $txnData = $applicantTxnDB->getTransaction($txnId, 1);

            if (empty($txnData)) {
                throw new Exception('Invalid Applicant ID');
            }

            //get transactions
            $select = $db->select()
                ->from(array('s' => 'applicant_transaction'))
                ->where('at_appl_id = ?', $txnData['at_appl_id']);
            $trans = $db->fetchAll($select);

            if (!empty($trans)) {
                foreach ($trans as $txn) {
                    $db->query($db->quoteInto("DELETE FROM applicant_transaction WHERE at_trans_id = ?", $txn['at_trans_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_program WHERE ap_at_trans_id = ?", $txn['at_trans_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_english_proficiency WHERE ep_transaction_id = ?", $txn['at_trans_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_financial WHERE af_trans_id = ?", $txn['at_trans_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_visa WHERE av_trans_id = ?", $txn['at_trans_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_employment WHERE ae_trans_id = ?", $txn['at_trans_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_health_condition WHERE ah_trans_id = ?", $txn['at_trans_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_education WHERE ae_transaction_id = ?", $txn['at_trans_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_documents WHERE ad_txn_id = ?", $txn['at_trans_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_accomodation WHERE acd_trans_id = ?", $txn['at_trans_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_assessment WHERE aar_trans_id = ?", $txn['at_trans_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_change_program WHERE acp_appl_id = ?", $txn['at_appl_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_declaration WHERE txn_id = ?", $txn['at_trans_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_document_status WHERE ads_txn_id = ?", $txn['at_txn_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_eligible WHERE appl_id = ?", $txn['at_appl_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_qualification WHERE ae_transaction_id = ?", $txn['at_txn_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_quantitative_proficiency WHERE qp_trans_id = ?", $txn['at_txn_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_quit WHERE aq_trans_id = ?", $txn['at_txn_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_referees WHERE r_txn_id = ?", $txn['at_txn_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_research_publication WHERE rpd_trans_id = ?", $txn['at_txn_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_family WHERE af_appl_id = ?", $txn['at_appl_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_generated_document WHERE tgd_txn_id = ?", $txn['at_txn_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_section_status WHERE appl_trans_id = ?", $txn['at_txn_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_status_history WHERE ash_trans_id = ?", $txn['at_txn_id']));
                    $db->query($db->quoteInto("DELETE FROM applicant_working_experience WHERE aw_trans_id = ?", $txn['at_txn_id']));
                    $db->query($db->quoteInto("DELETE FROM proforma_invoice_main WHERE trans_id = ?", $txn['at_txn_id']));
                }

                $db->query($db->quoteInto("DELETE FROM applicant_profile WHERE appl_id = ?", $txnData['at_appl_id']));
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Applicant Deleted"));

            $this->_redirect($this->baseUrl . '/application/index/');
        } else {
            throw new Exception('You dont have permission to delete applicant');
        }
    }

    public function editAction()
    {
        $this->view->title = "Edit Applicant";

        $txnId = $this->_getParam('id', 0);
        $this->view->txn_id = $txnId;

        //not required
        $idSection = $this->_getParam('idSection', '');
        $this->view->idSection = $idSection;

        //updated
        $this->view->u = $this->_getParam('u', 0);
        $this->view->tab = $tab = $this->_getParam('tab');


        if ($idSection == '' && $tab == '') {
            $this->_redirect('/application/index/edit/id/' . $txnId . '/idSection/1');
        }

        $pid = $this->_getParam('pid', 0);
        $this->view->pid = $pid;

        $appForm = new Application_Form_Manual();


        //MODELS
        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $objdeftypeDB = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $appInitConfigDB = new Application_Model_DbTable_ApplicationInitialConfig();
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $programDB = new App_Model_Application_DbTable_ApplicantProgram();
        $appQualificationDB = new App_Model_Application_DbTable_ApplicantQualification();
        $countryDB = new App_Model_General_DbTable_Country();
        $entryReqDB = new App_Model_Application_DbTable_EntryRequirement();
        $institutionDB = new App_Model_Application_DbTable_Institution();
        $appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();
        $appHealthDB = new App_Model_Application_DbTable_ApplicantHealthCondition();
        $appFinanacialDB = new App_Model_Application_DbTable_ApplicantFinancial();
        $appvisaDB = new App_Model_Application_DbTable_ApplicantVisa();
        $appSectionStatusDB = new App_Model_Application_DbTable_ApplicantSectionStatus();
        $branchDB = new GeneralSetup_Model_DbTable_Branchofficevenue();


        //txn data
        $txnData = $applicantTxnDB->getTransaction($txnId, 1);
        //var_dump($txnData); exit;
        $appl_id = $this->view->appl_id = $txnData['at_appl_id'];
        $transID = $this->view->transID = $txnData['at_trans_id'];
        $IdConfig = $this->view->IdConfig = $txnData['IdConfig'];

        $this->view->txnData = $txnData;
        $this->view->session_transactionID = $transID;
        //var_dump($txnData); exit;
        //applicant info
        $applicant_info = $appProfileDB->getData($appl_id, 0, 1);
        $applicant_info['applicant_id'] = $txnData['at_pes_id'];
        $this->view->applicant_info = $applicant_info;
        //var_dump($applicant_info); exit;
        $photo = $appDocumentDB->getDataArrayBySection($transID, 2, 'Passport sized photo');
        $app_photo = $this->view->app_photo_full = '';
        if (!empty($photo)) {
            $this->view->app_photo_full = DOCUMENT_URL . $photo[0]['ad_filepath'];
            $app_photo = $this->view->baseUrl() . '/thumb.php?s=1&w=100&f=' . DOCUMENT_PATH . $photo[0]['ad_filepath'];
        }

        $this->view->app_photo = $app_photo;

        //programme applied
        $program = $programDB->getProgram($txnId);
        $this->view->program = $program;


        $appProfileDB = new Application_Model_DbTable_ApplicantProfile();
        $appProgram = $appProfileDB->getProfileProgram($transID);
        $firstonly = 0;


        if ($appProgram['ap_prog_scheme'] == '' || $appProgram['appl_category'] == '') {
            $firstonly = 1;
        }


        $this->view->programID = $appProgram['ap_prog_id'];
        $this->view->nationality = $appProgram['appl_nationality'];
        $this->view->studentCategoryID = $appProgram['appl_category'];
        $this->view->scheme = $appProgram['ap_prog_scheme'];

        //get sidebar list
        $sectionDB = new Application_Model_DbTable_ApplicationSection();
        //$section_list = $sectionDB->getSection($program['ap_prog_scheme'], $txnData['appl_category'], $program['program_id']);
        $section_list = $sectionDB->getSection($IdConfig);


        /*  if ($firstonly == 1) {
              $this->view->firstonly = $firstonly;
              $section_list = array();
          }*/

//        array_unshift($section_list, array('sectionID' => 1, 'name' => 'Programme Details'));

        $sectionItems = array();

        //APPLICANT INFO
        $applicant = $applicant_info;

        if ($tab != '') {
            switch ($tab) {
                /*case 'financial':
                    $appFinForm = new Application_Form_ApplicantFinancial();
                    $appFinForm->populate(array('feeplan' => $txnData['at_fs_id'], 'discountplan' => $txnData['at_dp_id']));
                    $this->view->appFinForm = $appFinForm;
                    break;*/
                case 'remarks':

                    break;
            }
        } else {
            /*   if ($idSection == 1) {
                   //display program form
                   $this->view->title = $this->view->translate('Programme Details');
                   $this->view->section_tagging_id = $idSection;
                   $this->view->mainsectionid = $this->view->idSection = 1;
                   $idSection = 1;
                   $this->view->section_id = $idSection;
                   $this->section_name = 'Programme Details';

                   $this->view->appProgramID = $appProgram['ap_prog_id'];
                   $this->view->appModeStudy = $appProgram['mode_study'];
                   $this->view->appModeProgram = $appProgram['program_mode'];
                   $this->view->appProgramType = $appProgram['program_type'];
                   $this->view->intake = $appProgram['at_intake'];

                   $applicant['intake'] = $txnData['at_intake'];

                   $programDB = new App_Model_Record_DbTable_Program();
                   $this->view->list_program = $programDB->getData();

                   $this->view->branchList = $branchDB->fnGetBranchList();

                   //init
                   $appconfigDB = new Application_Model_DbTable_ApplicationInitialConfig();
                   $sections = $appconfigDB->getItemBasedOnSection(1);
   //
   //                $sectionItems[1] = $sections;
   //                $section_list = array(array('sectionID' => 1, 'name' => 'Programme Details')) + $section_list;

                   $this->view->sectionItems = $sectionItems;

                   //get applicant documents
                   /* 605 : Proforma
                    * 606 : Offer Letter
                    * 716 : CSE
                    * 851 : Appendix
                    *


                   $documentsDB = new Application_Model_DbTable_ApplicantDocument();

                   //get all documents type
                   $document_list = $documentsDB->getDocumentType(123);

                   foreach ($document_list as $index => $type) {

                       $documents = $documentsDB->getApplicantDocumentByType($transID, $type['idDefinition']);
                       $document_list[$index]['documents'] = $documents;
                   }
                   $this->view->document_list = $document_list;
                   //var_dump($documents);
                   //echo '<pre>';
                   //print_r($document_list);


               } else {*/
            // OTHER SECTIONS

            //sectionData
            $sectionData = $appInitConfigDB->fnGetDefination($idSection);
            $mainsectionid = $sectionData['sectionID'];
            $this->view->mainsectionid = $mainsectionid;

            $this->section_name = $sectionData['name'];

            $section_maxEntry = $sectionData['section_maxEntry'];
            $section_status = $appSectionStatusDB->getStatus($transID, $idSection);

            //Section items
            foreach ($section_list as $section) {
                // if ($section['sectionID'] != 1) {
                $listItem = $appInitConfigDB->getItem($IdConfig, $section['id']);

                if ($section['sectionID'] == 10) {
                    $key = count($listItem);
                    $appFinancial = $appFinanacialDB->getDataByTxn($transID);

                    if ($appFinancial && $appFinancial['af_type_scholarship'] == 523) {
                        $listItem[$key]['name'] = 'Scholarship Application Status';
                        $listItem[$key]['variable'] = 'scholar_status';
                        $listItem[$key]['form_type'] = 'text';
                        $listItem[$key]['view'] = 1;
                        $listItem[$key]['compulsory'] = 0;
                        $listItem[$key]['enable'] = 0;
                        $listItem[$key]['hide'] = 0;
                        $listItem[$key]['description'] = $appFinancial['sa_status'];
                    }

                    //dd($listItem); exit;
                }

                $sectionItems[$section['sectionID']] = $listItem;

                if ($idSection == $section['sectionID']) {
                    $this->view->section_id = $section['id'];
                }
                //}


                if ($section['sectionID'] == 1) {

                    $programDB = new App_Model_Record_DbTable_Program();
                    $this->view->list_program = $programDB->getData();

                    $this->view->branchList = $branchDB->fnGetBranchList();

                    //get applicant documents
                    /* 605 : Proforma
                     * 606 : Offer Letter
                     * 716 : CSE
                     * 851 : Appendix
                     */


                    $documentsDB = new Application_Model_DbTable_ApplicantDocument();

                    //get all documents type
                    $document_list = $documentsDB->getDocumentType(123);

                    foreach ($document_list as $index => $type) {

                        $documents = $documentsDB->getApplicantDocumentByType($transID, $type['idDefinition']);
                        $document_list[$index]['documents'] = $documents;
                    }

                    $this->view->document_list = $document_list;
                    //var_dump($documents);
                    //echo '<pre>';
                    //print_r($document_list);
                }
            }
            // }// section if

        } //tab if


        $this->view->section_list = $section_list;


        //uh oh, we have a problem
        //if ( !isset($this->view->section_id) )
        //{
        //	throw new Zend_Exception('Unable to set section id');
        //}

        $this->view->sectionItems = $sectionItems;

        //-------------------
        // section 2
        // ------------------

        if ($idSection == 2) {
            //$applicant['prev_student']=$this->getBooleanText($applicant['prev_student']);
            $applicant['prev_student'] = $applicant['prev_student'];
            $applicant['studentID'] = $applicant['prev_studentID'];

            //Applicant Documents
            $app_documents = $appDocumentDB->getDataArrayBySection($transID, $idSection);
            $this->view->app_docs = $app_documents;

            $applicant['salutation'] = $applicant['appl_salutation'];
            $applicant['salutation'] = $applicant['salutation'];
            $applicant['first_name'] = $applicant['appl_fname'];
            $applicant['last_name'] = $applicant['appl_lname'];
            $applicant['idNo'] = $applicant['appl_idnumber'];
            $applicant['typeID'] = $applicant['appl_idnumber_type'];
            if ($applicant['appl_passport_expiry']) {
                $applicant['passport_expiry'] = date('d-m-Y', strtotime($applicant['appl_passport_expiry']));
            }
            $applicant['email'] = $applicant['appl_email'];
            $applicant['dob'] = ($applicant['appl_dob']) ? date('d-m-Y', strtotime($applicant['appl_dob'])) : null;
            $applicant['gender'] = $applicant['appl_gender'];
            $applicant['nationality'] = $applicant['appl_nationality'];
            $applicant['appl_category'] = $applicant['appl_category'];
            $applicant['age'] = $this->calcAge($applicant['dob']);

            $applicant['religion'] = $applicant['appl_religion'];
            $applicant['race'] = $applicant['appl_race'];
            $applicant['marital_status'] = $applicant['appl_marital_status'];
            $applicant['bumiputra_eligibility'] = $applicant['appl_bumiputera'];

            $applicant['address_perm1'] = $applicant['appl_address1'];
            $applicant['address_perm2'] = $applicant['appl_address2'];
            $applicant['address_perm3'] = $applicant['appl_address3'];
            $applicant['postcode_perm'] = $applicant['appl_postcode'];
            $applicant['city_perm'] = $applicant['appl_city'];
            $applicant['state_perm'] = $applicant['appl_state'];

            $applicant['city_perm_others'] = $applicant['appl_city_others'];
            $applicant['state_perm_others'] = $applicant['appl_state_others'];

            $applicant['country_perm'] = $applicant['appl_country'];
            $applicant['home_perm'] = $applicant['appl_phone_home'];
            $applicant['mobile_perm'] = $applicant['appl_phone_mobile'];
            $applicant['office_perm'] = $applicant['appl_phone_office'];
            $applicant['fax_perm'] = $applicant['appl_fax'];

            $applicant['address_corr1'] = $applicant['appl_caddress1'];
            $applicant['address_corr2'] = $applicant['appl_caddress2'];
            $applicant['address_corr3'] = $applicant['appl_caddress3'];
            $applicant['postcode_corr'] = $applicant['appl_cpostcode'];
            $applicant['city_corr'] = $applicant['appl_ccity'];
            $applicant['state_corr'] = $applicant['appl_cstate'];

            $applicant['city_corr_others'] = $applicant['appl_ccity_others'];
            $applicant['state_corr_others'] = $applicant['appl_cstate_others'];

            $applicant['country_corr'] = $applicant['appl_ccountry'];
            $applicant['home_corr'] = $applicant['appl_cphone_home'];
            $applicant['mobile_corr'] = $applicant['appl_cphone_mobile'];
            $applicant['office_corr'] = $applicant['appl_cphone_office'];
            $applicant['fax-corr'] = $applicant['appl_cfax'];

            $applicant['phone_home'] = $applicant['appl_contact_home'];
            $applicant['phone_mobile'] = $applicant['appl_contact_mobile'];
            $applicant['phone_office'] = $applicant['appl_contact_office'];


        }


        //-------------------
        // section 3
        // ------------------

        //1st:track from current profile data exist or not
        if ($idSection == 3) {

            //1st:track from current profile data exist or not
            $appQualificationDB = new App_Model_Application_DbTable_ApplicantQualification();
            $appQualification = $appQualificationDB->getTransData($appl_id, $transID);

            if ($applicant['prev_student'] != 0 && !empty($applicant['prev_txn_id'])) {
                if (($section_status['retrieve_status'] == '')) {
                    //track from previous education data
                    $this->view->retrieve = 1; //to indicate student to retriev from prev data
                    $appQualification = $appQualificationDB->getTransData(0, $applicant['prev_txn_id']);
                    $transID = $applicant['prev_txn_id'];
                }
            }

            //get documents
            if (count($appQualification) > 0) {
                foreach ($appQualification as $index => $q) {
                    $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
                    $documents = $appDocumentDB->getDataArray($transID, $q['ae_id'], 'applicant_qualification');
                    $appQualification[$index]['documents'] = $documents;
                }
            } else {
                $applicant['qualification_id'] = $applicant['at_default_qlevel'];
            }

            $this->view->total_added = count($appQualification);
            $this->view->qualification = $appQualification;


            /*if(count($appQualification)>0){
                $applicant['primary_id']=$appQualification[0]['ae_id'];
                $applicant['qualification_id']=$appQualification[0]['ae_qualification'];
                $applicant['degree_awarded']=$appQualification[0]['ae_degree_awarded'];
                $applicant['class_degree']=$appQualification[0]['ae_class_degree'];
                $applicant['result']=$appQualification[0]['ae_result'];
                $applicant['year_graduated']=$appQualification[0]['ae_year_graduate'];
                $applicant['ae_institution_country']=$appQualification[0]['ae_institution_country'];
                $applicant['institution_name']=$appQualification[0]['ae_institution'];
                $applicant['medium_instruction']=$appQualification[0]['ae_medium_instruction'];
                $applicant['others']=$appQualification[0]['others'];
                $applicant['ae_majoring']=$appQualification[0]['ae_majoring'];

                $app_documents = $appDocumentDB->getDataArrayBySection($transID,$idSection);
                $this->view->app_docs =$app_documents;
            }*///if count


            $appQualification = $appQualificationDB->getTransData($appl_id, $txnId);

            //get documents
            if (count($appQualification) > 0) {
                foreach ($appQualification as $index => $q) {
                    $documents = $appDocumentDB->getDataArray($txnId, $q['ae_id'], 'applicant_qualification');
                    $appQualification[$index]['documents'] = $documents;
                }
            }
        }

        //-------------------
        // section 4
        // ------------------
        if ($idSection == 4) {
            //get Format
            $this->view->formatdetail = $objdeftypeDB->getListDataByCodeType(134);

            $appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
            $engprof = $appEngProfDB->getDataAll($transID); //not sure if should use getData or modify it to reflect OA

            if ($applicant['prev_student'] != 0 && !empty($applicant['prev_txn_id'])) {
                if (($section_status['retrieve_status'] == '')) {
                    //track from previous data
                    $this->view->retrieve = 1; //to indicate student to retriev from prev data
                    $engprof = $appEngProfDB->getData($applicant['prev_txn_id']);
                    $transID = $applicant['prev_txn_id'];
                }
            }


            //get documents
            if (count($engprof) > 0) {

                foreach ($engprof as $index => $q) {

                    //check details test
                    if ($q['ep_test'] == 13) {
                        $detail = $objdeftypeDB->fnGetDefinationNameString($q['ep_test_detail']);
                        $engprof[$index]['test_detail'] = $detail['DefinitionDesc'];
                    } else
                        if ($q['ep_test'] == 24) {
                            $qmasterDb = new App_Model_General_DbTable_Qualificationmaster();
                            $detail = $qmasterDb->getSubDatabyId($q['ep_test_detail']);
                            $engprof[$index]['test_detail'] = $detail['name'];
                        } else {
                            $engprof[$index]['test_detail'] = $q['ep_test_detail'];
                        }

                    $documents = $appDocumentDB->getDataArray($transID, $q['ep_id'], 'applicant_english_proficiency');
                    $engprof[$index]['documents'] = $documents;
                }
            }

            $this->view->total_added = count($engprof);
            $this->view->engprof = $engprof;

            if (count($engprof) > 0) {
                $applicant['primary_id'] = $engprof[0]['ep_id'];
                $applicant['english_test'] = $engprof[0]['ep_test'];
                $applicant['english_test_detail'] = $engprof[0]['ep_test_detail'];
                $applicant['english_date_taken'] = ($engprof[0]['ep_date_taken'] == null || $engprof[0]['ep_date_taken'] == '1970-01-01') ? '' : date('d-m-Y', strtotime($engprof[0]['ep_date_taken']));
                $applicant['english_score'] = $engprof[0]['ep_score'];

                $app_documents = $appDocumentDB->getDataArrayBySection($transID, $idSection);
                $this->view->app_docs = $app_documents;

            }//if count

        }

        //-------------------
        // section 5 (not used)
        // ------------------
        if ($idSection == 5) {
            $qpDB = new App_Model_Application_DbTable_ApplicantQuantitativeProficiency();
            $quanprof_list = $qpDB->getTransData($transID);

            //get documents
            foreach ($quanprof_list as $index => $q) {

                $documents = $appDocumentDB->getDataArray($transID, $q['qp_id'], 'applicant_quantitative_proficiency');
                $quanprof_list[$index]['documents'] = $documents;
            }

            $this->view->qp_list = $quanprof_list;
            $this->view->total_added = count($quanprof_list);


            if (count($quanprof_list) > 0) {
                $applicant = $quanprof_list[0];
                $applicant['primary_id'] = $quanprof_list[0]['qp_id'];

                $app_documents = $appDocumentDB->getDataArrayBySection($transID, $idSection);
                $this->view->app_docs = $app_documents;
            }//if count

        }

        //-------------------
        // section 6 (not used)
        // ------------------


        if ($idSection == 6) {
            $rpdDB = new App_Model_Application_DbTable_ApplicantResearchPublication();
            $research_list = $rpdDB->getTransData($transID);

            //get documents
            foreach ($research_list as $index => $r) {

                //$dcl_id = 1 ; transcript
                //$dcl_id = 2 ; certificate
                //TODO:to be changes based on izham document checklist

                $documents = $appDocumentDB->getDataArray($transID, $r['rpd_id'], 'applicant_research_publication');
                $research_list[$index]['documents'] = $documents;
            }

            $this->view->rpd_list = $research_list;
            $this->view->total_added = count($research_list);

            $app_documents = $appDocumentDB->getDataArrayBySection($transID, $idSection);
            $this->view->app_docs = $app_documents;

        }


        //-------------------
        // section 7 : Employment Details
        // ------------------
        if ($idSection == 7) {
            $appEmployment = $appEmploymentDB->getTransData($appl_id, $transID);

            if ($applicant['prev_student'] != 0 && !empty($applicant['prev_txn_id'])) {
                if (($section_status['retrieve_status'] == '')) {
                    //track from previous data
                    $this->view->retrieve = 1; //to indicate student to retriev from prev data
                    $appEmployment = $appEmploymentDB->getTransData(0, $applicant['prev_txn_id']);
                    $transID = $applicant['prev_txn_id']; //to query document
                }
            }

            if (count($appEmployment) > 0) {
                foreach ($appEmployment as $index => $emp) {
                    $documents = $appDocumentDB->getDataArray($transID, $emp['ae_id'], 'applicant_employment');
                    $appEmployment[$index]['documents'] = $documents;
                }
            }

            $this->view->total_added = count($appEmployment);
            $this->view->appEmployment = $appEmployment;

            if (count($appEmployment) > 0) {
                $applicant['primary_id'] = $appEmployment[0]['ae_id'];
                $applicant['emply_status'] = $appEmployment[0]['ae_status'];
                $applicant['emply_comp_name'] = $appEmployment[0]['ae_comp_name'];
                $applicant['emply_comp_address'] = $appEmployment[0]['ae_comp_address'];
                $applicant['emply_comp_phone'] = $appEmployment[0]['ae_comp_phone'];
                $applicant['emply_comp_fax'] = $appEmployment[0]['ae_comp_fax'];
                $applicant['emply_designation'] = $appEmployment[0]['ae_designation'];
                $applicant['emply_position_level'] = $appEmployment[0]['ae_position'];
                $applicant['emply_duration_from'] = $appEmployment[0]['ae_from'];
                $applicant['emply_duration_to'] = $appEmployment[0]['ae_to'];
                $applicant['emply_year_service'] = $appEmployment[0]['emply_year_service'];
                $applicant['emply_industry'] = $appEmployment[0]['ae_industry'];
                $applicant['emply_job_desc'] = $appEmployment[0]['ae_job_desc'];
            }
        }


        //-------------------
        // section 8 : Working XP
        // ------------------

        if ($idSection == 8) {
            $industryWorkingExperience = $objdeftypeDB->getListDataByCodeType('industry-working-experience');
            $this->view->industryWorkingExperience = $industryWorkingExperience;

            $this->view->yearofIndustry = $objdeftypeDB->getListDataByCodeType('years-of-industry');
            //$applicant = array();
        }

        //-------------------
        // section 9 : Health
        // ------------------
        if ($idSection == 9) {
            $result = $objdeftypeDB->getListDataByCodeType('health-condition');

            if ($applicant['prev_student'] != 0 && !empty($applicant['prev_txn_id'])) {
                if (($section_status['retrieve_status'] == '')) {
                    //track from previous data
                    $transID = $applicant['prev_txn_id'];
                }
            }

            foreach ($result as $index => $r) {
                //get data
                $appHealth = $appHealthDB->getRowData($transID, $r['idDefinition']);

                if (isset($appHealth) && $appHealth['ah_status'] != '') {
                    $applicant['health_condition'][$index] = $appHealth['ah_status'];
                }
            }
        }

        //-------------------
        // section 10 : Financial Particulars
        // ------------------
        if ($idSection == 10) {
            $appFinancial = $appFinanacialDB->getDataByTxn($transID);
            $this->view->appFinancial = $appFinancial;

            if (isset($appFinancial) && $appFinancial['af_id'] != '') {
                $app_documents = $appDocumentDB->getDataArray($transID, $appFinancial['af_id'], 'applicant_financial');
                $this->view->app_docs = $app_documents;

                if (count($app_documents) > 0) {
                    foreach ($app_documents as $doc) {
                        //$this->view->$doc['ad_dcl_id'] = $doc['ad_dcl_id'];
                        $filename = 'filename' . $doc['ad_dcl_id'];
                        $file_url = DOCUMENT_URL . $doc['ad_filepath'];
                        $this->view->$filename = '<a href="' . $file_url . '" target="_blank">' . $doc['ad_ori_filename'] . '</a>';
                    }
                }
            }

            $applicant['primary_id'] = $appFinancial['af_id'];
            $applicant['fin_method'] = $appFinancial['af_method'];
            $applicant['fin_sponsor_name'] = $appFinancial['af_sponsor_name'];
            $applicant['fin_type_scholarship'] = $appFinancial['af_type_scholarship'];
            $applicant['fin_scholarship_secured'] = $appFinancial['af_scholarship_secured'];
            $applicant['fin_scholarship_apply'] = $appFinancial['af_scholarship_apply'];
            $applicant['fin_apply'] = $appFinancial['af_scholarship_apply'];
            $applicant['file_fin_resume'] = $appFinancial['af_file'];

            $scholarshipDb = new App_Model_General_DbTable_Scholarship();

            $result_scholarship = $scholarshipDb->getScholarship($appProgram['ap_prog_scheme'], $appProgram['at_intake'], $applicant['appl_category']);

            $this->view->total_scholarship = count($result_scholarship);

            //520 - sponsorship
            if ($applicant['fin_method'] == 520) {
                $SponsorDB = new Studentfinance_Model_DbTable_Sponsor();
                $this->view->sponsor_list = $SponsorDB->fngetallsponsor();


                $applicantSponsor = $SponsorDB->getApplicantSponsor($applicant['appl_id']);
                $this->view->applicantSponsor = $applicantSponsor;

                $FeeitemDB = new Studentfinance_Model_DbTable_FeeItem();
                $feeList = $FeeitemDB->getData();
                $this->view->feeitem = $feeList;

            } elseif ($applicant['fin_method'] == 521) {
                //521 - scholarship
            }
        }


        //-------------------
        // section 11 : Visa Details
        // ------------------
        if ($idSection == 11) {
            $visa = $appvisaDB->getTransData($transID);

            $applicant['primary_id'] = $visa['av_id'];
            $applicant['visa_malaysian'] = $visa['av_malaysian_visa'];
            $applicant['visa_status'] = $visa['av_status'];
            $applicant['visa_expiry'] = $visa['av_expiry'];


        }


        //-------------------
        // section 12 : Accomodation
        // ------------------

        if ($idSection == 12) {
            $accDB = new App_Model_Application_DbTable_ApplicantAccomodation();
            $accomodation = $accDB->getDatabyTxn($txnId);

            $applicant['primary_id'] = $accomodation['acd_id'];
            $applicant['acd_assistance'] = $accomodation['acd_assistance'];
            //$appForm->acd_type->setValue($accomodation['acd_type']);

            $roomTypeDB = new Hostel_Model_DbTable_Hostelroom();
            $room_list = $roomTypeDB->getRoomList(263);
            //var_dump($room_list); exit;
            //$appForm->acd_type->setAttrib('class','accomodationtype');
            /*$table = '<table border="0" width="80%">
						<tr>
						   <th>'.$this->view->translate("Occupancy Type").'</th>
						   <th>'.$this->view->translate("Room Type").'</th>
						   <th>'.$this->view->translate("Monthly Rate").'<br>(RM)</th>
						   <th>'.$this->view->translate("Tick").'</th>
						</tr>';

			foreach ($room_list as $room) {
				$table .= "<tr>
							<td align='center'>".$room['OccupancyType']."</td>
							<td align='center'>".$room['RoomType']."</td>
							<td align='center'>".$room['Rate']."</td><td  align='center'>";
				//$table .= $appForm->acd_type->addMultiOption($room['IdHostelRoomType']);
				$table .= "</td></tr>";
			}
			$table .= '</table>';  */

            /*$table = '<table class="table" border=0 width="80%">
                                    <tr>
                                       <th>'.$this->view->translate("Occupancy Type").'</th>
                                       <th>'.$this->view->translate("Room Type").'</th>
                                       <th>'.$this->view->translate("Monthly Rate").'<br>(RM)</th>
                                       <th>'.$this->view->translate("Tick").'</th>
                                    </tr>';

                        foreach ($room_list as $room) {
                                if($accomodation['acd_type']==$room['IdHostelRoomType']){
                                        $selected = 'checked';
                                }else{
                                        $selected = '';
                                }
                                $table .= "<tr>
                                                        <td align='left'>".$room['OccupancyType']."</td>
                                                        <td align='left'>".$room['RoomType']."</td>
                                                        <td align='left'>".$room['Rate']."</td><td  align='left'>";
                                $table .= $form->$list['variable']->addMultiOption($room['IdHostelRoomType']);
                                $form->$list['variable']->clearMultiOptions();
                                $table .= "<input class='accomodationtype' type='radio' name='acd_type'  id='acd_type' value='".$room['IdHostelRoomType']."' ".$selected.">";
                                $table .= "</td></tr>";
                        }
                        $table .= '</table>';*/

            //$this->view->table = $table;
        }

        if ($idSection == 13) {
            //get subject from landscape
            $landscapeDB = new App_Model_General_DbTable_Landscape();
            $subject_list = $landscapeDB->getLandscapeSubject($appProgram['ap_prog_id'], $appProgram['ap_prog_scheme'], $appProgram['at_intake']);
            $landscapeInfo = $landscapeDB->getLandscapeInfo($appProgram['ap_prog_id'], $appProgram['ap_prog_scheme'], $appProgram['at_intake']);

            $table = '';

            if (!empty($landscapeInfo)) {
                $tagInfo = $landscapeDB->getLandscapeSubjectTag($landscapeInfo['IdLandscape']);
                $tagEncode = json_decode($tagInfo['subjectdata'], true);

                $intakeInfo = $landscapeDB->getIntakeInfo($appProgram['at_intake']);
                $semInfo = $landscapeDB->getSemesterInfo($intakeInfo['sem_year'], $intakeInfo['sem_seq']);
                //$subject_list = $landscapeDB->getSubject();
                $i = 0;

                foreach ($subject_list as $loop) {
                    if (isset($tagEncode[$loop['IdSubject']])) {
                        //var_dump($tagEncode[$loop['IdSubject']]);
                        if (isset($tagEncode[$loop['IdSubject']][$semInfo['SemesterType']])) {
                            $subject_list2[$i] = $loop;
                            $i++;
                        }
                    }
                }

                $table = '<table border=0 width="100%" class="table">
				<tr>
				<th>' . $this->view->translate("No") . '</th>
				<th>' . $this->view->translate("Subject Code") . '</th>
				<th align="left">' . $this->view->translate("Subject Name") . '</th>
				<th>' . $this->view->translate("Credit Hour") . '</th>		        			
				</tr>';
                foreach ($subject_list2 as $index => $subject) {
                    $no = $index + 1;
                    $table .= "<tr>
				<td align='center'>" . $no . ".</td>
				<td>" . $subject['SubCode'] . "</td>
				<td>" . $subject['SubjectName'] . "</td>
				<td align='center'>" . $subject['CreditHours'] . "</td>";
                    $table .= "</tr>";
                }
                $table .= '</table>';
            }


            $this->view->table = $table;
        }

        //-------------------
        // section 14 : Essay
        // ------------------
        if ($idSection == 14) {
            $this->view->app_docs = $app_documents = $appDocumentDB->getDataArrayBySection($transID, $idSection);
        }

        //-------------------
        // section 15 : Research of interest
        // ------------------
        if ($idSection == 15) {
            $this->view->app_docs = $app_documents = $appDocumentDB->getDataArrayBySection($transID, $idSection);

        }

        //-------------------
        // section 16 : Referees
        // ------------------
        if ($idSection == 16) {
            $refereeDB = new App_Model_Application_DbTable_ApplicantReferees();
            $referee = $refereeDB->getData($transID);
            if ($referee) {
                $applicant = $referee;
            }
        }

        /*
			FORM ELEMENTS/ETC
		*/

        $onload = array();


        if (!empty($sectionItems[$idSection])) {


            foreach ($sectionItems[$idSection] as $_item) {

                if ($_item['form_type'] != '') {
                    $class = $label_class = '';

                    // Processing Types
                    switch ($_item['form_type']) {
                        case 'header_title_center':
                            $_item['form_type'] = '';
                            break;

                        case 'calendar':

                            $_item['form_type'] = 'text';
                            $class = 'input-txt datepicker';

                            break;

                        case 'calendar_backdated':

                            $_item['form_type'] = 'text';
                            $class = 'input-txt backdated';

                            break;

                        case 'month':

                            $_item['form_type'] = 'text';
                            $class = 'input-txt monthcal';

                            break;

                        case 'text':

                            $class = 'input-txt';

                            break;

                        case 'select':

                            $class = 'select';

                            break;

                        case 'select_others':
                            $class = 'select others';
                            $_item['form_type'] = 'select';

                            $others_name = $_item['variable'] . '_others';

                            $appForm->addElement('text', $others_name, array(
                                'class'       => 'input-txt others',
                                'label_class' => $label_class,
                                'decorators'  => array(
                                    'ViewHelper',
                                    'Description',
                                    'Errors'),
                            ));

                            $appForm->$others_name->removeDecorator('Errors');
                            $appForm->$others_name->removeDecorator('HtmlTag');
                            $appForm->$others_name->removeDecorator('Label');

                            break;

                        case 'textarea':

                            $class = 'textarea';

                            break;

                        case 'checkbox':
                            $label_class = 'label_checkbox';

                            //checkbox => multicheckbox
                            if ($_item['variable'] == 'health_condition') {
                                $_item['form_type'] = 'multiCheckbox';
                            }

                            break;

                        case 'radio':

                            $class = 'radio';
                            $label_class = 'label_radio';

                            break;

                        default;

                            $label_class = '';

                    }


                    //echo $_item['form_type'] . '<br />';

                    if ($_item['form_type'] == 'file') {
                        $appForm->addElement($_item['form_type'], $_item['variable'], array(
                            'decorators' => array(
                                'File',
                                'Errors')
                        ));

                        //app docs

                        if (isset($app_documents) && count($app_documents) > 0) {
                            $stack_doc = array();
                            foreach ($app_documents as $doc) {
                                if ($doc['ad_dcl_id'] == $_item['item_dclId']) {
                                    $file_url = DOCUMENT_URL . $doc['ad_filepath'];
                                    $docs['ad_filename'] = '<a href="' . $file_url . '" target="_blank" class="appdoc">' . $doc['ad_ori_filename'] . '</a>';
                                    $docs['ad_id'] = $doc['ad_id'];
                                    array_push($stack_doc, $docs);
                                }
                            }
                            $stack = 'stack' . $_item['item_dclId'];
                            $this->view->$stack = $stack_doc;
                        }
                    } else if ($_item['form_type'] == 'download_file') {
                        //do something with the file
                    } else {
                        if ($_item['compulsory'] == 1) {
                            if ($_item['variable'] != 'studentID') {
                                $class .= ' reqfield';
                            }
                        }

                        if ($_item['form_type'] != '') {

                            $appForm->addElement($_item['form_type'], $_item['variable'], array(
                                'class'       => $class,
                                'label_class' => $label_class,
                                'decorators'  => array(
                                    'ViewHelper',
                                    'Description',
                                    'Errors'),
                            ));

                            $appForm->{$_item['variable']}->removeDecorator('Errors');
                            $appForm->{$_item['variable']}->removeDecorator('HtmlTag');
                            $appForm->{$_item['variable']}->removeDecorator('Label');
                        }

                        /* misc */
                        if ($_item['form_type'] == 'radio') {
                            $appForm->{$_item['variable']}->setSeparator(' ');
                        }


                        /* ----------------------------------------------------------------------------------------------
							WHERE ELEMENTS WILL GET THEIR VALUES
						   ---------------------------------------------------------------------------------------------- */
                        //var_dump($_item['variable']);
                        switch ($_item['variable']) {
                            /* program */
                            case "program":

                                foreach ($this->view->list_program as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['IdProgram'], $data['ProgramName']);

                                }

                                $applicant['program'] = $program['ap_prog_id'];
                                //$appForm->{$_item['variable']}->setValue($program['ap_prog_id']);
                                $appForm->{$_item['variable']}->setAttrib('disabled', 'disabled');
                                break;

                            case "program_type":
                            case "program_mode":
                            case "mode_study":
                                $appForm->{$_item['variable']}->setAttrib('onchange', "getIntake();");
                                break;

                            /* Prev Student */
                            case "prev_student":
                            case "visa_malaysian":

                                $appForm->{$_item['variable']}->setMultiOptions(array(
                                    '1' => $this->view->translate('Yes'),
                                    '0' => $this->view->translate('No')
                                ));

                                if ($_item['variable'] == 'prev_student') {
                                    $prev_stud_val = $applicant[$_item['variable']] == 1 ? 1 : 0;
                                    $onload[] = "togglePrevStudent($prev_stud_val);";
                                } else {
                                    $visa_val = $applicant[$_item['variable']] == 1 ? 1 : 0;
                                    $onload[] = "toggleVisa($visa_val);";
                                }

                                break;

                            /* typeID */
                            case "typeID":

                                $result = $objdeftypeDB->getListDataByCodeType('identity-card-type');
                                $dataArray = array();
                                foreach ($result as $data) {
                                    $dataArray[$data['idDefinition']] = $data['DefinitionDesc'];
                                }

                                $appForm->{$_item['variable']}->setMultiOptions($dataArray);

                                break;

                            /* gender */
                            case "gender":

                                $appForm->{$_item['variable']}->setMultiOptions(array(
                                        '1' => $this->view->translate('Male'),
                                        '2' => $this->view->translate('Female'))
                                );
                                break;

                            case "race":
                                $result = $objdeftypeDB->getListDataByCodeType('race');
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                                }
                                break;

                            case 'bumiputra_eligibility':

                                $result = $objdeftypeDB->getListDataByCodeType('bumiputera-eligibility');
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                                }

                                break;
                            /* Salutation */
                            case "salutation":

                                $result = $objdeftypeDB->getListDataByCodeType('salutation');
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                                }

                                break;

                            /* Countries */
                            case "nationality":
                            case "country_perm":
                            case "country_corr":
                            case "ae_institution_country":
                            case "r_ref1_country":
                            case "r_ref2_country":

                                $map = array(
                                    'country_perm'   => 'state_perm',
                                    'country_corr'   => 'state_corr',
                                    'r_ref1_country' => 'r_ref1_state',
                                    'r_ref2_country' => 'r_ref2_state'
                                );

                                $appForm->{$_item['variable']}->addMultiOption('', '--Please Select--');

                                foreach ($countryDB->getData() as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idCountry'], $data['CountryName']);
                                }

                                //has states
                                if (array_key_exists($_item['variable'], $map)) {
                                    $appForm->{$_item['variable']}->setAttrib('onchange', 'getState(this.value,"' . $map[$_item['variable']] . '")');

                                    //onload
                                    if (isset($applicant[$map[$_item['variable']]]) && $applicant[$map[$_item['variable']]] != '') {
                                        $onload[] = "getState($('#" . $_item['variable'] . "').val(),'" . $map[$_item['variable']] . "'," . $applicant[$map[$_item['variable']]] . ");";
                                    }
                                }

                                if ($_item['variable'] == 'ae_institution_country') {
                                    $appForm->{$_item['variable']}->setAttrib('onchange', 'getInstitution(this.value,"institution_name"); ');
                                    //var_dump($applicant);
                                    //onload
                                    //$onload[] = 'getInstitution('.$applicant['ae_institution_country'].',"institution_name","'.$applicant['institution_name'].'"); ';
                                }

                                break;

                            /* states */

                            case "state_perm":
                            case "state_corr":

                                $citymap = array('state_perm' => 'city_perm', 'state_corr' => 'city_corr');

                                $appForm->{$_item['variable']}->setAttrib('onchange', 'getCity(this.value, "' . $citymap[$_item['variable']] . '"); checkOthers(this)');

                                if (isset($applicant[$citymap[$_item['variable']]]) && $applicant[$citymap[$_item['variable']]] != '') {
                                    $onload[] = "getCity($('#" . $_item['variable'] . "').val(),'" . $citymap[$_item['variable']] . "'," . $applicant[$citymap[$_item['variable']]] . ");";
                                }

                                break;

                            /* cities */
                            case "city_perm":
                            case "city_corr":
                                $appForm->{$_item['variable']}->setAttrib('onchange', 'checkOthers(this)');
                                break;

                            /* marital status */
                            case "marital_status":

                                $result = $objdeftypeDB->getListDataByCodeType('marital-status');
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                                }

                                break;

                            /* religion */
                            case "religion":

                                $result = $objdeftypeDB->getListDataByCodeType('religion');
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                                }

                                break;

                            /* replicate perm address */
                            case "replicate_address_option":

                                $appForm->{$_item['variable']}->setAttrib('onchange', 'filladdress(this)');

                                break;

                            /* qualification_id */
                            case "qualification_id":

                                $result = $entryReqDB->getListGeneralReq();
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['IdQualification'], $data['QualificationLevel']);
                                }

                                break;

                            /* class_degree */
                            case "class_degree":
                                $result = $objdeftypeDB->getListDataByCodeType('class-degree');
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                                }
                                break;

                            /* institution name */
                            case "institution_name":
                                $appForm->{$_item['variable']}->setAttrib('onchange', 'displayOthers(this.value,"institution_name")');
                                break;

                            /* english test */
                            case "english_test":
                                $qualificationDB = new App_Model_General_DbTable_Qualificationmaster();
                                $result = $qualificationDB->getDataTest();
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['IdQualification'], $data['QualificationLevel']);
                                }
                                $appForm->{$_item['variable']}->setAttrib('onchange', 'getDetail(this.value)');

                                if (isset($applicant['english_test'])) {
                                    $onload[] = 'getDetail("' . $applicant['english_test'] . '");';
                                }

                                break;

                            /* employment stuff */

                            case "emply_position_level":

                                $appEmployment[0]['ae_position'] = isset($appEmployment[0]['ae_position']) ? $appEmployment[0]['ae_position'] : '';

                                $appForm->{$_item['variable']}->setValue($appEmployment[0]['ae_position']);
                                $appForm->{$_item['variable']}->setAttrib('class', 'positionlevel');

                                $positionLevel = $objdeftypeDB->getListDataByCodeType('position-level');
                                $table = '<table border="0" class="borderless">
											<tr>
												<td colspan="2"><strong>' . $this->view->translate("Position Level") . '</strong></td>
												<td><strong>' . $this->view->translate("Description") . '</strong></td>
											</tr>';
                                foreach ($positionLevel as $pl) {
                                    $table .= '<tr>
												<td>' . $appForm->{$_item['variable']}->addMultiOption($pl['idDefinition']) . '</td>
												<td>' . $pl["DefinitionDesc"] . '</td>
												<td>' . $pl["Description"] . '</td>
											</tr>';
                                    $appForm->{$_item['variable']}->clearMultiOptions();
                                }
                                $table .= '</table>	';

                                $this->view->table = $table;

                                break;

                            /* employment status */
                            case "emply_status":

                                $result = $objdeftypeDB->getListDataByCodeType('employment-status');
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                                }

                                break;
                            /* industry */
                            case 'emply_industry':
                                $result = $objdeftypeDB->getListDataByCodeType('industry-working-experience');
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                                }
                                break;
                            /* visa status */
                            case 'visa_status':
                                $result = $objdeftypeDB->getListDataByCodeType('visa-status');
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                                }
                                break;
                            /* health condition*/
                            case 'health_condition':

                                $result = $objdeftypeDB->getListDataByCodeType('health-condition');

                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                                }

                                $appForm->{$_item['variable']}->setSeparator('<br /><br />');
                                //$appForm->{$_item['variable']}->setAttrib('onclick', 'displayHealthCond(this.value)');

                                break;
                            /* funding method */
                            case 'fin_method':
                                $result = $objdeftypeDB->getListDataByCodeType('funding-method');
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                                }

                                $fin_method = $this->view->appFinancial['af_method'];
                                $fin_type_scholarship = $this->view->appFinancial['af_type_scholarship'];

                                $onload[] = "display_financial_block($fin_method); ";
                                $onload[] = "block_scholarship($fin_type_scholarship);";

                                break;

                            /* scholarship */
                            case 'fin_type_scholarship':
                                $result = $objdeftypeDB->fnGetDefinations('Select Scholarship');
                                //var_dump($result); exit;
                                $appForm->{$_item['variable']}->addMultiOption('', '-- Please Select --');
                                foreach ($result as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['idDefinition'], $data['DefinitionDesc']);
                                }
                                break;

                            case 'fin_apply':
                                //var_dump($result_scholarship); exit;
                                foreach ($result_scholarship as $data) {
                                    $appForm->{$_item['variable']}->addMultiOption($data['sch_Id'], $data['sch_name']);
                                }
                                break;

                            /* intake */
                            case 'intake':

                                $intakeDb = new GeneralSetup_Model_DbTable_Intake();
                                $listData = $intakeDb->fngetIntakeList();

                                foreach ($listData as $list) {
                                    $appForm->{$_item['variable']}->addMultiOption($list['IdIntake'], $list['IntakeDesc']);
                                }
                                break;

                            case 'acd_assistance':
                                $appForm->{$_item['variable']}->setMultiOptions(array(
                                    '1' => ' ' . $this->view->translate('Yes'),
                                    '0' => ' ' . $this->view->translate('No')));

                                $appForm->{$_item['variable']}->setValue(0);
                                $appForm->{$_item['variable']}->setAttrib('onclick', 'enableAccomodation(this.value)');
                                break;

                            case 'acd_type':
                                $table = '<table class="table" border=0 width="80%">
                                                                                    <tr>
                                                                                       <th>' . $this->view->translate("Occupancy Type") . '</th>
                                                                                       <th>' . $this->view->translate("Room Type") . '</th>
                                                                                       <th>' . $this->view->translate("Monthly Rate") . '<br>(RM)</th>
                                                                                       <th>' . $this->view->translate("Tick") . '</th>
                                                                                    </tr>';

                                foreach ($room_list as $room) {
                                    if ($accomodation['acd_type'] == $room['IdHostelRoomType']) {
                                        $selected = 'checked';
                                    } else {
                                        $selected = '';
                                    }
                                    $table .= "<tr>
                                                                                            <td align='left'>" . $room['occTypeName'] . "</td>
                                                                                            <td align='left'>" . $room['RoomType'] . "</td>
                                                                                            <td align='left'>" . $room['Rate'] . "</td><td  align='left'>";
                                    //$table .= $appForm->{$_item['variable']}->addMultiOption($room['IdHostelRoomType']);
                                    //$appForm->{$_item['variable']}->clearMultiOptions();
                                    $table .= "<input class='accomodationtype' type='radio' name='acd_type'  id='acd_type' value='" . $room['IdHostelRoomType'] . "' " . $selected . ">";
                                    $table .= "</td></tr>";
                                }
                                $table .= '</table>';

                                $this->view->table = $table;
                                break;

                            case 'acd_accomodation':
                                $commTemplateDB = new App_Model_General_DbTable_CommTemplate();
                                $this->view->file_download = $commTemplateDB->getDocumentDownload($list['item_docDownload']);
                                //var_dump($this->view->file_download); exit;
                                break;

                            case 'scholar_status':
                                //dd($_item);
                                $SponsorApplication = new Studentfinance_Model_DbTable_SponsorApplication();
                                $appForm->{$_item['variable']}->setAttrib('disable', true);
                                $appForm->{$_item['variable']}->setValue($SponsorApplication->status[$_item['description']]);
                                break;

                        }
                    }
                } else {
                    //print_R($_item);
                }
            }
        }


        //POST
        if ($this->_request->isPost() && $this->_request->getPost('tab')) {
            $formData = $this->getRequest()->getPost();

            if ($formData['tab'] == 'financial') {
                $db = getDB();
                $auth = Zend_Auth::getInstance();

                if ($formData['feeplan'] != '') {
                    $data['at_fs_id'] = $formData['feeplan'];
                    $data['at_fs_date'] = new Zend_Db_Expr('NOW()');
                    $data['at_fs_by'] = $auth->getIdentity()->iduser;
                } else {
                    $data['at_fs_id'] = '';
                    $data['at_fs_date'] = '';
                    $data['at_fs_by'] = '';
                }

                if ($formData['discountplan'] != '') {
                    $data['at_dp_id'] = $formData['discountplan'];
                    $data['at_dp_date'] = new Zend_Db_Expr('NOW()');
                    $data['at_dp_by'] = $auth->getIdentity()->iduser;
                } else {
                    $data['at_dp_id'] = '';
                    $data['at_dp_date'] = '';
                    $data['at_dp_by'] = '';
                }

                $db->update('applicant_transaction', $data, 'at_trans_id=' . (int)$txnId);

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->baseUrl . '/application/index/edit/id/' . $txnId . '/tab/financial');
            } else if ($formData['tab'] == 'remarks') {
                //var_dump($formData); exit;

                $dataRemarks = array(
                    'at_remarks' => $formData['at_remarks']
                );

                $db = getDB();
                $db->update('applicant_transaction', $dataRemarks, 'at_trans_id=' . (int)$txnId);

                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->baseUrl . '/application/index/edit/id/' . $txnId . '/tab/remarks');
            }
        }

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            $this->updateApplication($formData);
        }


        //form
        $this->view->appForm = $appForm;

        // Spit them out
        $this->view->onload = $onload;

        $this->view->applicant = $applicant;

    }

    public function updateApplication($formData)
    {
        $sectionID = $formData['id'];  //the id
        $idSection = $formData['idSection'];  // x,1,2,3,4,5,6

        $appl_id = $formData['appl_id'];
        $transID = $formData['transID'];

        $auth = Zend_Auth::getInstance();

        //save into database
        $appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
        $appTransaction = new App_Model_Application_DbTable_ApplicantTransaction();
        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $appSectionStatusDB = new App_Model_Application_DbTable_ApplicantSectionStatus();
        $appQualificationDB = new App_Model_Application_DbTable_ApplicantQualification();
        $appFinancial = new App_Model_Application_DbTable_ApplicantFinancial();
        $appHealthDB = new App_Model_Application_DbTable_ApplicantHealthCondition();
        $appFinancial = new App_Model_Application_DbTable_ApplicantFinancial();
        $appVisaDB = new App_Model_Application_DbTable_ApplicantVisa();
        $checklistHistoryDB = new Application_Model_DbTable_ChecklistHistory();
        $sponsorApplicationDB = new Studentfinance_Model_DbTable_SponsorApplication();
        $scholarshipDb = new App_Model_General_DbTable_Scholarship();
        $appProgram = $appProfileDB->getProfileProgram($transID);
        // PROGRAM DETAILS
        if ($sectionID == 1) {
            $idSection = 1;

            //get program
            $program = $appProgramDB->getProgram($transID);

            //get program scheme
            $scheme = $appProgramDB->getProgramScheme($program['ap_prog_id'], $formData['mode_study'], $formData['program_mode'], $formData['program_type']);

            $transInfo = $appTransaction->getTransaction($transID);

            /*if ($transInfo['branch_id'] != $formData['branch_id']){
                            $appTransaction->updateData(array('branch_id' => $formData['branch_id']),$transID);
                            $feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
                            $feeStructureData = $feeStructureDb->getApplicantFeeStructure($program['ap_prog_id'], $program['ap_prog_scheme'], $transInfo['at_intake'], $transInfo['appl_category']);
                            $appTransaction->updateData(array('at_fs_id' => $feeStructureData['fs_id']),$transID);
                        }*/

            if (($program['ap_prog_scheme'] != $scheme['IdProgramScheme']) || ($transInfo['at_intake'] != $formData['intake']) || ($transInfo['branch_id'] != $formData['branch_id'])) {

                if (isset($formData['mode_study']) && isset($formData['program_mode']) && isset($formData['program_type'])) {
                    $data1 = array(
                        'mode_study'     => $formData['mode_study'],
                        'program_mode'   => $formData['program_mode'],
                        'program_type'   => $formData['program_type'],
                        'ap_prog_scheme' => $scheme['IdProgramScheme'],
                        'upd_date'       => date("Y-m-d H:i:s")
                    );

                    $appProgramDB->update($data1, "ap_at_trans_id = " . $transID);
                }

                $trans_data = $appTransaction->getTransaction($transID);

                $appTransaction->updateData(array('at_intake_prev' => new Zend_Db_Expr('at_intake')), $formData['transID']);

                if (isset($formData['intake'])) {
                    $data2 = array(
                        'at_intake' => $formData['intake'],
                        'at_fs_id'  => null,
                        'upd_date'  => date("Y-m-d H:i:s")
                    );

                    $appTransaction->updateData($data2, $formData['transID']);
                }

                //audit compare
                $auditDB = new App_Model_Audit();
                $log_audit = $this->compareChanges($data1, $program, array('upd_date', 'ap_prog_scheme'));

                if (!empty($log_audit)) {
                    $auditDB->recordLog('Programme Details', 1, $transID, $log_audit, $auth->getIdentity()->iduser, 'application');
                }

                $log_audit2 = $this->compareChanges($data2, $trans_data, array('upd_date'));
                if (!empty($log_audit2)) {
                    $auditDB->recordLog('Programme Details', 1, $transID, $log_audit2, $auth->getIdentity()->iduser, 'application');
                }

                //update branch
                //$appProfileDB->updateData(array('branch_id' => $formData['branch_id']),$appl_id);
                //appProgram = $appProgramDB->getProgram($transID);
                $appTransaction->updateData(array('branch_id' => $formData['branch_id']), $transID);
                $classInvoice = new icampus_Function_Studentfinance_Invoice();
                if (($program['ap_prog_scheme'] != $scheme['IdProgramScheme']) || ($transInfo['at_intake'] != $formData['intake'])) {
                    $proformaList = $appTransaction->getProforma($transID);

                    if ($proformaList) {
                        foreach ($proformaList as $proformaLoop) {
                            if ($proformaLoop['invoice_id'] != 0) {
                                $classInvoice->generateCreditNoteByInvoice($transID, $proformaLoop['invoice_id'], 1, 1);
                            }

                            $cancelData = array(
                                'status'      => 'X',
                                'cancel_by'   => 665,
                                'cancel_date' => date('Y-m-d')
                            );
                            $appTransaction->cancelProforma($cancelData, $proformaLoop['id']);
                        }
                    }

                    $appTransaction->revertPaymentStatus($transID);
                }

                //need some tweak here
                if ($program['at_status'] == 593 || $program['at_status'] == 599 || $program['at_status'] == 601) {
                    $checkScheme = $this->model->checkScheme($program['ap_prog_id']);

                    $classInvoice->generateApplicantProformaInvoice($transID, 0);

                    if ($checkScheme['IdScheme'] != 1) {
                        $classInvoice->generateApplicantProformaInvoice($transID, 1);
                    }
                } else if ($program['at_status'] == 592 || $program['at_status'] == 595 || $program['at_status'] == 596 || $program['at_status'] == 597) {
                    $classInvoice->generateApplicantProformaInvoice($transID, 0);
                }

                //todo store checklist history
                if ($program['ap_prog_scheme'] != $scheme['IdProgramScheme']) {
                    $applicantProfileInfo = $checklistHistoryDB->getApplicantProfileById($appl_id);

                    $oldChecklist = $checklistHistoryDB->getOldChecklist($applicantProfileInfo['appl_category'], $program['ap_prog_scheme']);

                    $bind1 = array(
                        'adh_txn_id'           => $transID,
                        'adh_type'             => 1,
                        'adh_oldprogramscheme' => $program['ap_prog_scheme'],
                        'adh_newprogramscheme' => $scheme['IdProgramScheme'],
                        'adh_upddate'          => date('Y-m-d'),
                        'adh_updby'            => $auth->getIdentity()->iduser
                    );
                    $adh_id = $checklistHistoryDB->insertChecklistHistory($bind1);

                    if ($oldChecklist) {
                        foreach ($oldChecklist as $oldChecklistLoop) {

                            $bind4 = array(
                                'adc_adh_id'        => $adh_id,
                                'adc_dcl_id'        => $oldChecklistLoop['dcl_Id'],
                                'adc_stdCtgy'       => $oldChecklistLoop['dcl_stdCtgy'],
                                'adc_programScheme' => $oldChecklistLoop['dcl_programScheme'],
                                'adc_sectionid'     => $oldChecklistLoop['dcl_sectionid'],
                                'adc_name'          => $oldChecklistLoop['dcl_name'],
                                'adc_malayName'     => $oldChecklistLoop['dcl_malayName'],
                                'adc_desc'          => $oldChecklistLoop['dcl_desc'],
                                'adc_type'          => $oldChecklistLoop['dcl_type'],
                                'adc_activeStatus'  => $oldChecklistLoop['dcl_activeStatus'],
                                'adc_priority'      => $oldChecklistLoop['dcl_priority'],
                                'adc_uplStatus'     => $oldChecklistLoop['dcl_uplStatus'],
                                'adc_uplType'       => $oldChecklistLoop['dcl_uplType'],
                                'adc_finance'       => $oldChecklistLoop['dcl_finance'],
                                'adc_mandatory'     => $oldChecklistLoop['dcl_mandatory'],
                                'adc_specific'      => $oldChecklistLoop['dcl_specific'],
                                'adc_upddate'       => date('Y-m-d'),
                                'adc_updby'         => $auth->getIdentity()->iduser
                            );
                            $adc_id = $checklistHistoryDB->insertChecklistDclHistory($bind4);

                            $oldChecklistStatus = $checklistHistoryDB->getOldChecklistStatus($transID, $oldChecklistLoop['dcl_Id']);

                            if ($oldChecklistStatus) {
                                foreach ($oldChecklistStatus as $oldChecklistStatusLoop) {
                                    $bind2 = array(
                                        'adt_adh_id'     => $adh_id,
                                        'adt_adc_id'     => $adc_id,
                                        'adt_txn_id'     => $transID,
                                        'adt_dcl_id'     => $oldChecklistStatusLoop['ads_dcl_id'],
                                        'adt_section_id' => $oldChecklistStatusLoop['ads_section_id'],
                                        'adt_table_name' => $oldChecklistStatusLoop['ads_table_name'],
                                        'adt_table_id'   => $oldChecklistStatusLoop['ads_table_id'],
                                        'adt_status'     => $oldChecklistStatusLoop['ads_status'],
                                        'adt_comment'    => $oldChecklistStatusLoop['ads_comment'],
                                        'adt_upddate'    => date('Y-m-d'),
                                        'adt_updby'      => $auth->getIdentity()->iduser
                                    );
                                    $checklistHistoryDB->insertChecklistStatusHistory($bind2);
                                }
                            }

                            $oldChecklistDocument = $checklistHistoryDB->getOldChecklistDocument($transID, $oldChecklistLoop['dcl_Id']);

                            if ($oldChecklistDocument) {
                                foreach ($oldChecklistDocument as $oldChecklistDocumentLoop) {
                                    $bind3 = array(
                                        'adu_adh_id'       => $adh_id,
                                        'adu_adc_id'       => $adc_id,
                                        'adu_txn_id'       => $transID,
                                        'adu_dcl_id'       => $oldChecklistDocumentLoop['ad_dcl_id'],
                                        'adu_section_id'   => $oldChecklistDocumentLoop['ad_section_id'],
                                        'adu_table_name'   => $oldChecklistDocumentLoop['ad_table_name'],
                                        'adu_table_id'     => $oldChecklistDocumentLoop['ad_table_id'],
                                        'adu_filepath'     => $oldChecklistDocumentLoop['ad_filepath'],
                                        'adu_filename'     => $oldChecklistDocumentLoop['ad_filename'],
                                        'adu_ori_filename' => $oldChecklistDocumentLoop['ad_ori_filename'],
                                        'adu_upddate'      => date('Y-m-d'),
                                        'adu_updby'        => $auth->getIdentity()->iduser
                                    );
                                    $checklistHistoryDB->insertCheclistDocumentHistory($bind3);
                                }
                            }
                        }
                    }

                    //todo delete old cheklist data
                    $checklistHistoryDB->deleteOldchecklistDocument($transID);
                    $checklistHistoryDB->deleteOldchecklistStatus($transID);

                    //todo update payment
                    $this->updatePaymentStatus($transID);
                    $this->updateWaivedStatus($transID);
                }
            } else {
                $trans_data = $appTransaction->getTransaction($transID);

                $appTransaction->updateData(array('at_intake_prev' => new Zend_Db_Expr('at_intake')), $formData['transID']);

                $data2 = array(
                    'at_intake' => $formData['intake'],
                    'upd_date'  => date("Y-m-d H:i:s")
                );

                $appTransaction->updateData($data2, $formData['transID']);

                //audit compare
                $auditDB = new App_Model_Audit();
                $log_audit2 = $this->compareChanges($data2, $trans_data, array('upd_date'));
                if (!empty($log_audit2)) {
                    $auditDB->recordLog('Programme Details', 1, $transID, $log_audit2, $auth->getIdentity()->iduser, 'application');
                }
            }
        } else {

            //get main ID
            $appInitConfigDB = new Application_Model_DbTable_ApplicationInitialConfig();
            $sectionData = $appInitConfigDB->fnGetDefination($sectionID);

            if (empty($sectionData)) {
                throw Zend_Exception('Invalid Section ID');
            }
        }


        //program details
        if ($idSection == 1) {

            $appProgram = $appProgramDB->getProgrambyTxn($transID);

            $applicant['program'] = $appProgram['ap_prog_id'];
            $applicant['level_of_study'] = $appProgram['ap_level_of_study'];
            $applicant['program_mode'] = $appProgram['mode_study'];
            $applicant['program_type'] = $appProgram['program_type'];
            $applicant['IdProgramScheme'] = $appProgram['ap_prog_scheme'];
            $applicant['intake'] = $appProgram['at_intake'];
            $applicant['branch'] = $appProgram['branch_id'];

        }


        /*personal details*/
        if ($idSection == 2) {
            //var_dump($formData); exit;
            if (isset($formData['passport_expiry'])) {
                $passport_expiry = date('Y-m-d', strtotime($formData['passport_expiry']));
            }

            /*if ($formData["appl_type_nationality"] == 547) {
                $formData['appl_category'] = 579; //local
            } else if ($formData["appl_type_nationality"] == 548) {
                $formData['appl_category'] = 581; //pr
            } else if ($formData["appl_type_nationality"] == 549) {
                $formData['appl_category'] = 580; //int
            }*/

            $appProfile = $appProgramDB->getApplicantProfile($transID);
            $program = $appProgramDB->getProgram($transID);

            /*if ($formData['appl_category']!=$appProfile['appl_category']){
                            exit;
                        }*/

            $data5 = array(
                'prev_student'         => $formData['prev_student'],
                'prev_studentID'       => $formData['studentID'],
                'prev_txn_id'          => $formData['prev_txn_id'],
                'appl_salutation'      => $formData['salutation'],
                'appl_fname'           => $formData['first_name'],
                'appl_lname'           => $formData['last_name'],
                'appl_email'           => $formData['email'],
                //'appl_dob' => $formData['dob']['day']."-".$formData['dob']['month']."-".$formData['dob']['year'],
                'appl_dob'             => $formData['dob'],
                'appl_gender'          => $formData['gender'],
                'appl_race'            => $formData['race'],
                'appl_religion'        => $formData['religion'],
                'appl_marital_status'  => $formData['marital_status'],
                'appl_idnumber'        => $formData['idNo'],
                'appl_idnumber_type'   => $formData['typeID'],
                'appl_passport_expiry' => $passport_expiry,
                'appl_bumiputera'      => $formData['bumiputra_eligibility'],
                'appl_address1'        => $formData['address_perm1'],
                'appl_address2'        => $formData['address_perm2'],
                'appl_address3'        => $formData['address_perm3'],
                'appl_postcode'        => $formData['postcode_perm'],
                'appl_city'            => $formData['city_perm'],
                //'appl_category' => $formData['appl_category'],
                //'appl_nationality' => $formData['nationality'],
                //'appl_type_nationality' => $formData['appl_type_nationality'],
                'appl_country'         => $formData['country_perm'],
                'appl_state'           => $formData['state_perm'],
                'appl_city_others'     => $formData['city_perm_others'],
                'appl_state_others'    => $formData['state_perm_others'],
                'appl_phone_home'      => $formData['home_perm'],
                'appl_phone_mobile'    => $formData['mobile_perm'],
                'appl_phone_office'    => $formData['office_perm'],
                'appl_fax'             => $formData['fax_perm'],
                'appl_caddress1'       => $formData['address_corr1'],
                'appl_caddress2'       => $formData['address_corr2'],
                'appl_caddress3'       => $formData['address_corr3'],
                'appl_ccity'           => $formData['city_corr'],
                'appl_cstate'          => $formData['state_corr'],
                'appl_ccity_others'    => $formData['city_corr_others'],
                'appl_cstate_others'   => $formData['state_corr_others'],
                'appl_ccountry'        => $formData['country_corr'],
                'appl_cpostcode'       => $formData['postcode_corr'],
                'appl_cphone_home'     => $formData['home_corr'],
                'appl_cphone_mobile'   => $formData['mobile_corr'],
                'appl_cphone_office'   => $formData['office_corr'],
                'appl_cfax'            => $formData['fax_corr'],
                'appl_contact_home'    => $formData['phone_home'],
                'appl_contact_mobile'  => $formData['phone_mobile'],
                'appl_contact_office'  => $formData['phone_office'],
                'upd_date'             => date("Y-m-d H:i:s")
            );

            if (isset($formData['appl_category'])) {
                $data5['appl_category'] = $formData['appl_category'];
            }
            if (isset($formData['nationality'])) {
                $data5['appl_nationality'] = $formData['nationality'];
            }
            if (isset($formData['appl_type_nationality'])) {
                $data5['appl_type_nationality'] = $formData['appl_type_nationality'];
            }

            //upload files

            //audit compare
            $auditDB = new App_Model_Audit();
            $applicant = $appProfileDB->getData($appl_id);
            $log_audit = $this->compareChanges($data5, $applicant, array('upd_date'));

            if (!empty($log_audit)) {
                $auditDB->recordLog('Personal Details', 2, $transID, $log_audit, $auth->getIdentity()->iduser, 'application');
            }

            $appProfileDB->updateData($data5, $appl_id);
            //var_dump($_FILES);
            //exit;
            $this->upload($_FILES, $idSection, $appl_id, 'applicant_profile', $transID);

            if (isset($formData['appl_category']) && $formData['appl_category'] != $appProfile['appl_category']) {
                $data2 = array(
                    'at_fs_id' => null,
                    'upd_date' => date("Y-m-d H:i:s")
                );

                $appTransaction->updateData($data2, $transID);

                $classInvoice = new icampus_Function_Studentfinance_Invoice();
                $proformaList = $appTransaction->getProforma($transID);

                if ($proformaList) {
                    foreach ($proformaList as $proformaLoop) {
                        if ($proformaLoop['invoice_id'] != 0) {
                            $classInvoice->generateCreditNoteByInvoice($transID, $proformaLoop['invoice_id'], 1, 1);
                        }

                        $cancelData = array(
                            'status'      => 'X',
                            'cancel_by'   => 665,
                            'cancel_date' => date('Y-m-d')
                        );
                        $appTransaction->cancelProforma($cancelData, $proformaLoop['id']);
                    }
                }

                $appTransaction->revertPaymentStatus($transID);

                //need some tweak here
                if ($program['at_status'] == 593 || $program['at_status'] == 599 || $program['at_status'] == 601) {
                    $checkScheme = $this->model->checkScheme($program['ap_prog_id']);

                    $classInvoice->generateApplicantProformaInvoice($transID, 0);

                    if ($checkScheme['IdScheme'] != 1) {
                        $classInvoice->generateApplicantProformaInvoice($transID, 1);
                    }
                } else if ($program['at_status'] == 592 || $program['at_status'] == 595 || $program['at_status'] == 596 || $program['at_status'] == 597) {
                    $test = $classInvoice->generateApplicantProformaInvoice($transID, 0);
                }

                //todo store checklist history
                $applicantProfileInfo = $checklistHistoryDB->getApplicantProfileById($appl_id);

                $oldChecklist = $checklistHistoryDB->getOldChecklist($applicantProfileInfo['appl_category'], $program['ap_prog_scheme']);

                $bind1 = array(
                    'adh_txn_id'        => $transID,
                    'adh_type'          => 2,
                    'adh_oldstudentcat' => $appProfile['appl_category'],
                    'adh_newstudentcat' => $formData['appl_category'],
                    'adh_upddate'       => date('Y-m-d'),
                    'adh_updby'         => $auth->getIdentity()->iduser
                );
                $adh_id = $checklistHistoryDB->insertChecklistHistory($bind1);

                if ($oldChecklist) {
                    foreach ($oldChecklist as $oldChecklistLoop) {

                        $bind4 = array(
                            'adc_adh_id'        => $adh_id,
                            'adc_dcl_id'        => $oldChecklistLoop['dcl_Id'],
                            'adc_stdCtgy'       => $oldChecklistLoop['dcl_stdCtgy'],
                            'adc_programScheme' => $oldChecklistLoop['dcl_programScheme'],
                            'adc_sectionid'     => $oldChecklistLoop['dcl_sectionid'],
                            'adc_name'          => $oldChecklistLoop['dcl_name'],
                            'adc_malayName'     => $oldChecklistLoop['dcl_malayName'],
                            'adc_desc'          => $oldChecklistLoop['dcl_desc'],
                            'adc_type'          => $oldChecklistLoop['dcl_type'],
                            'adc_activeStatus'  => $oldChecklistLoop['dcl_activeStatus'],
                            'adc_priority'      => $oldChecklistLoop['dcl_priority'],
                            'adc_uplStatus'     => $oldChecklistLoop['dcl_uplStatus'],
                            'adc_uplType'       => $oldChecklistLoop['dcl_uplType'],
                            'adc_finance'       => $oldChecklistLoop['dcl_finance'],
                            'adc_mandatory'     => $oldChecklistLoop['dcl_mandatory'],
                            'adc_specific'      => $oldChecklistLoop['dcl_specific'],
                            'adc_upddate'       => date('Y-m-d'),
                            'adc_updby'         => $auth->getIdentity()->iduser
                        );
                        $adc_id = $checklistHistoryDB->insertChecklistDclHistory($bind4);

                        $oldChecklistStatus = $checklistHistoryDB->getOldChecklistStatus($transID, $oldChecklistLoop['dcl_Id']);

                        if ($oldChecklistStatus) {
                            foreach ($oldChecklistStatus as $oldChecklistStatusLoop) {
                                $bind2 = array(
                                    'adt_adh_id'     => $adh_id,
                                    'adt_adc_id'     => $adc_id,
                                    'adt_txn_id'     => $transID,
                                    'adt_dcl_id'     => $oldChecklistStatusLoop['ads_dcl_id'],
                                    'adt_section_id' => $oldChecklistStatusLoop['ads_section_id'],
                                    'adt_table_name' => $oldChecklistStatusLoop['ads_table_name'],
                                    'adt_table_id'   => $oldChecklistStatusLoop['ads_table_id'],
                                    'adt_status'     => $oldChecklistStatusLoop['ads_status'],
                                    'adt_comment'    => $oldChecklistStatusLoop['ads_comment'],
                                    'adt_upddate'    => date('Y-m-d'),
                                    'adt_updby'      => $auth->getIdentity()->iduser
                                );
                                $checklistHistoryDB->insertChecklistStatusHistory($bind2);
                            }
                        }

                        $oldChecklistDocument = $checklistHistoryDB->getOldChecklistDocument($transID, $oldChecklistLoop['dcl_Id']);

                        if ($oldChecklistDocument) {
                            foreach ($oldChecklistDocument as $oldChecklistDocumentLoop) {
                                $bind3 = array(
                                    'adu_adh_id'       => $adh_id,
                                    'adu_adc_id'       => $adc_id,
                                    'adu_txn_id'       => $transID,
                                    'adu_dcl_id'       => $oldChecklistDocumentLoop['ad_dcl_id'],
                                    'adu_section_id'   => $oldChecklistDocumentLoop['ad_section_id'],
                                    'adu_table_name'   => $oldChecklistDocumentLoop['ad_table_name'],
                                    'adu_table_id'     => $oldChecklistDocumentLoop['ad_table_id'],
                                    'adu_filepath'     => $oldChecklistDocumentLoop['ad_filepath'],
                                    'adu_filename'     => $oldChecklistDocumentLoop['ad_filename'],
                                    'adu_ori_filename' => $oldChecklistDocumentLoop['ad_ori_filename'],
                                    'adu_upddate'      => date('Y-m-d'),
                                    'adu_updby'        => $auth->getIdentity()->iduser
                                );
                                $checklistHistoryDB->insertCheclistDocumentHistory($bind3);
                            }
                        }
                    }
                }

                //todo delete old cheklist data
                $checklistHistoryDB->deleteOldchecklistDocument($transID);
                $checklistHistoryDB->deleteOldchecklistStatus($transID);

                //todo update payment
                $this->updatePaymentStatus($transID);
                $this->updateWaivedStatus($transID);
            }


        }//end section 2

        // qualification
        if ($idSection == 3) {

            $data6 = array(
                'ae_appl_id'             => $appl_id,
                'ae_transaction_id'      => $transID,
                'ae_qualification'       => $formData['qualification_id'],
                'ae_degree_awarded'      => $formData['degree_awarded'],
                'ae_class_degree'        => $formData['class_degree'],
                'ae_majoring'            => $formData['ae_majoring'],
                'ae_result'              => $formData['result'],
                'ae_year_graduate'       => $formData['year_graduated'],
                'ae_institution_country' => $formData['ae_institution_country'],
                'ae_institution'         => $formData['institution_name'],
                'ae_medium_instruction'  => $formData['medium_instruction'],
                'others'                 => $formData['others'],
                'createDate'             => date("Y-m-d H:i:s")
            );

            //audit compare
            $auditDB = new App_Model_Audit();
            $appQualification = $appQualificationDB->getTransData($appl_id, $transID);

            $log_audit = $this->compareChanges($data6, $appQualification[0], array('ae_appl_id', 'createDate'));

            if (!empty($log_audit)) {
                $auditDB->recordLog('Education Details', 3, $transID, $log_audit, $auth->getIdentity()->iduser, 'application');
            }

            if ($formData['primary_id']) {
                $appQualificationDB->updateData($data6, $formData['primary_id']);
                $ae_id = $formData['primary_id'];
            } else {
                $ae_id = $appQualificationDB->addData($data6);
            }

            //upload files
            $this->upload($_FILES, $idSection, $ae_id, 'applicant_qualification', $transID);

        }//end section 3

        //English Proficiency Details
        if ($idSection == 4) {

            $data4 = array(
                'ep_transaction_id' => $transID,
                'ep_test'           => $formData['english_test'],
                'ep_test_detail'    => $formData['english_test_detail'],
                'ep_date_taken'     => date('Y-m-d', strtotime($formData['english_date_taken'])),
                'ep_score'          => $formData['english_score'],
                'ep_updatedt'       => date("Y-m-d H:i:s"),
                'ep_updateby'       => $appl_id
            );

            $appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();

            //audit compare
            $auditDB = new App_Model_Audit();
            $engprof = $appEngProfDB->getData($transID);

            $log_audit = $this->compareChanges($data4, $engprof, array('ep_transaction_id', 'ep_updatedt', 'ep_updateby'));

            if (!empty($log_audit)) {
                $auditDB->recordLog('English Proficiency Details', 4, $transID, $log_audit, $auth->getIdentity()->iduser, 'application');
            }

            if (isset($formData['primary_id']) && $formData['primary_id'] != '') {
                //update
                $appEngProfDB->updateData($data4, $formData['primary_id']);
                $ep_id = $formData['primary_id'];
            } else {
                //add
                $ep_id = $appEngProfDB->addData($data4);
            }

            //upload files
            $this->upload($_FILES, $idSection, $ep_id, 'applicant_english_proficiency', $transID);
        }

        //quantitative proficiency
        if ($idSection == 5) {
            $data5 = array(
                'qp_trans_id'    => $transID,
                'qp_level'       => $formData['qp_level'],
                'qp_grade_mark'  => $formData['qp_grade_mark'],
                'qp_institution' => $formData['qp_institution'],
                'qp_createddt'   => date("Y-m-d H:i:s"),
                'qp_createdby'   => $appl_id
            );

            $qpDB = new App_Model_Application_DbTable_ApplicantQuantitativeProficiency();


            //audit compare
            $auditDB = new App_Model_Audit();
            $quanprof_list = $qpDB->getTransData($transID);

            $log_audit = $this->compareChanges($data5, $quanprof_list[0], array('qp_trans_id', 'qp_createddt', 'qp_createdby'));

            if (!empty($log_audit)) {
                $auditDB->recordLog('Quantitative Proficiency Details', 5, $transID, $log_audit, $auth->getIdentity()->iduser, 'application');
            }

            //add
            $qp_id = $qpDB->update($data5, 'qp_trans_id = ' . $transID);

            //upload files
            $this->upload($_FILES, $idSection, $qp_id, 'applicant_quantitative_proficiency', $transID);
        }


        // research & publication
        if ($idSection == 6) {

            $data6 = array(
                'rpd_trans_id'  => $transID,
                'rpd_title'     => $formData['rpd_title'],
                'rpd_date'      => date("Y-m-d", strtotime($formData['rpd_date'])),
                'rpd_publisher' => $formData['rpd_publisher'],
                'rpd_createddt' => date("Y-m-d H:i:s"),
                'rpd_createdby' => $appl_id
            );


            $rpdDB = new App_Model_Application_DbTable_ApplicantResearchPublication();

            //audit compare
            $auditDB = new App_Model_Audit();
            $log_audit = $this->compareChanges($data6, array(), array('rpd_trans_id', 'rpd_createddt', 'rpd_createdby'));

            if (!empty($log_audit)) {
                $auditDB->recordLog('Research/Publication Details', 6, $transID, $log_audit, $auth->getIdentity()->iduser, 'application');
            }

            //add
            $rpdDB->addData($data6);

            //upload files
            $this->upload($_FILES, $idSection, $qp_id, 'applicant_research_publication', $transID);
        }


        //Industry Working Experience
        if ($idSection == 8) {
            //delete
            $appWorkExpDb = new App_Model_Application_DbTable_ApplicantWorkingExperience();
            $objdeftypeDB = new App_Model_Definitiontype();

            $workxpvalues = array();
            foreach ($objdeftypeDB->fnGetDefinations('Industry Working Experience') as $workxp) {
                $get = $appWorkExpDb->getData($transID, $workxp["idDefinition"]);

                $workxpvalues[$workxp["idDefinition"]]['industry'] = $get['aw_industry_id'];
                $workxpvalues[$workxp["idDefinition"]]['years'] = $get['aw_years'];
            }

            $appWorkExpDb->deletebyTxn($transID);

            $log_audit = array();
            for ($x = 0; $x < count($formData['iwe']); $x++) {

                $iwe = $formData['iwe'][$x];

                $data8['aw_appl_id'] = $appl_id;
                $data8['aw_trans_id'] = $transID;
                $data8['aw_industry_id'] = $iwe;
                $data8['aw_years'] = $formData['yof'][$iwe];
                $data8['upd_date'] = date("Y-m-d H:i:s");

                $log_audit[] = array(
                    'type'       => 'field',
                    'variable'   => 'aw_industry_id',
                    'value'      => $formData['iwe'][$x] . '-' . $formData['yof'][$iwe],
                    'prev_value' => $workxpvalues[$iwe]['industry'] . '-' . $workxpvalues[$iwe]['years']
                );

                //add
                $iwe_id = $appWorkExpDb->addData($data8);
            }

            //audit compare
            $auditDB = new App_Model_Audit();

            if (!empty($log_audit)) {
                $auditDB->recordLog('Industry Working Experience', 8, $transID, $log_audit, $auth->getIdentity()->iduser, 'application');
            }
        }

        /*employment*/
        if ($idSection == 7) {
            $data8 = array(
                'ae_appl_id'         => $appl_id,
                'ae_trans_id'        => $transID,
                'ae_status'          => $formData['emply_status'],
                'ae_comp_name'       => $formData['emply_comp_name'],
                'ae_comp_address'    => $formData['emply_comp_address'],
                'ae_comp_phone'      => $formData['emply_comp_phone'],
                'ae_comp_fax'        => $formData['emply_comp_fax'],
                'ae_designation'     => $formData['emply_designation'],
                'ae_position'        => $formData['emply_position_level'],
                'ae_from'            => $formData['emply_duration_from'],
                'ae_to'              => $formData['emply_duration_to'],
                'emply_year_service' => $formData['emply_year_service'],
                'ae_industry'        => $formData['emply_industry'],
                'ae_job_desc'        => $formData['job_desc'],
                'upd_date'           => date("Y-m-d H:i:s")
            );

            $appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();

            //audit compare
            $auditDB = new App_Model_Audit();
            $empdata = $appEmploymentDB->getTransData($appl_id, $transID);

            $log_audit = $this->compareChanges($data8, (count($empdata) > 0 ? $empdata[0] : array()), array('ae_appl_id', 'ae_trans_id', 'upd_date'));


            if (!empty($log_audit)) {
                $auditDB->recordLog('Employment Details', 7, $transID, $log_audit, $auth->getIdentity()->iduser, 'application');
            }

            if ($formData['primary_id']) {
                $appEmploymentDB->updateData($data8, $formData['primary_id']);
                $ae_id = $formData['primary_id'];
            } else {
                $ae_id = $appEmploymentDB->addData($data8);
            }

            //upload files
            $this->upload($_FILES, $idSection, $ae_id, 'applicant_employment', $transID);
        }

        /*health condition */
        if ($idSection == 9) {

            $objdeftypeDB = new App_Model_Definitiontype();
            $result = $objdeftypeDB->getListDataByCodeType('health-condition');

            $prevhealth = array();
            foreach ($result as $index => $r) {
                //get data
                $appHealth = $appHealthDB->getRowData($transID, $r['idDefinition']);

                if (isset($appHealth) && $appHealth['ah_status'] != '') {
                    $prevhealth[] = $r['idDefinition'];
                }
            }

            //delete first
            $appHealthDB->deleteInfo($transID);

            $curhealth = array();
            $countHealth = count($formData['health_condition']);
            $i = 0;
            while ($i < $countHealth) {
                $data9 = array(
                    'ah_appl_id'  => $appl_id,
                    'ah_trans_id' => $transID,
                    'ah_status'   => $formData['health_condition'][$i],
                    'upd_date'    => date("Y-m-d H:i:s")
                );

                $curhealth[] = $formData['health_condition'][$i];

                $appHealthDB->addData($data9);
                $i++;
            }

            $auditDB = new App_Model_Audit();
            $log_audit = array(array(
                'type'       => 'field',
                'variable'   => 'health_condition',
                'value'      => implode(',', $prevhealth),
                'prev_value' => implode(',', $curhealth)
            ));

            if (!empty($log_audit)) {
                $auditDB->recordLog('Health Condition Details', 9, $transID, $log_audit, $auth->getIdentity()->iduser, 'application');
            }
        }

        /*financial details */
        if ($idSection == 10) {
            $result_scholarship = $scholarshipDb->getScholarship($appProgram['ap_prog_scheme'], $appProgram['at_intake'], $appProgram['appl_category'], $formData['fin_apply']);
            $af_scholarship_apply = $result_scholarship[0]["sct_Id"];//echo $af_scholarship_apply."af_scholarship_apply";exit;

            $data10 = array(
                'af_appl_id'             => $appl_id,
                'af_trans_id'            => $transID,
                'af_method'              => $formData['fin_method'],
                'af_sponsor_name'        => $formData['fin_sponsor_name'],
                'af_type_scholarship'    => $formData['fin_type_scholarship'],
                'af_scholarship_secured' => $formData['fin_scholarship_secured'],
                'af_scholarship_apply'   => $af_scholarship_apply,
                // 'af_scholarship_apply' => $formData['fin_apply'],
                'upd_date'               => date("Y-m-d H:i:s")
            );

            $appFinancialData = $appFinancial->getDataByTxn($transID);

            //audit compare
            $auditDB = new App_Model_Audit();

            if (isset($formData['primary_id']) && $formData['primary_id'] != '') {

                //check if fin method were changed
                $old_financial = $appFinancial->getDataById($formData['primary_id']);

                if ($old_financial['af_method'] != $formData['fin_method']) {

                    //empty old data
                    $data_clear = array(
                        'af_method'              => '',
                        'af_sponsor_name'        => '',
                        'af_type_scholarship'    => '',
                        'af_scholarship_secured' => '',
                        'af_scholarship_apply'   => '',
                        'upd_date'               => date("Y-m-d H:i:s")
                    );

                    $appFinancial->updateData($data_clear, $formData['primary_id']);

                    $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
                    $documents = $appDocumentDB->getDataArray($transID, $formData['primary_id'], 'applicant_financial');

                    //delete old files
                    $this->deleteDocuments($documents);


                }//end if

                $appFinancial->updateData($data10, $formData['primary_id']);
                $af_id = $formData['primary_id'];

                //scholarship tagging
                $hasApplied = $sponsorApplicationDB->fetch_my_application($transID);
                if (count($hasApplied) == 0) {//add new
                    $application['sa_af_id'] = $af_id;
                    $application['sa_cust_id'] = $transID;
                    $application['sa_cust_type'] = 2;
                    $application['sa_status'] = 1;
                    $application['sa_createddt'] = date('Y-m-d h:i:s');
                    $application['sa_createdby'] = $auth->getIdentity()->iduser;
//
                    $sponsorApplicationDB->insert($application);
                }

                //sponsorship tagging
                if (isset($formData['fin_sponsor_id']) && !empty($formData['fin_sponsor_id'])) {

                    $SponsorDB = new Studentfinance_Model_DbTable_Sponsor();
                    $applicantSponsor = $SponsorDB->getApplicantSponsor($formData['appl_id']);

                    $data = array(
                        'Sponsor'      => $formData['fin_sponsor_id'],
                        'appl_id'      => $formData['appl_id'],
                        'StartDate'    => $formData['StartDate'],
                        'EndDate'      => $formData['EndDate'],
                        'AggrementNo'  => $formData['AggrementNo'],
                        'Amount'       => $formData['Amount'],
                        'CalcMode'     => $formData['CalculationMode'],
                        'FeeItem'      => $formData['FeeItem'],
                        'UpdUser'      => $auth->getIdentity()->iduser,
                        'UpdDate'      => new Zend_Db_Expr('NOW()'),
                        'IdUniversity' => 1
                    );

                    //
                    if (!empty($applicantSponsor)) {
                        $SponsorDB->deleteApplicantSponsor($applicantSponsor['IdSponsorTag']);
                    }

                    $SponsorDB->addApplicantSponsor($data);
                }

            } else {
                //add
                $af_id = $appFinancial->addData($data10);
            }

            $log_audit = $this->compareChanges($data10, $appFinancialData, array('af_appl_id', 'af_trans_id', 'upd_date'));

            if (!empty($log_audit)) {
                $auditDB->recordLog('Financial Particulars', 10, $transID, $log_audit, $auth->getIdentity()->iduser, 'application');
            }


            //upload files
            $this->upload($_FILES, $idSection, $af_id, 'applicant_financial', $transID);

        }


        /*visa details */
        if ($idSection == 11) {

            $appvisaDB = new App_Model_Application_DbTable_ApplicantVisa();

            $data11 = array(
                'av_appl_id'        => $appl_id,
                'av_trans_id'       => $transID,
                'av_malaysian_visa' => $formData['visa_malaysian'],
                'av_status'         => $formData['visa_status'],
                'av_expiry'         => date('Y-m-d', strtotime($formData['visa_expiry'])),
                'upd_date'          => date("Y-m-d H:i:s")
            );

            //audit compare
            $auditDB = new App_Model_Audit();
            $visa = $appvisaDB->getTransData($transID);

            $log_audit = $this->compareChanges($data11, $visa, array('av_appl_id', 'av_trans_id', 'upd_date'));

            if (!empty($log_audit)) {
                $auditDB->recordLog('Visa Details', 11, $transID, $log_audit, $auth->getIdentity()->iduser, 'application');
            }

            if (isset($formData['primary_id']) && $formData['primary_id'] != '') {
                //update
                $appVisaDB->updateData($data11, $formData['primary_id']);
            } else {
                //add
                $appVisaDB->insert($data11);
            }
        }

        //accomodation
        if ($idSection == 12) {
            $data12 = array(
                'acd_trans_id'   => $formData['transID'],
                'acd_assistance' => $formData['acd_assistance'],
                'acd_type'       => $formData['acd_assistance'] == 0 ? 0 : $formData['acd_type'],
                'acd_updateby'   => $auth->getIdentity()->iduser,
                'acd_updatedt'   => date('Y-m-d')
            );

            $accDB = new App_Model_Application_DbTable_ApplicantAccomodation();

            if (isset($formData['primary_id']) && $formData['primary_id'] != '') {
                //update
                $accDB->updateData($data12, $formData['primary_id']);
            } else {
                //add
                $accDB->addData($data12);
            }

        }

        //essay
        if ($idSection == 14) {

            //upload files
            $this->upload($_FILES, $idSection, 0, '', $transID);

        }

        //statement of research interest
        if ($idSection == 15) {

            //upload files
            $this->upload($_FILES, $idSection, 0, '', $transID);

        }

        /* referees */
        if ($idSection == 16) {

            for ($x = 1; $x <= 2; $x++) {

                $data16['r_ref' . $x . '_name'] = $formData['r_ref' . $x . '_name'];
                $data16['r_ref' . $x . '_position'] = $formData['r_ref' . $x . '_position'];
                $data16['r_ref' . $x . '_add1'] = $formData['r_ref' . $x . '_add1'];
                $data16['r_ref' . $x . '_add2'] = $formData['r_ref' . $x . '_add2'];
                $data16['r_ref' . $x . '_add3'] = $formData['r_ref' . $x . '_add3'];
                $data16['r_ref' . $x . '_postcode'] = $formData['r_ref' . $x . '_postcode'];
                $data16['r_ref' . $x . '_city'] = $formData['r_ref' . $x . '_city'];
                $data16['r_ref' . $x . '_country'] = $formData['r_ref' . $x . '_country'];
                $data16['r_ref' . $x . '_state'] = $formData['r_ref' . $x . '_state'];
                $data16['r_ref' . $x . '_phone'] = $formData['r_ref' . $x . '_phone'];
                $data16['r_ref' . $x . '_fax'] = $formData['r_ref' . $x . '_fax'];
                $data16['r_ref' . $x . '_email'] = $formData['r_ref' . $x . '_email'];
            }
            $data16['r_txn_id'] = $transID;
            $data16['r_updateby'] = $appl_id;
            $data16['r_updatedt'] = date("Y-m-d H:i:s");

            $refereeDB = new App_Model_Application_DbTable_ApplicantReferees();
            $refereeDB->update($data16, 'r_txn_id = ' . $transID);

        }

        $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

        //Redir
        $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'index', 'action' => 'edit', 'id' => $transID, 'idSection' => $idSection), 'default', true));
    }

    public function compareChanges($refarr, $data = array(), $skip = array())
    {
        $newdata = array();

        foreach ($refarr as $ref_key => $ref_val) {
            if (!in_array($ref_key, $skip)) {
                if (empty($data)) {
                    $newdata[] = array(
                        'type'       => 'field',
                        'variable'   => $ref_key,
                        'value'      => $ref_val,
                        'prev_value' => ''
                    );
                } else {
                    if (isset($data[$ref_key]) && $ref_val != $data[$ref_key]) {
                        $newdata[] = array(
                            'type'       => 'field',
                            'variable'   => $ref_key,
                            'value'      => $ref_val,
                            'prev_value' => $data[$ref_key]
                        );
                    }
                }
            }
        }

        return $newdata;

    }

    public function offerAction()
    {
        //title
        $this->view->title = "Offer Program";

        $form = new Application_Form_Manual();

        $this->view->form = $form;

        echo $id = $this->_getParam('id', 0);

        $manual = new App_Model_Record_DbTable_Student();
        $applicant = $manual->getData($id);
        $this->view->applicant = $applicant;

        $appliedDB = new App_Model_Application_DbTable_AppliedProgram();
        $applied = $appliedDB->getAppliedProgram($id);
        $this->view->applied = $applied;
//
//    	if ($this->getRequest()->isPost()) {
//
//    		$formData = $this->getRequest()->getPost();
//
//	    	if ($form->isValid($formData)) {
//
//				$manual = new App_Model_Record_DbTable_Student();
//				$manual->updateData($formData,$id);
//
//				$this->_redirect($this->view->url(array('module'=>'application','controller'=>'manual', 'action'=>'index'),'default',true));
//			}else{
//				$form->populate($formData);
//			}
//    	}else{
//    		if($id>0){
//    			$manual = new App_Model_Record_DbTable_Student();
//    			$form->populate($manual->getData($id));
//    		}
//
//    	}
    }

    function deleteInfoAction()
    {

        $auth = Zend_Auth::getInstance();

        $sid = $this->_getParam('sid', 0);  //section id
        $pid = $this->_getParam('pid', 0);  //primary id for each table id to be deleted
        $type = $this->_getParam('type', 0);

        $txnid = $this->_getParam('txnid', 0);
        $idsection = $this->_getParam('idsection', 0);

        if ($type == 'qualification') {

            $qualificationDB = new App_Model_Application_DbTable_ApplicantQualification();
            $q = $qualificationDB->getQualification($pid);

            $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
            $documents = $appDocumentDB->getDataArray($txnid, $q['ae_id']);

            //delete file
            if (count($documents) > 0) {
                foreach ($documents as $doc) {
                    if (file_exists(DOCUMENT_PATH . $doc['ad_filepath'])) {
                        unlink(DOCUMENT_PATH . $doc['ad_filepath']);
                    }
                    //delete document info dtbase
                    $appDocumentDB->deleteData($doc['ad_id']);
                }
            }

            //delete data
            $qualificationDB->deleteData($pid);

        } elseif ($type == 'employement') {

            $appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();
            $e = $appEmploymentDB->getEmployement($pid);

            $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
            $documents = $appDocumentDB->getDataArray($txnid, $e['ae_id']);


            //delete file
            if (count($documents) > 0) {
                foreach ($documents as $doc) {
                    if (file_exists(DOCUMENT_PATH . $doc['ad_filepath'])) {
                        unlink(DOCUMENT_PATH . $doc['ad_filepath']);
                    }
                    //delete document info dtbase
                    $appDocumentDB->deleteData($doc['ad_id']);
                }
            }

            //delete data
            $appEmploymentDB->deleteData($pid);

        } elseif ($type == 'quantitative') {

            $qpDB = new App_Model_Application_DbTable_ApplicantQuantitativeProficiency();
            $qp = $qpDB->getData($pid);

            $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
            $documents = $appDocumentDB->getDataArray($txnid, $qp['qp_id'], 'applicant_quantitative_proficiency');

            //delete file
            if (count($documents) > 0) {
                foreach ($documents as $doc) {
                    if (file_exists(DOCUMENT_PATH . $doc['ad_filepath'])) {
                        unlink(DOCUMENT_PATH . $doc['ad_filepath']);
                    }
                    //delete document info dtbase
                    $appDocumentDB->deleteData($doc['ad_id']);
                }
            }

            //delete data
            $qpDB->deleteData($pid);


        } elseif ($type == 'research') {

            $rpdDB = new App_Model_Application_DbTable_ApplicantResearchPublication();
            $rpd = $rpdDB->getData($pid);

            $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
            $documents = $appDocumentDB->getDataArray($txnid, $rpd['rpd_id'], 'applicant_research_publication');

            //delete file
            if (count($documents) > 0) {
                foreach ($documents as $doc) {
                    if (file_exists(DOCUMENT_PATH . $doc['ad_filepath'])) {
                        unlink(DOCUMENT_PATH . $doc['ad_filepath']);
                    }
                    //delete document info dtbase
                    $appDocumentDB->deleteData($doc['ad_id']);
                }
            }

            //delete data
            $rpdDB->deleteData($pid);

        } elseif ($type == 'language') {

            $appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
            $engprof = $appEngProfDB->getDatabyId($pid);

            $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
            $documents = $appDocumentDB->getDataArray($txnid, $engprof['ep_id'], 'applicant_english_proficiency');

            //delete file
            if (count($documents) > 0) {
                foreach ($documents as $doc) {
                    if (file_exists(DOCUMENT_PATH . $doc['ad_filepath'])) {
                        unlink(DOCUMENT_PATH . $doc['ad_filepath']);
                    }
                    //delete document info dtbase
                    $appDocumentDB->deleteData($doc['ad_id']);
                }
            }

            //delete data
            $appEngProfDB->deleteData($pid);

        } elseif ($type == 'referee') {

            $refereeDB = new App_Model_Application_DbTable_ApplicantReferees();
            $refereeDB->deleteData($pid);
        }


        //redirect
        $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'index', 'action' => 'edit', 'id' => $txnid, 'idSection' => $idsection), 'default', true));

    }


    /*public function deleteAction($id = null){
    	$id = $this->_getParam('id', 0);

    	if($id>0){
    		$manual = new App_Model_Record_DbTable_Student();
    		$manual->deleteData($id);
    	}

    	$this->_redirect($this->view->url(array('module'=>'application','controller'=>'manual', 'action'=>'index'),'default',true));
    }*/

    public function deleteAttachmentAction($id = null)
    {
        $id = $this->_getParam('id', 0);
        $student_id = $this->_getParam('app_id', 0);

        //unlink('documents/Application/910629085797/910629085797_20110712_055933.jpg');

        if ($id > 0) {
            $uploadDB = new App_Model_Application_DbTable_UploadAttachment();

            $upload = $uploadDB->getDataUpload($student_id, $id);
            $ic = $upload["ARD_IC"];
            $fileupload = $upload["fileupload"];
            $path = "documents/Application/" . $ic . "/" . $fileupload;

            //delete physical file
            if (is_file($path)) {
                unlink($path);
            }

            //delete from database
            $uploadDB->deleteData($id);

        }

        $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'manual', 'action' => 'edit', 'id' => $student_id), 'default', true));

    }

    public function regenerateDocumentAction()
    {
        $txn_id = $this->_getParam('txn_id', 0);
        $document_id = $this->_getParam('doc_id', 0);

        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $txnData = $applicantTxnDB->getTransaction($txn_id);


        if ($document_id == 31) {
            $this->generatePssbConfimationLetter($txn_id);
        } else
            if ($document_id == 32) {
                $this->generateKartuBank($txn_id);
            } else
                if ($document_id == 45) {

                    if ($txnData["at_appl_type"] == 1) {
                        $this->generateUsmOfferLetterPDF($txn_id);
                    } elseif ($txnData["at_appl_type"] == 2) {
                        $this->generateOfferLetterPDF($txn_id);
                    }

                } else
                    if ($document_id == 30) {
                        $this->generateKartuUSM($txn_id);
                    } else
                        if ($document_id == 50) {
                            $this->generateAgreementLetter($txn_id);
                        }

        $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'index', 'action' => 'txn-detail', 'id' => $txn_id), 'default', true));
    }

    private function generateKartuUSM($txn_id)
    {
        $transaction_id = $txn_id;
        $profileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $applicant = $profileDB->viewkartu($transaction_id);

        $registry = Zend_Registry::getInstance();
        $locale = $registry->get('Zend_Locale');


        if ($applicant) {

            //print_r($applicant);exit;
            $billing_no = $applicant["billing_no"];
            $pin_no = $applicant["REGISTER_NO"];
            $applicantID = $applicant["at_pes_id"];

            $locadb = new App_Model_Application_DbTable_PlacementTestRoom();
            $room = $locadb->getdata($applicant["apt_room_id"]);

            $location = $applicant["apt_id"];


            //--------get applicant program  -----------
            $appprogramDB = new App_Model_Application_DbTable_ApplicantProgram();
            //$app_program = $appprogramDB->getPlacementProgram($transaction_id);
            $app_program = $appprogramDB->getProgramFaculty($transaction_id);

            $program_data["program_code1"] = "0";
            $program_data["program_code2"] = "0";
            $program_data["faculty_name2"] = "";
            $program_data["program_name2"] = "";

            //print_r($app_program);
            $i = 1;
            foreach ($app_program as $program) {
                //$program_data["program_name".$i] = $program["program_name"];
                $program_data["program_name" . $i] = $program["program_name_indonesia"];
                //$program_data["faculty_name".$i] = $program["faculty"];
                $program_data["faculty_name" . $i] = $program["faculty2"];
                $program_data["program_code" . $i] = $program["program_code"];

                $i++;
            }


            $documentDB = new App_Model_Application_DbTable_ApplicantDocument();
            $document = $documentDB->getData($transaction_id, 30); //kartu
            //$document=false;
            //print_r($document);exit;
            //if($document){


            //-------- get applicant photo --------
            $photo_name = '';
            $photoDB = new App_Model_Application_DbTable_ApplicantUploadFile();
            $photo = $photoDB->getTxnFile($transaction_id, 33); //PHoto

            if ($photo["auf_file_name"]) {
                $photo_name = $photo["auf_file_name"];
                $photodetail = $photo["pathupload"];

            }


            //------get program component info------
            $componentDB = new App_Model_Application_DbTable_ApplicantProgram();
            $rs_component = $componentDB->getComponentSchedulebytype($transaction_id, 1, $applicant["apt_aps_id"]);

            //print_r($rs_component);
            //$x=1;
            $comp['comp_program1'] = "";
            $comp['exam_date_time1'] = "";
            $comp['comp_program2'] = "";
            $comp['exam_date_time2'] = "";
            $comp['location_venue_sitno1'] = "";
            $comp['location_venue_sitno2'] = "";


            foreach ($rs_component as $component) {
                if ($locale == "en_US") {
                    $comp['comp_program1'] .= $component["ac_comp_name"] . ", ";
                } else if ($locale == "id_ID") {
                    $comp['comp_program1'] .= $component["ac_comp_name_bahasa"] . ", ";
                }
                //$comp['comp_program1'] .= $component["ac_comp_name_bahasa"].", ";

                $comp['exam_date_time1'] = date('M d Y', strtotime($component["aps_test_date"])) . ' ' . $component["ac_start_time"];
                $comp['location_venue_sitno1'] = $room["av_room_name"] . " - " . $applicant["apt_sit_no"];
                //$x++;
            }

            $rs_component2 = $componentDB->getComponentSchedulebytype($transaction_id, 2, $applicant["apt_aps_id"]);
            if ($rs_component2) {
                foreach ($rs_component2 as $component2) {
                    if ($locale == "en_US") {
                        $comp['comp_program2'] .= $component2["ac_comp_name"] . ", ";
                    } else if ($locale == "id_ID") {
                        $comp['comp_program2'] .= $component2["ac_comp_name_bahasa"] . ", ";
                    }
                    //$comp['comp_program2'] .= $component2["ac_comp_name"].", ";

                    $comp['exam_date_time2'] = date('M d Y', strtotime($component2["aps_test_date"])) . ' ' . $component2["ac_start_time"];
                    $comp['location_venue_sitno2'] = $room["av_room_name"] . " - " . $applicant["apt_sit_no"];
                    //$x++;
                }
            }
            //print_r($comp);exit;
            $fieldValues = array(
                '$[pinnumber]'             => $applicantID,
                '$[Applicantname]'         => $applicant["appl_fname"] . ' ' . $applicant["appl_mname"] . ' ' . $applicant["appl_lname"],
                '$[Address1]'              => $applicant["appl_address1"],
                '$[Address2]'              => $applicant["appl_address2"],
                '$[Mobilenumber]'          => $applicant["appl_phone_mobile"],
                '$[Facultyname1]'          => $program_data["faculty_name1"],
                '$[Facultyname2]'          => $program_data["faculty_name2"],
                '$[Programme1]'            => $program_data["program_name1"],
                '$[Programme2]'            => $program_data["program_name2"],
                '$[Comp_Program1]'         => $comp["comp_program1"],
                '$[Exam_Date_Time1]'       => $comp["exam_date_time1"],
                '$[location_venue_sitno1]' => $comp['location_venue_sitno1'],
                '$[Comp_Program2]'         => $comp["comp_program2"],
                '$[Exam_Date_Time2]'       => $comp["exam_date_time2"],
                '$[location_venue_sitno2]' => $comp['location_venue_sitno2'],
                '$[username]'              => $applicant["appl_email"],
                '$[password]'              => $applicant["appl_password"],
                '$[photodetail]'           => $photodetail
            );
            //print_r($fieldValues);exit;
            $monthyearfolder = date("mY");

            // ------ create kartu peserta ujian in PDF	----
            $filepath = DOCUMENT_PATH . "/template/kartu.docx";


            //directory to locate file
            $app_directory_path = DOCUMENT_PATH . "/applicant/" . $monthyearfolder;

            //create directory to locate file
            if (!is_dir($app_directory_path)) {
                mkdir($app_directory_path, 0775, true);
            }

            $output_directory_path = DOCUMENT_PATH . "/applicant/" . $monthyearfolder . "/" . $transaction_id;

            //create directory to locate file
            if (!is_dir($output_directory_path)) {
                mkdir($output_directory_path, 0775, true);
            }


            //$location_path
            $location_path = "applicant/" . $monthyearfolder . "/" . $transaction_id;


            //filename
            $output_filename = $applicantID . "_kartu.pdf";


            // ------- create PDF File section	--------
            try {
                require_once 'dompdf_config.inc.php';

                $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
                $autoloader->pushAutoloader('DOMPDF_autoload');

                //template path
                $html_template_path = DOCUMENT_PATH . "/template/kartu.html";

                $html = file_get_contents($html_template_path);

                //replace variable
                foreach ($fieldValues as $key => $value) {
                    $html = str_replace($key, $value, $html);
                }


                $dompdf = new DOMPDF();
                $dompdf->load_html($html);
                $dompdf->set_paper('a4', 'potrait');
                $dompdf->render();

                $dompdf = $dompdf->output();


                //to rename output file
                $output_file_path = $output_directory_path . "/" . $output_filename;

                file_put_contents($output_file_path, $dompdf);

                // ------- End PDF File section	--------

                $status = true;

            } catch (Exception $e) {
                $status = false;
            }

            //save file info
            $documentDB = new App_Model_Application_DbTable_ApplicantDocument();
            $fileexist = $documentDB->getDataArray($transaction_id, 30);

            $doc["ad_filepath"] = $location_path;
            $doc["ad_filename"] = $output_filename;
            $doc["ad_appl_id"] = $transaction_id;
            $doc["ad_type"] = 30;

            if (sizeof($fileexist) > 0) {
                //update file info
                $documentDB->updateDocument($doc, $transaction_id, 30);
            } else {

                $doc["ad_createddt"] = date("Y-m-d");

                $documentDB->addData($doc);
            }

            //}
        }
    }

    private function generatePssbConfimationLetter($txnId)
    {

        $registry = Zend_Registry::getInstance();
        $locale = $registry->get('Zend_Locale');

        $transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
        $txnData = $transactionDb->getTransaction($txnId);

        //get applicant profile
        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $applicant = $appProfileDB->getTransProfile($txnData['at_appl_id'], $txnId);

        //get next academic year
        $ayearDb = new App_Model_Record_DbTable_AcademicYear();
        $academic_year = $ayearDb->getNextAcademicYearData();

        //--------get applicant program  -----------
        $appprogramDB = new App_Model_Application_DbTable_ApplicantProgram();
        $app_program = $appprogramDB->getPlacementProgram($txnId);

        $program_data["program_code1"] = "0";
        $program_data["program_code2"] = "0";

        $i = 1;
        foreach ($app_program as $program) {
            $program_data["program_name" . $i] = $program["program_name"];
            $program_data["faculty_name" . $i] = $program["faculty"];
            $program_data["program_code" . $i] = $program["program_code"];

            $i++;
        }

        //filetype
        $fileType = 31;

        //template path
        $filepath = DOCUMENT_PATH . "/template/pssb_confirmation_letter.docx";

        //filename
        $output_filename = $txnData['at_pes_id'] . "_pssb_confirmation_letter.pdf";

        //pengumumam hasil seleksi
        setlocale(LC_ALL, $locale);

        //0=sunday onwards
        $today = date("w");

        if ($today <= 2) {
            $selection_date = strftime('%e %B %Y', strtotime("this Saturday")) . ', ' . strftime('%e %B %Y', strtotime("second Saturday")) . ' atau ' . strftime('%e %B %Y', strtotime("third Saturday"));
        } else {
            $selection_date = strftime('%e %B %Y', strtotime("second Saturday")) . ', ' . strftime('%e %B %Y', strtotime("third Saturday")) . ' atau ' . strftime('%e %B %Y', strtotime("fourth Saturday"));
        }

        if ($applicant["appl_gender"] == 1) $gender = "LAKI-LAKI";
        if ($applicant["appl_gender"] == 2) $gender = "PEREMPUAN";

        $fieldValues = array(
            'Applicantname'     => $applicant["appl_fname"] . ' ' . $applicant["appl_mname"] . ' ' . $applicant["appl_lname"],
            'dob'               => $applicant["appl_dob"],
            'Sex'               => $gender,
            'Address'           => $applicant["appl_address1"] . ',' . $applicant["appl_address2"],
            'phone'             => $applicant["appl_phone_mobile"],
            'email'             => $applicant["appl_email"],
            'Discipline'        => $applicant["discipline"],
            'PROGRAM1'          => $program_data["program_name1"],
            'submission_date'   => date('j M Y'),
            'ACADEMICYEAR'      => $academic_year["ay_code"],
            'registration_date' => date('j M Y'),
            'withdrawal_date'   => date('j M Y'),
            'seleksi_date'      => $selection_date
            // 'registration_date'=>$registrasi["StartDate"].' s.d '.$registrasi["EndDate"],
            // 'withdrawal_date'=>$withdrawal["StartDate"].' s.d '.$withdrawal["EndDate"]

        );

        $educationDB = new App_Model_Application_DbTable_ApplicantEducation();

        $education = $educationDB->getEducationDetail($txnId);

        if ($education == null) {
            $education = $educationDB->getEducationDetailApplId($applicant['appl_id']);
        }

        // ------- create PDF File section	--------


        //directory to locate file
        $app_directory_path = DOCUMENT_PATH . "/applicant/" . date("mY");

        //create directory to locate file
        if (!is_dir($app_directory_path)) {
            mkdir($app_directory_path, 0775, true);
        }

        $output_directory_path = DOCUMENT_PATH . "/applicant/" . date("mY") . "/" . $txnId;

        //create directory to locate file
        if (!is_dir($output_directory_path)) {
            mkdir($output_directory_path, 0775, true);
        }

        //$location_path
        $location_path = "applicant/" . date("mY") . "/" . $txnId;

        //to create PDF File
        $this->mailmergeConnection($filepath, $fieldValues, $education, $output_directory_path, $output_filename);

        // ------- End PDF File section	--------

        //update file info
        $documentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $fileexist = $documentDB->getDataArray($txnId, 31);

        $doc["ad_filepath"] = $location_path;
        $doc["ad_filename"] = $output_filename;
        $doc["ad_createddt"] = date("Y-m-d");


        if ($fileexist) {
            $documentDB->updateDocument($doc, $txnId, 31);
        } else {
            $doc['ad_appl_id'] = $txnId;
            $doc['ad_type'] = 31;

            $documentDB->addData($doc);
        }
    }

    private function generateKartuBank($txnId)
    {
        $transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
        $txnData = $transactionDb->getTransaction($txnId);

        //get applicant profile
        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $applicant = $appProfileDB->getTransProfile($txnData['at_appl_id'], $txnId);

        //get applicant parents info
        $appFamilyDB = new App_Model_Application_DbTable_ApplicantFamily();
        $applicant_family = $appFamilyDB->fetchdata($applicant['appl_id'], 21); //nak cari mother info

        //get plcement test date
        $scheduleDB = new App_Model_Application_DbTable_PlacementTestSchedule();
        $condition = array('schedule_id' => $applicant['schedule_id']);
        $placement = $scheduleDB->getInfo($condition);

        //get applicant program applied
        $programDB = new App_Model_Application_DbTable_ApplicantProgram();
        $app_program = $programDB->getPlacementProgram($txnId);

        $program_data["program_name1"] = '';
        $program_data["program_name2"] = '';
        $i = 1;
        foreach ($app_program as $program) {
            if ($applicant['appl_prefer_lang'] == 1) {
                $program_data["program_name" . $i] = $program["program_name"];
            } else
                if ($applicant['appl_prefer_lang'] == 2) {
                    $program_data["program_name" . $i] = $program["program_name_indonesia"];
                }

            $i++;
        }

        //template path
        $filepath = DOCUMENT_PATH . "/template/validasi_bank.docx";

        //filename
        $output_filename = $txnData['at_pes_id'] . "_validasi_bank.pdf";

        //field
        $fieldValues = array(
            'billno'        => $txnData['at_pes_id'],
            'fee'           => money_format('%i', $applicant["fee"]),
            'Applicantname' => $applicant["appl_fname"] . ' ' . $applicant["appl_mname"] . ' ' . $applicant["appl_lname"],
            'Address1'      => $applicant["appl_address1"],
            'Address2'      => $applicant["appl_address2"],
            'mobilenumber'  => $applicant["appl_phone_mobile"],
            'Kodya'         => $applicant["appl_address2"],
            'dob'           => $applicant["appl_dob"],
            'email'         => $applicant["appl_email"],
            'Mothername'    => $applicant_family["af_name"],
            'Schoolname'    => $applicant["discipline"],//jurusan yg dipilih
            'TestDate'      => date('j M Y', strtotime($placement["aps_test_date"])),
            'Location'      => $placement["location_name"],
            'PROGRAM1'      => $program_data["program_name1"],
            'PROGRAM2'      => $program_data["program_name2"]
        );


        // ------- create PDF File section	--------
        //directory to locate file
        $app_directory_path = DOCUMENT_PATH . "/applicant/" . date("mY");

        //create directory to locate file
        if (!is_dir($app_directory_path)) {
            mkdir($app_directory_path, 0775, true);
        }

        $output_directory_path = DOCUMENT_PATH . "/applicant/" . date("mY") . "/" . $txnId;

        //create directory to locate file
        if (!is_dir($output_directory_path)) {
            mkdir($output_directory_path, 0775, true);
        }

        //$location_path
        $location_path = "applicant/" . date("mY") . "/" . $txnId;


        //to create PDF File
        $this->mailmerge($filepath, $fieldValues, $output_directory_path, $output_filename);

        // ------- End PDF File section	--------

        //update file info
        $documentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $doc["ad_filepath"] = $location_path;
        $doc["ad_filename"] = $output_filename;
        $doc["ad_createddt"] = date("Y-m-d");

        $fileexist = $documentDB->getDataArray($txnId, 32);


        if ($fileexist) {
            $documentDB->updateDocument($doc, $txnId, 32);
        } else {
            $doc['ad_appl_id'] = $txnId;
            $doc['ad_type'] = 32;

            $documentDB->addData($doc);
        }
    }

    private function generateOfferLetter_old($txnId)
    {
        //get applicant info
        $applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
        $applicant = $applicantDB->getAllProfile($txnId);

        //get transaction info
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $txnData = $applicantTxnDB->getTransaction($txnId);

        //get assessment data
        $assessmentDb = new App_Model_Application_DbTable_ApplicantAssessment();
        $assessmentData = $assessmentDb->getData($txnId);

        //rank
        if ($assessmentData['aar_rating_rector'] == 1) {
            $rank = "1 (Satu)";
        } else
            if ($assessmentData['aar_rating_rector'] == 2) {
                $rank = "2 (Dua)";
            } else
                if ($assessmentData['aar_rating_rector'] == 3) {
                    $rank = "3 (Tiga)";
                }


        //getapplicantprogram
        $appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
        $program = $appProgramDB->getProgramFaculty($txnId);

        //get applicant parents info
        $familyDB = new App_Model_Application_DbTable_ApplicantFamily();
        $father = $familyDB->fetchdata($applicant["appl_id"], 20); //father's

        //get next intake
        $intakeDb = new GeneralSetup_Model_DbTable_Intake();
        $intakeData = $intakeDb->fngetIntakeDetails(78);

        //get fee structure
        //TODO:check local of foreign
        $feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
        $feeStructureData = $feeStructureDb->getApplicantFeeStructure($intakeData['IdIntake'], $program[0]["program_id"]);

        //fee structure plan
        $feeStructurePlanDb = new Studentfinance_Model_DbTable_FeeStructurePlan();
        $paymentPlanData = $feeStructurePlanDb->getStructureData($feeStructureData['fs_id']);
        $feeStructureData['payment_plan'] = $paymentPlanData;

        //fee structure program
        $feeStructureProgramDb = new Studentfinance_Model_DbTable_FeeStructureProgram();
        $feeStructureProgramData = $feeStructureProgramDb->getStructureData($feeStructureData['fs_id'], $program[0]["program_id"]);

        //fee structure plan detail
        $fspdDb = new Studentfinance_Model_DbTable_FeeStructurePlanDetail();
        /*
		$i=0;
		foreach ($feeStructureData['payment_plan'] as $plan){

			for($installment=1; $installment<=$plan['fsp_bil_installment']; $installment++){
				$feeStructureData['payment_plan'][$i]['plan_detail'][$installment] = $fspdDb->getPlanData($plan['fsp_structure_id'], $plan['fsp_id'], $installment);
			}


			$i++;
		}*/


        /*
		 * paket A
		 */
        $paket_a_plan = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[0]['fsp_id'], 1, 1, $feeStructureProgramData['fsp_program_id'], $assessmentData['aar_rating_rector']);


        /*
		 * paket B
		 */
        $paket_b_plan_cicilan1 = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[1]['fsp_id'], 1, 1, $feeStructureProgramData['fsp_program_id'], $assessmentData['aar_rating_rector']);
        $paket_b_plan_cicilan2 = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[1]['fsp_id'], 2, 1, $feeStructureProgramData['fsp_program_id'], $assessmentData['aar_rating_rector']);
        $paket_b_plan_cicilan3 = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[1]['fsp_id'], 3, 1, $feeStructureProgramData['fsp_program_id'], $assessmentData['aar_rating_rector']);
        $paket_b_plan_cicilan4 = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[1]['fsp_id'], 4, 1, $feeStructureProgramData['fsp_program_id'], $assessmentData['aar_rating_rector']);
        $paket_b_plan_cicilan5 = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[1]['fsp_id'], 5, 1, $feeStructureProgramData['fsp_program_id'], $assessmentData['aar_rating_rector']);
        $paket_b_plan_cicilan6 = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[1]['fsp_id'], 6, 1, $feeStructureProgramData['fsp_program_id'], $assessmentData['aar_rating_rector']);

        /*echo "<pre>";
		print_r($paket_b_plan_cicilan1);
		echo "<pre>";
		exit;*/

        //create image
        //$this->createImage();


        //$nomor = '010/AK.4.02/PSSB-BAA/Usakti/WR.I/I-3/2012';
        $nomor = $assessmentData['asd_nomor'];


        $fieldValues = array(
            'applicantID'            => $txnData["at_pes_id"],
            'NOMOR'                  => $nomor,
            'lampiran'               => "-",
            'Title_Template'         => $this->view->translate("Pemberitahuan diterima sebagai calon Mahasiswa di Universitas Trisakti"),
            'APPLICANTNAME'          => $applicant["appl_fname"] . ' ' . $applicant["appl_mname"] . ' ' . $applicant["appl_lname"],
            'PARENTNAME'             => $father["af_name"],
            'Address1'               => $applicant["appl_address1"],
            'Address2'               => $applicant["appl_address2"],
            'City'                   => $applicant["CityName"],
            'Postcode'               => $applicant["appl_postcode"],
            'State'                  => $applicant["StateName"],
            'semester_offer'         => $txnData['ay_code'],
            'period_offer'           => $txnData['ap_desc'],
            'faculty'                => $program[0]["faculty2"],
            'programme'              => $program[0]["program_name_indonesia"],
            'rank'                   => $rank,
            'print_date'             => date('j M Y'),
            'paket_a_date_payment'   => date('j F Y', strtotime($assessmentData['aar_payment_start_date'])),
            'paket_a_sp'             => number_format($paket_a_plan[0]['total_amount'], 2, '.', ','),
            'paket_a_bpp_pokok'      => number_format($paket_a_plan[1]['total_amount'], 2, '.', ','),
            'paket_a_bpp_sks'        => $feeStructureProgramData['fsp_first_sem_sks'],
            'paket_a_bpp_sks_value'  => number_format($paket_a_plan[2]['fsi_amount'], 2, '.', ','),
            'paket_a_bpp_sks_amount' => number_format($paket_a_plan[2]['total_amount'], 2, '.', ','),
            'paket_a_praktikum'      => number_format($paket_a_plan[3]['total_amount'], 2, '.', ','),
            'paket_a_total'          => number_format($paket_a_plan[0]['total_amount'] + $paket_a_plan[1]['total_amount'] + $paket_a_plan[2]['total_amount'] + $paket_a_plan[3]['total_amount'], 2, '.', ','),

            'paket_b_c1_date_payment'   => date('j F Y', strtotime($assessmentData['aar_payment_start_date'])),
            'paket_b_c1_sp'             => number_format($paket_b_plan_cicilan1[0]['total_amount'], 2, '.', ','),
            'paket_b_c1_bpp_pokok'      => number_format($paket_b_plan_cicilan1[1]['total_amount'], 2, '.', ','),
            'paket_b_c1_bpp_sks'        => $feeStructureProgramData['fsp_first_sem_sks'],
            'paket_b_c1_bpp_sks_value'  => number_format($paket_b_plan_cicilan1[2]['fsi_amount'], 2, '.', ','),
            'paket_b_c1_bpp_sks_amount' => number_format($paket_b_plan_cicilan1[2]['total_amount'], 2, '.', ','),
            'paket_b_c1_praktikum'      => number_format($paket_b_plan_cicilan1[3]['total_amount'], 2, '.', ','),
            'paket_b_c1_total'          => number_format($paket_b_plan_cicilan1[0]['total_amount'] + $paket_b_plan_cicilan1[1]['total_amount'] + $paket_b_plan_cicilan1[2]['total_amount'] + $paket_b_plan_cicilan1[3]['total_amount'], 2, '.', ','),

            'paket_b_c2_date_payment'   => date('F Y', strtotime('+1 month', strtotime($assessmentData['aar_payment_start_date']))),
            'paket_b_c2_sp'             => number_format($paket_b_plan_cicilan2[0]['total_amount'], 2, '.', ','),
            'paket_b_c2_bpp_pokok'      => number_format($paket_b_plan_cicilan2[1]['total_amount'], 2, '.', ','),
            'paket_b_c2_bpp_sks'        => $feeStructureProgramData['fsp_first_sem_sks'],
            'paket_b_c2_bpp_sks_value'  => number_format($paket_b_plan_cicilan2[2]['fsi_amount'], 2, '.', ','),
            'paket_b_c2_bpp_sks_amount' => number_format($paket_b_plan_cicilan2[2]['total_amount'], 2, '.', ','),
            'paket_b_c2_praktikum'      => number_format($paket_b_plan_cicilan2[3]['total_amount'], 2, '.', ','),
            'paket_b_c2_total'          => number_format($paket_b_plan_cicilan2[0]['total_amount'] + $paket_b_plan_cicilan2[1]['total_amount'] + $paket_b_plan_cicilan2[2]['total_amount'] + $paket_b_plan_cicilan2[3]['total_amount'], 2, '.', ','),

            'paket_b_c3_date_payment'   => date('F Y', strtotime('+2 month', strtotime($assessmentData['aar_payment_start_date']))),
            'paket_b_c3_sp'             => number_format($paket_b_plan_cicilan3[0]['total_amount'], 2, '.', ','),
            'paket_b_c3_bpp_pokok'      => number_format($paket_b_plan_cicilan3[1]['total_amount'], 2, '.', ','),
            'paket_b_c3_bpp_sks'        => $feeStructureProgramData['fsp_first_sem_sks'],
            'paket_b_c3_bpp_sks_value'  => number_format($paket_b_plan_cicilan3[2]['fsi_amount'], 2, '.', ','),
            'paket_b_c3_bpp_sks_amount' => number_format($paket_b_plan_cicilan3[2]['total_amount'], 2, '.', ','),
            'paket_b_c3_praktikum'      => number_format($paket_b_plan_cicilan3[3]['total_amount'], 2, '.', ','),
            'paket_b_c3_total'          => number_format($paket_b_plan_cicilan3[0]['total_amount'] + $paket_b_plan_cicilan3[1]['total_amount'] + $paket_b_plan_cicilan3[2]['total_amount'] + $paket_b_plan_cicilan3[3]['total_amount'], 2, '.', ','),

            'paket_b_c4_date_payment'   => date('F Y', strtotime('+3 month', strtotime($assessmentData['aar_payment_start_date']))),
            'paket_b_c4_sp'             => number_format($paket_b_plan_cicilan4[0]['total_amount'], 2, '.', ','),
            'paket_b_c4_bpp_pokok'      => number_format($paket_b_plan_cicilan4[1]['total_amount'], 2, '.', ','),
            'paket_b_c4_bpp_sks'        => $feeStructureProgramData['fsp_first_sem_sks'],
            'paket_b_c4_bpp_sks_value'  => number_format($paket_b_plan_cicilan4[2]['fsi_amount'], 2, '.', ','),
            'paket_b_c4_bpp_sks_amount' => number_format($paket_b_plan_cicilan4[2]['total_amount'], 2, '.', ','),
            'paket_b_c4_praktikum'      => number_format($paket_b_plan_cicilan4[3]['total_amount'], 2, '.', ','),
            'paket_b_c4_total'          => number_format($paket_b_plan_cicilan4[0]['total_amount'] + $paket_b_plan_cicilan4[1]['total_amount'] + $paket_b_plan_cicilan4[2]['total_amount'] + $paket_b_plan_cicilan4[3]['total_amount'], 2, '.', ','),

            'paket_b_c5_date_payment'   => date('F Y', strtotime('+4 month', strtotime($assessmentData['aar_payment_start_date']))),
            'paket_b_c5_sp'             => number_format($paket_b_plan_cicilan5[0]['total_amount'], 2, '.', ','),
            'paket_b_c5_bpp_pokok'      => number_format($paket_b_plan_cicilan5[1]['total_amount'], 2, '.', ','),
            'paket_b_c5_bpp_sks'        => $feeStructureProgramData['fsp_first_sem_sks'],
            'paket_b_c5_bpp_sks_value'  => number_format($paket_b_plan_cicilan5[2]['fsi_amount'], 2, '.', ','),
            'paket_b_c5_bpp_sks_amount' => number_format($paket_b_plan_cicilan5[2]['total_amount'], 2, '.', ','),
            'paket_b_c5_praktikum'      => number_format($paket_b_plan_cicilan5[3]['total_amount'], 2, '.', ','),
            'paket_b_c5_total'          => number_format($paket_b_plan_cicilan5[0]['total_amount'] + $paket_b_plan_cicilan5[1]['total_amount'] + $paket_b_plan_cicilan5[2]['total_amount'] + $paket_b_plan_cicilan5[3]['total_amount'], 2, '.', ','),

            'paket_b_c6_date_payment'   => date('F Y', strtotime('+5 month', strtotime($assessmentData['aar_payment_start_date']))),
            'paket_b_c6_sp'             => number_format($paket_b_plan_cicilan6[0]['total_amount'], 2, '.', ','),
            'paket_b_c6_bpp_pokok'      => number_format($paket_b_plan_cicilan6[1]['total_amount'], 2, '.', ','),
            'paket_b_c6_bpp_sks'        => $feeStructureProgramData['fsp_first_sem_sks'],
            'paket_b_c6_bpp_sks_value'  => number_format($paket_b_plan_cicilan6[2]['fsi_amount'], 2, '.', ','),
            'paket_b_c6_bpp_sks_amount' => number_format($paket_b_plan_cicilan6[2]['total_amount'], 2, '.', ','),
            'paket_b_c6_praktikum'      => number_format($paket_b_plan_cicilan6[3]['total_amount'], 2, '.', ','),
            'paket_b_c6_total'          => number_format($paket_b_plan_cicilan6[0]['total_amount'] + $paket_b_plan_cicilan6[1]['total_amount'] + $paket_b_plan_cicilan6[2]['total_amount'] + $paket_b_plan_cicilan6[3]['total_amount'], 2, '.', ','),

            'balance_installment_paket_b' => number_format(($paket_a_plan[0]['total_amount'] + $paket_a_plan[1]['total_amount'] + $paket_a_plan[2]['total_amount'] + $paket_a_plan[3]['total_amount']) - ($paket_b_plan_cicilan1[0]['total_amount'] + $paket_b_plan_cicilan1[1]['total_amount'] + $paket_b_plan_cicilan1[2]['total_amount'] + $paket_b_plan_cicilan1[3]['total_amount']), 2, '.', ',')
        );

        //template file path
        $filepath = DOCUMENT_PATH . "/template/offer_letter.doc";

        // ------- create PDF File section	--------


        //directory to locate file
        $app_directory_path = DOCUMENT_PATH . "/applicant/" . date("mY");

        //create directory to locate file
        if (!is_dir($app_directory_path)) {
            mkdir($app_directory_path, 0775, true);
        }

        $output_directory_path = DOCUMENT_PATH . "/applicant/" . date("mY") . "/" . $txnId;

        //create directory to locate file
        if (!is_dir($output_directory_path)) {
            mkdir($output_directory_path, 0775, true);
        }

        //$location_path
        $location_path = "applicant/" . date("mY") . "/" . $txnId;


        //output filename
        $output_filename = $txnData["at_pes_id"] . "_offer_letter.pdf";

        //to rename output file
        $output_file_path = $output_directory_path . "/" . $output_filename;

        //$this->mailmerge($filepath,$fieldValues,$output_directory_path,$output_filename,null);

        //create PDF File
        $mailMerge = new Zend_Service_LiveDocx_MailMerge();

        $mailMerge->setUsername('yatie')
            ->setPassword('al_hasib');

        $mailMerge->setLocalTemplate($filepath);

        $mailMerge->setFieldValues($fieldValues);

        $mailMerge->createDocument();

        $document = $mailMerge->retrieveDocument('pdf');

        //create directory to locate file
        if (!is_dir($output_directory_path)) {
            mkdir($output_directory_path, 0775, true);
        }

        //to rename output file
        $output_file_path = $output_directory_path . "/" . $output_filename;

        file_put_contents($output_file_path, $document);

        //update file info
        $documentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $fileexist = $documentDB->getDataArray($txnId, 45);

        $doc["ad_filepath"] = $location_path;
        $doc["ad_filename"] = $output_filename;
        $doc["ad_appl_id"] = $txnId;
        $doc["ad_type"] = 45;

        if ($fileexist) {
            $documentDB->updateDocument($doc, $txnId, 45);
        } else {
            $documentDB->addData($doc);
        }

    }

    private function generateOfferLetterPDF($txnId)
    {

        $offerleter = new icampus_Function_Application_Offerletter();

        return $offerleter->generateOfferLetter($txnId);
    }

    private function generateUsmOfferLetterPDF($txnId)
    {

        $offerleter = new icampus_Function_Application_Offerletter();

        return $offerleter->generateUsmOfferLetter($txnId);

    }


    private function generateUsmOfferLetterPDF_old($txnId)
    {

        //get applicant info
        $applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
        $applicant = $applicantDB->getAllProfile($txnId);

        //get transaction info
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $txnData = $applicantTxnDB->getTransaction($txnId);

        //get assessment data
        $assessmentDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
        $assessmentData = $assessmentDb->getData($txnId);

        //getapplicantprogram
        $appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
        $program = $appProgramDB->getUsmOfferProgram($txnId);

        //program data
        $programDb = new GeneralSetup_Model_DbTable_Program();
        $programData = $programDb->fngetProgramData($program['program_id']);


        //award type
        $award = "";

        if ($programData['Award'] == 36) {
            $award = "D3";
        } else
            if ($programData['Award'] == 363) {
                $award = "D4";
            } else {
                $award = "S1";
            }

        $learning_duration = $award . " = " . $programData['OptimalDuration'] . " Semester";


        //rank
        if ($assessmentData['aau_rector_ranking'] == 1) {
            $rank = "1 (Satu)";
            $biaya = $programData['Estimate_Fee_R1'] != null ? number_format($programData['Estimate_Fee_R1'], 2, '.', ',') : "";
        } else
            if ($assessmentData['aau_rector_ranking'] == 2) {
                $rank = "2 (Dua)";
                $biaya = $programData['Estimate_Fee_R2'] != null ? number_format($programData['Estimate_Fee_R2'], 2, '.', ',') : "";
            } else
                if ($assessmentData['aau_rector_ranking'] == 3) {
                    $rank = "3 (Tiga)";
                    $biaya = $programData['Estimate_Fee_R3'] != null ? number_format($programData['Estimate_Fee_R3'], 2, '.', ',') : "";
                }


        //faculty data
        $collegeMasterDb = new GeneralSetup_Model_DbTable_Collegemaster();
        $facultyData = $collegeMasterDb->fngetCollegemasterData($program['faculty_id']);

        //get applicant parents info
        $familyDB = new App_Model_Application_DbTable_ApplicantFamily();
        $father = $familyDB->fetchdata($applicant["appl_id"], 20); //father's

        //get next intake
        $intakeDb = new GeneralSetup_Model_DbTable_Intake();
        $intakeData = $intakeDb->fngetIntakeDetails($txnData['at_intake']);


        //get fee structure
        //TODO:check local or foreign
        $nationality = $applicant["appl_nationality"];
        if ($nationality == 1 || $nationality == 0) {
            $citizen = 314;
        } else {
            $citizen = 315;
        }
        $feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
        $feeStructureData = $feeStructureDb->getApplicantFeeStructure($intakeData[0]['IdIntake'], $program["program_id"], $citizen);


        //fee structure plan
        $feeStructurePlanDb = new Studentfinance_Model_DbTable_FeeStructurePlan();
        $paymentPlanData = $feeStructurePlanDb->getStructureData($feeStructureData['fs_id']);
        $feeStructureData['payment_plan'] = $paymentPlanData;


        //fee structure program
        $feeStructureProgramDb = new Studentfinance_Model_DbTable_FeeStructureProgram();
        $feeStructureProgramData = $feeStructureProgramDb->getStructureData($feeStructureData['fs_id'], $program["program_id"]);


        //fee structure plan detail
        $fspdDb = new Studentfinance_Model_DbTable_FeeStructurePlanDetail();

        foreach ($feeStructureData['payment_plan'] as $key => $plan) {

            for ($installment = 1; $installment <= $plan['fsp_bil_installment']; $installment++) {
                $feeStructureData['payment_plan'][$key]['plan_detail'][$installment] = $fspdDb->getPlanData($plan['fsp_structure_id'], $plan['fsp_id'], $installment, 1, $feeStructureProgramData['fsp_program_id'], $assessmentData['aau_rector_ranking']);
            }
        }


        /*
		 * paket A
		 */
        //$paket_a_plan = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[0]['fsp_id'], 1, 1,$feeStructureProgramData['fsp_program_id'],$assessmentData['aau_rector_ranking']);


        /*
		 * paket B
		 */
        //$paket_b_plan_cicilan1 = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[1]['fsp_id'], 1, 1,$feeStructureProgramData['fsp_program_id'],$assessmentData['aau_rector_ranking']);
        //$paket_b_plan_cicilan2 = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[1]['fsp_id'], 2, 1,$feeStructureProgramData['fsp_program_id'],$assessmentData['aau_rector_ranking']);
        //$paket_b_plan_cicilan3 = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[1]['fsp_id'], 3, 1,$feeStructureProgramData['fsp_program_id'],$assessmentData['aau_rector_ranking']);
        //$paket_b_plan_cicilan4 = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[1]['fsp_id'], 4, 1,$feeStructureProgramData['fsp_program_id'],$assessmentData['aau_rector_ranking']);
        //$paket_b_plan_cicilan5 = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[1]['fsp_id'], 5, 1,$feeStructureProgramData['fsp_program_id'],$assessmentData['aau_rector_ranking']);
        //$paket_b_plan_cicilan6 = $fspdDb->getPlanData($feeStructureData['fs_id'], $paymentPlanData[1]['fsp_id'], 6, 1,$feeStructureProgramData['fsp_program_id'],$assessmentData['aau_rector_ranking']);

        /*echo "<pre>";
		print_r($paket_b_plan_cicilan1);
		echo "<pre>";
		exit;*/

        //create image
        //$this->createImage();


        //$nomor = '010/AK.4.02/PSSB-BAA/Usakti/WR.I/I-3/2012';
        $nomor = $assessmentData['aaud_nomor'];

        $address = "";
        if (isset($applicant["appl_address1"]) && $applicant["appl_address1"] != "") {
            $address = $address . $applicant["appl_address1"] . "<br />";
        }
        if (isset($applicant["appl_address2"]) && $applicant["appl_address2"] != "") {
            $address = $address . $applicant["appl_address2"] . "<br />";
        }
        if (isset($applicant["CityName"]) && $applicant["CityName"] != "") {
            $address = $address . $applicant["CityName"] . "<br />";
        }
        if (isset($applicant["appl_postcode"]) && $applicant["appl_postcode"] != "") {
            $address = $address . $applicant["appl_postcode"] . "<br />";
        }
        if (isset($applicant["StateName"]) && $applicant["StateName"] != "") {
            $address = $address . $applicant["StateName"] . "<br />";
        }

        $fieldValues = array(
            '$[NO_PES]'                  => $txnData["at_pes_id"],
            '$[NOMOR]'                   => $nomor,
            '$[LAMPIRAN]'                => "-",
            '$[TITLE_TEMPLATE]'          => $this->view->translate("Pemberitahuan diterima sebagai calon Mahasiswa di Universitas Trisakti"),
            '$[APPLICANT_NAME]'          => $applicant["appl_fname"] . ' ' . $applicant["appl_mname"] . ' ' . $applicant["appl_lname"],
            '$[PARENTNAME]'              => $father["af_name"],
            '$[ADDRESS]'                 => $address,
            '$ADDRESS1]'                 => $applicant["appl_address1"],
            '$ADDRESS2]'                 => $applicant["appl_address2"],
            '$[CITY]'                    => $applicant["CityName"],
            '$[POSTCODE]'                => $applicant["appl_postcode"],
            '$[STATE]'                   => $applicant["StateName"],
            '$[ACADEMIC_YEAR]'           => $txnData['ay_code'],
            '$[PERIOD]'                  => $txnData['ap_desc'],
            '$[FACULTY]'                 => $program["faculty2"],
            '$[FACULTY_NAME]'            => ($facultyData['ArabicName'] != null ? $facultyData['ArabicName'] . " " : "-"),
            '$[FACULTY_SHORTNAME]'       => ($facultyData['ShortName'] != null ? $facultyData['ShortName'] . " " : "-"),
            '$[FACULTY_ADDRESS1]'        => ($facultyData['Add1'] != null ? $facultyData['Add1'] . " " : "-"),
            '$[FACULTY_ADDRESS2]'        => ($facultyData['Add2'] != null ? $facultyData['Add2'] . " " : ""),
            '$[FACULTY_ADDRESS]'         => ($facultyData['Add1'] != null ? $facultyData['Add1'] . " " : "") . ($facultyData['Add2'] != null ? $facultyData['Add2'] . " " : ""),
            '$[FACULTY_PHONE]'           => ($facultyData['Phone1'] != null ? $facultyData['Phone1'] . " " : "") . ($facultyData['Phone2'] != null ? ", " . $facultyData['Phone2'] . " " : ""),
            '$[FACULTY_FAX]'             => ($facultyData['Fax'] != null ? $facultyData['Fax'] . " " : ""),
            '$[PROGRAME]'                => $program["program_name_indonesia"],
            '$[RANK]'                    => $rank,
            '$[PRINT_DATE]'              => date('j M Y'),
            '$[REGISTRATION_DATE_START]' => date('j F Y', strtotime($assessmentData['aaud_reg_start_date'])),
            '$[REGISTRATION_DATE_END]'   => date('j F Y', strtotime($assessmentData['aaud_reg_end_date'])),
            /*'$[PAKET_A_DATE_PAYMENT]'=> date ( 'j F Y' , strtotime ( $assessmentData['aaud_payment_start_date'] ) ),
				'$[PAKET_A_SP]' => number_format($paket_a_plan[0]['total_amount'], 2, '.', ','),
				'$[PAKET_A_BPP_POKOK]' => number_format($paket_a_plan[1]['total_amount'], 2, '.', ','),
				'$[PAKET_A_BPP_SKS]' => $feeStructureProgramData['fsp_first_sem_sks'],
				'$[PAKET_A_BPP_SKS_VALUE]' => number_format($paket_a_plan[2]['fsi_amount'], 2, '.', ','),
				'$[PAKET_A_BPP_SKS_AMOUNT]' => number_format($paket_a_plan[2]['total_amount'], 2, '.', ','),
				'$[PAKET_A_PRAKTIKUM]' => number_format($paket_a_plan[3]['total_amount'], 2, '.', ','),
				'$[PAKET_A_TOTAL]' => number_format($paket_a_plan[0]['total_amount'] + $paket_a_plan[1]['total_amount'] + $paket_a_plan[2]['total_amount'] + $paket_a_plan[3]['total_amount'], 2, '.', ',') ,

				'$[PAKET_B_C1_DATE_PAYMENT]'=>date ( 'j F Y' , strtotime ( $assessmentData['aaud_payment_start_date'] ) ),
				'$[PAKET_B_C1_SP]' => number_format($paket_b_plan_cicilan1[0]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C1_BPP_POKOK]' => number_format($paket_b_plan_cicilan1[1]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C1_BPP_SKS]' => $feeStructureProgramData['fsp_first_sem_sks'],
				'$[PAKET_B_C1_BPP_SKS_VALUE]' => number_format($paket_b_plan_cicilan1[2]['fsi_amount'], 2, '.', ','),
				'$[PAKET_B_C1_BPP_SKS_AMOUNT]' => number_format($paket_b_plan_cicilan1[2]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C1_PRAKTIKUM]' => number_format($paket_b_plan_cicilan1[3]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C1_TOTAL]' => number_format($paket_b_plan_cicilan1[0]['total_amount'] + $paket_b_plan_cicilan1[1]['total_amount'] + $paket_b_plan_cicilan1[2]['total_amount'] + $paket_b_plan_cicilan1[3]['total_amount'], 2, '.', ',') ,

				'$[PAKET_B_C2_DATE_PAYMENT]'=>date ( 'F Y' , strtotime ( '+1 month' , strtotime ( $assessmentData['aaud_reg_start_date'] ) ) ),
				'$[PAKET_B_C2_SP]' => number_format($paket_b_plan_cicilan2[0]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C2_BPP_POKOK]' => number_format($paket_b_plan_cicilan2[1]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C2_BPP_SKS]' => $feeStructureProgramData['fsp_first_sem_sks'],
				'$[PAKET_B_C2_BPP_SKS_VALUE]' => number_format($paket_b_plan_cicilan2[2]['fsi_amount'], 2, '.', ','),
				'$[PAKET_B_C2_BPP_SKS_AMOUNT]' => number_format($paket_b_plan_cicilan2[2]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C2_PRAKTIKUM]' => number_format($paket_b_plan_cicilan2[3]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C2_TOTAL]' => number_format($paket_b_plan_cicilan2[0]['total_amount'] + $paket_b_plan_cicilan2[1]['total_amount'] + $paket_b_plan_cicilan2[2]['total_amount'] + $paket_b_plan_cicilan2[3]['total_amount'], 2, '.', ',') ,

				'$[PAKET_B_C3_DATE_PAYMENT]'=>date ( 'F Y' , strtotime ( '+2 month' , strtotime ( $assessmentData['aaud_reg_end_date'] ) ) ),
				'$[PAKET_B_C3_SP]' => number_format($paket_b_plan_cicilan3[0]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C3_BPP_POKOK]' => number_format($paket_b_plan_cicilan3[1]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C3_BPP_SKS]' => $feeStructureProgramData['fsp_first_sem_sks'],
				'$[PAKET_B_C3_SKS_VALUE]' => number_format($paket_b_plan_cicilan3[2]['fsi_amount'], 2, '.', ','),
				'$[PAKET_B_C3_BPP_SKS_AMOUNT]' => number_format($paket_b_plan_cicilan3[2]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C3_PRAKTIKUM]' => number_format($paket_b_plan_cicilan3[3]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C3_TOTAL]' => number_format($paket_b_plan_cicilan3[0]['total_amount'] + $paket_b_plan_cicilan3[1]['total_amount'] + $paket_b_plan_cicilan3[2]['total_amount'] + $paket_b_plan_cicilan3[3]['total_amount'], 2, '.', ',') ,

				'$[PAKET_B_C4_DATE_PAYMENT]'=>date ( 'F Y' , strtotime ( '+3 month' , strtotime ( $assessmentData['aaud_reg_end_date'] ) ) ),
				'$[PAKET_B_C4_SP]' => number_format($paket_b_plan_cicilan4[0]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C4_BPP_POKOK]' => number_format($paket_b_plan_cicilan4[1]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C4_BPP_SKS]' => $feeStructureProgramData['fsp_first_sem_sks'],
				'$[PAKET_B_C4_SKS_VALUE]' => number_format($paket_b_plan_cicilan4[2]['fsi_amount'], 2, '.', ','),
				'$[PAKET_B_C4_BPP_SKS_AMOUNT]' => number_format($paket_b_plan_cicilan4[2]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C4_PRAKTIKUM]' => number_format($paket_b_plan_cicilan4[3]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C4_TOTAL]' => number_format($paket_b_plan_cicilan4[0]['total_amount'] + $paket_b_plan_cicilan4[1]['total_amount'] + $paket_b_plan_cicilan4[2]['total_amount'] + $paket_b_plan_cicilan4[3]['total_amount'], 2, '.', ',') ,

				'$[PAKET_B_C5_DATE_PAYMENT]'=>date ( 'F Y' , strtotime ( '+4 month' , strtotime ( $assessmentData['aaud_reg_end_date'] ) ) ),
				'$[PAKET_B_C5_SP]' => number_format($paket_b_plan_cicilan5[0]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C5_BPP_POKOK]' => number_format($paket_b_plan_cicilan5[1]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C5_BPP_SKS]' => $feeStructureProgramData['fsp_first_sem_sks'],
				'$[PAKET_B_C5_SKS_VALUE]' => number_format($paket_b_plan_cicilan5[2]['fsi_amount'], 2, '.', ','),
				'$[PAKET_B_C5_BPP_SKS_AMOUNT]' => number_format($paket_b_plan_cicilan5[2]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C5_PRAKTIKUM]' => number_format($paket_b_plan_cicilan5[3]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C5_TOTAL]' => number_format($paket_b_plan_cicilan5[0]['total_amount'] + $paket_b_plan_cicilan5[1]['total_amount'] + $paket_b_plan_cicilan5[2]['total_amount'] + $paket_b_plan_cicilan5[3]['total_amount'], 2, '.', ',') ,

				'$[PAKET_B_C6_DATE_PAYMENT]'=>date ( 'F Y' , strtotime ( '+5 month' , strtotime ( $assessmentData['aaud_reg_end_date'] ) ) ),
				'$[PAKET_B_C6_SP]' => number_format($paket_b_plan_cicilan6[0]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C6_BPP_POKOK]' => number_format($paket_b_plan_cicilan6[1]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C6_BPP_SKS]' => $feeStructureProgramData['fsp_first_sem_sks'],
				'$[PAKET_B_C6_SKS_VALUE]' => number_format($paket_b_plan_cicilan6[2]['fsi_amount'], 2, '.', ','),
				'$[PAKET_B_C6_BPP_SKS_AMOUNT]' => number_format($paket_b_plan_cicilan6[2]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C6_PRAKTIKUM]' => number_format($paket_b_plan_cicilan6[3]['total_amount'], 2, '.', ','),
				'$[PAKET_B_C6_TOTAL]' => number_format($paket_b_plan_cicilan6[0]['total_amount'] + $paket_b_plan_cicilan6[1]['total_amount'] + $paket_b_plan_cicilan6[2]['total_amount'] + $paket_b_plan_cicilan6[3]['total_amount'], 2, '.', ',') ,

				'$[BALANCE_INSTALLMENT_PAKET_B]' => number_format( ( $paket_a_plan[0]['total_amount'] + $paket_a_plan[1]['total_amount'] + $paket_a_plan[2]['total_amount'] + $paket_a_plan[3]['total_amount'] ) - ( $paket_b_plan_cicilan1[0]['total_amount'] + $paket_b_plan_cicilan1[1]['total_amount'] + $paket_b_plan_cicilan1[2]['total_amount'] + $paket_b_plan_cicilan1[3]['total_amount'] ), 2, '.', ','),
				*/
            '$[LEARNING_DURATION]'       => $learning_duration,
            '$[ESTIMASI_BIAYA]'          => $biaya,
            '$[RECTOR_DATE]'             => date('j M Y', strtotime($assessmentData['aaud_decree_date']))
        );

        require_once 'dompdf_config.inc.php';

        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        $html_template_path = DOCUMENT_PATH . "/template/OfferLetterUSM.html";

        $html = file_get_contents($html_template_path);

        //replace variable
        foreach ($fieldValues as $key => $value) {
            $html = str_replace($key, $value, $html);
        }

        //program data
        global $program;
        $program = $feeStructureProgramData;

        //registration date
        global $reg_date;
        $reg_date = array(
            'REGISTRATION_DATE_START' => $assessmentData['aaud_reg_start_date'],
            'REGISTRATION_DATE_END'   => $assessmentData['aaud_reg_end_date']
        );

        //date payment
        foreach ($feeStructureData['payment_plan'] as $key => $plan) {
            $start = $assessmentData['aaud_reg_start_date'];
            $end = $assessmentData['aaud_reg_end_date'];

            foreach ($plan['plan_detail'] as $key2 => $installment) {
                $reg_date['date_payment'][$key][$key2]['start'] = $start;
                $reg_date['date_payment'][$key][$key2]['end'] = $end;

                $end = date('F Y', strtotime('+1 month', strtotime($end)));
            }

            $end = $assessmentData['aaud_reg_end_date'];
        }


        //fee data
        global $fees;
        $fees = $feeStructureData['payment_plan'];

        //footer variable
        global $pes;
        $pes = $txnData["at_pes_id"];
        //echo $html;
        //exit;

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'potrait');
        $dompdf->render();

        //$dompdf->stream($txnData["at_pes_id"]."_offer_letter.pdf");
        $pdf = $dompdf->output();
        //exit;

        //$location_path
        $location_path = "applicant/" . date("mY") . "/" . $txnId;

        //output_directory_path
        $output_directory_path = DOCUMENT_PATH . "/" . $location_path;

        //create directory to locate file
        if (!is_dir($output_directory_path)) {
            mkdir($output_directory_path, 0775, true);
        }

        //output filename
        $output_filename = $txnData["at_pes_id"] . "_offer_letter.pdf";

        //to rename output file
        $output_file_path = $output_directory_path . "/" . $output_filename;

        file_put_contents($output_file_path, $pdf);

        //update file info
        /*$documentDB = new App_Model_Application_DbTable_ApplicantDocument();
		$doc["ad_filepath"]=$location_path;
		$doc["ad_filename"]=$output_filename;
		$documentDB->updateDocument($doc,$txnId,45);*/

        //update file info
        $documentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $fileexist = $documentDB->getDataArray($txnId, 45);

        $doc["ad_filepath"] = $location_path;
        $doc["ad_filename"] = $output_filename;
        $doc["ad_appl_id"] = $txnId;
        $doc["ad_type"] = 45;
        $doc["ad_createddt"] = date("Y-m-d");

        if ($fileexist) {
            $documentDB->updateDocument($doc, $txnId, 45);
        } else {
            $documentDB->addData($doc);
        }

        //regenerate performa invoice
        $proformaInvoiceDb = new Application_Model_DbTable_ProformaInvoice();
        $proformaInvoiceDb->regenerateUSMProformaInvoice($txnId);

    }

    protected function mailmergeConnection($filepath, $fieldValues, $connection, $output_directory_path, $output_filename, $photoFilename = '')
    {


        $mailMerge = new Zend_Service_LiveDocx_MailMerge();

        $mailMerge->setUsername('yatie')
            ->setPassword('al_hasib');


        $mailMerge->setLocalTemplate($filepath);

        $mailMerge->assign($fieldValues);
        $mailMerge->assign('connection', $connection);
        $mailMerge->createDocument();

        $document = $mailMerge->retrieveDocument('pdf');

        //create directory to locate file
        if (!is_dir($output_directory_path)) {
            mkdir($output_directory_path, 0775, true);
        }

        //to rename output file
        $output_file_path = $output_directory_path . "/" . $output_filename;

        file_put_contents($output_file_path, $document);
    }

    protected function mailmerge($filepath, $fieldValues, $output_directory_path, $output_filename, $photoFilename = '')
    {

        //create PDF File
        /*			print_r($fieldValues);
			echo $photoFilename."<br>";
			echo $fieldValues["image:photo"];
			exit;
    		*/
        $mailMerge = new Zend_Service_LiveDocx_MailMerge();

        $mailMerge->setUsername('yatie')
            ->setPassword('al_hasib');


        /*if($photoFilename){
				if (!$mailMerge->imageExists($fieldValues["image:photo"])) {
				    $mailMerge->uploadImage($photoFilename);
				}
			}*/
        //buat ni sebab kalo takde gambor error
        if ($photoFilename) {
            $fieldValues["image:photo"] = $photoFilename;
            if (!$mailMerge->imageExists($fieldValues["image:photo"])) {
                $directory_photo = $output_directory_path . "/" . $photoFilename; //get directory DOCUMENT_PATH/$appl_id/$photo_name
                $mailMerge->uploadImage($directory_photo);
            }
        }

        $mailMerge->setLocalTemplate($filepath);

        $mailMerge->setFieldValues($fieldValues);
        //$mailMerge->assign('image:photo', $photoFilename);
        $mailMerge->createDocument();

        $document = $mailMerge->retrieveDocument('pdf');

        //create directory to locate file
        if (!is_dir($output_directory_path)) {
            mkdir($output_directory_path, 0775, true);
        }

        //to rename output file
        $output_file_path = $output_directory_path . "/" . $output_filename;

        file_put_contents($output_file_path, $document);
        if ($photoFilename) {
            $mailMerge->deleteImage($output_directory_path . "/" . $photoFilename);
        }
    }

    private function createImage()
    {
        // Create the image
        $im = imagecreatetruecolor(800, 600);

        // Create some colors
        $white = imagecolorallocate($im, 255, 255, 255);
        $black = imagecolorallocate($im, 0, 0, 0);
        imagefilledrectangle($im, 0, 0, 800, 600, $white);

        // The text to draw
        $text = "Muhammad Sarfraz \n";
        $text .= "Khurram Faraz\n";
        $text .= "Mohammad Imran\n";
        $text .= "Zulfiqar Ahmed Khan\n";
        $text .= "Najam ul Hasnain Shah\n";
        $text .= "Abdul Rehman Daniyal\n";
        $text .= "Yasir Siddiqui\n";
        $text .= "Sohail ur Rehman\n";
        $text .= "Hamad Aziz Sheikh\n";
        $text .= "Nauman Khalid\n";
        $text .= "Saad Ahmed\n";
        $text .= "Nasir Mehmood Butt\n";
        $text .= "Mohammad Aqeel Mirza";

        // Replace path by your own font path
        //$font = 'walt.ttf'; // Place this file in your code directory or if font is in font directory set its path with font name as $font=$path.'arial.ttf';
        $font = 'arial.ttf';

        // Add the text
        $x = 10;
        $y = 120;
        $font_size = 14;
        $angle = 0;
        $total_width = 0;
        $counter = 0;

        for ($i = 0; $i < strlen($text); $i++) {
            //$text_to_write=urldecode(substr($text,$i,1)."%0D_");
            $dimensions = imagettfbbox($font_size, $angle, $font, substr($text, $i, 1));
            $total_width += ($dimensions[2]);


        }
        echo "<pre>";
        $dimensions = imagettfbbox($font_size, $angle, $font, $text);
        echo "Dimension of full string=" . $dimensions[2] . "<br/>";

        echo "Total width calcuated by algorithm=" . $total_width . "<br/>";
        $difference = $dimensions[2] - $total_width;

        echo "Difference=" . $difference;
        imagettftext($im, $font_size, $angle, $x + 1, $y + 1, $black, $font, $text);
        imagettftext($im, $font_size, $angle, $x, $y, $black, $font, $text);

        $x2 = $x + $total_width + $difference + 2;
        //echo $total_width;
        echo "<pre/>";
        //imageline( $im , $x , $y+4 , $x2 , $y+4 , $black );
        //imageBoldLine($im, $x, $y+4, $x2, $y+4, $black, $BoldNess=4, $func='imageLine');

        // Using imagepng() results in clearer text compared with imagejpeg()
        imagepng($im, DOCUMENT_PATH . "/underline.png");
        echo "<img src='/documents/underline.png'/>";
        imagedestroy($im);
    }

    public function changePasswordAction($appl_id)
    {

        $this->view->title = "Change Password";

        $auth = Zend_Auth::getInstance();

        $profileDB = new App_Model_Application_DbTable_ApplicantProfile();


        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $info = array('appl_password' => $formData["new_password"], 'appl_password' => $formData["new_password"], 'change_passwordby' => $auth->getIdentity()->id, 'change_passworddt' => date("Y-m-d H:m:s"));
            $profileDB->updateData($info, $formData["applicant_id"]);

        }

        $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'index', 'action' => 'index'), 'default', true));

    }

    public function ajaxGetProfileAction()
    {
        // $appl_id = $this->_getParam('appl_id', 0);
        $txn_id = $this->_getParam('txn_id', 0);

        //if ($this->getRequest()->isXmlHttpRequest()) {
        $this->_helper->layout->disableLayout();
        //}

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('ap' => 'applicant_profile'), array('appl_id', 'appl_username', 'appl_email', 'appl_password'))
            ->joinLeft(array('at' => 'applicant_transaction'), 'at.at_appl_id=ap.appl_id');

        if ($txn_id != 0) {
            $select->where('at.at_trans_id = ?', $txn_id);
        }

        $row = $db->fetchRow($select);

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($row);

        echo $json;
        exit();
    }

    private function generateAgreementLetter($txnId)
    {

        //get applicant info
        $applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
        $applicant = $applicantDB->getAllProfile($txnId);

        //get transaction info
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $txnData = $applicantTxnDB->getTransaction($txnId);


        //getapplicantprogram
        $appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
        $programDb = new GeneralSetup_Model_DbTable_Program();

        if ($txnData['at_appl_type'] == 2) {
            $program = $appProgramDB->getProgramFaculty($txnId);
            //program data
            $programData = $programDb->fngetProgramData($program[0]['program_id']);

            //get assessment data
            $assessmentDb = new App_Model_Application_DbTable_ApplicantAssessment();
            $ass_data = $assessmentDb->getData($txnId);


            $assessmentData = array(
                'nomor'                   => $ass_data['asd_nomor'],
                'decree_date'             => $ass_data['asd_decree_date'],
                'rank'                    => $ass_data['aar_rating_rector'],
                'registration_start_date' => $ass_data['aar_reg_start_date'],
                'registration_end_date'   => $ass_data['aar_reg_end_date'],
                'payment_start_date'      => $ass_data['aar_payment_start_date'],
                'payment_end_date'        => $ass_data['aar_payment_end_date'],
            );
        } else {
            $program = $appProgramDB->getUsmOfferProgram($txnId);

            //get assessment data
            $assessmentDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
            $ass_data = $assessmentDb->getData($txnId);

            $assessmentData = array(
                'nomor'                   => $ass_data['aaud_nomor'],
                'decree_date'             => $ass_data['aaud_decree_date'],
                'rank'                    => $ass_data['aau_rector_ranking'],
                'registration_start_date' => $ass_data['aaud_reg_start_date'],
                'registration_end_date'   => $ass_data['aaud_reg_end_date'],
                'payment_start_date'      => $ass_data['aaud_payment_start_date'],
                'payment_end_date'        => $ass_data['aaud_payment_end_date'],
            );

            //program data
            $programData = $programDb->fngetProgramData($program['program_id']);
        }

        //award type
        $award = "";

        if ($programData['Award'] == 36) {
            $award = "D3";
        } else
            if ($programData['Award'] == 363) {
                $award = "D4";
            } else {
                $award = "S1";
            }


        $learning_duration = $award . " = " . $programData['OptimalDuration'] . " Semester";


        //rank


        $rank_digit = 3;
        if ($assessmentData['rank'] == 1) {

            $rank_digit = 1;
            $rank = "1 (Satu)";
            $biaya = $programData['Estimate_Fee_R1'] != null ? number_format($programData['Estimate_Fee_R1'], 2, '.', ',') : "";

        } else
            if ($assessmentData['rank'] == 2) {
                $rank_digit = 2;
                $rank = "2 (Dua)";
                $biaya = $programData['Estimate_Fee_R2'] != null ? number_format($programData['Estimate_Fee_R2'], 2, '.', ',') : "";
            } else
                if ($assessmentData['rank'] == 3) {
                    $rank_digit = 3;
                    $rank = "3 (Tiga)";
                    $biaya = $programData['Estimate_Fee_R3'] != null ? number_format($programData['Estimate_Fee_R3'], 2, '.', ',') : "";
                } else {
                    $rank = "3 (Tiga)";
                    $biaya = $programData['Estimate_Fee_R3'] != null ? number_format($programData['Estimate_Fee_R3'], 2, '.', ',') : "";
                }


        //faculty data
        $collegeMasterDb = new GeneralSetup_Model_DbTable_Collegemaster();
        $facultyData = $collegeMasterDb->fngetCollegemasterData($programData['IdCollege']);

        //get applicant parents info
        $familyDB = new App_Model_Application_DbTable_ApplicantFamily();
        $father = $familyDB->getData($applicant["appl_id"], 20); //father's

        //get next intake
        $intakeDb = new GeneralSetup_Model_DbTable_Intake();
        $intakeData = $intakeDb->fngetIntakeDetails($txnData['at_intake']);

        //Nomor
        $nomor = $assessmentData['nomor'];

        $address = "";
        if (isset($applicant["appl_address1"]) && $applicant["appl_address1"] != "") {
            $address = $address . $applicant["appl_address1"] . "<br />";
        }
        if (isset($applicant["appl_address2"]) && $applicant["appl_address2"] != "") {
            $address = $address . $applicant["appl_address2"] . "<br />";
        }
        if (isset($applicant["CityName"]) && $applicant["CityName"] != "") {
            $address = $address . $applicant["CityName"] . "<br />";
        }
        if (isset($applicant["appl_postcode"]) && trim($applicant["appl_postcode"]) != "") {
            $address = $address . $applicant["appl_postcode"] . "<br />";
        }
        if (isset($applicant["StateName"]) && $applicant["StateName"] != "") {
            $address = $address . $applicant["StateName"] . "<br />";
        }

        $fieldValues = array(
            '$[NO_PES]'                  => $txnData["at_pes_id"],
            '$[NOMOR]'                   => $nomor,
            '$[LAMPIRAN]'                => "-",
            '$[TITLE_TEMPLATE]'          => $this->view->translate("Pemberitahuan diterima sebagai calon Mahasiswa di Universitas Trisakti"),
            '$[APPLICANT_NAME]'          => $applicant["appl_fname"] . ' ' . $applicant["appl_mname"] . ' ' . $applicant["appl_lname"],
            '$[PARENTNAME]'              => $father["af_name"],
            '$[PARENTJOB]'               => $father["afj_title"],
            '$[ADDRESS]'                 => $address,
            '$ADDRESS1]'                 => $applicant["appl_address1"],
            '$ADDRESS2]'                 => $applicant["appl_address2"],
            '$[CITY]'                    => $applicant["CityName"],
            '$[POSTCODE]'                => $applicant["appl_postcode"],
            '$[STATE]'                   => $applicant["StateName"],
            '$[ACADEMIC_YEAR]'           => $txnData['ay_code'],
            '$[PERIOD]'                  => $txnData['ap_desc'],
            '$[FACULTY]'                 => $programData["IdCollege"],
            '$[FACULTY_NAME]'            => ($facultyData['ArabicName'] != null ? $facultyData['ArabicName'] . " " : "-"),
            '$[FACULTY_SHORTNAME]'       => ($facultyData['ShortName'] != null ? $facultyData['ShortName'] . " " : "-"),
            '$[FACULTY_ADDRESS1]'        => ($facultyData['Add1'] != null ? $facultyData['Add1'] . " " : "-"),
            '$[FACULTY_ADDRESS2]'        => ($facultyData['Add2'] != null ? $facultyData['Add2'] . " " : ""),
            '$[FACULTY_ADDRESS]'         => ($facultyData['Add1'] != null ? $facultyData['Add1'] . " " : "") . ($facultyData['Add2'] != null ? $facultyData['Add2'] . " " : ""),
            '$[FACULTY_PHONE]'           => ($facultyData['Phone1'] != null ? $facultyData['Phone1'] . " " : "") . ($facultyData['Phone2'] != null ? ", " . $facultyData['Phone2'] . " " : ""),
            '$[FACULTY_FAX]'             => ($facultyData['Fax'] != null ? $facultyData['Fax'] . " " : ""),
            '$[PROGRAME]'                => $programData["ArabicName"],
            '$[RANK]'                    => $rank,
            '$[PRINT_DATE]'              => date('j M Y'),
            '$[REGISTRATION_DATE_START]' => date('j F Y', strtotime($assessmentData['registration_start_date'])),
            '$[REGISTRATION_DATE_END]'   => date('j F Y', strtotime($assessmentData['registration_end_date'])),
            '$[LEARNING_DURATION]'       => $learning_duration,
            '$[ESTIMASI_BIAYA]'          => $biaya,
            '$[RECTOR_DATE]'             => date('j M Y', strtotime($assessmentData['decree_date']))
        );

        require_once 'dompdf_config.inc.php';

        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        $html_template_path = DOCUMENT_PATH . "/template/AgreementLetter.html";

        $html = file_get_contents($html_template_path);

        //replace variable
        foreach ($fieldValues as $key => $value) {
            $html = str_replace($key, $value, $html);
        }


        //payment data
        $paymentMainDb = new Studentfinance_Model_DbTable_PaymentMain();
        $payment = $paymentMainDb->getApplicantPaymentInfo($txnData['at_pes_id']);

        //get fee structure
        //TODO:check local or foreign
        $feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
        $fee_structure = $feeStructureDb->getApplicantFeeStructure($txnData['at_intake'], $programData['IdProgram']);

        //get selected payment plan
        $paymentplanDb = new Studentfinance_Model_DbTable_FeeStructurePlan();
        $payment_plan = $paymentplanDb->getBillingPlan($fee_structure['fs_id'], $payment[0]['billing_no']);

        //inject plan detail (installment)
        $paymentPlanDetailDb = new Studentfinance_Model_DbTable_FeeStructurePlanDetail();
        $payment_plan['installment_detail'] = array();
        for ($i = 1; $i <= $payment_plan['fsp_bil_installment']; $i++) {
            $payment_plan['installment_detail'][$i] = $paymentPlanDetailDb->getPlanData($fee_structure['fs_id'], $payment_plan['fsp_id'], $i, 1, $programData['IdProgram'], $assessmentData['rank']);

        }

        //registration date
        global $reg_date;
        $reg_date = array(
            'REGISTRATION_DATE_START' => $assessmentData['registration_start_date'],
            'REGISTRATION_DATE_END'   => $assessmentData['registration_end_date']
        );

        //date payment
        $start = $assessmentData['registration_start_date'];
        $end = $assessmentData['registration_end_date'];

        foreach ($payment_plan['installment_detail'] as $key => $installment) {
            $payment_plan['payment_date'][$key]['start'] = $start;
            $payment_plan['payment_date'][$key]['end'] = $end;

            $end = date('F Y', strtotime('+1 month', strtotime($end)));
        }

        $end = $assessmentData['registration_end_date'];

        global $fee;
        $fee = $payment_plan;

        global $program_fee_structure;
        $program_fee_structure = $fee_structure;


        //program data
        global $program;
        $program = $programData;

        //footer variable
        global $pes;
        $pes = $txnData["at_pes_id"];
        /*echo $html;
		exit;*/

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'potrait');
        $dompdf->render();


        //$dompdf->stream($txnData["at_pes_id"]."_agreement_letter.pdf");
        //exit;
        $pdf = $dompdf->output();


        //$location_path
        $location_path = "applicant/" . date("mY") . "/" . $txnId;

        //output_directory_path
        $output_directory_path = DOCUMENT_PATH . "/" . $location_path;

        //create directory to locate file
        if (!is_dir($output_directory_path)) {
            mkdir($output_directory_path, 0775);
        }

        //output filename
        $output_filename = $txnData["at_pes_id"] . "_agreement_letter.pdf";

        //to rename output file
        $output_file_path = $output_directory_path . "/" . $output_filename;

        file_put_contents($output_file_path, $pdf);

        //update file info
        /*$documentDB = new App_Model_Application_DbTable_ApplicantDocument();
		$doc["ad_filepath"]=$location_path;
		$doc["ad_filename"]=$output_filename;
		$documentDB->updateDocument($doc,$txnId,45);*/

        //update file info
        $documentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $fileexist = $documentDB->getDataArray($txnId, 50);

        $doc["ad_filepath"] = $location_path;
        $doc["ad_filename"] = $output_filename;
        $doc["ad_appl_id"] = $txnId;
        $doc["ad_type"] = 50;

        if ($fileexist) {

            $documentDB->updateDocument($doc, $txnId, 50);
        } else {

            $doc['ad_createddt'] = date('Y-m-d');
            $documentDB->addData($doc);
        }

    }


    public function verifyDocumentAction()
    {

        $auth = Zend_Auth::getInstance();

        $chk_txn_id = $this->_getParam('txn_id', null);

        if ($chk_txn_id) {
            $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
            $applicantTxnDB->updateData(array('at_document_verified' => 1, 'at_document_verifiedby' => $auth->getIdentity()->iduser, 'at_document_verifieddt' => date('Y-m-d H:i:s')), $chk_txn_id);
        }
        $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'index', 'action' => 'txn-detail', 'id' => $chk_txn_id), 'default', true));
    }

    function deleteDocuments($documents)
    {
        if (count($documents) > 0) {
            $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
            foreach ($documents as $doc) {
                if (file_exists(DOCUMENT_PATH . $doc['ad_filepath'])) {
                    unlink(DOCUMENT_PATH . $doc['ad_filepath']);
                }
                //delete document info dtbase
                $appDocumentDB->deleteData($doc['ad_id']);
            }//end foreach
        }//end if count
    }

    function regenCseAction()
    {
        //txn data
        $txnId = $this->_getParam('txn_id', null);

        //redo function tak pandai nak betul kan
        //$cseClass = new icampus_Function_Application_Cse();
        //$cseClass->generate($txnId);

        $profileDB = new Application_Model_DbTable_ApplicantProfile();
        $profile = $profileDB->getProfileProgram($txnId);

        if ($profile['at_status'] == 591) {
            $this->view->content = $this->view->translate("This application not submit yet");
            $this->view->redirect = $this->view->baseUrl() . '/application/index/edit/id/' . $txnId;
            $this->view->setBasePath(APPLICATION_PATH . '/views/');
            $this->renderScript('partials/error.phtml');
            return;
        }

        $templateDB = new Application_Model_DbTable_Template();
        $template = $templateDB->getTemplateContent(592, $profile['ap_prog_scheme'], $profile['appl_category'], 716);

        if (!$template) {
            $this->view->content = $this->view->translate("Template doesn't exist");
            $this->view->redirect = $this->view->baseUrl() . '/application/index/edit/id/' . $txnId;
            $this->view->setBasePath(APPLICATION_PATH . '/views/');
            $this->renderScript('partials/error.phtml');
            return;
        }

        $templateCMS = new Cms_TemplateTags();
        $html = $templateCMS->parseContent($txnId, $template['tpl_id'], $template['tpl_content']);
        //echo $html;

        //attachment name
        $name = $profile['at_pes_id'] . '_' . $template['attachmentTypeName'] . '_' . date('YmdHis');

        //attachment url
        $url = DOCUMENT_PATH . $profile['at_repository'];

        //generate pdf
        $this->generatePDF($html, $name, $url);

        $modelApp = new Application_Model_DbTable_ApplicantApproval();

        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        //store history generate pdf
        $dataGeneratePdf = array(
            'tgd_txn_id'   => $txnId,
            'tgd_filename' => $name . '.pdf',
            'sth_id'       => $template['sth_Id'],
            'tgd_fileurl'  => $profile['at_repository'] . '/' . $name . '.pdf',
            'tgd_updDate'  => date('Y-m-d H:i:s'),
            'tgd_userType' => 1,
            'tgd_updUser'  => $userId
        );
        $modelApp->storePdfDocGenHistory($dataGeneratePdf);

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Generate Success'));
        $this->_redirect($this->baseUrl . '/application/index/edit/id/' . $txnId . '/idSection/1');
        exit;
    }


    function regenerateProformaAction()
    {
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        //txn data
        $txnId = $this->_getParam('txn_id', null);

        $profileDB = new Application_Model_DbTable_ApplicantProfile();
        $profile = $profileDB->getProfileProgram($txnId);

        if ($profile['at_status'] == 591) {
            $this->view->content = $this->view->translate("This application not submit yet");
            $this->view->redirect = $this->view->baseUrl() . '/application/index/edit/id/' . $txnId;
            $this->view->setBasePath(APPLICATION_PATH . '/views/');
            $this->renderScript('partials/error.phtml');
            return;
        }

        $templateDB = new Application_Model_DbTable_Template();
        $template = $templateDB->getTemplateContent(592, $profile['ap_prog_scheme'], $profile['appl_category'], 605);
        //dd($template); exit;
        if (!$template) {
            $this->view->content = $this->view->translate("Template doesn't exist");
            $this->view->redirect = $this->view->baseUrl() . '/application/index/edit/id/' . $txnId;
            $this->view->setBasePath(APPLICATION_PATH . '/views/');
            $this->renderScript('partials/error.phtml');
            return;
        }
        //var_dump($template['tpl_id']);
        $templateCMS = new Cms_TemplateTags();
        $html = $templateCMS->parseContent($txnId, $template['tpl_id'], $template['tpl_content']);
        //echo $html; //exit;
        /*$auth = Zend_Auth::getInstance();
            $userId = $auth->getIdentity()->iduser;

            if($userId == 1){
           	 echo $html; //exit;
            }
            */
        //attachment name
        $name = $profile['at_pes_id'] . '_' . $template['attachmentTypeName'] . '_' . date('YmdHis');

        //attachment url
        $url = DOCUMENT_PATH . $profile['at_repository'];

        //generate pdf
        $this->generatePDF($html, $name, $url);

        $modelApp = new Application_Model_DbTable_ApplicantApproval();

        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        //store history generate pdf
        $dataGeneratePdf = array(
            'tgd_txn_id'   => $txnId,
            'tgd_filename' => $name . '.pdf',
            'sth_id'       => $template['sth_Id'],
            'tgd_fileurl'  => $profile['at_repository'] . '/' . $name . '.pdf',
            'tgd_updDate'  => date('Y-m-d H:i:s'),
            'tgd_userType' => 1,
            'tgd_updUser'  => $userId
        );
        $modelApp->storePdfDocGenHistory($dataGeneratePdf);

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Generate Success'));
        $this->_redirect($this->baseUrl . '/application/index/edit/id/' . $txnId . '/idSection/1');
        exit;
    }


    function regenerateAppendixAction()
    {
        //txn data
        $txnId = $this->_getParam('txn_id', null);

        $profileDB = new Application_Model_DbTable_ApplicantProfile();
        $profile = $profileDB->getProfileProgram($txnId);

        if ($profile['at_status'] != 593 && $profile['at_status'] != 599 && $profile['at_status'] != 601) {
            $this->view->content = $this->view->translate("Applicant status is not OFFERED yet.");
            $this->view->redirect = $this->view->baseUrl() . '/application/index/edit/id/' . $txnId;
            $this->view->setBasePath(APPLICATION_PATH . '/views/');
            $this->renderScript('partials/error.phtml');
            return;
        }

        $templateDB = new Application_Model_DbTable_Template();
        $template = $templateDB->getTemplateContent(593, $profile['ap_prog_scheme'], $profile['appl_category'], 851);

        if (!$template) {
            $this->view->content = $this->view->translate("Template doesn't exist");
            $this->view->redirect = $this->view->baseUrl() . '/application/index/edit/id/' . $txnId;
            $this->view->setBasePath(APPLICATION_PATH . '/views/');
            $this->renderScript('partials/error.phtml');
            return;
        }

        $templateCMS = new Cms_TemplateTags();
        $html = $templateCMS->parseContent($txnId, $template['tpl_id'], $template['tpl_content']);
        //echo $html; exit;

        //attachment name
        $name = $profile['at_pes_id'] . '_' . $template['attachmentTypeName'] . '_' . date('YmdHis');

        //attachment url
        $url = DOCUMENT_PATH . $profile['at_repository'];

        //generate pdf
        $this->generatePDF($html, $name, $url);

        $modelApp = new Application_Model_DbTable_ApplicantApproval();

        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        //store history generate pdf
        $dataGeneratePdf = array(
            'tgd_txn_id'   => $txnId,
            'tgd_filename' => $name . '.pdf',
            'sth_id'       => $template['sth_Id'],
            'tgd_fileurl'  => $profile['at_repository'] . '/' . $name . '.pdf',
            'tgd_updDate'  => date('Y-m-d H:i:s'),
            'tgd_userType' => 1,
            'tgd_updUser'  => $userId
        );
        $modelApp->storePdfDocGenHistory($dataGeneratePdf);

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Generate Success'));
        $this->_redirect($this->baseUrl . '/application/index/edit/id/' . $txnId . '/idSection/1');
        exit;
    }

    function generatePDF($html, $name, $url)
    {

        $options = array(
            'content'        => $html,
            'file_name'      => $name,
            'file_extension' => 'pdf',
            'save_path'      => $url,
            'save'           => true,
            /*'css' => '@page { margin: 110px 50px 95px 50px}
						body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
						table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',*/
            'css'            => '@page { margin: 110px 50px 80px 50px}
						body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
						table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',
            'header'         => '<script type="text/php">
						if ( isset($pdf) ) {
							$header = $pdf->open_object();

							$w = $pdf->get_width();
							$h = $pdf->get_height();
							$color = array(0,0,0);

							$img_w = 180; 
							$img_h = 59;
							$pdf->image("images/logo_text_high.jpg",  ($w/2)-85, 20, $img_w, $img_h);

							// Draw a line along the bottom
							$font = Font_Metrics::get_font("Helvetica");
							$size = 6;
							$text_height = Font_Metrics::get_font_height($font, $size)+2;
							$y = $h - (3.5 * $text_height)-10;
							$pdf->line(10, $y, $w - 10, $y, $color, 1);

					// Draw a second line along the bottom
							$y = $h - (3.5 * $text_height)+10;
							$pdf->line(10, $y, $w - 10, $y, $color, 1);

							$pdf->close_object();

							$pdf->add_object($header, "all");
						}
						</script>',
            'footer'         => '<script type="text/php">
						if ( isset($pdf) ) {
							$footer = $pdf->open_object();

							$font = Font_Metrics::get_font("Helvetica");
							$size = 6;
							$color = array(0,0,0);
							$text_height = Font_Metrics::get_font_height($font, $size)+2;

							$w = $pdf->get_width();
							$h = $pdf->get_height();


							// Draw a line along the bottom
							$y = $h - (3.5 * $text_height)-10;
							//$pdf->line(10, $y, $w - 10, $y, $color, 1);

							//1st row footer
							$text = "Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)";
							$width = Font_Metrics::get_text_width($text, $font, $size);	
							$y = $h - (2 * $text_height)-20;
							$x = ($w - $width) / 2.0;

							$pdf->page_text($x, $y, $text, $font, $size, $color);

							//2nd row footer
							$text = "t: +603 7651 4000		f : +603 7651 4094		w: www.inceif.org";
							$width = Font_Metrics::get_text_width($text, $font, $size);	
							$y = $h - (1 * $text_height)-20;
							$x = ($w - $width) / 2.0;

							$pdf->page_text($x, $y, $text, $font, $size, $color);

						   

							$pdf->close_object();

							$pdf->add_object($footer, "all");
						}
						</script>'
        );

        //generate pdf
        generatePdf($options);
    }

    function regenOfferLetterAction()
    {
        $this->view->title = "Regenerate Offer Letter";

        $templateCMS = new Cms_TemplateTags();
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $programDB = new App_Model_Application_DbTable_ApplicantProgram();
        $appProfileDB = new Application_Model_DbTable_ApplicantProfile();
        $defModel = new App_Model_General_DbTable_Definationms();
        $this->statusModel = new Application_Model_DbTable_Status();
        $this->appAprove = new Application_Model_DbTable_ApplicantApproval();

        $this->auth = Zend_Auth::getInstance();
        $getUserIdentity = $this->auth->getIdentity();

        //txn data
        $txnId = $this->_getParam('txn_id', null);

        $txnData = $applicantTxnDB->getTransaction($txnId, 1);
        $appl_id = $this->view->appl_id = $txnData['at_appl_id'];
        $transID = $this->view->transID = $txnData['at_trans_id'];
        $this->view->txnData = $txnData;


        //applicant info
        $applicant = $appProfileDB->getData($appl_id, 0, 1);

        //programme applied
        $program = $programDB->getProgram($txnId);
        $this->view->program = $program;

        //status
        $getStatus = $defModel->getIdByDefType('Status', 'Offered');

        $appProgram = $appProfileDB->getProfileProgram($transID);
        //$appProgram = array('ap_prog_scheme' => '', 'appl_category' => '');

        if ($appProgram['ap_prog_scheme'] != '' && $appProgram['appl_category'] != '') {
            if ($txnData['at_status'] != 593 && $txnData['at_status'] != 599 && $txnData['at_status'] != 601) //offer or accept or enrolled
            {
                $this->view->content = $this->view->translate('Applicant status is not OFFERED yet.');
                $this->view->redirect = $this->view->baseUrl() . '/application/index/edit/id/' . $transID;
                $this->view->setBasePath(APPLICATION_PATH . '/views/');
                $this->renderScript('partials/error.phtml');
                return;
            }
        } else {
            $this->view->content = $this->view->translate('Applicant proramme scheme not set yet.');
            $this->view->setBasePath(APPLICATION_PATH . '/views/');
            $this->renderScript('partials/error.phtml');
            return;
        }


        if ($txnData['at_status'] == 599 || $txnData['at_status'] == 601) {
            $txnData['at_status'] = 593;
        }
        //generate proforma
        $class = new icampus_Function_Studentfinance_Invoice();
        //$class->generateApplicantProformaInvoice($transID, 1);

        /*
		 * get info
		 *  -program
		 *  -program scheme
		 *  -status
		 *  -email
		 *  -attachment
		 *  -applicant
		 */
        $getProgScheme = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id = ' . $transID);

        $stsId = $this->statusModel->fnGetStatusByProgramAndStsType($getProgScheme, $txnData['at_status'], $txnData['appl_category']);
        $statusInfo = $this->appAprove->getStatusInfo($stsId['sts_Id']);

        if ($statusInfo == false) {
            throw new Exception("Offered status doesn't exist");
        }

        $statusEmailTemplate = $this->appAprove->getStatusTemplate($statusInfo['sts_emailNotification']);
        if ($statusEmailTemplate == false) {
            throw new Exception("Offered email template doesn't exist");
        }

        $statusAttachment = $this->appAprove->getStatusAttachment($statusEmailTemplate['stp_Id']);
        $applicantInfo = $this->appAprove->getApplicantInfo($transID);

        //var_dump($statusAttachment); exit;
        //enter attachment proses
        if (count($statusAttachment) > 0) {
            foreach ($statusAttachment as $statusAttachmentLoop) {
                if ($statusAttachmentLoop['sth_type'] == 606) {
                    $statusAttachmentTemplate = $this->appAprove->getStatusAttachmenTemplate($statusAttachmentLoop['sth_tplId']);
                    //var_dump($statusAttachmentTemplate); exit;
                    if (count($statusAttachmentTemplate) > 0) {
                        foreach ($statusAttachmentTemplate as $statusAttachmentTemplateLoop) {
                            if ($statusAttachmentTemplateLoop['locale'] == 'en_US') {

                                //attachment name
                                $name = $txnData['at_pes_id'] . '_' . $statusAttachmentLoop['typeName'] . '_' . date('YmdHis');

                                //attachment content
                                $html = $templateCMS->parseContent($transID, $statusAttachmentTemplateLoop['tpl_id'], $statusAttachmentTemplateLoop['tpl_content']);
                                //$html = $this->parseContent($transID, $statusAttachmentTemplateLoop['tpl_id'], $statusAttachmentTemplateLoop['tpl_content']);
                                //echo $html; exit;
                                //attachment url
                                $url = DOCUMENT_PATH . $applicantInfo['at_repository'];

                                //option pdf
                                $option = array(
                                    'content'        => $html,
                                    'save'           => true,
                                    'file_extension' => 'pdf',
                                    'save_path'      => $url,
                                    'css'            => '@page { margin: 110px 50px 95px 50px}
                                                            body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
                                                            table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                                                            table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}
                                                            table.tftable tr {background-color:#ffffff;}
                                                            table.tftable td {font-size:10px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}',
                                    'file_name'      => $name,
                                    'header'         => '<script type="text/php">
                                                            if ( isset($pdf) ) {
                                                                    $header = $pdf->open_object();

                                                                    $w = $pdf->get_width();
                                                                    $h = $pdf->get_height();
                                                                    $color = array(0,0,0);

                                                                    $img_w = 180; 
                                                                    $img_h = 59;
                                                                    $pdf->image("images/logo_text_high.jpg",  ($w/2)-85, 20, $img_w, $img_h);

                                                                    // Draw a line along the bottom
                                                                    $font = Font_Metrics::get_font("Helvetica");
                                                                    $size = 6;
                                                                    $text_height = Font_Metrics::get_font_height($font, $size)+2;
                                                                    $y = $h - (3.5 * $text_height)-10;
                                                                    $pdf->line(10, $y, $w - 10, $y, $color, 1);

                                                    // Draw a second line along the bottom
                                                                    $y = $h - (3.5 * $text_height)+10;
                                                                    $pdf->line(10, $y, $w - 10, $y, $color, 1);

                                                                    $pdf->close_object();

                                                                    $pdf->add_object($header, "all");
                                                            }
                                                            </script>',
                                    'footer'         => '<script type="text/php">
                                                            if ( isset($pdf) ) {
                                                                    $footer = $pdf->open_object();

                                                                    $font = Font_Metrics::get_font("Helvetica");
                                                                    $size = 6;
                                                                    $color = array(0,0,0);
                                                                    $text_height = Font_Metrics::get_font_height($font, $size)+2;

                                                                    $w = $pdf->get_width();
                                                                    $h = $pdf->get_height();


                                                                    // Draw a line along the bottom
                                                                    $y = $h - (3.5 * $text_height)-10;
                                                                    //$pdf->line(10, $y, $w - 10, $y, $color, 1);

                                                                    //1st row footer
                                                                    $text = "Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)";
                                                                    $width = Font_Metrics::get_text_width($text, $font, $size);	
                                                                    $y = $h - (2 * $text_height)-20;
                                                                    $x = ($w - $width) / 2.0;

                                                                    $pdf->page_text($x, $y, $text, $font, $size, $color);

                                                                    //2nd row footer
                                                                    $text = "t: +603 7651 4000		f : +603 7651 4094		w: www.inceif.org";
                                                                    $width = Font_Metrics::get_text_width($text, $font, $size);	
                                                                    $y = $h - (1 * $text_height)-20;
                                                                    $x = ($w - $width) / 2.0;

                                                                    $pdf->page_text($x, $y, $text, $font, $size, $color);



                                                                    $pdf->close_object();

                                                                    $pdf->add_object($footer, "all");
                                                            }
                                                            </script>'
                                );

                                //generate pdf
                                generatePdf($option);

                                //store history generate pdf
                                $dataGeneratePdf = array(
                                    'tgd_txn_id'   => $transID,
                                    'tgd_filename' => $name . '.pdf',
                                    'sth_id'       => $statusAttachmentLoop['sth_Id'],
                                    'tgd_fileurl'  => $applicantInfo['at_repository'] . '/' . $name . '.pdf',
                                    'tgd_updDate'  => date('Y-m-d H:i:s'),
                                    'tgd_userType' => 1,
                                    'tgd_updUser'  => $getUserIdentity->id
                                );
                                $this->appAprove->storePdfDocGenHistory($dataGeneratePdf);

                                //add to email que
                                /*$dataEmail = array(
								'recepient_email' => $applicantInfo['appl_email'],
								'subject' => $statusEmailTemplate['stp_title'],
								'content' => $this->parseContent($transID, $statusEmailTemplate['stp_tplId'], $statusEmailTemplate['contentEng']),
								'date_que' => date('Y-m-d H:i:s')
							);*/

                                //$emailQueId = $this->appAprove->addToEmailQue($dataEmail);

                                //add email attachment
                                /*$dataEmailAttachment = array(
								'eqa_emailque_id' => $emailQueId,
								'eqa_filename' => $name.'.pdf',
								'eqa_path' => APP_DOC_PATH.$applicantInfo['at_repository'].'/'.$name.'.pdf',
								'eqa_updDate' => date('Y-m-d H:i:s'),
								'eqa_updUser' => $getUserIdentity->id
							);*/
                                //$this->appAprove->addEmailQueAtttachment($dataEmailAttachment);
                            }
                        }
                    }
                }
            }
        }

        /*$this->view->content = $this->view->translate('All done.');
		$this->view->redirect = $this->view->baseUrl().'/application/index/edit/id/'.$transID;
		$this->view->setBasePath(APPLICATION_PATH.'/views/');
		$this->renderScript('partials/success.phtml');*/

        //redirect here
        $this->_helper->flashMessenger->addMessage(array('success' => 'Generate Success'));
        $this->_redirect($this->baseUrl . '/application/index/edit/id/' . $txnId . '/idSection/1');
        return;
    }

    protected function parseContent($trans_id, $tpl_id, $content)
    {
        $templateTags = $this->appAprove->getTemplateTag($tpl_id);

        foreach ($templateTags as $tag) {
            switch ($tag['tag_name']) {
                case "Date":
                    $content = str_replace("[" . $tag['tag_name'] . "]", date("j-m-Y, g:i a"), $content);
                    break;

                case "Applicant ID":
                    $applicantId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_pes_id', 'at_trans_id = ' . $trans_id);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $applicantId, $content);
                    break;

                case "Applicant Name":
                    $applId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_appl_id', 'at_trans_id = ' . $trans_id);
                    $fname = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_fname', 'appl_id = ' . $applId);
                    $mname = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_mname', 'appl_id = ' . $applId);
                    $lname = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_lname', 'appl_id = ' . $applId);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $fname . ' ' . $mname . ' ' . $lname, $content);
                    break;

                case "Salutation":
                    $applId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_appl_id', 'at_trans_id = ' . $trans_id);
                    $salutationCode = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_salutation', 'appl_id = ' . $applId);
                    $salutation = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_definationms', 'DefinitionDesc', 'idDefinition = ' . $salutationCode);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $salutation, $content);
                    break;

                case "Applicant Address":
                    $applId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_appl_id', 'at_trans_id = ' . $trans_id);
                    $address1 = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_address1', 'appl_id = ' . $applId);
                    $address2 = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_address2', 'appl_id = ' . $applId);
                    $poscode = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_postcode', 'appl_id = ' . $applId);
                    $cityCode = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_city', 'appl_id = ' . $applId);
                    $stateCode = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_state', 'appl_id = ' . $applId);
                    //$provinceCode = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_province', 'appl_id = '.$applId);
                    //$city = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_city', 'CityName', 'idCity = '.$cityCode);
                    //$state = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_state', 'StateName', 'idState = '.$stateCode);
                    //$country = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_state', 'StateName', 'idState = '.$stateCode);

                    $applicantAddress = $address1 . ",<br>";
                    $applicantAddress .= $address2 . ",<br>";
                    $applicantAddress .= $poscode . ", " . $cityCode . ",<br>";
                    $applicantAddress .= $stateCode . ".<br>";

                    $content = str_replace("[" . $tag['tag_name'] . "]", $applicantAddress, $content);
                    break;

                case "Program Status":
                    $programSchemeCode = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id = ' . $trans_id);
                    //$programCode = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'IdProgram', 'IdProgramScheme = '.$programSchemeCode);
                    $mopCode = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'mode_of_program', 'IdProgramScheme = ' . $programSchemeCode);
                    $mosCode = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'mode_of_study', 'IdProgramScheme = ' . $programSchemeCode);
                    $ptCode = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'program_type', 'IdProgramScheme = ' . $programSchemeCode);
                    $mop = Application_Model_DbTable_ChecklistVerification::fnGetDefination($mopCode);
                    $mos = Application_Model_DbTable_ChecklistVerification::fnGetDefination($mosCode);
                    $pt = Application_Model_DbTable_ChecklistVerification::fnGetDefination($ptCode);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $mop . " " . $mos . " " . $pt, $content);
                    break;

                case "Study Period":
                    $programSchemeCode = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id = ' . $trans_id);
                    //$programCode = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'IdProgram', 'IdProgramScheme = '.$programSchemeCode);
                    $programOptDuration = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'OptimalDuration', 'IdProgramScheme = ' . $programSchemeCode);
                    $programMaxDuration = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'Duration', 'IdProgramScheme = ' . $programSchemeCode);
                    $programOptDurationType = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'DurationType', 'IdProgramScheme = ' . $programSchemeCode);
                    $programMaxDurationType = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'OptDurationType', 'IdProgramScheme = ' . $programSchemeCode);

                    if ($programOptDurationType == 1) {
                        $OptDurationType = 'Semester';
                    } else {
                        $OptDurationType = 'Month';
                    }

                    if ($programMaxDurationType == 1) {
                        $MaxDurationType = 'Semester';
                    } else {
                        $MaxDurationType = 'Month';
                    }

                    //$studperiod_min = $programOptDuration." ".$OptDurationType;
                    //$studperiod_max = $programMaxDuration." ".$MaxDurationType;

                    $studperiod_max = floor($programOptDuration / 12) . ' year' . (($programOptDuration / 12) == 1 ? '' : 's') . ' ' . ($programOptDuration % 12 == 0 ? '' : $programOptDuration % 12 . ' months');
                    $studperiod_min = floor($programMaxDuration / 12) . ' year' . (($programMaxDuration / 12) == 1 ? '' : 's') . ' ' . ($programOptDuration % 12 == 0 ? '' : $programMaxDuration % 12 . ' months');

                    $studyPeriod = "Min. Duration : " . $studperiod_min . ", Max. Duration : " . $studperiod_max;

                    $content = str_replace("[" . $tag['tag_name'] . "]", $studyPeriod, $content);
                    break;

                case 'Intake':
                    $intakeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_intake', 'at_trans_id = ' . $trans_id);
                    $intake = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_intake', 'IntakeDesc', 'IdIntake = ' . $intakeId);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $intake, $content);
                    break;

                case 'Processing Fee':
                    $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                    $invoice = $invoiceClass->getApplicantProformaInfo($transID, $level = 0, $fee_category_id = null);

                    if ($invoice['proforma_invoice']['total_amount'] != 0) {
                        $processing_fee = $invoice['proforma_invoice']['currency']['cur_code'] . ' ' . $invoice['proforma_invoice']['total_amount'];
                    } else {
                        $processing_fee = '';
                    }
                    $content = str_replace("[" . $tag['tag_name'] . "]", $processing_fee, $content);
                    break;

                case 'Tuition Fee':
                    $programSchemeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id=' . $trans_id);
                    $programId = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'IdProgram', 'IdProgramScheme=' . $programSchemeId);

                    if ($programId == 2)
                        $level = 0;
                    else
                        $level = 1;

                    $class = new icampus_Function_Studentfinance_Invoice();
                    $proformaInfo = $class->getApplicantProformaInfo($trans_id, $level);

                    /*$proformaInfo['proforma_invoice']['total_amount']*/
                    $content = str_replace("[" . $tag['tag_name'] . "]", $proformaInfo['proforma_invoice']['currency']['cur_code'] . $proformaInfo['proforma_invoice']['total_amount'], $content);
                    break;

                case 'Date of Attend':
                    $content = str_replace("[" . $tag['tag_name'] . "]", date("j-m-Y"), $content);
                    break;

                case 'Time of Attend':
                    $content = str_replace("[" . $tag['tag_name'] . "]", date("g:i a"), $content);
                    break;

                case 'Vanue':
                    $content = str_replace("[" . $tag['tag_name'] . "]", 'INCIEF', $content);
                    break;

                case 'Program':
                    $programSchemeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id=' . $trans_id);
                    $programId = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'IdProgram', 'IdProgramScheme=' . $programSchemeId);
                    $program = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program', 'ProgramName', 'IdProgram=' . $programId);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $program, $content);
                    break;

                case 'Proforma Ref No':
                    $content = str_replace("[" . $tag['tag_name'] . "]", 'IN10/01897', $content);
                    break;

                case 'NRIC':
                    $profileId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_appl_id', 'at_trans_id = ' . $trans_id);
                    $nric = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_profile', 'appl_idnumber', 'appl_id = ' . $profileId);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $nric, $content);
                    break;

                case 'Mode Of Program':
                    $progSchemeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id = ' . $trans_id);
                    $mopId = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'mode_of_program', 'IdProgramScheme = ' . $progSchemeId);
                    $mop = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_definationms', 'DefinitionDesc', 'idDefinition = ' . $mopId);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $mop, $content);
                    break;

                case 'Mode Of Study':
                    $progSchemeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id = ' . $trans_id);
                    $mosId = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'mode_of_study', 'IdProgramScheme = ' . $progSchemeId);
                    $mos = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_definationms', 'DefinitionDesc', 'idDefinition = ' . $mosId);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $mos, $content);
                    break;

                case "Date of Commencement":
                    $transInfo = Application_Model_DbTable_ChecklistVerification::universalQueryRow('applicant_transaction', '*', 'at_trans_id = ' . $trans_id);
                    $intakeInfo = Application_Model_DbTable_ChecklistVerification::universalQueryRow('tbl_intake', '*', 'IdIntake = ' . $transInfo['at_intake']);
                    $semesterInfo = Application_Model_DbTable_ChecklistVerification::universalQueryRow('tbl_semestermaster', '*', 'AcademicYear = ' . $intakeInfo['sem_year'] . ' AND sem_seq = "' . $intakeInfo['sem_seq'] . '"');
                    $activityInfo = Application_Model_DbTable_ChecklistVerification::universalQueryRow('tbl_activity_calender', '*', 'IdSemesterMain = ' . $semesterInfo['IdSemesterMaster'] . ' AND IdActivity = 39');
                    $content = str_replace("[" . $tag['tag_name'] . "]", date("d-F-Y", strtotime($activityInfo['StartDate'])), $content);
                    break;

                case 'Programme':
                    $programSchemeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id=' . $trans_id);
                    $programId = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'IdProgram', 'IdProgramScheme=' . $programSchemeId);
                    $program = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program', 'ProgramName', 'IdProgram=' . $programId);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $program, $content);
                    break;

                case 'Mode of Programme':
                    $progSchemeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id = ' . $trans_id);
                    $mopId = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'mode_of_program', 'IdProgramScheme = ' . $progSchemeId);
                    $mop = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_definationms', 'DefinitionDesc', 'idDefinition = ' . $mopId);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $mop, $content);
                    break;

                case 'Mode of Study':
                    $progSchemeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id = ' . $trans_id);
                    $mosId = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'mode_of_study', 'IdProgramScheme = ' . $progSchemeId);
                    $mos = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_definationms', 'DefinitionDesc', 'idDefinition = ' . $mosId);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $mos, $content);
                    break;

                case 'Venue':
                    $transInfo = Application_Model_DbTable_ChecklistVerification::universalQueryRow('applicant_transaction', '*', 'at_trans_id = ' . $trans_id);
                    $info = Application_Model_DbTable_ChecklistVerification::universalQueryRow('intake_registration_location', '*', 'IdIntake = ' . $transInfo['at_intake']);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $info['rl_address'], $content);
                    break;

                case 'Program Type':
                    $progSchemeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id = ' . $trans_id);
                    $ptId = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'program_type', 'IdProgramScheme = ' . $progSchemeId);
                    $pt = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_definationms', 'DefinitionDesc', 'idDefinition = ' . $ptId);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $pt, $content);
                    break;

                case 'Semester':
                    $content = str_replace("[" . $tag['tag_name'] . "]", '2010 January Semester', $content);
                    break;

                case 'Proforma':
                    $programSchemeId = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id=' . $trans_id);
                    $programId = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program_scheme', 'IdProgram', 'IdProgramScheme=' . $programSchemeId);
                    $program = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program', 'ProgramName', 'IdProgram=' . $programId);
                    $programCode = Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program', 'ProgramCode', 'IdProgram=' . $programId);
                    //$content = str_replace("[".$tag['tag_name']."]", $program, $content);

                    if ($programId == 2)
                        $level = 0;
                    else
                        $level = 1;

                    $class = new icampus_Function_Studentfinance_Invoice();
                    $proformaInfo = $class->getApplicantProformaInfo($trans_id, $level);

                    /*$proformaInfo['proforma_invoice']['total_amount']*/
                    $content = str_replace("[" . $tag['tag_name'] . "]", $proformaInfo['proforma_invoice']['currency']['cur_code'] . $proformaInfo['proforma_invoice']['total_amount'], $content);
                    $proforma = "<table align='center' class='pure-table'>";
                    $proforma .= "<thead>";
                    $proforma .= "<tr>";
                    $proforma .= "<th>Item Code</th>";
                    $proforma .= "<th>Description</th>";
                    $proforma .= "<th>Type</th>";
                    $proforma .= "<th>Amount</th>";
                    $proforma .= "</tr>";
                    $proforma .= "</thead>";
                    $proforma .= "<tbody>";
                    if (count($proformaInfo['proforma_invoice_info']) > 0) {
                        foreach ($proformaInfo['proforma_invoice_info'] as $proformaInfoLoop) {
                            $proforma .= "<tr>";
                            //$proforma .= "<td>".$proformaInfoLoop['fc_code']."</td>";
                            $proforma .= "<td>" . $programCode . "</td>";
                            //$proforma .= "<td>".$proformaInfoLoop['fc_desc']."</td>";
                            $proforma .= "<td>" . $program . "</td>";
                            //$proforma .= "<td>".$proformaInfoLoop['invoice_type']."</td>";
                            $proforma .= "<td>" . $proformaInfoLoop['fc_desc'] . "</td>";
                            $proforma .= "<td>" . $proformaInfoLoop['bill_amount'] . "</td>";
                            $proforma .= "</tr>";
                        }
                    }
                    $proforma .= "<tr><td colspan='4' align='right'>Total : " . $proformaInfo['proforma_invoice']['currency']['cur_code'] . $proformaInfo['proforma_invoice']['total_amount'] . "</td></tr>";
                    $proforma .= "</tbody>";
                    $proforma .= "</table>";
                    $content = str_replace("[" . $tag['tag_name'] . "]", $proforma, $content);
                    break;


                case 'Fee - Student Union':

                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();

                    $invoice = $Invoice->getDataByTxnId($trans_id, 4);


                    $content = str_replace("[" . $tag['tag_name'] . "]", $invoice['bill_amount'], $content);


                    break;


                case 'Fee - Resource':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id, 6);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $invoice['bill_amount'], $content);
                    break;

                case 'Fee - Personal Bond':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id, 3);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $invoice['bill_amount'], $content);
                    break;

                case 'Fee - EMGS Visa Processing Fee':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id, 4);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $invoice['bill_amount'], $content);
                    break;

                case 'Fee - Student Pass Visa':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id, 12);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $invoice['bill_amount'], $content);
                    break;

                case 'Fee - Insurance':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id, 9);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $invoice['bill_amount'], $content);
                    break;

                case 'Fee - iKad':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id, 13);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $invoice['bill_amount'], $content);
                    break;

                case 'Fee - Medical Check-up':
                    $Invoice = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
                    $invoice = $Invoice->getDataByTxnId($trans_id, 11);
                    $content = str_replace("[" . $tag['tag_name'] . "]", $invoice['bill_amount'], $content);
                    break;
            }
        }
        return $content;
    }

    public function printApplicantSummaryAction()
    {
        $this->_helper->layout()->setLayout('/printstudentsummary');

        $txnId = $this->_getParam('id', 0);
        $this->view->txn_id = $txnId;

        //not required
        //$idSection = $this->_getParam('idSection', 0);
        //$this->view->idSection = $idSection;

        $this->view->tab = 'summary';

        $pid = $this->_getParam('pid', 0);
        $this->view->pid = $pid;

        //MODELS
        $appProfileDB = new App_Model_Application_DbTable_ApplicantProfile();
        $appDocumentDB = new App_Model_Application_DbTable_ApplicantDocument();
        $objdeftypeDB = new App_Model_Definitiontype();
        $appInitConfigDB = new Application_Model_DbTable_ApplicationInitialConfig();
        $applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $programDB = new App_Model_Application_DbTable_ApplicantProgram();
        $appQualificationDB = new App_Model_Application_DbTable_ApplicantQualification();
        $countryDB = new App_Model_General_DbTable_Country();
        $entryReqDB = new App_Model_Application_DbTable_EntryRequirement();
        $institutionDB = new App_Model_Application_DbTable_Institution();
        $appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();
        $appHealthDB = new App_Model_Application_DbTable_ApplicantHealthCondition();
        $appFinanacialDB = new App_Model_Application_DbTable_ApplicantFinancial();
        $appvisaDB = new App_Model_Application_DbTable_ApplicantVisa();
        $commDB = new Communication_Model_DbTable_General();

        //txn data
        $txnData = $applicantTxnDB->getTransaction($txnId, 1);
        $appl_id = $txnData['at_appl_id'];
        $transID = $txnData['at_trans_id'];
        $this->view->txnData = $txnData;
        //var_dump($txnData); exit;
        $this->view->session_transactionID = $transID;


        //programme applied
        $program = $programDB->getProgram($txnId);
        $this->view->program = $program;

        $photo = $appDocumentDB->getDataArrayBySection($transID, 2, 'Passport sized photo');
        $app_photo = $this->view->app_photo_full = '';
        if (!empty($photo)) {
            $this->view->app_photo_full = DOCUMENT_URL . $photo[0]['ad_filepath'];
            $app_photo = $this->view->baseUrl() . '/thumb.php?s=1&w=100&f=' . DOCUMENT_PATH . $photo[0]['ad_filepath'];
        }
        $this->view->app_photo = $app_photo;

        //echo $program['ap_prog_scheme'].'-'.$txnData['appl_category'];
        //exit;

        //get sidebar list
        $sectionDB = new Application_Model_DbTable_ApplicationSection();
        //$section_list = $sectionDB->getSection($program['ap_prog_scheme'], $txnData['appl_category'], $program['program_id']);
        $section_list = $sectionDB->getSection($program['ap_prog_id'], $program['ap_prog_scheme'], $txnData['appl_category']);


        $this->view->section_list = $section_list;
        //var_dump($section_list); //exit;
        //sectionData
        //$sectionData = $appInitConfigDB->fnGetDefination($idSection);
        //$mainsectionid = $sectionData['sectionID'];


        //Section items
        $sectionItems = array();


        foreach ($section_list as $section) {
            $listItem = $appInitConfigDB->getItem($program['ap_prog_scheme'], $section['id']);
            $sectionItems[$section['sectionID']] = $this->cleanSectionItem($listItem, $section['sectionID']);
        }

        $this->view->sectionItems = $sectionItems;
        //var_dump($sectionItems); exit;
        //-------------------
        // section 2
        // ------------------
        $applicant = $appProfileDB->getData($appl_id, 1);

        $this->view->applicantinfo = $applicant;

        $applicant['prev_student'] = $this->getBooleanText($applicant['prev_student']);
        $applicant['studentID'] = $applicant['prev_studentID'];

        //Applicant Documents
        $app_documents = $appDocumentDB->getDataArrayBySection($transID, 2);
        $this->view->app_docs = $app_documents;

        $applicant_documents = array();
        if (count($app_documents) > 0) {
            foreach ($app_documents as $doc) {
                //$this->view->$doc['ad_dcl_id'] = $doc['ad_dcl_id'];
                $filename = 'filename' . $doc['ad_dcl_id'];
                $file_url = '/documents' . $doc['ad_filepath'];
                $applicant_documents[] = '<a href="' . $file_url . '" target="_blank">' . $doc['ad_ori_filename'] . '</a>';
            }
        }
        $this->view->applicant_documents = $applicant_documents;


        $applicant['salutation'] = $applicant['appl_salutation'];
        $applicant['salutation'] = $this->getRealValueByDef($applicant['salutation'], $objdeftypeDB->fnGetDefinationsByLocale('Salutation'));
        $applicant['first_name'] = $applicant['appl_fname'];
        $applicant['last_name'] = $applicant['appl_lname'];
        $applicant['idNo'] = $applicant['appl_idnumber'];
        $applicant['typeID'] = $this->getRealValueByDef($applicant['appl_idnumber_type'], $objdeftypeDB->fnGetDefinationsByLocale('ID Type'));
        if ($applicant['appl_passport_expiry']) {
            $applicant['passport_expiry'] = date('d-m-Y', strtotime($applicant['appl_passport_expiry']));
        }
        $applicant['email'] = $applicant['appl_email'];
        $applicant['dob'] = date('d-m-Y', strtotime($applicant['appl_dob']));
        $applicant['gender'] = $applicant['appl_gender'] == '1' ? $this->view->translate('Male') : $this->view->translate('Female');
        $applicant['nationality'] = $this->getValueById($applicant['appl_nationality'], $countryDB->getData(), array('k' => 'idCountry', 'v' => 'CountryName'));
        $applicant['appl_category'] = $applicant['appl_category'];
        $applicant['age'] = $this->calcAge($applicant['dob']);

        $applicant['religion'] = $applicant['appl_religion'] == 99 ? $applicant['appl_religion_others'] : $this->getRealValueByDef($applicant['appl_religion'], $objdeftypeDB->fnGetDefinationsByLocale('Religion'));
        $applicant['race'] = $this->ifEmpty($applicant['appl_race']);
        $applicant['marital_status'] = $this->getRealValueByDef($applicant['appl_marital_status'], $objdeftypeDB->fnGetDefinationsByLocale('Marital Status'));
        $applicant['bumiputra_eligibility'] = $this->ifEmpty($applicant['appl_bumiputera']);

        $applicant['address_perm1'] = $applicant['appl_address1'];
        $applicant['address_perm2'] = $applicant['appl_address2'];
        $applicant['address_perm3'] = $applicant['appl_address3'];
        $applicant['postcode_perm'] = $applicant['appl_postcode'];
        $applicant['city_perm'] = $applicant['appl_city'] == 99 ? $applicant['appl_city_others'] : $applicant['appl_city'];
        $applicant['state_perm'] = $applicant['appl_state'] == 99 ? $applicant['appl_state_others'] : $this->getStateByCountryId($applicant['appl_state'], $applicant['appl_country'], $applicant['appl_state_others']);
        $applicant['country_perm'] = $applicant['appl_country'] == 99 ? $applicant['appl_country_others'] : $this->getValueById($applicant['appl_country'], $countryDB->getData(), array('k' => 'idCountry', 'v' => 'CountryName'));
        $applicant['home_perm'] = $applicant['appl_phone_home'];
        $applicant['mobile_perm'] = $applicant['appl_phone_mobile'];
        $applicant['office_perm'] = $applicant['appl_phone_office'];
        $applicant['fax_perm'] = $applicant['appl_fax'];

        $applicant['address_corr1'] = $applicant['appl_caddress1'];
        $applicant['address_corr2'] = $applicant['appl_caddress2'];
        $applicant['address_corr3'] = $applicant['appl_caddress3'];
        $applicant['postcode_corr'] = $applicant['appl_cpostcode'];
        $applicant['city_corr'] = $applicant['appl_ccity'] == 99 ? $applicant['appl_ccity_others'] : $applicant['appl_ccity'];
        $applicant['state_corr'] = $applicant['appl_cstate'] == 99 ? $applicant['appl_cstate_others'] : $this->getStateByCountryId($applicant['appl_cstate'], $applicant['appl_ccountry'], $applicant['appl_cstate_others']);
        $applicant['country_corr'] = $applicant['appl_ccountry'] == 99 ? $applicant['appl_ccountry_others'] : $this->getValueById($applicant['appl_ccountry'], $countryDB->getData(), array('k' => 'idCountry', 'v' => 'CountryName'));
        $applicant['home_corr'] = $applicant['appl_cphone_home'];
        $applicant['mobile_corr'] = $applicant['appl_cphone_mobile'];
        $applicant['office_corr'] = $applicant['appl_cphone_office'];
        $applicant['fax-corr'] = $applicant['appl_cfax'];

        $applicant['phone_home'] = $applicant['appl_contact_home'];
        $applicant['phone_mobile'] = $applicant['appl_contact_mobile'];
        $applicant['phone_office'] = $applicant['appl_contact_office'];


        //-------------------
        // section 3
        // ------------------

        //1st:track from current profile data exist or not
        $appQualification = $appQualificationDB->getTransData($appl_id, $txnId);

        //get documents
        if (count($appQualification) > 0) {
            foreach ($appQualification as $index => $q) {
                $documents = $appDocumentDB->getDataArray($txnId, $q['ae_id'], 'applicant_qualification');
                $appQualification[$index]['documents'] = $documents;
            }
        }

        $this->view->appQualification = $appQualification;

        if (isset($q['ae_id'])) {
            $q = $appQualificationDB->getQualification($q['ae_id']);

            $applicant['qualification_id'] = $this->getValueById($q['ae_qualification'], $entryReqDB->getListGeneralReq(), array('k' => 'IdQualification', 'v' => 'QualificationLevel'));
            $applicant['degree_awarded'] = $q['ae_degree_awarded'];
            $applicant['class_degree'] = $this->getRealValueByDef($q['ae_class_degree'], $objdeftypeDB->fnGetDefinations('Class Degree'));
            $applicant['result'] = $q['ae_result'];
            $applicant['year_graduated'] = $q['ae_year_graduate'];
            $applicant['institution_name'] = $this->getValueById($q['ae_institution'], $institutionDB->getData(), array('k' => 'idInstitution', 'v' => 'InstitutionName'));
            $applicant['medium_instruction'] = $q['ae_medium_instruction'];

        } else {
            //$applicant = array();
        }

        //-------------------
        // section 4
        // ------------------
        $appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
        $engprof = $appEngProfDB->getData($txnId);

        $app_documents = $appDocumentDB->getDataArrayBySection($txnId, 4);
        $this->view->app_docs = $app_documents;

        $this->view->pid = $engprof['ep_id'];
        $applicant['english_test'] = $this->getRealValueByDef($engprof['ep_test'], $objdeftypeDB->fnGetDefinations('English Proficiency Test List'));


        $applicant['english_test_detail'] = $engprof['ep_test_detail'];
        $applicant['english_date_taken'] = $engprof['ep_date_taken'];
        $applicant['english_score'] = $engprof['ep_score'];

        //-------------------
        // section 5 (not used)
        // ------------------

        $qpDB = new App_Model_Application_DbTable_ApplicantQuantitativeProficiency();
        $quanprof_list = $qpDB->getTransData($txnId);


        //get documents
        foreach ($quanprof_list as $index => $q) {

            $documents = $appDocumentDB->getDataArray($txnId, $q['qp_id'], 'applicant_quantitative_proficiency');
            $quanprof_list[$index]['documents'] = $documents;
        }

        $this->view->qp_list = $quanprof_list;
        if ($pid) {
            //$applicant = $qpDB->getData($pid);
        } else {
            //$applicant = array();
        }


        //-------------------
        // section 6 (not used)
        // ------------------


        $rpdDB = new App_Model_Application_DbTable_ApplicantResearchPublication();
        $research_list = $rpdDB->getTransData($txnId);
        //get documents
        foreach ($research_list as $index => $r) {

            $documents = $appDocumentDB->getDataArray($txnId, $r['rpd_id'], 'applicant_research_publication');
            $research_list[$index]['documents'] = $documents;
        }
        $this->view->rpd_list = $research_list;
        $this->view->total_added = count($research_list);


        if (isset($r['rpd_id'])) {
            //$applicant = $rpdDB->getData($r['rpd_id']);
        } else {
            //$applicant = array();
        }


        //-------------------
        // section 7 : Employment Details
        // ------------------

        $appEmployment = $appEmploymentDB->getTransData($appl_id, $txnId);

        if (count($appEmployment) > 0) {
            foreach ($appEmployment as $index => $emp) {
                $documents = $appDocumentDB->getDataArray($txnId, $emp['ae_id'], 'applicant_employment');
                $appEmployment[$index]['documents'] = $documents;
            }
        }
        $this->view->appEmployment = $appEmployment;
        $this->view->positionLevel = $objdeftypeDB->fnGetDefinations('Position Level');


        if (isset($emp['ae_id'])) {
            $emp = $appEmploymentDB->getEmployement($emp['ae_id']);
            $applicant['emply_status'] = $this->getRealValueByDef($emp['ae_status'], $objdeftypeDB->fnGetDefinations('Employment Status'));
            $applicant['emply_comp_name'] = $emp['ae_comp_name'];
            $applicant['emply_comp_address'] = $emp['ae_comp_address'];
            $applicant['emply_comp_phone'] = $emp['ae_comp_phone'];
            $applicant['emply_comp_fax'] = $emp['ae_comp_fax'];
            $applicant['emply_designation'] = $emp['ae_designation'];
            $applicant['emply_position_level'] = $emp['ae_position'];
            $applicant['emply_duration_from'] = $emp['ae_from'];
            $applicant['emply_duration_to'] = $emp['ae_to'];
            $applicant['emply_industry'] = $emp['ae_industry'];
            $applicant['job_desc'] = $emp['ae_job_desc'];
            $this->view->position = $emp['ae_position'];
        }


        //-------------------
        // section 8 : Working XP
        // ------------------


        $industryWorkingExperience = $objdeftypeDB->fnGetDefinations('Industry Working Experience');
        $this->view->industryWorkingExperience = $industryWorkingExperience;

        $this->view->yearofIndustry = $objdeftypeDB->fnGetDefinations('Years of Industry');
        //$applicant = array();

        //-------------------
        // section 9 : Health
        // ------------------
        $result = $objdeftypeDB->fnGetDefinationsByLocale('Health Condition');
        $applicant['health_condition'] = '';
        foreach ($result as $index => $r) {
            //get data
            $appHealth = $appHealthDB->getRowData($txnId, $r['idDefinition']);

            if (isset($appHealth) && $appHealth['ah_status'] != '') {
                $applicant['health_condition'] .= $r['DefinitionDesc'] . '<br />';
            }
        }

        //-------------------
        // section 10 : Financial Particulars
        // ------------------

        $appFinancial = $appFinanacialDB->getDataByTxn($txnId);
        $this->view->appFinancial = $appFinancial;

        if (isset($appFinancial) && $appFinancial['af_id'] != '') {
            $this->view->app_docs = $appDocumentDB->getDataArray($txnId, $appFinancial['af_id'], 'applicant_financial');
        }

        $this->view->pid = $appFinancial['af_id'];
        $applicant['fin_method'] = $this->getRealValueByDef($appFinancial['af_method'], $objdeftypeDB->fnGetDefinationsByLocale('Funding Method'));
        $applicant['fin_sponsor_name'] = $appFinancial['af_sponsor_name'];
        $applicant['fin_type_scholarship'] = $this->getRealValueByDef($appFinancial['af_type_scholarship'], $objdeftypeDB->fnGetDefinationsByLocale('Select Scholarship'));
        $applicant['fin_scholarship_secured'] = $appFinancial['af_scholarship_secured'];
        $applicant['fin_scholarship_apply'] = $appFinancial['af_scholarship_apply'];
        $applicant['file_fin_resume'] = $appFinancial['af_file'];


        //-------------------
        // section 11 : Visa Details
        // ------------------

        $visa = $appvisaDB->getTransData($txnId);
        $applicant['primary_id'] = $visa['av_id'];
        $applicant['visa_malaysian'] = $this->getBooleanText($visa['av_malaysian_visa']);
        $applicant['visa_status'] = $this->getRealValueByDef($visa['av_status'], $objdeftypeDB->fnGetDefinationsByLocale('Visa Status'));
        $applicant['visa_expiry'] = $visa['av_expiry'];


        //-------------------
        // section 12 : Accomodation
        // ------------------


        $accDB = new App_Model_Application_DbTable_ApplicantAccomodation();
        $accomodation = $accDB->getDatabyTxn($txnId);

        $applicant['primary_id'] = $accomodation['acd_id'];
        $applicant['acd_assistance'] = $accomodation['acd_assistance'];
        //$form->acd_type->setValue($accomodation['acd_type']);

        $roomTypeDB = new Hostel_Model_DbTable_Hostelroom();
        $room_list = $roomTypeDB->getRoomList(263);
        //$form->acd_type->setAttrib('class','accomodationtype');
        $table = '<table border=0 width="80%">
                                <tr>
                                   <th>' . $this->view->translate("Occupancy Type") . '</th>
                                   <th>' . $this->view->translate("Room Type") . '</th>
                                   <th>' . $this->view->translate("Monthly Rate") . '<br>(RM)</th>
                                   <th>' . $this->view->translate("Tick") . '</th>
                                </tr>';

        foreach ($room_list as $room) {
            $table .= "<tr>
                                        <td align='center'>" . $room['OccupancyType'] . "</td>
                                        <td align='center'>" . $room['RoomType'] . "</td>
                                        <td align='center'>" . $room['Rate'] . "</td><td  align='center'>";
            //$table .= $form->acd_type->addMultiOption($room['IdHostelRoomType']);
            $table .= "</td></tr>";
        }
        $table .= '</table>';

        $this->view->table = $table;

        // Spit them out
        $this->view->applicant = $applicant;
    }

    public function updatePaymentStatus($trxId)
    {
        $checklistVerification = new Application_Model_DbTable_ChecklistVerification();
        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        $fsProgramDB = new Application_Model_DbTable_ApplicantDocumentStatus();
        $checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();

        $info = $checklistVerification->infoBox($trxId);
        $checklistVerification->deleteStatusByTrx($trxId); //exit;
        //$checkDocExist = $fsProgramDB->getApplicantDocumentList($trxId);
        //$totalDocExist = count($checkDocExist);
        //var_dump($checkDocExist); //exit;

        //get applicant document status
        $docChecklist = $checklistVerificationDB->fnGetDocCheckList($info['ap_prog_scheme'], $info['appl_category'], $trxId);

        foreach ($docChecklist as $dataDoc) {

            $list = $checklistVerificationDB->universalQueryAllTable($dataDoc['table_name'], '*', $dataDoc['table_id_column'] . ' = ' . $trxId, $dataDoc['table_join'], $dataDoc['join_column1'], $dataDoc['join_column2']);
            //var_dump($list);

            if ($list) {
                foreach ($list as $listLoop) {
                    $adsCheck = $checklistVerificationDB->fnGetChecklistStatusByTxnId($trxId, $dataDoc['dcl_Id'], $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);
                    $adId = Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($trxId, $dataDoc['dcl_Id'], 'ad_id', $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);

                    if (!$adsCheck) { //update
                        $data = array(
                            'ads_txn_id'     => $trxId,
                            'ads_dcl_id'     => $dataDoc['dcl_Id'],
                            'ads_ad_id'      => $adId,
                            //'ads_appl_id'=>'',
                            //'ads_confirm'=>$check,
                            'ads_section_id' => $dataDoc['dcl_sectionid'],
                            'ads_table_name' => $dataDoc['table_name'],
                            'ads_table_id'   => $listLoop[$dataDoc['table_primary_id']],
                            'ads_status'     => 0,
                            'ads_comment'    => '',
                            'ads_createBy'   => $getUserIdentity->id,
                            'ads_createDate' => date('Y-m-d H:i:s')
                        );
                        $checklistVerificationDB->insert($data);
                    }
                }
            }
        }

        //loop by transaction start
        $reciptId = $checklistVerification->getReceiptId($trxId);
        //var_dump($reciptId); exit;
        if ($reciptId) {
            foreach ($reciptId as $reciptIdLoop) {
                //invoice
                $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
                $invoice_list = $invoiceMainDb->getInvoiceFromReceipt($reciptIdLoop['rcp_id']);

                $inv = array();
                $total_payment = 0.00;
                $total_balance = 0.00;
                //var_dump($invoice_list); exit;
                if ($invoice_list) {
                    foreach ($invoice_list as $invoice) {
                        $iv = array(
                            'invoice_no' => $invoice['bill_number'],
                            'amount'     => 0,
                            'currency'   => $invoice['cur_symbol_prefix']
                        );
                        //var_dump($invoice['receipt_invoice']); exit;
                        foreach ($invoice['receipt_invoice'] as $ri) {
                            $iv['amount'] += $ri['rcp_inv_amount'];
                            $total_payment += $ri['rcp_inv_amount'];
                            $total_balance += $ri['balance'];

                            $feeID = $ri['fi_id'];
                            $receipt_no = $ri['rcp_inv_rcp_no'];
                            $bill_paid = $ri['rcp_inv_amount'];
                            $currencyPaid = $ri['cur_symbol_prefix'];

                            /*
                            * get fee structure program item
                            * update docuemnt checklist status
                            */
                            $fsProgram = $fsProgramDB->getApplicantDocumentType($trxId, $feeID);
                            $newArray1 = array();
                            $ads_id = $fsProgram['ads_id'];
                            if (isset($ads_id)) {
                                $arrayAds [] = array(
                                    'id'         => $ads_id,
                                    'total'      => $bill_paid,
                                    'balance'    => $total_balance,
                                    'cur'        => $currencyPaid,
                                    'receipt_no' => $receipt_no
                                );
                            }
                            array_push($newArray1, $arrayAds);
                        }

                        $inv[] = $iv;
                    }

                    //update checklist
                    $this->updateChecklist($arrayAds);
                }
            }
        }//loop by transaction end

        return true;
    }

    public function updateWaivedStatus($trxId)
    {
        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        $checklistVerificationDB = new Application_Model_DbTable_ChecklistVerification();

        $info = $checklistVerificationDB->infoBox($trxId);
        $checklistVerificationDB->deleteStatusByTrx($trxId); //exit;

        //get applicant document status
        $docChecklist = $checklistVerificationDB->fnGetDocCheckList($info['ap_prog_scheme'], $info['appl_category'], $trxId);

        foreach ($docChecklist as $dataDoc) {

            $list = $checklistVerificationDB->universalQueryAllTable($dataDoc['table_name'], '*', $dataDoc['table_id_column'] . ' = ' . $trxId, $dataDoc['table_join'], $dataDoc['join_column1'], $dataDoc['join_column2']);
            //var_dump($list);

            if ($list) {
                foreach ($list as $listLoop) {
                    $adsCheck = $checklistVerificationDB->fnGetChecklistStatusByTxnId($trxId, $dataDoc['dcl_Id'], $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);
                    $adId = Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($trxId, $dataDoc['dcl_Id'], 'ad_id', $listLoop[$dataDoc['table_primary_id']], $dataDoc['dcl_sectionid']);

                    if (!$adsCheck) { //update
                        $data = array(
                            'ads_txn_id'     => $trxId,
                            'ads_dcl_id'     => $dataDoc['dcl_Id'],
                            'ads_ad_id'      => $adId,
                            //'ads_appl_id'=>'',
                            //'ads_confirm'=>$check,
                            'ads_section_id' => $dataDoc['dcl_sectionid'],
                            'ads_table_name' => $dataDoc['table_name'],
                            'ads_table_id'   => $listLoop[$dataDoc['table_primary_id']],
                            'ads_status'     => 0,
                            'ads_comment'    => '',
                            'ads_createBy'   => $getUserIdentity->id,
                            'ads_createDate' => date('Y-m-d H:i:s')
                        );
                        $checklistVerificationDB->insert($data);
                    }
                }
            }
        }

        //get proforma
        $proformas = $checklistVerificationDB->getWaivedProforma($trxId);

        if ($proformas) {
            foreach ($proformas as $proforma) {
                $db = Zend_Db_Table::getDefaultAdapter();

                $select = $db->select()
                    ->from(array('a' => 'proforma_invoice_main'))
                    ->join(array('b' => 'proforma_invoice_detail'), 'b.proforma_invoice_main_id = a.id', array('*'))
                    ->where('a.id =?', $proforma['id']);

                $txnDataApp = $db->fetchAll($select);

                $trans_id = $txnDataApp[0]['trans_id'];

                $paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
                $paymentClass->checkingApplicantDocumentChecklist($trans_id);

                foreach ($txnDataApp as $txnData) {

                    $feeID = $txnData['fi_id'];

                    $fsProgramDB = new Application_Model_DbTable_ApplicantDocumentStatus();
                    $fsProgram = $fsProgramDB->getApplicantDocumentType($trans_id, $feeID);
                    $ads_id = $fsProgram['ads_id'];

                    if ($ads_id) {
                        $newStatus = '';
                        if ($proforma['status'] == 'E') {
                            $newStatus = 'Exempted';
                        } else if ($proforma['status'] == 'W') {
                            $newStatus = 'Waived';
                        } else if ($proforma['status'] == 'S') {
                            $newStatus = 'Sponsorship';
                        }

                        $docStatus = array(
                            'ads_status'  => 5,//not applicable
                            'ads_comment' => $newStatus . ', ' . $proforma['remarks'],
                            'ads_modBy'   => $getUserIdentity->id,
                            'ads_modDate' => date('Y-m-d H:i:s'),
                        );

                        $appDocStatusDB = new Application_Model_DbTable_ApplicantDocumentStatus();
                        $appDocStatusDB->update($docStatus, array('ads_id =?' => $ads_id));
                    }
                }
            }
        }

        return true;
    }

    private function updateChecklist(&$arrayAds)
    {
        //var_dump($arrayAds); exit;
        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        $tmp = array();
        $n = 0;
        foreach ($arrayAds as $arg) {
            $tmp[$arg['id']]['tot'][$n] = $arg['total'];
            $tmp[$arg['id']]['bal'][$n] = $arg['balance'];
            $tmp[$arg['id']]['cur'] = $arg['cur'];
            $tmp[$arg['id']]['receipt_no'] = $arg['receipt_no'];
            $n++;
        }
        //var_dump($tmp); exit;
        $output = array();
        $m = 0;
        foreach ($tmp as $type => $labels) {
            $output[$m] = array(
                'id'      => $type,
                'total'   => number_format(array_sum($labels['tot']), 2),
                'balance' => number_format(array_sum($labels['bal']), 2),
                'cur'     => $labels['cur'],
                'receipt' => $labels['receipt_no']
            );
            $m++;
        }
        //var_dump($output); exit;
        //update applicant document type checklist
        foreach ($output as $out) {
            if ($out['balance'] == '0') {
                $docStatus = array(
                    'ads_status'  => 4,//completed
                    'ads_comment' => $out['receipt'] . ", " . $out['cur'] . "" . $out['total'],
                    'ads_modBy'   => $getUserIdentity->id,
                    'ads_modDate' => date('Y-m-d H:i:s'),
                );
                //var_dump($docStatus); exit;
                $appDocStatusDB = new Application_Model_DbTable_ApplicantDocumentStatus();
                $appDocStatusDB->update($docStatus, array('ads_id =?' => $out['id']));
            }
        }
    }

    public function deleteEducationDetailsAction()
    {
        $id = $this->_getParam('id', 0);
        $txnid = $this->_getParam('txnid', 0);
        $this->model->deleteEducationDetails($id);
        //var_dump($id); exit;
        $this->_helper->flashMessenger->addMessage(array('success' => "Data Has Been Deleted."));
        $this->_redirect($this->baseUrl . '/application/index/edit/id/' . $txnid . '/idSection/3');
    }

    public function deleteEmploymentDetailsAction()
    {
        $id = $this->_getParam('id', 0);
        $txnid = $this->_getParam('txnid', 0);
        $this->model->deleteEmploymentDetails($id);
        //var_dump($id); exit;
        $this->_helper->flashMessenger->addMessage(array('success' => "Data Has Been Deleted."));
        $this->_redirect($this->baseUrl . '/application/index/edit/id/' . $txnid . '/idSection/7');
    }
}

?>