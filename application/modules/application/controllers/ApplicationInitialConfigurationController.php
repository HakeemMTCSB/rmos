<?php

class Application_ApplicationInitialConfigurationController extends Zend_Controller_Action
{
    private $locale;
    private $registry;
    private $lobjAppInitConfig;
    private $lobjAppConfig;
    private $lobjAppItem;
    private $lobjAppItemTagging;
    private $lobjProg;
    private $auth;
    private $_gobjlog;
    private $scholarshipTagModel;
    private $defModel;
    private $statusModel;
    private $stpModel;

    public function init()
    {

        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();

    }

    public function fnsetObj()
    {
        $this->lobjAppInitConfig = new Application_Model_DbTable_ApplicationInitialConfiguration(); // ApplicationInitialConfiguration model object
        $this->lobjAppConfig = new Application_Model_DbTable_ApplicationInitialConfig();
        $this->lobjAppItem = new Application_Model_DbTable_ApplicationItem();
        $this->lobjProg = new Application_Model_DbTable_ProgramScheme();
        $this->lobjAppItemTagging = new Application_Model_DbTable_ApplicationItemTagging();
        $this->scholarshipTagModel = new Studentfinance_Model_DbTable_ScholarshipTagging();
        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->statusModel = new Application_Model_DbTable_Status();
        $this->stpModel = new Application_Model_DbTable_StatusTemplate();
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
    }

    public function sectionAction()
    {
        $this->view->title = $this->view->translate("Application Initial Configuration");

        $larrResultSection = $this->lobjAppInitConfig->fnGetSectionList();

        $this->view->sectionList = $larrResultSection;
    }

    public function itemAction()
    {
        $this->view->title = $this->view->translate("Application Initial Configuration");

        $larrResultItem = $this->lobjAppItem->fnGetItemList();

        $this->view->itemList = $larrResultItem;
    }


    public function indexAction()
    {
        $this->view->title = $this->view->translate("Application Initial Configuration");
        $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'index'), 'default', true);

        $configList = $this->lobjAppConfig->getListConfigs();

        $this->view->configs = $configList;
    }

    public function addConfigAction()
    {

        $this->view->title = 'Add Application Initial Configuration';

        $getUserIdentity = $this->auth->getIdentity();
        $db = Zend_Db_Table::getDefaultAdapter();

        $this->view->form = new Application_Form_ApplicationConfigs();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $checkExist = $this->lobjAppConfig->getConfig($formData['IdScheme'], $formData['IdProgram'] ?? 0, $formData['IdProgramScheme'] ?? 0, $formData['category'] ?? 0,$formData['active'], $formData['source']);

            if (!$checkExist) {

                $data = array(
                    'IdScheme'        => $formData['IdScheme'],
                    'IdProgram'       => $formData['IdProgram'] ?? 0,
                    'IdProgramScheme' => $formData['IdProgramScheme'] ?? 0,
                    'category'        => $formData['category'] ?? 0,
                    'source'          => $formData['source'],
                    'active'          => $formData['active'],
                    'created_at'      => date('Y-m-d H:i:s'),
                    'created_by'      => $getUserIdentity->id
                );

                $db->insert('tbl_application_configs', $data);

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully added"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('error' => "Configuration already exist"));
            }

            $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'index'), 'default', true));

        }
    }

    /*
     * 
     * list section
     * 
     */

    public function programSchemeListAction()
    {
        $program_id = $this->_getParam('id', null);
        $programinfo = $this->lobjProg->fnGetProgramBasedOnProgId($program_id);
        $this->view->title = $this->view->translate("Program Scheme List - " . $programinfo['ProgramName']);
        $programschemelist = $this->lobjProg->fnGetProgramschemeListBasedOnProgId($program_id);

        if (count($programschemelist) > 0) {
            $i = 0;
            foreach ($programschemelist as $programschemeloop) {
                $mop = $this->defModel->getData($programschemeloop['mode_of_program']);
                $mod = $this->defModel->getData($programschemeloop['mode_of_study']);
                $programType = $this->defModel->getData($programschemeloop['program_type']);

                $programschemelist[$i]['mopName'] = $mop['DefinitionDesc'];
                $programschemelist[$i]['modName'] = $mod['DefinitionDesc'];
                $programschemelist[$i]['ptName'] = $programType['DefinitionDesc'];

                $i++;
            }
        }

        $this->view->programschemelist = $programschemelist;
    }

    public function sectionListAction()
    {

        $this->view->title = $this->view->translate("Configuration Details");
        $id = $this->view->id = $this->_getParam('id', null);

        $configInfo = $this->lobjAppConfig->getListConfigs($id);
        $sectionList = $this->lobjAppConfig->getSectionBasedOnId($id);

        $this->view->config = $configInfo;
        $this->view->sectionList = $sectionList;
        $this->view->id = $id;
    }

    public function itemListAction()
    {
        $section_id = $this->_getParam('section_id', null);

        $this->view->title = $this->view->translate("Item List");

        $itemList = $this->lobjAppItemTagging->fnGetItemTaggingBasedOnSection($section_id);
        $section_info = $this->lobjAppConfig->fnGetSectionConfById($section_id);

        $this->view->itemList = $itemList;
        $this->view->sectionid = $section_id;
        $this->view->section = $section_info;
    }

    public function statusListAction()
    {

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $id = $this->_getParam('id', null);

//        $programshemeinfo = $this->lobjProg->fnGetProgramschemeListBasedOnId($programscheme_id);
        $statusList = $this->statusModel->getStatusList($id);


        /*if (count($statusList) > 0) {
            $i = 0;
            foreach ($statusList as $statusLoop) {
                $statusName = $this->defModel->getData($statusLoop['sts_name']);
                $message = $this->stpModel->fnGetStatusTemplateListById($statusLoop['sts_message']);
                $emailNotification = $this->stpModel->fnGetStatusTemplateListById($statusLoop['sts_emailNotification']);

                $statusList[$i]['statusName'] = $statusName['DefinitionDesc'];
                $statusList[$i]['message'] = $message['stp_title'];
                $statusList[$i]['emailNotification'] = $emailNotification['stp_title'];

                $i++;
            }
        }*/

        $this->view->title = $this->view->translate("Status List");
        $this->view->statusList = $statusList;
    }

    public function programmeDetailsSectionAction()
    {
        $program_id = $this->_getParam('id', null);
        $this->view->title = $this->view->translate("Program Details Section");
        $programInfo = $this->lobjAppInitConfig->fnGetProgramById($program_id);
        $sectionList = $this->lobjAppInitConfig->fnGetItemBySection(1);
        //var_dump($sectionList);
        $this->view->sectionList = $sectionList;
        $this->view->programInfo = $programInfo;
        $this->view->programId = $program_id;
    }

    public function itemTooltipSetupAction()
    {
        $program_id = $this->_getParam('id', null);
        $item_id = $this->_getParam('item_id', null);
        $this->view->title = $this->view->translate("Tooltip Setup");
        $programInfo = $this->lobjAppInitConfig->fnGetProgramById($program_id);
        $itemInfo = $this->lobjAppItem->fnGetItemById($item_id);
        $this->view->programInfo = $programInfo;
        $this->view->itemInfo = $itemInfo;
        $this->view->programId = $program_id;
        $this->view->itemId = $item_id;
        $getUserIdentity = $this->auth->getIdentity();

        $this->view->alert = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if (isset($formData['value']) && count($formData['value']) > 0) {
                $i = 0;
                foreach ($formData['value'] as $valueLoop) {
                    $checkTooltip = $this->lobjAppInitConfig->fnGetTooltipInfo($formData['item'], $formData['program'], $valueLoop);

                    if ($checkTooltip == false) {
                        $data = array(
                            'tts_program_id' => $formData['program'],
                            'tts_item_id'    => $formData['item'],
                            'tts_value_id'   => $valueLoop,
                            'tts_desc'       => $formData['content'][$i],
                            'tts_updUser'    => $getUserIdentity->id,
                            'tts_updDate'    => date('Y-m-d H:i:s')
                        );
                        $this->lobjAppInitConfig->insertTooltip($data);
                    } else {
                        $data = array(
                            'tts_desc'    => $formData['content'][$i],
                            'tts_updUser' => $getUserIdentity->id,
                            'tts_updDate' => date('Y-m-d H:i:s')
                        );
                        $this->lobjAppInitConfig->updateTooltip($data, 'tts_program_id = ' . $formData['program'] . ' and tts_item_id = ' . $formData['item'] . ' and tts_value_id = ' . $valueLoop);
                    }
                    $i++;
                }
                $this->view->alert = 1;
            }
        }

        $value = array();
        switch ($item_id) {
            case '1':
                $value[0]['value'] = 999;
                $value[1]['name'] = "Programme";
                break;

            case '2':
                $listValue = $this->lobjAppInitConfig->getProgramMode($program_id, 'mode_of_program');

                if (count($listValue) > 0) {
                    $i = 0;
                    foreach ($listValue as $listValueLoop) {
                        $value[$i]['value'] = $listValueLoop['mode_of_program'];
                        $value[$i]['name'] = $listValueLoop['DefinitionDesc'];
                        $getTooltipInformation = $this->lobjAppInitConfig->fnGetTooltipInfo($item_id, $program_id, $listValueLoop['mode_of_program']);
                        $value[$i]['content'] = ($getTooltipInformation != FALSE ? $getTooltipInformation['tts_desc'] : "");
                        $i++;
                    }
                }
                break;

            case '3':
                $listValue = $this->lobjAppInitConfig->getProgramMode($program_id, 'mode_of_study');

                if (count($listValue) > 0) {
                    $i = 0;
                    foreach ($listValue as $listValueLoop) {
                        $value[$i]['value'] = $listValueLoop['mode_of_study'];
                        $value[$i]['name'] = $listValueLoop['DefinitionDesc'];
                        $getTooltipInformation = $this->lobjAppInitConfig->fnGetTooltipInfo($item_id, $program_id, $listValueLoop['mode_of_study']);
                        $value[$i]['content'] = ($getTooltipInformation != FALSE ? $getTooltipInformation['tts_desc'] : "");
                        $i++;
                    }
                }
                break;

            case '4':
                $listValue = $this->lobjAppInitConfig->getIntake($program_id);

                if (count($listValue) > 0) {
                    $i = 0;
                    foreach ($listValue as $listValueLoop) {
                        $value[$i]['value'] = $listValueLoop['ip_id'];
                        $value[$i]['name'] = $listValueLoop['IntakeDesc'] . ' :- Start date: ' . $listValueLoop['ApplicationStartDate'] . ', End date: ' . $listValueLoop['ApplicationEndDate'];
                        $getTooltipInformation = $this->lobjAppInitConfig->fnGetTooltipInfo($item_id, $program_id, $listValueLoop['ip_id']);
                        $value[$i]['content'] = ($getTooltipInformation != FALSE ? $getTooltipInformation['tts_desc'] : "");
                        $i++;
                    }
                }
                break;

            case '15':
                $listValue = $this->lobjAppInitConfig->getProgramMode($program_id, 'program_type');

                if (count($listValue) > 0) {
                    $i = 0;
                    foreach ($listValue as $listValueLoop) {
                        $value[$i]['value'] = $listValueLoop['program_type'];
                        $value[$i]['name'] = $listValueLoop['DefinitionDesc'];
                        $getTooltipInformation = $this->lobjAppInitConfig->fnGetTooltipInfo($item_id, $program_id, $listValueLoop['program_type']);
                        $value[$i]['content'] = ($getTooltipInformation != FALSE ? $getTooltipInformation['tts_desc'] : "");
                        $i++;
                    }
                }
                break;
        }
        //var_dump($value);
        $this->view->value = $value;
    }

    /*
     * 
     * CRUD section
     * 
     */

    public function editSectionAction()
    {
        $section_id = $this->_getParam('id', null);
        $larrResult = $this->lobjAppInitConfig->fnGetSectionById($section_id);
        $formSection = new Application_Form_EditSection();
        $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'index'), 'default', true);

        $getUserIdentity = $this->auth->getIdentity();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($formSection->isValid($formData)) {

                $larrResult = array(
                    'name'        => $formData['name'],
                    'instruction' => $formData['instruction'],
                    'status'      => $formData['status'],
                    'desc'        => $formData['desc'],
                    'modDate'     => date('Y-m-d H:i:s'),
                    'modBy'       => $getUserIdentity->id
                );

                $this->lobjAppInitConfig->update($larrResult, 'id = ' . $section_id);

                //redirect
                $this->_redirect($this->view->backURL);
            } else {
                $formSection->populate($formData);
            }

        } else {
            $formSection->populate($larrResult);
        }

        $this->view->title = $this->view->translate("Application Initial Configuration - Edit Section - " . $larrResult['name']);
        $this->view->section = $larrResult;
        $this->view->formSection = $formSection;
    }

    public function addSectionTaggingAction()
    {
        $id = $this->_getParam('id', null);

        $addSectionTaggingForm = new Application_Form_SectionTaggedToProgramScheme(array('addoredit' => 'add'));

        $getUserIdentity = $this->auth->getIdentity();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($addSectionTaggingForm->isValid($formData)) {

                $countSection = count($this->lobjAppConfig->getSectionBasedOnId($id));

                if (isset($formData['section_mandatory']) && $formData['section_mandatory'] == '1') {
                    $formData['section_mandatory'] = '1';
                } else {
                    $formData['section_mandatory'] = '0';
                }

                $data = array(
                    'IdConfig'              => $id,
                    'sectionID'             => $formData['sectionID'],
                    'section_maxEntry'      => $formData['section_maxEntry'],
                    'section_mandatory'     => $formData['section_mandatory'],
                    'seq_order'             => $countSection + 1,
                    'sectionTagInstruction' => $formData['sectionTagInstruction'],
                    'initialconfigdesc'     => $formData['initialconfigdesc'],
                    'createDate'            => date('Y-m-d H:i:s'),
                    'createBy'              => $getUserIdentity->id
                );

                $this->lobjAppConfig->insert($data);

                //redirect
                $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'section-list', 'id' => $id), 'default', true));
            } else {
                $addSectionTaggingForm->populate($formData);
            }
        }

        $this->view->title = $this->view->translate("Application Initial Configuration - Add Section Tagging");
        $this->view->addSectionTaggingForm = $addSectionTaggingForm;
    }

    public function copySectionTaggingAction()
    {

        $this->view->title = $this->view->translate("Application Initial Configuration - Copy Section Tagging");

        $id = $this->_getParam('id', null);

        $configInfo = $this->lobjAppConfig->getListConfigs($id);
        $sectionList = $this->lobjAppConfig->getSectionBasedOnId($id);

        $progscheme_id = $configInfo['IdProgramScheme'];
        $stdCtgy = $configInfo['category'];
        $IdProgram = $configInfo['IdProgram'];
        $IdScheme = $configInfo['IdScheme'];

        $this->view->config = $configInfo;
        $this->view->sectionList = $sectionList;

        $copySectionTaggingForm = new Application_Form_CopySection();

        $db = Zend_Db_Table::getDefaultAdapter();
        $getUserIdentity = $this->auth->getIdentity();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($copySectionTaggingForm->isValid($formData)) {

                if ($formData['IdProgramScheme'] == $progscheme_id &&
                    $formData['IdProgram'] == $IdProgram &&
                    $formData['IdScheme'] == $IdScheme &&
                    $formData['category'] == $stdCtgy
                ) {

                    //redirect
                    $this->_helper->flashMessenger->addMessage(array('error' => "Failed to copy. Configuration already exist."));
                    $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'copy-section-tagging', 'id' => $progscheme_id, 'stdCtgy' => $stdCtgy), 'default', true));

                } else {

                    //@todo copy section function
                    $sectionCopylist = $this->lobjAppConfig->getCopySection($formData['IdScheme'], $formData['IdProgram'] ?? 0, $formData['IdProgramScheme'] ?? 0, $formData['category'] ?? 0);

                    if (count($sectionCopylist)) {

                        foreach ($sectionCopylist as $sectionCopyloop) {
                            $countSection = count($this->lobjAppConfig->getSectionBasedOnId($id));

                            $data = array(
                                'IdConfig'              => $id,
                                'sectionID'             => $sectionCopyloop['sectionID'],
                                'section_maxEntry'      => $sectionCopyloop['section_maxEntry'],
                                'section_mandatory'     => $sectionCopyloop['section_mandatory'],
                                'seq_order'             => $countSection + 1,
                                'sectionTagInstruction' => $sectionCopyloop['sectionTagInstruction'],
                                'initialconfigdesc'     => $sectionCopyloop['initialconfigdesc'],
                                'createDate'            => date('Y-m-d H:i:s'),
                                'createBy'              => $getUserIdentity->id
                            );

                            $lastinsertid = $this->lobjAppConfig->insert($data);

                            $itemCopyList = $this->lobjAppConfig->getCopyItem($sectionCopyloop['key']);

                            if (count($itemCopyList)) {
                                foreach ($itemCopyList as $itemCopyLoop) {
                                    $getDclCopy = $this->lobjAppConfig->getDclCopy($itemCopyLoop['item_dclId']);

                                    if ($getDclCopy) {
                                        $checkDclCopy = $this->lobjAppConfig->checkDclCopy($progscheme_id, $stdCtgy, $sectionCopyloop['sectionID'], $getDclCopy['dcl_uplType']);

                                        if ($checkDclCopy) {
                                            $itemDclId = $checkDclCopy['dcl_Id'];
                                        } else {
                                            $dataDcl = array(
                                                'dcl_stdCtgy'       => $stdCtgy,
                                                'dcl_programScheme' => $progscheme_id,
                                                'dcl_sectionid'     => $sectionCopyloop['sectionID'],
                                                'dcl_name'          => $getDclCopy['dcl_name'],
                                                'dcl_malayName'     => $getDclCopy['dcl_malayName'],
                                                'dcl_desc'          => $getDclCopy['dcl_desc'],
                                                'dcl_type'          => $getDclCopy['dcl_type'],
                                                'dcl_activeStatus'  => $getDclCopy['dcl_activeStatus'],
                                                'dcl_priority'      => $getDclCopy['dcl_priority'],
                                                'dcl_uplStatus'     => $getDclCopy['dcl_uplStatus'],
                                                'dcl_uplType'       => $getDclCopy['dcl_uplType'],
                                                'dcl_finance'       => $getDclCopy['dcl_finance'],
                                                'dcl_mandatory'     => $getDclCopy['dcl_mandatory'],
                                                'dcl_addDate'       => date('Y-m-d H:i:s'),
                                                'dcl_addBy'         => $getUserIdentity->id
                                            );

                                            $itemDclId = $this->lobjAppConfig->copyDcl($dataDcl);
                                        }
                                    } else {
                                        $itemDclId = 0;
                                    }

                                    $countItem = count($this->lobjAppConfig->getItemBasedOnSection($lastinsertid));
                                    $dataItem = array(
                                        'idTagSection'            => $lastinsertid,
                                        'idItem'                  => $itemCopyLoop['idItem'],
                                        'item_dclId'              => $itemDclId,
                                        'view'                    => $itemCopyLoop['view'],
                                        'compulsory'              => $itemCopyLoop['compulsory'],
                                        'enable'                  => $itemCopyLoop['enable'],
                                        'hide'                    => $itemCopyLoop['hide'],
                                        'item_docDownload'        => $itemCopyLoop['item_docDownload'],
                                        'itemInstructionPosition' => $itemCopyLoop['itemInstructionPosition'],
                                        'seq_order'               => $countItem + 1,
                                        'itemTagInstruction'      => $itemCopyLoop['itemTagInstruction'],
                                        'tagDesc'                 => $itemCopyLoop['tagDesc'],
                                        'createDate'              => date('Y-m-d H:i:s'),
                                        'createBy'                => $getUserIdentity->id
                                    );

                                    $this->lobjAppItemTagging->insert($dataItem);
                                }
                            }
                        }
                    }
                }
                //redirect
                $this->_helper->flashMessenger->addMessage(array('success' => "Succesfully Copied"));
                $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'section-list', 'id' => $id), 'default', true));
            }
        }

        $this->view->id = $id;
        $this->view->copySectionTaggingForm = $copySectionTaggingForm;
    }

    public function copySectionPreviewAction()
    {
        $schemeId = $this->_getParam('schemeId', 0);
        $programId = $this->_getParam('programId', 0);
        $programSchemeId = $this->_getParam('programSchemeId', 0);
        $stdCtgy = $this->_getParam('stdCtgy', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $sectionCopylist = $this->lobjAppConfig->getCopySection($schemeId, $programId, $programSchemeId, $stdCtgy);

        if (count($sectionCopylist)) {
            $i = 0;
            foreach ($sectionCopylist as $sectionCopyloop) {
                $itemCopyList = $this->lobjAppConfig->getCopyItem($sectionCopyloop['key']);
                $sectionCopylist[$i]['itemCount'] = count($itemCopyList);
                if (count($itemCopyList)) {
                    $j = 0;
                    foreach ($itemCopyList as $itemCopyLoop) {
                        $getDclCopy = $this->lobjAppConfig->getDclCopy($itemCopyLoop['item_dclId']);

                        if ($getDclCopy) {
                            $sectionCopylist[$i]['ChecklistCopy'][$j] = $getDclCopy['dcl_name'];
                        }
                        $j++;
                    }
                }

                $i++;
            }
        }

        $json = Zend_Json::encode($sectionCopylist);

        echo $json;
        exit();
    }

    public function editSectionTaggingAction()
    {
        $id = $this->_getParam('id', null);
        $section_id = $this->_getParam('section_id', null);

        $editSectionTaggingForm = new Application_Form_SectionTaggedToProgramScheme();
        $getSectionInfo = $this->lobjAppConfig->fnGetSectionConfJoinWithAppById($section_id);

        $getUserIdentity = $this->auth->getIdentity();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($editSectionTaggingForm->isValid($formData)) {

                if (isset($formData['section_mandatory']) && $formData['section_mandatory'] == '1') {
                    $formData['section_mandatory'] = '1';
                } else {
                    $formData['section_mandatory'] = '0';
                }

                $data = array(
                    'section_maxEntry'      => $formData['section_maxEntry'],
                    'section_mandatory'     => $formData['section_mandatory'],
                    'sectionTagInstruction' => $formData['sectionTagInstruction'],
                    'initialconfigdesc'     => $formData['initialconfigdesc'],
                    'initModDate'           => date('Y-m-d H:i:s'),
                    'initModBy'             => $getUserIdentity->id
                );

                $this->lobjAppConfig->update($data, 'id = ' . $section_id);

                //redirect
                $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'section-list', 'id' => $id), 'default', true));
            } else {
                $editSectionTaggingForm->populate($getSectionInfo);
            }

        } else {
            $editSectionTaggingForm->populate($getSectionInfo);
        }

        $this->view->title = $this->view->translate("Application Initial Configuration - Edit Section Tagging");
        $this->view->item = $getSectionInfo;
        $this->view->editSectionTaggingForm = $editSectionTaggingForm;
        $this->view->id = $id;
    }

    public function deleteSectionTaggingAction()
    {
        $id = $this->_getParam('id', null);
        $section_id = $this->_getParam('section_id', null);

        $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'section-list', 'id' => $id), 'default', true);

        $this->lobjAppItemTagging->delete('idTagSection =' . $section_id);
        $this->lobjAppConfig->delete('id =' . $section_id);

        $sectionInfo = $this->lobjAppConfig->getSectionBasedOnId($id);

        foreach ($sectionInfo as $index => $programInfo) {
            $data = array(
                'seq_order' => $index + 1,
            );

            $this->lobjAppConfig->update($data, "id = " . $programInfo['key']);
        }

        //redirect
        $this->_redirect($this->view->backURL);
    }

    public function deleteListAction()
    {
        $id = $this->_getParam('id', null);
        $db = Zend_Db_Table::getDefaultAdapter();

        $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'index'), 'default', true);

        $db->update('tbl_application_configs', array('active' => 0), 'id =' . $id);

        //redirect
        $this->_helper->flashMessenger->addMessage(array('success' => "Configuration updated"));

        $this->_redirect($this->view->backURL);
    }

    public function editItemAction()
    {
        $item_id = $this->_getParam('id', null);
        $formEditItem = new Application_Form_EditItem();
        $larrResult = $this->lobjAppItem->fnGetItemById($item_id);
        $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'index'), 'default', true) . '#tab-3';

        $getUserIdentity = $this->auth->getIdentity();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($formEditItem->isValid($formData)) {

                $larrResult = array(
                    'name'          => $formData['name'],
                    'description'   => $formData['description'],
                    'itemMalayName' => $formData['itemMalayName'],
                    'modDate'       => date('Y-m-d H:i:s'),
                    'modBy'         => $getUserIdentity->id
                );

                $this->lobjAppItem->update($larrResult, 'id = ' . $item_id);

                //redirect
                $this->_redirect($this->view->backURL);
            } else {
                $formEditItem->populate($formData);
            }

        } else {
            $formEditItem->populate($larrResult);
        }

        $this->view->title = $this->view->translate("Application Initial Configuration - Edit Item - " . $larrResult['name']);
        $this->view->item = $larrResult;
        $this->view->formEditItem = $formEditItem;
    }

    public function addItemTaggingAction()
    {
        $section_id = $this->_getParam('section_id', null);
        $sectionInfo = $this->lobjAppConfig->fnGetSectionConfJoinWithAppById($section_id);

        $addItemTaggingForm = new Application_Form_ApplicationItemTagging(array('sdtCtgy' => $sectionInfo['sdtCtgy'], 'programID' => $sectionInfo['programID'], 'section_id' => $sectionInfo['sectionID'], 'addoredit' => 'add'));

        $copyItemTaggingForm = new Application_Form_CopyItem(array('sectionID' => $sectionInfo['sectionID']));
        $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'item-list', 'section_id' => $section_id), 'default', true);

        $getUserIdentity = $this->auth->getIdentity();

        $mop = $this->defModel->getData($sectionInfo['mode_of_program']);
        $mod = $this->defModel->getData($sectionInfo['mode_of_study']);
        $pt = $this->defModel->getData($sectionInfo['program_type']);
        $studentCategory = $this->defModel->getData($sectionInfo['sdtCtgy']);
        $sectionInfo['ProgramScheme'] = $pt['DefinitionDesc'] . ' ' . $mop['DefinitionDesc'] . ' ' . $mod['DefinitionDesc'];
        $sectionInfo['StudentCategory'] = $studentCategory['DefinitionDesc'];

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            if (isset($formData['save_item_tagging']) && $formData['save_item_tagging'] == 'Save') {
                if ($addItemTaggingForm->isValid($formData)) {

                    if (isset($formData['view']) && $formData['view'] == '1') {
                        $formData['view'] = '1';
                    } else {
                        $formData['view'] = '0';
                    }

                    if (isset($formData['compulsory']) && $formData['compulsory'] == '1') {
                        $formData['compulsory'] = '1';
                    } else {
                        $formData['compulsory'] = '0';
                    }

                    if (isset($formData['enable']) && $formData['enable'] == '1') {
                        $formData['enable'] = '1';
                    } else {
                        $formData['enable'] = '0';
                    }

                    if (isset($formData['hide']) && $formData['hide'] == '1') {
                        $formData['hide'] = '1';
                    } else {
                        $formData['hide'] = '0';
                    }

                    $countItem = count($this->lobjAppConfig->getItemBasedOnSection($section_id));

                    $data = array(
                        'idTagSection'            => $section_id,
                        'idItem'                  => $formData['idItem'],
                        'item_dclId'              => $formData['item_dclId'],
                        'view'                    => $formData['view'],
                        'compulsory'              => $formData['compulsory'],
                        'enable'                  => $formData['enable'],
                        'hide'                    => $formData['hide'],
                        'item_docDownload'        => $formData['item_docDownload'],
                        'itemInstructionPosition' => $formData['itemInstructionPosition'],
                        'seq_order'               => $countItem + 1,
                        'itemTagInstruction'      => $formData['itemTagInstruction'],
                        'tagDesc'                 => $formData['tagDesc'],
                        'createDate'              => date('Y-m-d H:i:s'),
                        'createBy'                => $getUserIdentity->id
                    );

                    $checkDuplicateItem = $this->lobjAppConfig->checkDuplicateItem($section_id, $formData['idItem']);

                    if (!$checkDuplicateItem) {
                        $this->lobjAppItemTagging->insert($data);
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('error' => "Add Item Failed (Duplicate Item)"));
                    }

                    //redirect
                    $this->_redirect($this->view->backURL);
                } else {
                    $addItemTaggingForm->populate($sectionInfo);
                }
            } else {
                if ($copyItemTaggingForm->isValid($formData)) {
                    $itemCopyList = $this->lobjAppConfig->getCopyItem($formData['field5']);

                    if (count($itemCopyList)) {
                        foreach ($itemCopyList as $itemCopyLoop) {
                            $countItem = count($this->lobjAppConfig->getItemBasedOnSection($section_id));
                            $data = array(
                                'idTagSection'            => $section_id,
                                'idItem'                  => $itemCopyLoop['idItem'],
                                //'item_dclId' => $itemCopyLoop['item_dclId'],
                                'view'                    => $itemCopyLoop['view'],
                                'compulsory'              => $itemCopyLoop['compulsory'],
                                'enable'                  => $itemCopyLoop['enable'],
                                'hide'                    => $itemCopyLoop['hide'],
                                'item_docDownload'        => $itemCopyLoop['item_docDownload'],
                                'itemInstructionPosition' => $itemCopyLoop['itemInstructionPosition'],
                                'seq_order'               => $countItem + 1,
                                'itemTagInstruction'      => $itemCopyLoop['itemTagInstruction'],
                                'tagDesc'                 => $itemCopyLoop['tagDesc'],
                                'createDate'              => date('Y-m-d H:i:s'),
                                'createBy'                => $getUserIdentity->id
                            );

                            $this->lobjAppItemTagging->insert($data);
                        }
                    }

                    //redirect
                    $this->_redirect($this->view->backURL);
                } else {
                    $copyItemTaggingForm->populate($formData);
                }
            }
        } else {
            $addItemTaggingForm->populate($sectionInfo);
        }

        $this->view->title = $this->view->translate("Application Initial Configuration - Add Item Tagging");
        $this->view->addItemTaggingForm = $addItemTaggingForm;
        $this->view->copyItemTaggingForm = $copyItemTaggingForm;
        $this->view->section_id = $section_id;
    }

    public function copyItemTaggingAction()
    {
        $section_id = $this->_getParam('section_id', null);
        $sectionInfo = $this->lobjAppConfig->fnGetSectionConfJoinWithAppById($section_id);
        $copyItemTaggingForm = new Application_Form_CopyItem(array('sectionID' => $sectionInfo['sectionID']));
              $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'item-list', 'section_id' => $section_id), 'default', true);

        $getUserIdentity = $this->auth->getIdentity();

        $mop = $this->defModel->getData($sectionInfo['mode_of_program']);
        $mod = $this->defModel->getData($sectionInfo['mode_of_study']);
        $pt = $this->defModel->getData($sectionInfo['program_type']);
        $studentCategory = $this->defModel->getData($sectionInfo['sdtCtgy']);
        $sectionInfo['ProgramScheme'] = $pt['DefinitionDesc'] . ' ' . $mop['DefinitionDesc'] . ' ' . $mod['DefinitionDesc'];
        $sectionInfo['StudentCategory'] = $studentCategory['DefinitionDesc'];

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($copyItemTaggingForm->isValid($formData)) {
                //var_dump($formData); exit;
                if ($formData['field2'] == $sectionInfo['programID'] && $formData['field4'] == $sectionInfo['sdtCtgy']) {
                    $redirectURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'copy-item-tagging', 'section_id' => $section_id), 'default', true);
                    //redirect
                    $this->_helper->flashMessenger->addMessage(array('error' => "Copy Failed, Cannot copy at same place"));
                    $this->_redirect($redirectURL);
                } else {
                    $itemCopyList = $this->lobjAppConfig->getCopyItem($formData['field5']);

                    if (count($itemCopyList)) {
                        foreach ($itemCopyList as $itemCopyLoop) {
                            $getDclCopy = $this->lobjAppConfig->getDclCopy($itemCopyLoop['item_dclId']);

                            if ($getDclCopy) {
                                $checkDclCopy = $this->lobjAppConfig->checkDclCopy($sectionInfo['programID'], $sectionInfo['sdtCtgy'], $sectionInfo['sectionID'], $getDclCopy['dcl_uplType']);

                                if ($checkDclCopy) {
                                    $itemDclId = $checkDclCopy['dcl_Id'];
                                } else {
                                    $dataDcl = array(
                                        'dcl_stdCtgy'       => $sectionInfo['sdtCtgy'],
                                        'dcl_programScheme' => $sectionInfo['programID'],
                                        'dcl_sectionid'     => $sectionInfo['sectionID'],
                                        'dcl_name'          => $getDclCopy['dcl_name'],
                                        'dcl_malayName'     => $getDclCopy['dcl_malayName'],
                                        'dcl_desc'          => $getDclCopy['dcl_desc'],
                                        'dcl_type'          => $getDclCopy['dcl_type'],
                                        'dcl_activeStatus'  => $getDclCopy['dcl_activeStatus'],
                                        'dcl_priority'      => $getDclCopy['dcl_priority'],
                                        'dcl_uplStatus'     => $getDclCopy['dcl_uplStatus'],
                                        'dcl_uplType'       => $getDclCopy['dcl_uplType'],
                                        'dcl_finance'       => $getDclCopy['dcl_finance'],
                                        'dcl_mandatory'     => $getDclCopy['dcl_mandatory'],
                                        'dcl_addDate'       => date('Y-m-d H:i:s'),
                                        'dcl_addBy'         => $getUserIdentity->id
                                    );
                                    //var_dump($dataDcl);
                                    $itemDclId = $this->lobjAppConfig->copyDcl($dataDcl);
                                }
                            } else {
                                $itemDclId = 0;
                            }

                            $countItem = count($this->lobjAppConfig->getItemBasedOnSection($section_id));
                            $data = array(
                                'idTagSection'            => $section_id,
                                'idItem'                  => $itemCopyLoop['idItem'],
                                'item_dclId'              => $itemDclId,
                                'view'                    => $itemCopyLoop['view'],
                                'compulsory'              => $itemCopyLoop['compulsory'],
                                'enable'                  => $itemCopyLoop['enable'],
                                'hide'                    => $itemCopyLoop['hide'],
                                'item_docDownload'        => $itemCopyLoop['item_docDownload'],
                                'itemInstructionPosition' => $itemCopyLoop['itemInstructionPosition'],
                                'seq_order'               => $countItem + 1,
                                'itemTagInstruction'      => $itemCopyLoop['itemTagInstruction'],
                                'tagDesc'                 => $itemCopyLoop['tagDesc'],
                                'createDate'              => date('Y-m-d H:i:s'),
                                'createBy'                => $getUserIdentity->id
                            );

                            $this->lobjAppItemTagging->insert($data);
                        }
                    }
                }

                //redirect
                $this->_redirect($this->view->backURL);
            }
        }

        $this->view->title = $this->view->translate("Application Initial Configuration - Copy Item Tagging");
        $this->view->sectionInfo = $sectionInfo;
        $this->view->copyItemTaggingForm = $copyItemTaggingForm;
        $this->view->section_id = $section_id;
    }

    public function copyItemPreviewAction()
    {
        $programId = $this->_getParam('programId', null);
        $programSchemeId = $this->_getParam('programSchemeId', null);
        $stdCtgy = $this->_getParam('stdCtgy', null);
        $sectionId = $this->_getParam('sectionId', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $itemCopyList = $this->lobjAppConfig->getCopyItem($sectionId);

        $json = Zend_Json::encode($itemCopyList);

        echo $json;
        exit();
    }

    public function editItemTaggingAction()
    {
        $section_id = $this->_getParam('section_id', null);
        $item_id = $this->_getParam('id', null);
        $getItemInfo = $this->lobjAppItemTagging->fnGetItemTaggingById($item_id);

        $editItemTaggingForm = new Application_Form_ApplicationItemTagging(array('section_id' => $getItemInfo['sectionID']));

        $getUserIdentity = $this->auth->getIdentity();

        $mop = $this->defModel->getData($getItemInfo['mode_of_program']);
        $mod = $this->defModel->getData($getItemInfo['mode_of_study']);
        $pt = $this->defModel->getData($getItemInfo['program_type']);
        $studentCategory = $this->defModel->getData($getItemInfo['sdtCtgy']);
        $getItemInfo['ProgramScheme'] = $pt['DefinitionDesc'] . ' ' . $mop['DefinitionDesc'] . ' ' . $mod['DefinitionDesc'];
        $getItemInfo['StudentCategory'] = $studentCategory['DefinitionDesc'];

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($editItemTaggingForm->isValid($formData)) {

                $data = array(
                    'idTagSection'            => $section_id,
                    //'idItem' => $formData['idItem'],
                    'item_dclId'              => $formData['item_dclId'],
                    'view'                    => $formData['view'],
                    'compulsory'              => $formData['compulsory'],
                    'enable'                  => $formData['enable'],
                    'hide'                    => $formData['hide'],
                    'item_docDownload'        => $formData['item_docDownload'],
                    'itemTagInstruction'      => $formData['itemTagInstruction'],
                    'itemInstructionPosition' => $formData['itemInstructionPosition'],
                    'tagDesc'                 => $formData['tagDesc'],
                    'tagModDate'              => date('Y-m-d H:i:s'),
                    'tagModBy'                => $getUserIdentity->id
                );

                $this->lobjAppItemTagging->update($data, 'id = ' . $item_id);

                //redirect
                $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'item-list', 'section_id' => $section_id), 'default', true));
            } else {
                $editItemTaggingForm->populate($getItemInfo);
            }

        } else {
            $editItemTaggingForm->populate($getItemInfo);
        }

        $this->view->title = $this->view->translate("Application Initial Configuration - Edit Item Tagging - " . $getItemInfo['name']);
        $this->view->item = $getItemInfo;
        $this->view->editItemTaggingForm = $editItemTaggingForm;
        $this->view->section_id = $section_id;
    }

    public function deleteItemTaggingAction()
    {
        $item_id = $this->_getParam('id', null);
        $section_id = $this->_getParam('section_id', null);

        $this->lobjAppItemTagging->delete('id =' . $item_id);

        $sectionInfo = $this->lobjAppConfig->getItemBasedOnSection($section_id);

        foreach ($sectionInfo as $index => $sectionInfo) {
            $data = array(
                'seq_order' => $index + 1,
            );
            $this->lobjAppItemTagging->update($data, "id = " . $sectionInfo['key']);
        }

        //redirect
        $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'item-list', 'section_id' => $section_id), 'default', true));
    }

    public function addStatusAction()
    {
        $id = $this->_getParam('id', null);
        $stdCtgy = $this->_getParam('stdCtgy', null);
        $this->view->message = 0;

        $this->view->title = $this->view->translate("Add Status");
        $addStatusForm = new Application_Form_AddStatus();
        $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'section-list', 'id' => $id), 'default', true);

        $getUserIdentity = $this->auth->getIdentity();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($addStatusForm->isValid($formData)) {
                $checkduplicate = $this->statusModel->checkStatusDuplicate($id, $formData['sts_name']);

                if (!$checkduplicate) {
                    $data = array(
                        'sts_name'      => $formData['sts_name'],
                        'sts_malayName' => $formData['sts_malayName'],
                        'IdConfig'      => $id,
                        'sts_desc'      => $formData['sts_desc'],
                        //'sts_message'=>$formData['sts_message'],
                        //'sts_emailNotification'=>$formData['sts_emailNotification'],
                        'sts_addBy'     => $getUserIdentity->id,
                        'sts_addDate'   => date('Y-m-d H:i:s')
                    );

                    $this->statusModel->insert($data);


                    //redirect
                    $this->_helper->flashMessenger->addMessage(array('success' => "Data successfully added"));
                    $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'section-list', 'id' => $id), 'default', true));
                } else {
                    $this->view->message = 1;
                    $addStatusForm->populate($formData);
                }
            } else {
                $addStatusForm->populate($formData);
            }
        }

        $this->view->addStatusForm = $addStatusForm;
    }

    public function copyStatusAction()
    {
        $id = $this->_getParam('id', null);

        $configInfo = $this->lobjAppConfig->getListConfigs($id);
        $sectionList = $this->lobjAppConfig->getSectionBasedOnId($id);

        $progscheme_id = $configInfo['IdProgramScheme'];
        $stdCtgy = $configInfo['category'];
        $IdProgram = $configInfo['IdProgram'];
        $IdScheme = $configInfo['IdScheme'];

        $copyStatusForm = new Application_Form_CopySection();
        $getUserIdentity = $this->auth->getIdentity();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($copyStatusForm->isValid($formData)) {

                if ($formData['IdProgramScheme'] == $progscheme_id &&
                    $formData['IdProgram'] == $IdProgram &&
                    $formData['IdScheme'] == $IdScheme &&
                    $formData['category'] == $stdCtgy
                ) {

                    $redirectURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'copy-status', 'id' => $id), 'default', true);
                    //redirect
                    $this->_helper->flashMessenger->addMessage(array('error' => "Copy Failed, Cannot copy at same place"));
                    $this->_redirect($redirectURL);
                } else {
                    $statusCopylist = $this->statusModel->getCopyStatus($formData['IdScheme'], $formData['IdProgram'], $formData['IdProgramScheme'], $formData['category']);

                    dd($statusCopylist);


                    if (count($statusCopylist) > 0) {
                        foreach ($statusCopylist as $statusCopyloop) {
                            $checkduplicate = $this->statusModel->checkStatusDuplicate($id, $statusCopyloop['sts_name']);

                            if (!$checkduplicate) {
                                //copy message and email notification
                                $message = $this->statusModel->getCopyStatusTemplate($statusCopyloop['sts_message']);
                                //var_dump($message);
                                if ($message) {
                                    $stsMsgData = array(
                                        'stp_tplId'    => $message['stp_tplId'],
                                        'stp_tplType'  => $message['stp_tplType'],
                                        'stp_title'    => $message['stp_title'],
                                        'stp_language' => $message['stp_language'],
                                        'contentMy'    => $message['contentMy'],
                                        'contentEng'   => $message['contentEng'],
                                        'stp_addBy'    => $message['stp_addBy'],
                                        'stp_addDate'  => $message['stp_addDate'],
                                        'stp_modBy'    => $message['stp_modBy'],
                                        'stp_modDate'  => $message['stp_modDate']
                                    );
                                    $msgId = $this->statusModel->insertCopyStatusTemplate($stsMsgData);

                                    $attachmentMsgList = $this->statusModel->getCopyStatusAttachment($message['stp_Id']);

                                    if ($attachmentMsgList) {
                                        foreach ($attachmentMsgList as $attachmentMsgLoop) {
                                            $attachmentMsgData = array(
                                                'sth_stpId'   => $msgId,
                                                'sth_tplId'   => $attachmentMsgLoop['sth_tplId'],
                                                'sth_type'    => $attachmentMsgLoop['sth_type'],
                                                'sth_addBy'   => $attachmentMsgLoop['sth_addBy'],
                                                'sth_addDate' => $attachmentMsgLoop['sth_addDate']
                                            );
                                            $this->statusModel->insertCopyStatusAttachment($attachmentMsgData);
                                        }
                                    }
                                } else {
                                    $msgId = 0;
                                }

                                $email = $this->statusModel->getCopyStatusTemplate($statusCopyloop['sts_emailNotification']);
                                //var_dump($email);
                                if ($email) {
                                    $stsEmailData = array(
                                        'stp_tplId'    => $email['stp_tplId'],
                                        'stp_tplType'  => $email['stp_tplType'],
                                        'stp_title'    => $email['stp_title'],
                                        'stp_language' => $email['stp_language'],
                                        'contentMy'    => $email['contentMy'],
                                        'contentEng'   => $email['contentEng'],
                                        'stp_addBy'    => $email['stp_addBy'],
                                        'stp_addDate'  => $email['stp_addDate'],
                                        'stp_modBy'    => $email['stp_modBy'],
                                        'stp_modDate'  => $email['stp_modDate']
                                    );
                                    $emailId = $this->statusModel->insertCopyStatusTemplate($stsEmailData);

                                    $attachmentList = $this->statusModel->getCopyStatusAttachment($email['stp_Id']);

                                    if ($attachmentList) {
                                        foreach ($attachmentList as $attachmentLoop) {
                                            $attachmentData = array(
                                                'sth_stpId'   => $emailId,
                                                'sth_tplId'   => $attachmentLoop['sth_tplId'],
                                                'sth_type'    => $attachmentLoop['sth_type'],
                                                'sth_addBy'   => $attachmentLoop['sth_addBy'],
                                                'sth_addDate' => $attachmentLoop['sth_addDate']
                                            );
                                            $this->statusModel->insertCopyStatusAttachment($attachmentData);
                                        }
                                    }
                                } else {
                                    $emailId = 0;
                                }

                                //copy status
                                $data = array(
                                    'sts_name'              => $statusCopyloop['sts_name'],
                                    'sts_malayName'         => $statusCopyloop['sts_malayName'],
                                    'sts_programScheme'     => $progscheme_id,
                                    'sts_stdCtgy'           => $stdCtgy,
                                    'sts_desc'              => $statusCopyloop['sts_desc'],
                                    'sts_message'           => $msgId,
                                    'sts_emailNotification' => $emailId,
                                    'IdConfig'              => $id,
                                    'sts_addBy'             => $getUserIdentity->id,
                                    'sts_addDate'           => date('Y-m-d H:i:s')
                                );
                                $this->statusModel->insert($data);
                            }
                        }//exit;
                    }
                }
                $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'section-list', 'id' => $id), 'default', true));
            }
        }

        $this->view->copyStatusForm = $copyStatusForm;
        $this->view->config = $configInfo;
        $this->view->title = $this->view->translate("Copy Status");
        $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'section-list', 'id' => $id), 'default', true);
    }

    public function copyStatusPreviewAction()
    {
        $schemeId = $this->_getParam('schemeId', 0);
        $programId = $this->_getParam('programId', 0);
        $programSchemeId = $this->_getParam('programSchemeId', 0);
        $stdCtgy = $this->_getParam('stdCtgy', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $statusCopylist = $this->statusModel->getCopyStatus($schemeId, $programId, $programSchemeId, $stdCtgy);

        $json = Zend_Json::encode($statusCopylist);

        echo $json;
        exit();
    }

    public function editStatusAction()
    {
        $stsId = $this->_getParam('sts_id', null);
        $id = $this->_getParam('id', null);
        $this->view->title = $this->view->translate("Edit Status");
        $statusList = $this->statusModel->fnGetStatusListById($stsId);
        $editStatusForm = new Application_Form_AddStatus(array('IdProgram' => $statusList['IdProgram']));
        $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'section-list', 'id' => $id), 'default', true) . '#tab-2';

        $getUserIdentity = $this->auth->getIdentity();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($editStatusForm->isValid($formData)) {
                $data = array(
                    'sts_malayName' => $formData['sts_malayName'],
                    'sts_desc'      => $formData['sts_desc'],
                    'sts_modBy'     => $getUserIdentity->id,
                    'sts_modDate'   => date('Y-m-d H:i:s')
                );

                $this->statusModel->update($data, 'sts_Id = ' . $stsId);

                //redirect
                $this->_helper->flashMessenger->addMessage(array('success' => "Data successfully updated"));
                $this->_redirect($this->view->backURL);
            } else {
                $editStatusForm->populate($statusList);
            }
        } else {
            $editStatusForm->populate($statusList);
        }

        $this->view->editStatusForm = $editStatusForm;
        $this->view->stsId = $stsId;
        $this->view->statusList = $statusList;

        $statusName = $this->defModel->getData($statusList['sts_name']);
        $this->view->statusName = $statusName['DefinitionDesc'];
    }

    public function deleteStatusAction()
    {
        $stsId = $this->_getParam('sts_id', null);
        $id = $this->_getParam('id', null);

        $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'section-list', 'id' => $id), 'default', true);

        if ($stsId != null) {
            $this->statusModel->delete('sts_Id = ' . $stsId);
        }

        //redirect
        $this->_helper->flashMessenger->addMessage(array('success' => "Data deleted"));
        $this->_redirect($this->view->backURL);
    }

    /*
     * sequence setup
     */
    public function moveSectionAction()
    {

        $direction = $this->_getParam('direction', null);
        $section_id = $this->_getParam('section_id', null);
        $item_id = $this->_getParam('item_id', null);

        if ($direction == 'up') {

            //check seq for current id
            $sectionInfo = $this->lobjAppConfig->fnGetSectionConfById($section_id);

            if ($sectionInfo['seq_order'] == 1) {
                return false;
            } else {
                $seq_order = $sectionInfo['seq_order'] - 1;

                //update above selected seq to +1 from current seq
                $this->lobjAppConfig->update(array('seq_order' => $seq_order + 1), "seq_order='$seq_order' and programID=" . $sectionInfo['programID']);

                //update sequence for selected order minus -1 from current seq
                $this->lobjAppConfig->update(array('seq_order' => $sectionInfo['seq_order'] - 1), "id='$section_id'");
            }
        }

        if ($direction == 'down') {

            //check seq for current id
            $sectionInfo = $this->lobjAppConfig->fnGetSectionConfById($section_id);

            //get total section under program
            $total = $this->lobjAppConfig->getSectionBasedOnProgramScheme($sectionInfo['programID']);

            if ($sectionInfo['seq_order'] == count($total)) {
                return false;
            } else {
                $seq_order = $sectionInfo['seq_order'] + 1;

                //update below selected seq to -1 from current seq
                $this->lobjAppConfig->update(array('seq_order' => $seq_order - 1), "seq_order='$seq_order' and programID=" . $sectionInfo['programID']);

                //update sequence for selected order add +1 from current seq
                $this->lobjAppConfig->update(array('seq_order' => $sectionInfo['seq_order'] + 1), "id='$section_id'");
            }
        }

        if ($direction == 'item_up') {

            //check seq for current id
            $itemInfo = $this->lobjAppItemTagging->fnGetItemTaggingByIdForMove($item_id);

            if ($itemInfo['seq_order'] == 1) {
                return false;
            } else {
                $seq_order = $itemInfo['seq_order'] - 1;

                //update above selected seq to +1 from current seq
                $this->lobjAppItemTagging->update(array('seq_order' => $seq_order + 1), "seq_order='$seq_order' and idTagSection=" . $itemInfo['idTagSection']);

                //update sequence for selected order minus -1 from current seq
                $this->lobjAppItemTagging->update(array('seq_order' => $itemInfo['seq_order'] - 1), "id='$item_id'");
            }
        }

        if ($direction == 'item_down') {

            //check seq for current id
            $itemInfo = $this->lobjAppItemTagging->fnGetItemTaggingByIdForMove($item_id);

            //get total item under section
            $total = $this->lobjAppConfig->getItemBasedOnSection($itemInfo['idTagSection']);

            if ($itemInfo['seq_order'] == count($total)) {
                return false;
            } else {
                $seq_order = $itemInfo['seq_order'] + 1;

                //update below selected seq to -1 from current seq
                $this->lobjAppItemTagging->update(array('seq_order' => $seq_order - 1), "seq_order='$seq_order' and idTagSection=" . $itemInfo['idTagSection']);

                //update sequence for selected order add +1 from current seq
                $this->lobjAppItemTagging->update(array('seq_order' => $itemInfo['seq_order'] + 1), "id='$item_id'");
            }
        }
        exit();
    }

    public function sectionAjaxAction()
    {

        $section_id = $this->_getParam('section_id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $sectionInfo = $this->lobjAppConfig->fnGetSectionConfById($section_id);

        $sectionList = $this->lobjAppConfig->getSectionBasedOnProgram($sectionInfo['IdProgram']);

        $i = 0;

        foreach ($sectionList as $sectionloop) {

            $sectionList[$i]['createName'] = $sectionloop['createBy'] != 0 ? Application_Model_DbTable_ApplicationInitialConfig::getUserName($sectionloop['createBy']) : 'n/a';
            $sectionList[$i]['modName'] = $sectionloop['initModBy'] != 0 ? Application_Model_DbTable_ApplicationInitialConfig::getUserName($sectionloop['initModBy']) : 'n/a';

            $i++;
        }

        $json = Zend_Json::encode($sectionList);

        echo $json;
        exit();
    }

    /*
     * 
     * ajax section
     * 
     */

    public function sectionTaggingAjaxAction()
    {

        $program_id = $this->_getParam('id', null);
        $stdCtgy = $this->_getParam('stdCtgy', null);
        $sectionID = $this->_getParam('sectionID', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $sectionList = $this->lobjAppConfig->getSectionAjax($program_id, $stdCtgy, $sectionID);

        $json = Zend_Json::encode($sectionList);

        echo $json;
        exit();
    }

    public function itemTaggingAjaxAction()
    {

        $section_id = $this->_getParam('id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $sectionList = $this->lobjAppConfig->getItemBasedOnSection($section_id);

        $i = 0;

        foreach ($sectionList as $sectionloop) {

            $sectionList[$i]['createName'] = $sectionloop['createBy'] != 0 ? Application_Model_DbTable_ApplicationInitialConfig::getUserName($sectionloop['createBy']) : 'n/a';
            $sectionList[$i]['modName'] = $sectionloop['tagModBy'] != 0 ? Application_Model_DbTable_ApplicationInitialConfig::getUserName($sectionloop['tagModBy']) : 'n/a';

            $i++;
        }

        $json = Zend_Json::encode($sectionList);

        echo $json;
        exit();
    }

    public function getUsernameAjaxAction()
    {

        $user_id = $this->_getParam('user_id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $userName = Application_Model_DbTable_ApplicationInitialConfig::getUserName($user_id);

        $json = Zend_Json::encode($userName);

        echo $json;
        exit();
    }

    public function loadInstructionAjaxAction()
    {

        $section_id = $this->_getParam('id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $getInstruction = $this->lobjAppInitConfig->fnGetSectionById($section_id);

        //var_dump($getInstruction); exit;

        $json = Zend_Json::encode($getInstruction);

        echo $json;
        exit();
    }

    public function moveAjaxAction()
    {
        $item = $this->getRequest()->getPost();

        foreach ($item['item'] as $key => $value) {
            $data = array(
                'seq_order' => $key + 1
            );

            $this->lobjAppConfig->update($data, 'id = ' . $value);
        }
        exit();
    }

    public function moveAjaxItemAction()
    {
        $item = $this->getRequest()->getPost();

        foreach ($item['item'] as $key => $value) {
            $data = array(
                'seq_order' => $key + 1
            );

            $this->lobjAppItemTagging->update($data, 'id = ' . $value);
        }
        exit();
    }

    public function moveAjaxStatusAction()
    {
        $item = $this->getRequest()->getPost();

        foreach ($item['item'] as $key => $value) {
            $data = array(
                'sts_sequence' => $key + 1
            );

            $this->statusModel->update($data, 'sts_Id = ' . $value);
        }
        exit();
    }

    public function tooltipSetupAction()
    {
        $program_id = $this->_getParam('id', null);
        $programInfo = $this->lobjAppInitConfig->fnGetProgramById($program_id);
        $this->view->title = $this->view->translate("Tooltip Setup - " . $programInfo['ProgramName']);
        $form = new Application_Form_TooltipSetupForm(array('program_id' => $program_id));
        $getUserIdentity = $this->auth->getIdentity();

        $this->view->form = $form;
        $this->view->program_id = $program_id;
    }

    public function getValueAjaxAction()
    {
        $item_id = $this->_getParam('item_id', null);
        $program_id = $this->_getParam('program_id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $value = array();

        switch ($item_id) {
            case '1':
                break;

            case '2':
                $listValue = $this->lobjAppInitConfig->getProgramMode($program_id, 'mode_of_program');

                if (count($listValue) > 0) {
                    $i = 0;
                    foreach ($listValue as $listValueLoop) {
                        $value[$i]['value'] = $listValueLoop['mode_of_program'];
                        $value[$i]['name'] = $listValueLoop['DefinitionDesc'];
                        $i++;
                    }
                }
                break;

            case '3':
                $listValue = $this->lobjAppInitConfig->getProgramMode($program_id, 'mode_of_study');

                if (count($listValue) > 0) {
                    $i = 0;
                    foreach ($listValue as $listValueLoop) {
                        $value[$i]['value'] = $listValueLoop['mode_of_study'];
                        $value[$i]['name'] = $listValueLoop['DefinitionDesc'];
                        $i++;
                    }
                }
                break;

            case '4':
                $listValue = $this->lobjAppInitConfig->getIntake($program_id);

                if (count($listValue) > 0) {
                    $i = 0;
                    foreach ($listValue as $listValueLoop) {
                        $value[$i]['value'] = $listValueLoop['ip_id'];
                        $value[$i]['name'] = $listValueLoop['IntakeDesc'] . ' :- Start date: ' . $listValueLoop['ApplicationStartDate'] . ', End date: ' . $listValueLoop['ApplicationEndDate'];
                        $i++;
                    }
                }
                break;

            case '15':
                $listValue = $this->lobjAppInitConfig->getProgramMode($program_id, 'program_type');

                if (count($listValue) > 0) {
                    $i = 0;
                    foreach ($listValue as $listValueLoop) {
                        $value[$i]['value'] = $listValueLoop['program_type'];
                        $value[$i]['name'] = $listValueLoop['DefinitionDesc'];
                        $i++;
                    }
                }
                break;

            default:
                break;
        }

        //$getTooltipInformation = $this->lobjAppInitConfig->fnGetTooltipInfo($item_id, $program_id);

        $json = Zend_Json::encode($value);

        echo $json;
        exit();
    }

    public function loadTooltipAjaxAction()
    {
        $item_id = $this->_getParam('item_id', null);
        $program_id = $this->_getParam('program_id', null);
        $value_id = $this->_getParam('value_id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $getTooltipInformation = $this->lobjAppInitConfig->fnGetTooltipInfo($item_id, $program_id, $value_id);

        $json = Zend_Json::encode($getTooltipInformation);

        echo $json;
        exit();
    }

    public function saveTooltipAjaxAction()
    {
        $item = $this->getRequest()->getPost();
        $getUserIdentity = $this->auth->getIdentity();

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        if ($item) {
            $checkTooltip = $this->lobjAppInitConfig->fnGetTooltipInfo($item['item'], $item['program_id'], $item['value']);

            if ($checkTooltip == false) {
                $data = array(
                    'tts_program_id' => $item['program_id'],
                    'tts_item_id'    => $item['item'],
                    'tts_value_id'   => $item['value'],
                    'tts_desc'       => $item['tooltiptextarea'],
                    'tts_updUser'    => $getUserIdentity->id,
                    'tts_updDate'    => date('Y-m-d H:i:s')
                );
                $this->lobjAppInitConfig->insertTooltip($data);
            } else {
                $data = array(
                    'tts_desc'    => $item['tooltiptextarea'],
                    'tts_updUser' => $getUserIdentity->id,
                    'tts_updDate' => date('Y-m-d H:i:s')
                );
                $this->lobjAppInitConfig->updateTooltip($data, 'tts_program_id = ' . $item['program_id'] . ' and tts_item_id = ' . $item['item'] . ' and tts_value_id = ' . $item['value']);
            }
        }

        $tooltipInfo = $this->lobjAppInitConfig->fnGetTooltipInfo($item['item'], $item['program_id'], $item['value']);

        $json = Zend_Json::encode($tooltipInfo);

        echo $json;
        exit();
    }

    public function copyTooltipAjaxAction()
    {
        $item = $this->getRequest()->getPost();
        $getUserIdentity = $this->auth->getIdentity();

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        if ($item) {
            $getCopyTooltip = $this->lobjAppInitConfig->getCopyTooltip($item['program']);
            if (count($getCopyTooltip) > 0) {
                foreach ($getCopyTooltip as $getCopyTooltipLoop) {
                    $checkTooltip = $this->lobjAppInitConfig->fnGetTooltipInfo($getCopyTooltipLoop['tts_item_id'], $item['program2']);

                    if ($checkTooltip == false) {
                        $data = array(
                            'tts_program_id' => $item['program2'],
                            'tts_item_id'    => $getCopyTooltipLoop['tts_item_id'],
                            'tts_desc'       => $getCopyTooltipLoop['tts_desc'],
                            'tts_updUser'    => $getUserIdentity->id,
                            'tts_updDate'    => date('Y-m-d H:i:s')
                        );
                        $this->lobjAppInitConfig->insertTooltip($data);
                    } else {
                        $data = array(
                            'tts_desc'    => $getCopyTooltipLoop['tts_desc'],
                            'tts_updUser' => $getUserIdentity->id,
                            'tts_updDate' => date('Y-m-d H:i:s')
                        );
                        $this->lobjAppInitConfig->updateTooltip($data, 'tts_program_id = ' . $item['program2'] . ' and tts_item_id = ' . $getCopyTooltipLoop['tts_item_id']);
                    }
                }
            }
        }

        exit();
    }

    public function previewOnlineAppAction()
    {

        $this->view->ajax = 0;
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->view->ajax = 1;
        }


        $scheme_id = $this->_getParam('id', 0);
        $stud_category = $this->_getParam('cat_id', 0);
        $program_id = $this->_getParam('program_id', 0);

        $sectionDB = new Application_Model_DbTable_ApplicationSection();
        $appInitConfigDB = new Application_Model_DbTable_ApplicationInitialConfig();
        $appForm = new Application_Form_Manual();

        $section_list = $sectionDB->getSection($program_id, $scheme_id, $stud_category);

        foreach ($section_list as $section) {
            $listItem = $appInitConfigDB->getItem($scheme_id, $section['id']);
            $sectionItems[$section['sectionID']] = $listItem;

            //--
            //stuff


            foreach ($sectionItems[$section['sectionID']] as $_item) {

                if ($_item['form_type'] != '') {
                    $class = $label_class = '';

                    // Processing Types
                    switch ($_item['form_type']) {
                        case 'calendar':
                        case 'calendar_backdated':
                        case 'month':

                            $_item['form_type'] = 'text';
                            $class = 'input-txt';

                            break;

                        case 'text':

                            $class = 'input-txt';

                            break;

                        case 'select':

                            $class = 'select';

                            break;

                        case 'textarea':

                            $class = 'textarea';

                            break;

                        case 'checkbox':
                            $label_class = 'label_checkbox';

                            //checkbox => multicheckbox
                            if ($_item['variable'] == 'health_condition') {
                                $_item['form_type'] = 'multiCheckbox';
                            }

                            break;

                        case 'radio':

                            $class = 'radio';
                            $label_class = 'label_radio';

                            break;

                        default;

                            $label_class = '';

                    }


                    //echo $_item['form_type'] . '<br />';

                    if ($_item['form_type'] == 'file') {
                        $appForm->addElement($_item['form_type'], $_item['variable'], array(
                            'decorators' => array(
                                'File',
                                'Errors')
                        ));


                    } else if ($_item['form_type'] == 'download_file') {
                        //do something with the file
                    } else if ($_item['form_type'] == 'select_others') {
                        //do something with the file
                    } else if ($_item['form_type'] == 'header_title_left') {
                        //do something with the file
                    } else if ($_item['form_type'] == 'header_title_center') {
                        //do something with the file
                    } else {

                        $appForm->addElement($_item['form_type'], $_item['variable'], array(
                            'class'       => $class,
                            'label_class' => $label_class,
                            'decorators'  => array(
                                'ViewHelper',
                                'Description',
                                'Errors'),
                        ));

                        $appForm->$_item['variable']->removeDecorator('Errors');
                        $appForm->$_item['variable']->removeDecorator('HtmlTag');
                        $appForm->$_item['variable']->removeDecorator('Label');

                        /* misc */
                        if ($_item['form_type'] == 'radio') {
                            $appForm->$_item['variable']->setSeparator(' ');
                        }
                    }
                }
            }
        }


        //view
        $this->view->appForm = $appForm;
        $this->view->section_list = $section_list;
        $this->view->sectionItems = $sectionItems;


        /*
         $trxId = $this->lobjAppInitConfig->getTransForPreview($programscheme_id, $stdCtgy);

         $this->_redirect($this->view->url(array('module'=>'application','controller'=>'index', 'action'=>'edit', 'id' =>$trxId, 'from'=>1, 'scheme'=>$programscheme_id, 'stdCtgy'=>$stdCtgy),'default',true));
         exit();*/
    }

    public function programItemSetupAction()
    {
        $programId = $this->_getParam('id', 0);
        $itemId = $this->_getParam('item_id', 0);
        $this->view->title = $this->view->translate("Edit Item");
        $form = new Application_Form_ProgramItemSetup();
        $this->view->programId = $programId;
        $this->view->form = $form;
        $programInfo = $this->lobjAppInitConfig->fnGetProgramById($programId);
        $itemInfo = $this->lobjAppItem->fnGetItemById($itemId);
        $this->view->programInfo = $programInfo;
        $this->view->itemInfo = $itemInfo;
        $getUserIdentity = $this->auth->getIdentity();

        $progSecInfo = $this->lobjAppInitConfig->getProgramSectionDetailBasedOnProgram($programId, $itemId);
        if ($progSecInfo) {
            $form->populate($progSecInfo);
        }

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            ///var_dump($formData); exit;
            if ($form->isValid($formData)) {

                if (isset($formData['pds_view']) && $formData['pds_view'] == '1') {
                    $formData['pds_view'] = '1';
                } else {
                    $formData['pds_view'] = '0';
                }

                if (isset($formData['pds_compulsory']) && $formData['pds_compulsory'] == '1') {
                    $formData['pds_compulsory'] = '1';
                } else {
                    $formData['pds_compulsory'] = '0';
                }

                if (isset($formData['pds_enable']) && $formData['pds_enable'] == '1') {
                    $formData['pds_enable'] = '1';
                } else {
                    $formData['pds_enable'] = '0';
                }

                if (isset($formData['pds_hide']) && $formData['pds_hide'] == '1') {
                    $formData['pds_hide'] = '1';
                } else {
                    $formData['pds_hide'] = '0';
                }

                if ($progSecInfo) { //update
                    $data = array(
                        //'pds_instruction_position'=>$formData['pds_instruction_position'],
                        //'pds_instruction'=>$formData['pds_instruction'],
                        //'pds_tooltip'=>$formData['pds_tooltip'],
                        'pds_view'       => $formData['pds_view'],
                        'pds_compulsory' => $formData['pds_compulsory'],
                        'pds_enable'     => $formData['pds_enable'],
                        //'pds_hide'=>$formData['pds_hide'],
                        'pds_updUser'    => $getUserIdentity->id,
                        'pds_updDate'    => date('Y-m-d H:i:s')
                    );

                    $this->lobjAppInitConfig->updateProgramSectionDetail($data, $progSecInfo['pds_id']);
                } else { //insert
                    $data = array(
                        'pds_program_id' => $programId,
                        'pds_item_id'    => $itemId,
                        //'pds_instruction_position'=>$formData['pds_instruction_position'],
                        //'pds_instruction'=>$formData['pds_instruction'],
                        //'pds_tooltip'=>$formData['pds_tooltip'],
                        'pds_view'       => $formData['pds_view'],
                        'pds_compulsory' => $formData['pds_compulsory'],
                        'pds_enable'     => $formData['pds_enable'],
                        //'pds_hide'=>$formData['pds_hide'],
                        'pds_updUser'    => $getUserIdentity->id,
                        'pds_updDate'    => date('Y-m-d H:i:s')
                    );
                    $this->lobjAppInitConfig->insertProgramSectionDetail($data);
                }

                $this->_helper->flashMessenger->addMessage(array('success' => "Information Update"));
                $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'program-item-setup', 'id' => $programId, 'item_id' => $itemId), 'default', true));
            }
        }
    }
}

?>