<?php

/**
 * PlacementtestmarksController
 *
 * @author
 * @version
 */


class Application_PlacementtestmarksbulkController extends Base_Base {
	private $_gobjlog;
	/**
	 * The default action - show the home page
	 */

	public function init()
	{
		$this->fnsetObj();
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object

	}
	public function fnsetObj(){
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
		$this->lobjplacementtestmodel = new Application_Model_DbTable_Placementtest();
		$this->lobjplacementtestmarksbulkmodel = new Application_Model_DbTable_Placementtestmarksbulk();
		$this->lobjplacementtestmarksmodel = new Application_Model_DbTable_Placementtestmarks();
	}

	public function indexAction()
	{
		$this->view->lobjform = $this->lobjform;
		$this->view->IdPrgm = 0;
		// TODO Auto-generated PlacementtestmarksController::indexAction() default action
		$larrresult = $this->view->PlacementTestComponents = array();//$this->lobjstudentapplication->fetchAll('Active = 1', "FName ASC");
		if(!$this->_getParam('search')) {
			unset($this->gobjsessionsis->placementmarkspaginatorresult);
			unset($this->view->PlacementTestComponents) ;
		}
			
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		$larProgramNameCombo = $this->lobjplacementtestmarksmodel->fngetProgramNameCombo();
		$this->view->lobjform->field5->addMultiOptions($larProgramNameCombo);
		$this->view->lobjform->field5->setAttrib('onChange','fnGetPlacementtest(this.value)');

		$larBranchNameCombo = $this->lobjplacementtestmarksmodel->fngetBranchCombo();
		$this->view->lobjform->field8->addMultiOptions($larBranchNameCombo);

		if(isset($this->gobjsessionsis->placementmarkspaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->placementmarkspaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			unset($this->gobjsessionsis->placementmarkspaginatorresult);
			unset($this->view->PlacementTestComponents) ;
			$this->view->PlacementTestComponents = $this->view->paginator = array();
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				//$larrresult = $this->lobjplacementtestmarksbulkmodel->fnSearchEducationlist( $this->lobjform->getValues ()); // hided on feedback
				$larrresult = $this->lobjplacementtestmarksbulkmodel->fnSearchEducationlistBulk( $this->lobjform->getValues ());
				//echo "<pre>";
				$this->view->IdPlacementTest = $larrformData['field1'];
				$this->view->IdPrgm =  $larrformData['field5'];
				//print_r($larrresult);exit;

				if(count($larrresult) > 0 ) {
					$ArraIdApplications['IdApplication'][0] =  0;
					$ArraIdApplication[0]['IdApplication'] =  0;
					$ArraIdApplication[0]['FName'] =  0;
					$ArraIdApplication[0]['PermAddressDetails'] =  0;
					$ArraIdApplication[0]['ICNumber'] =  0;
					$ArraIdApplication[0]['ProgramName'][0] =  0;
					$ArraIdApplication[0]['PlacementTestName'] =  0;
					$ArraIdApplication[0]['PlacementTestDate'] =  0;
					$ArraIdApplication[0]['PlacementTestTime'] =  0;
					$ArraIdApplication[0]['PermCity'] =  0;
					$ArraIdApplication[0]['CityName'] =  0;
					$cnts = 0;
					for($i=0;$i<count($larrresult);$i++){
						if( $larrresult[$i]['Marks'])$ArraIdApplications['marks'][$larrresult[$i]['IdApplication']][$larrresult[$i]['IdPlacementTestComponent']] = $larrresult[$i]['Marks'];
						else $ArraIdApplications['marks'][$larrresult[$i]['IdApplication']][$larrresult[$i]['IdPlacementTestComponent']] = 0;
						if(!in_array($larrresult[$i]['IdApplication'],$ArraIdApplications['IdApplication'])){
							$ArraIdApplications['IdApplication'][$cnts] = $larrresult[$i]['IdApplication'];
							$ArraIdApplication[$cnts]['IdApplication'] = $larrresult[$i]['IdApplication'];
							$ArraIdApplication[$cnts]['FName'] = $larrresult[$i]['FName'];
							$ArraIdApplication[$cnts]['PermAddressDetails'] = $larrresult[$i]['PermAddressDetails'];
							$ArraIdApplication[$cnts]['ICNumber'] = $larrresult[$i]['ExtraIdField1'];
							$ArraIdApplication[$cnts]['ProgramName'] = $larrresult[$i]['ProgramName'];
							$ArraIdApplication[$cnts]['PlacementTestName'] = $larrresult[$i]['PlacementTestName'];
							$ArraIdApplication[$cnts]['PlacementTestDate'] = $larrresult[$i]['PlacementTestDate'];
							$ArraIdApplication[$cnts]['PlacementTestTime'] = $larrresult[$i]['PlacementTestTime'];
							$ArraIdApplication[$cnts]['PermCity'] = $larrresult[$i]['PermCity'];
							$ArraIdApplication[$cnts]['CityName'] = $larrresult[$i]['CityName'];
							$cnts++;
						}
					}
					//echo "<pre>";print_r($ArraIdApplications['marks']);
					$ArraIdPlacementTestComponents['IdPlacementTestComponent'][0] =  0;
					$ArraIdPlacementTestComponent[0]['IdPlacementTestComponent'] =  0;
					$ArraIdPlacementTestComponent[0]['ComponentName'] =  0;
					$cnts = 0;
					for($i=0;$i<count($larrresult);$i++){
						if(!in_array($larrresult[$i]['IdPlacementTestComponent'],$ArraIdPlacementTestComponents['IdPlacementTestComponent'])){
							$ArraIdPlacementTestComponents['IdPlacementTestComponent'][$cnts] = $larrresult[$i]['IdPlacementTestComponent'];
							$ArraIdPlacementTestComponent[$cnts]['IdPlacementTestComponent'] = $larrresult[$i]['IdPlacementTestComponent'];
							$ArraIdPlacementTestComponent[$cnts]['ComponentName'] = $larrresult[$i]['ComponentName'];
							$ArraIdPlacementTestComponent[$cnts]['ComponentWeightage'] = $larrresult[$i]['ComponentWeightage'];
							$ArraIdPlacementTestComponent[$cnts]['ComponentTotalMarks'] = $larrresult[$i]['ComponentTotalMarks'];
							$ArraIdPlacementTestComponent[$cnts]['MinimumMark'] = $larrresult[$i]['MinimumMark'];
							//$ArraIdPlacementTestComponent[$cnts]['ComponentWeightage'] = $larrresult[$i]['ComponentWeightage'];
							$cnts++;
						}
					}
					//print_r($ArraIdPlacementTestComponent);exit;
					//$larrComponents = $this->lobjplacementtestmarksbulkmodel->fngetComponents();
					//$this->view->components = $larrComponents;
					$lintpagecount= 1000;
					$this->view->PlacementTestComponents = $ArraIdPlacementTestComponent;
					$this->view->marks = $ArraIdApplications;
					$this->view->paginator = $lobjPaginator->fnPagination($ArraIdApplication,$lintpage,$lintpagecount);
					$this->gobjsessionsis->placementmarkspaginatorresult = $larrresult;
				}
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			echo "<pre>";

			for($ss=0;$ss<count($larrformData['StudentsIds']);$ss++){
				$StudentsIds  = $larrformData['StudentsIds'][$ss];
				$larrformdata['IdApplication'] = $StudentsIds;
				$larrformdata['IdPlacementTest'] = $larrformData['IdPlacementTest'];
				$this->lobjplacementtestmarksmodel->fndeleteOldMarks($larrformdata);
				for($mm=0;$mm<count($larrformData['IdPlacementTestComponent']);$mm++){
					$IdPlacementTestComponent  = $larrformData['IdPlacementTestComponent'][$mm];
					$Marks = $larrformData['Marks'][$StudentsIds][$IdPlacementTestComponent];

					$larrformdata['IdPlacementTestComponent'] = $IdPlacementTestComponent;
					$larrformdata['Marks']                    = $Marks;
					$larrformdata['UpdDate']                  = date('Y-m-d');
					$larrformdata['UpdUser']                  = 1;
					$larrformdata['IdComponent'][0]           = $IdPlacementTestComponent;
					$larrformdata['IdComponentMarks'][0]      = $Marks;
					$this->lobjplacementtestmarksmodel->fninsertNewMarks($larrformdata);


				}
			}
			$auth = Zend_Auth::getInstance();
			// Write Logs
			$priority=Zend_Log::INFO;
			$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
					'level' => $priority,
					'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					'time' => date ( 'Y-m-d H:i:s' ),
					'message' => 'Add Placement Test Marks Entry Bulk',
					'Description' =>  Zend_Log::DEBUG,
					'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
			$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
			$this->_redirect( $this->baseUrl . '/application/placementtestmarksbulk/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'application' ,'controller'=>'placementtestmarks', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/application/placementtestmarksbulk/index');
		}

	}
	public function getplacementtestlistAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idProgram = $this->_getParam('idProgram');
		$larrcomponentsset = $this->lobjplacementtestmarksbulkmodel->fnViewPlacementTestCombo($idProgram);
		$larrProgramlist= $this->lobjCommon->fnResetArrayFromValuesToNames($larrcomponentsset);
		echo Zend_Json_Encoder::encode($larrProgramlist);
	}
	/*
	 public function placementtestmarkslistAction()
	 {
	if ($this->_request->isPost ())
	{
	$larrformdata = $this->_request->getPost ();
	$this->lobjplacementtestmarksmodel->fndeleteOldMarks($larrformdata);
	$this->lobjplacementtestmarksmodel->fninsertNewMarks($larrformdata);
	//$this->_redirect($this->view->url(array('module'=>'application' ,'controller'=>'placementtestmarks', 'action'=>'index'),'default',true));
	$this->_redirect( $this->baseUrl . '/application/placementtestmarks/index');
	}
	else
	{
	$this->view->UpdDate = date ( 'Y-m-d H:i:s' );
	$auth = Zend_Auth::getInstance();
	$this->view->UpdUser =  $auth->getIdentity()->iduser;
	$lidapplication = $this->_getParam('idapplication');
	$larrresult = $this->lobjplacementtestmarksmodel->fngetPlacementtestdetails($lidapplication);
	$larrcomponentsset = $this->lobjplacementtestmodel->fnViewPlacementComponentDetails($larrresult['IdPlacementTest']);

	$this->view->larrcomponentsset = $larrcomponentsset;
	$this->view->larrstudptest = $larrresult;
	$larrcomponentmarksset = $this->lobjplacementtestmarksmodel->fngetAllMarksDetails($lidapplication);

	foreach ($larrcomponentmarksset as $larrcomponentmarks)
		$larrmarks[$larrcomponentmarks['IdApplication']][$larrcomponentmarks['IdPlacementTestComponent']] = $larrcomponentmarks['Marks'];

	$this->view->larrcomponentmarks = @$larrmarks;
	}
	}
	*/
}
