<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_ApplicantEligibleController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $dclModel;
    private $defModel;
    private $lobjAppItem;
    private $lobjAppConfig;
    private $stpModel;
    private $tplModel;
    private $sthModel;
    private $checklistVerification;
    private $appAprove;
    private $appEligible;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->dclModel = new Application_Model_DbTable_DocumentChecklist();
        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->lobjAppItem = new Application_Model_DbTable_ApplicationItem();
        $this->lobjAppConfig = new Application_Model_DbTable_ApplicationInitialConfig();
        $this->stpModel = new Application_Model_DbTable_StatusTemplate();
        $this->tplModel = new Communication_Model_DbTable_Template();
        $this->sthModel = new Application_Model_DbTable_StatusAttachment();
        $this->checklistVerification = new Application_Model_DbTable_ChecklistVerification();
        $this->appAprove = new Application_Model_DbTable_ApplicantApproval();
        $this->appEligible = new Application_Model_DbTable_ApplicantEligible();
    }
    
    public function indexAction(){
        $this->view->title=$this->view->translate("Non Eligible Applicant");
        $eligibleSearchForm = new Application_Form_ApplicantEligibleSearch();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if (isset($formData['Search']) && $formData['Search']=='Search'){
                $listEligibleApp = $this->appEligible->getEligibleApplicantSearch($formData);
                $eligibleSearchForm->populate($formData);
            }else{
                $listEligibleApp = $this->appEligible->getEligibleApplicantList();
            }
        }else{
            $listEligibleApp = $this->appEligible->getEligibleApplicantList();
        }
        
        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->qualificationpaginatorresult);
        }
            
        $lintpagecount = $this->gintPageCount;// Definitiontype model

        $lintpage = $this->_getParam('page',1); // Paginator instance
        if(isset($this->gobjsessionsis->qualificationpaginatorresult)) {
            $paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->qualificationpaginatorresult,$lintpage,$lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($listEligibleApp,$lintpage,$lintpagecount);
        }
        
        $paginator->setItemCountPerPage(10);
        
        $this->view->eligibleSearchForm = $eligibleSearchForm;
        $this->view->paginator = $paginator;
    }
}
?>