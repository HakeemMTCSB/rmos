<?php 
class Application_OldsystemController extends Zend_Controller_Action {
	public function indexAction() {
		$this->view->title="Copy from Old System";
		
		$transactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
		$profileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		$familyDB  = new App_Model_Application_DbTable_ApplicantFamily();
		$aprogDB  = new App_Model_Application_DbTable_ApplicantProgram();
		$appeduDB = new App_Model_Application_DbTable_ApplicantEducation();
		$ptestDB = new App_Model_Application_DbTable_ApplicantPtest();	
		$badb = new Application_Model_DbTable_Batchagent();		
		$selDb = new App_Model_Application_DbTable_ApplicantSelectionDetl();
		$assDb= new App_Model_Application_DbTable_ApplicantAssessment();
		
		$migrateDB = new Application_Model_DbTable_Migrate();
		
		if($this->_getparam("skr",0)==1){
			$skrs=$migrateDB->getAllSkr();
			foreach ($skrs as $skr){
				$rector["asd_type"]=2;
				$rector["asd_faculty_id"]=0;
				$rector["asd_createdby"]=99988;
				$rector["asd_createddt"]=date("Y-m-d H:i:s");
				$rector["asd_nomor"]=$skr["NO_SKR"];
				$rector["asd_decree_date"]=$skr["TGLSKR"];
				
				echo "<pre>";
				print_r($rector);
				echo "</pre>";
				$selDb->addData($rector);
			}
			exit;
		}
		
		if($this->_getparam("skr",0)==2){
			require_once 'dompdf_config.inc.php';
			$html = file_get_contents("/var/www/html/triapp/public/arabic.html");
			//echo $html;exit;
					$dompdf = new DOMPDF();
					$html=iconv('UTF-8','Windows-1250',$html);
					$dompdf->load_html($html);
					$dompdf->set_paper('a4', 'potrait');
					$dompdf->render();
					
					$dompdf = $dompdf->output();
					$output_file_path = "/var/www/html/triapp/public/test/arabic.pdf";
			
					file_put_contents($output_file_path, $dompdf);					
			exit;
		}
		
		$olddata = $migrateDB->fnGetFromOld();
		$this->view->results = $olddata;
		foreach ($olddata as $i=>$thedata){
			if(!$migrateDB->exist($thedata["no_form"])){
			$profile["appl_fname"] = $thedata["nama"];
			$profile["appl_email"] = $thedata["e_mail"];
			if($profile["appl_email"]==""){ 
				$profile["appl_email"]=$thedata["no_form"];
			}
			if($profile["appl_email"]=="-"){ 
				$profile["appl_email"]=$thedata["no_form"];
			}			
			$profile["appl_password"] = $thedata["no_form"];
			
			$profile["appl_gender"] = $thedata["jn_kel"];
			if($profile["appl_gender"]=="")  $profile["appl_gender"]="0";	
			
			$profile["appl_dob"] = $thedata["TGLLHRDT"];
			if($profile["appl_dob"]=="")  $profile["appl_dob"]="0";	
			$profile["appl_nationality"] =$thedata["w_negara"];
			if($profile["appl_nationality"]=="")  $profile["appl_nationality"]="1";	
			$profile["appl_religion"] = $thedata["agama"];
			if($profile["appl_religion"]=="")  $profile["appl_religion"]="5";	
			$profile["appl_phone_home"] = $thedata["telp"];
			if($profile["appl_phone_home"]=="")  $profile["appl_phone_home"]="0";	
			$profile["appl_phone_mobile"] = $thedata["telp"];
			if($profile["appl_phone_mobile"]=="")  $profile["appl_phone_mobile"]="0";	
					
			$profile["appl_address1"] = $thedata["alamat"]." No Rum-".$thedata["no_rum"]." RT-".$thedata["rt"]." RW-".$thedata["rw"];
			$profile["appl_postcode"] = $thedata["kd_pos"];		
			if($profile["appl_postcode"]=="")  $profile["appl_postcode"]="0";	
			
            $profile["appl_state"] = $migrateDB->getstateID($profile["cityname"]);
			if($profile["appl_state"]=="") $profile["appl_state"]=1;
                    
			$profile["appl_city"] = $thedata["kota"];	
			$profile["appl_city"] = $migrateDB->getcityID($profile["appl_city"]);
			if($profile["appl_city"]=="") $profile["appl_city"]=733;
			
			echo"<pre>";
				print_r($profile);
			echo"</pre>";
			$migrateDB->checkEmail($profile["appl_email"]);		
			$profileID=$profileDB->addData($profile);

			$family["af_relation_type"] = 20;
			$family["af_name"] = $thedata["nama_ot"];		
			if($family["af_name"]=="")$family["af_name"]=1;								
			$family["af_address1"] = $thedata["alamat_ot"]." No Rum-".$thedata["norumot"]." RT-".$thedata["rt_ot"]." RW-".$thedata["rw_ot"];
			$family["af_postcode"] = $thedata["pos_ot"];
			if($family["af_postcode"]=="")$family["af_postcode"]=1;
			$family["af_state"] = $profile["appl_state"] ;
			if($family["af_state"]=="")$family["af_state"]=1;
			$family["af_city"] = $profile["appl_city"];		
			if($family["af_city"]=="")$family["af_city"]=733;			
			$family["af_family_condition"] = $thedata["adaayh"];
			if($family["af_family_condition"]=="")$family["af_family_condition"]=1;
			
			$family["af_education_level"] = $thedata["ddkayh"]; //sis_setup_dtl
			$family["af_job"] = $thedata["pekerjaan_ayah"]; //tbl familyjob	
			if($family["af_job"]=="")$family["af_job"]=1;				
			$family["af_phone"] = $thedata["telp_ot"];	
			if($family["af_phone"]=="")$family["af_phone"]=1;				
			$family["af_appl_id"] = $profileID;		

			if($family["af_city"]==""){
				$family["af_city"]=733; //Jakarta Pusat
			}
			
			if($family["af_state"]==""){
				$family["af_state"]=255; // DKI Jakarta
			}

			if($family["af_education_level"]==""){
				$family["af_education_level"]=0; 
			}			
/*			echo"<pre>";
				print_r($family);
			echo"</pre>";*/
			$familyDB->addData($family);
			
			$family2["af_relation_type"] = 21;
			$family2["af_name"] = $thedata["nama_ibu"];
			if($family2["af_name"]=="")$family2["af_name"]="No Name";					
			$family2["af_family_condition"] = $thedata["adaibu"];
			if($family2["af_family_condition"]=="")$family2["af_family_condition"]=1;
			$family2["af_education_level"] = $thedata["ddkibu"]; //sis_setup_dtl
			$family2["af_job"] = $thedata["kerjaibu"]; //tbl familyjob
			if($family2["af_job"]=="")$family2["af_job"]=1;
			$family2["af_appl_id"] = $profileID;
			
			if($family2["af_education_level"]==""){
				$family2["af_education_level"]=0; 					
			}

/*			echo"<pre>";
				print_r($family2);
			echo"</pre>";*/
			$familyDB->addData($family2);
			

			$txndata["at_appl_id"]=$profileID;
			$txndata["at_pes_id"]=$thedata["no_form"];
			$txndata["at_appl_type"]=2;
			$txndata["at_academic_year"]=2;
			$txndata["at_intake"]=78;
			$txndata["at_period"]=3;
			$txndata["at_status"]="OFFER";
			$txndata["at_create_by"]=99988;
			$txndata["at_create_date"]=date("Y-m-d H:i:s");
			$txndata["at_submit_date"]=date("Y-m-d H:i:s");	
			$txndata["at_selection_status"]=3;
			$txndata["at_payment_status"]=1;		
/*			echo"<pre>";
				print_r($txndata);
			echo"</pre>";*/
			$txnID=$transactionDB->addData($txndata);
			
			$prog["ap_at_trans_id"] = $txnID;
			$prog["ap_prog_code"]="0".$thedata["KD_PIL"];
			$prog["ap_preference"]=1;	
			$prog["ap_usm_status"]=0;
			
/*			echo"<pre>";
				print_r($prog);
			echo"</pre>";	*/
			$aprogDB->insert($prog);	

			$hs["ae_institution"]=$thedata["kd_sla"];
			if($hs["ae_institution"]=="")$hs["ae_institution"]="0";
			$hs["ae_transaction_id"]=$txnID;
			$hs["ae_discipline_code"]=$thedata["jur_sla"];
			if($hs["ae_discipline_code"]=="")$hs["ae_discipline_code"]="0";
			$hs["ae_appl_id"]=$profileID;	

/*			echo"<pre>";
				print_r($hs);
			echo"</pre>";*/	
			$appeduDB->insert($hs);
			
			$selection["aar_trans_id"]=$txnID;
			$selection["aar_rating_rector"]=$thedata["peringkat"];
			$selection["aar_rector_status"]=1;
			$selection["aar_remarks"]="From Old System";
			$selection["aar_rector_selectionid"]=$migrateDB->getSKRid($thedata["NO_SKR"]);
			$selection["aar_rector_rateby"]=99988;
			$selection["aar_rector_ratedt"]=date("Y-m-d H:i:s");
			$selection["aar_reg_start_date"]="2012-12-01";
			$selection["aar_reg_end_date"]="2012-12-31";
			$selection["aar_payment_start_date"]="2012-12-01";
			$selection["aar_payment_end_date"]="2012-12-31";
/*			echo"<pre>";
				print_r($selection);
			echo"</pre>";*/		
			$assDb->addData($selection);	
			
			$udata["status"]=1;
			$migrateDB->update($udata,"no_form='".$thedata["no_form"]."'");
			echo $thedata["no_form"]." - New applicant added<br>";
			}else{
				echo $thedata["no_form"]."!! No Formulir Exist!<br>";
			}	
		}	
							
	}
}
?>