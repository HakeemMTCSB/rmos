<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_ReportController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $model;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
		$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Application_Model_DbTable_Report();
    }
    
    public function indexAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->view->title = $this->view->translate('List of Applicants');
        
        $session = Zend_Registry::get('sis');
        
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
        $form = new Application_Form_SearchApplicantReport();
        $applicantDB = new Application_Model_DbTable_Report();
         
         if($this->getRequest()->isPost()){
        	
			 $formData = $this->getRequest()->getPost();
			 
			// print_r($formData);exit;
             if($form->isValid($formData))	{
				 
				$form->populate($formData);
				$this->view->IdProgramScheme = $formData['IdProgramScheme'];
							
			    $student_list = $applicantDB->getApplicantList($formData);
			    $this->view->student = $student_list;
				
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));				
				$paginator->setItemCountPerPage(1000);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));				
				$form->populate($formData);
				$this->view->paginator = $paginator;
				
				//echo '<pre>';
        		//print_r($student_list);
        		//exit;
			}
        }else{ 
    		
    		//populate by session 
	    	if (isset($session->result)) {    
	    			
	    		$form->populate($session->result);
	    		$formData = $session->result;			
	    		$this->view->IdProgramScheme = $formData['IdProgramScheme'];
				
	    		$student_list = $applicantDB->getApplicantList($formData);
			  	$this->view->student = $student_list;
			  	  
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));				
				$paginator->setItemCountPerPage(1000);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));				
			
				$this->view->paginator = $paginator;
	    	}
    		
    	}
        $this->view->form = $form;
            
        
       
    }
    
    public function printReportAction(){
    	
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
    	$this->_helper->layout->disableLayout();
    	 
     	if($this->getRequest()->isPost()){
        	
			$formData = $this->getRequest()->getPost();
          	 	
			$applicantDB = new Application_Model_DbTable_Report();
		    $student_list = $applicantDB->getApplicantList($formData);
		    $this->view->paginator = $student_list;	
        }
        $this->view->filename = date('Ymd').'_listofapplicant.xls';
    }
    
	public function getProgramSchemeAction()
	{	
    	$idProgram = $this->_getParam('idProgram', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
        $programDb = new Registration_Model_DbTable_Program();
        $result = $programDb->getProgramSchemeList($idProgram);
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
}