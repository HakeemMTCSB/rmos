<?php 
class Application_UsmSelectionController extends Zend_Controller_Action {
	
	public function usmSummaryAction(){		
	
		$this->view->title = "USM Summary";
		
		$intake_id = $this->_getParam('intake_id', null);
    	$this->view->intake_id = $intake_id;
    	
    	$period_id = $this->_getParam('period_id', null);
    	$this->view->period_id = $period_id;
    	
    	$ptest = $this->_getParam('ptest', null);
    	$this->view->ptest = $ptest;
    	
    	$load_previous_period = $this->_getParam('load_previous_period', 0);
    	$this->view->load_previous_period = $load_previous_period;
    	
    	$aps_test_date = $this->_getParam('aps_test_date', null);
    	$this->view->aps_test_date = $aps_test_date;
    	
    	$aps_id = $this->_getParam('aps_id', null);
    	$this->view->aps_id = $aps_id ;
    	    	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale =$locale;
					
		$collegeDB = new App_Model_General_DbTable_Collegemaster();
		$college = $collegeDB->getFaculty();
		$this->view->college = $college;
		
		$form = new Application_Form_SearchSummaryUsm(array('ptest'=>$ptest,'ptest_date'=>$aps_test_date));
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) {
			if ($form->isValid ( $_POST )) {
				
				$formData = $this->getRequest()->getPost();	
				$this->view->formData  = $formData;	
				$form->populate($formData);
				$this->view->form = $form;
				
			}
		}
				
	}//end function
	
	public function listPassUsmAction(){
		
		$this->view->title = "USM Selection Process";
		
		$msg = $this->_getParam('msg', null);
		if($msg){
    		$this->view->noticeSuccess = $this->view->translate("Information has been save successfully");
		}
    	
		$intake_id = $this->_getParam('intake_id', null);
    	$this->view->intake_id = $intake_id;
    	
    	$period_id = $this->_getParam('period_id', null);
    	$this->view->period_id = $period_id;
    	
    	$load_previous_period = $this->_getParam('load_previous_period', 0);
    	$this->view->load_previous_period = $load_previous_period;
    	
    	$ptest = $this->_getParam('ptest', null);
    	$this->view->ptest = $ptest;
    	
    	$aps_test_date = $this->_getParam('aps_test_date', null);
    	$this->view->aps_test_date = $aps_test_date;
    	
    	$aps_id = $this->_getParam('aps_id', null);
    	$this->view->aps_id = $aps_id ;
    	
    	$faculty = $this->_getParam('faculty', null);
    	$this->view->faculty = $faculty;
    	
    	$programme = $this->_getParam('programme', null);
    	$this->view->programme = $programme;
    	
    	$preference = $this->_getParam('preference', null);
    	$this->view->preference = $preference;
    	
    	$quota = $this->_getParam('quota',1);
    	$this->view->quota = $quota;
    	
    	$limit = $this->_getParam('limit',null);
    	$this->view->limit = $limit;
    	
    	$attendance = $this->_getParam('aps_usm_attendance',null);
    	$this->view->attendance = $attendance;
		    	
		$form = new Application_Form_SearchUsm(array('ptest'=>$ptest,'ptest_date'=>$aps_test_date,'quota'=>$quota,'limitt'=>$limit));
		$this->view->form = $form;
		
			
		if ($this->getRequest()->isPost()) {
			if ($form->isValid ( $_POST )) {
				$formData = $this->getRequest()->getPost();				
				$form->populate($formData);
				$this->view->form = $form;
				
				$transDB = new App_Model_Application_DbTable_ApplicantTransaction();
				//$passingmark = $transDB->getPassingMark($programme,$ptest);
				$this->view->passingmark=$formData["passmark"];
				$applicant = $transDB->getApplicantPassNotInPoolUSM($ptest,$preference,$intake_id,$period_id,0,$load_previous_period,$programme,$aps_id,$aps_test_date,$limit,$formData["passmark"],'',$attendance);
				$this->view->applicant = $applicant;
				
			}
		}elseif($programme && $aps_test_date){
			
			$formData = array('intake_id'=>$intake_id,'ptest'=>$ptest,'aps_test_date'=>$aps_test_date,'aps_id'=>$aps_id,'programme'=>$programme,'preference'=>$preference,'faculty'=>$faculty,'quota'=>1);
			
			$form->populate($formData);
		    $this->view->form = $form;
		    
		    $transDB = new App_Model_Application_DbTable_ApplicantTransaction();
			$applicant = $transDB->getApplicantPassNotInPoolUSM($ptest,$preference,$intake_id,$period_id,0,$load_previous_period,$programme,$aps_id,$aps_test_date,$limit);
			$this->view->applicant = $applicant;
		}
	}
	
	public function ajaxGetProgramAction($faculty_id=0){
    	$faculty_id = $this->_getParam('faculty_id', 4);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select()
	                 ->from(array('p'=>'tbl_program'))
	                 ->where('p.IdCollege = ?', $faculty_id)
	                 ->order('p.ProgramName ASC');
	    
			
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();
        
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		
		$i=0;
		foreach ($row as $p){
			
			if($locale=="id_ID"){
				$program_name = $p["ArabicName"];
			}elseif($locale=="en_US"){
				$program_name = $p["ProgramName"];
			}
			
		$program[$i]["ProgramCode"]=$p["ProgramCode"];
		$program[$i]["ProgramName"]=$program_name;
		$i++;
		}
			
	    
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($program);
		
		echo $json;
		exit();
    }
    
	public function ajaxGetPeriodAction(){
    	$intake_id = $this->_getParam('intake_id', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select()
	                 ->from(array('ap'=>'tbl_academic_period'),array('ap_id','ap_desc'))
	                 ->order('ap.ap_year')
	                 ->order('ap.ap_number');
	    
	    if($intake_id!=0){
	    	$select->where('ap.ap_intake_id = ?', $intake_id);
	    }
		
	   
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
	public function ajaxGetDateAction(){
    	$ptest = $this->_getParam('ptest', null);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select()
	                 ->from(array('aps'=>'appl_placement_schedule'))
	                 ->where("aps_test_date <= curdate()")
					 ->where("aps_placement_code = '".$ptest."'")
					 ->group("aps_test_date");
	    
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
    public function ajaxGetLocationAction(){
    	$aps_test_date = $this->_getParam('aps_test_date', null);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select()
	                 ->from(array('aps'=>'appl_placement_schedule'),array("aps.aps_id"))
	                 ->joinLeft(array('l'=>'appl_location'),'l.al_id=aps.aps_location_id ',array('location_name'=>'l.al_location_name'))
					  ->where("aps.aps_test_date = '".$aps_test_date."'");	
	    
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
	public function ajaxGetNomorAction(){
    	$aaud_id = $this->_getParam('aaud_id', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db ->select()
					  ->from(array('aaud'=>'applicant_assessment_usm_detl'))
					  ->joinleft(array('ay'=>'tbl_academic_year'),'ay.ay_id=aaud.aaud_academic_year',array('ay_code'))
					  ->where("aaud.aaud_id = '".$aaud_id."'")
					  ->where("aaud_lock_status!=1");
		
        $row = $db->fetchRow($select);
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
	public function saveSelectionAction(){
    	
    	if ($this->getRequest()->isPost()) {
			
			    $formData = $this->getRequest()->getPost();	
			    
			    $data["ats_program_code"]   =  $formData["program_code"];					
				$data["ats_preference"]     =  $formData["preference"];
					
				for($i=0; $i<count($formData["transaction_id"]); $i++){
					
					$txn_id =  $formData["transaction_id"][$i];
					$status =  $formData["status"][$i];
					
					if($status=="1"){ //save in pool
						
						$data["ats_transaction_id"] =  $formData["transaction_id"][$i];
						$data["ats_ap_id"]          =  $formData["ap_id"][$i];
					
						//print_r($data);
					
						$tempSelectionDB =  new App_Model_Application_DbTable_ApplicantTempUsmSelection();
						$tempSelectionDB->addData($data);
						
						$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
						$transactionDB->updateData(array('at_selection_status'=>4),$txn_id);//save in pool
					
					}elseif($status==2){//reject preference
						
						$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
						$appProgramDB->updateData(array("ap_usm_status"=>2),$formData["ap_id"][$i]);
						
						$program_apply = $appProgramDB->getProgramRejectStatus($txn_id);
						
						if(count($program_apply)==1){
				        	
				        	$pref1 = $appProgramDB->getProgramPreference($txn_id,1);
				        	
				        	if($pref1["ap_usm_status"]==2){
				        		
				        		//reject application
								$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
								$transactionDB->updateData(array('at_status'=>'REJECT','at_selection_status'=>3),$txn_id);
				        	}else{
								$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
								$transactionDB->updateData(array('at_selection_status'=>0),$txn_id);
							}
				        	
				        }elseif(count($program_apply)==2){
				        	
				        	//cehck if both program preference rejected set at_status=REJECT
							$pref1 = $appProgramDB->getProgramPreference($txn_id,1);
							$pref2 = $appProgramDB->getProgramPreference($txn_id,2);
							
							if(($pref1["ap_usm_status"]==2) && ($pref2["ap_usm_status"]==2)){
								
								//reject application
								$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
								$transactionDB->updateData(array('at_status'=>'REJECT','at_selection_status'=>3),$txn_id);
							}else{
								$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
								$transactionDB->updateData(array('at_selection_status'=>0),$txn_id);
							}
				        }
        
						//cehck if both program preference rejected set at_status=REJECT
						/*$pref1 = $appProgramDB->getProgramPreference($txn_id,1);
						$pref2 = $appProgramDB->getProgramPreference($txn_id,2);
						
						if(($pref1["ap_usm_status"]==2) && ($pref2["ap_usm_status"]==2)){
							//reject application
							$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
							$transactionDB->updateData(array('at_status'=>'REJECT','at_selection_status'=>3),$txn_id);
							
						}else{
							$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
							$transactionDB->updateData(array('at_selection_status'=>0),$txn_id);
						}*/
						
					}elseif($status==3){ //reject application
						
							$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
							$appProgramDB->updateStatusData(array("ap_usm_status"=>2),$txn_id);
							
							//reject application
							$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
							$transactionDB->updateData(array('at_status'=>'REJECT','at_selection_status'=>3),$txn_id);//completed
							
							//send reject email
					}
					
				}//end if
				
				
				$this->_redirect($this->view->url(array('module'=>'application','controller'=>'usm-selection','action'=>'list-pass-usm','msg'=>'success'),'default',true));
    	}// end if	
    	
    }
    
    
   
    
    
    public function listInPoolAction(){    	
    	
		$this->view->title = "Verification Process";
				
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
		
		$msg = $this->_getParam('msg', null);
		if($msg){
    		$this->view->noticeSuccess = $this->view->translate("Information has been save successfully");
		}
    	
		$faculty = $this->_getParam('faculty', null);
    	$this->view->faculty = $faculty;
				
		$programme = $this->_getParam('programme', null);
    	$this->view->programme = $programme;
    	
    	$preference = $this->_getParam('preference', null);
    	$this->view->preference = $preference;
    	
    	$quota = $this->_getParam('quota',1);
    	$this->view->quota = $quota;
    	
    	$limit = $this->_getParam('limit',50);
    	$this->view->limit = $limit;
    	
    	$form = new Application_Form_SearchPool();
		$this->view->form = $form;
		
		//if(($programme && $preference) || ($form->isValid ( $_POST ))){	
		if ($this->getRequest()->isPost()) {
			
			    //$formData = array('programme'=>$programme,'preference'=>$preference,'faculty'=>$faculty,'quota'=>$quota,'limit'=>$limit);
			    
				$formData = $this->getRequest()->getPost();
				
			    $form->populate($formData);
			    $this->view->form = $form;
			    
				$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
				
				$applicant = $transactionDB->getApplicantInPool($programme,$preference,$faculty);
				
				
				$this->view->applicant = $applicant;
				
		}
		
    }
    
 	public function saveVerificationAction(){
    	
    	if ($this->getRequest()->isPost()) {
			
			    $formData = $this->getRequest()->getPost();	
			    $auth = Zend_Auth::getInstance(); 
			    
			    for($i=0; $i<count($formData["transaction_id"]); $i++){
			    	
				    	$txn_id =  $formData["transaction_id"][$i];
						$ats_id =  $formData["ats_id"][$i];
						$status =  $formData["status"][$i];
	
					
				    	if($status=="1"){ //selected dari pool
							
							//add dalam assessment USM
							$data["aau_trans_id"]       =  $formData["transaction_id"][$i];
							$data["aau_ap_id"]          =  $formData["ap_id"][$i];
							$data["aau_createdby"]      =  $auth->getIdentity()->id;
							$data["aau_createddt"]      =  date("Y-m-d H:m:s");
						
							
							
							//add assessment data
							$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
							$assessmentDb->addData($data);
														
							//update transaction
							$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
							$transactionDB->updateData(array('at_selection_status'=>1),$txn_id);
												
						
						}elseif($status==2){//reject prefference
							
							$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
							$appProgramDB->updateData(array("ap_usm_status"=>2),$formData["ap_id"][$i]);
							
							$program_apply = $appProgramDB->getProgramRejectStatus($txn_id);
						
							if(count($program_apply)==1){
				        	
						        	$pref1 = $appProgramDB->getProgramPreference($txn_id,1);
						        	
						        	if($pref1["ap_usm_status"]==2){
						        		
						        		//reject application
										$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
										$transactionDB->updateData(array('at_status'=>'REJECT','at_selection_status'=>3),$txn_id);
						        	}else{
										$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
										$transactionDB->updateData(array('at_selection_status'=>0),$txn_id);
									}
				        	
				        	}elseif(count($program_apply)==2){
				        	
						        	//cehck if both program preference rejected set at_status=REJECT
									$pref1 = $appProgramDB->getProgramPreference($txn_id,1);
									$pref2 = $appProgramDB->getProgramPreference($txn_id,2);
									
									if(($pref1["ap_usm_status"]==2) && ($pref2["ap_usm_status"]==2)){
										
										//reject application
										$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
										$transactionDB->updateData(array('at_status'=>'REJECT','at_selection_status'=>3),$txn_id);
									}else{
										$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
										$transactionDB->updateData(array('at_selection_status'=>0),$txn_id);
									}
				       		 }//else if
				        
							/*//cehck if both program preference was rejected set at_status=REJECT
							$pref1 = $appProgramDB->getProgramPreference($txn_id,1);
							$pref2 = $appProgramDB->getProgramPreference($txn_id,2);
							
							if(($pref1["ap_usm_status"]==2) && ($pref2["ap_usm_status"]==2)){
								//reject application
								$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
								$transactionDB->updateData(array('at_status'=>'REJECT','at_selection_status'=>3),$txn_id);
								
							}else{
								$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
								$transactionDB->updateData(array('at_selection_status'=>0),$txn_id);
							}*/
							
						}elseif($status==3){ //reject application
							
								$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
								$appProgramDB->updateStatusData(array("ap_usm_status"=>2),$txn_id);
								
								//reject application
								$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
								$transactionDB->updateData(array('at_status'=>'REJECT','at_selection_status'=>3),$txn_id);//completed
								
								//send reject email
						}
						
						if($status!=4){							
							//delete at temp
							$tempSelectionDB =  new App_Model_Application_DbTable_ApplicantTempUsmSelection();
							$tempSelectionDB->deleteData($ats_id);
						}
						
			    }//end for
	
								
				$this->_redirect($this->view->url(array('module'=>'application','controller'=>'usm-selection','action'=>'list-in-pool','msg'=>'success'),'default',true));
    	}// end if	
    	
    }
    
    
	public function rectorApprovalAction(){
    	
    	$this->view->title = "Rector Approval";
		
    	$msg = $this->_getParam('msg', null);
    	$this->view->msg = $msg;
    	
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Data has been saved.");
		}
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
		
		$intake = $this->_getParam('intake', null);
    	$this->view->intake = $intake;
    	
		$faculty = $this->_getParam('faculty', null);
    	$this->view->faculty = $faculty;
				
		$programme = $this->_getParam('programme', null);
    	$this->view->programme = $programme;
    	
    	$preference = $this->_getParam('preference', null);
    	$this->view->preference = $preference;
    	
    	//get list nomor
    	$usmDB =  new App_Model_Application_DbTable_ApplicantAssessmentUsmDetl();		
		$list_nomor = $usmDB->getUnlockNomor();
		
		$this->view->list_nomor = $list_nomor;
		
    	//academic year for save
    	$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
    	$academicYearList = $academicYearDb->getAcademicYear(array('NEXT','FUTURE'));
    	$this->view->academicYearList = $academicYearList;
    	
    	//get lock data
    	$usmDB =  new App_Model_Application_DbTable_ApplicantAssessmentUsmDetl();    		
    	$lockDate = $usmDB->getLockData();
    	
    	$l_date = array();
    	foreach ($lockDate as $date_l){
    		$l_date[] = date('n-j-Y', strtotime($date_l['date']));
    	}    	
    	$this->view->localDate = $l_date;
    	
    	$form = new Application_Form_RectorSearch();
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) {
			    if($form->isValid ( $_POST )){
			    				    	
			    	$formData = $this->getRequest()->getPost();	
			    	$auth = Zend_Auth::getInstance(); 
			    	
			    	$form->populate($formData);
			        $this->view->form = $form;
			    	
			    	$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
			    	$applicant = $transactionDB->getUSMRectorVerification($intake,$programme,$preference,$faculty);
			    	$this->view->applicant = $applicant;

			    }
			    
		}
    }
    
	public function saveRectorApprovalAction(){
    
		if ($this->getRequest()->isPost()) {
			
			    $formData = $this->getRequest()->getPost();	
			    $auth = Zend_Auth::getInstance(); 
			   
				$detl_id = $formData["nomor"];
				
				 //lock rector nomor
				if(isset($formData['lock_date']) && $formData['lock_date']==1 ){
					
					$data = array(						
						'aaud_lock_status'=>1,					 
						'aaud_lock_by'=>$auth->getIdentity()->iduser,
						'aaud_lock_date'=>date ('Y-m-d h:i:s')
					);
					
					 $assessmentDetlDb = new App_Model_Application_DbTable_ApplicantAssessmentUsmDetl();
			   	     $assessmentDetlDb->updateData($data,$detl_id);
				}

				
			    
			    for($i=0; $i<count($formData["transaction_id"]); $i++){
			    	
			   		if($formData["rector_status"][$i]==1){ //ACCEPT			   			
			   			
			   			//update assessment usm
				    	$data["aau_id"]   =$formData["aau_id"][$i];
				    	$data["aau_rector_ranking"]  =$formData["rector_ranking"][$i];
				    	$data["aau_rector_status"]   =$formData["rector_status"][$i];
				    	$data["aau_rector_createby"] =$auth->getIdentity()->id;
				    	$data["aau_rector_createdt"] =date("Y-m-d H:m:s");
				    	$data["aau_rector_selectionid"]  = $detl_id;
				    	 
				    	$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
						$assessmentDb->updateData($data,$formData["aau_id"][$i]);
									   			
			   			//update selection status to completed
						$status["at_status"]="OFFER";
						$status["at_selection_status"]=3;//completed
						$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
						$transactionDB->updateData($status,$formData["transaction_id"][$i]);
						
						//update status offer program,
						$program["ap_usm_status"]=1;
						$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
						$appProgramDB->updateData($program,$formData["ap_id"][$i]);
						
						//create performa invoice
						/*$proformaInvoiceDb = new Application_Model_DbTable_ProformaInvoice();
						if(!$proformaInvoiceDb->generateUSMProformaInvoice($formData["transaction_id"][$i],date("Y-m-d",strtotime($formData["decree_date"])),$formData["ap_id"][$i]) ){
							$errorMsg = $this->view->noticeError;
							$this->view->noticeError = $errorMsg." ".$this->view->translate("Proforma Invoice not created. Please inform Admin");
						}*/

			   		}elseif($formData["rector_status"][$i]==5){ //Lulus tahap pertama			   			
			   			
			   			//update assessment usm
				    	$data["aau_id"]   =$formData["aau_id"][$i];
				    	$data["aau_rector_ranking"]  =$formData["rector_ranking"][$i];
				    	$data["aau_rector_status"]   =$formData["rector_status"][$i];
				    	$data["aau_rector_createby"] =$auth->getIdentity()->id;
				    	$data["aau_rector_createdt"] =date("Y-m-d H:m:s");
				    	$data["aau_rector_selectionid"]  = $detl_id;
				    	 
				    	$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
						$assessmentDb->updateData($data,$formData["aau_id"][$i]);
									   			
			   			//update selection status to completed
						$status["at_status"]="PROCESS";
						$status["at_selection_status"]=5;//lulus tahap pertama rector
						$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
						$transactionDB->updateData($status,$formData["transaction_id"][$i]);
						
						//update status offer program,
						$program["ap_usm_status"]=1; //Lulus tahap pertama
						$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
						$appProgramDB->updateData($program,$formData["ap_id"][$i]);
						
						//create performa invoice
						/*$proformaInvoiceDb = new Application_Model_DbTable_ProformaInvoice();
						if(!$proformaInvoiceDb->generateUSMProformaInvoice($formData["transaction_id"][$i],date("Y-m-d",strtotime($formData["decree_date"])),$formData["ap_id"][$i]) ){
							$errorMsg = $this->view->noticeError;
							$this->view->noticeError = $errorMsg." ".$this->view->translate("Proforma Invoice not created. Please inform Admin");
						}*/
						

					

					}elseif($formData["rector_status"][$i]==2){//reject preference
						
						//update assessment usm
						$data["aau_rector_status"]   =$formData["rector_status"][$i];
						$data["aau_rector_createby"] =$auth->getIdentity()->id;
						$data["aau_rector_createdt"] =date("Y-m-d H:m:s");
						$data["aau_rector_selectionid"]  = $detl_id;
						
						$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
						$assessmentDb->updateData($data,$formData["aau_id"][$i]);
						
						
						
						$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
						$appProgramDB->updateData(array("ap_usm_status"=>2),$formData["ap_id"][$i]);
						
						
						$program_apply = $appProgramDB->getProgramRejectStatus($formData["transaction_id"][$i]);
						
						if(count($program_apply)==1){
				        	
				        	$pref1 = $appProgramDB->getProgramPreference($formData["transaction_id"][$i],1);
				        	
				        	if($pref1["ap_usm_status"]==2){
				        		
				        		//reject application
								$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
								$transactionDB->updateData(array('at_status'=>'REJECT','at_selection_status'=>3),$formData["transaction_id"][$i]);
				        	}else{
								$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
								$transactionDB->updateData(array('at_selection_status'=>0),$formData["transaction_id"][$i]);
							}
				        	
				        }elseif(count($program_apply)==2){
				        	
				        	//cehck if both program preference rejected set at_status=REJECT
							$pref1 = $appProgramDB->getProgramPreference($formData["transaction_id"][$i],1);
							$pref2 = $appProgramDB->getProgramPreference($formData["transaction_id"][$i],2);
							
							if(($pref1["ap_usm_status"]==2) && ($pref2["ap_usm_status"]==2)){
								
								//reject application
								$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
								$transactionDB->updateData(array('at_status'=>'REJECT','at_selection_status'=>3),$formData["transaction_id"][$i]);
							}else{
								$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
								$transactionDB->updateData(array('at_selection_status'=>0),$formData["transaction_id"][$i]);
							}
				        }
						
						/*//cehck if both program preference was rejected set at_status=REJECT
						$pref1 = $appProgramDB->getProgramPreference($formData["transaction_id"][$i],1);
						$pref2 = $appProgramDB->getProgramPreference($formData["transaction_id"][$i],2);
						
						if(($pref1["ap_usm_status"]==2) && ($pref2["ap_usm_status"]==2)){
							//reject application
							$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
							$transactionDB->updateData(array('at_status'=>'REJECT','at_selection_status'=>3),$formData["transaction_id"][$i]);
							
						}else{
							$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
							$transactionDB->updateData(array('at_selection_status'=>0),$formData["transaction_id"][$i]);
						}*/
						
						
					}elseif($formData["rector_status"][$i]==3){//reject application
						
						//update assessment usm
						$data["aau_rector_status"]   =$formData["rector_status"][$i];
						$data["aau_rector_createby"] =$auth->getIdentity()->id;
						$data["aau_rector_createdt"] =date("Y-m-d H:m:s");
						$data["aau_rector_selectionid"]  = $detl_id;
						
						$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
						$assessmentDb->updateData($data,$formData["aau_id"][$i]);
						
						//reject application
						$status["at_status"]="REJECT";
						$status["at_selection_status"]=3;//completed
						$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
						$transactionDB->updateData($status,$formData["transaction_id"][$i]);
						
						//update status program
						$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
						$appProgramDB->updateStatusData(array("ap_usm_status"=>2),$formData["transaction_id"][$i]);
											
					}//END IF ACCEPT/REJECT
					
					
			    }//end for
			    
			    
			    
			 $this->_redirect($this->view->url(array('module'=>'application','controller'=>'usm-selection','action'=>'rector-approval','msg'=>'success.'),'default',true));   
				
		}//en if post
		
		
    }
    
    
    public function selectionStatusAction(){
    	
    	$this->view->title = $this->view->translate("usm selection_status");
    
    	$msg = $this->_getParam('msg', null);
    	$this->view->msg = $msg;
    	
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Email has been sent.");
		}
		$session = new Zend_Session_Namespace('sis');
		
		if($session->IdRole == 298){ //FACULTY ADMIN			
			$faculty = $this->_getParam('faculty', $session->idCollege);
		}else{
			$faculty = $this->_getParam('faculty',null);
		}				
    	$this->view->faculty_id = $faculty;
    	
    	$intake = $this->_getParam('intake', null);
    	$this->view->intake = $intake;
    	
    	$period = $this->_getParam('period', null);
    	$this->view->period = $period;

    	$ptest = $this->_getParam('ptest', null);
    	$this->view->ptest = $ptest;
    	
    	$aps_test_date = $this->_getParam('aps_test_date', null);
    	$this->view->aps_test_date = $aps_test_date;
    	
    	$aps_id = $this->_getParam('aps_id', null);
    	$this->view->aps_id = $aps_id ;
    	
		$programme = $this->_getParam('programme', null);
    	$this->view->programme = $programme;
    	
    	$preference = $this->_getParam('preference', null);
    	$this->view->preference = $preference;

    	$selection_status = $this->_getParam('selection_status',null);
    	$this->view->selection_status = $selection_status;
    	
    	$form = new Application_Form_UsmSelectionStatusSearch(array('intake'=>$intake,'ptest'=>$ptest,'ptest_date'=>$aps_test_date));
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) {
			    if($form->isValid ( $_POST )){
			    				    	
			    	$formData = $this->getRequest()->getPost();	
			    	$auth = Zend_Auth::getInstance(); 
			    	
			    	$form->populate($formData);
			        $this->view->form = $form;

			        $transDB = new App_Model_Application_DbTable_ApplicantTransaction();
			        $applicant_data = $transDB->getUSMSelectionStatus($intake,$period,$programme,$preference,$selection_status,$ptest,$aps_test_date,$aps_id);
					
					$this->view->applicant_data=$applicant_data;
					$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($applicant_data));
					$paginator->setItemCountPerPage(100);
					$paginator->setCurrentPageNumber($this->_getParam('page',1));			
					$this->view->paginator = $paginator;
			    	
			    }
			    
		}
    }
    
	
    
    public function selectionDetailAction(){
    	
    	$this->view->title = $this->view->translate("Selection Status").$this->view->translate(" - Detail");
		
		$txnId = $this->_getParam('txn_id',null);
		$this->view->txn_id = $txnId;
		
		//txn data
		$applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
		$txnData = $applicantTxnDB->getTransaction($txnId);
		
		//programme applied
		$programDB =  new App_Model_Application_DbTable_ApplicantProgram();
		$program_list = $programDB->getApplicantProgramByID($txnId);
		$txnData['programme'] = $program_list;		
		$this->view->data = $txnData;
		
		//selection rating
		$assessmentDB=new App_Model_Application_DbTable_ApplicantAssessmentUsm();
		$selection_status = $assessmentDB->getData($txnId);
		$this ->view->selection_data = $selection_status;
		
		//document
		$docs = array();
		$arr_doc = array();
		$arr_upload = array();
		
		$documentDB = new App_Model_Application_DbTable_ApplicantDocument();
		$documentUploadDb = new App_Model_Application_DbTable_ApplicantUploadFile();
			
		//document
		$arr_doc[0] = $documentDB->getDataArray($txnId, 30);
		$arr_doc[1] = $documentDB->getDataArray($txnId, 31);
		$arr_doc[2] = $documentDB->getDataArray($txnId, 32);
		$arr_doc[3] = $documentDB->getDataArray($txnId, 42);
		$arr_doc[4] = $documentDB->getDataArray($txnId, 45);
		$docs['document'] = $arr_doc;
		$this->view->arr_doc = $arr_doc;
		
		//upload document
		/*$arr_upload[0] = $documentUploadDb->getTxnFileArray($txnId,33); //gambar
		$arr_upload[1] = $documentUploadDb->getTxnFileArray($txnId,34); //kpt
		$arr_upload[2] = $documentUploadDb->getTxnFileArray($txnId,35); //passport
		$arr_upload[3] = $documentUploadDb->getTxnFileArray($txnId,36); //surat doctor
		$arr_upload[4] = $documentUploadDb->getTxnFileArray($txnId,37); //transcript
		$docs['upload_document'] = $arr_upload;			
		$docs['path'] = "http://".ONNAPP_HOSTNAME."/documents/";		 
		$this->view->doc = $docs;*/
    }
    
	public function sendMailAction(){
		
		if ($this->getRequest()->isPost()) {
		    				    	
		   	$formData = $this->getRequest()->getPost();	
		   	
		   	if($formData["status"]==3) $status="OFFER";
		   	if($formData["status"]==5) $status="REJECT";
		   	
		   	if(isset($status)){
			   	 for($i=0; $i<count($formData["transaction_id"]); $i++){
			   	 	
			   	 	$this->sendmail($formData["transaction_id"][$i],$status);
			   	 }
			   	 
			   	 $this->_redirect($this->view->url(array('module'=>'application','controller'=>'usm-selection','action'=>'selection-status','msg'=>'email success.'),'default',true));
		   	}
			
		}
		
	}
	
	private function sendmail($transaction_id,$status=null){
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		//get applicant info
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
    	$applicant = $applicantDB->getAllProfile($transaction_id);
    	
    	//getapplicantprogram
		$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();		
		$program = $appProgramDB->getProgramOffered($transaction_id);
		
		if($locale=="id_ID"){
			$program_name = $program[0]["program_name_indonesia"];
		}elseif($locale=="en_US"){
			$program_name = $program[0]["program_name"];
		}
		
		//get applicant parents info
    	$familyDB =  new App_Model_Application_DbTable_ApplicantFamily();
    	$father = $familyDB->fetchdata($applicant["appl_id"],20); //father's    	
    	
    	//get assessment info
    	$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
    	$assessment = $assessmentDb->getInfo($transaction_id);
			 	
			$fullname = $applicant["appl_fname"].' '.$applicant["appl_mname"].' '.$applicant["appl_lname"];
			$url_student_portal = "http://".APP_HOSTNAME."/online-application";
			
			$templateDB = new App_Model_General_DbTable_EmailTemplate();
			$templateData = $templateDB->getData(5,$applicant["appl_prefer_lang"]);//offer letter
			
			if(isset($applicant["appl_prefer_lang"]) || ($applicant["appl_prefer_lang"]==0)){
				$applicant["appl_prefer_lang"]=2;
			}
			
			if($status=="OFFER"){				
				$templateData = $templateDB->getData(5,$applicant["appl_prefer_lang"]);//offer letter
			}elseif ($status=="REJECT"){
				$templateData = $templateDB->getData(7,$applicant["appl_prefer_lang"]);//reject application letter
			}

    		$templateMail = $templateData['body'];				
			$templateMail = str_replace("[applicant_name]",$fullname,$templateMail);
			$templateMail = str_replace("[program]",$program_name,$templateMail);
			$templateMail = str_replace("[faculty]",$program[0]["faculty2"],$templateMail);
			$templateMail = str_replace("[url_student_portal]",$url_student_portal,$templateMail);
			$templateMail = str_replace("[assessment_type]",'Ujian Saringan Masuk',$templateMail);
			$templateMail = str_replace("[peringkat]",$assessment["aau_rector_ranking"],$templateMail);
			
			$emailDb = new App_Model_System_DbTable_Email();		
			$data = array(
				'recepient_email' => $applicant["appl_email"],
				'subject' => $templateData["subject"],
				'content' => $templateMail,
			    'attachment_path'=>'',
			    'attachment_filename'=>''
			);	
			
			//to send email with attachment
			$emailDb->addData($data);		
		
	}
	
	
	public function saveReverseSelectionAction(){
				
		 $auth = Zend_Auth::getInstance(); 	
		 
		if ($this->getRequest()->isPost()) {
		    				    	
		   	$formData = $this->getRequest()->getPost();	
		   	
		   	//print_r($formData);
		   	
		   		 for($i=0; $i<count($formData["transaction_id"]); $i++){
			   	 		
		   		 		//update status,selection status
		   				$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
						$transactionDB->updateData(array('at_status'=>'PROCESS','at_selection_status'=>0),$formData["transaction_id"][$i]);
			   	 	
						//upldate applicant program
						$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
						$appProgramDB->updateStatusData(array("ap_usm_status"=>0),$formData["transaction_id"][$i]);	
						
						//reversal_status=1 if (selection_status==2) or delete
						$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessmentUsm();
						//$assessmentDb->updateStatusData(array("aau_reversal_status"=>1),$formData["transaction_id"][$i]);
				        $assessmentDb->deleteAssessmentData($formData["transaction_id"][$i]);	
					
						//delete dalam pool
						$tempSelectionDB =  new App_Model_Application_DbTable_ApplicantTempUsmSelection();
						$tempSelectionDB->deleteTransaction($formData["transaction_id"][$i]);
						
						$trackDB = new App_Model_Application_DbTable_ApplicantReversalTrack();
						$data["art_appl_type"]=1;
						$data["art_trans_id"]=$formData["transaction_id"][$i];
						$data["art_createdby"]=$auth->getIdentity()->iduser;
						$data["art_createddt"]=date ('Y-m-d h:i:s');
						$trackDB->addData($data);
						
			   	 }			   	 
			   	 $this->_redirect($this->view->url(array('module'=>'application','controller'=>'usm-selection','action'=>'selection-status','msg'=>'reversal'),'default',true));
			   	
		}
		
	}
}

?>
