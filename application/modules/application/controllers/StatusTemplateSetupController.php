<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_StatusTemplateSetupController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $dclModel;
    private $defModel;
    private $lobjAppItem;
    private $lobjAppConfig;
    private $stpModel;
    private $tplModel;
    private $sthModel;
    private $statusModel;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->dclModel = new Application_Model_DbTable_DocumentChecklist();
        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->lobjAppItem = new Application_Model_DbTable_ApplicationItem();
        $this->lobjAppConfig = new Application_Model_DbTable_ApplicationInitialConfig();
        $this->stpModel = new Application_Model_DbTable_StatusTemplate();
        $this->tplModel = new Communication_Model_DbTable_Template();
        $this->sthModel = new Application_Model_DbTable_StatusAttachment();
        $this->statusModel = new Application_Model_DbTable_Status();
    }
    
    public function indexAction(){
        $this->view->title=$this->view->translate("Status Template Setup"); 
        $statusTplList = $this->stpModel->fnGetStatusTemplateList();
        
        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->qualificationpaginatorresult);
        }
            
        $lintpagecount = $this->gintPageCount;// Definitiontype model

        $lintpage = $this->_getParam('page',1); // Paginator instance
        if(isset($this->gobjsessionsis->qualificationpaginatorresult)) {
            $paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->qualificationpaginatorresult,$lintpage,$lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($statusTplList,$lintpage,$lintpagecount);
        }
        
        $paginator->setItemCountPerPage(10);
        
        $this->view->paginator = $paginator;
        $this->view->statusTplList = $statusTplList;
    }  
    
    public function addStatusTemplateAction(){
        $stsId = $this->_getParam('stsId', null);
        $type = $this->_getParam('type', null);
        $title = $this->_getParam('title', null);
        $progscheme_id = $this->_getParam('id',null);
        $stdCtgy = $this->_getParam('stdCtgy',null);
        $this->view->type = $type;
        $this->view->title=$this->view->translate("Add Status Template");
        $addStatusTplForm = new Application_Form_AddStatusTemplate(array('type'=>$type, 'title'=>$title));
        $populate['stp_tplType'] = $type;
        $addStatusTplForm->populate($populate);
        $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'application-initial-configuration', 'action'=>'edit-status', 'id'=>$progscheme_id, 'stdCtgy'=>$stdCtgy, 'sts_id'=>$stsId),'default',true);
    
        $getUserIdentity = $this->auth->getIdentity();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            if ($addStatusTplForm->isValid($formData)) {
                //var_dump($formData); exit;
                $data = array(
                    'stp_tplId'=>$formData['stp_tplId'],
                    'stp_tplType'=>$type,
                    'stp_title'=>$formData['stp_title'],
                    'contentMy'=>$formData['contentMy'],
                    'contentEng'=>$formData['contentEng'],
                    'stp_addDate' => date('Y-m-d H:i:s'),
                    'stp_addBy' => $getUserIdentity->id
                );
                
                $this->stpModel->insert($data);
                
                $stp_Id = $this->stpModel->fnGetMaxStpId();
                
                if ($type == 604){
                    $stsData = array(
                        'sts_emailNotification'=>$stp_Id
                    );
                }else{
                    $stsData = array(
                        'sts_message'=>$stp_Id
                    );
                }
                $this->statusModel->update($stsData, 'sts_Id = '.$stsId);
                
                //attachment part
                if (isset($formData['attach']) && isset($formData['template'])){
                    if (count($formData['attach']) > 0 && count($formData['template']) > 0){
                        if (count($formData['attach']) == count($formData['template'])){
                            $stpId = $this->stpModel->fnGetMaxStpId();
                            $i=0;
                            foreach ($formData['attach'] as $attachLoop){
                                if ($attachLoop != "" && $formData['template'][$i] != ""){
                                    $dataAttach = array(
                                        'sth_stpId'=>$stpId,
                                        'sth_tplId'=>$formData['template'][$i],
                                        'sth_type'=>$attachLoop,
                                        'sth_addDate' => date('Y-m-d H:i:s'),
                                        'sth_addBy' => $getUserIdentity->id
                                    );
                                    $this->sthModel->insert($dataAttach);
                                }
                                $i++;
                            }
                        }
                    }    
                }
                
                //redirect
                $this->_redirect($this->view->backURL);
            }
        }
        
        $this->view->addStatusTplForm = $addStatusTplForm;
    }
    
    public function editStatusTemplateAction(){
        $stpId = $this->_getParam('stpId', null);
        $stsId = $this->_getParam('stsId', null);
        $type = $this->_getParam('type', null);
        $this->view->type = $type;
        $progscheme_id = $this->_getParam('id',null);
        $stdCtgy = $this->_getParam('stdCtgy',null);
        $this->view->title=$this->view->translate("Edit Status Template");
        $statusTplList = $this->stpModel->fnGetStatusTemplateListById($stpId);
        $addStatusTplForm = new Application_Form_AddStatusTemplate();
        $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'application-initial-configuration', 'action'=>'edit-status', 'id'=>$progscheme_id, 'stdCtgy'=>$stdCtgy, 'sts_id'=>$stsId),'default',true);
        
        $attachmentList = $this->sthModel->fnGetStatusAttachListById($stpId);
        
        $getUserIdentity = $this->auth->getIdentity();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($addStatusTplForm->isValid($formData)) {
                
                $data = array(
                    'stp_tplId'=>$formData['stp_tplId'],
                    //'stp_tplType'=>$formData['stp_tplType'],
                    'stp_title'=>$formData['stp_title'],
                    'contentMy'=>$formData['contentMy'],
                    'contentEng'=>$formData['contentEng'],
                    'stp_modDate' => date('Y-m-d H:i:s'),
                    'stp_modBy' => $getUserIdentity->id
                );
                
                $this->stpModel->update($data, 'stp_Id = '.$stpId);
                
                //attachment part
                if (isset($formData['attach']) && isset($formData['template'])){
                    if (count($formData['attach']) > 0 && count($formData['template']) > 0){
                        if (count($formData['attach']) == count($formData['template'])){
                            $i=0;
                            foreach ($formData['attach'] as $attachLoop){
                                if ($attachLoop != "" && $formData['template'][$i] != ""){
                                    $dataAttach = array(
                                        'sth_stpId'=>$stpId,
                                        'sth_tplId'=>$formData['template'][$i],
                                        'sth_type'=>$attachLoop,
                                        'sth_addDate' => date('Y-m-d H:i:s'),
                                        'sth_addBy' => $getUserIdentity->id
                                    );
                                    $this->sthModel->insert($dataAttach);
                                }
                                $i++;
                            }
                        }
                    }    
                }
                
                if (isset($formData['sth_Id']) && count($formData['sth_Id']) > 0){
                    foreach ($formData['sth_Id'] as $deleteAttachmentLoop){
                        //$this->sthModel->delete('sth_Id = '.$deleteAttachmentLoop);

                        $dataAttach = array(
                            'sth_stpId'=>0,
                            'sth_tplId'=>0,
                        );
                        $this->sthModel->updateStatusAttachment($dataAttach, $deleteAttachmentLoop);
                    }
                }
                
                //redirect
                $this->_redirect($this->view->backURL);
            }else{
                $addStatusTplForm->populate($statusTplList);
            }
        }else{
            $addStatusTplForm->populate($statusTplList);
        }
        
        $this->view->addStatusTplForm = $addStatusTplForm;
        $this->view->attachmentList = $attachmentList;
    }
    
    public function deleteStatusTemplateAction(){
        $stpId = $this->_getParam('stpId', null);
        $stsId = $this->_getParam('stsId', null);
        $type = $this->_getParam('type', null);
        //$this->view->type = $type;
        $progscheme_id = $this->_getParam('id',null);
        $stdCtgy = $this->_getParam('stdCtgy',null);
        $this->view->backURL = $this->view->url(array('module'=>'application','controller'=>'application-initial-configuration', 'action'=>'edit-status', 'id'=>$progscheme_id, 'stdCtgy'=>$stdCtgy, 'sts_id'=>$stsId),'default',true);

        if ($stpId != null){
            $this->stpModel->delete('stp_Id = '.$stpId);
            $this->sthModel->delete('sth_stpId = '.$stpId);
            
            if ($type == 604){
                $stsData = array(
                    'sts_emailNotification'=>0
                );
            }else{
                $stsData = array(
                    'sts_message'=>0
                );
            }
            $this->statusModel->update($stsData, 'sts_Id = '.$stsId);
        }
        
        //redirect
        $this->_redirect($this->view->backURL);
    }
    
    /*
     * ajax section
     */
    public function getTplAction()
    {
	if ($this->_request->isPost())
        {
            $formData = $this->getRequest()->getPost();
			
            $template_content = $this->tplModel->getTemplateContent($formData['tpl_id']);
            
            $output = array();

            if (empty($template_content) )
            {
                $output = array(); //error maybe?	
            }else{
                $i=0;
                foreach($template_content as $template_Loop){
                    $output[$i] = array('content' => $template_Loop['tpl_content'], 'tpl_type' => $template_Loop['tpl_type']);
                    $i++;
                }
            }
            echo Zend_Json::encode($output);
	}
        exit();
    }
    
    public function generateAttachmentFormAction(){
        $attachmentCount = $this->_getParam('id', null);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $attachmentType = $this->defModel->getDataByType(123);
        $tplList = $this->lobjAppConfig->fnGetTemplateList();
        
        $arr = array();
        $i=0;
        while ($i < $attachmentCount){
            $arr[$i]['attach'] = $attachmentType;
            $arr[$i]['template'] = $tplList;
            $i++;
        }
        
        $json = Zend_Json::encode($arr);
        //var_dump($arr);
	echo $json;
	exit();
    }
}
?>