<?php 
class Application_GenerateResultController extends Zend_Controller_Action
{
	private $_DbObj;
	private $_config;
	
	public function init(){
		
		$sis_session = new Zend_Session_Namespace('sis');
		$configDb = new GeneralSetup_Model_DbTable_Initialconfiguration();
		$this->_config = $configDb->fnGetInitialConfigDetails($sis_session->idUniversity);
		
		//$this->_DbObj = new Application_Model_DbTable_Placementtest();
	}
	
	public function indexAction() {
		//title
    	$this->view->title= $this->view->translate("Generate Result - Schedule List");
    	
    	$form = new Application_Form_PlacementTestSearch();
    	
    	$scheduleDb = new App_Model_Application_DbTable_PlacementTestSchedule();
    	
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			$form->populate($formData);
			
			$cond = array(
				'location' => $formData['location']
			);
			
			//intake
    		if( isset($formData['intake_id']) && $formData['intake_id']!="" ){
				$intakeDb = new App_Model_Record_DbTable_Intake();
				$intakeData = $intakeDb->getData($formData['intake_id']);

				$cond['dt_from'] = date('Y-m-d', strtotime($intakeData['ApplicationStartDate']));
				$cond['dt_to'] = date('Y-m-d', strtotime($intakeData['ApplicationEndDate']));
			}
			//period
			if( isset($formData['period_id']) && $formData['period_id']!="" ){
				$periodDb = new App_Model_Record_DbTable_AcademicPeriod();
				$periodData = $periodDb->getData($formData['period_id']);
				
				$cond['dt_from'] = date('d-n-Y', strtotime('01-'.$periodData['ap_month']."-".$periodData['ap_year']));
				$cond['dt_to'] = date('t-m-Y', strtotime($cond['dt_from']));
			}
			
			//get schedule by search
			$list = $scheduleDb->search($cond);
			
			$this->view->scheduleList = $list; 
    	}else{
    		$list = $scheduleDb->getData();
    	}
    	
    	$this->view->placementTestList = $list;
    	
    	$this->view->form = $form;
    	
	}
	
	public function roomDetailAction(){
		$schedule_id = $this->_getParam('scid', 0);
		$this->view->schedule_id = $schedule_id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//title
    	$this->view->title=$this->view->translate("Generate Result - By Schedule");
    	
    	//schedule data
    	$placementTestScheduleDb = new App_Model_Application_DbTable_PlacementTestSchedule();
    	$scheduleData = $placementTestScheduleDb->getScheduleInfo($schedule_id);
    	$this->view->schedule_data = $scheduleData;
    	
    	//placement test data
    	$placementTestDb = new App_Model_Application_DbTable_PlacementTest();
    	$placementTestData = $placementTestDb->getDataFromCode($scheduleData['aps_placement_code']);

		//generate result
		//list of applicant
	    $select = $db ->select()
					->from(array('apt'=>'applicant_ptest'))
					->where("apt.apt_aps_id='".$schedule_id."'");														
       
        $row = $db->fetchAll($select);
        
        $applicantList = $row;
        
        if($this->_getparam("generateAll","0")=="1"){
        	//list of applicant
		    $select = $db ->select()
						->from(array('apt'=>'applicant_ptest'))
						->where("apt.apt_aps_id='".$schedule_id."'");														
	       
	        $row = $db->fetchAll($select);
	        
	        $applicantList = $row;
	                
       		foreach ($applicantList as $key=>$applicant){
        	
	        	$txn_id = $applicant['apt_at_trans_id'];
	        	
	        	$select2 = $db ->select()
						->from(array('ap'=>'applicant_program'))
						->join(array('p'=>'tbl_program'), 'p.ProgramCode = ap.ap_prog_code', array('ptest_room_type','ArabicName'))
						->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege = p.IdCollege', array('ShortName'))
						->where("ap.ap_at_trans_id='".$txn_id."'")
						->Order("ap.ap_preference asc");
				$frow2 = $db->fetchAll($select2);
	
				
	       		//check tpa or usm. if tpa do not update applicant program usm mark
				if($placementTestData['aph_testtype']==0 && $this->_getparam("generateAll","0")=="1"){				
	            	$compDB2 = new App_Model_Application_DbTable_ApplicantCompMark();
	            	$aprogDB = new App_Model_Application_DbTable_ApplicantProgram();
	            	
			        foreach ($frow2 as $key=>$prog){
			       		$compProg2 = $compDB2->getCompByProgram($prog["ap_prog_code"],$scheduleData['aps_placement_code'],$txn_id,$prog["ap_id"]);
			        } 
	       		} 
       		}
       		$this->_redirect($this->view->url(array('module'=>'application','controller'=>'generate-result', 'action'=>'room-detail','scid'=>$schedule_id),'default',true));     	
        }
        
    	//get room data 
		$roomDb = new App_Model_Application_DbTable_PlacementTestRoom();
		$roomList = $roomDb->getLocationVenue($scheduleData['al_id']);
    	
    	
    	//inject applicant count
    	$ptestDb = new App_Model_Application_DbTable_ApplicantPtest();
    	
    	$pthDB = new App_Model_Application_DbTable_PlacementTest();
    	$aph = $pthDB->getDataFromCode($scheduleData["aps_placement_code"]);
		$aph_id=$aph["aph_id"];
		$this->view->testtype=$aph["aph_testtype"];
		
    	foreach ($roomList as $key=>$room){
    		
    		//inject array with 'theory' if not have theory type
    		if( sizeof($room['type'])>1 ){
    			array_unshift($roomList[$key]['type'], array('art_test_type'=>1, 'test_name'=>'THEORY'));
    		}
    		
    		//applicant count
    		$roomList[$key]['applicant_count'] = $ptestDb->getApplicantCount($scheduleData['aps_id'], $room['av_id']);	
    		
    		//remove from array if no applicant
    		if($roomList[$key]['applicant_count'] == 0 || $roomList[$key]['applicant_count'] == null){
    			unset($roomList[$key]);
    		}else{
    			
    		
    			if( sizeof($room['type'])>1 ){

	    			//list of applicant
				    $select = $db ->select()
								->from(array('apt'=>'applicant_ptest'))
								->where("apt.apt_aps_id='".$scheduleData['aps_id']."'")
								->where("apt.apt_room_id='".$room['av_id']."'");														
			       
			        $row = $db->fetchAll($select);
			        
			        $roomList[$key]['applicant_list'] = $row;
			        
			        //loop budak utk cari programme
			        foreach ($roomList[$key]['applicant_list'] as $key2=>$applicant){
			        	
			        	$txn_id = $applicant['apt_at_trans_id'];
			        	
			        	$select2 = $db ->select()
								->from(array('ap'=>'applicant_program'),array())
								->join(array('p'=>'tbl_program'), 'p.ProgramCode = ap.ap_prog_code', array('ptest_room_type'))
								->where("ap.ap_at_trans_id='".$txn_id."'");
																						
			       		$row2 = $db->fetchAll($select2);
			       		
			       		$roomList[$key]['applicant_list'][$key2]['program'] = $row2;
			       		
			       		//get applicant's room type from all programs
			       		if( !isset($row2[1]) ){
			       			$row2[1]['ptest_room_type'] = 0;
			       		}
			       		
			        	$select3 = $db ->select()
								->from(array('ara'=>'appl_room_assign'),array('ara_room'))
								->where("ara.ara_program1='".$row2[0]['ptest_room_type']."'")
								->where("ara.ara_program2='".$row2[1]['ptest_room_type']."'");
			       		
						$row3 = $db->fetchRow($select3);
							
						$roomList[$key]['applicant_list'][$key2]['room_type'] = $row3['ara_room'];
						
						//assign to array which have key as room type
						$roomList[$key]['room_type'][$row3['ara_room']][] = $roomList[$key]['applicant_list'][$key2];
			        }
    				
    			}
	    		
    		}
    	}
    	
    	$roomList = array_values($roomList);
    	
		$this->view->roomList = $roomList;
		
	}

public function scheduleDetailAction(){
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		setlocale (LC_ALL, $locale);
		
		$schedule_id = $this->_getParam('scid', 0);
		$this->view->scid = $schedule_id;
		
		$room_id = $this->_getParam('rid', 0);
		$this->view->rid = $room_id;

		
		$this->view->title=$this->view->translate("Generate Result - By Room");
		
		//schedule data
    	$placementTestScheduleDb = new App_Model_Application_DbTable_PlacementTestSchedule();
    	$scheduleData = $placementTestScheduleDb->getScheduleInfo($schedule_id);
    	$this->view->schedule_data = $scheduleData;
    	
    	//get room data 
		$roomDb = new App_Model_Application_DbTable_PlacementTestRoom();
		$roomData = $roomDb->getData($room_id);
		$this->view->room = $roomData;

		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//list of applicant
	    $select = $db ->select()
					->from(array('apt'=>'applicant_ptest'))
					->where("apt.apt_aps_id='".$schedule_id."'")
					->where("apt.apt_room_id = ?",(int)$room_id);														
       
        $row = $db->fetchAll($select);
        
        $applicantList = $row;
        
        if($this->_getparam("generateAll","0")=="1"){
       		foreach ($applicantList as $key=>$applicant){
        	
	        	$txn_id = $applicant['apt_at_trans_id'];
	        	
	        	$select2 = $db ->select()
						->from(array('ap'=>'applicant_program'))
						->join(array('p'=>'tbl_program'), 'p.ProgramCode = ap.ap_prog_code', array('ptest_room_type','ArabicName'))
						->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege = p.IdCollege', array('ShortName'))
						->where("ap.ap_at_trans_id='".$txn_id."'")
						->Order("ap.ap_preference asc");
				$frow2 = $db->fetchAll($select2);
	
				
	       		
				if($this->_getparam("generateAll","0")=="1"){				
	            	$compDB2 = new App_Model_Application_DbTable_ApplicantCompMark();
	            	$aprogDB = new App_Model_Application_DbTable_ApplicantProgram();
	            	
			        foreach ($frow2 as $key=>$prog){
			        	if(trim($prog["ap_prog_code"])!=""){	
				       		$compProg = $compDB2->getCompByProgram($prog["ap_prog_code"],$scheduleData['aps_placement_code'],$txn_id,$prog["ap_id"]);
				       		$row3[$key]["res"]=$compProg;
			        	} 
			        } 
	       		} 
       		}
       		$this->_redirect($this->view->url(array('module'=>'application','controller'=>'generate-result', 'action'=>'schedule-detail','scid'=>$schedule_id, 'rid'=>$room_id),'default',true));     	
        }
        //loop budak utk cari programme
        foreach ($applicantList as $key=>$applicant){
        	
        	$txn_id = $applicant['apt_at_trans_id'];
        	
        	$select2 = $db ->select()
					->from(array('ap'=>'applicant_program'))
					->join(array('p'=>'tbl_program'), 'p.ProgramCode = ap.ap_prog_code', array('ptest_room_type','ArabicName'))
					->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege = p.IdCollege', array('ShortName'))
					->where("ap.ap_at_trans_id='".$txn_id."'")
					->Order("ap.ap_preference asc");
			$row2 = $db->fetchAll($select2);

       		if($row2){
       			$applicantList[$key]['program'] = $row2;
       		}else{
       			$applicantList[$key]['program'] = null;
       			echo "No Programme";
       			exit;
       		}
       		
       		//get applicant's room type from all programs
       		if( !isset($row2[1]) ){
       			$row2[1]['ptest_room_type'] = 0;
       		}
       		
        	$select3 = $db ->select()
					->from(array('ara'=>'appl_room_assign'),array('ara_room'))
					->where("ara.ara_program1='".$row2[0]['ptest_room_type']."'")
					->where("ara.ara_program2='".$row2[1]['ptest_room_type']."'");
       		
			$row3 = $db->fetchRow($select3);
				
			$applicantList[$key]['room_type'] = $row3['ara_room'];
			
			
        }
        
        //loop applicant and inject profile and transaction data
        foreach ($applicantList as $key=>$applicant){
        	
        	//get transaction data
        	$select = $db ->select()
					->from(array('at'=>'applicant_transaction'))
					->where("at.at_trans_id = '".$applicant['apt_at_trans_id']."'");
																			
       		$row = $db->fetchRow($select);
       		$applicantList[$key]['transaction'] = $row;
        	
        	//get profile data
        	$select2 = $db ->select()
					->from(array('ap'=>'applicant_profile'))
					->where("ap.appl_id = '".$row['at_appl_id']."'");
																			
       		$row2 = $db->fetchRow($select2);
       		$applicantList[$key]['profile'] = $row2;
        	
        }
        
        $this->view->applicant_list = $applicantList;

	}
	
	public function markDetailAction(){
		
		$this->view->title=$this->view->translate("Generate Result - By Applicant");
		
		$txn_id = $this->_getParam('txnid', null);
		$this->view->txn_id = $txn_id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//ptest data
		$select = $db ->select()
					->from(array('apt'=>'applicant_ptest'))
					->where("apt.apt_at_trans_id = '".$txn_id."'");
					
		$ptest_data = $db->fetchRow($select);
		$this->view->ptest = $ptest_data;
		
		//schedule data
    	$placementTestScheduleDb = new App_Model_Application_DbTable_PlacementTestSchedule();
    	$scheduleData = $placementTestScheduleDb->getScheduleInfo($ptest_data['apt_aps_id']);
    	$this->view->schedule_data = $scheduleData;
    	
    	//get transaction data
        $select = $db ->select()
				->from(array('at'=>'applicant_transaction'))
				->where("at.at_trans_id = '".$txn_id."'");
																		
       	$row = $db->fetchRow($select);
       	$txnData = $row;
       		
    	//profile data
    	//get profile data
        $select2 = $db ->select()
				->from(array('ap'=>'applicant_profile'))
				->where("ap.appl_id = '".$txnData['at_appl_id']."'");
																		
       	$row2 = $db->fetchRow($select2);
       	$this->view->profile = $row2;

        $select2 = $db ->select()
				->from(array('ap'=>'applicant_program'))
				->join(array('p'=>'tbl_program'), 'p.ProgramCode = ap.ap_prog_code', array('ptest_room_type','ArabicName'))
				->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege = p.IdCollege', array('ShortName'))
				->where("ap.ap_at_trans_id='".$txn_id."'")
				->Order("ap.ap_preference asc");
				
																		
       	$row3 = $db->fetchAll($select2);
       	
       	$compDB = new App_Model_Application_DbTable_ApplicantCompMark();
       	foreach ($row3 as $key=>$prog){
/*     	echo "<pre>";
       	print_r($prog);
       	echo "</pre>"; */      	
	       	if(trim($prog["ap_prog_code"])!=""){	
	       		$compProg = $compDB->getCompByProgram($prog["ap_prog_code"],$scheduleData['aps_placement_code'],$txn_id,$prog["ap_id"]);
	       		$row3[$key]["res"]=$compProg;
	       		//echo "s";
	       	}
	       	
       	}
       //	exit;
       	$this->view->compProg=$row3;
/*       	echo "<pre>";
       	print_r($row3);
       	echo "</pre>";*/
       		      	
 		$com_select = $db ->select()
					->from(array('apd'=>'appl_placement_detl'))
					->joinLeft(array('ac' => 'appl_component'),'ac.ac_comp_code = apd.apd_comp_code')
					->joinLeft(array('apcm'=>'applicant_ptest_comp_mark'), 'apcm.apcm_at_trans_id = '.$txn_id.' and apcm.apcm_apd_id = apd.apd_id')
					->where("apd.apd_placement_code = '".$ptest_data['apt_ptest_code']."'");
       	
					//echo $com_select;exit;
		$comp_row = $db->fetchAll($com_select);
		$this->view->compDetail = $comp_row;	      	
       	
	}
public function scheduleDetailTpaAction(){
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		setlocale (LC_ALL, $locale);
		
		$schedule_id = $this->_getParam('scid', 0);
		$this->view->scid = $schedule_id;
		
		$room_id = $this->_getParam('rid', 0);
		$this->view->rid = $room_id;

		
		$this->view->title=$this->view->translate("Generate Result - By Room");
		
		//schedule data
    	$placementTestScheduleDb = new App_Model_Application_DbTable_PlacementTestSchedule();
    	$scheduleData = $placementTestScheduleDb->getScheduleInfo($schedule_id);
    	$this->view->schedule_data = $scheduleData;
    	
    	//get room data 
		$roomDb = new App_Model_Application_DbTable_PlacementTestRoom();
		$roomData = $roomDb->getData($room_id);
		$this->view->room = $roomData;

		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//list of applicant
	    $select = $db ->select()
					->from(array('apt'=>'applicant_ptest'))
					->where("apt.apt_aps_id='".$schedule_id."'")
					->where("apt.apt_room_id = ?",(int)$room_id);														
       
        $row = $db->fetchAll($select);
        
        $applicantList = $row;
        
        if($this->_getparam("generateAll","0")=="1"){
       		foreach ($applicantList as $key=>$applicant){
        	
	        	$txn_id = $applicant['apt_at_trans_id'];
	        	
	        	$select2 = $db ->select()
						->from(array('ap'=>'applicant_program'))
						->join(array('p'=>'tbl_program'), 'p.ProgramCode = ap.ap_prog_code', array('ptest_room_type','ArabicName'))
						->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege = p.IdCollege', array('ShortName'))
						->where("ap.ap_at_trans_id='".$txn_id."'")
						->Order("ap.ap_preference asc");
				$frow2 = $db->fetchAll($select2);
	
				
	       		
				if($this->_getparam("generateAll","0")=="1"){				
	            	$compDB2 = new App_Model_Application_DbTable_ApplicantCompMark();
	            	$aprogDB = new App_Model_Application_DbTable_ApplicantProgram();
	            	
			        foreach ($frow2 as $key=>$prog){
			        	if(trim($prog["ap_prog_code"])!=""){	
				       		$compProg = $compDB2->getCompByProgram($prog["ap_prog_code"],$scheduleData['aps_placement_code'],$txn_id,$prog["ap_id"]);
				       		$row3[$key]["res"]=$compProg;
			        	} 
			        } 
	       		} 
       		}
       		$this->_redirect($this->view->url(array('module'=>'application','controller'=>'generate-result', 'action'=>'schedule-detail','scid'=>$schedule_id, 'rid'=>$room_id),'default',true));     	
        }
        //loop budak utk cari programme
        foreach ($applicantList as $key=>$applicant){
        	
        	$txn_id = $applicant['apt_at_trans_id'];
        	
        	$select2 = $db ->select()
					->from(array('ap'=>'applicant_program'))
					->join(array('p'=>'tbl_program'), 'p.ProgramCode = ap.ap_prog_code', array('ptest_room_type','ArabicName'))
					->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege = p.IdCollege', array('ShortName'))
					->where("ap.ap_at_trans_id='".$txn_id."'")
					->Order("ap.ap_preference asc");
			$row2 = $db->fetchAll($select2);

       		if($row2){
       			$applicantList[$key]['program'] = $row2;
       		}else{
       			$applicantList[$key]['program'] = null;
       			echo "No Programme";
       			exit;
       		}
       		
       		//get applicant's room type from all programs
       		if( !isset($row2[1]) ){
       			$row2[1]['ptest_room_type'] = 0;
       		}
       		
        	$select3 = $db ->select()
					->from(array('ara'=>'appl_room_assign'),array('ara_room'))
					->where("ara.ara_program1='".$row2[0]['ptest_room_type']."'")
					->where("ara.ara_program2='".$row2[1]['ptest_room_type']."'");
       		
			$row3 = $db->fetchRow($select3);
				
			$applicantList[$key]['room_type'] = $row3['ara_room'];
			
			
        }
        
        //loop applicant and inject profile and transaction data
        foreach ($applicantList as $key=>$applicant){
        	
        	//get transaction data
        	$select = $db ->select()
					->from(array('at'=>'applicant_transaction'))
					->where("at.at_trans_id = '".$applicant['apt_at_trans_id']."'");
																			
       		$row = $db->fetchRow($select);
       		$applicantList[$key]['transaction'] = $row;
        	
        	//get profile data
        	$select2 = $db ->select()
					->from(array('ap'=>'applicant_profile'))
					->where("ap.appl_id = '".$row['at_appl_id']."'");
																			
       		$row2 = $db->fetchRow($select2);
       		$applicantList[$key]['profile'] = $row2;
        	
        }
        
        $this->view->applicant_list = $applicantList;

	}
	
}
?>