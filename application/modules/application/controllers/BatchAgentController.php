<?php
/**
 * @author Ajaque Rahman
 * @version 1.0
 */

class Application_BatchAgentController extends Zend_Controller_Action {
	
	private $_DbObj;
	private $_config;
	
	public function init(){
		
/*		$sis_session = new Zend_Session_Namespace('sis');
		$configDb = new GeneralSetup_Model_DbTable_Initialconfiguration();
		$this->_config = $configDb->fnGetInitialConfigDetails($sis_session->idUniversity);*/
		
		
	}
	
	public function indexAction() {
		//title
    	$this->view->title= $this->view->translate("Batch Application : By OMR");
    	$this->view->scid=$this->_getparam("scid",0);
    	$form = new Application_Form_BatchAgent();
    	
    	$form_csv = new Application_Form_BatchCsvProfileUpload(array('scidval'=>$this->view->scid));
    	$this->view->form_csv = $form_csv;
    	
  		$format = new Zend_Form_Element_hidden('scid');
        $format->setLabel('Start Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue($this->_getparam("scid",null));
		$form->addElement($format);	  	
    	$this->view->form = $form;

    	if($this->_getparam("upemail",0)==1){
    			$db = Zend_Db_Table::getDefaultAdapter();
    		    $select = $db ->select()
					->from(array('apt'=>'applicant_ptest'),array())
					->join(array('at'=>'applicant_transaction'),'at.at_trans_id = apt.apt_at_trans_id')
					->where("apt.apt_aps_id='".$this->_getparam("scid",0)."'");														
       
				//echo $select;
		        $row = $db->fetchAll($select);		        
		        $applicantList = $row;
		        
		        foreach($applicantList as $applicant){
		        	$sql = $db ->select()
					->from(array('re'=>'raw_email'),array('email'))					
					->where("re.appid='".$applicant["at_pes_id"]."'");
					
					$row2 = $db->fetchRow($sql);
					
					if($row2){
						$scheck="select appl_id,appl_email from applicant_profile where appl_email='".$row2["email"]."'";
						$exist=$db->fetchRow($scheck);
						if($exist){
							if($applicant["at_appl_id"]!=$exist["appl_id"]){
								$sqlu = "update applicant_profile set appl_email='".$row2["email"].".problem' where appl_id='".$exist["appl_id"]."'";
								//echo $sql."<hr>";
								$db->query($sqlu);
							}
						}
						$sql = "update applicant_profile set appl_email='".$row2["email"]."' where appl_id='".$applicant["at_appl_id"]."'";
						echo $applicant["at_appl_id"]." : ".$row2["email"]."<br>";
						if(trim($row2["email"])==""){
							echo "No Email Found (No Value)<br>";
						}else{
							echo $sql;
							$db->query($sql);
						}
					}else{
						echo "No Email Found:".$applicant["at_pes_id"]."<br>";
					}
					
		        }
		        echo "<b>Email Updated</b>";
    	}
	}
	
	public function uploadOmrAction(){
		echo "Please Remove Exit in coding";
		exit;
		
		//title
		$adaproblemprogram=0;
    	$this->view->title= $this->view->translate("Batch Application - By OMR ");
    	$badb = new Application_Model_DbTable_Batchagent();	
    	
    	$form = new Application_Form_BatchAgent();
    	
    	if ($this->_request->isPost()) {
    		//BETULKAN PREFERENCE PROGRAM ORDER
    		if($adaproblemprogram=="1"){
	    		exit;
	    		$formData = $this->_request->getPost();
	       		//print_r($formData);
	            // success - do something with the uploaded file
	            $uploadedData = $form->getValues();
	            $fullFilePath = $form->file->getFileName();
	            
	            //echo $fullFilePath;
				$file = fopen($fullFilePath, "r") or exit("Unable to open file!");
					//Output a line of the file until the end is reached
				$i=0;
				$data = array();
				
				while(!feof($file)){
					$line_data = fgets($file); 
					
					if(substr($line_data,40,8)!=""){
						$at_pes_id=substr($line_data,40,8);
	
						$data[$i]["applicantID"] = substr($line_data,40,8);
						$data[$i]["programcode1"] = substr($line_data,48,4);
						$data[$i]["programcode2"] = substr($line_data,52,4);	
	
						$i++;
											
					}
					
					
				}     
				
				fclose($file);
			
				
				
				//loop utk dapatkan transation id
				$db = Zend_Db_Table::getDefaultAdapter();
				foreach ($data as $key=>$data_app){
					$select2 = $db ->select()
								->from(array('ap'=>'applicant_ptest'))
								->where("ap.apt_bill_no = '".$data_app['applicantID']."'");
					
					$row2 = $db->fetchRow($select2);
					
					$data[$key]['txn_id'] = $row2['apt_at_trans_id'];
				}
				
				//update program seq
				foreach ($data as $key=>$data_app){
					
					//1st preference
					$where = array();
					$where[] = "ap_at_trans_id = '".$data_app['txn_id']."'";
					$where[] = "ap_prog_code = '".$data_app['programcode1']."'";
					$db->update('applicant_program', array('ap_preference'=>'1'), $where);
					
					//2nd preference
					$where2 = array();
					$where2[] = "ap_at_trans_id = '".$data_app['txn_id']."'";
					$where2[] = "ap_prog_code = '".$data_app['programcode2']."'";
					if(isset($data_app['programcode2']) && $data_app['programcode2']!=""){
						$db->update('applicant_program', array('ap_preference'=>'2'), $where2);
					}
				}
				
				
				echo "<pre>";
				print_r($data);
				echo "</pre>";
				
				exit;
				
				/*** END BETUL KAN PREFERENCE ***/
    		}
       		$formData = $this->_request->getPost();
       		//echo $formData["scid"];
       		if($formData["scid"]==""){
       			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'batch-agent', 'action'=>'schedule-list'),'default',true));
       		}

            // success - do something with the uploaded file
            $uploadedData = $form->getValues();
            $fullFilePath = $form->file->getFileName();
            
            echo $fullFilePath;exit;
			$file = fopen($fullFilePath, "r") or exit("Unable to open file!");
				//Output a line of the file until the end is reached
			$i=0;
			$data = array();
			$transactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
			$profileDB  = new App_Model_Application_DbTable_ApplicantProfile();
			$familyDB  = new App_Model_Application_DbTable_ApplicantFamily();
			$aprogDB  = new App_Model_Application_DbTable_ApplicantProgram();
			$appeduDB = new App_Model_Application_DbTable_ApplicantEducation();
			$ptestDB = new App_Model_Application_DbTable_ApplicantPtest();
			while(!feof($file)){
				$line_data = fgets($file); 
				
				if(substr($line_data,40,8)!=""){
					$at_pes_id=substr($line_data,40,8);
			     	
				    /*$appl_info=$transactionDB->uniqueApplicantid( $at_pes_id );
					//for app profile table
					if($appl_info){
						$data[$i]["status"]="Not Valid";
					}else{
						$data[$i]["status"]="Saved";
					}*/
					$data[$i]["appl_fname"] = substr($line_data,97,25);
					$data[$i]["appl_gender"] = substr($line_data,186,1);
					$data[$i]["appl_gender"] = $badb->getlookupID("SEX",$data[$i]["appl_gender"]);
					$data[$i]["appl_religion"] = substr($line_data,187,1);//sis_setup_detail
					$data[$i]["appl_religion"] = $badb->getlookupID("RELIGION",$data[$i]["appl_religion"]);
					$data[$i]["appl_dob"] = substr($line_data,79,6);
					$data[$i]["citycode_dob"] = substr($line_data,87,3);		
					$data[$i]["appl_nationality"] = substr($line_data,93,1);
					
					$data[$i]["appl_phone_home"] = substr($line_data,147,6);
					$data[$i]["appl_phone_mobile"] = substr($line_data,62,12);
					
					$data[$i]["appl_address1"] = substr($line_data,122,25);
					$data[$i]["appl_postcode"] = substr($line_data,180,5);					
					$data[$i]["cityname"] = substr($line_data,153,27)	;
					$data[$i]["appl_state"] = substr($line_data,74,2);
                                        if($data[$i]["appl_state"]=="") $data[$i]["appl_state"]=1;				
					$data[$i]["appl_state"] = $badb->getstateID($data[$i]["appl_state"]);
					 if($data[$i]["appl_state"]=="") $data[$i]["appl_state"]=1;

                                        $data[$i]["appl_city"] = substr($line_data,76,3);	
					$data[$i]["appl_city"] = $badb->getcityID($data[$i]["appl_state"],$data[$i]["appl_city"]);
										
					//for app family table
					$data[$i]["father_af_name"] = substr($line_data,237,25);										
					$data[$i]["father_af_address1"] = substr($line_data,287,25);
					$data[$i]["father_af_postcode"] = substr($line_data,324,5);
					$data[$i]["father_af_state"] = substr($line_data,332,2);
					$data[$i]["father_af_state"] = $badb->getstateID($data[$i]["father_af_state"]);
					$data[$i]["father_af_city"] = substr($line_data,329,3);
					$data[$i]["father_af_city"] = $badb->getcityID($data[$i]["father_af_state"],$data[$i]["father_af_city"]);
					
					$data[$i]["father_af_family_condition"] = substr($line_data,198,1);
					$data[$i]["father_af_family_condition"] = $badb->getlookupID("PARENTCON",$data[$i]["father_af_family_condition"]);	
					$data[$i]["father_af_education_level"] = substr($line_data,201,1); //sis_setup_dtl
					$data[$i]["father_af_education_level"] = $badb->getlookupID("EDULEVEL",$data[$i]["father_af_education_level"]);					
					$data[$i]["father_af_job"] = substr($line_data,203,1); //tbl familyjob					
					$data[$i]["father_af_phone"] = substr($line_data,312,6);
										

					$data[$i]["mother_af_name"] = substr($line_data,262,25);
					$data[$i]["mother_af_family_condition"] = substr($line_data,199,1);
					$data[$i]["mother_af_family_condition"] = $badb->getlookupID("PARENTCON",$data[$i]["mother_af_family_condition"]);
					$data[$i]["mother_af_education_level"] = substr($line_data,202,1);
					$data[$i]["mother_af_education_level"] = $badb->getlookupID("EDULEVEL",$data[$i]["mother_af_education_level"]);	
					$data[$i]["mother_af_job"] = substr($line_data,204,1);	

					
 
										
					//education
					$data[$i]["hsyearpass"] = substr($line_data,206,6);
					$data[$i]["ae_institution"] = substr($line_data,223,8);		//schoolmaster//	
					$data[$i]["ae_institution"] = $badb->gethsID($data[$i]["ae_institution"]);		
					$data[$i]["ae_discipline_code"] = substr($line_data,212,3);
					$data[$i]["hsprovince"] = substr($line_data,215,2);
					$data[$i]["hsprovince"] = $badb->getstateID($data[$i]["hsprovince"]);
					$data[$i]["hscitycode"] = substr($line_data,217,3);
					$data[$i]["hscitycode"] = $badb->getcityID($data[$i]["hsprovince"],$data[$i]["hscitycode"]);
				
					//for txn table
					$data[$i]["applicantID"] = substr($line_data,40,8);
					$data[$i]["programcode1"] = substr($line_data,48,4);
					$data[$i]["programcode2"] = substr($line_data,52,4);	

					$i++;
										
				}
				
				
			}    

			fclose($file);
			foreach ($data as $appl){
				
					$checkexist=$badb->checkApplicantExist($appl["applicantID"]);
					
					if(!$checkexist){
					
						$d = substr($appl["appl_dob"],0,2);
						$m = substr($appl["appl_dob"],2,2);
						$y = substr($appl["appl_dob"],4,2);
						
						$z = (int)$y;
						if($z>12) $y="19$y"; else $y="20$y";					
						
						$profile["appl_fname"] = $appl["appl_fname"];
						$profile["appl_gender"] = $appl["appl_gender"];
						$profile["appl_religion"] = $appl["appl_religion"];
						$profile["appl_dob"] = "$d-$m-$y"; 
						$profile["appl_nationality"] = $appl["appl_nationality"];					
						$profile["appl_phone_home"] = $appl["appl_phone_home"];
						$profile["appl_phone_mobile"] = $appl["appl_phone_mobile"];					
						$profile["appl_address1"] = $appl["appl_address1"];
						$profile["appl_postcode"] = $appl["appl_postcode"];	
						$profile["appl_state"] = $appl["appl_state"];
						$profile["appl_city"] = $appl["appl_city"];	
						$profile["appl_province"] = 96;
						$profile["appl_email"] = $appl["applicantID"];
						$profile["appl_password"] = $appl["applicantID"];
/*						echo "<pre>";
						print_r($profile);
						echo "</pre>";*/
							
						$profileID=$profileDB->addData($profile);
						
						
						$family["af_relation_type"] = 20;
						$family["af_name"] = $appl["father_af_name"];										
						$family["af_address1"] = $appl["father_af_address1"];
						$family["af_postcode"] = $appl["father_af_postcode"];
						$family["af_state"] = $appl["father_af_state"] ;
						$family["af_city"] = $appl["father_af_city"];					
						$family["af_family_condition"] = $appl["father_af_family_condition"];
						$family["af_education_level"] = $appl["father_af_education_level"]; //sis_setup_dtl
						$family["af_job"] = $appl["father_af_job"]; //tbl familyjob					
						$family["af_phone"] = $appl["father_af_phone"];					
						$family["af_appl_id"] = $profileID;
						
						if($family["af_city"]==""){
							$family["af_city"]=733; //Jakarta Pusat
						}
						
						if($family["af_state"]==""){
							$family["af_state"]=255; // DKI Jakarta
						}
	
						if($family["af_education_level"]==""){
							$family["af_education_level"]=0; 
						}				
/*						echo "<pre>";
						print_r($family);
						echo "</pre>";*/	
								
						$familyDB->addData($family);
						
						$family2["af_relation_type"] = 21;
						$family2["af_name"] = $appl["mother_af_name"];					
						$family2["af_family_condition"] = $appl["mother_af_family_condition"];
						$family2["af_education_level"] = $appl["mother_af_education_level"]; //sis_setup_dtl
						$family2["af_job"] = $appl["mother_af_job"]; //tbl familyjob
						$family2["af_appl_id"] = $profileID;
						
						if($family2["af_education_level"]==""){
							$family2["af_education_level"]=0; 					
						}
	
/*						echo "<pre>";
						print_r($family2);
						echo "</pre>";*/
						
						$familyDB->addData($family2);
						$aycode=substr($appl["applicantID"],0,2)+1;
						
						$txndata["at_appl_id"]=$profileID;
						$txndata["at_pes_id"]=$appl["applicantID"];
						$txndata["at_appl_type"]=1;
						$txndata["at_academic_year"]=$badb->getAcademicYear($aycode);
						$txndata["at_intake"]=$badb->getIntake($aycode);
						$txndata["at_period"]=5;
						$txndata["at_status"]="PROCESS";
						$txndata["at_create_by"]=99999;
						$txndata["at_create_date"]=date("Y-m-d H:i:s");
						$txndata["at_submit_date"]=date("Y-m-d H:i:s");;
	
/*						echo "<pre>";
						print_r($txndata);
						echo "</pre>";	*/		
	
						
						$txnID=$transactionDB->addData($txndata);
						
						$prog["ap_at_trans_id"] = $txnID;
						$prog["ap_prog_code"]=$appl["programcode1"];
						$prog["ap_preference"]=1;
/*						echo "<pre>";
						print_r($prog);
						echo "</pre>";	*/
						$aprogDB->insert($prog);
						
						if($appl["programcode2"]!=""){
							$prog2["ap_at_trans_id"] = $txnID;
							$prog2["ap_prog_code"]=$appl["programcode2"];
							$prog2["ap_preference"]=2;
/*							echo "<pre>";
							print_r($prog2);
							echo "</pre>";*/												
							$aprogDB->insert($prog2);					
						}else{
							$appl["programcode2"]=$appl["programcode1"];
						}
											
						
						$ptest["apt_at_trans_id"]=$txnID;
						$ptest["apt_appl_id"]=0;
						$ptest["apt_no_pes"]=$appl["applicantID"];
						$ptest["apt_ptest_code"]='PT00001';
						//33 = Pontianak 13/01
						//19 = Denpasar 27/1
						//20 = Balikpapan 27/1
						//21 = Bengkulu 27/1
						//18 = Jambi 27/1
						$ptest["apt_aps_id"]=$formData["scid"];
						$ptest["apt_fee_amt"]="250000.00";
						$ptest["apt_bill_no"]=$appl["applicantID"];
/*						echo "<pre>";
						print_r($ptest);
						echo "</pre>";*/								
						$ptestDB->addData($ptest);
						
						$hs["ae_institution"]=$appl["ae_institution"];
						if($hs["ae_institution"]=="")$hs["ae_institution"]="0";
						$hs["ae_transaction_id"]=$txnID;
						$hs["ae_discipline_code"]=$appl["ae_discipline_code"];
						$hs["ae_appl_id"]=$profileID;
						
/*						echo "<pre>";
						print_r($hs);
						echo "</pre>";*/					
						
						$appeduDB->insert($hs);
						
						$aprogDB->getProcedure($txnID,$appl["programcode1"],$appl["programcode2"],$ptest["apt_aps_id"]);
						
						//echo "Update profile $profileID >> Update transaction $txnID<br>";					
					}
			}	
    	}    
    	$this->view->data = $data;
	}
	
	public function uploadCsvAction(){
		echo "Disabled function";
		exit;
		
		$save = $this->_getparam("save",null);
		
		//title
		$adaproblemprogram=0;
    	$this->view->title= $this->view->translate("Batch Application - By CSV ");
    	$badb = new Application_Model_DbTable_Batchagent();	
    	
    	$form = new Application_Form_BatchCsvProfileUpload();
    	
    	
    	if ($this->_request->isPost()) {
    		
       		$formData = $this->_request->getPost();
       		
       		//echo $formData["scid"];
       		if($formData["scid"]==""){
       			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'batch-agent', 'action'=>'schedule-list'),'default',true));
       		}
       		
       		$this->view->remove_header = $formData['remove_header']==1?true:false;

            // success - do something with the uploaded file
            $uploadedData = $form->getValues();
            $fullFilePath = $form->file->getFileName();
            
            $header_skip = $this->view->remove_header;
            $data = array();
            $data_mapped = array();
            if (($handle = fopen($fullFilePath, 'r')) !== FALSE)
            {
            	$i=0;
            	while (($row = fgetcsv($handle, 1000, ",")) !== FALSE)
            	{
            		$data = $row;
            		
            		if($header_skip && $i==0){
            			
            		
            		}else{
            			
	            		
	            		/*
	            		 * Data Mapping
	            		 * 
	            		 */
	            		$data_mapped[$i]["appl_fname"] = $data[2];
	            		$data_mapped[$i]["appl_gender"] = $data[3];
	            		$data_mapped[$i]["appl_gender"] = $badb->getlookupID("SEX",$data[3]=='L'?1:2);
	            		$data_mapped[$i]["appl_religion"] = $data[7];//sis_setup_detail
	            		$data_mapped[$i]["appl_religion"] = $badb->getlookupID("RELIGION",$data[7]);
	            		$data_mapped[$i]["appl_dob"] = date('Y-m-d', strtotime($data[5]));
	            		$data_mapped[$i]["citycode_dob"] = $data[4];
	            		$data_mapped[$i]["appl_nationality"] = $data[9];
	            			
	            		$phone = explode("/",$data[6]);
	            		$data_mapped[$i]["appl_phone_home"] = $phone[0];
	            		$data_mapped[$i]["appl_phone_mobile"] = substr(strrchr($data[6], '/'), 1);
	            			
	            		$data_mapped[$i]["appl_address1"] = $data[8];
	            		
	            		
	            		//$data_mapped[$i]["appl_postcode"] = $data[];
	            		//$data_mapped[$i]["cityname"] = $data[];
	            		//$data_mapped[$i]["appl_state"] = $data[];
	            		//if($data_mapped[$i]["appl_state"]=="") $data[];
	            		//$data[$i]["appl_state"] = $data[];
	            		//if($data[$i]["appl_state"]=="") $data[$i]["appl_state"]=1;
	            		
	            		//$data[$i]["appl_city"] = substr($line_data,76,3);
	            		//$data[$i]["appl_city"] = $badb->getcityID($data[$i]["appl_state"],$data[$i]["appl_city"]);
	            		
	            		//for app family table
	            		$data_mapped[$i]["father_af_name"] = $data[13];
	            		$data_mapped[$i]["father_af_address1"] = $data[14];
	            		//$data[$i]["father_af_postcode"] = substr($line_data,324,5);
	            		//$data[$i]["father_af_state"] = substr($line_data,332,2);
	            		//$data[$i]["father_af_state"] = $badb->getstateID($data[$i]["father_af_state"]);
	            		//$data[$i]["father_af_city"] = substr($line_data,329,3);
	            		//$data[$i]["father_af_city"] = $badb->getcityID($data[$i]["father_af_state"],$data[$i]["father_af_city"]);
	            			
	            		//$data[$i]["father_af_family_condition"] = substr($line_data,198,1);
	            		//$data[$i]["father_af_family_condition"] = $badb->getlookupID("PARENTCON",$data[$i]["father_af_family_condition"]);
	            		//$data[$i]["father_af_education_level"] = substr($line_data,201,1); //sis_setup_dtl
	            		//$data[$i]["father_af_education_level"] = $badb->getlookupID("EDULEVEL",$data[$i]["father_af_education_level"]);
	            		//$data[$i]["father_af_job"] = substr($line_data,203,1); //tbl familyjob
	            		$phone_father = explode("/",$data[15]);
	            		$data_mapped[$i]["father_af_phone"] = $phone_father[0];
	            		
	            		
	            		//$data[$i]["mother_af_name"] = substr($line_data,262,25);
	            		//$data[$i]["mother_af_family_condition"] = substr($line_data,199,1);
	            		//$data[$i]["mother_af_family_condition"] = $badb->getlookupID("PARENTCON",$data[$i]["mother_af_family_condition"]);
	            		//$data[$i]["mother_af_education_level"] = substr($line_data,202,1);
	            		//$data[$i]["mother_af_education_level"] = $badb->getlookupID("EDULEVEL",$data[$i]["mother_af_education_level"]);
	            		//$data[$i]["mother_af_job"] = substr($line_data,204,1);
	            		
	            			
	            		
	            		
	            		//education
	            		//$data[$i]["hsyearpass"] = substr($line_data,206,6);
	            		//$data[$i]["ae_institution"] = substr($line_data,223,8);		//schoolmaster//
	            		//$data[$i]["ae_institution"] = $badb->gethsID($data[$i]["ae_institution"]);
	            		//$data[$i]["ae_discipline_code"] = substr($line_data,212,3);
	            		//$data[$i]["hsprovince"] = substr($line_data,215,2);
	            		//$data[$i]["hsprovince"] = $badb->getstateID($data[$i]["hsprovince"]);
	            		//$data[$i]["hscitycode"] = substr($line_data,217,3);
	            		//$data[$i]["hscitycode"] = $badb->getcityID($data[$i]["hsprovince"],$data[$i]["hscitycode"]);
	            		
	            		//for txn table
	            		$data_mapped[$i]["no_form"] = $data[17];
	            		$data_mapped[$i]["programcode1"] = $data[1];
	            		//$data[$i]["programcode2"] = substr($line_data,52,4);
	            		
            		}
            		
            		$i++;
            		
            	}
            	fclose($handle);
            }
            
            
            //INSERT DATA
            $migrateDB = new Application_Model_DbTable_Migrate();
            $transactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
            $profileDB  = new App_Model_Application_DbTable_ApplicantProfile();
            $familyDB  = new App_Model_Application_DbTable_ApplicantFamily();
            $aprogDB  = new App_Model_Application_DbTable_ApplicantProgram();
            $appeduDB = new App_Model_Application_DbTable_ApplicantEducation();
            $ptestDB = new App_Model_Application_DbTable_ApplicantPtest();
            $badb = new Application_Model_DbTable_Batchagent();
            $selDb = new App_Model_Application_DbTable_ApplicantSelectionDetl();
            $assDb= new App_Model_Application_DbTable_ApplicantAssessment();
            
            foreach ($data_mapped as $i=>$thedata){
            
            	if(!$migrateDB->exist($thedata["no_form"])){
            		$profile["appl_fname"] = $thedata["appl_fname"];
            		$profile["appl_email"] = $thedata["appl_email"];
            		if($profile["appl_email"]==""){
            			$profile["appl_email"]=$thedata["no_form"];
            		}
            		if($profile["appl_email"]=="-"){
            			$profile["appl_email"]=$thedata["no_form"];
            		}
            		$profile["appl_password"] = $thedata["no_form"];
            			
            		$profile["appl_gender"] = $thedata["appl_gender"];
            		if($profile["appl_gender"]=="")  $profile["appl_gender"]="0";
            			
            		$profile["appl_dob"] = $thedata["appl_dob"];
            		if($profile["appl_dob"]=="")  $profile["appl_dob"]="0";
            		$profile["appl_nationality"] =$thedata["w_negara"];
            		if($profile["appl_nationality"]=="")  $profile["appl_nationality"]="1";
            		$profile["appl_religion"] = $thedata["agama"];
            		if($profile["appl_religion"]=="")  $profile["appl_religion"]="5";
            		$profile["appl_phone_home"] = $thedata["appl_phone_home"];
            		if($profile["appl_phone_home"]=="")  $profile["appl_phone_home"]="0";
            		$profile["appl_phone_mobile"] = $thedata["appl_phone_mobile"];
            		if($profile["appl_phone_mobile"]=="")  $profile["appl_phone_mobile"]="0";
            			
            		$profile["appl_address1"] = $thedata["appl_address1"];
            		$profile["appl_postcode"] = $thedata["kd_pos"];
            		if($profile["appl_postcode"]=="")  $profile["appl_postcode"]="0";
            			
            		$profile["appl_state"] = $migrateDB->getstateID($profile["cityname"]);
            		if($profile["appl_state"]=="") $profile["appl_state"]=1;
            
            		$profile["appl_city"] = $thedata["kota"];
            		$profile["appl_city"] = $migrateDB->getcityID($profile["appl_city"]);
            		if($profile["appl_city"]=="") $profile["appl_city"]=733;
            		
            		$migrateDB->checkEmail($profile["appl_email"]);
            		$profileID=$profileDB->addData($profile);
            
            		$family["af_relation_type"] = 20;
            		$family["af_name"] = $thedata["father_af_name"];
            		if($family["af_name"]=="")$family["af_name"]=1;
            		$family["af_address1"] = $thedata["father_af_address1"];
            		$family["af_postcode"] = $thedata["pos_ot"];
            		if($family["af_postcode"]=="")$family["af_postcode"]=1;
            		$family["af_state"] = $profile["appl_state"] ;
            		if($family["af_state"]=="")$family["af_state"]=1;
            		$family["af_city"] = $profile["appl_city"];
            		if($family["af_city"]=="")$family["af_city"]=733;
            		$family["af_family_condition"] = $thedata["adaayh"];
            		if($family["af_family_condition"]=="")$family["af_family_condition"]=1;
            			
            		$family["af_education_level"] = $thedata["ddkayh"]; //sis_setup_dtl
            		$family["af_job"] = $thedata["pekerjaan_ayah"]; //tbl familyjob
            		if($family["af_job"]=="")$family["af_job"]=1;
            		$family["af_phone"] = $thedata["father_af_phone"];
            		if($family["af_phone"]=="")$family["af_phone"]=1;
            		$family["af_appl_id"] = $profileID;
            
            		if($family["af_city"]==""){
            			$family["af_city"]=733; //Jakarta Pusat
            		}
            			
            		if($family["af_state"]==""){
            			$family["af_state"]=255; // DKI Jakarta
            		}
            
            		if($family["af_education_level"]==""){
            			$family["af_education_level"]=0;
            		}
            		
            		/*echo"<pre>";
            		print_r($family);
            		echo"</pre>";
            		exit;*/
            		$familyDB->addData($family);
            			
            		/*$family2["af_relation_type"] = 21;
            		$family2["af_name"] = $thedata["nama_ibu"];
            		if($family2["af_name"]=="")$family2["af_name"]="No Name";
            		$family2["af_family_condition"] = $thedata["adaibu"];
            		if($family2["af_family_condition"]=="")$family2["af_family_condition"]=1;
            		$family2["af_education_level"] = $thedata["ddkibu"]; //sis_setup_dtl
            		$family2["af_job"] = $thedata["kerjaibu"]; //tbl familyjob
            		if($family2["af_job"]=="")$family2["af_job"]=1;
            		$family2["af_appl_id"] = $profileID;
            			
            		if($family2["af_education_level"]==""){
            			$family2["af_education_level"]=0;
            		}
            
            		echo"<pre>";
            		 print_r($family2);
            		echo"</pre>";
            		$familyDB->addData($family2);*/
            			
            
            		$txndata["at_appl_id"]=$profileID;
            		$txndata["at_pes_id"]=$thedata["no_form"];
            		$txndata["at_appl_type"]=2;
            		$txndata["at_academic_year"]=2;
            		$txndata["at_intake"]=78;
            		$txndata["at_period"]=4;
            		$txndata["at_status"]="PROCESS";
            		$txndata["at_create_by"]=99989;
            		$txndata["at_create_date"]=date("Y-m-d H:i:s");
            		$txndata["at_submit_date"]=date("Y-m-d H:i:s");
            		$txndata["at_selection_status"]=3;
            		$txndata["at_payment_status"]=1;
            		/*echo"<pre>";
            		 print_r($txndata);
            		echo"</pre>";
            		exit;*/
            		$txnID=$transactionDB->addData($txndata);
            			
            		$prog["ap_at_trans_id"] = $txnID;
            		$prog["ap_prog_code"]=$thedata["programcode1"];
            		$prog["ap_preference"]=1;
            		$prog["ap_usm_status"]=0;
            			
            		/*echo"<pre>";
            		 print_r($prog);
            		echo"</pre>";	
            		exit;*/
            		$aprogDB->insert($prog);
            
            		$hs["ae_institution"]=$thedata["kd_sla"];
            		if($hs["ae_institution"]=="")$hs["ae_institution"]="0";
            		$hs["ae_transaction_id"]=$txnID;
            		$hs["ae_discipline_code"]=$thedata["jur_sla"];
            		if($hs["ae_discipline_code"]=="")$hs["ae_discipline_code"]="0";
            		$hs["ae_appl_id"]=$profileID;
            
            		/*echo"<pre>";
            		 print_r($hs);
            		echo"</pre>";
            		exit;*/
            		$appeduDB->insert($hs);
            			
            		$selection["aar_trans_id"]=$txnID;
            		$selection["aar_rating_rector"]=1;
            		$selection["aar_rector_status"]=1;
            		$selection["aar_remarks"]="Data Sore FE";
            		$selection["aar_rector_selectionid"]=581;
            		$selection["aar_rector_rateby"]=99988;
            		$selection["aar_rector_ratedt"]='2013-08-19';
            		$selection["aar_reg_start_date"]="2013-08-01";
            		$selection["aar_reg_end_date"]="2013-08-31";
            		$selection["aar_payment_start_date"]="2013-08-01";
            		$selection["aar_payment_end_date"]="2013-08-31";
            		/*echo"<pre>";
            		print_r($selection);
            		echo"</pre>";
            		exit;*/
            		$assDb->addData($selection);
            		
            	}else{
            		echo $thedata["no_form"]."!! No Formulir Exist!<br>";
            	}
            	
            }
            
            echo "complete";
            exit;
            
            echo "<pre>";
            print_r($formData);
            echo "</pre>";
            exit;

    	}  
    	  
    	$this->view->data = $data_mapped;
    	//$this->view->data = $data;
    	
	}
	
	public function ajaxGetLocationAction($id=null){

//	 	$storage = new Zend_Auth_Storage_Session ();
//		$data = $storage->read ();
//		if (! $data) {
//			$this->_redirect ( 'index/index' );
//		}
			
    	$select_date = $this->_getParam('select_date', 0);
     
     	if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        
        $applicantPlacementScheduleDB = new App_Model_Application_DbTable_ApplicantPlacementSchedule();
    	$location_list = $applicantPlacementScheduleDB->getLocationByDate($select_date);
    	
    	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($location_list);
		echo $json;
		exit;
    }	
    
    public function scheduleListAction(){
    	$this->view->title= $this->view->translate("Batch Create Applicant - Schedule");
    	
    	$form = new Application_Form_PlacementTestSearch();
    	
    	$scheduleDb = new App_Model_Application_DbTable_PlacementTestSchedule();
    	
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			$form->populate($formData);
			
			$cond = array(
				'location' => $formData['location']
			);
			
			//intake
    		if( isset($formData['intake_id']) && $formData['intake_id']!="" ){
				$intakeDb = new App_Model_Record_DbTable_Intake();
				$intakeData = $intakeDb->getData($formData['intake_id']);

				$cond['dt_from'] = date('Y-m-d', strtotime($intakeData['ApplicationStartDate']));
				$cond['dt_to'] = date('Y-m-d', strtotime($intakeData['ApplicationEndDate']));
			}
			//period
			if( isset($formData['period_id']) && $formData['period_id']!="" ){
				$periodDb = new App_Model_Record_DbTable_AcademicPeriod();
				$periodData = $periodDb->getData($formData['period_id']);
				
				$cond['dt_from'] = date('d-n-Y', strtotime('01-'.$periodData['ap_month']."-".$periodData['ap_year']));
				$cond['dt_to'] = date('t-m-Y', strtotime($cond['dt_from']));
			}
			
			//get schedule by search
			$list = $scheduleDb->search($cond);
			
			$this->view->scheduleList = $list; 
    	}else{
    		$list = $scheduleDb->getData();
    	}
    	
    	$this->view->placementTestList = $list;
    	
    	$this->view->form = $form;    
    }
}
