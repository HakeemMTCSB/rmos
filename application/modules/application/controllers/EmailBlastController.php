<?php

class Application_EmailBlastController extends Base_Base {
	
	public function indexAction(){
		//title
    	$this->view->title="Email Blast - Select Receipient";
    	
    	$session = new Zend_Session_Namespace('sis');
    	
    	$applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
		
	    $form = new Application_Form_SearchApplicant();
	    $this->view->form = $form;
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			//Only admin can view all txn
			if($session->IdRole == 1){
				$txnList = $applicantTxnDB->getSearchPaginate($formData);
			}else{
				$txnList = $applicantTxnDB->getSearchPaginateFaculty($formData,$session->idCollege);
			}
			
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($txnList));
			$paginator->setItemCountPerPage(10000000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$form->populate($formData);
			
		}else{
			
			//Only admin can view all txn
			if($session->IdRole == 1){
				$txnList = $applicantTxnDB->getPaginateData();
			}else{
				$txnList = $applicantTxnDB->getPaginateDataFaculty($session->idCollege);
			}
			
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($txnList));
			$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
		}
		
		$this->view->paginator = $paginator;
	}
	
	public function composeAction() {
		//title
    	$this->view->title="Email Blast - Compose";
    	
    	//form
    	$form = new Application_Form_PlacementTestComponent();
    	
    	if ( $this->getRequest()->isPost() ){
    			$formData = $this->getRequest()->getPost();
    			
    			$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
    			
    			$transactionList = array();
    			$i=0;
    			foreach ($formData['txn_id'] as $txn){
    				//get transaction data
    				$transactionList[$i] = $txnDb->getTransaction($txn);
    				
    				$i++;
    			}
    			
    			$this->view->txnList = $transactionList; 
    	}
	}
	
	public function selectTemplateAction(){
		$this->_helper->layout()->disableLayout();
		
		$emailBlastTemplateDb = new App_Model_General_DbTable_EmailBlastTemplate();
		
		if ( $this->getRequest()->isPost() ){
			//return template data
			$formData = $this->getRequest()->getPost();
			
			$content = $emailBlastTemplateDb->getContent($formData['id'],$formData['language']);
			
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
			
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
			
			$json = Zend_Json::encode($content);
			
			echo $json;
			
			exit();
			
		}else{
			//list template
			
			$session = new Zend_Session_Namespace('sis');
			 
			//Only admin can view all template
			if($session->IdRole == 1){
				$template = $emailBlastTemplateDb->getData();
			}else{
				$template = $emailBlastTemplateDb->getDataCollege($session->idCollege);
			}
			
			
			$this->view->template = $template;
		}
		
	}
	
	public function sendEmailAction(){
		
		if ( $this->getRequest()->isPost() ){
    			$formData = $this->getRequest()->getPost();
    			
    			$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
    			
    			$i=0;
    			foreach ($formData['recepient'] as $txn){
    				//get txn Data
    				$data = $txnDb->getTransaction($txn);
    				$formData['status'][$i] = $this->sendEmailQue($data['appl_email'], $formData['subject'], $formData['content'], $data);
    				
    				$i++;	
    			}
    			
    			//save history
    			$auth = Zend_Auth::getInstance();
    			$emailBlasHistoryDb = new Application_Model_DbTable_EmailBlastHistory();
    			
    			$i=0;
    			foreach ($formData['recepient'] as $txn){
    				$dataHistory = array(
    					'send_date'					 => date("Y-m-d H:i:s"),
    					'send_by'					 => $auth->getIdentity()->id,
    					'application_transaction'	 =>$txn,
    					'subject'					 =>$formData['subject'],
    					'content'					 => $formData['content'],
    					'status'					 =>$formData['status'][$i]
    				);
    				
    				$emailBlasHistoryDb->addData($dataHistory);
    				$i++;
    			}
    			
    			//redirect
				$this->_redirect($this->view->url(array('module'=>'application','controller'=>'email-blast', 'action'=>'history'),'default',true));
		}
		
		exit;
	}
	
	public function exportPdfAction(){
		
		$utility = new icampus_Function_General_Utility();
		
		if ( $this->getRequest()->isPost() ){
			$formData = $this->getRequest()->getPost();
			
			$txnDb = new App_Model_Application_DbTable_ApplicantTransaction();
			 
			$i=0;
			foreach ($formData['recepient'] as $txn){
				//get txn Data
				$data = $txnDb->getTransaction($txn);
				
				//filename
				$filename = str_replace(' ', '_', $formData['subject']);
				
				//data
				try {
						
					$email_receipient = $data["appl_email"];
					$name_receipient  = $data["appl_fname"].' '.$data["appl_mname"].' '.$data["appl_lname"];
						
					$templateMail =$formData['content'];
				
					//replace tag
					$templateMail = str_replace("[Candidate]",$name_receipient,$templateMail);
					$templateMail = str_replace("[FIRST_NAME]",$data["appl_fname"],$templateMail);
					$templateMail = str_replace("[MIDDLE_NAME]",$data["appl_mname"],$templateMail);
					$templateMail = str_replace("[LAST_NAME]",$data["appl_lname"],$templateMail);
						
						
					$templateMail = str_replace("[EmailApplicant]",$data["appl_email"],$templateMail);
					$templateMail = str_replace("[PassApplicant]",$data['appl_password'],$templateMail);
				
						
					//export
					$utility->exportPdf($templateMail,$filename.".pdf");
						
				} catch (Exception $e) {
					$status = false;
				}
				
				
				
		
				$i++;
			}
		}
		
		exit;
	}
	
	public function historyAction(){
		//title
    	$this->view->title="Email Blast - History";
    	
    	$session = new Zend_Session_Namespace('sis');
    	
    	$emailBlasHistoryDb = new Application_Model_DbTable_EmailBlastHistory();
    	
    	//Only admin can view all history
    	if($session->IdRole == 1){
    		$data = $emailBlasHistoryDb->getPaginateData();
    	}else{
    		$data = $emailBlasHistoryDb->getPaginateDataCollege($session->idCollege);
    	}
    	
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
		$paginator->setItemCountPerPage($this->gintPageCount);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;
	}
	
	public function templateAction(){
		//title
    	$this->view->title = "Email Blast Template";
    	
    	$session = new Zend_Session_Namespace('sis');
    	
    	$emailBlastTemplateDb = new App_Model_General_DbTable_EmailBlastTemplate();
    	
    	//Only admin can view all templates
    	if($session->IdRole == 1){
    		$p_data = $emailBlastTemplateDb->getPaginateData();
    	}else{
    		$p_data = $emailBlastTemplateDb->getPaginateDataCollege($session->idCollege);
    	}
    	
    	
    	
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;

	}
	
	public function templateDetailAction(){
		//title
		$this->view->title = "Email Blast Template - Detail";
		
		$id = $this->_getParam('id', 0);
		
		$emailBlastTemplateDb = new App_Model_General_DbTable_EmailBlastTemplate();
		$template = $emailBlastTemplateDb->getData($id);
		
		$this->view->template = $template;
		
		if ( $this->getRequest()->isPost() ){
			$formData = $this->getRequest()->getPost();
			
			$emailBlastTemplateDB = new App_Model_General_DbTable_EmailBlastTemplate();
			//update template
			$data = array(
						'ebt_template_name' => $formData['ebt_template_name'],
						'ebt_email_from' => $formData['ebt_email_from'],
						'ebt_email_from_name' => $formData['ebt_email_from_name']
					);
			
			
			$emailBlastTemplateDB->update($data,'ebt_id = '.$id);
			
			
			//save content
			$emailBlastTemplateContentDb = new App_Model_General_DbTable_EmailBlastTemplateContent();
			
			//english
			$data = array(
						'ebtc_subject' => $formData['subject_english'],
						'ebtc_body' => $formData['content_english']
					);
			
			$content = $emailBlastTemplateDB->getContent($id, 1);
			
			if($content){
				$emailBlastTemplateContentDb->update($data,'ebtc_ebt_id = '.$content['ebtc_id'].' and ebtc_language = 1');
			}else{
				$data['ebtc_ebt_id'] = $id;
				$data['ebtc_language'] = 1;
				$emailBlastTemplateContentDb->insert($data);
			}
			
			//indonesia
			$data2 = array(
					'ebtc_subject' => $formData['subject_indonesia'],
					'ebtc_body' => $formData['content_indonesia']
			);
				
			$content2 = $emailBlastTemplateDB->getContent($id, 2);
				
			if($content2){
				$emailBlastTemplateContentDb->update($data2,'ebtc_ebt_id = '.$content['ebtc_id'].' and ebtc_language = 2');
			}else{
				$data2['ebtc_ebt_id'] = $id;
				$data2['ebtc_language'] = 2;
				$emailBlastTemplateContentDb->insert($data);
			}
			
			//redirect
			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'email-blast', 'action'=>'template'),'default',true));
		}
		
		/*echo "<pre>";
		print_r($template);
		echo "</pre>";*/
		
		
	}
	
	public function templateAddAction(){
		
		//title
		$this->view->title = "Email Blast Template - Add";
		
		if ( $this->getRequest()->isPost() ){
			$formData = $this->getRequest()->getPost();
				
			$emailBlastTemplateDB = new App_Model_General_DbTable_EmailBlastTemplate();
			//update template
			$data = array(
					'ebt_template_name' => $formData['ebt_template_name'],
					'ebt_email_from' => $formData['ebt_email_from'],
					'ebt_email_from_name' => $formData['ebt_email_from_name']
			);
				
			$id = $emailBlastTemplateDB->insert($data);
				
				
			//save content
			$emailBlastTemplateContentDb = new App_Model_General_DbTable_EmailBlastTemplateContent();
				
			//english
			$data = array(
					'ebtc_subject' => $formData['subject_english'],
					'ebtc_body' => $formData['content_english'],
					'ebtc_ebt_id' => $id,
					'ebtc_language' => 1
			);
			
			$emailBlastTemplateContentDb->insert($data);
			
				
			//indonesia
			$data = array(
					'ebtc_subject' => $formData['subject_indonesia'],
					'ebtc_body' => $formData['content_indonesia'],
					'ebtc_ebt_id' => $id,
					'ebtc_language' => 2
			);
		
			$emailBlastTemplateContentDb->insert($data);
			
				
			//redirect
			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'email-blast', 'action'=>'template'),'default',true));
		}
		
			
	}
	
	private function sendEmailQue($receipient, $subject, $content, $data, $attachment_path=null, $attachment_filename=null){
		$status = true;
		
		try {
			
			$email_receipient = $data["appl_email"];
			$name_receipient  = $data["appl_fname"].' '.$data["appl_mname"].' '.$data["appl_lname"];    		
			    		    		
			$templateMail =$content;

			//replace tag
			$templateMail = str_replace("[Candidate]",$name_receipient,$templateMail);	
			$templateMail = str_replace("[FIRST_NAME]",$data["appl_fname"],$templateMail);
			$templateMail = str_replace("[MIDDLE_NAME]",$data["appl_mname"],$templateMail);
			$templateMail = str_replace("[LAST_NAME]",$data["appl_lname"],$templateMail);
			
			
			$templateMail = str_replace("[EmailApplicant]",$data["appl_email"],$templateMail);
			$templateMail = str_replace("[PassApplicant]",$data['appl_password'],$templateMail);
						
			$emailDb = new App_Model_System_DbTable_Email();		
			$data = array(
				'recepient_email' => $receipient,
				'subject' => $subject,
				'content' => $templateMail,
				'attachment_path' => $attachment_path,
			    'attachment_filename' => $attachment_filename
			);	
			
			//to send email with attachment
			$emailDb->addData($data);	
			
		} catch (Exception $e) {
			$status = false;
		}
		
		return $status;
	}
	
}