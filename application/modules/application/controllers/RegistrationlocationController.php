<?php
class Application_RegistrationlocationController extends Base_Base {
	private $lobjformregistrationlocation;
	private $lobjsubjectgrademodel;
	private $lobjreglocationmodel;
	private $lobjcodegenerator;
	private $lobjconfigmodel;


	public function init() {
		$this->fnsetObj();
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	}

	public function fnsetObj(){
		$this->lobjformregistrationlocation = new Application_Form_Registrationlocation();
		$this->lobjsubjectgrademodel = new Application_Model_DbTable_Subjectgradetype();
		$this->lobjreglocationmodel = new Application_Model_DbTable_Registrationlocation();
		$this->lobjcodegenerator = new Cms_CodeGeneration();
		$this->lobjconfigmodel = new GeneralSetup_Model_DbTable_Initialconfiguration();
	}

	public function indexAction() {
		$this->view->lobjform = $this->lobjform;
		$larrresult = $this->lobjreglocationmodel->fngetreglocations();
		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->reglocationpaginatorresult);
		$lintpagecount =$this->gintPageCount;
		$lintpage = $this->_getParam('page',1);
		$lobjPaginator = new App_Model_Common();
		if(isset($this->gobjsessionsis->reglocationpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->reglocationpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjreglocationmodel->fnSearchreglocation($this->lobjform->getValues()); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->subjectgradepaginatorresult = $larrresult;
			}
		}
	}

	public function newregistrationlocationAction() {
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			$registrationlocationArray['RegistrationLocationName'] = $formData['RegistrationLocationName'];
			$registrationlocationArray['RegistrationLocationCode'] = $formData['RegistrationLocationCode'];
			$registrationlocationArray['RegistrationLocationShortName'] = $formData['RegistrationLocationShortName'];
			$registrationlocationArray['RegistrationLocationDescription'] = $formData['RegistrationLocationDescription'];
			$registrationlocationArray['RegistrationLocationIntake'] = $formData['RegistrationLocationIntake'];
			$registrationlocationArray['RegistrationLocationStatus'] = $formData['RegistrationLocationStatus'];
			$registrationlocationArray['RegistrationLocationAddress1'] = $formData['RegistrationLocationAddress1'];
			$registrationlocationArray['RegistrationLocationAddress2'] = $formData['RegistrationLocationAddress2'];
			$registrationlocationArray['country'] = $formData['country'];
			$registrationlocationArray['state'] = $formData['state'];
			$registrationlocationArray['city'] = $formData['city'];
			$registrationlocationArray['zipCode'] = $formData['zipCode'];
			$registrationlocationArray['RegPhone'] = $formData['Regcountrycode'].'-'.$formData['Regstatecode'].'-'.$formData['RegPhone'];
			$registrationlocationArray['fax'] = $formData['faxcountrycode'].'-'.$formData['faxstatecode'].'-'.$formData['fax'];

			$Idreglocation = $this->lobjreglocationmodel->fnaddreglocation($registrationlocationArray);
			// Now create Subject grade point array
			$regInfoArray = array();
			$len = count($formData['RegistrationLocationScheme']);
			$temp = array();
			for($i = 0; $i<$len; $i++){
				$temp['IdRegistrationLocation'] = $Idreglocation;
				$temp['RegistrationLocationScheme'] = $formData['RegistrationLocationScheme'][$i];
				$temp['RegistrationLocationDate'] = $formData['RegistrationLocationDate'][$i];
				$temp['RegistrationLocationTime'] = $formData['RegistrationLocationTime'][$i];
				$temp['RegistrationLocationRemarks'] = $formData['RegistrationLocationRemarks'][$i];
				$temp['RegistrationLocationProgram'] = $formData['RegistrationLocationProgram'][$i];
				$temp['RegistrationLocationBranch'] = $formData['RegistrationLocationBranch'][$i];
				$regInfoArray[] = $temp;
			}
			foreach($regInfoArray as $reg){
				$this->lobjreglocationmodel->fnaddregInfo($reg);
			}
			$this->_redirect( $this->baseUrl . '/application/registrationlocation');
		}

		$this->view->lobjformregistrationlocation = $this->lobjformregistrationlocation;
		$this->view->larrstatusmessagelist = array();

	}


	public function editregistrationlocationAction(){
		$lobjCommonModel = new App_Model_Common();
		$this->view->lobjformregistrationlocation = $this->lobjformregistrationlocation;
		$id = $this->_getParam('id');

		$this->view->larrstatusmessagelist = $this->lobjreglocationmodel->fngetsubjectgradepoints($id);

		$returnArray = $this->lobjreglocationmodel->fngetreglocation($id);
		$this->view->cur_registrationlocation = $returnArray;
		// Setting Phone field
		$pices = explode ( '-' , $returnArray['RegPhone'] );
		$this->view->lobjformregistrationlocation->Regcountrycode->setValue( $pices[0] );
		$this->view->lobjformregistrationlocation->Regstatecode->setValue( $pices[1] );
		$this->view->lobjformregistrationlocation->RegPhone->setValue( $pices[2] );

		// Setting Fax Field field
		$pics = explode ( '-' , $returnArray['RegPhone'] );
		$this->view->lobjformregistrationlocation->faxcountrycode->setValue( $pics[0] );
		$this->view->lobjformregistrationlocation->faxstatecode->setValue( $pics[1] );
		$this->view->lobjformregistrationlocation->fax->setValue( $pics[2] );

		$this->view->lobjformregistrationlocation->RegistrationLocationShortName->setValue( $returnArray['RegistrationLocationShortName'] );
		$this->view->lobjformregistrationlocation->RegistrationLocationName->setValue( $returnArray['RegistrationLocationName'] );
		$this->view->lobjformregistrationlocation->RegistrationLocationCode->setValue( $returnArray['RegistrationLocationCode'] );
		$this->view->lobjformregistrationlocation->RegistrationLocationDescription->setValue( $returnArray['RegistrationLocationDescription'] );
		$this->view->lobjformregistrationlocation->RegistrationLocationIntake->setValue( $returnArray['RegistrationLocationIntake'] );
		$this->view->lobjformregistrationlocation->RegistrationLocationStatus->setValue( $returnArray['RegistrationLocationStatus'] );
		$this->view->lobjformregistrationlocation->RegistrationLocationAddress1->setValue( $returnArray['RegistrationLocationAddress1'] );
		$this->view->lobjformregistrationlocation->RegistrationLocationAddress2->setValue( $returnArray['RegistrationLocationAddress2'] );
		$this->view->lobjformregistrationlocation->country->setValue( $returnArray['country'] );


		$larrCountyrStatesList = $lobjCommonModel->fnGetCountryStateList(intval($returnArray['country']));
		foreach($larrCountyrStatesList as $state) {
			$this->lobjformregistrationlocation->state->addMultiOption($state['key'],$state['value'] );
		}

		$cities = $this->lobjCommon->fnGetCityList($returnArray['state']);
		foreach($cities as $city) {
			$this->lobjformregistrationlocation->city->addMultiOption($city['key'],$city['value']);
		}
		//$this->view->states = $larrCountyrStatesList = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjCommon->fnGetCountryStateList(intval($returnArray['country'])));
		//$this->view->cities = $larrStateCityList = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjCommon->fnGetCityList($returnArray['state']));

		//$this->lobjformregistrationlocation->state->addMultiOptions($larrCountyrStatesList);
		//$this->lobjformregistrationlocation->city->addMultiOptions($larrStateCityList);

		$this->view->lobjformregistrationlocation->state->setValue( $returnArray['state'] );
		$this->view->lobjformregistrationlocation->city->setValue( $returnArray['city'] );
		$this->view->lobjformregistrationlocation->zipCode->setValue( $returnArray['zipCode'] );


		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			$registrationlocationArray['RegistrationLocationName'] = $formData['RegistrationLocationName'];
			$registrationlocationArray['RegistrationLocationCode'] = $formData['RegistrationLocationCode'];
			$registrationlocationArray['RegistrationLocationShortName'] = $formData['RegistrationLocationShortName'];
			$registrationlocationArray['RegistrationLocationDescription'] = $formData['RegistrationLocationDescription'];
			$registrationlocationArray['RegistrationLocationIntake'] = $formData['RegistrationLocationIntake'];
			$registrationlocationArray['RegistrationLocationStatus'] = $formData['RegistrationLocationStatus'];
			$registrationlocationArray['RegistrationLocationAddress1'] = $formData['RegistrationLocationAddress1'];
			$registrationlocationArray['RegistrationLocationAddress2'] = $formData['RegistrationLocationAddress2'];
			
			$registrationlocationArray['country'] = $formData['country'];
			$registrationlocationArray['state'] = $formData['state'];
			$registrationlocationArray['city'] = $formData['city'];
			$registrationlocationArray['zipCode'] = $formData['zipCode'];
			$registrationlocationArray['RegPhone'] = $formData['Regcountrycode'].'-'.$formData['Regstatecode'].'-'.$formData['RegPhone'];
			$registrationlocationArray['fax'] = $formData['faxcountrycode'].'-'.$formData['faxstatecode'].'-'.$formData['fax'];

			$this->lobjreglocationmodel->fnupdatereglocation($registrationlocationArray,$id);
			$reglocInfoArray = array();
			$len = count($formData['RegistrationLocationScheme']);
			$temp = array();
			$this->lobjreglocationmodel->fndeletereglocInfo($id);

			for($i = 0; $i<$len; $i++){
				$temp['IdRegistrationLocation'] = $id;
				$temp['RegistrationLocationScheme'] = $formData['RegistrationLocationScheme'][$i];
				$temp['RegistrationLocationDate'] = $formData['RegistrationLocationDate'][$i];
				$temp['RegistrationLocationTime'] = $formData['RegistrationLocationTime'][$i];
				$temp['RegistrationLocationRemarks'] = $formData['RegistrationLocationRemarks'][$i];
				$temp['RegistrationLocationProgram'] = $formData['RegistrationLocationProgram'][$i];
				$temp['RegistrationLocationBranch'] = $formData['RegistrationLocationBranch'][$i];
				$reglocInfoArray[] = $temp;
			}
			foreach($reglocInfoArray as $reg){
				$this->lobjreglocationmodel->fnaddregInfo($reg);
			}
			$this->_redirect( $this->baseUrl . '/application/registrationlocation');
		}
	}


}

?>
