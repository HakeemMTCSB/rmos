<?php
class Application_PlacementvenueController extends Base_Base { //Controllerclass for the Placement venue module
	public function init() { //initialization function
		$this->locale = Zend_Registry::get('Zend_Locale');
		$this->view->translate =Zend_Registry::get('Zend_Translate');
		Zend_Form::setDefaultTranslator($this->view->translate);
		$this->_gobjlog = Zend_Registry::get ( 'log' );
		$this->fnsetObj();
	}

	public function fnsetObj()
	{
		$this->lobjform = new App_Form_Search (); //searchform
		$this->lobjVenue = new Application_Model_DbTable_Placementvenue();//object of placement venue model
		$this->lobjVenueForm = new Application_Form_Placementvenue();//object of placement venue form
		$this->gobjsessionsis = Zend_Registry::get('sis');
	}

	public function indexAction() {//index action
		$this->view->lobjform = $this->lobjform;
		$larrresult =$this->lobjVenue->fnGetVenueDetails();//function to get venue details from database and to search
		if(!$this->_getParam('search'))
			unset($this->gobjsessionstudent->bankpaginatorresult);
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->bankpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionstudent->bankpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjVenue->fnSearchVenue( $this->lobjform->getValues () ); //searching the values for the Venue
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->gobjsessionstudent = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/application/placementvenue/index');
		}
	}

	public function newvenueAction() { //action to add new venues
		$this->view->lobjVenueForm = $this->lobjVenueForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjVenueForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjVenueForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
		$lobjcountry = $lobjUser->fnGetCountryList();//function to get country list
		$this->view->lobjVenueForm->Country->addMultiOptions($lobjcountry);
		$this->view->lobjVenueForm->Country->setValue ($this->view->DefaultCountry);
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			if ($this->lobjVenueForm->isValid ( $larrformData )) {
				$result = $this->lobjVenue->fnAddVenue($larrformData); //instance for adding the lobjVenueForm values to DB
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'New Bank Add',
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				$this->_redirect( $this->baseUrl . '/application/placementvenue/index');
			}
		}
	}

	public function venuelistAction() {	//action to edit placement venue
		$this->view->lobjVenueForm = $this->lobjVenueForm;
		$lintIdVenue = ( int ) $this->_getParam ( 'id' );
		$this->view->IdVenue = $lintIdVenue;

		$larrresult = $this->lobjVenue->fnViewVenu($lintIdVenue);//function to view particular placement test venue and to populate while editing
		$City = $larrresult['City'];
		$lobjCommonModel = new App_Model_Common();
		$lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
		$lobjcountry = $lobjUser->fnGetCountryList();
		$this->view->lobjVenueForm->Country->addMultiOptions($lobjcountry);
		$this->lobjVenueForm->populate($larrresult);//to populate data of a venue
		if($City == 12) {
			$this->view->lobjVenueForm->City->addMultiOption('12','Others');
		}
		$larrStateCityList = $lobjCommonModel->fnGetCityList($larrresult['State']);
		$this->view->lobjVenueForm->City->addMultiOptions($larrStateCityList);
		$larrCountryStateList = $lobjCommonModel->fnGetCountryStateList($larrresult['Country']);
		$this->view->lobjVenueForm->State->addMultiOptions($larrCountryStateList);
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjVenueForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjVenueForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjVenueForm->isValid ( $larrformData )) {
				$this->lobjVenue->fnUpdateVenue($lintIdVenue, $larrformData );// function to update a Venue
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Bank Edit Id=' . $lintIdBank,
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				$this->_redirect( $this->baseUrl . '/application/placementvenue/index');
					
			}
		}
	}
}
