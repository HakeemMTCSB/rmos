<?php
class Application_CandidateController extends Zend_Controller_Action
{

public function indexAction(){
    	$this->view->title="Candidate List";
    	
    	$this->view->scid = $this->_getParam('scid');
    	$this->view->order = $this->_getParam('order',"appl_fname");
    	$this->view->room_id = $this->_getParam('room_id');
    	
    	if($this->view->room_id!=""){
    				$roomDb = new App_Model_Application_DbTable_PlacementTestRoom();
					$roomData = $roomDb->getData($this->view->room_id);
					$this->view->room = $roomData;
    	}
    	//paginator
		$candidateDb = new Application_Model_DbTable_Candidate();
		//$dataList = $placementTestDb->getPaginateData();
		
/*		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($dataList));
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($this->_getParam('page',1));
		
		$this->view->paginator = $paginator;*/
    	$placementTestScheduleDb = new App_Model_Application_DbTable_PlacementTestSchedule();
    	$scheduleData = $placementTestScheduleDb->getScheduleInfo($this->_getParam('scid',""));
    	$candidataData = $candidateDb->fnGetPtest($this->_getParam('scid',1),$this->_getParam('order',"appl_fname"),$this->view->room_id);

    	
		if($this->_getparam("act")=="print"){
			try{
				require_once 'dompdf_config.inc.php';
				
				$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
				$autoloader->pushAutoloader('DOMPDF_autoload');
				
				$dompdf = new DOMPDF();
	
				
				$html_template_path = DOCUMENT_PATH."/template/PoskoAttendance.html";
			
				$html = file_get_contents($html_template_path);
							
				//footer variable
				global $schedule_data,$candidata_data,$room_data;
				$schedule_data = $scheduleData;
				$candidata_data = $candidataData;
				
				if(isset($roomData)){
					if(is_array($roomData)){
						$room_data =$roomData;
					}
				}
				
				$dompdf->load_html($html);

				$dompdf->set_paper('a4', 'potrait');
				$dompdf->render();
				
				$dompdf = $dompdf->stream(date('dmY', strtotime($schedule_data['aps_test_date']))."_".$schedule_data['al_location_name']."_posko.pdf");
			
			}catch (Exception $e) {
				$status = false;	
			}
			
			exit;	
		}
	    
		$this->view->schedule_data = $scheduleData;		
		$this->view->candidatelist = $candidataData;		
		//print_r($aptest);
	} 
}
?>