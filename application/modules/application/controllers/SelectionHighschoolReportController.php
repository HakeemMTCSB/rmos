<?php
class Application_SelectionHighschoolReportController extends Zend_Controller_Action
{

	public function reportListAction(){
	
		$this->view->title = $this->view->translate("report list");
				
		
    	
	} 
	
	public function reportStudentViewAction(){
	
		$this->view->title = $this->view->translate("pengumuman hasil seleksi");
				
		//get academic year
		$academicDB = new App_Model_Record_DbTable_AcademicYear();
		$academic_year = $academicDB->getData();		
    	$this->view->academic_year = $academic_year;
    	
    	//get academic period
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period = $periodDB->getData();
    	$this->view->period = $period;
    	
    	//get program
    	$programDB = new App_Model_Record_DbTable_Program();
    	$program = $programDB->getData();
    	$this->view->program = $program;
    	
    	$form = new Application_Form_HighSchoolSelectionSearch();
    	$this->view->form=$form;    	
    	
	} 
	
	public function printStudentViewAction(){
	
		$this->_helper->layout->setLayout('print');
		
		
    	$academic_year = $this->_getParam('academic_year', 0);
    	$this->view->ayear = $academic_year;
    	
    	$period = $this->_getParam('period', 0);
    	$this->view->period = $period;
    	
    	//get periode
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period_info = $periodDB->getData($period);    
    	$this->view->period_name = $period_info["ap_desc"];
    	
    	$condition=array('admission_type'=>2,
						 "academic_year"=>$academic_year,
						 "period"=>$period,						
						 'status'=>'OFFER',
			 			 );
    	
		//$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
		$applicant_data = $transactionDB->getResultSelection($condition);	
		$this->view->applicant = $applicant_data;
		
		//get academic year
    	$academicDB = new App_Model_Record_DbTable_AcademicYear();
    	$academic_year_data = $academicDB->getData($academic_year);
		$academic_year_info=	$academic_year_data["ay_code"];	
    	$this->view->current_academic_year = $academic_year_info;
    	
	} 
	
	public function reportDeanViewAction(){
	
		$this->view->title = $this->view->translate("pengumuman hasil seleksi");
				
		//get academic year
		$academicDB = new App_Model_Record_DbTable_AcademicYear();
		$academic_year = $academicDB->getData();		
    	$this->view->academic_year = $academic_year;
    	
    	//get academic period
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period = $periodDB->getData();
    	$this->view->period = $period;
    	
    	//get program
    	$programDB = new App_Model_Record_DbTable_Program();
    	$program = $programDB->getData();
    	$this->view->program = $program;
    	
    	$form = new Application_Form_HighSchoolDeanSelectionSearch();
    	$this->view->form=$form;    	
    	
	} 
	
	public function printDeanViewAction(){
	
		$this->_helper->layout->setLayout('print');		
		
    	$academic_year = $this->_getParam('academic_year', 0);
    	$this->view->ayear = $academic_year;
    	
    	$period = $this->_getParam('period', 0);
    	$this->view->period = $period;
    	
    	//get periode
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period_info = $periodDB->getData($period);    
    	$this->view->period_name = $period_info["ap_desc"];
    	
    	$condition=array('admission_type'=>2,
							"academic_year"=>$academic_year,
							"period"=>$period,							
							'status'=>'OFFER',
			 				);
    	
		$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
		$applicant_data = $transactionDB->getResultSelection($condition);	
		$this->view->applicant = $applicant_data;
		
		//get academic year
    	$academicDB = new App_Model_Record_DbTable_AcademicYear();
    	$academic_year_data = $academicDB->getData($academic_year);
		$academic_year_info=	$academic_year_data["ay_code"];	
    	$this->view->current_academic_year = $academic_year_info;
    	
	} 
	
	public function raportPssbAction(){
	
		$this->view->title = $this->view->translate("Hasil Seleksi PSSB");	
    	
    	$form = new Application_Form_SearchRaportPssb();
    	$this->view->form=$form;    	
    	
	} 
	
	
public function printRaportPssbAction(){
		
				
		$this->_helper->layout->setLayout('print');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		$academic_year = $this->_getParam('academic_year', null);
    	$this->view->ayear = $academic_year;
    	
    	$period_id = $this->_getParam('period', null);
    	$this->view->period_id = $period_id;
    	
    	$nomor= $this->_getParam('nomor', null);
    	$this->view->nomor = $nomor;
    	
		$faculty_id = $this->_getParam('faculty', null);
		$this->view->faculty_id = $faculty_id;
				
		$collegeDB = new App_Model_General_DbTable_Collegemaster();
		$faculty = $collegeDB->getData($faculty_id);
		
			if($locale=="id_ID"){
				$college_name = $faculty["ArabicName"];				
			}elseif($locale=="en_US"){
				$college_name = $faculty["CollegeName"];				
			}
			
		$this->view->faculty_name = $college_name;
	
		
		//get program based on faculty    	
    	$programDB = new App_Model_Record_DbTable_Program();
    	$condition = array("IdCollege"=>$faculty_id);    	
    	$program = $programDB->searchAllProgram($condition);
    	$this->view->program_list = $program;
    	    	
    	
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period = $periodDB->getData($period_id);
    	    
    	setlocale (LC_ALL, $locale);
    	$this->view->bulan = strftime('%B', mktime(null, null, null, $period["ap_month"], 01));
    	    	
    	$this->view->periode = $period["ap_desc"];
    	
    	$assessmentDB = new App_Model_Application_DbTable_ApplicantAssessment();
		$this->view->faculty_total_offer  = $assessmentDB->getTotalOffer($period_id,$nomor,$faculty_id);
		
		$transctionDB = new App_Model_Application_DbTable_ApplicantTransaction();
		$this->view->faculty_total_reject = $transctionDB->getTotalReject($period_id,$nomor,$faculty_id);
		
		//get dean name
		$deanDB = new App_Model_General_DbTable_DeanList();
		$dean = $deanDB->getDeanByCollege($faculty_id);
		
		//get salutation
		$definationDB =  new App_Model_General_DbTable_Definationms();
		$defination1=$definationDB->getData($dean["FrontSalutation"]);			
		$defination2=$definationDB->getData($dean["BackSalutation"]);
		
		$this->view->dean_name = $defination1["DefinitionCode"].' '.$dean["FullName"].' '.$defination2["DefinitionCode"];
    	
	}
	
	
	
	
	public function searchFormAction(){
		
		$session = new Zend_Session_Namespace('sis');
		
		$this->view->title = $this->view->translate("Senarai Keputusan Wakil Rektor (PSSB)");
		    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
    	$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
    	
    	$academic_year_selected = null;
    	$nomor = $this->_getParam('nomor', null);
    	
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			$form = new Application_Form_SuratKeputusan();
			
			if ($form->isValid($formData)) {
				$academic_year_selected = $formData['academic_year'];
			}
			
    	}else{
    		
			$academicYear = $academicYearDb->getNextAcademicYearData();
			$form = new Application_Form_SuratKeputusan(array('academicyear'=>$academicYear['ay_id']));
			
			$academic_year_selected = $academicYear['ay_id'];
    	}
    	
    	$this->view->form=$form;
    	
    	//academic year data
    	$academicYear = $academicYearDb->getData($academic_year_selected);
    	$this->view->academic_year = $academicYear; 

    	/**
    	 * get data
    	 */
    	$db = Zend_Db_Table::getDefaultAdapter();
		
	    $select = $db ->select()
					  ->from(array('as'=>'applicant_assessment'), array())
					  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('asd.asd_nomor','asd.asd_decree_date'))
					  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_academic_year'))
					  //->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
					  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
					  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
					  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
					  ->order("c.CollegeCode")
					  ->order("asd.asd_decree_date desc")
					 // ->group('p.ap_id')
					  ->group('c.IdCollege')
					  ->group('asd.asd_nomor');
					  
		if($academic_year_selected){	
			$select->where("at.at_academic_year = ".$academic_year_selected);
		}
		
		if($nomor!=null){	
			$select->where("asd.asd_nomor = '".$nomor."'");
		}
		
			
		//echo $select;
		//FACULTY DEAN atau FACULTY ADMIN nampak faculty dia sahaja
		if($session->IdRole == 311 || $session->IdRole == 298){ 
			$select->where("c.IdCollege = '".$session->idCollege."'");		
    	}else{
    		if(isset($formData["faculty"]) && $formData["faculty"]!=""){
    			$select->where("c.IdCollege = '".$formData["faculty"]."'");
    		}
    	}
		$stmt = $db->query($select);
        $row = $stmt->fetchAll();
        
       
        //Offer / Reject
        if($row){
	        foreach ($row as $key=>$data){
	        	//offer
				$bil_applicant = $db ->select()
							  ->from(array('as'=>'applicant_assessment'), array('(as.aar_trans_id)txn_id','asd.asd_nomor'))
							  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('asd.asd_decree_date'))
							  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_intake'))
							 // ->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
							//  ->where('at.at_period = '.$data['ap_id'])
							  ->where('c.IdCollege = '.$data['IdCollege'])
							  ->where("asd.asd_nomor = '".$data['asd_nomor']."'")
							  ->where("asd.asd_decree_date = '".$data['asd_decree_date']."'")
							  //->where("at.at_status in ('OFFER')")
							  ->where("as.aar_dean_status = 1")
							  ->where("at.at_appl_type = 2")
							  ->group('as.aar_trans_id');
				
					    
				$row_bil = $db->fetchAll($bil_applicant);
				//$row[$key]['offer'] = $row_bil;
				
				if($row_bil){
					$row[$key]['bil_offer'] = sizeof($row_bil);
				}else{
					$row[$key]['bil_offer'] = 0;
				}
				
	        	//reject asal
				/*$bil_applicant = $db ->select()
							  //->from(array('as'=>'applicant_assessment'), array('(as.aar_trans_id)txn_id','asd.asd_nomor'))
							  //->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('asd.asd_decree_date'))
							  ->from(array('at'=>'applicant_transaction'),array('at.at_intake'))
							 // ->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
							 // ->where('at.at_period = '.$data['ap_id'])
							  ->where('c.IdCollege = '.$data['IdCollege'])
							  //->where("asd.asd_nomor = '".$data['asd_nomor']."'")
							  ->where("at.at_status in ('REJECT')")
							  ->where("at.at_appl_type = 2")
							  ->group('at.at_intake');*/
				
				$bil_applicant = $db ->select()
							  ->from(array('as'=>'applicant_assessment'), array('(as.aar_trans_id)txn_id','asd.asd_nomor'))
							  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('asd.asd_decree_date'))
							  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_intake'))							
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))						
							  ->where('c.IdCollege = '.$data['IdCollege'])
							  ->where("asd.asd_nomor = '".$data['asd_nomor']."'")
							  ->where("asd.asd_decree_date = '".$data['asd_decree_date']."'")
							  //->where("at.at_status in ('REJECT')")
							  ->where("as.aar_dean_status = 2")
							  ->where("at.at_appl_type = 2")
							  ->group('as.aar_trans_id');

							  
				$row_bil = $db->fetchAll($bil_applicant);
				//$row[$key]['reject'] = $row_bil;
				
				if($row_bil){
					$row[$key]['bil_reject'] = sizeof($row_bil);
				}else{
					$row[$key]['bil_reject'] = 0;
				}
				
	        				$bil_applicant = $db ->select()
							  ->from(array('as'=>'applicant_assessment'), array('(as.aar_trans_id)txn_id','asd.asd_nomor'))
							  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('asd.asd_decree_date'))
							  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_intake'))							
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))						
							  ->where('c.IdCollege = '.$data['IdCollege'])
							  ->where("asd.asd_nomor = '".$data['asd_nomor']."'")
							  ->where("asd.asd_decree_date = '".$data['asd_decree_date']."'")
							  //->where("at.at_status in ('REJECT')")
							  ->where("as.aar_dean_status = 4")
							  ->where("at.at_appl_type = 2")
							  ->group('as.aar_trans_id');

							  
				$row_bil = $db->fetchAll($bil_applicant);
				//$row[$key]['reject'] = $row_bil;
				
				if($row_bil){
					$row[$key]['bil_next'] = sizeof($row_bil);
				}else{
					$row[$key]['bil_next'] = 0;
				}				
	        }
		}
	        
    	$this->view->selection_detail = $row;
    	   	
				
	}
	
	
	
	public function cetakSuratKeputusanAction(){
	
		$this->_helper->layout->setLayout('print');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale =$locale;
		
    	$intake = $this->_getParam('intake', null);    	
    	
    	//$period = $this->_getParam('period', null);
    	
    	$asd_id = $this->_getParam('nomor', null);
    		
    	$facuty_id = $this->_getParam('facuty_id', null);  
    	
    	
    	
    	//get nomor
    	$selectionDB = new App_Model_Application_DbTable_ApplicantSelectionDetl();
    	$nomor = $selectionDB->getData($asd_id);    
    	$this->view->nomor = $nomor["asd_nomor"];
    	$this->view->decree_date = $nomor["asd_decree_date"];
    	
    	//get academic year
    	$academicDB = new App_Model_Record_DbTable_AcademicYear();
    	$academic_year_data = $academicDB->getData($nomor["asd_academic_year"]);
		$this->view->academic_year = $academic_year_data["ay_code"];
		
		//get periode
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period_info = $periodDB->getData($nomor["asd_period_id"]);    
    	$this->view->period_name = $period_info["ap_desc"];
    	
    	
		if($faculty_id!=null){
			//get faculty name
			$facultyDB = new App_Model_General_DbTable_Collegemaster();
			$faculty = $facultyDB->getData($faculty_id);
			
			if($locale=="id_ID"){
				$this->view->faculty_name = $faculty["ArabicName"];
			}else{
				$this->view->faculty_name = $faculty["CollegeName"];
			}
    	}else{
    		
    		$this->view->faculty_name = $this->view->translate("All");
    	}
    	
    	$form = new Application_Form_SuratKeputusan();
    	    	
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) {
				
				$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
				$applicant_list = $transactionDb->getApplicantByNomor($asd_id);
				$this->view->applicant = $applicant_list;
			}
    	}
    	
	}//end function 
	
	
	public function ajaxGetPeriodAction(){
    	$intake_id = $this->_getParam('intake_id', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select(array('ap_id','ap_desc'))
	                 ->from(array('ap'=>'tbl_academic_period'))
	                 ->order('ap.ap_year')
	                 ->order('ap.ap_number');
	    
	    if($intake_id!=0){
	    	$select->where('ap.ap_intake_id = ?', $intake_id);
	    }
		
	    
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
    
	public function ajaxGetNomorAction(){
		
		$session = new Zend_Session_Namespace('sis'); 
		 
    	$academic_year = $this->_getParam('academic_year', 0);
    	$faculty = $this->_getParam('faculty', null);
    	 
		//FACULTY DEAN atau FACULTY ADMIN nampak faculty dia sahaja
		if($session->IdRole == 311 || $session->IdRole == 298){ 
			$faculty = $session->idCollege;				
    	}
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	/*$select = $db->select()
	                 ->from(array('asd'=>'applicant_selection_detl'),array('DISTINCT(asd.asd_nomor)'))
	                 ->where('asd.asd_type = 2');*/
	  	
	  	if(isset($faculty) && $faculty!=null){
	  		$select = $db ->select()
						  ->from(array('as'=>'applicant_assessment'), array())
						  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('DISTINCT(asd.asd_nomor)'))
						  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array())
						  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
						  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code',array())
						  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array())
						  ->order("asd.asd_decree_date")						 
						  ->where("at.at_academic_year = '".$academic_year."'")
						  ->where("c.IdCollege = '".$faculty."'");
		   //  echo $select;
	        $stmt = $db->query($select);
	        $row = $stmt->fetchAll();
	  	}else{
		  	$select = $db ->select()
						  ->from(array('as'=>'applicant_assessment'), array())
						  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('DISTINCT(asd.asd_nomor)'))
						  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array())						  
						  ->where("at.at_academic_year = ".$academic_year);
		    //echo $select;
	        $stmt = $db->query($select);
	        $row = $stmt->fetchAll();
	  	}
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
public function ajaxGetDeanNomorAction(){
	
	     $session = new Zend_Session_Namespace('sis'); 
	     
    	 $academic_year = $this->_getParam('academic_year', null);
    	 
    	 $faculty = $this->_getParam('faculty', null);
    	 
		//FACULTY DEAN atau FACULTY ADMIN nampak faculty dia sahaja
		if($session->IdRole == 311 || $session->IdRole == 298){ 
			$faculty = $session->idCollege;				
    	}
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	/*$select = $db->select()
	                 ->from(array('asd'=>'applicant_selection_detl'),array('DISTINCT(asd.asd_nomor)'))
	                 ->where('asd.asd_type = 2');*/
	  	
	  	if(isset($faculty) && $faculty!=null){
	  		$select = $db ->select()
						  ->from(array('as'=>'applicant_assessment'), array())
						  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_dean_selectionid', array('DISTINCT(asd.asd_nomor)'))
						  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array())
						  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
						  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code',array())
						  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array())
						  ->where("at.at_academic_year = '".$academic_year."'")
						  ->where("c.IdCollege = '".$faculty."'");
		    
	        $stmt = $db->query($select);
	        $row = $stmt->fetchAll();
	  	}else{
		  	$select = $db ->select()
						  ->from(array('as'=>'applicant_assessment'), array())
						  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_dean_selectionid', array('DISTINCT(asd.asd_nomor)'))
						  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array())
						  ->where("at.at_academic_year = '".$academic_year."'");
		    
	        $stmt = $db->query($select);
	        $row = $stmt->fetchAll();
	  	}
	  	
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
    
    
    public function cetakSuratKeputusanDiterimaAction(){
	
		$this->_helper->layout->setLayout('print');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale =$locale;
		
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
						
			$this->view->asd_nomor = $formData['nomor'];
			$this->view->asd_decree_date = $formData['decree_date'];
			
	    	//academic year data
	    	$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
	    	$academicYear = $academicYearDb->getData($formData['academic_year']);
	    	$this->view->academic_year = $academicYear; 
	    	
	    	
	    	//get LOCALE selection period	    	
			setlocale(LC_ALL, 'id_ID');
			$month = strftime("%B",strtotime($formData["decree_date"]));
			$year =  date("Y",strtotime($formData["decree_date"]));
			$this->view->selection_period = "Periode ".$month.' '.$year;
	
	    	/**
	    	 * get data
	    	 */
	    	$db = Zend_Db_Table::getDefaultAdapter();
			
		    $select = $db ->select()
						  ->from(array('as'=>'applicant_assessment'), array('as.aar_rating_rector'))
						  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('asd_nomor','asd_decree_date'))
						  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_academic_year','txn_id'=>'at.at_trans_id','at.at_intake','at.at_pes_id'))
						  ->join(array('aprof'=>'applicant_profile'),'aprof.appl_id = at.at_appl_id')
						  //->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
						  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
						  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
						  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
						  //->group('p.ap_id')
						  ->where("at.at_status in ('OFFER')")
						  ->order('pr.ProgramCode')
						  ->order('as.aar_dean_selectionid');
						
						  
			if($academicYear){	
				$select->where("at.at_academic_year = ".$academicYear['ay_id']);
			}
			
			if( isset($formData['nomor']) ){	
				$select->where("asd.asd_nomor = '".$formData['nomor']."'");
			}
			
			if( isset($formData['faculty']) ){ 
				$select->where("c.IdCollege = '".$formData['faculty']."'");		
	    	}
	    	
    		if( isset($formData['decree_date']) ){ 
				$select->where("asd.asd_decree_date = '".$formData['decree_date']."'");		
	    	}
	    	
			$row = $db->fetchAll($select);
	       
			//echo $select;
	       
			if($row){
				$i=0;
				foreach($row as $appl){
					
					$dean_applicant= $db ->select()
										 ->from(array('aar'=>'applicant_assessment'))
										 ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = aar.aar_dean_selectionid', array('asd_nomor','asd_decree_date'))
										 ->where('aar.aar_trans_id = ?',$appl["txn_id"]);
					
					$row_dean = $db->fetchRow($dean_applicant);
					$row[$i]["asd_dean_nomor"] = $row_dean["asd_nomor"];
					
				$i++;}
			}
	        
	        //exit;
	        
	        //Offer / Reject
	      /*  if($row){

	        	//offer
				$bil_applicant = $db ->select()
							  ->from(array('as'=>'applicant_assessment'), array('DISTINCT (at.at_trans_id)txn_id','as.aar_rating_rector','asd.asd_nomor'))
							  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('asd.asd_decree_date'))
							  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_intake','at.at_pes_id'))
							  ->join(array('aprof'=>'applicant_profile'),'aprof.appl_id = at.at_appl_id')
							  //->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
							  //->where('at.at_period = '.$row['ap_id'])
							  ->where('c.IdCollege = '.$row['IdCollege'])
							  ->where("asd.asd_nomor = '".$row['asd_nomor']."'")
							  ->where("asd.asd_decree_date = '".$row['asd_decree_date']."'")
							  ->where("at.at_status in ('OFFER')")
							  ->group('as.aar_trans_id')
							  ->order('pr.ArabicName');
				
						    
				$row_bil = $db->fetchAll($bil_applicant);
				$row['offer'] = $row_bil;
				
				if($row_bil){
					$row['bil_offer'] = sizeof($row_bil);
				}else{
					$row['bil_offer'] = 0;
				}
		        
	        }*/
	        
			$this->view->applicant = $row;
		
    	}
    	
	}//end function 
	
   public function cetakSuratKeputusanTahap1Action(){
	
		$this->_helper->layout->setLayout('print');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale =$locale;
		
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
						
			$this->view->asd_nomor = $formData['nomor'];
			$this->view->asd_decree_date = $formData['decree_date'];
			
	    	//academic year data
	    	$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
	    	$academicYear = $academicYearDb->getData($formData['academic_year']);
	    	$this->view->academic_year = $academicYear; 
	    	
	    	
	    	//get LOCALE selection period	    	
			setlocale(LC_ALL, 'id_ID');
			$month = strftime("%B",strtotime($formData["decree_date"]));
			$year =  date("Y",strtotime($formData["decree_date"]));
			$this->view->selection_period = "Periode ".$month.' '.$year;
	
	    	/**
	    	 * get data
	    	 */
	    	$db = Zend_Db_Table::getDefaultAdapter();
			
		    $select = $db ->select()
						  ->from(array('as'=>'applicant_assessment'), array('as.aar_rating_rector'))
						  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('asd_nomor','asd_decree_date'))
						  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_academic_year','txn_id'=>'at.at_trans_id','at.at_intake','at.at_pes_id'))
						  ->join(array('aprof'=>'applicant_profile'),'aprof.appl_id = at.at_appl_id')
						  //->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
						  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
						  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
						  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
						  //->group('p.ap_id')
						  //->where("at.at_status in ('OFFER')")
						  ->where("as.aar_rector_status=4")
						  ->order('pr.ProgramCode')
						  ->order('as.aar_dean_selectionid');
						
						  
			if($academicYear){	
				$select->where("at.at_academic_year = ".$academicYear['ay_id']);
			}
			
			if( isset($formData['nomor']) ){	
				$select->where("asd.asd_nomor = '".$formData['nomor']."'");
			}
			
			if( isset($formData['faculty']) ){ 
				$select->where("c.IdCollege = '".$formData['faculty']."'");		
	    	}
	    	
    		if( isset($formData['decree_date']) ){ 
				$select->where("asd.asd_decree_date = '".$formData['decree_date']."'");		
	    	}
	    	
			$row = $db->fetchAll($select);
	       
			//echo $select;
	       
			if($row){
				$i=0;
				foreach($row as $appl){
					
					$dean_applicant= $db ->select()
										 ->from(array('aar'=>'applicant_assessment'))
										 ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = aar.aar_dean_selectionid', array('asd_nomor','asd_decree_date'))
										 ->where('aar.aar_trans_id = ?',$appl["txn_id"]);
					
					$row_dean = $db->fetchRow($dean_applicant);
					$row[$i]["asd_dean_nomor"] = $row_dean["asd_nomor"];
					
				$i++;}
			}
	        
	        //exit;
	        
	        //Offer / Reject
	      /*  if($row){

	        	//offer
				$bil_applicant = $db ->select()
							  ->from(array('as'=>'applicant_assessment'), array('DISTINCT (at.at_trans_id)txn_id','as.aar_rating_rector','asd.asd_nomor'))
							  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('asd.asd_decree_date'))
							  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_intake','at.at_pes_id'))
							  ->join(array('aprof'=>'applicant_profile'),'aprof.appl_id = at.at_appl_id')
							  //->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
							  //->where('at.at_period = '.$row['ap_id'])
							  ->where('c.IdCollege = '.$row['IdCollege'])
							  ->where("asd.asd_nomor = '".$row['asd_nomor']."'")
							  ->where("asd.asd_decree_date = '".$row['asd_decree_date']."'")
							  ->where("at.at_status in ('OFFER')")
							  ->group('as.aar_trans_id')
							  ->order('pr.ArabicName');
				
						    
				$row_bil = $db->fetchAll($bil_applicant);
				$row['offer'] = $row_bil;
				
				if($row_bil){
					$row['bil_offer'] = sizeof($row_bil);
				}else{
					$row['bil_offer'] = 0;
				}
		        
	        }*/
	        
			$this->view->applicant = $row;
		
    	}
    	
	}//end function 	
	public function cetakSuratKeputusanDitolakAction(){
	
	$this->_helper->layout->setLayout('print');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale =$locale;
		
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
						
	    	//academic year data
	    	$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
	    	$academicYear = $academicYearDb->getData($formData['academic_year']);
	    	$this->view->academic_year = $academicYear; 
	
	    	//get LOCALE selection period	    	
			setlocale(LC_ALL, 'id_ID');
			$month = strftime("%B",strtotime($formData["decree_date"]));
			$year =  date("Y",strtotime($formData["decree_date"]));
			$this->view->selection_period = "Periode ".$month.' '.$year;
			
	    	/**
	    	 * get data
	    	 */
	    	$db = Zend_Db_Table::getDefaultAdapter();
			
		    $select = $db ->select()
						  ->from(array('as'=>'applicant_assessment'), array())
						  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('asd.asd_nomor','asd.asd_decree_date'))
						  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_academic_year'))
						  //->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
						  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
						  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
						  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
						  //->group('p.ap_id')
						  ->group('c.IdCollege')
						  ->group('asd.asd_nomor');
						  
			if($academicYear){	
				$select->where("at.at_academic_year = ".$academicYear['ay_id']);
			}
			
			if( isset($formData['nomor']) ){	
				$select->where("asd.asd_nomor = '".$formData['nomor']."'");
			}
			
			if( isset($formData['faculty']) ){ 
				$select->where("c.IdCollege = '".$formData['faculty']."'");		
	    	}
	    	
    		if( isset($formData['decree_date']) ){ 
				$select->where("asd.asd_decree_date = '".$formData['decree_date']."'");		
	    	}
	    	
    		/*if( isset($formData['period']) ){ 
				$select->where("p.ap_id = ".$formData['period']);		
	    	}*/
	    	
			$stmt = $db->query($select);
	        $row = $stmt->fetch();
	        
	        
	        //exit;
	        
	        //Offer / Reject
	        if($row){

	        	//offer
				$bil_applicant = $db ->select()
							  ->from(array('as'=>'applicant_assessment'), array('DISTINCT (at.at_trans_id)txn_id','as.aar_rating_rector','asd.asd_nomor'))
							  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('asd.asd_decree_date'))
							  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_intake','at.at_pes_id'))
							  ->join(array('aprof'=>'applicant_profile'),'aprof.appl_id = at.at_appl_id')
							  //->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
							 // ->where('at.at_period = '.$row['ap_id'])
							  ->where('c.IdCollege = '.$row['IdCollege'])
							  ->where("asd.asd_nomor = '".$row['asd_nomor']."'")
							  ->where("asd.asd_decree_date = '".$row['asd_decree_date']."'")
							  ->where("at.at_status in ('REJECT')")
							  ->group('as.aar_trans_id')
							  ->order('pr.ArabicName');
				
						    
				$row_bil = $db->fetchAll($bil_applicant);
				$row['reject'] = $row_bil;
				
				if($row_bil){
					$row['bil_reject'] = sizeof($row_bil);
				}else{
					$row['bil_reject'] = 0;
				}
		        
	        }
	        

			$this->view->applicant = $row;
		
    	}
    	
	}//end function 
	
	
	
public function deanSearchFormAction(){
		
		$this->view->title = $this->view->translate("Senarai Keputusan Dekan Fakultas");
				
		$session = new Zend_Session_Namespace('sis');    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
    	$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
    	
    	$academic_year_selected = null;
    	$nomor = $this->_getParam('nomor', null);
    	$faculty = $this->_getParam('faculty', null);
    	
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			$form = new Application_Form_SuratKeputusanDekan();
			
			if ($form->isValid($formData)) {
				$academic_year_selected = $formData['academic_year'];
			}
			
    	}else{
    		
			$academicYear = $academicYearDb->getNextAcademicYearData();
			$form = new Application_Form_SuratKeputusanDekan(array('academicyear'=>$academicYear['ay_id']));
			
			$academic_year_selected = $academicYear['ay_id'];
    	}
    	
    	$this->view->form=$form;
    	
    	//academic year data
    	$academicYear = $academicYearDb->getData($academic_year_selected);
    	$this->view->academic_year = $academicYear; 

    	/**
    	 * get data
    	 */
    	$db = Zend_Db_Table::getDefaultAdapter();
		
	    $select = $db ->select()
					  ->from(array('as'=>'applicant_assessment'), array())
					  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_dean_selectionid', array('asd.asd_nomor','asd.asd_decree_date'))
					  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_academic_year'))
					  ->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
					  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
					  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
					  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
					  ->order("c.IdCollege")
					  ->order("asd.asd_decree_date")
					  ->group('p.ap_id')
					  ->group('c.IdCollege')
					  ->group('asd.asd_nomor');
					  
		if($academic_year_selected){	
			$select->where("at.at_academic_year = ".$academic_year_selected);
		}
		
		if($nomor!=null){	
			$select->where("asd.asd_nomor = '".$nomor."'");
		}
		
		if($faculty!=null){	
			$select->where("c.IdCollege = '".$faculty."'");
		}
		
			
		//FACULTY DEAN atau FACULTY ADMIN nampak faculty dia sahaja
		if($session->IdRole == 311 || $session->IdRole == 298){ 
			$select->where("c.IdCollege = '".$session->idCollege."'");		
    	}
    	
    	//echo $select;
		$stmt = $db->query($select);
        $row = $stmt->fetchAll();
        
       
        //Offer / Reject
        if($row){
	        foreach ($row as $key=>$data){
	        	//offer
				$bil_applicant = $db ->select()
							  ->from(array('as'=>'applicant_assessment'), array('(as.aar_trans_id)txn_id','asd.asd_nomor'))
							  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_dean_selectionid', array('asd.asd_decree_date'))
							  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_intake'))
							  ->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
							  ->where('at.at_period = '.$data['ap_id'])
							  ->where('c.IdCollege = '.$data['IdCollege'])
							  ->where("asd.asd_nomor = '".$data['asd_nomor']."'")
							  //->where("at.at_status in ('OFFER')")
							  ->where("as.aar_dean_status = 1")
							  ->where("at.at_appl_type = 2")
							  ->group('as.aar_trans_id');
				
						    
				$row_bil = $db->fetchAll($bil_applicant);
				//$row[$key]['offer'] = $row_bil;
				
				if($row_bil){
					$row[$key]['bil_offer'] = sizeof($row_bil);
				}else{
					$row[$key]['bil_offer'] = 0;
				}
				
	        	//reject

				$bil_applicant = $db ->select()
							  ->from(array('as'=>'applicant_assessment'), array('(as.aar_trans_id)txn_id','asd.asd_nomor'))
							  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_dean_selectionid', array('asd.asd_decree_date'))
							  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_intake'))
							  ->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
							  ->where('at.at_period = '.$data['ap_id'])
							  ->where('c.IdCollege = '.$data['IdCollege'])
							  ->where("asd.asd_nomor = '".$data['asd_nomor']."'")
							  //->where("at.at_status in ('OFFER')")
							  ->where("as.aar_dean_status = 2")
							  ->where("at.at_appl_type = 2")
							  ->group('as.aar_trans_id');				
						    
				$row_bil = $db->fetchAll($bil_applicant);
				//$row[$key]['reject'] = $row_bil;
				
				if($row_bil){
					$row[$key]['bil_reject'] = sizeof($row_bil);
				}else{
					$row[$key]['bil_reject'] = 0;
				}
				
				//Lulus tahap pertama
	        				$bil_applicant = $db ->select()
							  ->from(array('as'=>'applicant_assessment'), array('(as.aar_trans_id)txn_id','asd.asd_nomor'))
							  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_dean_selectionid', array('asd.asd_decree_date'))
							  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_intake'))
							  ->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
							  ->where('at.at_period = '.$data['ap_id'])
							  ->where('c.IdCollege = '.$data['IdCollege'])
							  ->where("asd.asd_nomor = '".$data['asd_nomor']."'")
							  //->where("at.at_status in ('OFFER')")
							  ->where("as.aar_dean_status = 4")
							  ->where("at.at_appl_type = 2")
							  ->group('as.aar_trans_id');				
						    
				$row_bil = $db->fetchAll($bil_applicant);
				//$row[$key]['reject'] = $row_bil;
				
				if($row_bil){
					$row[$key]['bil_next'] = sizeof($row_bil);
				}else{
					$row[$key]['bil_next'] = 0;
				}				
				
	        }
	        
	        
		}
	        
		
    	$this->view->selection_detail = $row;	
				
	}
	
	
	public function cetakSuratDekanDiterimaAction(){
	
		$this->_helper->layout->setLayout('print');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale =$locale;
		
		$faculty_id = $this->_getParam('faculty_id', null);  
    	$this->view->faculty_id = $faculty_id;
		
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
						
	    	//academic year data
	    	$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
	    	$academicYear = $academicYearDb->getData($formData['academic_year']);
	    	$this->view->academic_year = $academicYear; 
	
	    	/**
	    	 * get data
	    	 */
	    	$db = Zend_Db_Table::getDefaultAdapter();
			
		    $select = $db ->select()
						  ->from(array('as'=>'applicant_assessment'), array())
						  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_dean_selectionid', array('asd.asd_nomor','asd.asd_decree_date'))
						  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_academic_year'))
						  ->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
						  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
						  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
						  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
						  ->group('p.ap_id')
						  ->group('c.IdCollege')
						  ->group('asd.asd_nomor');
						  
			if($academicYear){	
				$select->where("at.at_academic_year = ".$academicYear['ay_id']);
			}
			
			if( isset($formData['nomor']) ){	
				$select->where("asd.asd_nomor = '".$formData['nomor']."'");
			}
			
			if( isset($formData['faculty']) ){ 
				$select->where("c.IdCollege = '".$formData['faculty']."'");		
	    	}
	    	
    		if( isset($formData['period']) ){ 
				$select->where("p.ap_id = ".$formData['period']);		
	    	}
	    	
			$stmt = $db->query($select);
	        $row = $stmt->fetch();
	        
	        
	        //exit;
	        
	        //Offer / Reject
	        if($row){

	        	//offer
				$bil_applicant = $db ->select()
							  ->from(array('as'=>'applicant_assessment'), array('DISTINCT (at.at_trans_id)txn_id','as.aar_rating_dean','asd.asd_nomor'))
							  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_dean_selectionid', array('asd.asd_decree_date'))
							  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_intake','at.at_pes_id'))
							  ->join(array('aprof'=>'applicant_profile'),'aprof.appl_id = at.at_appl_id')
							  ->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
							  ->where('at.at_period = '.$row['ap_id'])
							  ->where('c.IdCollege = '.$row['IdCollege'])
							  ->where("asd.asd_nomor = '".$row['asd_nomor']."'")
							  //->where("at.at_status in ('OFFER')")
							  ->where("as.aar_dean_status = 1")
							  ->group('as.aar_trans_id')
							  ->order('pr.ArabicName');
				
						    
				$row_bil = $db->fetchAll($bil_applicant);
				$row['offer'] = $row_bil;
				
				if($row_bil){
					$row['bil_offer'] = sizeof($row_bil);
				}else{
					$row['bil_offer'] = 0;
				}
		        
	        }
	        
	        
			$this->view->applicant = $row;
			
    		if($faculty_id!=null){
				//get faculty name
				$facultyDB = new App_Model_General_DbTable_Collegemaster();
				$faculty = $facultyDB->getData($faculty_id);
				
				if($locale=="id_ID"){
					$this->view->title = $this->view->translate("Surat Dekan Fakultas").' '.$faculty["ArabicName"];
					$this->view->faculty_name = $faculty["ArabicName"];
				}else{
					$this->view->title = $this->view->translate("Surat Dekan Fakultas").' '.$faculty["CollegeName"];
					$this->view->faculty_name = $faculty["CollegeName"];
				}
				
				
				//get dean name
				$deanDB = new App_Model_General_DbTable_DeanList();
				$dean = $deanDB->getDeanByCollege($faculty_id);
				
				//get salutation
				$definationDB =  new App_Model_General_DbTable_Definationms();
				$defination1=$definationDB->getData($dean["FrontSalutation"]);			
				$defination2=$definationDB->getData($dean["BackSalutation"]);
				
				$this->view->dean_name = $defination1["DefinitionCode"].' '.$dean["FullName"].' '.$defination2["DefinitionCode"];
				
	    	}else{    		
	    			$this->view->title = $this->view->translate("Surat Dekan Universitas Trisakti");
	    			$this->view->faculty_name = "";
	    			$this->view->dean_name = "";
	    	}
		
    	}
	}//end function 
	
	public function cetakSuratDekanTahap1Action(){
	
		$this->_helper->layout->setLayout('print');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale =$locale;
		
		$faculty_id = $this->_getParam('faculty_id', null);  
    	$this->view->faculty_id = $faculty_id;
		
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
						
	    	//academic year data
	    	$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
	    	$academicYear = $academicYearDb->getData($formData['academic_year']);
	    	$this->view->academic_year = $academicYear; 
	
	    	/**
	    	 * get data
	    	 */
	    	$db = Zend_Db_Table::getDefaultAdapter();
			
		    $select = $db ->select()
						  ->from(array('as'=>'applicant_assessment'), array())
						  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_dean_selectionid', array('asd.asd_nomor','asd.asd_decree_date'))
						  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_academic_year'))
						  ->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
						  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
						  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
						  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
						  ->group('p.ap_id')
						  ->group('c.IdCollege')
						  ->group('asd.asd_nomor');
						  
			if($academicYear){	
				$select->where("at.at_academic_year = ".$academicYear['ay_id']);
			}
			
			if( isset($formData['nomor']) ){	
				$select->where("asd.asd_nomor = '".$formData['nomor']."'");
			}
			
			if( isset($formData['faculty']) ){ 
				$select->where("c.IdCollege = '".$formData['faculty']."'");		
	    	}
	    	
    		if( isset($formData['period']) ){ 
				$select->where("p.ap_id = ".$formData['period']);		
	    	}
	    	
			$stmt = $db->query($select);
	        $row = $stmt->fetch();
	        
	        
	        //exit;
	        
	        //Offer / Reject
	        if($row){

	        	//offer
				$bil_applicant = $db ->select()
							  ->from(array('as'=>'applicant_assessment'), array('DISTINCT (at.at_trans_id)txn_id','as.aar_rating_dean','asd.asd_nomor'))
							  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_dean_selectionid', array('asd.asd_decree_date'))
							  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_intake','at.at_pes_id'))
							  ->join(array('aprof'=>'applicant_profile'),'aprof.appl_id = at.at_appl_id')
							  ->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
							  ->where('at.at_period = '.$row['ap_id'])
							  ->where('c.IdCollege = '.$row['IdCollege'])
							  ->where("asd.asd_nomor = '".$row['asd_nomor']."'")
							  //->where("at.at_status in ('OFFER')")
							  ->where("as.aar_dean_status = 4")
							  ->group('as.aar_trans_id')
							  ->order('pr.ArabicName');
				
						    
				$row_bil = $db->fetchAll($bil_applicant);
				$row['offer'] = $row_bil;
				
				if($row_bil){
					$row['bil_offer'] = sizeof($row_bil);
				}else{
					$row['bil_offer'] = 0;
				}
		        
	        }
	        
	        
			$this->view->applicant = $row;
			
    		if($faculty_id!=null){
				//get faculty name
				$facultyDB = new App_Model_General_DbTable_Collegemaster();
				$faculty = $facultyDB->getData($faculty_id);
				
				if($locale=="id_ID"){
					$this->view->title = $this->view->translate("Surat Dekan Fakultas").' '.$faculty["ArabicName"];
					$this->view->faculty_name = $faculty["ArabicName"];
				}else{
					$this->view->title = $this->view->translate("Surat Dekan Fakultas").' '.$faculty["CollegeName"];
					$this->view->faculty_name = $faculty["CollegeName"];
				}
				
				
				//get dean name
				$deanDB = new App_Model_General_DbTable_DeanList();
				$dean = $deanDB->getDeanByCollege($faculty_id);
				
				//get salutation
				$definationDB =  new App_Model_General_DbTable_Definationms();
				$defination1=$definationDB->getData($dean["FrontSalutation"]);			
				$defination2=$definationDB->getData($dean["BackSalutation"]);
				
				$this->view->dean_name = $defination1["DefinitionCode"].' '.$dean["FullName"].' '.$defination2["DefinitionCode"];
				
	    	}else{    		
	    			$this->view->title = $this->view->translate("Surat Dekan Universitas Trisakti");
	    			$this->view->faculty_name = "";
	    			$this->view->dean_name = "";
	    	}
		
    	}
	}//end functio	
	public function cetakSuratDekanDitolakAction(){
	
		$this->_helper->layout->setLayout('print');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale =$locale;
			
    	$faculty_id = $this->_getParam('faculty_id', null);  
    	$this->view->faculty_id = $faculty_id;
     	
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
						
	    	//academic year data
	    	$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
	    	$academicYear = $academicYearDb->getData($formData['academic_year']);
	    	$this->view->academic_year = $academicYear; 
	
	    	/**
	    	 * get data
	    	 */
	    	$db = Zend_Db_Table::getDefaultAdapter();
			
		    $select = $db ->select()
						  ->from(array('as'=>'applicant_assessment'), array())
						  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_dean_selectionid', array('asd.asd_nomor','asd.asd_decree_date'))
						  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_academic_year'))
						  ->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
						  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
						  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
						  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
						  ->group('p.ap_id')
						  ->group('c.IdCollege')
						  ->group('asd.asd_nomor');
						  
			if($academicYear){	
				$select->where("at.at_academic_year = ".$academicYear['ay_id']);
			}
			
			if( isset($formData['nomor']) ){	
				$select->where("asd.asd_nomor = '".$formData['nomor']."'");
			}
			
			if( isset($formData['faculty']) ){ 
				$select->where("c.IdCollege = '".$formData['faculty']."'");		
	    	}
	    	
    		if( isset($formData['period']) ){ 
				$select->where("p.ap_id = ".$formData['period']);		
	    	}
	    	
			$stmt = $db->query($select);
	        $row = $stmt->fetch();
	        
	        
	        //exit;
	        
	        //Offer / Reject
	        if($row){

	        	//offer
				$bil_applicant = $db ->select()
							  ->from(array('as'=>'applicant_assessment'), array('DISTINCT (at.at_trans_id)txn_id','as.aar_rating_dean','asd.asd_nomor'))
							  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_dean_selectionid', array('asd.asd_decree_date'))
							  ->join(array('at'=>'applicant_transaction'),'at.at_trans_id = as.aar_trans_id',array('at.at_intake','at.at_pes_id'))
							  ->join(array('aprof'=>'applicant_profile'),'aprof.appl_id = at.at_appl_id')
							  ->join(array('p'=>'tbl_academic_period'),'p.ap_id = at.at_period',array('p.ap_id','p.ap_desc'))
							  ->join(array('ap'=>'applicant_program'), 'ap.ap_at_trans_id = at.at_trans_id', array())
							  ->join(array('pr'=>'tbl_program'), 'pr.ProgramCode = ap.ap_prog_code', array('pr.ProgramCode'))
							  ->join(array('c'=>'tbl_collegemaster'), 'c.IdCollege = pr.IdCollege', array('c.IdCollege','c.ArabicName'))
							  ->where('at.at_period = '.$row['ap_id'])
							  ->where('c.IdCollege = '.$row['IdCollege'])
							  ->where("asd.asd_nomor = '".$row['asd_nomor']."'")
							  //->where("at.at_status in ('REJECT')")
							  ->where("as.aar_dean_status = 2")
							  ->group('as.aar_trans_id')
							  ->order('pr.ArabicName');
				
						    
				$row_bil = $db->fetchAll($bil_applicant);
				$row['reject'] = $row_bil;
				
				if($row_bil){
					$row['bil_reject'] = sizeof($row_bil);
				}else{
					$row['bil_reject'] = 0;
				}
		        
	        }
	        
	        
			$this->view->applicant = $row;
			
			
			if($faculty_id!=null){
				//get faculty name
				$facultyDB = new App_Model_General_DbTable_Collegemaster();
				$faculty = $facultyDB->getData($faculty_id);
				
				if($locale=="id_ID"){
					$this->view->title = $this->view->translate("Surat Dekan Fakultas").' '.$faculty["ArabicName"];
					$this->view->faculty_name = $faculty["ArabicName"];
				}else{
					$this->view->title = $this->view->translate("Surat Dekan Fakultas").' '.$faculty["CollegeName"];
					$this->view->faculty_name = $faculty["CollegeName"];
				}
				
				//get dean name
				$deanDB = new App_Model_General_DbTable_DeanList();
				$dean = $deanDB->getDeanByCollege($faculty_id);
				
				//get salutation
				$definationDB =  new App_Model_General_DbTable_Definationms();
				$defination1=$definationDB->getData($dean["FrontSalutation"]);			
				$defination2=$definationDB->getData($dean["BackSalutation"]);
				
				$this->view->dean_name = $defination1["DefinitionCode"].' '.$dean["FullName"].' '.$defination2["DefinitionCode"];
				
	    	}else{    		
	    			$this->view->title = $this->view->translate("Surat Dekan Universitas Trisakti");
	    			$this->view->faculty_name = "";
	    	}
    	}
    	    	
    	
	}//end function 
	
	
	public function ajaxGetDecreeDateAction(){
    	 $nomor = $this->_getParam('nomor', null);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select()
	                 ->from(array('asd'=>'applicant_selection_detl'),array('asd.asd_decree_date'))
	                 ->where("asd.asd_nomor = '".$nomor."'");
	                 
	  
	    $row = $db->fetchRow($select);
	    
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
    
    public function printCletterRectorAction(){
		
				
		$this->_helper->layout->setLayout('print');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		$academic_year = $this->_getParam('academic_year', null);
    	$this->view->ayear = $academic_year;
    	
    	/*$period_id = $this->_getParam('period', null);
    	$this->view->period_id = $period_id;*/
    	
    	$nomor= $this->_getParam('nomor', null);
    	$this->view->nomor = $nomor;
    	
		$faculty_id = $this->_getParam('faculty', null);
		$this->view->faculty_id = $faculty_id;
		
		$decree_date = $this->_getParam('decree_date', null);
		$this->view->decree_date = $decree_date;
		
		//get selection period
		setlocale(LC_ALL, 'id_ID');
		$month = strftime("%B",strtotime($decree_date));
		$year =  date("Y",strtotime($decree_date));
		$this->view->periode = $month;
		
		//get faculty
		$collegeDB = new App_Model_General_DbTable_Collegemaster();
		$faculty = $collegeDB->getData($faculty_id);
		
			if($locale=="id_ID"){
				$college_name = $faculty["ArabicName"];				
			}elseif($locale=="en_US"){
				$college_name = $faculty["CollegeName"];				
			}
			
		$this->view->faculty = $college_name;
	
		//get normo info
		$assessmentDB= new App_Model_Application_DbTable_ApplicantAssessment();
		$nomor_info = $assessmentDB->getRectorNomorInfo($academic_year,null,$nomor,$faculty_id,$decree_date);
		$this->view->reg_start_date = $nomor_info["aar_reg_start_date"];
		$this->view->reg_end_date = $nomor_info["aar_reg_end_date"];		
		$this->view->dean_nomor_info = $assessmentDB->getDeanNomorInfo($academic_year,null,$faculty_id,$nomor,$decree_date);
		
		//$this->view->dean_nomor =	$dean_nomor_info["asd_nomor"];
		//$this->view->dean_decree_date =	$dean_nomor_info["asd_decree_date"];
		
		//get academic year
    	$academicDB = new App_Model_Record_DbTable_AcademicYear();
    	$academic_year_data = $academicDB->getData($academic_year);
		$this->view->academic_year = $academic_year_data["ay_code"];
		
		
    	
		//get priod
    	/*$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period = $periodDB->getData($period_id);    	    
    	setlocale (LC_ALL, $locale);
    	$this->view->bulan = strftime('%B', mktime(null, null, null, $period["ap_month"], 01));*/
    	//$this->view->periode = $period["ap_desc"];
    	
	}
	
}
?>