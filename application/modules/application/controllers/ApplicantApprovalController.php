<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_ApplicantApprovalController extends Base_Base {
    
    private $_gobjlog;
    private $locale;
    private $dclModel;
    private $defModel;
    private $lobjAppItem;
    private $lobjAppConfig;
    private $stpModel;
    private $tplModel;
    private $sthModel;
    private $checklistVerification;
    private $appAprove;
    private $statusModel;
    //private $generatePdfDoc;
    
    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate'); 
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	Zend_Form::setDefaultTranslator($this->view->translate);
	$this->fnsetObj();
    }
    
    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
	$this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->dclModel = new Application_Model_DbTable_DocumentChecklist();
        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->lobjAppItem = new Application_Model_DbTable_ApplicationItem();
        $this->lobjAppConfig = new Application_Model_DbTable_ApplicationInitialConfig();
        $this->stpModel = new Application_Model_DbTable_StatusTemplate();
        $this->tplModel = new Communication_Model_DbTable_Template();
        $this->sthModel = new Application_Model_DbTable_StatusAttachment();
        $this->checklistVerification = new Application_Model_DbTable_ChecklistVerification();
        $this->appAprove = new Application_Model_DbTable_ApplicantApproval();
        $this->statusModel = new Application_Model_DbTable_Status();
        //$this->generatePdfDoc = new Application_GenerateDocument();
    }
    
    public function indexAction(){
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->view->title=$this->view->translate("Applicant Approval");
        $approvalForm = new Application_Form_ApplicantApproval();
        $getUserIdentity = $this->auth->getIdentity();
        $where = 'a.at_status != 602';

        $cacheObj = new Cms_Cache();
        $cache = $cacheObj->get();
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $formData['field1'] = $this->view->escape(strip_tags($formData['field1']));
            $formData['field7'] = $this->view->escape(strip_tags($formData['field7']));
            $formData['field3'] = $this->view->escape(strip_tags($formData['field3']));
            $formData['field2'] = $this->view->escape(strip_tags($formData['field2']));
            $formData['field4'] = $this->view->escape(strip_tags($formData['field4']));
            $formData['field9'] = $this->view->escape(strip_tags($formData['field9']));
            $formData['at_status'] = $this->view->escape(strip_tags($formData['at_status']));

            if (isset($formData['Search']) && $formData['Search']=='Search'){
                $searchForm = new Application_Form_ApplicantSearch(array('IdProgram'=>$formData['field3']));
                $searchForm->populate($formData);
                $applicantList = $this->checklistVerification->getApplicantSearchList($formData, $where);
                //$this->gobjsessionsis->applicantaprovalpaginatorresult = $applicantList;
                $cache->save($applicantList, 'applicantaprovalpaginatorresult');

                $this->_redirect('/application/applicant-approval/index/search/1');
                
            }else if (isset($formData['Approve']) && $formData['Approve']=='Approve'){
            	
                if (isset($formData['at_trans_id']) && count($formData['at_trans_id']) > 0){
                   
                    $i=0;
                    
                    foreach ($formData['at_trans_id'] as $transLoop){
                        
                        $this->offered($transLoop);
                        $i++;
                    }
                }
                $searchForm = new Application_Form_ApplicantSearch();
                $applicantList = $this->checklistVerification->fnGetApplicantList($where);
                $this->_redirect( '/application/applicant-approval/index/');
            }else{
                $searchForm = new Application_Form_ApplicantSearch();
                $applicantList = $this->checklistVerification->fnGetApplicantList($where);
                $this->_redirect( '/application/applicant-approval/index/');
            }
        }else{
            $searchForm = new Application_Form_ApplicantSearch();
            $applicantList = $this->checklistVerification->fnGetApplicantList($where);
        }
        
        if(!$this->_getParam('search')){
            //unset($this->gobjsessionsis->applicantaprovalpaginatorresult);
            $cache->remove('applicantaprovalpaginatorresult');
        }
            
        $lintpagecount = $this->gintPageCount;// Definitiontype model

        $lintpage = $this->_getParam('page',1); // Paginator instance

        $cached_results = $cache->load('applicantaprovalpaginatorresult');
        if($cached_results != '') {
            $paginator = $this->lobjCommon->fnPagination($cached_results,$lintpage,$lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($applicantList,$lintpage,$lintpagecount);
        }
        
        $paginator->setItemCountPerPage(25);
        
        $this->view->paginator = $paginator;
        $this->view->searchForm = $searchForm;
        $this->view->approvalForm = $approvalForm;
    }
    
    public function loadDocumentChecklistAjaxAction(){
        $trxId = $this->_getParam('id',null);
		
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $txnInfo = $this->checklistVerification->infoBox($trxId);
        
        $getDocumentChecklist = $this->checklistVerification->fnGetDocCheckList($txnInfo['ap_prog_scheme'], $txnInfo['appl_category'], $trxId);
			
        if (count($getDocumentChecklist)){
            $i=0;
            foreach ($getDocumentChecklist as $getDocumentCheckloop){
                $getDocChecklistFileList = Application_Model_DbTable_ChecklistVerification::universalQueryAll($getDocumentCheckloop['table_name'], '*', $getDocumentCheckloop['table_id_column'].' = '.$trxId, $getDocumentCheckloop['table_join'], $getDocumentCheckloop['join_column1'], $getDocumentCheckloop['join_column2']);
            
                if (count($getDocChecklistFileList)>0){
                    $j=0;
                    foreach ($getDocChecklistFileList as $getDocChecklistFileLoop){
                        if (count($getDocChecklistFileList)>1){
                            $getDocumentChecklist[$i]['column1'][$j] = $getDocChecklistFileLoop[$getDocumentCheckloop['table_column']];
                        }else{
                            $getDocumentChecklist[$i]['column1'][$j] = '-';
                        }
                        $getDocumentChecklist[$i]['column2'][$j] = (Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($trxId, $getDocumentCheckloop['dcl_Id'], 'ad_id', $getDocChecklistFileLoop[$getDocumentCheckloop['table_primary_id']], $getDocumentCheckloop['id']) != FALSE ? 'Uploaded':'Not Upload');
                        $getDocumentChecklist[$i]['column3'][$j] = (Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_document_status', 'ads_confirm', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocumentCheckloop['dcl_Id'].' and ads_table_id='.$getDocChecklistFileLoop[$getDocumentCheckloop['table_primary_id']].' and ads_section_id='.$getDocumentCheckloop['id']) == 1 ? 'Yes':'No');
                        $getDocChecklistStatus = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_document_status', 'ads_status', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocumentCheckloop['dcl_Id'].' and ads_table_id='.$getDocChecklistFileLoop[$getDocumentCheckloop['table_primary_id']].' and ads_section_id='.$getDocumentCheckloop['id']);

                        switch ($getDocChecklistStatus){
                            case 1:
                                $getDocumentChecklist[$i]['column4'][$j]='New';
                                break;
                            case 2:
                                $getDocumentChecklist[$i]['column4'][$j]='Viewed';
                                break;
                            case 3:
                                $getDocumentChecklist[$i]['column4'][$j]='Incomplete';
                                break;
                            case 4:
                                $getDocumentChecklist[$i]['column4'][$j]='Complete';
                                break;
                            case 5:
                                $getDocumentChecklist[$i]['column4'][$j]='Not Applicable';
                                break;
                            default:
                                $getDocumentChecklist[$i]['column4'][$j]='-';
                                break;
                        }

                        $getDocumentChecklist[$i]['column5'][$j] = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_document_status', 'ads_comment', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocumentCheckloop['dcl_Id'].' and ads_table_id='.$getDocChecklistFileLoop[$getDocumentCheckloop['table_primary_id']].' and ads_section_id='.$getDocumentCheckloop['id']);
                        $getDocumentChecklist[$i]['column5'][$j] = $getDocumentChecklist[$i]['column5'][$j] == FALSE ? '-': $getDocumentChecklist[$i]['column5'][$j];
                        $j++;
                    }
                }else{
                    $h = 0;
                    $getDocumentChecklist[$i]['column1'][$h] = '-';
                    $getDocumentChecklist[$i]['column2'][$h] = (Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_documents', 'ad_id', 'ad_txn_id='.$trxId.' and ad_dcl_id='.$getDocumentCheckloop['dcl_Id']) != FALSE ? 'Uploaded':'Not Upload');
                    $getDocumentChecklist[$i]['column3'][$h] = (Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_document_status', 'ads_confirm', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocumentCheckloop['dcl_Id']) == 1 ? 'Yes':'No');
                    $getDocChecklistStatus = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_document_status', 'ads_status', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocumentCheckloop['dcl_Id']);

                    switch ($getDocChecklistStatus){
                        case 1:
                            $getDocumentChecklist[$i]['column4'][$h]='New';
                            break;
                        case 2:
                            $getDocumentChecklist[$i]['column4'][$h]='Viewed';
                            break;
                        case 3:
                            $getDocumentChecklist[$i]['column4'][$h]='Incomplete';
                            break;
                        case 4:
                            $getDocumentChecklist[$i]['column4'][$h]='Complete';
                            break;
                        case 5:
                            $getDocumentChecklist[$i]['column4'][$h]='Not Applicable';
                            break;
                        default:
                            $getDocumentChecklist[$i]['column4'][$h]='-';
                            break;
                    }

                    $getDocumentChecklist[$i]['column5'][$h] = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_document_status', 'ads_comment', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocumentCheckloop['dcl_Id']);
                    $getDocumentChecklist[$i]['column5'][$h] = $getDocumentChecklist[$i]['column5'][$h] == FALSE ? '-': $getDocumentChecklist[$i]['column5'][$h];
                    
                }
                $i++;
            }
        }
        
        $json = Zend_Json::encode($getDocumentChecklist);
		
	echo $json;
	exit();
    }
    
    public function loadDocumentChecklistdetailAjaxAction(){
        $trxId = $this->_getParam('id',null);
        $dclId = $this->_getParam('dclId',null);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $getDocChecklistInfo = $this->appAprove->getDocChecklistInfo($dclId);
        //var_dump($getDocChecklistInfo); exit;
        $getDocChecklistFileList = Application_Model_DbTable_ChecklistVerification::universalQueryAll($getDocChecklistInfo['table_name'], '*', $getDocChecklistInfo['table_id_column'].' = '.$trxId, $getDocChecklistInfo['table_join'], $getDocChecklistInfo['join_column1'], $getDocChecklistInfo['join_column2']);
        
        if (count($getDocChecklistFileList)>0){
            $i=0;
            foreach ($getDocChecklistFileList as $getDocChecklistFileLoop){
                //if (count($getDocChecklistFileList)>1){
                  //  $getDocChecklistFileList[$i]['column1'] = $getDocChecklistFileLoop[$getDocChecklistInfo['table_column']];
                //}else{
                    $getDocChecklistFileList[$i]['column1'] = '';
                //}
                $getDocChecklistFileList[$i]['column2'] = (Application_Model_DbTable_ChecklistVerification::fnGetUploadStatus($trxId, $getDocChecklistInfo['dcl_Id'], 'ad_id', $getDocChecklistFileLoop[$getDocChecklistInfo['table_primary_id']], $getDocChecklistInfo['id']) != FALSE ? 'Uploaded':'Not Upload');
                $getDocChecklistFileList[$i]['column3'] = (Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_document_status', 'ads_confirm', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocChecklistInfo['dcl_Id'].' and ads_table_id='.$getDocChecklistFileLoop[$getDocChecklistInfo['table_primary_id']].' and ads_section_id='.$getDocChecklistInfo['id']) == 1 ? 'Yes':'No');
                $getDocChecklistStatus = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_document_status', 'ads_status', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocChecklistInfo['dcl_Id'].' and ads_table_id='.$getDocChecklistFileLoop[$getDocChecklistInfo['table_primary_id']].' and ads_section_id='.$getDocChecklistInfo['id']);
                
                switch ($getDocChecklistStatus){
                    case 1:
                        $getDocChecklistFileList[$i]['column4']='New';
                        break;
                    case 2:
                        $getDocChecklistFileList[$i]['column4']='Viewed';
                        break;
                    case 3:
                        $getDocChecklistFileList[$i]['column4']='Incomplete';
                        break;
                    case 4:
                        $getDocChecklistFileList[$i]['column4']='Complete';
                        break;
                    default:
                        $getDocChecklistFileList[$i]['column4']=' ';
                        break;
                }
                
                $getDocChecklistFileList[$i]['column5'] = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_document_status', 'ads_comment', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocChecklistInfo['dcl_Id'].' and ads_table_id='.$getDocChecklistFileLoop[$getDocChecklistInfo['table_primary_id']].' and ads_section_id='.$getDocChecklistInfo['id']);
                
                $i++;
            }
        }else{
            $i=0;
            
            $getDocChecklistFileList[$i]['column1'] = ' ';
            $getDocChecklistFileList[$i]['column2'] = (Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_documents', 'ad_id', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocChecklistInfo['dcl_Id']) != FALSE ? 'Uploaded':'Not Upload');
            $getDocChecklistFileList[$i]['column3'] = (Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_document_status', 'ads_confirm', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocChecklistInfo['dcl_Id']) == 1 ? 'Yes':'No');
            $getDocChecklistStatus = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_document_status', 'ads_status', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocChecklistInfo['dcl_Id']);

            switch ($getDocChecklistStatus){
                case 1:
                    $getDocChecklistFileList[$i]['column4']='New';
                    break;
                case 2:
                    $getDocChecklistFileList[$i]['column4']='Viewed';
                    break;
                case 3:
                    $getDocChecklistFileList[$i]['column4']='Incomplete';
                    break;
                case 4:
                    $getDocChecklistFileList[$i]['column4']='Complete';
                    break;
                default:
                    $getDocChecklistFileList[$i]['column4']=' ';
                    break;
            }

            $getDocChecklistFileList[$i]['column5'] = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_document_status', 'ads_comment', 'ads_txn_id='.$trxId.' and ads_dcl_id='.$getDocChecklistInfo['dcl_Id']);
        
        }
        
        $json = Zend_Json::encode($getDocChecklistFileList);
		
	echo $json;
	exit();
    }
    
    public function editAction(){
        $trxId = $this->_getParam('id',null);
        $stdCtgy = $this->_getParam('stdCtgy',null);
        $this->view->title=$this->view->translate("Edit Application");
        $applicantInfo = $this->checklistVerification->infoBox($trxId);
        $getUserIdentity = $this->auth->getIdentity();
        $editApplicationForm = new Application_Form_EditApplication(array('ProgramId'=>$applicantInfo['IdProgram'], 'ProgramSchemeId'=>$applicantInfo['ap_prog_scheme'], 'StdCtgy'=>$applicantInfo['appl_category'], 'statuscode'=>$applicantInfo['at_status']));
        
        $this->view->applicantInfo = $applicantInfo;
        $this->view->editApplicationForm = $editApplicationForm;
        
        $mop = $this->defModel->getData($applicantInfo['mode_of_program']); 
        $mod = $this->defModel->getData($applicantInfo['mode_of_study']);
        $programType = $this->defModel->getData($applicantInfo['program_type']);
        $studentCategory = $this->defModel->getData($applicantInfo['appl_category']);
        $status = $this->defModel->getData($applicantInfo['at_status']);
        $this->view->status = $status['DefinitionDesc'];
        
        //pass info to view
        $this->view->applicantName = $applicantInfo['appl_fname'].' '.$applicantInfo['appl_mname'].' '.$applicantInfo['appl_lname'];
        $this->view->applicantId = $applicantInfo['at_pes_id'];
        $this->view->idNumber = $applicantInfo['appl_idnumber'];
        $this->view->programName = $applicantInfo['ProgramName'];
        $this->view->programScheme = $programType['DefinitionDesc']." ".$mop['DefinitionDesc']." ".$mod['DefinitionDesc'];
        $this->view->studentCategory = $studentCategory['DefinitionDesc'];
        $this->view->intake = $applicantInfo['IntakeDesc'];
        $this->view->appTrxId = $trxId;
        $this->view->editSuccess = 0;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($editApplicationForm->isValid($formData)) {
                
                $oldStatus = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_status', 'at_trans_id = '.$trxId);
                
                $dataTrans = array(
                    'at_status'=>$formData['at_status'],
                    'at_reason'=>$formData['at_reason']
                );
                $this->checklistVerification->updateAppTrans($dataTrans, $trxId);
                
                $dataHistory = array(
                    'ash_trans_id'=>$trxId,
                    'ash_status'=>$formData['at_status'],
                    'ash_oldStatus'=>$oldStatus,
                    'ash_changeType'=>1,
                    'ash_updUser'=>$getUserIdentity->id,
                    'ash_userType'=>1,
                    'ash_updDate'=>date('Y-m-d H:i:s')
                );
                $this->checklistVerification->storeStatusUpdateHistory($dataHistory);
                
                //revert proforma
                if ($oldStatus == 593 || $oldStatus == 599){
                    if ($formData['at_status']==595 || 
                            $formData['at_status']==592 || 
                            $formData['at_status']==596 || 
                            $formData['at_status']==597 ||
                            $formData['at_status']==598
                        ){
                        
                        $cancelData = array(
                            'status'=>'X',
                            'cancel_by'=>$getUserIdentity->id,
                            'cancel_date'=>date('Y-m-d')
                        );
                        $this->appAprove->cancelProforma($cancelData, $trxId);
                    }
                }
                $this->_helper->flashMessenger->addMessage(array('success' => 'Information Updated'));
                $this->_redirect( '/application/applicant-approval/edit/id/'.$trxId);
                
                $this->view->editSuccess = 1;
            }else{
                $editApplicationForm->populate($applicantInfo);
            }
        }else{
            $editApplicationForm->populate($applicantInfo);
        }
    }
    
    /*
	* credits to WP
	*/
    public function mkdir_p( $target ){
        // safe mode fails with a trailing slash under certain PHP versions.
        $target = rtrim($target, '/'); // Use rtrim() instead of untrailingslashit to avoid formatting.php dependency.
        if ( empty($target) )
            $target = '/';

        if ( file_exists( $target ) )
            return @is_dir( $target );

        // We need to find the permissions of the parent folder that exists and inherit that.
        $target_parent = dirname( $target );
        while ( '.' != $target_parent && ! is_dir( $target_parent ) ) {
            $target_parent = dirname( $target_parent );
        }

        // Get the permission bits.
        $dir_perms = false;
        if ( $stat = @stat( $target_parent ) ) 
        {
            $dir_perms = $stat['mode'] & 0007777;
        }
        else 
        {
            $dir_perms = 0777;
        }

        if ( @mkdir( $target, $dir_perms, true ) ) 
        {

            // If a umask is set that modifies $dir_perms, we'll have to re-set the $dir_perms correctly with chmod()
            if ( $dir_perms != ( $dir_perms & ~umask() ) ) 
            {
                $folder_parts = explode( '/', substr( $target, strlen( $target_parent ) + 1 ) );
                for ( $i = 1; $i <= count( $folder_parts ); $i++ ) 
                {
                    @chmod( $target_parent . '/' . implode( '/', array_slice( $folder_parts, 0, $i ) ), $dir_perms );
                }
            }

            return true;
        }

        return false;
    }
    
    public function getApplicantFeeStructure($program_id, $scheme, $intake_id, $student_category=579){
		
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('fs'=>'fee_structure'))
            ->join( array('fsp'=>'fee_structure_program'), 'fsp.fsp_fs_id = fs.fs_id and fsp.fsp_program_id = '.$program_id.' and fsp.fsp_idProgramScheme = '.$scheme)
            ->where("fs.fs_intake_start = '".$intake_id."'")
            ->where("fs.fs_student_category = '".$student_category."'");
        $row = $db->fetchRow($selectData);

        return $row;
    }
    
    public function offered($transLoop){
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $getUserIdentity = $this->auth->getIdentity();
        $class = new icampus_Function_Studentfinance_Invoice();
        $templateTagsCMS = new Cms_TemplateTags();
        
        //get info
        $applicantInfo = $this->appAprove->getApplicantInfo($transLoop);

        if ($applicantInfo['ap_prog_id']!=2){
            $check = $this->getApplicantFeeStructure($applicantInfo['ap_prog_id'], $applicantInfo['ap_prog_scheme'], $applicantInfo['at_intake'], $applicantInfo['appl_category']);

            if (!$check){
                $this->_helper->flashMessenger->addMessage(array('error' => "No Fee Structure Setup For Programme : ".Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program', 'ProgramName', 'IdProgram='.$applicantInfo['ap_prog_id'])));
                $this->_redirect( '/application/applicant-approval/');  
            }
        }

        $getStatus = $this->defModel->getIdByDefType('Status', 'Offered');
        $oldStatus = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_transaction', 'at_status', 'at_trans_id = '.$transLoop);

        $data = array(
            'at_status'=>$getStatus['key']
        );

        $getProgScheme = Application_Model_DbTable_ChecklistVerification::universalQuery('applicant_program', 'ap_prog_scheme', 'ap_at_trans_id = '.$transLoop);
        $stsId = $this->statusModel->fnGetStatusByProgramAndStsType($getProgScheme, $data['at_status'], $applicantInfo['appl_category']);
        $statusInfo = $this->appAprove->getStatusInfo($stsId['sts_Id']);
        //var_dump($statusInfo); exit;
        if ($statusInfo == false){
            $this->_helper->flashMessenger->addMessage(array('error' => "No Offered Status For Programme : ".Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program', 'ProgramName', 'IdProgram='.$applicantInfo['ap_prog_id'])));
            $this->_redirect('/application/applicant-approval/');  
        }
        $statusEmailTemplate = $this->appAprove->getStatusTemplate($statusInfo['sts_emailNotification']);
        if ($statusEmailTemplate == false){
            $this->_helper->flashMessenger->addMessage(array('error' => "No Offered Email Notification For Programme : ".Application_Model_DbTable_ChecklistVerification::universalQuery('tbl_program', 'ProgramName', 'IdProgram='.$applicantInfo['ap_prog_id'])));
            $this->_redirect( '/application/applicant-approval/');
        }

        //generate proforma
        if ($applicantInfo['ap_prog_id']!=2 && $applicantInfo['at_tution_fee'] == null ){
            $class->generateApplicantProformaInvoice($transLoop, 1);
        }

        //update status
        $this->checklistVerification->updateAppTrans($data, $transLoop);

        //store change status history
        $dataHistory = array(
            'ash_trans_id'=>$transLoop,
            'ash_status'=>$data['at_status'],
            'ash_oldStatus'=>$oldStatus,
            'ash_changeType'=>1,
            'ash_updUser'=>$getUserIdentity->id,
            'ash_userType'=>1,
            'ash_updDate'=>date('Y-m-d H:i:s')
        );
        $this->checklistVerification->storeStatusUpdateHistory($dataHistory);

        $statusAttachment = $this->appAprove->getStatusAttachment($statusEmailTemplate['stp_Id']);
        
        //add to email que
        $dataEmail = array(
            'recepient_email' => $applicantInfo['appl_email'],
            'subject' => $statusEmailTemplate['stp_title'],
            'content' => $templateTagsCMS->parseContent($transLoop, $statusEmailTemplate['stp_tplId'], $statusEmailTemplate['contentEng']),
            'date_que' => date('Y-m-d H:i:s')
        );
        $emailQueId = $this->appAprove->addToEmailQue($dataEmail);

        //enter attachment proses
        if (count($statusAttachment) > 0){
            foreach ($statusAttachment as $statusAttachmentLoop){
                $statusAttachmentTemplate = $this->appAprove->getStatusAttachmenTemplate($statusAttachmentLoop['sth_tplId']);
                if (count($statusAttachmentTemplate) > 0){
                    foreach ($statusAttachmentTemplate as $statusAttachmentTemplateLoop){
                        if ($statusAttachmentTemplateLoop['locale'] == 'en_US'){
                            //attachment name
                            if ($statusAttachmentTemplateLoop['locale'] == 'en_US'){
                                $name=$applicantInfo['at_pes_id'].'_'.$statusAttachmentLoop['typeName'].'_'.date('YmdHis');
                            }else{
                                $name=$applicantInfo['at_pes_id'].'_'.$statusAttachmentLoop['typeName'].'_'.date('YmdHis');
                            }

                            //attachment content
                            $html = $templateTagsCMS->parseContent($transLoop, $statusAttachmentTemplateLoop['tpl_id'], $statusAttachmentTemplateLoop['tpl_content']);
                            //echo $html;
                            //attachment url
                            $url = DOCUMENT_PATH.$applicantInfo['at_repository'];

                            //option pdf
                            $option = array(
                                'content' => $html,
                                'save' => true,
                                'file_extension' => 'pdf',
                                'save_path' => $url,
                                'file_name' => $name,
                                /*'css' => '@page { margin: 110px 50px 50px 50px}
                               body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
						table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;}',*/
                                'css' => '@page { margin: 110px 50px 50px 50px}
						body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
						table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 2px;border-style: solid;border-color: #729ea5;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',
                                'header' => '<script type="text/php">
                                if ( isset($pdf) ) {
                                        $header = $pdf->open_object();

                                        $w = $pdf->get_width();
                                        $h = $pdf->get_height();
                                        $color = array(0,0,0);

                                        $img_w = 180; 
                                        $img_h = 59;
                                        $pdf->image("images/logo_text_high.jpg",  ($w/2)-85, 20, $img_w, $img_h);

                                        // Draw a line along the bottom
                                        $font = Font_Metrics::get_font("Helvetica");
                                        $size = 6;
                                        $text_height = Font_Metrics::get_font_height($font, $size)+2;
                                        $y = $h - (3.5 * $text_height)-10;
                                        $pdf->line(10, $y, $w - 10, $y, $color, 1);

                                        // Draw a second line along the bottom
                                        $y = $h - (3.5 * $text_height)+10;
                                        $pdf->line(10, $y, $w - 10, $y, $color, 1);

                                        $pdf->close_object();

                                        $pdf->add_object($header, "all");
                                }
                                </script>',
                                'footer' => '<script type="text/php">
                                if ( isset($pdf) ) {
                                        $footer = $pdf->open_object();

                                        $font = Font_Metrics::get_font("Helvetica");
                                        $size = 6;
                                        $color = array(0,0,0);
                                        $text_height = Font_Metrics::get_font_height($font, $size)+2;

                                        $w = $pdf->get_width();
                                        $h = $pdf->get_height();


                                        // Draw a line along the bottom
                                        $y = $h - (3.5 * $text_height)-10;
                                        //$pdf->line(10, $y, $w - 10, $y, $color, 1);

                                        //1st row footer
                                        $text = "Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)";
                                        $width = Font_Metrics::get_text_width($text, $font, $size);	
                                        $y = $h - (2 * $text_height)-20;
                                        $x = ($w - $width) / 2.0;

                                        $pdf->page_text($x, $y, $text, $font, $size, $color);

                                        //2nd row footer
                                        $text = "t: +603 7651 4000		f : +603 7651 4094		w: www.inceif.org";
                                        $width = Font_Metrics::get_text_width($text, $font, $size);	
                                        $y = $h - (1 * $text_height)-20;
                                        $x = ($w - $width) / 2.0;

                                        $pdf->page_text($x, $y, $text, $font, $size, $color);



                                        $pdf->close_object();

                                        $pdf->add_object($footer, "all");
                                }
                                </script>'
                            );

                            //genarate attachment
                            $pdf = generatePdf($option);
                            //echo $pdf;

                            //genarate attachment
                            //generatePdfDocument($html, $url, $name);

                            //store history generate pdf
                            $dataGeneratePdf = array(
                                'tgd_txn_id' => $transLoop,
                                'tgd_filename' => $name.'.pdf',
                                'sth_id'=> $statusAttachmentLoop['sth_Id'],
                                'tgd_fileurl' =>$statusAttachmentTemplateLoop['tpl_type']!=619 ? $applicantInfo['at_repository'].'/'.$name.'.pdf':$statusAttachmentTemplateLoop['tpl_content'],
                                'tgd_updDate' => date('Y-m-d H:i:s'),
                                'tgd_updUser' => $getUserIdentity->id
                            );
                            $this->appAprove->storePdfDocGenHistory($dataGeneratePdf);

                            //add email attachment
                            $dataEmailAttachment = array(
                                'eqa_emailque_id' => $emailQueId,
                                'eqa_filename' => $name.'.pdf',
                                'eqa_path' => $statusAttachmentTemplateLoop['tpl_type']!=619 ? APP_DOC_PATH.$applicantInfo['at_repository'].'/'.$name.'.pdf':APP_DOC_PATH.$statusAttachmentTemplateLoop['tpl_content'],
                                'eqa_updDate' => date('Y-m-d H:i:s'),
                                'eqa_updUser' => $getUserIdentity->id
                            );
                            $this->appAprove->addEmailQueAtttachment($dataEmailAttachment);
                        }
                    }
                }
            }
        }
    }
    
    public function offerApplicantAction(){
        $trxId = $this->_getParam('id',null);
        if($trxId!=null){
            $this->offered($trxId);
            //exit;
            $this->_helper->flashMessenger->addMessage(array('success' => 'Offered success'));
            $this->_redirect( '/application/applicant-approval/edit/id/'.$trxId);
        }else{
            $this->_helper->flashMessenger->addMessage(array('error' => 'Undefined applicant to approve'));
            $this->_redirect( '/application/applicant-approval/');
        }
    }
}
?>