<?php
class Application_ImportApplicationController extends Zend_Controller_Action {

	private $_DbObj;
	private $_config;

	public function init(){
		
	}

	public function indexAction() {
		
		//title
		$this->view->title= $this->view->translate("Import Application");
	}	
	
	public function spmbnAction(){
		//title
		$this->view->title= $this->view->translate("Import Application - SPMBN");
		
		$step = $this->_getParam('step', 1);
		$this->view->step = $step;
		
		$ses_spmbn =  new Zend_Session_Namespace('ses_spmbn');
		
		if($step==1){
				
			$form = new Application_Form_PlacementTestSearch();
			$this->view->form = $form;
			
			if ($this->getRequest()->isPost()) {
				$formData = $this->getRequest()->getPost();
		
				$ses_spmbn->aps_id = $formData['aps_id'];
						
				//redirect
				$this->_redirect($this->view->url(array('module'=>'application','controller'=>'import-application', 'action'=>'spmbn', 'step'=>2),'default',true));
		
			}else{
				
			}
				
		}else
		if($step==2){
			
			//step validation
			if( !isset($ses_spmbn->aps_id) ){
				$this->_redirect($this->view->url(array('module'=>'application','controller'=>'import-application', 'action'=>'spmbn'),'default',true));
			}
			
			if ($this->getRequest()->isPost()) {
				$formData = $this->getRequest()->getPost();
				
				$ses_spmbn->skr = $formData['skr_id'];
				$ses_spmbn->intake = $formData['intake'];
				$ses_spmbn->period = $formData['period'];
				
				//redirect
				$this->_redirect($this->view->url(array('module'=>'application','controller'=>'import-application', 'action'=>'spmbn', 'step'=>3),'default',true));
				
			}else{
				//get SKR
				$applicantAssessmentUsmDetailDb = new App_Model_Application_DbTable_ApplicantAssessmentUsmDetl();
				$this->view->skr_list = $applicantAssessmentUsmDetailDb->getPaginateNomor();
				
				//get intake
				$intakeDb = new App_Model_Record_DbTable_Intake();
				$this->view->intake_list = $intakeDb->getData();
			}
			
		}else
		if($step==3){
			
			//step validation
			if(!isset($ses_spmbn->skr) || !isset($ses_spmbn->intake) || !isset($ses_spmbn->period)){
				$this->_redirect($this->view->url(array('module'=>'application','controller'=>'import-application', 'action'=>'spmbn', 'step'=>2),'default',true));
			}
			
			$ses_spmbn->data = null;
			
			//get SKR info
			$applicantAssessmentUsmDetailDb = new App_Model_Application_DbTable_ApplicantAssessmentUsmDetl();
			$skr_data = $applicantAssessmentUsmDetailDb->getData($ses_spmbn->skr);
			
			//get usm info
			$placementTestScheduleDb = new App_Model_Application_DbTable_PlacementTestSchedule();
			$ptest_sch_data = $placementTestScheduleDb->getScheduleInfo($ses_spmbn->aps_id);
						
			$form = new Application_Form_BatchCsvProfileUpload();
			$form->setAction('/application/import-application/spmbn/step/3');
			$this->view->form = $form;
			
			if ($this->getRequest()->isPost()) {
				$formData = $this->getRequest()->getPost();
				
				// success - do something with the uploaded file
				$uploadedData = $form->getValues();
				$fullFilePath = $form->file->getFileName();
				
				$header_skip = $formData['remove_header']==1?true:false;
				
				$data = array();
				$data_mapped = array();
				if (($handle = fopen($fullFilePath, 'r')) !== FALSE)
				{
					$i=0;
					while (($row = fgetcsv($handle, 1000, ",")) !== FALSE)
					{
						$data = $row;
				
						if($header_skip && $i==0){
							 
				
						}else{
							 
							 
							/*
							 * Data Mapping
							*
							*/
							
							//profile
							$data_mapped[$i]['profile']["appl_fname"] = $data[5];
							$data_mapped[$i]['profile']["appl_mname"] = $data[6];
							$data_mapped[$i]['profile']["appl_lname"] = $data[7];
							
							$data_mapped[$i]['profile']["appl_address1"] = $data[8];
							$data_mapped[$i]['profile']["appl_postcode"] = $data[9];
							$data_mapped[$i]['profile']["appl_state"] = "0";
							$data_mapped[$i]['profile']["appl_province"] = "261";
							
							$data_mapped[$i]['profile']["appl_email"] = $data[12];
							$data_mapped[$i]['profile']["appl_phone_home"] = $data[10];
							$data_mapped[$i]['profile']["appl_phone_mobile"] = $data[11];
							
							$data_mapped[$i]['profile']["appl_password"] = $data[1];
							$data_mapped[$i]['profile']["appl_dob"] = date('Y-m-d', strtotime($data[14]));
							$data_mapped[$i]['profile']["appl_birth_place"] = $data[15];
							$data_mapped[$i]['profile']["appl_gender"] = $data[16];
							$data_mapped[$i]['profile']["appl_prefer_lang"] = 2;
							$data_mapped[$i]['profile']["appl_nationality"] = 1;
							$data_mapped[$i]['profile']["appl_religion"] = $data[17];
							
							
							//parent
							$data_mapped[$i]['family']["father_af_name"] = $data[18];
							$data_mapped[$i]['family']["father_af_phone"] = $data[19];
							$data_mapped[$i]['family']["father_af_job"] = $data[20];
							$data_mapped[$i]['family']["appl_parent_salary"] = $data[21];
							
							//transaction
							$data_mapped[$i]['transaction']["at_pes_id"] = $data[1];
							$data_mapped[$i]['transaction']["at_appl_type"] = 1;
							$data_mapped[$i]['transaction']["at_academic_year"] = $skr_data['aaud_academic_year'];
							$data_mapped[$i]['transaction']["at_intake"] = $ses_spmbn->intake;
							$data_mapped[$i]['transaction']["at_period"] = $ses_spmbn->period;
							
							
							if($data[3]==5){//TPA
								$data_mapped[$i]['transaction']["at_status"] = "PROCESS";
								$data_mapped[$i]['transaction']["at_selection_status"] = 5;
							}else{
								$data_mapped[$i]['transaction']["at_status"] = "OFFER";
								$data_mapped[$i]['transaction']["at_selection_status"] = 3;
							}
							
							$data_mapped[$i]['transaction']["at_create_by"] = '98999';
							$data_mapped[$i]['transaction']["entry_type"] = null;
							
							//program
							$data_mapped[$i]['program']["ap_prog_code"] = $data[2];
							$data_mapped[$i]['program']["ap_preference"] = 1;
							$data_mapped[$i]['program']["ap_usm_status"] = 1;
							
							//ptest
							$data_mapped[$i]['ptest']['apt_ptest_code'] = $ptest_sch_data['aps_placement_code'];
							$data_mapped[$i]['ptest']['apt_aps_id'] = $ptest_sch_data['aps_id'];
							
							
							//selection
							$data_mapped[$i]['selection']["rector_status"] = $data[3];
							$data_mapped[$i]['selection']["rank"] = $data[4];
							

						}
				
						$i++;
				
					}
					fclose($handle);
				}
				
				
				//check for error on data
				$no_error = true;
				foreach ($data_mapped as $index=>$data){
					$error = array();
					
					//nopes
					if($data['transaction']['at_pes_id']==""){
						$error[] = "No NOPES";
					}
					
					$migrateDB = new Application_Model_DbTable_Migrate();
					if($migrateDB->exist($data['transaction']['at_pes_id'])){
						$error[] = "NOPES Already exists";
					}
					
					//email
					if($data['profile']['appl_email']==""){
						$error[] = "No Email";
					}else{
						$db = Zend_Db_Table::getDefaultAdapter();
						$scheck="select appl_id,appl_email from applicant_profile where appl_email='".$data['profile']['appl_email']."'";
						$exist=$db->fetchRow($scheck);
						if($exist){
							$error[] = "Email already exists";
						}
					}
					
					
					//program
					if($data['program']['ap_prog_code']==""){
						$error[] = "No Program";
					}
					
					
					
					
					if(sizeof($error)>0){
						$data_mapped[$index]['error'] = $error;
					}
				}
				
				$this->view->data_mapped = $data_mapped;
				$ses_spmbn->data = $data_mapped;
				
				foreach ($data_mapped as $index=>$data){
					if(isset($data['error'])){
						$no_error = false;
						break;
					}
				}
				
				if($no_error){
					$this->_redirect($this->view->url(array('module'=>'application','controller'=>'import-application', 'action'=>'spmbn', 'step'=>4),'default',true));
				}else{
					$this->view->noticeError = "Error in CSV Data. Please repair before you can proceed";
				}
			}
			
			
			
		}else
		if($step==4){
			
			//step validation
			if(!isset($ses_spmbn->skr)){
				$this->_redirect($this->view->url(array('module'=>'application','controller'=>'import-application', 'action'=>'spmbn'),'default',true));
			}
			
			//step validation
			if(!isset($ses_spmbn->data)){
				$this->_redirect($this->view->url(array('module'=>'application','controller'=>'import-application', 'action'=>'spmbn', 'step'=>3),'default',true));
			}
			
			//get usm info
			$placementTestScheduleDb = new App_Model_Application_DbTable_PlacementTestSchedule();
			$ptest_sch_data = $placementTestScheduleDb->getScheduleInfo($ses_spmbn->aps_id);
			$this->view->placement_test = $ptest_sch_data;
				
			//get SKR info
			$applicantAssessmentUsmDetailDb = new App_Model_Application_DbTable_ApplicantAssessmentUsmDetl();
			$skr_data = $applicantAssessmentUsmDetailDb->getData($ses_spmbn->skr);
			$this->view->skr_data = $skr_data;
				
			//intake
			$intakeDb = new App_Model_Record_DbTable_Intake();
			$intake = $intakeDb->getData($ses_spmbn->intake);
			$this->view->intake = $intake;
				
			//period
			$periodDb = new App_Model_Record_DbTable_AcademicPeriod();
			$period = $periodDb->getData($ses_spmbn->period);
			$this->view->period = $period;
			
			//get SKR info
			$applicantAssessmentUsmDetailDb = new App_Model_Application_DbTable_ApplicantAssessmentUsmDetl();
			$skr_data = $applicantAssessmentUsmDetailDb->getData($ses_spmbn->skr);
			$this->view->skr_data = $skr_data;
			
			//applicant data
			$this->view->applicant_data = $ses_spmbn->data;
				
		}
		
		
	}
	
	public function saveAction(){
		
		$ses_spmbn =  new Zend_Session_Namespace('ses_spmbn');
		
		//step validation
		if(!isset($ses_spmbn->skr)){
			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'import-application', 'action'=>'spmbn'),'default',true));
		}
			
		//step validation
		if(!isset($ses_spmbn->data)){
			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'import-application', 'action'=>'spmbn', 'step'=>2),'default',true));
		}
			
		//get SKR info
		$applicantAssessmentUsmDetailDb = new App_Model_Application_DbTable_ApplicantAssessmentUsmDetl();
		$skr_data = $applicantAssessmentUsmDetailDb->getData($ses_spmbn->skr);
		$this->view->skr_data = $skr_data;
		
		
		//INSERT DATA
		$migrateDB = new Application_Model_DbTable_Migrate();
		$transactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
		$profileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		$familyDB  = new App_Model_Application_DbTable_ApplicantFamily();
		$aprogDB  = new App_Model_Application_DbTable_ApplicantProgram();
		$appeduDB = new App_Model_Application_DbTable_ApplicantEducation();
		$ptestDB = new App_Model_Application_DbTable_ApplicantPtest();
		$badb = new Application_Model_DbTable_Batchagent();
		$selDb = new App_Model_Application_DbTable_ApplicantSelectionDetl();
		$aaudDb= new App_Model_Application_DbTable_ApplicantAssessmentUsm();
		
		
		foreach ($ses_spmbn->data as $i=>$thedata){
			
		
			if(!$migrateDB->exist($thedata['transaction']['at_pes_id'])){
				
				/*
				 * PROFILE
				 */
				$profile["appl_fname"] = $thedata['profile']['appl_fname'];
				$profile["appl_mname"] = $thedata['profile']['appl_mname'];
				$profile["appl_lname"] = $thedata['profile']['appl_lname'];
				
				$profile["appl_email"] = $thedata['profile']["appl_email"];
				
				if($profile["appl_email"]==""){
					$profile["appl_email"]=$thedata['transaction']["at_pes_id"];
				}
				
				$profile["appl_password"] = $thedata['transaction']["at_pes_id"];
				 
				$profile["appl_gender"] = $thedata['profile']["appl_gender"];
				if($profile["appl_gender"]=="")  $profile["appl_gender"]="0";
				 
				$profile["appl_dob"] = $thedata['profile']["appl_dob"];
				if($profile["appl_dob"]=="")  $profile["appl_dob"]="0";
				
				$profile["appl_nationality"] =$thedata['profile']["appl_nationality"];
				if($profile["appl_nationality"]=="")  $profile["appl_nationality"]="1";
				
				$profile["appl_religion"] = $thedata['profile']["appl_religion"];
				if($profile["appl_religion"]=="")  $profile["appl_religion"]="5";
				
				$profile["appl_phone_home"] = $thedata['profile']["appl_phone_home"];
				if($profile["appl_phone_home"]=="")  $profile["appl_phone_home"]="0";
				
				$profile["appl_phone_mobile"] = $thedata['profile']["appl_phone_mobile"];
				if($profile["appl_phone_mobile"]=="")  $profile["appl_phone_mobile"]="0";
				 
				$profile["appl_address1"] = $thedata['profile']["appl_address1"];
				
				$profile["appl_postcode"] = $thedata['profile']["appl_postcode"];
				if($profile["appl_postcode"]=="")  $profile["appl_postcode"]="0";
				 
				$profile["appl_state"] = $thedata['profile']["appl_state"];
				if($profile["appl_state"]=="") $profile["appl_state"]=1;
		
				$migrateDB->checkEmail($profile["appl_email"]);
				
				$profileID=$profileDB->addData($profile);
								
				
				//FATHER INFOMATION
				$family["af_relation_type"] = 20;
				
				$family["af_name"] = $thedata['family']["father_af_name"];
				if($family["af_name"]=="")$family["af_name"]="Father";
				
				$family["af_address1"] = "";//$thedata['family']["father_af_address1"];
				if($family["af_address1"]=="")$family["af_address1"]="Address";
				
				$family["af_postcode"] = "";//$thedata['family']["father_af_ostcode"];
				if($family["af_postcode"]=="")$family["af_postcode"]=1;
				
				$family["af_state"] = "";//$thedata['family']["father_appl_state"];
				if($family["af_state"]=="")$family["af_state"]=255; //DKI Jakarta
				
				$family["af_city"] = "";//$thedata['family']["father_appl_city"];
				if($family["af_city"]=="")$family["af_city"]=733; //Jakarta Pusat
								
				$family["af_family_condition"] = "";//$thedata['family']["father_condition"];
				if($family["af_family_condition"]=="")$family["af_family_condition"]=1;
				 
				$family["af_education_level"] = "";//$thedata["ddkayh"]; //sis_setup_dtl
				if($family["af_education_level"]==""){
					$family["af_education_level"]=0;
				}
				
				$family["af_job"] = "";//$thedata['family']["pekerjaan_ayah"];
				if($family["af_job"]=="")$family["af_job"]=1;
				
				$family["af_phone"] = $thedata['family']["father_af_phone"];
				if($family["af_phone"]=="")$family["af_phone"]=1;
				
				$family["af_appl_id"] = $profileID;
		
				$familyDB->addData($family);
				 
				
				// MOTHER INFORMATION
				
				/*$family2["af_relation_type"] = 21;
				 $family2["af_name"] = $thedata["nama_ibu"];
				if($family2["af_name"]=="")$family2["af_name"]="No Name";
				$family2["af_family_condition"] = $thedata["adaibu"];
				if($family2["af_family_condition"]=="")$family2["af_family_condition"]=1;
				$family2["af_education_level"] = $thedata["ddkibu"]; //sis_setup_dtl
				$family2["af_job"] = $thedata["kerjaibu"]; //tbl familyjob
				if($family2["af_job"]=="")$family2["af_job"]=1;
				$family2["af_appl_id"] = $profileID;
				 
				if($family2["af_education_level"]==""){
				$family2["af_education_level"]=0;
				}
		
				echo"<pre>";
				print_r($family2);
				echo"</pre>";
				$familyDB->addData($family2);*/
				 
				
				//TRANSACTION DATA
		
				$txndata["at_appl_id"] = $profileID;
				$txndata["at_pes_id"] = $thedata['transaction']["at_pes_id"];
				$txndata["at_appl_type"] = $thedata['transaction']["at_appl_type"];
				$txndata["at_academic_year"] = $thedata['transaction']["at_academic_year"];
				$txndata["at_intake"] = $thedata['transaction']["at_intake"];
				$txndata["at_period"] = $thedata['transaction']["at_period"];
				$txndata["at_status"] = $thedata['transaction']["at_status"];
				$txndata["at_create_by"]=99989;
				$txndata["at_create_date"]=date("Y-m-d H:i:s");
				$txndata["at_submit_date"]=date("Y-m-d H:i:s");
				$txndata["at_selection_status"]=$thedata['transaction']["at_selection_status"];
				
				//$txndata["at_payment_status"]=1;
				
				$txnID=$transactionDB->addData($txndata);
				
				
				//PTEST DATA
				$ptest["apt_at_trans_id"]=$txnID;
				$ptest["apt_appl_id"]=$profileID;
				$ptest["apt_no_pes"]=$thedata['transaction']["at_pes_id"];
				$ptest["apt_ptest_code"]=$thedata['ptest']["apt_ptest_code"];
				$ptest["apt_aps_id"]=$thedata['ptest']["apt_aps_id"];
				$ptest["apt_fee_amt"]="0.00";
				$ptest["apt_bill_no"]=$thedata['transaction']["at_pes_id"];
				$ptest["apt_status"]="";
				$ptest["apt_usm_attendance"]=1;
				$ptest["apt_mark"]=0;
				
				$ptestDB->addData($ptest);
				
				
				
				//PROGRAM DATA
				 
				$prog["ap_at_trans_id"] = $txnID;
				$prog["ap_prog_code"] = str_pad($thedata['program']["ap_prog_code"],4,0,STR_PAD_LEFT);
				$prog["ap_preference"] = $thedata['program']["ap_preference"];
				$prog["ap_usm_status"] = $thedata['program']["ap_usm_status"];
				
				$apId = $aprogDB->insert($prog);
		
				
				//HIGH SCHOOL
				
				$hs["ae_institution"]="";//$thedata["kd_sla"];
				if($hs["ae_institution"]=="")$hs["ae_institution"]="0";
				$hs["ae_transaction_id"]=$txnID;
				$hs["ae_discipline_code"]="";//$thedata["jur_sla"];
				if($hs["ae_discipline_code"]=="")$hs["ae_discipline_code"]="0";
				$hs["ae_appl_id"]=$profileID;
		
				$appeduDB->insert($hs);

				
				//SELECTION INFO
				
				$selection["aau_trans_id"] = $txnID;
				$selection["aau_ap_id"] = $apId;
				$selection["aau_createddt"] = date("Y-m-d H:i:s");
				$selection["aau_createdby"] = 98999;
				$selection["aau_rector_ranking"] = $thedata['selection']['rank'];
				$selection["aau_rector_status"] = $thedata['selection']["rector_status"];
				$selection["aau_rector_createby"] = 98999;
				$selection["aau_rector_createdt"] = date("Y-m-d H:i:s");
				$selection["aau_rector_selectionid"] = $skr_data['aaud_id'];
				$selection["aau_reversal_status"] = 0;
								
				$aaudDb->insert($selection);
		
			}else{
				echo $thedata["no_form"].": No Formulir Already Exist!<br>";
			}
			 
		}
		
		$this->view->noticeSuccess = "Success Inport SPMBN Data";
	}
	
	public function getUsmScheduleAction(){
		
		$this->_helper->layout->disableLayout();
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();
			
			$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();
			
			$scheduleDb = new App_Model_Application_DbTable_PlacementTestSchedule();
			
			$cond = array(
					'location' => $formData['location']
			);
			
			//intake
			if( isset($formData['intake_id']) && $formData['intake_id']!="" ){
				$intakeDb = new App_Model_Record_DbTable_Intake();
				$intakeData = $intakeDb->getData($formData['intake_id']);
			
				$cond['dt_from'] = date('Y-m-d', strtotime($intakeData['ApplicationStartDate']));
				$cond['dt_to'] = date('Y-m-d', strtotime($intakeData['ApplicationEndDate']));
			}
			//period
			if( isset($formData['period_id']) && $formData['period_id']!="" ){
				$periodDb = new App_Model_Record_DbTable_AcademicPeriod();
				$periodData = $periodDb->getData($formData['period_id']);
			
				$cond['dt_from'] = date('d-n-Y', strtotime('01-'.$periodData['ap_month']."-".$periodData['ap_year']));
				$cond['dt_to'] = date('t-m-Y', strtotime($cond['dt_from']));
			}
				
			//get schedule by search
			$list = $scheduleDb->search($cond);
			
			$json = Zend_Json::encode($list);
			echo $json;
		
		}
		
		exit;
		
	}
}
?>