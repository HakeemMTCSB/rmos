<?php
class Application_PlacementteststatController extends Base_Base {

	public function init() { //initialization function
		$this->locale = Zend_Registry::get('Zend_Locale');
		$this->view->translate =Zend_Registry::get('Zend_Translate');
		$this->_gobjlog = Zend_Registry::get ( 'log' );
		Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}

	public function fnsetObj(){
		$this->lobjPlacementteststat = new Application_Model_DbTable_Placementteststat();

	}

	//Action to list and search the placement statistics
	public function indexAction() {
		$this->view->lobjform = $this->lobjform;
		$this->lobjform->field11->addMultiOptions(array('Pass' => 'Pass',
				'Fail' => 'Fail'));
		$marks = array();
		$lobjstudentlist = $this->lobjPlacementteststat->fngetplacementteststat();
		$i = 0 ;
		foreach($lobjstudentlist as $list){
			$marks[$i]['IdApplication'] = $list['IdApplication'];
			$marks[$i]['Marks'] = $list['Marks'];
			$marks[$i]['ComponentName'] = $list['ComponentName'];
			$i++;
		}

		$this->view->marks = $marks;
		for ($e = 0; $e < count($lobjstudentlist); $e++) {
			$duplicate = null;
			for ($ee = $e; $ee < count($lobjstudentlist); $ee++) {
				if (strcmp($lobjstudentlist[$ee]['IdApplication'], $lobjstudentlist[$e]['IdApplication']) === 0) {
					$duplicate = $ee;
					break;
				}
			}
			if (!is_null($duplicate))
				array_splice($lobjstudentlist, $duplicate, 1);
		}
		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->studlistpaginatorresult);
		$lintpagecount =$this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		if(isset($this->gobjsessionsis->studlistpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->studlistpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($lobjstudentlist,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjPlacementteststat->fnSearchplacementteststat( $this->lobjform->getValues () ); //searching the values for the user
				for ($e = 0; $e < count($larrresult); $e++) {
					$duplicate = null;
					for ($ee = $e; $ee < count($larrresult); $ee++) {
						if (strcmp($larrresult[$ee]['IdApplication'], $larrresult[$e]['IdApplication']) === 0) {
							$duplicate = $ee;
							break;
						}
					}
					if (!is_null($duplicate))
						array_splice($larrresult, $duplicate, 1);
				}

				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->applistpaginatorresult = $larrresult;
			}
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/application/placementteststat/index');

		}
	}
}