<?php
class Application_UsmSelectionReportController extends Zend_Controller_Action
{

	public function rectorSelectionAction(){
		$this->view->title = $this->view->translate("Senarai Keputusan Wakil Rektor (USM)");
		
		
		$date = null;
		$location_id = null;
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			
			//intake
			$intakeDb = new App_Model_Record_DbTable_Intake();
			$intake = $intakeDb->getData($formData['intake']);
			
			//date
			if( isset($formData['date']) && $formData['date']!="ALL" ){
				$date = $formData['date'];
			}
			
			//location
			if( isset($formData['location']) && $formData['location']!="ALL" ){
				$location_id = $formData['location'];
			}
			
			$form = new Application_Form_UsmSelectionRectorSearch(array('intake'=>$intake['IdIntake']));
			$form->populate($formData);
			
		}else{
			
			//intake
			$intakeDb = new App_Model_Record_DbTable_Intake();
			
			$intake = $intakeDb->getCurrentIntake();
			if($intake==null){
				$intake = $intakeDb->getNextIntake();
			}
			
			$form = new Application_Form_UsmSelectionRectorSearch(array('intake'=>$intake['IdIntake']));
		}
			
		$this->view->intake = $intake;
	
		
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		//decree number
		$select = $db->select()
						->from(array('aau'=>'applicant_assessment_usm'),array())
						->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id', array('aaud.aaud_nomor','aaud.aaud_decree_date'))
						->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id',array('at.at_intake'))
						->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
						->join(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_id','aps.aps_test_date'))
						->join(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
						->where('at.at_intake = '.$intake['IdIntake'])
						//->where("at.at_status in ('OFFER','REGISTERED')")
					    ->where("at.at_selection_status = 3 OR at.at_selection_status = 5")
					    //->group("apt.apt_aps_id")
					    //->group("aps.aps_test_date")aaud_decree_date
					    ->group("aaud.aaud_decree_date")
					    ->group("al.al_id")
					    ->group("aaud.aaud_nomor");
					    
		//filter by date
		if($date!=null){
			$select->where("aaud.aaud_decree_date = '".$date."'");
		}
		
		//filter by location
		if($location_id!=null){
			$select->where('al.al_id = '.$location_id);
		}

		$row = $db->fetchAll($select);
		if(!$row){
			$row = null;
		}
		
		
		
		
		// bill offer/reject
		if($row){
			foreach ($row as $key=>$data){
				
				
				
				//bill applicant
				$bill_total = $db->select()
				->from(array('aau'=>'applicant_assessment_usm'),array())
				->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
				->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id', array('DISTINCT (at.at_trans_id)txn_id') )
				->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
				->join(array('aph'=>'appl_placement_head'),'apt.apt_ptest_code = aph.aph_placement_code',array())
				->join(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_test_date'))
				->join(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
				->where("aaud.aaud_nomor = '".$data['aaud_nomor']."'")
				->where('at.at_intake = '.$data['at_intake'])
				->where("aaud.aaud_decree_date = '".$data['aaud_decree_date']."'")
				->where("al.al_id = ".$data['al_id'])
				->where("aph.aph_testtype=0");
				
				$row_bil_total = $db->fetchAll($bill_total);
				
				if($row_bil_total){
					$row[$key]['bil_total'] = sizeof($row_bil_total);
				}else{
					$row[$key]['bil_total'] = 0;
				}
				
				//offer
				$bil_applicant = $db->select()
							->from(array('aau'=>'applicant_assessment_usm'),array())
							->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
							->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id', array('DISTINCT (at.at_trans_id)txn_id') )
							->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
							->join(array('aph'=>'appl_placement_head'),'apt.apt_ptest_code = aph.aph_placement_code',array())
							->join(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_test_date'))
							->join(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
							->where("aaud.aaud_nomor = '".$data['aaud_nomor']."'")
							->where('at.at_intake = '.$data['at_intake'])
							->where("aaud.aaud_decree_date = '".$data['aaud_decree_date']."'")
							->where("al.al_id = ".$data['al_id'])
							->where("aph.aph_testtype=0")
							->where("at.at_status in ('OFFER','REGISTERED')")
						    ->where("at.at_selection_status = 3")
						    ->where("aau.aau_rector_status = 1");
				
						    
				$row_bil = $db->fetchAll($bil_applicant);
				//$row[$key]['offer'] = $row_bil;
				
				if($row_bil){
					$row[$key]['bil_offer'] = sizeof($row_bil);
				}else{
					$row[$key]['bil_offer'] = 0;
				}
				
				//Lulus tahap 1
				$bil_applicant = $db->select()
							->distinct()
							->from(array('aau'=>'applicant_assessment_usm'),array())
							->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
							->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id', array('DISTINCT (at.at_trans_id)txn_id') )
							->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
							->join(array('aph'=>'appl_placement_head'),'apt.apt_ptest_code = aph.aph_placement_code',array())
							->join(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_test_date'))
							->join(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
							->where("aaud.aaud_nomor = '".$data['aaud_nomor']."'")
							->where('at.at_intake = '.$data['at_intake'])
							->where("aaud.aaud_decree_date = '".$data['aaud_decree_date']."'")
							->where("al.al_id = ".$data['al_id'])
							->where("aph.aph_testtype=0")
							//->where("at.at_selection_status = 5")
							->where("aau.aau_rector_status = 5");
				
						    
				$row_bil = $db->fetchAll($bil_applicant);
				//$row[$key]['offer'] = $row_bil;
				
				if($row_bil){
					$row[$key]['bil_next'] = sizeof($row_bil);
				}else{
					$row[$key]['bil_next'] = 0;
				}		
						
				//reject
				/*$bil_applicant = $db->select()
							//->from(array('aau'=>'applicant_assessment_usm'),array())
							//->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
							->from(array('at'=>'applicant_transaction'), array('DISTINCT (at.at_trans_id)txn_id') )
							->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
							->joinleft(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_test_date'))
							->joinleft(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
							//->where("aaud.aaud_nomor = '".$data['aaud_nomor']."'")
							->where('at.at_intake = '.$data['at_intake'])
							//->where("aaud.aaud_decree_date = '".$data['aaud_decree_date']."'")
							->where("al.al_id = ".$data['al_id'])
							->where("at.at_status in ('REJECT')")
						    ->where("at.at_selection_status = 3");*/
				
				$bil_applicant = $db->select()
				->from(array('at'=>'applicant_transaction'),array('at.at_trans_id','at.at_status') )
				->join(array('aau'=>'applicant_assessment_usm'),'aau.aau_trans_id = at.at_trans_id',array('aau.aau_rector_status'))
				->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id and aaud.aaud_type = 2',array('aaud.aaud_nomor'))
				->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
				->join(array('aph'=>'appl_placement_head'),'apt.apt_ptest_code = aph.aph_placement_code',array())
				->join(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_test_date'))
				->join(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
				->where("aaud.aaud_nomor = '".$data['aaud_nomor']."'")
				->where('at.at_intake = '.$data['at_intake'])
				->where("aaud.aaud_decree_date = '".$data['aaud_decree_date']."'")
				->where("al.al_id = ".$data['al_id'])
				->where("aph.aph_testtype=0")
				->where("at.at_status = 'REJECT'");
				//->where('aau.aau_rector_status not in (5)');
			
				$row_bil = $db->fetchAll($bil_applicant);
				//$row[$key]['reject'] = $row_bil;
				
				if($row_bil){
					$row[$key]['bil_reject'] = sizeof($row_bil);
				}else{
					$row[$key]['bil_reject'] = 0;
				}
			}
		}
        
        $this->view->data = $row;

		/*echo "<pre>";
		print_r($row);
		echo "</pre>";*/
		
		$this->view->form = $form;
				
	}
	
	/**
	 * Utility action
	 */
	public function ajaxGetPeriodAction(){
    	$intake_id = $this->_getParam('intake_id', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select(array('ap_id','ap_desc'))
	                 ->from(array('ap'=>'tbl_academic_period'))
	                 ->order('ap.ap_year')
	                 ->order('ap.ap_number');
	    
	    if($intake_id!=0){
	    	$select->where('ap.ap_intake_id = ?', $intake_id);
	    }
		
	    
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
    
	public function ajaxGetNomorAction(){
    	$academic_year = $this->_getParam('academic_year', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select(array('asd_id','asd_nomor'))
	                 ->from(array('asd'=>'applicant_selection_detl'));
	                 
	    
	    if($academic_year!=0){
	    	$select->where("asd_academic_year = '".$academic_year."'");
	    }
		
	    
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
	public function ajaxGetDecreeDateAction(){
    	$intake_id = $this->_getParam('intake_id', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	
	  	//intake date range
	  	$select = $db->select()
	                 ->from(array('i'=>'tbl_intake'))
	                 ->where('i.IdIntake = ?',$intake_id);
	  	$stmt = $db->query($select);
        $intake = $stmt->fetch();
        
	  	//decree date for specific intake
	  	$select = $db->select()
	                 ->from(array('aaud'=>'applicant_assessment_usm_detl'), array('DISTINCT(aaud.aaud_decree_date)'))
	                 ->where("aaud.aaud_decree_date between '".$intake['ApplicationStartDate']."' and '".$intake['ApplicationEndDate']."'")
	                 ->order('aaud.aaud_decree_date');
	    
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
	public function ajaxGetPtLocationAction(){
    	$date = $this->_getParam('date', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	
	  	//schedule date and location
	  	$select = $db->select()
	                 ->from(array('aps'=>'appl_placement_schedule'), array())
	                 ->join(array('al'=>'appl_location'), 'al.al_id = aps.aps_location_id', array('al.al_id','al.al_location_name', 'al.al_location_code'))
	                 //->where("aps.aps_test_date = '".$date."'")
	                 //->order('aps.aps_test_date');
	                 ->group('al.al_location_name');
	    
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
    public function rectorSelectionDetailAction(){
    	
    	$this->view->title = $this->view->translate("Senarai Keputusan Wakil Rektor : Detail");
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			//intake
			$intakeDb = new App_Model_Record_DbTable_Intake();
			$intake = $intakeDb->getData($formData['intake']);
			$this->view->intake = $intake;
			
			//period
			$periodDb = new App_Model_Record_DbTable_AcademicPeriod();
			$period = $periodDb->getData($formData['period']);
			$this->view->period = $period;

			$db = Zend_Db_Table::getDefaultAdapter();
		
			//decree number
			$select = $db->select()
							->from(array('aau'=>'applicant_assessment_usm'),array())
							->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id', array('aaud_nomor'))
							->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id',array('at.at_period'))
							->join(array('ap'=>'tbl_academic_period'),'ap.ap_id = at.at_period',array('ap.ap_desc'))
							->where('at.at_intake = '.$intake['IdIntake'])
							->where("at.at_status in ('OFFER','REGISTERED')")
						    ->where("at.at_selection_status = 3")
						    ->where("aaud.aaud_nomor = '".$formData['nomor']."'")
						    ->group("aaud.aaud_nomor")
						    ->group("at.at_period");
						    
			//filter by period
			if(isset($period)){
				$select->where('at.at_period = '.$period['ap_id']);
			}
						    
			$row = $db->fetchRow($select);
			if(!$row){
				$row = null;
			}
			
			//faculty
			if($row){
				
				$facultyList = $db->select()
								->from(array('aau'=>'applicant_assessment_usm'),array())
								->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
								->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id', array() )
								->join(array('ap'=>'tbl_academic_period'),'ap.ap_id = at.at_period',array())
								->join(array('apr'=>'applicant_program'),'apr.ap_at_trans_id = at.at_trans_id',array('apr.ap_prog_code'))
								->join(array('p'=>'tbl_program'),'p.ProgramCode = apr.ap_prog_code',array())
								->join(array('c'=>'tbl_collegemaster'),'c.IdCollege = p.IdCollege',array('IdCollege','ArabicName'))
								->where("aaud.aaud_nomor = '".$row['aaud_nomor']."'")
								->where('at.at_intake = '.$intake['IdIntake'])
								->where('at.at_period = '.$row['at_period'])
								->where("at.at_status in ('OFFER','REGISTERED')")
							    ->where("at.at_selection_status = 3")
							    ->group("c.IdCollege");
							    
				$row['program'] = $db->fetchAll($facultyList);
				
			}
			
			
			
			// bill offer/reject in program
			if($row['program']){
				foreach ($row['program'] as $key=>$prog_data){
					//offer
					$bil_applicant = $db->select()
								->from(array('aau'=>'applicant_assessment_usm'),array())
								->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
								->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id', array('DISTINCT (at.at_trans_id)txn_id, at.at_period') )
								->join(array('ap'=>'tbl_academic_period'),'ap.ap_id = at.at_period',array('ap.ap_desc'))
								->join(array('apr'=>'applicant_program'),'apr.ap_at_trans_id = at.at_trans_id and apr.ap_usm_status = 1',array('apr.ap_prog_code'))
								->join(array('p'=>'tbl_program'),'p.ProgramCode = apr.ap_prog_code',array())
								->join(array('c'=>'tbl_collegemaster'),'c.IdCollege = p.IdCollege',array('IdCollege','ArabicName'))
								->where("aaud.aaud_nomor = '".$row['aaud_nomor']."'")
								->where('at.at_intake = '.$intake['IdIntake'])
								->where('at.at_period = '.$row['at_period'])
								->where("at.at_status in ('OFFER','REGISTERED')")
							    ->where("at.at_selection_status = 3")
							    ->where("c.IdCollege = ".$prog_data['IdCollege']);
					
							    
					$row_bil = $db->fetchAll($bil_applicant);
					$row['program'][$key]['offer'] = $row_bil;
					
					if($row_bil){
						$row['program'][$key]['bil_offer'] = sizeof($row_bil);
					}else{
						$row['program'][$key]['bil_offer'] = 0;
					}
					
					//reject
					$bil_applicant = $db->select()
								->from(array('aau'=>'applicant_assessment_usm'),array())
								->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
								->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id', array('DISTINCT (at.at_trans_id)txn_id, at.at_period') )
								->join(array('ap'=>'tbl_academic_period'),'ap.ap_id = at.at_period',array('ap.ap_desc'))
								->join(array('apr'=>'applicant_program'),'apr.ap_at_trans_id = at.at_trans_id and apr.ap_usm_status = 0',array('apr.ap_prog_code'))
								->join(array('p'=>'tbl_program'),'p.ProgramCode = apr.ap_prog_code',array())
								->join(array('c'=>'tbl_collegemaster'),'c.IdCollege = p.IdCollege',array('IdCollege','ArabicName'))
								->where("aaud.aaud_nomor = '".$row['aaud_nomor']."'")
								->where('at.at_intake = '.$intake['IdIntake'])
								->where('at.at_period = '.$row['at_period'])
								->where("at.at_status in ('REJECT')")
							    ->where("at.at_selection_status = 3")
							    ->where("c.IdCollege = ".$prog_data['IdCollege']);
					
					$row_bil = $db->fetchAll($bil_applicant);
					//$row['program'][$key]['reject'] = $row_bil;
					
					if($row_bil){
						$row['program'][$key]['bil_reject'] = sizeof($row_bil);
					}else{
						$row['program'][$key]['bil_reject'] = 0;
					}
				}
			}
			
			

	        
	        $this->view->data = $row;
		}else{
			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'usm-selection-report'),'default',true));
		}
		
    }
    
	public function rectorSelectionDetailLocationAction(){
    	
    	$this->view->title = $this->view->translate("Senarai Keputusan Wakil Rektor : By Location");
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			//intake
			$intakeDb = new App_Model_Record_DbTable_Intake();
			$intake = $intakeDb->getData($formData['intake']);
			$this->view->intake = $intake;
			
			//period
			$periodDb = new App_Model_Record_DbTable_AcademicPeriod();
			$period = $periodDb->getData($formData['period']);
			$this->view->period = $period;

			$db = Zend_Db_Table::getDefaultAdapter();
		
			//decree number
			$select = $db->select()
							->from(array('aau'=>'applicant_assessment_usm'),array())
							->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id', array('aaud_nomor'))
							->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id',array('at.at_period'))
							->join(array('ap'=>'tbl_academic_period'),'ap.ap_id = at.at_period',array('ap.ap_desc'))
							->where('at.at_intake = '.$intake['IdIntake'])
							->where("at.at_status in ('OFFER','REGISTERED')")
						    ->where("at.at_selection_status = 3")
						    ->where("aaud.aaud_nomor = '".$formData['nomor']."'")
						    ->group("aaud.aaud_nomor")
						    ->group("at.at_period");
						    
			//filter by period
			if(isset($period)){
				$select->where('at.at_period = '.$period['ap_id']);
			}
						    
			$row = $db->fetchRow($select);
			if(!$row){
				$row = null;
			}
			
			//location
			if($row){
				
				$facultyList = $db->select()
								->from(array('aau'=>'applicant_assessment_usm'),array())
								->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
								->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id', array() )
								->join(array('ap'=>'tbl_academic_period'),'ap.ap_id = at.at_period',array())
								->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array('apt.'))
								->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
								->where("aaud.aaud_nomor = '".$row['aaud_nomor']."'")
								->where('at.at_intake = '.$intake['IdIntake'])
								->where('at.at_period = '.$row['at_period'])
								->where("at.at_status in ('OFFER','REGISTERED')")
							    ->where("at.at_selection_status = 3")
							    ->group("c.IdCollege");
							    
				$row['location'] = $db->fetchAll($facultyList);
				
			}
			
			echo "<pre>";
			print_r($row);
			echo "</pre>";
			exit;
			
			// bill offer/reject in program
			if($row['program']){
				foreach ($row['program'] as $key=>$prog_data){
					//offer
					$bil_applicant = $db->select()
								->from(array('aau'=>'applicant_assessment_usm'),array())
								->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
								->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id', array('DISTINCT (at.at_trans_id)txn_id, at.at_period') )
								->join(array('ap'=>'tbl_academic_period'),'ap.ap_id = at.at_period',array('ap.ap_desc'))
								->join(array('apr'=>'applicant_program'),'apr.ap_at_trans_id = at.at_trans_id and apr.ap_usm_status = 1',array('apr.ap_prog_code'))
								->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
								->join(array('aph'=>'appl_placement_head'),'apt.apt_ptest_code = aph.aph_placement_code',array())								
								->join(array('p'=>'tbl_program'),'p.ProgramCode = apr.ap_prog_code',array())
								->join(array('c'=>'tbl_collegemaster'),'c.IdCollege = p.IdCollege',array('IdCollege','ArabicName'))
								->where("aaud.aaud_nomor = '".$row['aaud_nomor']."'")
								->where('at.at_intake = '.$intake['IdIntake'])
								->where('at.at_period = '.$row['at_period'])
								->where("at.at_status in ('OFFER','REGISTERED')")
							    ->where("at.at_selection_status = 3")
							    ->where ("aph.aph_testtype = 0")
							    ->where("c.IdCollege = ".$prog_data['IdCollege']);
					
							    
					$row_bil = $db->fetchAll($bil_applicant);
					$row['program'][$key]['offer'] = $row_bil;
					
					if($row_bil){
						$row['program'][$key]['bil_offer'] = sizeof($row_bil);
					}else{
						$row['program'][$key]['bil_offer'] = 0;
					}
					
					//reject
					$bil_applicant = $db->select()
								->from(array('aau'=>'applicant_assessment_usm'),array())
								->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
								->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id', array('DISTINCT (at.at_trans_id)txn_id, at.at_period') )
								->join(array('ap'=>'tbl_academic_period'),'ap.ap_id = at.at_period',array('ap.ap_desc'))
								//->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
								//->join(array('aph'=>'appl_placement_head'),'apt.apt_ptest_code = aph.aph_placement_code',array())
								->join(array('apr'=>'applicant_program'),'apr.ap_at_trans_id = at.at_trans_id and apr.ap_usm_status = 0',array('apr.ap_prog_code'))
								->join(array('p'=>'tbl_program'),'p.ProgramCode = apr.ap_prog_code',array())
								->join(array('c'=>'tbl_collegemaster'),'c.IdCollege = p.IdCollege',array('IdCollege','ArabicName'))
								->where("aaud.aaud_nomor = '".$row['aaud_nomor']."'")
								->where('at.at_intake = '.$intake['IdIntake'])
								->where('at.at_period = '.$row['at_period'])
								->where("at.at_status in ('REJECT')")
							    ->where("at.at_selection_status = 3")
							    //->where ("aph.aph_testtype = 0")
							    ->where("c.IdCollege = ".$prog_data['IdCollege']);
					
					$row_bil = $db->fetchAll($bil_applicant);
					//$row['program'][$key]['reject'] = $row_bil;
					
					if($row_bil){
						$row['program'][$key]['bil_reject'] = sizeof($row_bil);
					}else{
						$row['program'][$key]['bil_reject'] = 0;
					}
				}
			}
			
			

	        
	        $this->view->data = $row;
		}else{
			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'usm-selection-report'),'default',true));
		}
		
    }
    
    public function offerReportAction(){
    	
    	$this->_helper->layout->setLayout('print');
    	
    	$date = null;
		$location_id = null;
		$nomor = null;
		
    	if ($this->getRequest()->isPost()) {
			
    		$formData = $this->getRequest()->getPost();
    		
    		$this->view->rank = $formData['rank'];
    		
    		//intake
			$intakeDb = new App_Model_Record_DbTable_Intake();
			$intake = $intakeDb->getData($formData['intake']);
			$this->view->intake = $intake;
			
			//date
			if( isset($formData['date']) && $formData['date']!="ALL" ){
				$date = $formData['date'];
			}
			
			//location
			if( isset($formData['location']) && $formData['location']!="ALL" ){
				$location_id = $formData['location'];
			}
			
    		//nomor
			if( isset($formData['nomor']) ){
				$nomor = $formData['nomor'];
			}
			
	    	$this->view->intake = $intake;
		
			
			
			$db = Zend_Db_Table::getDefaultAdapter();
			
			//decree number
			$select = $db->select()
							->from(array('aau'=>'applicant_assessment_usm'),array())
							->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id', array('aaud_nomor','aaud_decree_date'))
							->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id',array('at.at_intake'))
							->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
							->join(array('aph'=>'appl_placement_head'),'apt.apt_ptest_code = aph.aph_placement_code',array())
							->join(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_id','aps.aps_test_date'))
							->join(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
							->where('at.at_intake = '.$intake['IdIntake'])
							//->where("at.at_status in ('OFFER','REGISTERED')")
						    ->where("at.at_selection_status = 3")
						    ->where ("aph.aph_testtype = 0")
						    //->group("apt.apt_aps_id")
						    ->group("aaud.aaud_decree_date")
						    ->group("al.al_id")
						    ->group("aaud.aaud_nomor");
						    
			//filter by date
			if($date!=null){
				$select->where("aaud.aaud_decree_date = '".$date."'");
			}
			
			//filter by location
			if($location_id!=null){
				$select->where('al.al_id = '.$location_id);
			}
			
    		//filter by nomor
			if($nomor!=null){
				$select->where("aaud.aaud_nomor = '".$nomor."'");
			}
			
			
			$row = $db->fetchRow($select);
			if(!$row){
				$row = null;
			}
			
			// bill offer/reject
			if($row){
				
				//offer
				$bil_applicant = $db->select()
							->from(array('aau'=>'applicant_assessment_usm'),array('DISTINCT (at.at_trans_id)txn_id','aau.aau_rector_ranking'))
							->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
							->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id', array('at_pes_id') )
							->join(array('ap'=>'applicant_profile'),'ap.appl_id = at.at_appl_id')
							->join(array('apr'=>'applicant_program'),'apr.ap_at_trans_id = at.at_trans_id and apr.ap_usm_status = 1', array('apr.ap_prog_code'))
							->join(array('p'=>'tbl_program'),'p.ProgramCode = apr.ap_prog_code', array('p.ArabicName'))
							->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array('apt.apt_no_pes','apt.apt_bill_no'))
							->join(array('aph'=>'appl_placement_head'),'apt.apt_ptest_code = aph.aph_placement_code',array())
							->join(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_test_date'))
							->join(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
							->where("aaud.aaud_nomor = '".$row['aaud_nomor']."'")
							->where('at.at_intake = '.$row['at_intake'])
							->where("aaud.aaud_decree_date = '".$row['aaud_decree_date']."'")
							->where("al.al_id = ".$row['al_id'])
							->where("at.at_status in ('OFFER','REGISTERED')")
						    ->where("at.at_selection_status = 3")
						    ->where ("aph.aph_testtype = 0")
						    ->where("aau.aau_rector_status = 1")
						    //->order("p.ArabicName")
						    ->order("ap.appl_fname");
						    			
						   
						    
				$row_bil = $db->fetchAll($bil_applicant);
				$row['offer'] = $row_bil;
				
				if($row_bil){
					$row['bil_offer'] = sizeof($row_bil);
				}else{
					$row['bil_offer'] = 0;
				}
				
			}

			$this->view->applicant = $row;
						
		}else{
			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'usm-selection-report'),'default',true));
		}	
    }

    public function nextReportAction(){
    	
    	$this->_helper->layout->setLayout('print');
    	
    	$date = null;
		$location_id = null;
		$nomor = null;
		
    	if ($this->getRequest()->isPost()) {
			
    		$formData = $this->getRequest()->getPost();
    		
    		$this->view->rank = $formData['rank'];
    		
    		//intake
			$intakeDb = new App_Model_Record_DbTable_Intake();
			$intake = $intakeDb->getData($formData['intake']);
			$this->view->intake = $intake;
			
			//date
			if( isset($formData['date']) && $formData['date']!="ALL" ){
				$date = $formData['date'];
			}
			
			//location
			if( isset($formData['location']) && $formData['location']!="ALL" ){
				$location_id = $formData['location'];
			}
			
    		//nomor
			if( isset($formData['nomor']) ){
				$nomor = $formData['nomor'];
			}
			
	    	$this->view->intake = $intake;
		
			
			
			$db = Zend_Db_Table::getDefaultAdapter();
			
			//decree number
			$select = $db->select()
						->from(array('aau'=>'applicant_assessment_usm'),array())
						->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id', array('aaud.aaud_nomor','aaud.aaud_decree_date'))
						->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id',array('at.at_intake'))
						->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
						->join(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_id','aps.aps_test_date'))
						->join(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
						->where('at.at_intake = '.$intake['IdIntake'])
						//->where("at.at_status in ('OFFER','REGISTERED')")
					    ->where("at.at_selection_status = 3 OR at.at_selection_status = 5")
					    //->group("apt.apt_aps_id")
					    //->group("aps.aps_test_date")aaud_decree_date
					    ->group("aaud.aaud_decree_date")
					    ->group("al.al_id")
					    ->group("aaud.aaud_nomor");
						    
			//filter by date
			if($date!=null){
				$select->where("aaud.aaud_decree_date = '".$date."'");
			}
			
			//filter by location
			if($location_id!=null){
				$select->where('al.al_id = '.$location_id);
			}
			
    		//filter by nomor
			if($nomor!=null){
				$select->where("aaud.aaud_nomor = '".$nomor."'");
			}
			//echo $select;
			
			$row = $db->fetchRow($select);
			if(!$row){
				$row = null;
			}
			
			// bill offer/reject
			if($row){
				
				//tahap1
				$bil_applicant = $db->select()
							->from(array('aau'=>'applicant_assessment_usm'),array('DISTINCT (at.at_trans_id)txn_id','aau.aau_rector_ranking'))
							->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
							->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id', array('at_pes_id') )
							->join(array('ap'=>'applicant_profile'),'ap.appl_id = at.at_appl_id')
							->join(array('apr'=>'applicant_program'),'apr.ap_at_trans_id = at.at_trans_id and (apr.ap_usm_status = 1 OR apr.ap_usm_status = 3)', array('apr.ap_prog_code'))
							->join(array('p'=>'tbl_program'),'p.ProgramCode = apr.ap_prog_code', array('p.ArabicName'))
							->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array('apt.apt_no_pes','apt.apt_bill_no'))
							->join(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_test_date'))
							->join(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
							->join(array('aph'=>'appl_placement_head'),'apt.apt_ptest_code = aph.aph_placement_code',array())
							->where("aaud.aaud_nomor = '".$row['aaud_nomor']."'")
							->where('at.at_intake = '.$row['at_intake'])
							->where("aaud.aaud_decree_date = '".$row['aaud_decree_date']."'")
							->where("al.al_id = ".$row['al_id'])
							->where("aph.aph_testtype=0")
							//->where("at.at_selection_status = 5")
							->where("aau.aau_rector_status = 5")
						    //->order("p.ArabicName")
						    ->order("ap.appl_fname");
	
				$row_bil = $db->fetchAll($bil_applicant);
				$row['offer'] = $row_bil;
				
				if($row_bil){
					$row['bil_offer'] = sizeof($row_bil);
				}else{
					$row['bil_offer'] = 0;
				}
				
			}

			$this->view->applicant = $row;
						
		}else{
			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'usm-selection-report'),'default',true));
		}	
    }   
    
	public function rejectReportAction(){
    	
    	$this->_helper->layout->setLayout('print');
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale=$locale;
    	
    	$date = null;
		$location_id = null;
		$nomor = null;
		
    	if ($this->getRequest()->isPost()) {
			
    		$formData = $this->getRequest()->getPost();
    		    		
    		//intake
			$intakeDb = new App_Model_Record_DbTable_Intake();
			$intake = $intakeDb->getData($formData['intake']);
			$this->view->intake = $intake;
			
			//date
			if( isset($formData['date']) && $formData['date']!="ALL" ){
				$date = $formData['date'];
			}
			
			//location
			if( isset($formData['location']) && $formData['location']!="ALL" ){
				$location_id = $formData['location'];
			}
			
    		//nomor
			if( isset($formData['nomor']) ){
				$nomor = $formData['nomor'];
			}
			
	    	$this->view->intake = $intake;
		
			
			
			$db = Zend_Db_Table::getDefaultAdapter();
			
			//decree number
			/*$select = $db->select()
							->from(array('aau'=>'applicant_assessment_usm'),array())
							->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id', array('aaud_nomor','aaud_decree_date'))
							->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id',array('at.at_intake'))
							->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
							->join(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_id','aps.aps_test_date'))
							->join(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
							->where('at.at_intake = '.$intake['IdIntake'])
							//->where("at.at_status in ('OFFER','REGISTERED')")
						    ->where("at.at_selection_status = 3")
						    //->group("apt.apt_aps_id")
						    ->group("aps.aps_test_date")
						    ->group("al.al_id")
						    ->group("aaud.aaud_nomor");*/
			
			$select = $db->select()
			->from(array('aau'=>'applicant_assessment_usm'),array())
			->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id', array('aaud.aaud_nomor','aaud.aaud_decree_date'))
			->join(array('at'=>'applicant_transaction'),'aau.aau_trans_id = at.at_trans_id',array('at.at_intake'))
			->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array())
			->join(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_id','aps.aps_test_date'))
			->join(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
			->where('at.at_intake = '.$intake['IdIntake'])
			//->where("at.at_status in ('OFFER','REGISTERED')")
			->where("at.at_selection_status = 3 OR at.at_selection_status = 5")
			//->group("apt.apt_aps_id")
			//->group("aps.aps_test_date")aaud_decree_date
			->group("aaud.aaud_decree_date")
			->group("al.al_id")
			->group("aaud.aaud_nomor");
						    
			//filter by date
			if($date!=null){
				$select->where("aaud.aaud_decree_date = '".$date."'");
			}
			
			//filter by location
			if($location_id!=null){
				$select->where('al.al_id = '.$location_id);
			}
			
    		//filter by nomor
			if($nomor!=null){
				$select->where("aaud.aaud_nomor = '".$nomor."'");
			}
			
			
			$row = $db->fetchRow($select);
			if(!$row){
				$row = null;
			}
			
			
			// bill offer/reject
			if($row){
				
				//reject
				/*$bil_applicant = $db->select()
							//->from(array('aau'=>'applicant_assessment_usm'),array())
							//->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id',array())
							->from(array('at'=>'applicant_transaction'), array('DISTINCT (at.at_trans_id)txn_id') )
							->join(array('ap'=>'applicant_profile'),'ap.appl_id = at.at_appl_id')
							//->joinLeft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id = at.at_trans_id', array('apr.ap_prog_code'))
							//->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode = apr.ap_prog_code', array('p.ArabicName'))
							->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array('apt.apt_no_pes','apt.apt_bill_no'))
							->joinleft(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_test_date'))
							->joinleft(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
							->where("aaud.aaud_nomor = '".$data['aaud_nomor']."'")
							->where('at.at_intake = '.$row['at_intake'])
							->where("aaud.aaud_decree_date = '".$row['aaud_decree_date']."'")
							//->where("aps.aps_test_date = '".$row['aps_test_date']."'")
							->where("al.al_id = ".$row['al_id'])
							->where("at.at_status in ('REJECT')")
						    ->where("at.at_selection_status = 3");
						    //->order("p.ArabicName");*/
						    
						    $bil_applicant = $db->select()
						    ->from(array('at'=>'applicant_transaction'),array('(at.at_trans_id)txn_id','at.at_status') )
						    ->join(array('ap'=>'applicant_profile'),'ap.appl_id = at.at_appl_id')
						    ->join(array('aau'=>'applicant_assessment_usm'),'aau.aau_trans_id = at.at_trans_id',array('aau.aau_rector_status'))
						    ->join(array('aaud'=>'applicant_assessment_usm_detl'), 'aau.aau_rector_selectionid = aaud.aaud_id and aaud.aaud_type = 2',array('aaud.aaud_nomor'))
						    ->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id = at.at_trans_id',array('apt.apt_no_pes','apt.apt_bill_no'))
						    ->join(array('aph'=>'appl_placement_head'),'apt.apt_ptest_code = aph.aph_placement_code',array())
						    ->join(array('aps'=>'appl_placement_schedule'),'aps.aps_id = apt.apt_aps_id',array('aps.aps_test_date'))
						    ->join(array('al'=>'appl_location'),'al.al_id = aps.aps_location_id',array('al.al_id','al.al_location_name'))
						    ->where("aaud.aaud_nomor = '".$row['aaud_nomor']."'")
						    ->where('at.at_intake = '.$row['at_intake'])
						    ->where("aaud.aaud_decree_date = '".$row['aaud_decree_date']."'")
						    ->where("al.al_id = ".$row['al_id'])
						    ->where("aph.aph_testtype=0")
						    ->where("at.at_status = 'REJECT'");
						    
				$row_bil = $db->fetchAll($bil_applicant);
				$row['reject'] = $row_bil;
				
				if($row_bil){
					$row['bil_reject'] = sizeof($row_bil);
				}else{
					$row['bil_reject'] = 0;
				}
				
			}
			
			$this->view->applicant = $row;
			
						
		}else{
			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'usm-selection-report'),'default',true));
		}	
    }
}
?>