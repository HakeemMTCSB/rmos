<?php
/**
 * @author Ajaque Rahman
 * @version 1.0
 */

class Application_ManageApplicantController extends Zend_Controller_Action {
	
	private $_DbObj;
	private $_config;
	
	public function init(){
		
/*		$sis_session = new Zend_Session_Namespace('sis');
		$configDb = new GeneralSetup_Model_DbTable_Initialconfiguration();
		$this->_config = $configDb->fnGetInitialConfigDetails($sis_session->idUniversity);*/
		
		
	}
	
	public function indexAction() {
    	$this->view->title= $this->view->translate("Manage USM Applicant (OMR) - By Schedule");
    	
    	$form = new Application_Form_PlacementTestSearch();
    	
    	$scheduleDb = new App_Model_Application_DbTable_PlacementTestSchedule();
    	
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			$form->populate($formData);
			
			$cond = array(
				'location' => $formData['location']
			);
			
			//intake
    		if( isset($formData['intake_id']) && $formData['intake_id']!="" ){
				$intakeDb = new App_Model_Record_DbTable_Intake();
				$intakeData = $intakeDb->getData($formData['intake_id']);

				$cond['dt_from'] = date('Y-m-d', strtotime($intakeData['ApplicationStartDate']));
				$cond['dt_to'] = date('Y-m-d', strtotime($intakeData['ApplicationEndDate']));
			}
			//period
			if( isset($formData['period_id']) && $formData['period_id']!="" ){
				$periodDb = new App_Model_Record_DbTable_AcademicPeriod();
				$periodData = $periodDb->getData($formData['period_id']);
				
				$cond['dt_from'] = date('d-n-Y', strtotime('01-'.$periodData['ap_month']."-".$periodData['ap_year']));
				$cond['dt_to'] = date('t-m-Y', strtotime($cond['dt_from']));
			}
			
			//get schedule by search
			$list = $scheduleDb->search($cond);
			
			$this->view->scheduleList = $list; 
    	}else{
    		$list = $scheduleDb->getData();
    	}
    	
    	$this->view->placementTestList = $list;
    	
    	$this->view->form = $form;  		
	}
	public function scheduleDetailAction(){
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		setlocale (LC_ALL, $locale);
		
		$schedule_id = $this->_getParam('scid', 0);
		$this->view->scid = $schedule_id;

		
		$this->view->title=$this->view->translate("Edit USM Applicant (OMR) - By Schedule");
		
		//schedule data
    	$placementTestScheduleDb = new App_Model_Application_DbTable_PlacementTestSchedule();
    	$scheduleData = $placementTestScheduleDb->getScheduleInfo($schedule_id);
    	$this->view->schedule_data = $scheduleData;

		
		$db = Zend_Db_Table::getDefaultAdapter();

		//list of applicant
	    $select = $db ->select()
					->from(array('apt'=>'applicant_ptest'))
					-> join(array('at'=>'applicant_transaction'),'apt.apt_at_trans_id = at.at_trans_id')
					->where("apt.apt_aps_id='".$schedule_id."'")
					->where("at.at_create_by=99999");					
					;														
       //echo $select;
        $row = $db->fetchAll($select);
        
        $applicantList = $row;
        
        //loop budak utk cari programme
        foreach ($applicantList as $key=>$applicant){
        	
        	$txn_id = $applicant['apt_at_trans_id'];
        	
        	$select2 = $db ->select()
					->from(array('ap'=>'applicant_program'))
					->join(array('p'=>'tbl_program'), 'p.ProgramCode = ap.ap_prog_code', array('ptest_room_type','ArabicName'))
					->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege = p.IdCollege', array('ShortName'))
					->where("ap.ap_at_trans_id='".$txn_id."'")
					->Order("ap.ap_preference asc");
			$row2 = $db->fetchAll($select2);

       		if($row2){
       			$applicantList[$key]['program'] = $row2;
       		}else{
       			$applicantList[$key]['program'] = null;
       		}
 			
       		if($this->getParam("asgnroom")==1){
       			$aprogDB  = new App_Model_Application_DbTable_ApplicantProgram();
       		
       			if(trim($row2[1]["ap_prog_code"])=="") $row2[1]["ap_prog_code"]=$row2[0]["ap_prog_code"];
       			$aprogDB->getProcedure($txn_id,$row2[0]["ap_prog_code"],$row2[1]["ap_prog_code"],$schedule_id);      		
       			//echo "$txn_id-".$row2[0]["ap_prog_code"]."-".$row2[1]["ap_prog_code"]."-".$schedule_id."<hr>";
       		
       		}
       		
       		//get applicant's room type from all programs
       		if( !isset($row2[1]) ){
       			$row2[1]['ptest_room_type'] = 0;
       		}
       		
        	$select3 = $db ->select()
					->from(array('ara'=>'appl_room_assign'),array('ara_room'))
					->where("ara.ara_program1='".$row2[0]['ptest_room_type']."'")
					->where("ara.ara_program2='".$row2[1]['ptest_room_type']."'");
       		
			$row3 = $db->fetchRow($select3);
				
			$applicantList[$key]['room_type'] = $row3['ara_room'];
			
			
        }
        
        //loop applicant and inject profile and transaction data
        foreach ($applicantList as $key=>$applicant){
        	
        	//get transaction data
        	$select = $db ->select()
					->from(array('at'=>'applicant_transaction'))
					->where("at.at_trans_id = '".$applicant['apt_at_trans_id']."'");
																			
       		$row = $db->fetchRow($select);
       		$applicantList[$key]['transaction'] = $row;
        	
        	//get profile data
        	$select2 = $db ->select()
					->from(array('ap'=>'applicant_profile'))
					->where("ap.appl_id = '".$row['at_appl_id']."'");
																			
       		$row2 = $db->fetchRow($select2);
       		$applicantList[$key]['profile'] = $row2;
        	
        }
        
        $this->view->applicant_list = $applicantList;

	}	
	
	public function editApplicantAction(){
    	$this->view->title= $this->view->translate("Edit USM Applicant (OMR) - By Schedule");
    	$txnid=$this->_getparam("txnid",0);
    	$scid = $this->_getparam("scid",0);
    	
    	$this->view->scid=$scid;
		$db = Zend_Db_Table::getDefaultAdapter();
			
		if ($this->getRequest()->isPost()){
			$formData = $this->getRequest()->getPost();
			$multidb = Zend_Registry::get("multidb");
            $mdb = $multidb->getDb('master');
			//$db = Zend_Db_Table::getDefaultAdapter();
			//save no pes @ applicant_ptest.apt_no_pes			
			//save no formulir @ applicant_ptest.apt_bill_no
			$aptdata["apt_no_pes"] = $formData["no_pes"];
			$aptdata["apt_bill_no"]= $formData["no_formulir"];
			$mdb->update("applicant_ptest",$aptdata,"apt_at_trans_id = $txnid");
			
			//save no formulir @ applicant_transaction.at_pes_id
			$atdata["at_pes_id"] = $formData["no_formulir"];
			$mdb->update("applicant_transaction",$atdata,"at_trans_id = $txnid");
			
			//save name @ applicant_profile.appl_fname
			//save email @ applicant_profile.appl_email
			$apdata["appl_email"] = $formData["appl_email"];
			$apdata["appl_fname"] = $formData["appl_name"];
			$mdb->update("applicant_profile",$apdata,"appl_id = ".$formData["profileid"]);
			
			//save exam code @ applicant_ptest_ans.apa_set_code 
			$apadata["apa_set_code"] = $formData["exam_code"];
			$mdb->update("applicant_ptest_ans",$apadata,"apa_trans_id = ".$txnid);
			
			
			//save program 1 and 2 @ applicant_program.ap_prog_code
			$ap1data["ap_prog_code"] = $formData["app_id1"];
			$mdb->update("applicant_program",$ap1data,"ap_at_trans_id = ".$txnid." AND ap_preference=1");
			
			$ap2data["ap_prog_code"] = $formData["app_id2"];
			$mdb->update("applicant_program",$ap2data,"ap_at_trans_id = ".$txnid." AND ap_preference=2");
			
			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'manage-applicant', 'action'=>'schedule-detail','scid'=>$scid),'default',true));
			
		}
		$sql = $db->select()
				->from(array('at'=>"applicant_transaction"))
				->join(array('ap'=>"applicant_profile"),"at.at_appl_id=ap.appl_id",array('email'=>'ap.appl_email','fname'=>'ap.appl_fname','mname'=>'ap.appl_mname','lname'=>'ap.appl_lname'))
    			->join(array('apt'=>'applicant_ptest'),"at.at_trans_id=apt.apt_at_trans_id",array('ptestcode'=>'apt.apt_ptest_code','no_pes'=>"apt.apt_no_pes"))
    			->join(array('apa'=>'applicant_ptest_ans'),"at.at_trans_id=apa.apa_trans_id",array('setcode'=>'apa.apa_set_code'))
				->where ("at.at_trans_id = ?", $txnid);
    			//echo $sql;
    	$row = $db->fetchRow($sql);
    	
    	$appprog = new App_Model_Application_DbTable_ApplicantProgram();
    	$form = new Application_Form_Applicant(array('code'=>$row["ptestcode"]));	
    	
    	$prog1=$appprog->getProgramPreference($txnid,1);
    	$prog2=$appprog->getProgramPreference($txnid,2);

    	$formdata["no_pes"]=$row["no_pes"];
    	$formdata["no_formulir"]=$row["at_pes_id"];
    	$formdata["appl_name"]=$row["fname"];
    	$formdata["appl_email"]=$row["email"];
    	$formdata["exam_code"]=$row["setcode"];
    	$formdata["app_id1"]=$prog1['ap_prog_code'];
    	$formdata["app_id2"]=$prog2['ap_prog_code'];
    	$formdata["profileid"]=$row['at_appl_id'];
    	$form->populate($formdata);
		$this->view->form = $form; 
	}
	
	public function mergeAction(){
		$this->view->title=$this->view->translate("Merge 2 applications to a profile");
		$form = new Application_Form_Merge();
		if ($this->_request->isPost()) {
			$auth = Zend_Auth::getInstance(); 
			
			$this->view->print=1;
			$formData = $this->_request->getPost();
			$profileDB = new App_Model_Application_DbTable_ApplicantProfile();
			if($this->_getparam("process",null)==1){
				
				$chdata["maintain_appl_id"]=$formData["mprofile"];
				if($formData["fprofile1"]==$chdata["maintain_appl_id"]){
					$maintain=1;	
					$change=2;					
				}else{
					$maintain=2;
					$change=1;					
				}

				$chdata["archive_appl_id"]=$formData["fprofile".$change];
				$chdata["maintain_trans_id"]=$formData["ftrans".$maintain];
				$chdata["chg_trans_id"]=$formData["ftrans".$change];			
				$chdata["chg_formulir"]=$formData["fpes".$change];	
				
				$chdata["maintain_formulir"]=$formData["fpes".$maintain];
				$chdata["mail_archive"]=$formData["femail".$change];
				$chdata["mail_maintain"]=$formData["femail".$maintain];
				$chdata["mergeby"]=$auth->getIdentity()->id;
				$chdata["mergedate"]=date("Y-m-d H:i:s");
				$profileDB->mergeProfile($chdata); 
				
				$this->view->chdata = $chdata;
				$this->view->merged=1;
				
			}else{
			
				
				$progDB = new App_Model_Application_DbTable_ApplicantProgram();
				$profile1 = $profileDB->getProfileByFormulir($formData["no_formulir1"]);
				$profile2 = $profileDB->getProfileByFormulir($formData["no_formulir2"]);
				
				if(is_array($profile1)){
					$this->view->profile1=$profile1;
					$this->view->prog1=$progDB->getProgram($profile1["at_trans_id"]);
					$this->view->searched=1;
				}else{
					$this->view->error1="no profile 1";
				}
				
				if(is_array($profile2)){
					$this->view->profile2=$profile2;
					$this->view->prog2=$progDB->getProgram($profile2["at_trans_id"]);
					$this->view->searched=1;
				}else{
					
					$this->view->error2="no profile 2";
				}			
			
			}
			
		}

		$this->view->form = $form; 
		
	}		
}

?>