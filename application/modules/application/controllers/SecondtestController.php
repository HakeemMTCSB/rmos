<?php 
class Application_SecondtestController extends Base_Base {
	
	public function indexAction() {
		
		$msg = $this->_getParam('msg', null);
		$this->view->noticeMessage = $msg;
		
		$this->view->title = $this->view->translate("TPA - Schedule Assignment");
		$candidatedb =  new App_Model_Application_DbTable_Secondtest();
		
		
		$searchform = new Application_Form_SelectionStatusSearch();
		$searchform->removeElement('selection_status');
		$searchform->removeElement('load_previous_period');
		$searchform->removeElement('period_id');
		$searchform->removeElement('programme');
		$searchform->removeElement('faculty');
		$searchform->getElement("intake_id")->setAttrib("onchange",null);
		$searchform->setAction($this->view->url(array('module'=>'application','controller'=>'secondtest', 'action'=>'index'),'default',true));
		
		
    	if ($this->_request->isPost()) {
	    	$formdata = $this->_request->getPost();
	    	
	    	$this->view->intake_id = $formdata["intake_id"];
	    	$searchform->populate(array('intake_id'=>$formdata["intake_id"]));
	    	
	    	$applicant_list= $candidatedb->getCandidate($formdata["intake_id"]);
	    	
	    	
	    	
	    	//set program offer
	    	$applicantProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
	    	foreach ($applicant_list as $index=>$applicant){
	    		$program_offered = $applicantProgramDb->getProgramOffered($applicant['at_trans_id'], $applicant['at_appl_type']);
	    		$applicant_list[$index]['program_offered'] = $program_offered;
	    	}
	    	
	    	/*echo "<pre>";
	    	print_r($applicant_list);
	    	echo "</pre>";*/
	    	 
	    	 $this->view->rows = $applicant_list;
	    	 
	    	 
	    	 //location
	    	 $placementTestLocationDb = new App_Model_Application_DbTable_PlacementTestLocation();
	    	 $this->view->locations = $placementTestLocationDb->getData();
	    	 
    	}
    	
    	$this->view->form=$searchform;
	
	}
	
	public function getExamScheduleAction(){
		
		$intake_id = $this->_getParam('intake', 0);
		$location_id = $this->_getParam('location', 0);
		 
		//if ($this->getRequest()->isXmlHttpRequest()) {
		$this->_helper->layout->disableLayout();
		//}
		
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('sc'=>'appl_placement_schedule'))
			->join(array('aph'=>'appl_placement_head'), 'aph.aph_placement_code = sc.aps_placement_code ')
			->where('aph.aph_academic_year = ?', $intake_id)
			->where('aph.aph_testtype = 1')
			->where('sc.aps_test_date >= now()')
			->order('sc.aps_test_date');
		
		if($location_id!=0){
			$select->where('sc.aps_location_id = ?', $location_id);
		}
		 
		$stmt = $db->query($select);
		$row = $stmt->fetchAll();
		
		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();
		
		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
	}
	
	public function saveAssignStudentAction(){
		$msg = null;
		
		if ($this->_request->isPost()) {
			$formdata = $this->_request->getPost();
			
			//get ptest from schedule
			$placementTestDb = new App_Model_Application_DbTable_PlacementTestSchedule();
			$ptestSchedule = $placementTestDb->getData($formdata['schedule_id']);
			
			
				
				foreach ($formdata['nopes'] as $pes){

					//get txn from pes
					$applicantTransactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
					$txnData = $applicantTransactionDb->getTransactionDataByFomulir($pes);
						
					//get offered program
					$applicantProgramDb = new App_Model_Application_DbTable_ApplicantProgram();
					$program_offered = $applicantProgramDb->getProgramOffered($txnData['at_trans_id'], $txnData['at_appl_type']);
						
					//insert applicant ptest
					$applicantPtestDb = new App_Model_Application_DbTable_ApplicantPtest();
					$ptest_data = array(
							'apt_at_trans_id' => $txnData['at_trans_id'],
							'apt_appl_id' => $txnData['at_appl_id'],
							'apt_no_pes' => $txnData['at_pes_id'],
							'apt_ptest_code' => $ptestSchedule['aps_placement_code'],
							'apt_aps_id' => $ptestSchedule['aps_id'],
							'apt_fee_amt' => 0,
							'apt_usm_attendance' => 0
					);
					
					$ptest_id = $applicantPtestDb->insert($ptest_data);
					
					
					//to get and update sit no to ptest using procedure
					$appprogramDB = new App_Model_Application_DbTable_ApplicantProgram();
					
					$data = $appprogramDB->getProcedure($txnData['at_trans_id'],$program_offered["ap_prog_code"],$program_offered["ap_prog_code"],$formdata['schedule_id'], 1);
					
					
					if($data[0]["roomid"]==0){
						$msg="Maaf tempat untuk USM telah penuh. Sila hubungi pihak manajemen universitas";
						$applicantPtestDb->delete('apt_id = '.$ptest_id.' and apt_at_trans_id = '.$txnData['at_trans_id']);
					}
				}
				
				
			
			
		}
				
		$this->_redirect($this->view->url(array('module'=>'application','controller'=>'secondtest', 'action'=>'index', 'msg'=>$msg),'default',true));
		
	}
}

?>