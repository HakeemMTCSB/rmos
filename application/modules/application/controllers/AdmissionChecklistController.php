<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_AdmissionChecklistController extends Base_Base
{

    private $_gobjlog;
    private $locale;
    private $dclModel;
    private $defModel;
    private $lobjAppItem;
    private $lobjAppConfig;

    public function init()
    {
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj()
    {
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->dclModel = new Application_Model_DbTable_DocumentChecklist();
        //$this->defModel = new App_Model_General_DbTable_Definationms();
        $this->defModel = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $this->lobjAppItem = new Application_Model_DbTable_ApplicationItem();
        //$this->lobjAppConfig = new Application_Model_DbTable_ApplicationInitialConfig();
    }

    public function indexAction()
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $this->view->title = $this->view->translate("Admission Checklist");
        $message = $this->_getParam('message', 0);
        $this->view->message = $message;
        $copyForm = new Application_Form_CopyAdmissionChecklist();
        $getUserIdentity = $this->auth->getIdentity();

        $cacheObj = new Cms_Cache();
        $cache = $cacheObj->get();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();




            if (isset($formData['Search']) && $formData['Search'] == 'Search') {



                $formData['field1'] = $this->view->escape(strip_tags($formData['field1']));
                $formData['field3'] = $this->view->escape(strip_tags($formData['field3']));
                $formData['field2'] = $this->view->escape(strip_tags($formData['field2']));
                $formData['field4'] = $this->view->escape(strip_tags($formData['field4']));

                $searchForm = new Application_Form_ChecklistSearch(array('IdProgram' => $formData['field3']));
                $searchForm->populate($formData);
                $dclList = $this->dclModel->getDclSearchList($formData);

                /*if (count($dclList) > 0) {

                    $i = 0;
                    foreach ($dclList as $dclLoop) {
                        echo "sini";
                        exit;
                        $mop = $this->defModel->getData($dclLoop['mode_of_program']);
                        $mod = $this->defModel->getData($dclLoop['mode_of_study']);
                        $pt = $this->defModel->getData($dclLoop['program_type']);
                        $sc = $this->defModel->getData($dclLoop['dcl_stdCtgy']);
                        $dt = $this->defModel->getData($dclLoop['dcl_uplType']);

                        $dclList[$i]['ProgramScheme'] = $pt['DefinitionDesc'] . '<br/>' . $mop['DefinitionDesc'] . '<br/>' . $mod['DefinitionDesc'];
                        $dclList[$i]['studentType'] = $sc['DefinitionDesc'];
                        $dclList[$i]['docType'] = $dt['DefinitionDesc'];

                        $i++;
                    }
                }*/

                if (count($dclList) > 0) {

                    $i = 0;
                    foreach ($dclList as $dclLoop) {

                        $dclList[$i]['awardName'] =  $dclLoop['awardName'] . '<br/>';
                        $dclList[$i]['mopName'] = $dclLoop['mopName'];
                        $dclList[$i]['ptName'] = $dclLoop['ptName'];
                        $dclList[$i]['ProgramName'] = $dclLoop['ProgramName'];
                        $dclList[$i]['studentCategory'] = $dclLoop['studentCategory'];

                        $i++;
                    }

                }

                $cache->save($dclList, 'admissionchecklistpaginatorresult');
                //$this->gobjsessionsis->admissionchecklistpaginatorresult = $dclList;

                $this->_redirect($this->baseUrl . '/application/admission-checklist/index/');
            } else if (isset($formData['Copy']) && $formData['Copy'] == 'Copy') {
                //dd($formData);
               // exit;

                if ($formData['programSchemeFrom'] == $formData['programSchemeTo'] && $formData['studentCategoryFrom'] == $formData['studentCategoryTo']) {
                    $this->_helper->flashMessenger->addMessage(array('error' => "Cannot copy, copy from and copy to cannot be the same"));
                    $this->_redirect($this->baseUrl . '/application/admission-checklist/index/');
                } else {

                    ////////////////////////// copy checklist to main table //////////////////////////////

                    $checkExist = $this->dclModel->getConfigcheckcopy($formData['programTo'] ?? 0, $formData['programSchemeTo'] ?? 0, $formData['studentCategoryTo'] ?? 0);

                    if (!$checkExist) {

                        $getCopyMainChecklist = $this->dclModel->getDclMainCopy($formData['programFrom'], $formData['programSchemeFrom'], $formData['studentCategoryFrom']);

                        if (count($getCopyMainChecklist) > 0) {
                            foreach ($getCopyMainChecklist as $getCopyMainChecklistLoop) {
                                $copyData = array(

                                    'IdScheme' => $getCopyMainChecklistLoop['IdScheme'],
                                    'IdProgram' => $formData['programTo'],
                                    'IdProgramScheme' => $formData['programSchemeTo'],
                                    'category' => $formData['studentCategoryTo'],
                                    'created_by' => $getUserIdentity->id,
                                    'created_at' => date('Y-m-d H:i:s')
                                );
                                //$this->dclModel->insert($copyData);
                                $db->insert('tbl_documentchecklist_main', $copyData);

                            }
                        }

                    ///////////////////////////////////////////////////////////////////////////////////

                    $getCopyDocChecklist = $this->dclModel->getDclCopy($formData['programSchemeFrom'], $formData['studentCategoryFrom']);

                    if (count($getCopyDocChecklist) > 0) {
                        foreach ($getCopyDocChecklist as $getCopyDocChecklistLoop) {
                            $copyData = array(
                                'dcl_stdCtgy'       => $formData['studentCategoryTo'],
                                'dcl_programScheme' => $formData['programSchemeTo'],
                                'dcl_sectionid'     => $getCopyDocChecklistLoop['dcl_sectionid'],
                                'dcl_name'          => $getCopyDocChecklistLoop['dcl_name'],
                                'dcl_malayName'     => $getCopyDocChecklistLoop['dcl_malayName'],
                                'dcl_desc'          => $getCopyDocChecklistLoop['dcl_desc'],
                                'dcl_type'          => $getCopyDocChecklistLoop['dcl_type'],
                                'dcl_activeStatus'  => $getCopyDocChecklistLoop['dcl_activeStatus'],
                                'dcl_priority'      => $getCopyDocChecklistLoop['dcl_priority'],
                                'dcl_uplStatus'     => $getCopyDocChecklistLoop['dcl_uplStatus'],
                                'dcl_uplType'       => $getCopyDocChecklistLoop['dcl_uplType'],
                                'dcl_finance'       => $getCopyDocChecklistLoop['dcl_finance'],
                                'dcl_mandatory'     => $getCopyDocChecklistLoop['dcl_mandatory'],
                                'dcl_addDate'       => date('Y-m-d H:i:s'),
                                'dcl_addBy'         => $getUserIdentity->id
                            );
                            $this->dclModel->insert($copyData);
                            }
                        }
                    } else {

                        $this->_helper->flashMessenger->addMessage(array('error' => "Document checklist already exist"));
                        $this->_redirect($this->baseUrl . '/application/admission-checklist/index/');
                    }


                }


                //unset($this->gobjsessionsis->admissionchecklistpaginatorresult);
                $searchForm = new Application_Form_ChecklistSearch();
                //$dclList = $this->dclModel->getDclList();
                $dclList = $this->dclModel->getAllConfig();
                $this->_helper->flashMessenger->addMessage(array('success' => "Copy success"));
                $this->_redirect($this->baseUrl . '/application/admission-checklist/index/');
            } else {

                //unset($this->gobjsessionsis->admissionchecklistpaginatorresult);
                $searchForm = new Application_Form_ChecklistSearch();
                //$dclList = $this->dclModel->getDclList();
                $dclList = $this->dclModel->getAllConfig();


                $this->_redirect($this->baseUrl . '/application/admission-checklist/index/');
            }
        } else {

            unset($this->gobjsessionsis->admissionchecklistpaginatorresult);
            $searchForm = new Application_Form_ChecklistSearch();
            //$dclList = $this->dclModel->getDclList();
            $dclList = $this->dclModel->getAllConfig();


        }

        if (count($dclList) > 0) {

            $i = 0;
            foreach ($dclList as $dclLoop) {

                $dclList[$i]['awardName'] =  $dclLoop['awardName'] . '<br/>';
                $dclList[$i]['mopName'] = $dclLoop['mopName'];
                $dclList[$i]['ptName'] = $dclLoop['ptName'];
                $dclList[$i]['ProgramName'] = $dclLoop['ProgramName'];
                $dclList[$i]['studentCategory'] = $dclLoop['studentCategory'];

                $i++;
            }

        }

        if (!$this->_getParam('search')) {
            unset($this->gobjsessionsis->admissionchecklistpaginatorresult);
            //$cache->remove('admissionchecklistpaginatorresult');
        }

        $lintpagecount = $this->gintPageCount;// Definitiontype model
        //var_dump($this->gobjsessionsis->admissionchecklistpaginatorresult);
        $lintpage = $this->_getParam('page', 1); // Paginator instance

        $cached_results = $cache->load('admissionchecklistpaginatorresult');
        if ($cached_results != '') {
            $paginator = $this->lobjCommon->fnPagination($cached_results, $lintpage, $lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($dclList, $lintpage, $lintpagecount);
        }

        $paginator->setItemCountPerPage(25);

        $this->view->paginator = $paginator;
        $this->view->dclList = $dclList;
        $this->view->searchForm = $searchForm;
        $this->view->copyForm = $copyForm;
    }

    public function addMainchecklistAction()
    {

        $this->view->title = 'Add Admission Checklist';

        $getUserIdentity = $this->auth->getIdentity();
        $db = Zend_Db_Table::getDefaultAdapter();

        $this->view->form = new Application_Form_ApplicationConfigs();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $checkExist = $this->dclModel->getConfig($formData['IdScheme'], $formData['IdProgram'] ?? 0, $formData['IdProgramScheme'] ?? 0, $formData['category'] ?? 0);

            if (!$checkExist) {

                $data = array(
                    'IdScheme'        => $formData['IdScheme'],
                    'IdProgram'       => $formData['IdProgram'],
                    'IdProgramScheme' => $formData['IdProgramScheme'],
                    'category'        => $formData['category'],
                    'created_at'      => date('Y-m-d H:i:s'),
                    'created_by'      => $getUserIdentity->id
                );

                $db->insert('tbl_documentchecklist_main', $data);

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully added"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('error' => "Document checklist already exist"));
            }

            $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'admission-checklist', 'action' => 'index'), 'default', true));

        }
    }

    public function subChecklistAction()
    {


        $this->view->title = $this->view->translate("Admission Checklist");
        $message = $this->_getParam('message', 0);
        $this->view->message = $message;
        $getUserIdentity = $this->auth->getIdentity();

        $idscheme = $this->_getParam('idscheme', null);
        $idprogram = $this->_getParam('idprogram', null);
        $idprogscheme = $this->_getParam('idprogscheme', 0);
        $idstudtype = $this->_getParam('idstudtype', null);



        unset($this->gobjsessionsis->admissionchecklistpaginatorresult);
        $dclList = $this->dclModel->getSubDclList($idscheme,$idprogram,$idprogscheme,$idstudtype);

        $lintpagecount = $this->gintPageCount;// Definitiontype model
        //var_dump($this->gobjsessionsis->admissionchecklistpaginatorresult);
        $lintpage = $this->_getParam('page', 1); // Paginator instance

        /*$cached_results = $cache->load('admissionchecklistpaginatorresult');
        if ($cached_results != '') {
            $paginator = $this->lobjCommon->fnPagination($cached_results, $lintpage, $lintpagecount);
        } else {*/
            $paginator = $this->lobjCommon->fnPagination($dclList, $lintpage, $lintpagecount);
       // }

        $paginator->setItemCountPerPage(25);

        $this->view->paginator = $paginator;
        $this->view->dclList = $dclList;
    }

   /* public function subChecklistAction()
    {
        $this->view->title = $this->view->translate("Admission Checklist");
        $message = $this->_getParam('message', 0);
        $this->view->message = $message;
        $copyForm = new Application_Form_CopyAdmissionChecklist();
        $getUserIdentity = $this->auth->getIdentity();

        $cacheObj = new Cms_Cache();
        $cache = $cacheObj->get();

        $idprogram = $this->_getParam('idprogram', null);
        $idprogscheme = $this->_getParam('idprogscheme', null);
        $idstudtype = $this->_getParam('idstudtype', null);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['Search']) && $formData['Search'] == 'Search') {

                $formData['field1'] = $this->view->escape(strip_tags($formData['field1']));
                $formData['field3'] = $this->view->escape(strip_tags($formData['field3']));
                $formData['field2'] = $this->view->escape(strip_tags($formData['field2']));
                $formData['field4'] = $this->view->escape(strip_tags($formData['field4']));

                $searchForm = new Application_Form_ChecklistSearch(array('IdProgram' => $formData['field3']));
                $searchForm->populate($formData);
                $dclList = $this->dclModel->getSubDclSearchList($formData);

                if (count($dclList) > 0) {
                    $i = 0;
                    foreach ($dclList as $dclLoop) {
                        $mop = $this->defModel->getData($dclLoop['mode_of_program']);
                        $mod = $this->defModel->getData($dclLoop['mode_of_study']);
                        $pt = $this->defModel->getData($dclLoop['program_type']);
                        $sc = $this->defModel->getData($dclLoop['dcl_stdCtgy']);
                        $dt = $this->defModel->getData($dclLoop['dcl_uplType']);

                        $dclList[$i]['ProgramScheme'] = $pt['DefinitionDesc'] . '<br/>' . $mop['DefinitionDesc'] . '<br/>' . $mod['DefinitionDesc'];
                        $dclList[$i]['studentType'] = $sc['DefinitionDesc'];
                        $dclList[$i]['docType'] = $dt['DefinitionDesc'];

                        $i++;
                    }
                }

                $cache->save($dclList, 'admissionchecklistpaginatorresult');
                //$this->gobjsessionsis->admissionchecklistpaginatorresult = $dclList;

                $this->_redirect($this->baseUrl . '/application/admission-checklist/index/search/1');
            } else if (isset($formData['Copy']) && $formData['Copy'] == 'Copy') {

                if ($formData['programSchemeFrom'] == $formData['programSchemeTo'] && $formData['studentCategoryFrom'] == $formData['studentCategoryTo']) {
                    $this->_helper->flashMessenger->addMessage(array('error' => "Cannot copy, copy from and copy to cannot be the same"));
                    $this->_redirect($this->baseUrl . '/application/admission-checklist/index/');
                } else {
                    $getCopyDocChecklist = $this->dclModel->getDclCopy($formData['programSchemeFrom'], $formData['studentCategoryFrom']);

                    if (count($getCopyDocChecklist) > 0) {
                        foreach ($getCopyDocChecklist as $getCopyDocChecklistLoop) {
                            $copyData = array(
                                'dcl_stdCtgy'       => $formData['studentCategoryTo'],
                                'dcl_programScheme' => $formData['programSchemeTo'],
                                'dcl_sectionid'     => $getCopyDocChecklistLoop['dcl_sectionid'],
                                'dcl_name'          => $getCopyDocChecklistLoop['dcl_name'],
                                'dcl_malayName'     => $getCopyDocChecklistLoop['dcl_malayName'],
                                'dcl_desc'          => $getCopyDocChecklistLoop['dcl_desc'],
                                'dcl_type'          => $getCopyDocChecklistLoop['dcl_type'],
                                'dcl_activeStatus'  => $getCopyDocChecklistLoop['dcl_activeStatus'],
                                'dcl_priority'      => $getCopyDocChecklistLoop['dcl_priority'],
                                'dcl_uplStatus'     => $getCopyDocChecklistLoop['dcl_uplStatus'],
                                'dcl_uplType'       => $getCopyDocChecklistLoop['dcl_uplType'],
                                'dcl_finance'       => $getCopyDocChecklistLoop['dcl_finance'],
                                'dcl_mandatory'     => $getCopyDocChecklistLoop['dcl_mandatory'],
                                'dcl_addDate'       => date('Y-m-d H:i:s'),
                                'dcl_addBy'         => $getUserIdentity->id
                            );
                            $this->dclModel->insert($copyData);
                        }
                    }
                }

                //unset($this->gobjsessionsis->admissionchecklistpaginatorresult);
                $searchForm = new Application_Form_ChecklistSearch();
                $dclList = $this->dclModel->getSubDclList($idprogram,$idprogscheme,$idstudtype);
                $this->_helper->flashMessenger->addMessage(array('success' => "Copy success"));
                $this->_redirect($this->baseUrl . '/application/admission-checklist/index/');
            } else {
                //unset($this->gobjsessionsis->admissionchecklistpaginatorresult);
                $searchForm = new Application_Form_ChecklistSearch();
                $dclList = $this->dclModel->getSubDclList($idprogram,$idprogscheme,$idstudtype);
                $this->_redirect($this->baseUrl . '/application/admission-checklist/index/');
            }
        } else {
            //unset($this->gobjsessionsis->admissionchecklistpaginatorresult);
            $searchForm = new Application_Form_ChecklistSearch();
            $dclList = $this->dclModel->getSubDclList($idprogram,$idprogscheme,$idstudtype);
        }

        if (count($dclList) > 0) {
            $i = 0;
            foreach ($dclList as $dclLoop) {
                $mop = $this->defModel->getData($dclLoop['mode_of_program']);
                $mod = $this->defModel->getData($dclLoop['mode_of_study']);
                $pt = $this->defModel->getData($dclLoop['program_type']);
                $sc = $this->defModel->getData($dclLoop['dcl_stdCtgy']);
                $dt = $this->defModel->getData($dclLoop['dcl_uplType']);

                $dclList[$i]['ProgramScheme'] = $pt['DefinitionDesc'] . '<br/>' . $mop['DefinitionDesc'] . '<br/>' . $mod['DefinitionDesc'];
                $dclList[$i]['studentType'] = $sc['DefinitionDesc'];
                $dclList[$i]['docType'] = $dt['DefinitionDesc'];

                $i++;
            }
        }

        if (!$this->_getParam('search')) {
            //unset($this->gobjsessionsis->admissionchecklistpaginatorresult);
            $cache->remove('admissionchecklistpaginatorresult');
        }

        $lintpagecount = $this->gintPageCount;// Definitiontype model
        //var_dump($this->gobjsessionsis->admissionchecklistpaginatorresult);
        $lintpage = $this->_getParam('page', 1); // Paginator instance

        $cached_results = $cache->load('admissionchecklistpaginatorresult');
        if ($cached_results != '') {
            $paginator = $this->lobjCommon->fnPagination($cached_results, $lintpage, $lintpagecount);
        } else {
            $paginator = $this->lobjCommon->fnPagination($dclList, $lintpage, $lintpagecount);
        }

        $paginator->setItemCountPerPage(25);

        $this->view->paginator = $paginator;
        $this->view->dclList = $dclList;
        $this->view->searchForm = $searchForm;
        $this->view->copyForm = $copyForm;
    }*/

    public function addChecklistAction()
    {
        $this->view->title = $this->view->translate("Add Admission Checklist");

        $idscheme = $this->_getParam('idscheme', null);
        $idprogram = $this->_getParam('idprogram', null);
        $idprogscheme = $this->_getParam('idprogscheme', 0);
        $idstudtype = $this->_getParam('idstudtype', null);



        //$addChecklistForm = new Application_Form_AddChecklist(array('idprogram' => $idprogram), array('idstudtype' => $idstudtype));
        $addChecklistForm = new Application_Form_AddChecklist(array(
            'idprogram' => $idprogram,
            'idstudtype' => $idstudtype
        ));


        $this->view->idscheme = $idscheme;
        $this->view->idprogram = $idprogram;
        $this->view->idprogscheme = $idprogscheme;
        $this->view->idstudtype = $idstudtype;

       $info = $this->dclModel->getConfig($idscheme, $idprogram , $idprogscheme, $idstudtype);

        //dd($info);

        //$this->lobjEditQualification->populate($this->_DbObj->getData($id));

        /*$form = new Application_Form_AddChecklist();
        $form->populate($info);
        $this->view->form = $form;*/

        $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'admission-checklist', 'action' => 'sub-checklist', 'idscheme' => $idscheme, 'idprogram' => $idprogram, 'idstudtype' => $idstudtype, 'idprogscheme' => $idprogscheme), 'default', true);

        $getUserIdentity = $this->auth->getIdentity();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //dd($formData); exit;
            if ($addChecklistForm->isValid($formData)) {

                if (isset($formData['active']) && $formData['active'] == '1') {
                    $formData['active'] = '1';
                } else {
                    $formData['active'] = '0';
                }

                if (isset($formData['mandatory']) && $formData['mandatory'] == '1') {
                    $formData['mandatory'] = '1';
                } else {
                    $formData['mandatory'] = '0';
                }

                if (isset($formData['upload']) && $formData['upload'] == '1') {
                    $formData['upload'] = '1';
                } else {
                    $formData['upload'] = '0';
                }

                if (isset($formData['dcl_finance']) && $formData['dcl_finance'] == '1') {
                    $formData['dcl_finance'] = '1';
                } else {
                    $formData['dcl_finance'] = '0';
                }

                if (isset($formData['dcl_sectionid']) && $formData['dcl_sectionid'] == '') {
                    $formData['dcl_sectionid'] = 23;
                }

                $data = array(
                    'dcl_stdCtgy'       => $idstudtype,
                    'dcl_programScheme' => $idprogscheme,
                    'dcl_sectionid'     => $formData['dcl_sectionid'],
                    'dcl_name'          => $formData['dcl_name'],
                    'dcl_malayName'     => $formData['dcl_malayName'],
                    'dcl_priority'      => $formData['dcl_priority'],
                    'dcl_activeStatus'  => $formData['active'],
                    'dcl_mandatory'     => $formData['mandatory'],
                    'dcl_uplStatus'     => $formData['upload'],
                    'dcl_uplType'       => $formData['dcl_uplType'],
                    'dcl_finance'       => $formData['dcl_finance'],
                    'dcl_desc'          => $formData['dcl_desc'],
                    'dcl_addDate'       => date('Y-m-d H:i:s'),
                    'dcl_addBy'         => $getUserIdentity->id
                );
                //dd($data); exit;
                //$this->dclModel->insert($data);
                $this->dclModel->add($data);

                $this->_helper->flashMessenger->addMessage(array('success' => "New checklist succesfully added"));


                //redirect
                $this->_redirect($this->view->backURL);
            }
        }

        $this->view->addChecklistForm = $addChecklistForm;
    }

    public function editChecklistAction()
    {
        $dclId = $this->_getParam('id', null);
        $idscheme = $this->_getParam('idscheme', null);
        $idprogram = $this->_getParam('idprogram', null);
        $idprogscheme = $this->_getParam('idprogscheme', 0);
        $idstudtype = $this->_getParam('idstudtype', null);

        $this->view->idscheme = $idscheme;
        $this->view->idprogram = $idprogram;
        $this->view->idprogscheme = $idprogscheme;
        $this->view->idstudtype = $idstudtype;


        $this->view->title = $this->view->translate("Edit Admission Checklist");
        $dclList = $this->dclModel->getDclListById($dclId);
        $editChecklistForm = new Application_Form_AddChecklist(array('idprogram' => $dclList['IdProgram'], 'idstudtype' => $dclList['dcl_stdCtgy'], 'idprogscheme' => $dclList['dcl_programScheme']));

        $this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'admission-checklist', 'action' => 'sub-checklist', 'idprogram' => $dclList['IdProgram'], 'idstudtype' => $dclList['dcl_stdCtgy'], 'idprogscheme' => $dclList['dcl_programScheme']), 'default', true);

        $getUserIdentity = $this->auth->getIdentity();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($editChecklistForm->isValid($formData)) {

                if (isset($formData['active']) && $formData['active'] == '1') {
                    $formData['active'] = '1';
                } else {
                    $formData['active'] = '0';
                }

                if (isset($formData['mandatory']) && $formData['mandatory'] == '1') {
                    $formData['mandatory'] = '1';
                } else {
                    $formData['mandatory'] = '0';
                }

                if (isset($formData['upload']) && $formData['upload'] == '1') {
                    $formData['upload'] = '1';
                } else {
                    $formData['upload'] = '0';
                }

                if (isset($formData['dcl_finance']) && $formData['dcl_finance'] == '1') {
                    $formData['dcl_finance'] = '1';
                } else {
                    $formData['dcl_finance'] = '0';
                }

                if (isset($formData['dcl_sectionid']) && $formData['dcl_sectionid'] == '') {
                    $formData['dcl_sectionid'] = 23;
                }

                $data = array(
                    'dcl_stdCtgy'       => $dclList['dcl_stdCtgy'],
                    'dcl_programScheme' => $dclList['dcl_programScheme'],
                    'dcl_sectionid'     => $formData['dcl_sectionid'],
                    'dcl_name'          => $formData['dcl_name'],
                    'dcl_malayName'     => $formData['dcl_malayName'],
                    'dcl_priority'      => $formData['dcl_priority'],
                    'dcl_activeStatus'  => $formData['active'],
                    'dcl_mandatory'     => $formData['mandatory'],
                    'dcl_uplStatus'     => $formData['upload'],
                    'dcl_uplType'       => $formData['dcl_uplType'],
                    'dcl_finance'       => $formData['dcl_finance'],
                    'dcl_desc'          => $formData['dcl_desc'],
                    'dcl_modDate'       => date('Y-m-d H:i:s'),
                    'dcl_modBy'         => $getUserIdentity->id
                );
                //dd($data);
                //exit;
                $this->dclModel->updateData($data, $dclId);

                //redirect
                //$this->_redirect($this->view->backURL);
                $this->_helper->flashMessenger->addMessage(array('success' => "Information Updated"));

                /*$this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'admission-checklist', 'action' => 'edit-checklist', 'id' => $dclId), 'default', true));

                $dclListEdit = $this->dclModel->getDclListById($dclId);

                $editChecklistForm->populate($dclListEdit);*/

                $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'admission-checklist', 'action' => 'sub-checklist', 'idscheme' => $idscheme, 'idprogram' => $idprogram, 'idstudtype' => $idstudtype, 'idprogscheme' => $idprogscheme), 'default', true));

            } else {
                $editChecklistForm->populate($dclList);
            }
        } else {
            $editChecklistForm->populate($dclList);
        }

        $this->view->editChecklistForm = $editChecklistForm;
    }

    public function deleteChecklistAction()
    {
        $dclId = $this->_getParam('id', null);
        $idscheme = $this->_getParam('idscheme', null);
        $idprogram = $this->_getParam('idprogram', null);
        $idprogscheme = $this->_getParam('idprogscheme', 0);
        $idstudtype = $this->_getParam('idstudtype', null);

        $this->view->idscheme = $idscheme;
        $this->view->idprogram = $idprogram;
        $this->view->idprogscheme = $idprogscheme;
        $this->view->idstudtype = $idstudtype;

        $message = 0;

        if ($dclId != null) {
            $checkUsingChecklist = $this->dclModel->checkUsingChecklist($dclId);
            if (!$checkUsingChecklist) {
                $this->dclModel->deleteData($dclId);
            } else {
                $message = 1;
            }
        }
        //$this->view->backURL = $this->view->url(array('module' => 'application', 'controller' => 'admission-checklist', 'action' => 'index', 'message' => $message), 'default', true);

        //redirect
        //$this->_redirect($this->view->backURL);

        $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'admission-checklist', 'action' => 'sub-checklist', 'idscheme' => $idscheme, 'idprogram' => $idprogram, 'idstudtype' => $idstudtype, 'idprogscheme' => $idprogscheme), 'default', true));
    }

    public function docTaggingAjaxAction()
    {
        $item_id = $this->_getParam('id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $getItemInfo = $this->lobjAppItem->fnGetItemById($item_id);

        $json = Zend_Json::encode($getItemInfo);

        echo $json;
        exit();
    }

    public function docChecklistAjaxAction()
    {
        $section_id = $this->_getParam('section_id', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $getSectionInfo = $this->lobjAppConfig->fnGetSectionConfById($section_id);

        $getChecklist = $this->dclModel->getDclListByStudentCtgyProgScheme($getSectionInfo['category'], 1, $getSectionInfo['IdProgramScheme'], $getSectionInfo['sectionID']);

        $json = Zend_Json::encode($getChecklist);

        echo $json;
        exit();
    }

    public function sectionTaggingAjaxAction()
    {

        $program_id = $this->_getParam('id', null);
        $stdCtgy = $this->_getParam('stdCtgy', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $sectionList = $this->lobjAppConfig->getSectionAjaxChecklist($program_id, $stdCtgy);

        $json = Zend_Json::encode($sectionList);

        echo $json;
        exit();
    }

    public function progschemeTagAjaxAction(){

        $program_id = $this->_getParam('id',null);


       /* if ($program_id == ''){
            $program_id = 0;
        }*/

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();


        $progModel = new Application_Model_DbTable_ProgramScheme();
        $progSchemeList = $progModel->fnGetProgramschemeListBasedOnProgId($program_id);

        $i = 0;

        foreach($progSchemeList as $progSchemeListloop){

            $progSchemeList[$i]['awardName'] = $progSchemeListloop['awardName'];
            $progSchemeList[$i]['mopName'] = $progSchemeListloop['mopName'];
            $progSchemeList[$i]['ptName'] = $progSchemeListloop['ptName'];

            $i++;
        }

        $json = Zend_Json::encode($progSchemeList);

        echo $json;
        exit();
    }
}

?>


