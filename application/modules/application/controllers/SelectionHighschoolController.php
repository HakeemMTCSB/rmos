<?php
class Application_SelectionHighschoolController extends Base_Base 
{

	public function indexAction(){
	
		$this->view->title = $this->view->translate("application_summary");
				
		//get academic year
		$academicDB = new App_Model_Record_DbTable_AcademicYear();
		$academic_year = $academicDB->getData();		
    	$this->view->academic_year = $academic_year;
    	
    	//get academic period
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period = $periodDB->getData();
    	$this->view->period = $period;
    	
    	//get program
    	$programDB = new App_Model_Record_DbTable_Program();
    	$program = $programDB->getData();
    	$this->view->program = $program;
    	
    	if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			$this->view->ac_year = $formData["academic_year"];
			
			$condition=array("IdProgram"=>$formData["programme"]);
			$program_data = $programDB->searchProgram($condition);		
			
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($program_data));
			$paginator->setItemCountPerPage(100);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
    	}else{
    		
    		$program_data = $programDB->searchPaginateProgram();	
    	
    		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($program_data));
			$paginator->setItemCountPerPage(100);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
    	}
    	
			$this->view->paginator = $paginator;
	} 
	
	
	protected function mailmerge($filepath,$fieldValues,$output_directory_path,$output_filename,$photoFilename=''){
		
		    //create PDF File  
/*			print_r($fieldValues);
			echo $photoFilename."<br>";
			echo $fieldValues["image:photo"];
			exit; 		
    		*/
			$mailMerge = new Zend_Service_LiveDocx_MailMerge();
									
			$mailMerge->setUsername('yatie')
			          ->setPassword('al_hasib');	
			           
			 
			          
			$mailMerge->setLocalTemplate($filepath);
		   
			
			$mailMerge->assign('connection', $fieldValues);
			$mailMerge->createDocument();
			 
			$document = $mailMerge->retrieveDocument('pdf');
						
			//create directory to locate file			
			if (!is_dir($output_directory_path)) {
		    	mkdir($output_directory_path, 0775, true);
			}

			//to rename output file			
			$output_file_path = $output_directory_path."/".$output_filename;
			
			file_put_contents($output_file_path, $document);
			if($photoFilename){
				$mailMerge->deleteImage($output_directory_path."/".$photoFilename);
			}
	}
	
	
	
	
	
	
	public function applicantListAction(){
		
		$this->view->title = $this->view->translate("applicant_list");
		
		$faculty_id = $this->_getParam('faculty_id', 0);
    	$this->view->faculty_id = $faculty_id;
    	
		$program_code = $this->_getParam('program_code', 0);
    	$this->view->program_code = $program_code;
    	
    	$academic_year = $this->_getParam('academic_year', 0);
    	$this->view->ayear = $academic_year;
    	
    	$period = $this->_getParam('period', 0);
    	$this->view->period = $period;
    	
    	
    	
		
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			$condition=array('admission_type'=>2,"academic_year"=>$formData["academic_year"],"program_code"=>$formData["programme"],'status'=>'PROCESS');
		
			$applicant_data = $applicantDB->getDatabyProgram($condition);		
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($applicant_data));
			$paginator->setItemCountPerPage(100);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
		}else{
			
			$condition=array('admission_type'=>2,'status'=>'PROCESS');
			$applicant_data = $applicantDB->getPaginateDatabyProgram($condition);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($applicant_data));
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
		}
		
		$this->view->paginator = $paginator;
	}
	
	public function documentAction(){
		
		$this->view->title = $this->view->translate("document");
		
		$program_code = $this->_getParam('program_code', 0);
    	$this->view->program_code = $program_code;
		
		$transaction_id = $this->_getParam('id', 0);
    	$this->view->transaction_id = $transaction_id;
		
		//get applicant Document/Form
    	$documentDB = new App_Model_Application_DbTable_ApplicantDocument();
    	  		
    		    		
    	$document = $documentDB->getData($transaction_id,32); //validasi bank/hs output
    	echo $directory = "http://".APP_HOSTNAME."/documents/".$document["ad_filepath"]."/".$document["ad_filename"];  
    	$this->view->download_file=$directory;
    		
	}
	
	public function processingAction(){
		
		$this->view->title = $this->view->translate("processing");
		
		$program_code = $this->_getParam('program_code', 0);
    	$this->view->program_code = $program_code;
    	
    	$academic_year = $this->_getParam('academic_year', 0);
    	$this->view->ayear = $academic_year;
		
		$transaction_id = $this->_getParam('id', 0);
    	$this->view->transaction_id = $transaction_id;
		
		
    	//get applicant program info
    	$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
    	$condition=array('admission_type'=>2,'program_code'=>$program_code,'transaction_id'=>$transaction_id);
		$applicant = $applicantDB->getDatabyProgram($condition);
    	$this->view->applicant= $applicant;
    	
    	//print_r($applicant);
    	
    	//get education average mark
		$educationDB = new App_Model_Application_DbTable_ApplicantEducation();
		$everage_mark = $educationDB->getAverageMark($applicant["appl_id"]);
		$this->view->everage_mark= $everage_mark;   	
    	
    	//get transaction info
    	$transDB = new App_Model_Application_DbTable_ApplicantTransaction();
    	$transaction_info = $transDB->getTransactionData($transaction_id);
    	$this->view->transaction_status = $transaction_info["at_status"];
    	
    	if($transaction_info["at_status"]=="OFFER" || $transaction_info["at_status"]=="REJECT"){
    		
    		$this->view->noticeMessage=$this->view->translate("this_application_has_been_processed");
    		
    		$ratingDB = new App_Model_Application_DbTable_ApplicantAssessment();
    		$data= $ratingDB->getData($transaction_id);
    		$this->view->rating = $data;
    		
    		$form = new Application_Form_Processing();    		
    		
    		if($transaction_info["at_status"]=='OFFER') $status = 1;
			if($transaction_info["at_status"]=='REJECT') $status = 2;
						
			$formData["aar_rating_dean"]=$data["aar_rating_dean"];
			$formData["aar_dean_status"]=$data["aar_dean_status"];							
			$formData["aar_rating_rector"]=$data["aar_rating_rector"];
			$formData["aar_rector_status"]=$data["aar_rector_status"];
			$formData["application_status"]=$status;				
			$formData["remarks"]=$data["aar_remarks"];	
			
			$form->populate($formData);
			$this->view->form = $form;
			
    	}else
    	
    	if($transaction_info["at_status"]=="PROCESS")
    	{    		
    		$form = new Application_Form_Processing();			
			$this->view->form = $form;
					
		
			if ($this->getRequest()->isPost()) {
				$formData = $this->getRequest()->getPost();
				
				//---------update transaction status----------
				$transDB = new App_Model_Application_DbTable_ApplicantTransaction();
				
			    $status='';
				if($formData["application_status"]==1) $status = 'OFFER';
				if($formData["application_status"]==2) $status = 'REJECT';
				//if($formData["application_status"]==3) $status = 'INCOMPLETE';
				
				$infoUpd["at_status"]=$status;
				$infoUpd["at_selection_status"]=3;
				$transDB->updateData($infoUpd,$transaction_id);
				
				
					//----------add rating info----------
					$ratingDB = new App_Model_Application_DbTable_ApplicantAssessment();
					$auth = Zend_Auth::getInstance(); 
    							
					$data["aar_trans_id"]=$transaction_id;				
					$data["aar_rating_dean"]=$formData["aar_rating_dean"];
					$data["aar_dean_status"]=$formData["aar_dean_status"];							
					$data["aar_rating_rector"]=$formData["aar_rating_rector"];
					$data["aar_rector_status"]=$formData["aar_rector_status"];				
					$data["aar_remarks"]=$formData["remarks"];	
					$data["aar_approvalby"]=$auth->getIdentity()->id;
					$data["aar_approvaldt"]=date("Y-m-d H:m:s");		
					
					$ratingDB->addData($data);
				
					
													
					//generate and send offer letter to applicant
					/*if($formData["application_status"]==1){ //offer						
						$this->sendMail($transaction_id);										
					}*/
				
				
				$this->_redirect($this->view->url(array('module'=>'application','controller'=>'selection-highschool', 'action'=>'processing','id'=>$transaction_id,'program_code'=>$program_code),'default',true));
			}//end post
    		
    	}
    	
    	
	}
	
	
	
	
	public function offerLetter($transaction_id){
		
		
		//get applicant info
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
    	$applicant = $applicantDB->getAllProfile($transaction_id);
    	
    	//getapplicantprogram
		$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();		
		$program = $appProgramDB->getProgramFaculty($transaction_id);
		
		//get applicant parents info
    	$familyDB =  new App_Model_Application_DbTable_ApplicantFamily();
    	$father = $familyDB->fetchdata($applicant["appl_id"],20); //father's    	
    	
		
		//$nomor = '010/AK.4.02/PSSB-BAA/Usakti/WR.I/I-3/2012';
		$nomor="";
    	
			$fieldValues = array(
					'applicantID'=>$applicant["applicantID"],
			        'NOMOR'=>$nomor,
			        'Title_Template'=>$this->view->translate("title_template_offer_letter"),
			        'APPLICANTNAME'=>$applicant["appl_fname"].' '.$applicant["appl_mname"].' '.$applicant["appl_lname"],
					'PARENTNAME'=>$father["af_name"],
			        'Address1'=>$applicant["appl_address1"],
					'Address2'=>$applicant["appl_address2"],
					'City'=>$applicant["CityName"],
					'Postcode'=>$applicant["appl_postcode"],
					'State'=>$applicant["StateName"],				
			    	'semester_offer'=>'',
					'period_offer'=>'',
					'FACULTYNAME'=>$program[0]["faculty"],
					'PROGRAMME'=>$program[0]["program_name"],
			        'print_date'=>date('j M Y'),
					'payment_date_paketa'=>'',
					'payment_date_paketb'=>'',
					'payment_date_paketb_cicilan1'=>'',
					'payment_date_paketb_cicilan2'=>'',
					'payment_date_paketb_cicilan3'=>'',
					'payment_date_paketb_cicilan4'=>'',
					'payment_date_paketb_cicilan5'=>'',
					'payment_date_paketb_cicilan6'=>''			        
			);
		
		 	//template file path
			//$filepath = DOCUMENT_PATH."/template/offer_letter.docx"; 
						
			//create directory to locate file
			//$output_directory_path = "/data/apps/triapp/documents/".$transaction_id;
			//if (!is_dir($output_directory_path)) {
		    //	mkdir($output_directory_path, 0775);
			//}
									
			//output filename 
			//$output_filename = $applicant["applicantID"]."_offer_letter.pdf";

			//to rename output file			
			//$output_file_path = $output_directory_path."/".$output_filename;		   

			//no need to create here create on offer letter
			//$this->mailmergeAction($filepath,$fieldValues,$output_file_path);
			
			//save file info
			//$documentDB = new App_Model_Application_DbTable_ApplicantDocument();
			//$doc["ad_appl_id"]=$applicant["appl_id"];
			//$doc["ad_type"]=43; //offer letter
			//$doc["ad_filepath"]=$output_directory_path;
			//$doc["ad_filename"]=$output_filename;
			//$doc["ad_createddt"]=date("Y-m-d");
			//$documentDB->addData($doc);		
		
			
			
			
			//-------------------send mail section------------------
			$fullname = $applicant["appl_fname"].' '.$applicant["appl_mname"].' '.$applicant["appl_lname"];
			//$attachment_path = $output_directory_path.'/'.$output_filename;
			
			$templateDB = new App_Model_General_DbTable_EmailTemplate();
			$templateData = $templateDB->getData(5,$applicant["appl_prefer_lang"]);//offer letter
    		    				
    		$templateMail = $templateData['body'];				
			$templateMail = str_replace("[applicant_name]",$fullname,$templateMail);
			
			$emailDb = new App_Model_System_DbTable_Email();		
			$data = array(
				'recepient_email' => $applicant["appl_email"],
				'subject' => $templateData["subject"],
				'content' => $templateMail
				//'attachment_path' => $attachment_path,
			    //'attachment_filename' => $output_filename
			);	
			
			//to send email with attachment
			$emailDb->addData($data);		
		
	}
	
	public function mailmergeAction($filepath,$fieldValues,$output_file_path){
			
		    // create and retrieve document	
			$mailMerge = new Zend_Service_LiveDocx_MailMerge();
			 
			$mailMerge->setUsername('yatie')
			          ->setPassword('al_hasib');			
			  
			$mailMerge->setLocalTemplate($filepath);
			
			$mailMerge->setFieldValues($fieldValues);
			
			$mailMerge->createDocument();
			 
			$document = $mailMerge->retrieveDocument('pdf');			 
			
			file_put_contents($output_file_path, $document);
	}
	
	
	public function batchDeanRatingAction(){
		
		$this->view->title = $this->view->translate("PSSB Selection")." - ".$this->view->translate("Batch Dean Rating");
			
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale=$locale;
		   
		$faculty = $this->_getParam('faculty',null);				
    	$this->view->faculty_id = $faculty;
    	
		$program_code = $this->_getParam('programme', null);
    	$this->view->program_code = $program_code;
    	
    	$intake_id = $this->_getParam('intake_id', null);
    	$this->view->intake_id = $intake_id;
    	
    	$period_id = $this->_getParam('period_id', null);
    	$this->view->period_id = $period_id;

    	$load_previous_period = $this->_getParam('load_previous_period', 0);
    	$this->view->load_previous_period = $load_previous_period;
    	
    	$form = new Application_Form_SelectionSearch(array('facultyid'=>$faculty));
    	$this->view->form=$form;  
    	
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		
		if ($this->getRequest()->isPost()) {
			if ($form->isValid ( $_POST )) {
				$formData = $this->getRequest()->getPost();
			
				$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
				
				$applicant_data = 	$transactionDb->getSelectionPSSBTransaction($formData["intake_id"], $formData["period_id"],'PROCESS',$formData["faculty"],$formData["programme"],$formData["load_previous_period"]);
				
				
				echo "<pre>";
				echo "</pre>";
				$this->view->paginator = $applicant_data;			
				
				$form->populate($formData);
				$this->view->form = $form;
			
			}else{
				$this->view->noticeError = $this->view->translate("Please Select all the required values");
			}
			
		}
		
		
		
	}
	
	public function ajaxGetPeriodAction(){
    	$intake_id = $this->_getParam('intake_id', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select(array('ap_id','ap_desc'))
	                 ->from(array('ap'=>'tbl_academic_period'))
	                 ->order('ap.ap_year')
	                 ->order('ap.ap_number');
	    
	    if($intake_id!=0){
	    	$select->where('ap.ap_intake_id = ?', $intake_id);
	    }
		
	    
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($row);
		
		echo $json;
		exit();
    }
    
	public function saveDeanRatingAction(){
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		setlocale (LC_ALL, $locale);
		
		$auth = Zend_Auth::getInstance(); 
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$periodDB = new App_Model_Record_DbTable_AcademicPeriod();	
			
			$selection_period = strftime('%B', mktime(null, null, null, date("m"), 01)).' '.date("Y");
						
			$selection["asd_type"]=1;//Dean
			$selection["asd_nomor"]=$formData["nomor"];
			$selection["asd_decree_date"]=$formData["decree_date"];
			$selection["asd_faculty_id"]=$formData["faculty_id"];
			$selection["asd_selection_period"]=$selection_period;		
			$selection["asd_createdby"]=$auth->getIdentity()->id;
			$selection["asd_createddt"]=date("Y-m-d H:m:s");	
			
			$selectionDB = new App_Model_Application_DbTable_ApplicantSelectionDetl();			
			$selection_id = $selectionDB->addData($selection);
			
			$ratingDB = new App_Model_Application_DbTable_ApplicantAssessment();
			
			for($i=0; $i<count($formData["dean_status"]); $i++){				
				
				
				//update selection status
				//jika dean status==3 (pending) indicate masih dalam process
				if($formData["dean_status"][$i]==1 || $formData["dean_status"][$i]==2 || $formData["dean_status"][$i]==5){
					
					
					$status["at_selection_status"]=1;
					$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
					$transactionDB->updateData($status,$formData["transaction_id"][$i]);
					
					//cek dah ada ke belum transaction id kalo dah ada edit/update 
					$data_exist = $ratingDB->getInfo($formData["transaction_id"][$i]);
					
					//add selection information
					$rating["aar_trans_id"]=$formData["transaction_id"][$i];
					$rating["aar_dean_status"]=$formData["dean_status"][$i];	
					$rating["aar_rating_dean"]=isset($formData["dean_rating"][$i])?$formData["dean_rating"][$i]:0;
					$rating["aar_dean_selectionid"]=$selection_id;								
					$rating["aar_dean_rateby"]=$auth->getIdentity()->id;
					$rating["aar_dean_ratedt"]=date("Y-m-d H:m:s");	
							
					if($data_exist){
						$ratingDB->updateAssessmentData($rating,$formData["transaction_id"][$i]);
					}else{
						$ratingDB->addData($rating);
					}
				
				}				
												
			}//end for
			
			
			$this->view->noticeSuccess = $this->view->translate("information_saved_successfully");
			//should notify rector by email to rate or not?
			
			//$this->_redirect($this->view->url(array('module'=>'application','controller'=>'selection-highschool', 'action'=>'batch-dean-rating','program_code'=>$program_code,'faculty_id'=>$faculty_id),'default',true));
			
		}//end if
	}
	
	
	public function batchRectorRatingAction(){
		
		$this->view->title = $this->view->translate("PSSB Selection")." - ".$this->view->translate("Batch Rector Rating");
		
		$faculty = $this->_getParam('faculty',null);			
    	$this->view->faculty_id = $faculty;
    	
		$program_code = $this->_getParam('programme', null);
    	$this->view->program_code = $program_code;
    	
    	$intake_id = $this->_getParam('intake_id', null);
    	$this->view->intake_id = $intake_id;
    	
    	$period_id = $this->_getParam('period_id', null);
    	$this->view->period_id = $period_id;
    	
    	$load_previous_period = $this->_getParam('load_previous_period', 0);
    	$this->view->load_previous_period = $load_previous_period;
    	
    	$form = new Application_Form_SelectionSearch(array('facultyid'=>$faculty));
    	$this->view->form=$form;  
    	
    	//academic year for save
    	$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
    	$academicYearList = $academicYearDb->getAcademicYear(array('NEXT','FUTURE'));
    	$this->view->academicYearList = $academicYearList;
    	
    	//locked assessment date
    	$assessmentDateDB = new Application_Model_DbTable_AssessmentDate();
    	$lockDate = $assessmentDateDB->getLockData();
    	$l_date = array();
    	foreach ($lockDate as $date_l){
    		$l_date[] = date('n-j-Y', strtotime($date_l['date']));
    	}
    	
    	$this->view->localDate = $l_date;

		
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
			
			$applicant_data = $transactionDb->getRectorSelectionPSSBTransaction($formData["intake_id"], $formData["period_id"],'PROCESS',$formData["faculty"],$formData["programme"],$formData["load_previous_period"]);
			$this->view->paginator = $applicant_data;
			
			
			$form->populate($formData);
			$this->view->form = $form;
		}
		
		
	}
	
	public function printBatchDeanRatingAction(){
				
		//$this->_helper->layout->disableLayout();
		  $this->_helper->layout->setLayout('print');
		  
	    $registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale=$locale;
		   
		$this->view->title = $this->view->translate("batch_dean_rating");
    	
		//$form = new Application_Form_HighSchoolSelectionSearch();		
		//$this->view->form = $form;	
			
		$faculty_id = $this->_getParam('faculty_id', null); 
		   	
		$program_code  = $this->_getParam('program_code', null);
		$this->view->program_code = $program_code;
		
		$intake_id = $this->_getParam('intake_id', null);
		
		$period_id = $this->_getParam('period_id', null);
		
		$load_previous_period = $this->_getParam('load_previous_period', 0);
    	   	
		if($faculty_id!=null){
			//get faculty name
			$facultyDB = new App_Model_General_DbTable_Collegemaster();
			$faculty = $facultyDB->getData($faculty_id);
			
			if($locale=="id_ID"){
				$this->view->faculty_name = $faculty["ArabicName"];
			}else{
				$this->view->faculty_name = $faculty["CollegeName"];
			}
			
			
			//get dean name
			$deanDB = new App_Model_General_DbTable_DeanList();
			$dean = $deanDB->getDeanByCollege($faculty_id);
			
			//get salutation
			$definationDB =  new App_Model_General_DbTable_Definationms();
			$defination1=$definationDB->getData($dean["FrontSalutation"]);			
			$defination2=$definationDB->getData($dean["BackSalutation"]);
			
			$this->view->dean_name = $defination1["DefinitionCode"].' '.$dean["FullName"].' '.$defination2["DefinitionCode"];
		}
		
		//get academic year
    	$academicDB = new App_Model_Record_DbTable_AcademicYear();
    	$academic_year_data = $academicDB->getNextAcademicYearData();
		$academic_year_info=	$academic_year_data["ay_code"];	
    	$this->view->current_academic_year = $academic_year_info;
		
		
		
		//get periode
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period = $periodDB->getData($period_id);
    	$this->view->period = $period;
		
    	//to get list applicant    	
		/*$condition=array('admission_type'=>2,"faculty_id"=>$faculty_id,'program_code'=>$program_code,'academic_year'=>$academic_year,'status'=>'PROCESS','period'=>$period["ap_id"]);
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		$applicant_data = $applicantDB->getDeanSelection($condition);*/
		
		$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
		//$applicant_data = 	$transactionDb->getSelectionPSSBTransaction($intake_id, $period_id,'PROCESS',$faculty_id,$program_code);
		$applicant_data = 	$transactionDb->getSelectionPSSBTransaction($intake_id, $period_id,'PROCESS',$faculty_id,$program_code,$load_previous_period);
				
		$this->view->applicant = $applicant_data;
	
		
		//get program name
		/*$programDB = new App_Model_Record_DbTable_Program();
		$program_data = $programDB->getProgrambyCode($program_code);
		
			if($locale=="id_ID"){
				$program_name = $program_data["ArabicName"];
			}elseif($locale=="en_US"){
				$program_name = $program_data["ProgramName"];
			}
			
		$this->view->program_name = $program_name;*/
    	
	}
	
	public function downloadBatchDeanRatingAction(){
		
		$faculty_id 	= $this->_getParam('faculty_id', null);    	
		$program_code  	= $this->_getParam('program_code', null);	
		$intake_id		= $this->_getParam('intake_id', null);
		$period_id      = $this->_getParam('period_id', null);
		$load_previous_period = $this->_getParam('load_previous_period', 0);
				
		if($faculty_id==""){
			$faculty_id = null;
		}
		
		if($program_code==""){
			$program_code = null;	
		}
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale=$locale;
		
    	//to get list applicant    	
		/*$condition=array('admission_type'=>2,"faculty_id"=>$faculty_id,'program_code'=>$program_code,'academic_year'=>$academic_year,'status'=>'PROCESS','period'=>$period);
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		$applicant_data = $applicantDB->getDeanSelection($condition);*/
		
		$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
		//$applicant_data = 	$transactionDb->getSelectionPSSBTransaction($intake_id, $period_id,'PROCESS',$faculty_id,$program_code);
		$applicant_data = 	$transactionDb->getSelectionPSSBTransaction($intake_id, $period_id,'PROCESS',$faculty_id,$program_code,$load_previous_period);
		
		$this->view->applicant = $applicant_data;
		
		
	}
	
	public function uploadBatchDeanRatingAction(){
		
		$this->view->title = $this->view->translate("upload_batch_dean_rating");
		
		$faculty_id = $this->_getParam('faculty_id', null); 
		$this->view->faculty_id = 	$faculty_id;
		   	
		$program_code  = $this->_getParam('program_code', null);
		$this->view->program_code = 	$program_code;
		
		$intake_id = $this->_getParam('intake_id', null);
		$this->view->intake_id = 	$intake_id;
		
		$period_id        = $this->_getParam('period_id', null);
    	$this->view->period_id = $period_id;
    	
		
		$form = new Application_Form_UploadBatch(array('facultyid'=>$faculty_id,'programcode'=>$program_code,'intakeid'=>$intake_id));
		$this->view->form = $form;
		
		$auth = Zend_Auth::getInstance(); 
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				
			 if ($form->isValid ( $_POST )) {			 	
	         	    if ($form->filename->isUploaded()) {	         	    	
	         	    
						$myfile = $form->getValues ();
						$locationFile = $form->filename->getFileName();
						
						$myfile ["filename"] = date ( 'Ymd' ).'_'.$myfile ["filename"];
												
						//echo $myfile ["filename"];
						$fullPathNameFile = DOCUMENT_PATH.'/download/'.date("mY").'/' .$myfile ["filename"];
						
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );
						
						   //no,transaction_id,no peserta,nama,program,mark,rank,status
						   
							$row = 1;
							if (($handle = fopen($fullPathNameFile, "r")) !== FALSE) {
													
								
					    		$selection["asd_type"]=1;//Dean
								$selection["asd_nomor"]=$formData["nomor"];
								$selection["asd_decree_date"]=$formData["decree_date"];
								$selection["asd_createdby"]=$auth->getIdentity()->id;
								$selection["asd_createddt"]=date("Y-m-d H:m:s");	
								
								$selectionDB = new App_Model_Application_DbTable_ApplicantSelectionDetl();			
								$selection_id = $selectionDB->addData($selection);
							    		
								
								
							    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
							        
							       
							    	if($row!=1){	
							    		
							            //add selection information
										$rating["aar_trans_id"]=$data[1];
										$rating["aar_dean_status"]=$data[7];	
										$rating["aar_rating_dean"]=$data[6];
										$rating["aar_dean_selectionid"]=$selection_id;								
										$rating["aar_dean_rateby"]=$auth->getIdentity()->id;
										$rating["aar_dean_ratedt"]=date("Y-m-d H:m:s");																			
										
										$ratingDB = new App_Model_Application_DbTable_ApplicantAssessment();
										$result=$ratingDB->getData($data[1]);
										
										//check if data exist
										if(!$result){	
																				
											$ratingDB->addData($rating);
											
											//update selection status
											//jika dean status==3 (pending) indicate masih dalam process
											if($data[7]==1 || $data[7]==2){
												
												$status["at_selection_status"]=1;										
												$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
												$transactionDB->updateData($status,$data[1]);
											}
										}//end if result
											
										
							    	}//end row
							       
							    $row++;    
							    }
							    fclose($handle);
							    
							    $this->view->form=null;
							    $this->view->noticeSuccess = $this->view->translate("information_saved_successfully");
								//should notify rector by email to rate or not?
							    
							}//end if
						
						
						
					} else {
						$myfile ["filename"] = "";
					}
	            }
            			
				
			
					
		}//end post

		
		
	}
	
	public function printNilaiRaportAction(){
				
		//$this->_helper->layout->disableLayout();
		$this->_helper->layout->setLayout('print');
	
		$faculty_id  = $this->_getParam('faculty_id', '');
		$program_code  = $this->_getParam('program_code', '');
		$academic_year = $this->_getParam('academic_year', '');
		$period        = $this->_getParam('period', '');
    	   	
		/*
		//get periode
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period = $periodDB->getData($period);
    	$this->view->period = $period;*/
    	
    	//to get list applicant    	
		$condition=array('admission_type'=>2,'faculty_id'=>$faculty_id,'program_code'=>$program_code,'academic_year'=>$academic_year,'status'=>'PROCESS','period'=>$period);
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		$applicant_data = $applicantDB->getDeanSelection($condition);				
		$this->view->applicant = $applicant_data;	
		
		
		//get faculty name
		if($faculty_id){
			$facultyDB =  new App_Model_General_DbTable_Collegemaster();
			$faculty_info = $facultyDB->getData($faculty_id);
			$this->view->faculty_name = $faculty_info["ArabicName"];
			$this->view->faculty_code = $faculty_info["CollegeCode"];
		}
		
		if($program_code){
			//get program name
			$programDB = new App_Model_Record_DbTable_Program();
			$program_data = $programDB->getProgrambyCode($program_code);
			$this->view->program_name = $program_data["ArabicName"];
			$this->view->program_code = $program_code;
		}
    
    	
	}
	
	
	public function printBatchRectorRatingAction(){
				
		//$this->_helper->layout->disableLayout();
		$this->_helper->layout->setLayout('print');
		 
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale=$locale;
			
		$faculty_id		= $this->_getParam('faculty_id', null);
		$program_code	= $this->_getParam('program_code', null);
		$intake_id		= $this->_getParam('intake_id', null);
		$period_id		= $this->_getParam('period_id', null);
		$load_previous_period = $this->_getParam('load_previous_period', 0);
    	   	
		//get periode
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period = $periodDB->getData($period_id);
    	$this->view->period = $period;
    	
    	if($faculty_id!=null){
			//get faculty name
			$facultyDB = new App_Model_General_DbTable_Collegemaster();
			$faculty = $facultyDB->getData($faculty_id);
			
			if($locale=="id_ID"){
				$this->view->faculty_name = $faculty["ArabicName"];
			}else{
				$this->view->faculty_name = $faculty["CollegeName"];
			}
    	}
		
		$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
		//$applicant_data = 	$transactionDb->getRectorSelectionPSSBTransaction($intake_id, $period_id,'PROCESS',$faculty_id,$program_code);
		$applicant_data = $transactionDb->getRectorSelectionPSSBTransaction($intake_id, $period_id,'PROCESS',$faculty_id,$program_code,$load_previous_period);
				
		$this->view->applicant = $applicant_data;
	
		//get academic year
    	$academicDB = new App_Model_Record_DbTable_AcademicYear();
    	$academic_year_data = $academicDB->getNextAcademicYearData();
		$academic_year=	$academic_year_data["ay_code"];	
    	$this->view->current_academic_year = $academic_year;
    	
    	
    	
	}
	
	
	public function printRectorNilaiRaportAction(){
				
		//$this->_helper->layout->disableLayout();
		  $this->_helper->layout->setLayout('print');
	
		$faculty_id  = $this->_getParam('faculty_id', '');
		$program_code  = $this->_getParam('program_code', '');
		$academic_year = $this->_getParam('academic_year', '');
		$period        = $this->_getParam('period', '');
    	   	
		
		//get periode
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period = $periodDB->getData($period);
    	$this->view->period = $period;
    	
    	//to get list applicant    	
		$condition=array('admission_type'=>2,"faculty_id"=>$faculty_id,'program_code'=>$program_code,'academic_year'=>$academic_year,'status'=>'PROCESS','period'=>$period["ap_id"]);
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		$applicant_data = $applicantDB->getRectorSelection($condition);				
		$this->view->applicant = $applicant_data;	
		
		
		
		//get faculty name
		if($faculty_id){
			$facultyDB =  new App_Model_General_DbTable_Collegemaster();
			$faculty_info = $facultyDB->getData($faculty_id);
			$this->view->faculty_name = $faculty_info["ArabicName"];
			$this->view->faculty_code = $faculty_info["CollegeCode"];
		}
		
		if($program_code){
			//get program name
			$programDB = new App_Model_Record_DbTable_Program();
			$program_data = $programDB->getProgrambyCode($program_code);
			$this->view->program_name = $program_data["ArabicName"];
			$this->view->program_code = $program_code;
		}
    
    	
	}
	
	
	public function downloadBatchRectorRatingAction(){
		
		$faculty_id = $this->_getParam('faculty_id', null); 
		   	
		$program_code  = $this->_getParam('program_code', null);
		$this->view->program_code = $program_code;
		
		$intake_id = $this->_getParam('intake_id', null);
		
		$period_id  = $this->_getParam('period_id', null);
		$load_previous_period = $this->_getParam('load_previous_period', 0);
		
		//get periode
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period_data = $periodDB->getData($period_id);
    	$this->view->period = $period_data["ap_code"];
    	
    	//to get list applicant    	
		//$condition=array('admission_type'=>2,"faculty_id"=>$faculty_id,'program_code'=>$program_code,'academic_year'=>$academic_year,'status'=>'PROCESS','period'=>$period_data["ap_id"]);
		//$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		//$applicant_data = $applicantDB->getRectorSelection($condition);
		
    	$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
		//$applicant_data = 	$transactionDb->getRectorSelectionPSSBTransaction($intake_id, $period_id,'PROCESS',$faculty_id,$program_code);
		$applicant_data = $transactionDb->getRectorSelectionPSSBTransaction($intake_id, $period_id,'PROCESS',$faculty_id,$program_code,$load_previous_period);
		
		$this->view->applicant = $applicant_data;
		
	}
	
	
	public function uploadBatchRectorRatingAction(){
		
		$this->view->title = $this->view->translate("upload_batch_rector_rating");
				
		$faculty_id = $this->_getParam('faculty_id', null);
		$this->view->faculty_id = 	$faculty_id; 
		   	
		$program_code  = $this->_getParam('program_code', null);
		$this->view->program_code = 	$program_code;
		
		$intake_id = $this->_getParam('intake_id', null);
		$this->view->intake_id = 	$intake_id;
		
		$period_id  = $this->_getParam('period_id', null);
		$this->view->period_id = 	$period_id;
		
		$form = new Application_Form_UploadBatch();
		$this->view->form = $form;
		
		$auth = Zend_Auth::getInstance(); 
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				
			 if ($form->isValid ( $_POST )) {			 	
	         	    if ($form->filename->isUploaded()) {	         	    	
	         	    
						$myfile = $form->getValues ();
						$locationFile = $form->filename->getFileName();
						
						$myfile ["filename"] = date ( 'Ymd' ).'_'.$myfile ["filename"];
												
						//echo $myfile ["filename"];
						$fullPathNameFile = DOCUMENT_PATH.'/download/'.date("mY").'/' .$myfile ["filename"];
						
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );
						
						    //No,ID Transaksi,No Peserta,Nama,Program,Nilai,Dean Rank,Dean Status,WR Rank,WR Status
							$row = 1;
							if (($handle = fopen($fullPathNameFile, "r")) !== FALSE) {
								
								//add selection information
								$selection["asd_type"]=2;//Rector
								$selection["asd_nomor"]=$formData["nomor"];
								$selection["asd_decree_date"]=$formData["decree_date"];
								$selection["asd_createdby"]=$auth->getIdentity()->id;
								$selection["asd_createddt"]=date("Y-m-d H:m:s");	
								
								$selectionDB = new App_Model_Application_DbTable_ApplicantSelectionDetl();			
								$selection_id = $selectionDB->addData($selection);
								
										
							    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
							        	
							    	if($row!=1){							            							            
							    									    		
							    		$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
							    		$status = $transactionDB->getTransactionData($data[1]);

							    		if($status["at_selection_status"]==1){
										
							    			
							    			if($data[9]==1 || $data[9]==2){
							    				
							    				$rating["aar_rating_rector"]=$data[8];	
												$rating["aar_rector_status"]=$data[9];	
												$rating["aar_rector_selectionid"]=$selection_id;								
												$rating["aar_rector_rateby"]=$auth->getIdentity()->id;
												$rating["aar_rector_ratedt"]=date("Y-m-d H:m:s");
								    			
												$rating["aar_academic_year"]		 = $formData["academic_year"][$i];
												$rating["aar_reg_start_date"]		 = date("Y-m-d",strtotime($formData["registration_start_date"]));
												$rating["aar_reg_end_date"]			 = date("Y-m-d",strtotime($formData["registration_end_date"]));
												$rating["aar_payment_start_date"]	 = date("Y-m-d",strtotime($formData["first_payment_start_date"]));
												$rating["aar_payment_end_date"]		 = date("Y-m-d",strtotime($formData["first_payment_start_date"]));
												
												
												$ratingDB = new App_Model_Application_DbTable_ApplicantAssessment();
												$ratingDB->updateAssessmentData($rating,$data[1]);
												
												
												
									    		if($data[9]==1){ //ACCEPT
								
													$status["at_status"]="OFFER";
													$status["at_selection_status"]=3;//completed
													$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
													$transactionDB->updateData($status,$data[1]);	
													
													//$this->sendMail($data[1]);
													
										    		//create offer letter
													try{
														$this->generateOfferLetter($formData["transaction_id"][$i]);
														
														$this->view->noticeSuccess = $this->view->translate("saved_successfully");
														
													}catch (Exception $e){
														$this->view->noticeError = $this->view->translate("Data is save but unable to create Offer Letter. Offer letter will be generate when student download it");
													}
													
													//create performa invoice
													$proformaInvoiceDb = new Application_Model_DbTable_ProformaInvoice();
													if(!$proformaInvoiceDb->generateProformaInvoice($formData["transaction_id"][$i],date("Y-m-d",strtotime($formData["decree_date"]))) ){
														$errorMsg = $this->view->noticeError;
														$this->view->noticeError = $errorMsg." ".$this->view->translate("Proforma Invoice not created. Please inform Admin");
													}
													
													//send email
													//$this->sendMail($formData["transaction_id"][$i]);
													
												}elseif($data[9]==2){//reject
													
													$status["at_status"]="REJECT";
													$status["at_selection_status"]=3;//completed
													$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
													$transactionDB->updateData($status,$data[1]);
																		
												}
							    			}//endd if data[9]
							    			
							    			
							    													
												
							    		}//end status
										
							    	}
							       
							    $row++;    
							    }
							    fclose($handle);
							    
							}//end if
						
							$this->view->form=null;
							$this->view->noticeSuccess=$this->view->translate("document_has_been_successfully_uploaded");
						
					} else {
						$myfile ["filename"] = "";
					}
	            }
            
				
				
			
					
		}//end post

		
		
	}
	
	
	public function saveRectorRatingAction(){
	
		
		$auth = Zend_Auth::getInstance(); 
		
		$program_code  = $this->_getParam('program_code', 0);
		$faculty_id = $this->_getParam('faculty_id', 0);
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$selection["asd_type"]=2;//Rector
			$selection["asd_nomor"]=$formData["nomor"];
			$selection["asd_decree_date"]=date("Y-m-d",strtotime($formData["decree_date"]));
			$selection["asd_createdby"]=$auth->getIdentity()->id;
			$selection["asd_createddt"]=date("Y-m-d H:m:s");	
			
			$selectionDB = new App_Model_Application_DbTable_ApplicantSelectionDetl();
			$selection_id = $selectionDB->addData($selection);
			
						
			for($i=0; $i<count($formData["transaction_id"]); $i++){	
				
					
				/*if($formData["rector_verification"][$i]=="1"){
					//accept dean rate			
					$rating["aar_rating_verified"]='Y';
					$final_rate = $formData["dean_rating"][$i]; 
							
				}else {
					//reject dean rate	
					$rating["aar_rating_verified"]='N';  
					$rating["aar_rating_rector"]=$formData["rector_rating"][$i];
					$final_rate = $formData["rector_rating"][$i]; 
				}*/
							
				
				
				//update selection status (accept/reject)
				if($formData["rector_status"][$i]==1 || $formData["rector_status"][$i]==2 || $formData["rector_status"][$i]==4){
															
								
					$rating["aar_rating_rector"] = $formData["rector_rating"][$i];
					$rating["aar_rector_status"] = $formData["rector_status"][$i];
					$rating["aar_rector_selectionid"] = $selection_id;
					$rating["aar_rector_rateby"]=$auth->getIdentity()->id;
					$rating["aar_rector_ratedt"]=date("Y-m-d H:m:s");	
					
					if($formData["rector_status"][$i]==1){ //ACCEPT
						$rating["aar_academic_year"]		 = $formData["academic_year"][$i];
						$rating["aar_reg_start_date"]		 = date("Y-m-d",strtotime($formData["registration_start_date"]));
						$rating["aar_reg_end_date"]			 = date("Y-m-d",strtotime($formData["registration_end_date"]));
						$rating["aar_payment_start_date"]	 = date("Y-m-d",strtotime($formData["first_payment_start_date"]));
						$rating["aar_payment_end_date"]		 = date("Y-m-d",strtotime($formData["first_payment_start_date"]));
					}
					
					
					//update rector rating
					$ratingDB = new App_Model_Application_DbTable_ApplicantAssessment();
					$ratingDB->updateAssessmentData($rating,$formData["transaction_id"][$i]);
					
					
					if($formData["rector_status"][$i]==1){ //ACCEPT
						
						$status["at_status"]="OFFER";
						$status["at_selection_status"]=3;//completed
						$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
						$transactionDB->updateData($status,$formData["transaction_id"][$i]);	
						
						//create offer letter
						try{
							$this->generateOfferLetter($formData["transaction_id"][$i]);
							$this->view->noticeSuccess = $this->view->translate("saved_successfully");
							
						}catch (Exception $e){
							$this->view->noticeError = $this->view->translate("Data is save but unable to create Offer Letter. Please regenerate.");
						}
						
						//create performa invoice
						$proformaInvoiceDb = new Application_Model_DbTable_ProformaInvoice();
						if(!$proformaInvoiceDb->generateProformaInvoice($formData["transaction_id"][$i],date("Y-m-d",strtotime($formData["decree_date"]))) ){
							$errorMsg = $this->view->noticeError;
							$this->view->noticeError = $errorMsg." ".$this->view->translate("Proforma Invoice not created. Please inform Admin");
						}
						
						//send email
						//$this->sendMail($formData["transaction_id"][$i],"OFFER");
						
					}elseif($formData["rector_status"][$i]==2){//reject
						
						$status["at_status"]="REJECT";
						$status["at_selection_status"]=3;//completed
						$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
						$transactionDB->updateData($status,$formData["transaction_id"][$i]);
						
						//$this->sendMail($formData["transaction_id"][$i],"REJECT");
											
					}elseif($formData["rector_status"][$i]==4){//lulus tahap pertama
						
						$status["at_selection_status"]=5;//Lulus Tahap 1
						$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
						$transactionDB->updateData($status,$formData["transaction_id"][$i]);
						
						//update status offer program,
						$program["ap_usm_status"]=1;
						$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();
						$ap_data = $appProgramDB->getOfferProgram($formData["transaction_id"][$i]);
						
						$appProgramDB->updateData($program,$ap_data["ap_id"]);
						
						//$this->sendMail($formData["transaction_id"][$i],"REJECT");
											
					}
					
				}//end update selection status				
				
				
				$this->view->noticeSuccess = $this->view->translate("information_saved_successfully");
								
			}//end for
			
			//lock rector date
			if(isset($formData['lock_date']) && $formData['lock_date']==1 ){
				$auth = Zend_Auth::getInstance();
				
				$assessmentDateDb = new Application_Model_DbTable_AssessmentDate();
				
				$data = array(
					'date'=>date('Y-m-d', strtotime($formData['decree_date'])),
					'lock_status'=>1,
					'lock_by'=>$auth->getIdentity()->iduser,
					'lock_date'=>date ('Y-m-d h:i:s')
				);
				$assessmentDateDb->addData($data);
			}
			
			//$this->view->noticeSuccess = $this->view->translate("saved_successfully");
			//should notify rector by email to rate or not?
			
			
		}//end if
	}
	
	
	public function batchApprovalAction(){
		
		$this->view->title = $this->view->translate("Batch Approval");
				
		$faculty = $this->_getParam('faculty',null);
    	$this->view->faculty_id = $faculty;
		
		$program_code = $this->_getParam('programme', '');
    	$this->view->program_code = $program_code;
    	
    	$academic_year = $this->_getParam('academic_year', '');
    	$this->view->ayear = $academic_year;
    	
    	$period = $this->_getParam('period', '');
    	$this->view->period = $period;
    	
    	
    	
		$form = new Application_Form_SelectionSearch(array('facultyid'=>$faculty));
    	$this->view->form=$form;  
    	
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			$condition=array('admission_type'=>2,
							"academic_year"=>$formData["academic_year"],
							"period"=>$formData["period"],
			 				"faculty_id"=>$formData["faculty"],
							"program_code"=>$formData["programme"],
							'status'=>'PROCESS',
			 				);
						
			$applicant_data = $applicantDB->getApprovalSelection($condition);
					
			$this->view->paginator = $applicant_data;
			
			$form->populate($formData);
			$this->view->form = $form;
		}
	}
	
	
	public function saveApprovalAction(){
		
		
		$auth = Zend_Auth::getInstance(); 
		
		$program_code  = $this->_getParam('program_code', 0);
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
					
			$selection["asd_type"]=3;//Final
			$selection["asd_nomor"]=$formData["nomor"];
			$selection["asd_decree_date"]=$formData["decree_date"];
			$selection["asd_createdby"]=$auth->getIdentity()->id;
			$selection["asd_createddt"]=date("Y-m-d H:m:s");	
			
			$selectionDB = new App_Model_Application_DbTable_ApplicantSelectionDetl();			
			$selection_id = $selectionDB->addData($selection);
			
			for($i=0; $i<count($formData["transaction_id"]); $i++){					
							
				$rating["aar_final_selectionid"]=$selection_id;	
				$rating["aar_approvalby"]=$auth->getIdentity()->id;
				$rating["aar_approvaldt"]=date("Y-m-d H:m:s");				
				
				//update rector rating
				$ratingDB = new App_Model_Application_DbTable_ApplicantAssessment();
				$ratingDB->updateAssessmentData($rating,$formData["transaction_id"][$i]);
				
				
				//update selection status
				$status["at_status"]=$formData["approval"][$i];
				$status["at_selection_status"]=3;	//completed			
				$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
				$transactionDB->updateData($status,$formData["transaction_id"][$i]);

				/*if($formData["approval"][$i]=="OFFER"){					
					$this->sendMail($formData["transaction_id"][$i]);
				}//END SEND MAIL*/
				
			}//end for
			
			$this->view->noticeSuccess = $this->view->translate("saved_successfully");
			//should notify rector by email to rate or not?
			
			
		}//end if
	}
	
	
	public function printBatchApprovalAction(){
				
		//$this->_helper->layout->disableLayout();
		$this->_helper->layout->setLayout('print');
			
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
    	    	
		$faculty_id = $this->_getParam('faculty_id', '');  
		$this->view->faculty_id = $faculty_id;  
		
		$program_code = $this->_getParam('programme', '');
    	$this->view->program_code = $program_code;
    	
    	$academic_year = $this->_getParam('academic_year', '');
    	$this->view->ayear = $academic_year;
    	
    	$period = $this->_getParam('period', '');
    	$this->view->period = $period;
    	
    	//get periode
    	$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period_info = $periodDB->getData($period);    
    	$this->view->period_name = $period_info["ap_desc"];
    	
    	$condition2=array('admission_type'=>2,
							"academic_year"=>$academic_year,
							"period"=>$period,
    						"faculty_id"=>$faculty_id,
							"program_code"=>$program_code,
							'status'=>'PROCESS',
			 				);
    	
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		$applicant_data = $applicantDB->getApprovalSelection($condition2);	
		$this->view->applicant = $applicant_data;
		
		//get faculty name
		$facultyDB = new App_Model_General_DbTable_Collegemaster();
		$faculty = $facultyDB->getData($faculty_id);
		
		
		if($locale=="id_ID"){
			$this->view->faculty_name = $faculty["ArabicName"];
		}else{
			$this->view->faculty_name = $faculty["CollegeName"];
		}
		
		//get academic year
    	$academicDB = new App_Model_Record_DbTable_AcademicYear();
    	$academic_year_data = $academicDB->getCurrentAcademicYearData();
		$academic_year_info=	$academic_year_data["ay_code"];	
    	$this->view->current_academic_year = $academic_year_info;
		
    	
    	$activityDB = new App_Model_Record_DbTable_ActivityCalender();	
    	$registrasi = $activityDB->getNearestActivityDate(2,$period);//registrasi
    	$this->view->registrasi =$registrasi;
	}
	
	
	public function downloadBatchApprovalAction(){
		
		$faculty_id = $this->_getParam('faculty', ''); 
		$this->view->faculty_id = 	$faculty_id;
		   	
		$program_code  = $this->_getParam('programme', '');
		$this->view->program_code = 	$program_code;
		
		$academic_year = $this->_getParam('academic_year', '');
		$this->view->ayear = 	$academic_year;
		
		$period  = $this->_getParam('period','');
		$this->view->period = $period;
		
    	
    	//to get list applicant    	
		$condition=array('admission_type'=>2,"faculty_id"=>$faculty_id,'program_code'=>$program_code,'academic_year'=>$academic_year,'status'=>'PROCESS','period'=>$period);
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		$applicant_data = $applicantDB->getApprovalSelection($condition);
		
		$this->view->applicant = $applicant_data;
		
	}
	
	
	public function uploadBatchApprovalAction(){
		
		$this->view->title = $this->view->translate("upload_batch_approval");
		
		$faculty_id = $this->_getParam('faculty_id', ''); 
		$this->view->faculty_id = 	$faculty_id;
		   	
		$program_code  = $this->_getParam('program_code', '');
		$this->view->program_code = 	$program_code;
		
		$academic_year = $this->_getParam('academic_year', '');
		$this->view->ayear = 	$academic_year;
		
		$period        = $this->_getParam('period', '');
    	$this->view->period = $period;
    	
		
		$form = new Application_Form_UploadBatch(array ('facultyid' => $faculty_id,'programcode' => $program_code, 'academicyear' => $academic_year ) );
		$this->view->form = $form;
		
		$auth = Zend_Auth::getInstance(); 
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();		
          
			 	if ($form->isValid ( $_POST )) {
			 		
	         	    if ($form->filename->isUploaded ()) {
	         	    	
	         	    	$myfile = $form->getValues ();
						$locationFile = $form->filename->getFileName();
						
						$myfile ["filename"] = date ( 'Ymd' ).'_'.$myfile ["filename"];
												
						//echo $myfile ["filename"];
						$fullPathNameFile = DOCUMENT_PATH.'/download/'.date("mY").'/' .$myfile ["filename"];
						
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );
						
						
							$row = 1;
							if (($handle = fopen($fullPathNameFile, "r")) !== FALSE) {
								
								 //add selection information
					            $selection["asd_type"]=3;//Rector
								$selection["asd_nomor"]=$formData["nomor"];
								$selection["asd_decree_date"]=$formData["decree_date"];
								$selection["asd_createdby"]=$auth->getIdentity()->id;
								$selection["asd_createddt"]=date("Y-m-d H:m:s");	
								
								$selectionDB = new App_Model_Application_DbTable_ApplicantSelectionDetl();			
								$selection_id = $selectionDB->addData($selection);
										
										
							    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

							    	
							    	if($row!=1){							    		
							           
										
										$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
							    		$status = $transactionDB->getTransactionData($data[1]);

							    		if($status["at_selection_status"]==2){
										
							    				$rating["aar_final_selectionid"]=$selection_id;	
									    		$rating["aar_approvalby"]=$auth->getIdentity()->id;
												$rating["aar_approvaldt"]=date("Y-m-d H:m:s");				
												
												//update rector rating
												$ratingDB = new App_Model_Application_DbTable_ApplicantAssessment();
												$ratingDB->updateAssessmentData($rating,$data[1]);
												
												
												//update selection status
												if($data[8]==1){
														$status["at_status"]="OFFER";
												}elseif($data[8]==2){
													    $status["at_status"]="REJECT";
												}
												
												$status["at_selection_status"]=3;	//completed	
													
												$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
												$transactionDB->updateData($status,$data[1]);
												
												/*if($data[8]==1){
													//$transaction_id = $data[1]; //row number 2
													$this->sendMail($data[1]);
												}//END SEND MAIL*/
										
							    		}//end if status
							    		
							    	}//end if row
							       
							    $row++;    
							    }//end while
							    fclose($handle);
							    
							}//end if
							
							$this->view->form=null;
							$this->view->noticeSuccess=$this->view->translate("document_has_been_successfully_uploaded");
    		
						
					} else {
						$myfile ["filename"] = "";
					}
					
	            }//end post
									
		}//end post

		
		
	}
	
	
	public function sendMail($transaction_id,$status=null){
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		//get applicant info
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
    	$applicant = $applicantDB->getAllProfile($transaction_id);
    	
    	//getapplicantprogram
		$appProgramDB = new App_Model_Application_DbTable_ApplicantProgram();		
		$program = $appProgramDB->getProgramFaculty($transaction_id);
		
		if($locale=="id_ID"){
			$program_name = $program[0]["program_name_indonesia"];
		}elseif($locale=="en_US"){
			$program_name = $program[0]["program_name"];
		}
		
		//get applicant parents info
    	$familyDB =  new App_Model_Application_DbTable_ApplicantFamily();
    	$father = $familyDB->fetchdata($applicant["appl_id"],20); //father's    	
    	
    	//get assessment info
    	$assessmentDb = new App_Model_Application_DbTable_ApplicantAssessment();
    	$assessment = $assessmentDb->getInfo($transaction_id);
			 	
			$fullname = $applicant["appl_fname"].' '.$applicant["appl_mname"].' '.$applicant["appl_lname"];
			$url_student_portal = "http://".APP_HOSTNAME."/online-application";
			
			$templateDB = new App_Model_General_DbTable_EmailTemplate();
			
			if($status=="OFFER"){
				$templateData = $templateDB->getData(5,$applicant["appl_prefer_lang"]);//offer letter
			}elseif ($status=="REJECT"){
				$templateData = $templateDB->getData(7,$applicant["appl_prefer_lang"]);//reject application letter
			}
			
    		/*$templateMail = $templateData['body'];				
			$templateMail = str_replace("[applicant_name]",$fullname,$templateMail);
			$templateMail = str_replace("[program]",$program[0]["program_name"],$templateMail);
			$templateMail = str_replace("[faculty]",$program[0]["faculty"],$templateMail);
			$templateMail = str_replace("[url_student_portal]",$url_student_portal,$templateMail);*/
			
			$templateMail = $templateData['body'];				
			$templateMail = str_replace("[applicant_name]",$fullname,$templateMail);
			$templateMail = str_replace("[program]",$program_name,$templateMail);
			$templateMail = str_replace("[faculty]",$program[0]["faculty2"],$templateMail);
			$templateMail = str_replace("[url_student_portal]",$url_student_portal,$templateMail);
			$templateMail = str_replace("[assessment_type]",'Rapor',$templateMail);
			$templateMail = str_replace("[peringkat]",$assessment["aar_rating_rector"],$templateMail);
			
			$emailDb = new App_Model_System_DbTable_Email();		
			$data = array(
				'recepient_email' => $applicant["appl_email"],
				'subject' => $templateData["subject"],
				'content' => $templateMail,
			    'attachment_path'=>'',
			    'attachment_filename'=>''
			);	
			
			//to send email with attachment
			$emailDb->addData($data);		
		
	}
	
	
	public function ajaxGetProgramAction($faculty_id=0){
    	$faculty_id = $this->_getParam('faculty_id', 4);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select()
	                 ->from(array('p'=>'tbl_program'))
	                 ->where('p.IdCollege = ?', $faculty_id)
	                 ->order('p.ProgramName ASC');
	    
			
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();
        
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		
		$i=0;
		foreach ($row as $p){
			
			if($locale=="id_ID"){
				$program_name = $p["ArabicName"];
			}elseif($locale=="en_US"){
				$program_name = $p["ProgramName"];
			}
			
		$program[$i]["ProgramCode"]=$p["ProgramCode"];
		$program[$i]["ProgramName"]=$program_name;
		$i++;
		}
			
	    
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($program);
		
		echo $json;
		exit();
    }
    
    
    
	public function selectionStatusAction(){
		
		$this->view->title = $this->view->translate("selection_status");
		
		$session = new Zend_Session_Namespace('sis');
		
		if($session->IdRole == 298){ //FACULTY ADMIN			
			$faculty = $this->_getParam('faculty', $session->idCollege);
		}else{
			$faculty = $this->_getParam('faculty',null);
		}				
    	$this->view->faculty_id = $faculty;
    	
		$program_code = $this->_getParam('program_code', '');
    	$this->view->program_code = $program_code;
    	
    	$academic_year = $this->_getParam('academic_year',null);
    	$this->view->ayear = $academic_year;
    	
    	$period = $this->_getParam('period',null);
    	$this->view->period = $period;
    	
    	$selection_status = $this->_getParam('selection_status', '');
    	$this->view->selection_status = $selection_status;
    	
    	
    	$condition=array('admission_type'=>2,
						 "academic_year"=>$academic_year,
						 "period"=>$period,
						 "faculty"=>$faculty,
						 "program_code"=>$program_code,
						 "selection_status"=>$selection_status					
						 );		
								 
							 
    	  	
    	$form = new Application_Form_SelectionStatusSearch(array('facultyid'=>$faculty));
    	$this->view->form=$form;  
		
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
		
		if ($this->getRequest()->isPost()) {			
			
			$formData = $this->getRequest()->getPost();											 
			
			if ($form->isValid($_POST)) {
			
				//$applicant_data = $applicantDB->getStatusSelection($condition);
				
				$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
				$applicant_data = $transactionDb->getSelectionStatus($formData["intake_id"], $formData["period_id"],null,$formData["faculty"],$formData["programme"],$formData["selection_status"], $formData['load_previous_period'],2);
				
				$educationDB = new App_Model_Application_DbTable_ApplicantEducation();
				$assessmentDB=new App_Model_Application_DbTable_ApplicantAssessment();
				
				$i=0;
				foreach ($applicant_data as $data){
					
					//get education average mark
					$applicant_data[$i]['average_mark'] = $educationDB->getSelectionAverageMark($data["at_appl_id"],$data["at_trans_id"]);
					
					//get dean & rector rating
					$applicant_data[$i]['selection_rating'] = $assessmentDB->getData($data["at_trans_id"]);
					$i++;	
				}
											
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($applicant_data));
				$paginator->setItemCountPerPage($this->gintPageCount);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));			
				$this->view->paginator = $paginator;
			}
			
		}else
		if($academic_year && $period && $faculty){
			
			//$applicant_data = $applicantDB->getStatusSelection($condition);
			
			$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
			$applicant_data = $transactionDb->getTransactionStatus();
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($applicant_data));
			$paginator->setItemCountPerPage(2);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));			
			$this->view->paginator = $paginator;				
		}
		
		
		$this->view->searchParams = array('admission_type'=>2,
							 "academic_year"=>$academic_year,
							 "period"=>$period,
							 "faculty"=>$faculty,
							 "program_code"=>$program_code,
							 "selection_status"=>$selection_status					
							 );	
		
		$form->populate($condition);
		$this->view->form = $form;
		
		
	}
	
	
	public function formatAction(){
		$txnId = $this->_getParam('txnId',null);
		
		$this->generateOfferLetter($txnId);
		exit;
	}
	
	public function selectionDetailAction(){
		
		$this->view->title = $this->view->translate("Selection Status").$this->view->translate(" - Detail");
		
		$txnId = $this->_getParam('txn_id',null);
		$this->view->txn_id = $txnId;
		
		//txn data
		$applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
		$txnData = $applicantTxnDB->getTransaction($txnId);
		
		//programme applied
		$programDB =  new App_Model_Application_DbTable_ApplicantProgram();
		$program_list = $programDB->getApplicantProgramByID($txnId);
		$txnData['programme'] = $program_list;
		
		$this->view->data = $txnData;
		
		//selection rating
		$assessmentDB=new App_Model_Application_DbTable_ApplicantAssessment();
		$selection_status = $assessmentDB->getData($txnId);
		
		$this ->view->selection_data = $selection_status;
		
		//document
		$docs = array();
		$arr_doc = array();
		$arr_upload = array();
		
		$documentDB = new App_Model_Application_DbTable_ApplicantDocument();
		$documentUploadDb = new App_Model_Application_DbTable_ApplicantUploadFile();
			
		//document
		$arr_doc[0] = $documentDB->getDataArray($txnId, 30);
		$arr_doc[1] = $documentDB->getDataArray($txnId, 31);
		$arr_doc[2] = $documentDB->getDataArray($txnId, 32);
		$arr_doc[3] = $documentDB->getDataArray($txnId, 42);
		$arr_doc[4] = $documentDB->getDataArray($txnId, 45);
		$docs['document'] = $arr_doc;
		$this->view->arr_doc = $arr_doc;
		
		//upload document
		$arr_upload[0] = $documentUploadDb->getTxnFileArray($txnId,33); //gambar
		$arr_upload[1] = $documentUploadDb->getTxnFileArray($txnId,34); //kpt
		$arr_upload[2] = $documentUploadDb->getTxnFileArray($txnId,35); //passport
		$arr_upload[3] = $documentUploadDb->getTxnFileArray($txnId,36); //surat doctor
		$arr_upload[4] = $documentUploadDb->getTxnFileArray($txnId,37); //transcript
		$docs['upload_document'] = $arr_upload;
			
		$docs['path'] = "http://".ONNAPP_HOSTNAME."/documents/";
		 
		$this->view->doc = $docs;
		
	}
	
	public function generateOfferLetter($txnId){
		//get applicant info
		$applicantDB = new App_Model_Application_DbTable_ApplicantProfile();
    	$applicant = $applicantDB->getAllProfile($txnId);
    	
    	//get transaction info
    	$applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
		$txnData = $applicantTxnDB->getTransaction($txnId);
		
		$offerleter = new icampus_Function_Application_Offerletter();
		
		if($txnData['at_appl_type']==1){
			return $offerleter->generateUsmOfferLetter($txnId);
		}else{
			return $offerleter->generateOfferLetter($txnId);
		}
	}
	
	public function reverseSelectionAction(){
		
		$this->view->title = $this->view->translate("reverse_selection");
		
		$form = new Application_Form_SelectionStatusSearch(array('facultyid'=>$faculty));
    	$this->view->form=$form;  
    	
    	if ($this->getRequest()->isPost()) {			
			
			$formData = $this->getRequest()->getPost();											 
			
			$transactionDb = new App_Model_Application_DbTable_ApplicantTransaction();
			
			$applicant_data = $transactionDb->getRectorSelectionPSSBTransaction($formData["intake_id"], $formData["period_id"],'PROCESS',$formData["faculty"],$formData["programme"],$formData["load_previous_period"]);
			$this->view->paginator = $applicant_data;
			
			
			$form->populate($formData);
			$this->view->form = $form;
    	}
    	
	}
}
?>