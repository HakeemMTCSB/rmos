<?php

class Application_ProgramentryController extends Base_Base
{
    private $lobjprogramentry;
    private $lobjprogramentryForm;
    private $lobjprogram;
    private $lobjdefinitiontype;
    private $_gobjlog;
    private $lobjsubjecttogroupForm;
    private $lobjsubjectgradepoint;

    public function init()
    {
        $this->fnsetObj();

    }

    public function fnsetObj()
    {
        $this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
        $this->lobjprogramentryForm = new Application_Form_Programentry ();
        $this->lobjprogramentry = new Application_Model_DbTable_Programentry();
        $this->lobjdefinitiontype = new App_Model_Definitiontype();
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        $this->lobjsubjecttogroupForm = new Application_Form_Addsubjecttogroup();
        $this->lobjsubjectgradepoint = new Application_Model_DbTable_Subjectgradetype();
        $this->ProposedProgram = new Application_Model_DbTable_ProposedProgram();
    }

    public function indexAction()
    {

        $this->view->title = "Entry Requirements";
        $this->view->lobjform = $this->lobjform;

        $programlists = $this->lobjprogram->fnGetProgramList();
        $this->view->lobjform->field1->addMultiOptions($programlists);
        $query = $this->lobjprogramentry->fngetProgramEntryDetails();

        $sessionID = Zend_Session::getId();
        $this->lobjprogramentry->fnDeleteTempProgramentryDetailsBysession($sessionID);

        if (!$this->_getParam('search'))
            unset($this->gobjsessionsis->programentrypaginatorresult);

        if ($this->_request->isPost() && $this->_request->getPost('Search')) {
            $larrformData = $this->_request->getPost();

//            $larrformData['field1'] = $this->view->escape(strip_tags($larrformData['field1']));
//            $larrformData['field7'] = $this->view->escape(strip_tags($larrformData['field7']));

            if ($this->lobjform->isValid($larrformData)) {

                $query = $this->lobjprogramentry->fnSearchProgramEntry($this->lobjform->getValues()); //searching the values for the user
                $this->gobjsessionsis->filterQuery = $query;

            }
        }

        if (!$this->_getParam('search')) {
            unset($this->gobjsessionsis->filterQuery);
        } else {
            $query = $this->gobjsessionsis->filterQuery;
        }


        $pageCount = 50;
        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);

        $this->view->paginator = $paginator;


    }

    public function newprogramentryAction()
    { //title

        $this->view->lobjprogramentryform = $this->lobjprogramentryForm;
        $ldtsystemDate = date('Y-m-d H:i:s');
        $auth = Zend_Auth::getInstance();
        $this->view->lobjprogramentryform->UpdDate->setValue($ldtsystemDate);
        $this->view->lobjprogramentryform->UpdUser->setValue($auth->getIdentity()->iduser);

        $programlists = $this->lobjprogram->fnGetProgramList();
        $this->view->lobjprogramentryform->IdProgram->addMultiOptions($programlists);

        if ($this->_request->isPost() && $this->_request->getPost('Save')) {
            $larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post//
            $existing_entry = $this->lobjprogramentry->find_existing_program_entry($larrformData['IdProgram'], $larrformData['StartDate'], $larrformData['EndDate']);
            if (count($existing_entry) > 0) {
                $this->view->lobjprogramentryform->populate($larrformData);
                $this->view->lobjprogramentryform->IdProgram->setValue($larrformData['IdProgram']);
                $this->gobjsessionsis->flash = array('message' => $this->view->translate('There is already an program requirement set at this date'), 'type' => 'error');
            } else {
                if ($this->lobjprogramentryForm->isValid($larrformData)) {

                    $programentryresult_id = $this->lobjprogramentry->fnAddPOEntry($larrformData);

                    $programentrydetailsresult = $this->lobjprogramentry->fnAddPOEntryDetails($programentryresult_id, $larrformData);

                    $otherdetailsresult = $this->lobjprogramentry->fnAddOtherDetails($programentryresult_id, $larrformData);


                    if (count($larrformData['proposed_program'])) {
                        //clear the ones before this
                        $this->ProposedProgram->clear_program($programentryresult_id);

                        foreach ($larrformData['proposed_program'] as $proposed_id) {
                            $proposed['pp_IdProgramEntry'] = $programentryresult_id;
                            $proposed['pp_IdProgram'] = $proposed_id;
                            $proposed['pp_createddt'] = date('Y-m-d h:i:s');
                            $proposed['pp_createdby'] = $auth->getIdentity()->iduser;
                            $this->ProposedProgram->insert($proposed);
                        }

                    }

                    $this->_redirect($this->baseUrl . '/application/programentry/index');

                }
            }
        }

        $this->view->programs = $this->lobjprogram->list_all();
    }

    public function checkclashAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $post_data = $this->_request->getPost();
        $existing_entry = $this->lobjprogramentry->find_existing_program_entry($post_data['IdProgram'], $post_data['StartDate'], $post_data['EndDate']);
        if (count($existing_entry) > 0) {
            echo "true";
        } else {
            echo "false";
        }
        exit;
    }

    public function editprogramentryAction()
    {
        $sessionID = Zend_Session::getId();
        $this->view->lobjprogramentryform = $this->lobjprogramentryForm;
        $lintIdProgramEntry = (int)$this->_getParam('id');

        if (empty($lintIdProgramEntry)) {
            $this->gobjsessionsis->flash = array('message' => $this->view->translate('Could not get entry requirement setup'), 'type' => 'error');
            $this->_redirect($this->baseUrl . '/application/programentry/index');
        }

        $academicsreq_bygroup = array();
        $groupincurrentreq = array();

        $programentrylist = $this->lobjprogramentry->fngetAllentry($lintIdProgramEntry);
        foreach ($programentrylist as $programentrylist) {
            $academicsreq_bygroup[$programentrylist['GroupId']]['academic_requirement'][] = $programentrylist;
            $groupincurrentreq[$programentrylist['GroupId']] = $programentrylist['Groupname'];
        }
        $curotherdetails = $this->lobjprogramentry->fnGetProgramEditOtherDetails($lintIdProgramEntry);
        $this->view->curotherdetails = $curotherdetails;
        //$this->view->otherrequirements = $this->lobjprogramentry->fnGetTempprogramentryOtherDetails( $lintIdProgramEntry );
        foreach ($this->view->curotherdetails as $otherreq) {
            $academicsreq_bygroup[$otherreq['GroupId']]['other_requirement'][] = $otherreq;
            $groupincurrentreq[$otherreq['GroupId']] = $otherreq['GroupName'];
        }

        $this->view->groupincurrentreq = $groupincurrentreq;
        $this->view->academicsreq_bygroup = $academicsreq_bygroup;

        $this->view->programentrylist = $programentrylist;


        $programentrysubjectlist = $this->lobjprogramentry->fngetallsubjectentry($lintIdProgramEntry);
        $this->view->programentrysubjectlist = $programentrysubjectlist;

        $this->view->idprogramentry = $lintIdProgramEntry;

        if ($this->_getParam('update') != 'true') {
            //$sessionID = Zend_Session::getId();
            $this->lobjprogramentry->fnDeleteTempProgramentryDetailsBysession($sessionID);
            $this->lobjprogramentry->fnDeleteTempProgramentryOtherDetailsBysession($sessionID);
        }

        $ldtsystemDate = date('Y-m-d H:i:s');

        $auth = Zend_Auth::getInstance();
        $this->view->lobjprogramentryform->UpdDate->setValue($ldtsystemDate);
        $this->view->lobjprogramentryform->UpdUser->setValue($auth->getIdentity()->iduser);
        $this->view->lobjprogramentryform->IdProgramEntry->setValue($lintIdProgramEntry);


        $programlists = $this->lobjprogram->fnGetProgramList();
        $this->view->lobjprogramentryform->IdProgram->addMultiOptions($programlists);

        //get program entry
        $larrmodelResultDtls = $this->lobjprogramentry->fnViewProgramEntry($lintIdProgramEntry);
        $this->view->cur_program_entry = $this->lobjprogramentry->getProgramEntryDetails($lintIdProgramEntry);

        $this->view->lobjprogramentryform->populate($larrmodelResultDtls[0]);
        $this->view->IdProgram = $larrmodelResultDtls[0]['IdProgram'];

        $programentries = $this->lobjprogramentry->fnGetProgramEditEntryDetails($lintIdProgramEntry);

        $curotherdetails = $this->lobjprogramentry->fnGetProgramEditOtherDetails($lintIdProgramEntry);


        $existingprogram = $larrmodelResultDtls[0]['IdProgram'];

        $larrresult = $this->lobjprogramentry->fnGetTempprogramentryReqDetails($larrmodelResultDtls[0]['IdProgramEntry']);

        $this->view->otherdetails = $this->lobjprogramentry->fnGetTempprogramentryOtherDetails($larrmodelResultDtls[0]['IdProgramEntry'], $sessionID);


        $lintpagecount = $this->gintPageCount;
        $lintpage = $this->_getParam('page', 1); // Paginator instance

        $this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);

        if ($this->_request->isPost() && $this->_request->getPost('Save')) {
            $larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post

            if ($this->lobjprogramentryForm->isValid($larrformData)) {

                $this->lobjprogramentry->fnUpdateProgramentry($larrmodelResultDtls[0]['IdProgramEntry'], $larrformData);

                // Write Logs
                $priority = Zend_Log::INFO;
                $larrlog = array('user_id'     => $auth->getIdentity()->iduser,
                                 'level'       => $priority,
                                 'hostname'    => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                                 'time'        => date('Y-m-d H:i:s'),
                                 'message'     => 'Program Edit Id=' . $lintIdProgramEntry,
                                 'Description' => Zend_Log::DEBUG,
                                 'ip'          => $this->getRequest()->getServer('REMOTE_ADDR'));
                $this->_gobjlog->write($larrlog); //insert to tbl_log

                //$this->_redirect($this->view->url(array('module'=>'application' ,'controller'=>'programentry', 'action'=>'index'),'default',true));
                $this->_redirect($this->baseUrl . '/application/programentry/index');
                //}
            }
        }

        $this->view->programs = $this->lobjprogram->list_all();

    }

    public function editmainprogramentryAction()
    {
        $sessionID = Zend_Session::getId();
        $this->view->lobjprogramentryform = $this->lobjprogramentryForm;
        $lintIdProgramEntry = (int)$this->_getParam('id');

        if (empty($lintIdProgramEntry)) {
            $this->gobjsessionsis->flash = array('message' => $this->view->translate('Could not get entry requirement setup'), 'type' => 'error');
            $this->_redirect($this->baseUrl . '/application/programentry/index');
        }

        $this->view->idprogramentry = $lintIdProgramEntry;


        $ldtsystemDate = date('Y-m-d H:i:s');

        $auth = Zend_Auth::getInstance();
        $this->view->lobjprogramentryform->UpdDate->setValue($ldtsystemDate);
        $this->view->lobjprogramentryform->UpdUser->setValue($auth->getIdentity()->iduser);
        $this->view->lobjprogramentryform->IdProgramEntry->setValue($lintIdProgramEntry);


        $programlists = $this->lobjprogram->fnGetProgramList();
        $this->view->lobjprogramentryform->IdProgram->addMultiOptions($programlists);

        //get program entry
        $larrmodelResultDtls = $this->lobjprogramentry->fnViewProgramEntry($lintIdProgramEntry);
        $this->view->cur_program_entry = $this->lobjprogramentry->getProgramEntryDetails($lintIdProgramEntry);

        $this->view->lobjprogramentryform->populate($larrmodelResultDtls[0]);
        $this->view->IdProgram = $larrmodelResultDtls[0]['IdProgram'];


        if ($this->_request->isPost() && $this->_request->getPost('Save')) {
            $larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post

            if ($this->lobjprogramentryForm->isValid($larrformData)) {

                $this->lobjprogramentry->fnUpdateProgramentry($larrmodelResultDtls[0]['IdProgramEntry'], $larrformData);

                // Write Logs
                $priority = Zend_Log::INFO;
                $larrlog = array('user_id'     => $auth->getIdentity()->iduser,
                                 'level'       => $priority,
                                 'hostname'    => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                                 'time'        => date('Y-m-d H:i:s'),
                                 'message'     => 'Program Edit Id=' . $lintIdProgramEntry,
                                 'Description' => Zend_Log::DEBUG,
                                 'ip'          => $this->getRequest()->getServer('REMOTE_ADDR'));
                $this->_gobjlog->write($larrlog); //insert to tbl_log

                //$this->_redirect($this->view->url(array('module'=>'application' ,'controller'=>'programentry', 'action'=>'index'),'default',true));
                $this->_redirect($this->baseUrl . '/application/programentry/index');
                //}
            }
        }

        //$this->view->programs = $this->lobjprogram->list_all();

    }

    public function summaryprogramentryAction()
    {
        $lintIdProgramEntry = (int)$this->_getParam('id');
        $programentrylist = $this->lobjprogramentry->fngetAllentry($lintIdProgramEntry);
        $this->view->programentrylist = $programentrylist;
    }

    public function insertotherdetailreqAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $post_data = $this->_request->getPost();

        if (!empty($post_data['Item'])) {

            $IdProgramEntry = $post_data['IdProgramEntry'];
            $IdProgramReq = $post_data['IdProgramReq'];
            $Item = $post_data['Item'];
            $Condition = $post_data['Condition'];
            $Value = $post_data['Value'];
            $GroupId = $post_data['GroupId'];

        } else {
            $IdProgramEntry = $this->_getParam('IdProgramEntry');
            $IdProgramReq = $this->_getParam('IdProgramReq');
            $Item = $this->_getParam('Item');
            $Condition = $this->_getParam('Condition');
            $Value = $this->_getParam('Value');
            $GroupId = $this->_getParam('groupId');
        }
        $upddate = date('Y-m-d H:i:s');
        $auth = Zend_Auth::getInstance();
        $upduser = $auth->getIdentity()->iduser;

        $sessionID = Zend_Session::getId();

        if ($IdProgramReq) {
            $this->lobjprogramentry->fnUpdateEditOtherDetail($IdProgramEntry, $IdProgramReq, $Item, $Condition, $Value, $upddate, $upduser, $GroupId);
            echo "1";
        } else {
            $existsincurrent = $this->lobjprogramentry->fnCheckCurrentOtherItem($Item, $GroupId, $Condition, $IdProgramEntry);

            if (empty($existingresult) && empty($existsincurrent)) {
                $this->lobjprogramentry->fnInsertNewOtherDetail($IdProgramEntry, $IdProgramReq, $Item, $Condition, $Value, $upddate, $upduser, $GroupId);
                echo "1";
            } else {
                echo "2";
            }
        }
        exit;
    }

    public function insertprogramreqAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $post_data = $this->_request->getPost();
        if (!empty($post_data['EntryLevel'])) {

            $IdProgramEntry = $post_data['IdProgramEntry'];
            $IdProgramReq = $post_data['IdProgramReq'];
            $IdSpecialization = $post_data['IdSpecialization'];
            $Item = $post_data['Item'];
            $Condition = $post_data['Condition'];
            $Value = $post_data['Value'];
            $EntryLevel = $post_data['EntryLevel'];
            $Validity = $post_data['Validity'];
            $GroupId = $post_data['groupId'];

        } else {
            $IdProgramEntry = $this->_getParam('IdProgramEntry');
            $IdProgramReq = $this->_getParam('IdProgramReq');
            $IdSpecialization = $this->_getParam('IdSpecialization');
            $Item = $this->_getParam('Item');
            $Condition = $this->_getParam('Condition');
            $Value = $this->_getParam('Value');
            $EntryLevel = $this->_getParam('EntryLevel');
            $Validity = $this->_getParam('Validity');
            $GroupId = $this->_getParam('groupId');
        }

        $upddate = date('Y-m-d H:i:s');
        $auth = Zend_Auth::getInstance();
        $upduser = $auth->getIdentity()->iduser;


        $sessionID = Zend_Session::getId();

        if ($IdProgramReq) {
            $this->lobjprogramentry->fnUpdateEditProgramentryreqDetails(
                $IdProgramEntry, $IdProgramReq, $IdSpecialization, $Item, $EntryLevel, $Condition, $Value, $Validity, $upddate, $upduser, $GroupId);
            echo "1";
        } else {
            $existsincurrent = $this->lobjprogramentry->fnCheckCurrentItem($EntryLevel, $GroupId, $IdProgramEntry);

            if (empty($existingresult) && empty($existsincurrent)) {
                $this->lobjprogramentry->fnInsertNewProgramentryreqDetails(
                    $IdProgramEntry, $IdProgramReq,
                    $IdSpecialization, $Item,
                    $EntryLevel, $Condition,
                    $Value, $Validity,
                    $upddate, $upduser,
                    $GroupId);
                echo "1";
            } else {
                echo "2";
            }
        }

        exit;
    }

    public function deleteotherdetailAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        //Get Po details Id
        $IdProgramEntryReqOtherDetail = $this->_getParam('IdProgramEntryReqOtherDetail');
        $larrDelete = $this->lobjprogramentry->delete_other_details($IdProgramEntryReqOtherDetail);
        echo "1";
    }

    public function deleteprogramentryAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();


        $IdProgramEntryReq = $this->_getParam('IdProgramEntryReq');
        $larrDelete = $this->lobjprogramentry->delete_academic_requirement($IdProgramEntryReq);
        echo "1";
    }

    public function deletesubjectAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $GroupId = $this->_getParam('GroupId');
        $ProgramEntry = $this->_getParam('ProgramEntry');
        $IdSubject = $this->_getParam('IdSubject');
        $this->lobjprogramentry->fndeletesubject($GroupId, $ProgramEntry, $IdSubject);
        echo "1";
    }

    public function getlearningmodelistAction()
    {

        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender();
        $lintidProgram = $this->_getParam('idProgram');
        $ProgramentryModel = new Application_Model_DbTable_Programentry();
        $learningdtls = $ProgramentryModel->fnviewLearningModeDetails($lintidProgram);
        $data = "";
        for ($i = 0; $i < count($learningdtls); $i++) {
            $data = $data . $learningdtls[$i]['key'] . "-";
        }
        echo $data;
        exit;
        $this->view->lobjprogramentryForm = $this->lobjprogramentryForm;
        /*		if($learningdtls){
        $learningmode=$this->lobjdefinitiontype->fnGetDefinationMs("Learning Mode");*/
        $this->view->lobjprogramentryForm->LearningMode->addMultiOptions($learningdtls);
        /*		foreach($learningdtls as $learningdtls){
                   $arrIdLearningMode[]  = $learningdtls['key'];
               }
        echo $this->lobjprogramentryForm->LearningMode->setValue($arrIdLearningMode);
        }else{

            $learningmode=$this->lobjdefinitiontype->fnGetDefinationMs("Learning Mode");
            $this->lobjprogramentryForm->LearningMode->addMultiOptions( $learningmode);
            foreach($learningmode as $learningmode){
                   $arrIdLearningMode[]  = $learningmode['key'];
               }
        echo $this->lobjprogramentryForm->LearningMode->setValue('0');

        }	*/
    }

    public function addgrouptosubjectAction()
    {
        $groupId = $this->_getParam('groupid');
        $IdprogramEntry = $this->_getParam('programentry');
        $this->view->IdprogramEntryReq = $IdprogramEntryReq = $this->_getParam('programentryreq');

        $programreqDet = $this->lobjprogramentry->fnViewProgramEntryreq($IdprogramEntryReq);

        $result = $this->lobjdefinitiontype->fnGetDefinationNameString($groupId);
        $this->view->lobjsubjecttogroupForm = $this->lobjsubjecttogroupForm;
        $this->view->Idprogramentry = $IdprogramEntry;


        $this->view->larrstatusmessagelist = $this->lobjprogramentry->fngetgroupsubject($groupId, $IdprogramEntry, $programreqDet['EntryLevel']);

        $this->view->programentry = $IdprogramEntry;
        $ldtsystemDate = date('Y-m-d H:i:s');
        $auth = Zend_Auth::getInstance();
        $this->view->lobjsubjecttogroupForm->UpdDate->setValue($ldtsystemDate);
        $this->view->lobjsubjecttogroupForm->UpdUser->setValue($auth->getIdentity()->iduser);
        $this->view->lobjsubjecttogroupForm->IdProgramEntry->setValue($IdprogramEntry);
        $this->view->lobjsubjecttogroupForm->GroupId->setValue($groupId);
        $this->view->lobjsubjecttogroupForm->GroupName->setValue($result['DefinitionCode']);
        $this->view->lobjsubjecttogroupForm->IdProgramEntryReq->setValue($IdprogramEntryReq);
        $this->view->lobjsubjecttogroupForm->QualificationName->setValue($programreqDet['QualificationLevel']);
        $this->view->lobjsubjecttogroupForm->QualificationId->setValue($programreqDet['EntryLevel']);
        $gradePointList = $this->lobjsubjectgradepoint->getGradePoint($programreqDet['EntryLevel']);
        $this->view->lobjsubjecttogroupForm->MinGradePoint->addMultiOptions($gradePointList);

        if ($this->_request->isPost() && $this->_request->getPost('Save')) {
            $larrformData = $this->_request->getPost();

            $dataArray = array();
            $i = 1;
            $j = 1;
            $k = 1;
            $l = 1;
            $m = 1;
            $n = 1;
            if (isset($larrformData['subjectgrid']) && $larrformData['subjectgrid'] != '') {
                foreach ($larrformData['subjectgrid'] as $subjectId) {
                    if (isset($larrformData['IdProgramEntry'])) {
                        $dataArray[$i]['ProgramEntry'] = $larrformData['IdProgramEntry'];
                        $programentry = $larrformData['IdProgramEntry'];
                    } else {
                        $dataArray[$i]['ProgramEntry'] = $programentry;
                    }
                    $dataArray[$i]['IdSubject'] = $subjectId;
                    $i++;
                }
            }

            if (isset($larrformData['groupgrid']) && $larrformData['groupgrid'] != '') {
                foreach ($larrformData['groupgrid'] as $groupId) {
                    $dataArray[$j]['GroupId'] = $groupId;
                    $j++;
                }
            }

            if (isset($larrformData['qualificationgrid']) && $larrformData['qualificationgrid'] != '') {
                foreach ($larrformData['qualificationgrid'] as $qualificationId) {
                    $dataArray[$k]['IdQualification'] = $qualificationId;
                    $k++;
                }
            }

            if (isset($larrformData['fieldofstudygrid']) && $larrformData['fieldofstudygrid'] != '') {
                foreach ($larrformData['fieldofstudygrid'] as $studyfield) {
                    $dataArray[$l]['fieldofstudy'] = $studyfield;
                    $l++;
                }
            }

            if (isset($larrformData['noofsubjectgrid']) && $larrformData['noofsubjectgrid'] != '') {
                foreach ($larrformData['noofsubjectgrid'] as $nosubject) {
                    $dataArray[$m]['NoofSubject'] = $nosubject;
                    $m++;
                }
            }

            if (isset($larrformData['mingradepointgrid']) && $larrformData['mingradepointgrid'] != '') {
                foreach ($larrformData['mingradepointgrid'] as $gradepoint) {
                    $dataArray[$n]['MinGradePoint'] = $gradepoint;
                    $n++;
                }
            }
            $res = $this->lobjprogramentry->fndeleteSubjectGroup($groupId, $IdprogramEntry, $programreqDet['EntryLevel']);

            foreach ($dataArray as $data) {
                $this->lobjprogramentry->fnAddSubjectGroup($data);
            }
            $this->_redirect($this->baseUrl . '/application/programentry/editprogramentry/id/' . $IdprogramEntry);
        }

    }

    public function getprogramlistAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $ret = $this->lobjprogram->fngetprogrambyScheme();
        echo Zend_Json_Encoder::encode($ret);
    }

    public function getsubjectcatagoryAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $lintidsubject = $this->_getParam('idsubject');
        $result = $this->lobjprogram->fngetcatagory($lintidsubject);
        echo Zend_Json_Encoder::encode($result);
    }

    public function addproposedAction()
    {
        $auth = Zend_Auth::getInstance();
        if ($this->_request->isPost()) {
            $post_data = $this->_request->getPost();

            if (count($post_data['proposed_program'])) {
                //clear the ones before this
                $this->ProposedProgram->clear_program($post_data['idProgram']);

                foreach ($post_data['proposed_program'] as $proposed_id) {
                    $proposed['pp_IdProgramEntry'] = $post_data['idProgram'];
                    $proposed['pp_IdProgram'] = $proposed_id;
                    $proposed['pp_createddt'] = date('Y-m-d h:i:s');
                    $proposed['pp_createdby'] = $auth->getIdentity()->iduser;
                    $this->ProposedProgram->insert($proposed);
                }
            }

            $proposed_programs = $this->ProposedProgram->listByProgramEntry($post_data['idProgram']);
            echo json_encode($proposed_programs->toArray());
        }
        exit();
    }

    public function fetchproposedAction()
    {
        $program_id = $this->_getParam('id');
        $proposed_programs = $this->ProposedProgram->listByProgramEntry($program_id);
        echo json_encode($proposed_programs->toArray());
        exit();
    }

    public function removeproposedAction()
    {
        $program_id = $this->_getParam('program_id');

        $this->ProposedProgram->delete_program($program_id);
        echo "true";
        exit();
    }


    public function fetchAcademicRequirementsAction()
    {
        $programentry_id = $this->_getParam('id');
        $programentrylist = $this->lobjprogramentry->fngetAllentry($programentry_id);
        echo json_encode($programentrylist);
        exit();
    }

    public function addAcademicRequirementAction()
    {
        $auth = Zend_Auth::getInstance();
        if ($this->_request->isPost()) {
            $post_data = $this->_request->getPost();

            //add
            $programentry[IdProgramEntry] = $post_data['IdProgramEntry'];
            $programentry[IdProgramReq] = $post_data['IdProgramReq'];
            $programentry[IdSpecialization] = $post_data['IdSpecialization'];
            $programentry[Item] = $post_data['Item'];
            $programentry[Condition] = $post_data['Condition'];
            $programentry[Value] = $post_data['Value'];
            $programentry[EntryLevel] = $post_data['EntryLevel'];
            $programentry[Validity] = $post_data['Validity'];
            $programentry[GroupId] = $post_data['groupId'];

            //return list
            $proposed_programs = $this->ProposedProgram->listByProgramEntry($post_data['idProgram']);
            echo json_encode($proposed_programs->toArray());
        }
        exit();
    }

    public function fetchOtherRequirementsAction()
    {
        $programentry_id = $this->_getParam('id');
        $curotherdetails = $this->lobjprogramentry->fnGetProgramEditOtherDetails($programentry_id);
        if (!empty($curotherdetails)) {
            $curotherdetails = $curotherdetails->toArray();
        }
        echo json_encode($curotherdetails);
        exit();
    }

    public function generatesummaryAction()
    {
        $this->_helper->layout->disableLayout();

        $lintIdProgramEntry = (int)$this->_getParam('id');

        if (empty($lintIdProgramEntry)) {
            $this->gobjsessionsis->flash = array('message' => $this->view->translate('Could not get entry requirement setup'), 'type' => 'error');
            $this->_redirect($this->baseUrl . '/application/programentry/index');
        }

        $academicsreq_bygroup = array();
        $groupincurrentreq = array();

        $programentrylist = $this->lobjprogramentry->fngetAllentry($lintIdProgramEntry);
        foreach ($programentrylist as $programentrylist) {
            $academicsreq_bygroup[$programentrylist['GroupId']]['academic_requirement'][] = $programentrylist;
            $groupincurrentreq[$programentrylist['GroupId']] = $programentrylist['Groupname'];
        }
        $curotherdetails = $this->lobjprogramentry->fnGetProgramEditOtherDetails($lintIdProgramEntry);
        $this->view->curotherdetails = $curotherdetails;
        //$this->view->otherrequirements = $this->lobjprogramentry->fnGetTempprogramentryOtherDetails( $lintIdProgramEntry );
        foreach ($this->view->curotherdetails as $otherreq) {
            $academicsreq_bygroup[$otherreq['GroupId']]['other_requirement'][] = $otherreq;
            $groupincurrentreq[$otherreq['GroupId']] = $otherreq['GroupName'];
        }

        $this->view->groupincurrentreq = $groupincurrentreq;
        $this->view->academicsreq_bygroup = $academicsreq_bygroup;

    }

}
