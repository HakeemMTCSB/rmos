<?php

class Application_ProgramentryController extends Base_Base
{
    private $lobjprogramentry;
    private $lobjprogramentryForm;
    private $lobjprogram;
    private $lobjdefinitiontype;
    private $_gobjlog;
    private $lobjsubjecttogroupForm;
    private $lobjsubjectgradepoint;
    private $lobjscheme;

    public function init()
    {
        $this->fnsetObj();

    }

    public function fnsetObj()
    {
        $this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
        $this->lobjprogramentryForm = new Application_Form_Programentry ();
        $this->lobjprogramentry = new Application_Model_DbTable_Programentry();
        $this->lobjdefinitiontype = new App_Model_Definitiontype();
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        $this->lobjsubjecttogroupForm = new Application_Form_Addsubjecttogroup();
        $this->lobjsubjectgradepoint = new Application_Model_DbTable_Subjectgradetype();
        $this->ProposedProgram = new Application_Model_DbTable_ProposedProgram();
        $this->lobjscheme = new GeneralSetup_Model_DbTable_Schemesetup();
    }

    public function indexAction()
    {

        $this->view->title = "Entry Requirements";
        $this->view->lobjform = $this->lobjform;

        $programlists = $this->lobjprogram->fnGetProgramList();
        $this->view->lobjform->IdProgram->addMultiOptions($programlists);

        $schemeList = $this->lobjscheme->fngetSchemes();
        $this->view->lobjform->field1->addMultiOptions($schemeList);

        $query = $this->lobjprogramentry->fngetProgramEntryDetails();

        $sessionID = Zend_Session::getId();
        $this->lobjprogramentry->fnDeleteTempProgramentryDetailsBysession($sessionID);

        if (!$this->_getParam('search'))
            unset($this->gobjsessionsis->programentrypaginatorresult);

        if ($this->_request->isPost() && $this->_request->getPost('Search')) {
            $larrformData = $this->_request->getPost();

            if ($this->lobjform->isValid($larrformData)) {

                $query = $this->lobjprogramentry->fnSearchProgramEntry($this->lobjform->getValues()); //searching the values for the user
                $this->gobjsessionsis->filterQuery = $query;

            }
        }

        if (!$this->_getParam('search')) {
            unset($this->gobjsessionsis->filterQuery);
        } else {
            $query = $this->gobjsessionsis->filterQuery;
        }


        $pageCount = 50;
        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);

        $this->view->paginator = $paginator;


    }

    public function newprogramentryAction()
    {

        $auth = Zend_Auth::getInstance();

        $ldtsystemDate = date('Y-m-d H:i:s');
        $this->view->lobjprogramentryform = $this->lobjprogramentryForm;
        $this->view->lobjprogramentryform->UpdDate->setValue($ldtsystemDate);
        $this->view->lobjprogramentryform->UpdUser->setValue($auth->getIdentity()->iduser);

        $programlists = $this->lobjprogram->fnGetProgramList();
        $this->view->lobjprogramentryform->IdProgram->addMultiOptions($programlists);

        if ($this->_request->isPost() && $this->_request->getPost('Save')) {
            $larrformData = $this->_request->getPost();

            if ($this->lobjprogramentryForm->isValid($larrformData)) {

                $programentryresult_id = $this->lobjprogramentry->fnAddPOEntry($larrformData);

                $this->gobjsessionsis->flash = array('message' => $this->view->translate('Successfully saved'), 'type' => 'success');
                $this->_redirect($this->baseUrl . '/application/programentry/editprogramentry/id/' . $programentryresult_id);

            }
        }
    }

    public function checkclashAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $post_data = $this->_request->getPost();
        $existing_entry = $this->lobjprogramentry->find_existing_program_entry($post_data['IdProgram'], $post_data['StartDate'], $post_data['EndDate']);
        if (count($existing_entry) > 0) {
            echo "true";
        } else {
            echo "false";
        }
        exit;
    }

    public function editprogramentryAction()
    {

        $lintIdProgramEntry = (int)$this->_getParam('id');

        $sessionID = Zend_Session::getId();
        $this->view->lobjprogramentryform = $this->lobjprogramentryForm;

        if (empty($lintIdProgramEntry)) {
            $this->gobjsessionsis->flash = array('message' => $this->view->translate('Could not get entry requirement setup'), 'type' => 'error');
            $this->_redirect($this->baseUrl . '/application/programentry/index');
        }

        $academicsreq_bygroup = array();
        $groupincurrentreq = array();

        $programentrylist = $this->lobjprogramentry->fngetAllentry($lintIdProgramEntry);
        foreach ($programentrylist as $programentrylist) {
            $academicsreq_bygroup[$programentrylist['GroupId']]['academic_requirement'][] = $programentrylist;
            $groupincurrentreq[$programentrylist['GroupId']] = $programentrylist['Groupname'];
        }
        $curotherdetails = $this->lobjprogramentry->fnGetProgramEditOtherDetails($lintIdProgramEntry);
        $this->view->curotherdetails = $curotherdetails;
        //$this->view->otherrequirements = $this->lobjprogramentry->fnGetTempprogramentryOtherDetails( $lintIdProgramEntry );
        foreach ($this->view->curotherdetails as $otherreq) {
            $academicsreq_bygroup[$otherreq['GroupId']]['other_requirement'][] = $otherreq;
            $groupincurrentreq[$otherreq['GroupId']] = $otherreq['GroupName'];
        }

        $this->view->groupincurrentreq = $groupincurrentreq;
        $this->view->academicsreq_bygroup = $academicsreq_bygroup;
        $this->view->id = $lintIdProgramEntry;

        $this->view->programentrylist = $programentrylist;


        $programentrysubjectlist = $this->lobjprogramentry->fngetallsubjectentry($lintIdProgramEntry);
        $this->view->programentrysubjectlist = $programentrysubjectlist;

        $this->view->idprogramentry = $lintIdProgramEntry;

        if ($this->_getParam('update') != 'true') {
            //$sessionID = Zend_Session::getId();
            $this->lobjprogramentry->fnDeleteTempProgramentryDetailsBysession($sessionID);
            $this->lobjprogramentry->fnDeleteTempProgramentryOtherDetailsBysession($sessionID);
        }

        $ldtsystemDate = date('Y-m-d H:i:s');

        $auth = Zend_Auth::getInstance();
        $this->view->lobjprogramentryform->UpdDate->setValue($ldtsystemDate);
        $this->view->lobjprogramentryform->UpdUser->setValue($auth->getIdentity()->iduser);
        $this->view->lobjprogramentryform->IdProgramEntry->setValue($lintIdProgramEntry);


        $programlists = $this->lobjprogram->fnGetProgramList();
        $this->view->lobjprogramentryform->IdProgram->addMultiOptions($programlists);

        //get program entry
        $larrmodelResultDtls = $this->lobjprogramentry->fnViewProgramEntry($lintIdProgramEntry);
        $this->view->cur_program_entry = $this->lobjprogramentry->getProgramEntryDetails($lintIdProgramEntry);

        $this->view->lobjprogramentryform->populate($larrmodelResultDtls[0]);
        $this->view->IdProgram = $larrmodelResultDtls[0]['IdProgram'];


        $curotherdetails = $this->lobjprogramentry->fnGetProgramEditOtherDetails($lintIdProgramEntry);


        $existingprogram = $larrmodelResultDtls[0]['IdProgram'];

        $larrresult = $this->lobjprogramentry->fnGetTempprogramentryReqDetails($larrmodelResultDtls[0]['IdProgramEntry']);

        $this->view->otherdetails = $this->lobjprogramentry->fnGetTempprogramentryOtherDetails($larrmodelResultDtls[0]['IdProgramEntry'], $sessionID);


        $lintpagecount = $this->gintPageCount;
        $lintpage = $this->_getParam('page', 1); // Paginator instance

        $this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);

        if ($this->_request->isPost() && $this->_request->getPost('Save')) {
            $larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post

            if ($this->lobjprogramentryForm->isValid($larrformData)) {

                $this->lobjprogramentry->fnUpdateProgramentry($larrmodelResultDtls[0]['IdProgramEntry'], $larrformData);

                // Write Logs
                $priority = Zend_Log::INFO;
                $larrlog = array('user_id'     => $auth->getIdentity()->iduser,
                                 'level'       => $priority,
                                 'hostname'    => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                                 'time'        => date('Y-m-d H:i:s'),
                                 'message'     => 'Program Edit Id=' . $lintIdProgramEntry,
                                 'Description' => Zend_Log::DEBUG,
                                 'ip'          => $this->getRequest()->getServer('REMOTE_ADDR'));
                $this->_gobjlog->write($larrlog); //insert to tbl_log

                //$this->_redirect($this->view->url(array('module'=>'application' ,'controller'=>'programentry', 'action'=>'index'),'default',true));
                $this->_redirect($this->baseUrl . '/application/programentry/index');
                //}
            }
        }


        if ($this->_request->isPost()) {
            $post_data = $this->_request->getPost();


            //add academic requirements
            if (isset($post_data['idprogramentry'])) {

                //add
                $programentry['IdProgramEntry'] = $post_data['idprogramentry'];
                $programentry['IdSpecialization'] = $post_data['IdSpecialization'];
                $programentry['Item'] = $post_data['Item'];
                $programentry['Condition'] = $post_data['Condition'];
                $programentry['Value'] = $post_data['Value'];
                $programentry['EntryLevel'] = $post_data['EntryLevel'];
                $programentry['require_au'] = $post_data['require_au'];
                $programentry['honors'] = $post_data['honors'];
                $programentry['Validity'] = $post_data['Validity'];
                $programentry['pid'] = $post_data['pid'] ?? 0;
                $programentry['Mandatory'] = $post_data['Mandatory'];
                $programentry['requirement'] = $post_data['requirement'];
                $programentry['requirementMalay'] = $post_data['requirementMalay'];
                $programentry['requirementCategory'] = $post_data['requirementCategory'];
                $programentry['open_entry'] = $post_data['open_entry'] ?? 0;

                $this->lobjprogramentry->addAcademicRequirement($programentry);

                //add academic requirements subject
            } elseif ($post_data['IdProgramEntryReq']) {

                //add
                $programentry['IdProgramEntryReq'] = $post_data['IdProgramEntryReq'];
                $programentry['IdSubject'] = $post_data['IdSubject'];
                $programentry['IdGrade'] = $post_data['IdGrade'];
                $programentry['created_by'] = $auth->getIdentity()->iduser;
                $programentry['created_at'] = date('Y-m-d H:i:s');

                $this->lobjprogramentry->addAcademicRequirementSubject($programentry);

                if ($post_data['subject_no']) {
                    $this->lobjprogramentry->updateAcademicRequirement(array('subject_no' => $post_data['subject_no']), $post_data['IdProgramEntryReq']);
                }

            }
            $this->gobjsessionsis->flash = array('message' => $this->view->translate('Data successfully added'), 'type' => 'success');
            $this->_redirect($this->baseUrl . '/application/programentry/editprogramentry/id/' . $lintIdProgramEntry . '#tab-2');


        }


        $this->view->programs = $this->lobjprogram->list_all();

    }

    public function editmainprogramentryAction()
    {

        $lintIdProgramEntry = (int)$this->_getParam('id');

        $this->view->lobjprogramentryform = $this->lobjprogramentryForm;

        if (empty($lintIdProgramEntry)) {
            $this->gobjsessionsis->flash = array('message' => $this->view->translate('Data not found'), 'type' => 'error');
            $this->_redirect($this->baseUrl . '/application/programentry/index');
        }

        $this->view->idprogramentry = $lintIdProgramEntry;

        $ldtsystemDate = date('Y-m-d H:i:s');

        $auth = Zend_Auth::getInstance();
        $this->view->lobjprogramentryform->UpdDate->setValue($ldtsystemDate);
        $this->view->lobjprogramentryform->UpdUser->setValue($auth->getIdentity()->iduser);
        $this->view->lobjprogramentryform->IdProgramEntry->setValue($lintIdProgramEntry);

        $programlists = $this->lobjprogram->fnGetProgramList();
        $this->view->lobjprogramentryform->IdProgram->addMultiOptions($programlists);

        //get program entry
        $larrmodelResultDtls = $this->lobjprogramentry->getProgramEntryDetails($lintIdProgramEntry);
        $this->view->lobjprogramentryform->populate($larrmodelResultDtls);
        $this->view->IdProgram = $larrmodelResultDtls['IdProgram'];

        if ($this->_request->isPost() && $this->_request->getPost('Save')) {
            $larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post

            if ($this->lobjprogramentryForm->isValid($larrformData)) {

                $this->lobjprogramentry->fnUpdateProgramentry($larrmodelResultDtls['IdProgramEntry'], $larrformData);

                // Write Logs
                $priority = Zend_Log::INFO;
                $larrlog = array('user_id'     => $auth->getIdentity()->iduser,
                                 'level'       => $priority,
                                 'hostname'    => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                                 'time'        => date('Y-m-d H:i:s'),
                                 'message'     => 'Program Edit Id=' . $lintIdProgramEntry,
                                 'Description' => Zend_Log::DEBUG,
                                 'ip'          => $this->getRequest()->getServer('REMOTE_ADDR'));
                $this->_gobjlog->write($larrlog); //insert to tbl_log

                $this->gobjsessionsis->flash = array('message' => $this->view->translate('Data successfully updated'), 'type' => 'success');
                $this->_redirect($this->baseUrl . '/application/programentry/index');
            }
        }

    }

    public function summaryprogramentryAction()
    {
        $lintIdProgramEntry = (int)$this->_getParam('id');
        $programentrylist = $this->lobjprogramentry->fngetAllentry($lintIdProgramEntry);
        $this->view->programentrylist = $programentrylist;
    }

    public function insertotherdetailreqAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $post_data = $this->_request->getPost();

        if (!empty($post_data['Item'])) {

            $IdProgramEntry = $post_data['IdProgramEntry'];
            $IdProgramReq = $post_data['IdProgramReq'];
            $Item = $post_data['Item'];
            $Condition = $post_data['Condition'];
            $Value = $post_data['Value'];
            $GroupId = $post_data['GroupId'];

        } else {
            $IdProgramEntry = $this->_getParam('IdProgramEntry');
            $IdProgramReq = $this->_getParam('IdProgramReq');
            $Item = $this->_getParam('Item');
            $Condition = $this->_getParam('Condition');
            $Value = $this->_getParam('Value');
            $GroupId = $this->_getParam('groupId');
        }
        $upddate = date('Y-m-d H:i:s');
        $auth = Zend_Auth::getInstance();
        $upduser = $auth->getIdentity()->iduser;

        $sessionID = Zend_Session::getId();

        if ($IdProgramReq) {
            $this->lobjprogramentry->fnUpdateEditOtherDetail($IdProgramEntry, $IdProgramReq, $Item, $Condition, $Value, $upddate, $upduser, $GroupId);
            echo "1";
        } else {
            $existsincurrent = $this->lobjprogramentry->fnCheckCurrentOtherItem($Item, $GroupId, $Condition, $IdProgramEntry);

            if (empty($existingresult) && empty($existsincurrent)) {
                $this->lobjprogramentry->fnInsertNewOtherDetail($IdProgramEntry, $IdProgramReq, $Item, $Condition, $Value, $upddate, $upduser, $GroupId);
                echo "1";
            } else {
                echo "2";
            }
        }
        exit;
    }

    public function insertprogramreqAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $post_data = $this->_request->getPost();
        if (!empty($post_data['EntryLevel'])) {

            $IdProgramEntry = $post_data['IdProgramEntry'];
            $IdProgramReq = $post_data['IdProgramReq'];
            $IdSpecialization = $post_data['IdSpecialization'];
            $Item = $post_data['Item'];
            $Condition = $post_data['Condition'];
            $Value = $post_data['Value'];
            $EntryLevel = $post_data['EntryLevel'];
            $Validity = $post_data['Validity'];
            $GroupId = $post_data['groupId'];

        } else {
            $IdProgramEntry = $this->_getParam('IdProgramEntry');
            $IdProgramReq = $this->_getParam('IdProgramReq');
            $IdSpecialization = $this->_getParam('IdSpecialization');
            $Item = $this->_getParam('Item');
            $Condition = $this->_getParam('Condition');
            $Value = $this->_getParam('Value');
            $EntryLevel = $this->_getParam('EntryLevel');
            $Validity = $this->_getParam('Validity');
            $GroupId = $this->_getParam('groupId');
        }

        $upddate = date('Y-m-d H:i:s');
        $auth = Zend_Auth::getInstance();
        $upduser = $auth->getIdentity()->iduser;


        $sessionID = Zend_Session::getId();

        if ($IdProgramReq) {
            $this->lobjprogramentry->fnUpdateEditProgramentryreqDetails(
                $IdProgramEntry, $IdProgramReq, $IdSpecialization, $Item, $EntryLevel, $Condition, $Value, $Validity, $upddate, $upduser, $GroupId);
            echo "1";
        } else {
            $existsincurrent = $this->lobjprogramentry->fnCheckCurrentItem($EntryLevel, $GroupId, $IdProgramEntry);

            if (empty($existingresult) && empty($existsincurrent)) {
                $this->lobjprogramentry->fnInsertNewProgramentryreqDetails(
                    $IdProgramEntry, $IdProgramReq,
                    $IdSpecialization, $Item,
                    $EntryLevel, $Condition,
                    $Value, $Validity,
                    $upddate, $upduser,
                    $GroupId);
                echo "1";
            } else {
                echo "2";
            }
        }

        exit;
    }

    public function deleteotherdetailAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        //Get Po details Id
        $IdProgramEntryReqOtherDetail = $this->_getParam('IdProgramEntryReqOtherDetail');
        $larrDelete = $this->lobjprogramentry->delete_other_details($IdProgramEntryReqOtherDetail);
        echo "1";
    }

    public function deleteprogramentryAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();


        $IdProgramEntryReq = $this->_getParam('IdProgramEntryReq');
        $larrDelete = $this->lobjprogramentry->delete_academic_requirement($IdProgramEntryReq);
        echo "1";
    }

    public function deletesubjectAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $GroupId = $this->_getParam('GroupId');
        $ProgramEntry = $this->_getParam('ProgramEntry');
        $IdSubject = $this->_getParam('IdSubject');
        $this->lobjprogramentry->fndeletesubject($GroupId, $ProgramEntry, $IdSubject);
        echo "1";
    }

    public function getlearningmodelistAction()
    {

        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender();
        $lintidProgram = $this->_getParam('idProgram');
        $ProgramentryModel = new Application_Model_DbTable_Programentry();
        $learningdtls = $ProgramentryModel->fnviewLearningModeDetails($lintidProgram);
        $data = "";
        for ($i = 0; $i < count($learningdtls); $i++) {
            $data = $data . $learningdtls[$i]['key'] . "-";
        }
        echo $data;
        exit;
        $this->view->lobjprogramentryForm = $this->lobjprogramentryForm;
        /*		if($learningdtls){
        $learningmode=$this->lobjdefinitiontype->fnGetDefinationMs("Learning Mode");*/
        $this->view->lobjprogramentryForm->LearningMode->addMultiOptions($learningdtls);
        /*		foreach($learningdtls as $learningdtls){
                   $arrIdLearningMode[]  = $learningdtls['key'];
               }
        echo $this->lobjprogramentryForm->LearningMode->setValue($arrIdLearningMode);
        }else{

            $learningmode=$this->lobjdefinitiontype->fnGetDefinationMs("Learning Mode");
            $this->lobjprogramentryForm->LearningMode->addMultiOptions( $learningmode);
            foreach($learningmode as $learningmode){
                   $arrIdLearningMode[]  = $learningmode['key'];
               }
        echo $this->lobjprogramentryForm->LearningMode->setValue('0');

        }	*/
    }

    public function addgrouptosubjectAction()
    {
        $groupId = $this->_getParam('groupid');
        $IdprogramEntry = $this->_getParam('programentry');
        $this->view->IdprogramEntryReq = $IdprogramEntryReq = $this->_getParam('programentryreq');

        $programreqDet = $this->lobjprogramentry->fnViewProgramEntryreq($IdprogramEntryReq);

        $result = $this->lobjdefinitiontype->fnGetDefinationNameString($groupId);
        $this->view->lobjsubjecttogroupForm = $this->lobjsubjecttogroupForm;
        $this->view->Idprogramentry = $IdprogramEntry;


        $this->view->larrstatusmessagelist = $this->lobjprogramentry->fngetgroupsubject($groupId, $IdprogramEntry, $programreqDet['EntryLevel']);

        $this->view->programentry = $IdprogramEntry;
        $ldtsystemDate = date('Y-m-d H:i:s');
        $auth = Zend_Auth::getInstance();
        $this->view->lobjsubjecttogroupForm->UpdDate->setValue($ldtsystemDate);
        $this->view->lobjsubjecttogroupForm->UpdUser->setValue($auth->getIdentity()->iduser);
        $this->view->lobjsubjecttogroupForm->IdProgramEntry->setValue($IdprogramEntry);
        $this->view->lobjsubjecttogroupForm->GroupId->setValue($groupId);
        $this->view->lobjsubjecttogroupForm->GroupName->setValue($result['DefinitionCode']);
        $this->view->lobjsubjecttogroupForm->IdProgramEntryReq->setValue($IdprogramEntryReq);
        $this->view->lobjsubjecttogroupForm->QualificationName->setValue($programreqDet['QualificationLevel']);
        $this->view->lobjsubjecttogroupForm->QualificationId->setValue($programreqDet['EntryLevel']);
        $gradePointList = $this->lobjsubjectgradepoint->getGradePoint($programreqDet['EntryLevel']);
        $this->view->lobjsubjecttogroupForm->MinGradePoint->addMultiOptions($gradePointList);

        if ($this->_request->isPost() && $this->_request->getPost('Save')) {
            $larrformData = $this->_request->getPost();

            $dataArray = array();
            $i = 1;
            $j = 1;
            $k = 1;
            $l = 1;
            $m = 1;
            $n = 1;
            if (isset($larrformData['subjectgrid']) && $larrformData['subjectgrid'] != '') {
                foreach ($larrformData['subjectgrid'] as $subjectId) {
                    if (isset($larrformData['IdProgramEntry'])) {
                        $dataArray[$i]['ProgramEntry'] = $larrformData['IdProgramEntry'];
                        $programentry = $larrformData['IdProgramEntry'];
                    } else {
                        $dataArray[$i]['ProgramEntry'] = $programentry;
                    }
                    $dataArray[$i]['IdSubject'] = $subjectId;
                    $i++;
                }
            }

            if (isset($larrformData['groupgrid']) && $larrformData['groupgrid'] != '') {
                foreach ($larrformData['groupgrid'] as $groupId) {
                    $dataArray[$j]['GroupId'] = $groupId;
                    $j++;
                }
            }

            if (isset($larrformData['qualificationgrid']) && $larrformData['qualificationgrid'] != '') {
                foreach ($larrformData['qualificationgrid'] as $qualificationId) {
                    $dataArray[$k]['IdQualification'] = $qualificationId;
                    $k++;
                }
            }

            if (isset($larrformData['fieldofstudygrid']) && $larrformData['fieldofstudygrid'] != '') {
                foreach ($larrformData['fieldofstudygrid'] as $studyfield) {
                    $dataArray[$l]['fieldofstudy'] = $studyfield;
                    $l++;
                }
            }

            if (isset($larrformData['noofsubjectgrid']) && $larrformData['noofsubjectgrid'] != '') {
                foreach ($larrformData['noofsubjectgrid'] as $nosubject) {
                    $dataArray[$m]['NoofSubject'] = $nosubject;
                    $m++;
                }
            }

            if (isset($larrformData['mingradepointgrid']) && $larrformData['mingradepointgrid'] != '') {
                foreach ($larrformData['mingradepointgrid'] as $gradepoint) {
                    $dataArray[$n]['MinGradePoint'] = $gradepoint;
                    $n++;
                }
            }
            $res = $this->lobjprogramentry->fndeleteSubjectGroup($groupId, $IdprogramEntry, $programreqDet['EntryLevel']);

            foreach ($dataArray as $data) {
                $this->lobjprogramentry->fnAddSubjectGroup($data);
            }
            $this->_redirect($this->baseUrl . '/application/programentry/editprogramentry/id/' . $IdprogramEntry);
        }

    }

    public function getprogramlistAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $ret = $this->lobjprogram->fngetprogrambyScheme();
        echo Zend_Json_Encoder::encode($ret);
    }

    public function getsubjectcatagoryAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $lintidsubject = $this->_getParam('idsubject');
        $result = $this->lobjprogram->fngetcatagory($lintidsubject);
        echo Zend_Json_Encoder::encode($result);
    }

    public function addproposedAction()
    {
        $auth = Zend_Auth::getInstance();
        if ($this->_request->isPost()) {
            $post_data = $this->_request->getPost();

            if (count($post_data['proposed_program'])) {
                //clear the ones before this
                $this->ProposedProgram->clear_program($post_data['idProgram']);

                foreach ($post_data['proposed_program'] as $proposed_id) {
                    $proposed['pp_IdProgramEntry'] = $post_data['idProgram'];
                    $proposed['pp_IdProgram'] = $proposed_id;
                    $proposed['pp_createddt'] = date('Y-m-d h:i:s');
                    $proposed['pp_createdby'] = $auth->getIdentity()->iduser;
                    $this->ProposedProgram->insert($proposed);
                }
            }

            $proposed_programs = $this->ProposedProgram->listByProgramEntry($post_data['idProgram']);
            echo json_encode($proposed_programs->toArray());
        }
        exit();
    }

    public function fetchproposedAction()
    {
        $program_id = $this->_getParam('id');
        $proposed_programs = $this->ProposedProgram->listByProgramEntry($program_id);
        echo json_encode($proposed_programs->toArray());
        exit();
    }

    public function removeproposedAction()
    {
        $program_id = $this->_getParam('program_id');

        $this->ProposedProgram->delete_program($program_id);
        echo "true";
        exit();
    }


    public function fetchAcademicRequirementsAction()
    {
        $programentry_id = $this->_getParam('id');
        $programentrylist = $this->lobjprogramentry->fngetAllentry($programentry_id);
        echo json_encode($programentrylist);
        exit();
    }

    public function addAcademicRequirementAction()
    {

        $this->_helper->layout->disableLayout();
        $auth = Zend_Auth::getInstance();

        if ($this->_request->isPost()) {
            $post_data = $this->_request->getPost();

            dd($post_data);

            exit;
            //add
            $programentry['IdProgramEntry'] = $post_data['idprogramentry'];
            $programentry['IdSpecialization'] = $post_data['IdSpecialization'];
            $programentry['Item'] = $post_data['Item'];
            $programentry['Condition'] = $post_data['Condition'];
            $programentry['Value'] = $post_data['Value'];
            $programentry['EntryLevel'] = $post_data['EntryLevel'];
            $programentry['Validity'] = $post_data['Validity'];
            $programentry['pid'] = 0;
            $programentry['Mandatory'] = $post_data['Mandatory'];

            $this->lobjprogramentry->addAcademicRequirement($programentry);
        }

        $this->gobjsessionsis->flash = array('message' => $this->view->translate('Data successfully added'), 'type' => 'success');
        $this->_redirect($this->baseUrl . '/application/programentry/editprogramentry/id/' . $post_data['idprogramentry'] . '#tab-2');
    }

    public function fetchOtherRequirementsAction()
    {
        $programentry_id = $this->_getParam('id');
        $curotherdetails = $this->lobjprogramentry->fnGetProgramEditOtherDetails($programentry_id);
        if (!empty($curotherdetails)) {
            $curotherdetails = $curotherdetails->toArray();
        }
        echo json_encode($curotherdetails);
        exit();
    }

    public function generatesummaryAction()
    {
        $this->_helper->layout->disableLayout();

        $lintIdProgramEntry = (int)$this->_getParam('id');

        if (empty($lintIdProgramEntry)) {
            $this->gobjsessionsis->flash = array('message' => $this->view->translate('Could not get entry requirement setup'), 'type' => 'error');
            $this->_redirect($this->baseUrl . '/application/programentry/index');
        }

        $academicsreq_bygroup = array();
        $groupincurrentreq = array();

        $programentrylist = $this->lobjprogramentry->fngetAllentry($lintIdProgramEntry);
        foreach ($programentrylist as $programentrylist) {
            $academicsreq_bygroup[$programentrylist['GroupId']]['academic_requirement'][] = $programentrylist;
            $groupincurrentreq[$programentrylist['GroupId']] = $programentrylist['Groupname'];
        }
        $curotherdetails = $this->lobjprogramentry->fnGetProgramEditOtherDetails($lintIdProgramEntry);
        $this->view->curotherdetails = $curotherdetails;
        //$this->view->otherrequirements = $this->lobjprogramentry->fnGetTempprogramentryOtherDetails( $lintIdProgramEntry );
        foreach ($this->view->curotherdetails as $otherreq) {
            $academicsreq_bygroup[$otherreq['GroupId']]['other_requirement'][] = $otherreq;
            $groupincurrentreq[$otherreq['GroupId']] = $otherreq['GroupName'];
        }

        $this->view->groupincurrentreq = $groupincurrentreq;
        $this->view->academicsreq_bygroup = $academicsreq_bygroup;

    }

    public function academicRequirementAction()
    {

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $this->view->title = "View Academic Requirements";

        $id = $this->_getParam('id', 0);

        $programentries = $this->lobjprogramentry->getAcademicRequirements($id);


        $this->view->form = new Application_Form_AcademicRequirement(array('id' => $id));
        $this->view->formSubject = new Application_Form_AcademicRequirementSubject();
        $this->view->id = $id;
        $this->view->data = $programentries;

    }

    public function editAcademicRequirementAction()
    {

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $this->view->title = "Edit Academic Requirements";

        $id = $this->_getParam('id', 0);

        $this->view->form = new Application_Form_AcademicRequirement(array('id' => $id));

        $this->view->id = $id;

    }

    public function getGradeAction()
    {
        $IdQualification = $this->_getParam('IdQualification', null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $subjectGradeDB = new Application_Model_DbTable_Subjectgradetype();
        $gradeList = $subjectGradeDB->getGradeByQualification($IdQualification);

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($gradeList);

        echo $json;
        exit();
    }

    public function academicRequirementOeAction()
    {

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }

        $this->view->title = "View Academic Requirements";

        $id = $this->_getParam('id', 0);

        $programentries = $this->lobjprogramentry->getAcademicRequirements($id, 1);


        $this->view->form = new Application_Form_AcademicRequirement(array('id' => $id, 'oe' => 1));
        $this->view->formSubject = new Application_Form_AcademicRequirementSubject();
        $this->view->id = $id;
        $this->view->data = $programentries;

    }

    /*
     * MIGRATION
     */

    public function migrateAction()
    {
        $auth = Zend_Auth::getInstance();
        $db = Zend_Db_Table::getDefaultAdapter();

        echo '<pre>';
        $select = $db->select()
            ->from(array("a" => "migrate_entry_req_main"))
//            ->where('ERM_DEGREE = ? ', 'MIDT')
            ->where('migrated_at is null')
            //->where('ERM_COMPULSORY is not null')
            ->order('ERM_PROG')
            ->order('ERM_DEGREE')
            ->order('ERM_STRUC')
            ->order('ERM_REQ_PKG')
            ->order('ISNULL(ERM_COMPULSORY)');
//            ->order('ERM_COMPULSORY');
//            ->group('ERM_REQ_PKG')
//            ->group('ERM_COMPULSORY')
//            ->group('ERM_QUA_TYPE');
//            ->limit(100);

        $mainReqs = $db->fetchAll($select);

        if ($mainReqs) {
            foreach ($mainReqs as $main) {

                //tbl_programentry
                $schemeId = $this->getSchemeByCode($main['ERM_PROG']);
                $programId = ($main['ERM_DEGREE'] == 'ALL') ? 0 : $this->getProgramByCode($main['ERM_DEGREE']);
                $programSchemeId = ($main['ERM_STRUC'] == 'ALL') ? 0 : $this->getProgramScheme($programId, $main['ERM_STRUC']);
//                $programSchemeId = ($main['ERM_STRUC'] == 'ALL') ? 0 : $this->getRegistryValue('pg-structure', $main['ERM_STRUC']);
//                $programSchemeId = $this->getRegistryValue('pg-structure', $main['ERM_STRUC']);

                //check existing
                $programentryresult_id = $this->checkProgramEntry($schemeId, $programId, $programSchemeId);

                if (!$programentryresult_id) {

                    $programentry['IdProgram'] = $programId;
                    $programentry['IdScheme'] = $schemeId;
                    $programentry['IdProgramScheme'] = $programSchemeId;
                    $programentry['pe_proposal'] = ($main['ERM_PROPOSAL'] == 'Y') ? 1 : 0;
                    $programentry['pe_url'] = $main['ERM_LINK'];
                    $programentry['open_entry'] = ($main['ERM_OE'] == 'Y') ? 1 : 0;
//                    $programentry['open_entry'] = 0;
                    $programentry['migrated_at'] = date('Y-m-d H:i:s');
                    $programentry['UpdDate'] = date('Y-m-d H:i:s');
                    $programentry['UpdUser'] = 1;

//                    dd($programentry);exit;

                    $programentryresult_id = $this->lobjprogramentry->fnAddPOEntry($programentry);
                    $pid = 0;
                }

                //tbl_programentryrequirement
                $refEntry = $main['ERM_COMPULSORY'] . $main['ERM_QUA_TYPE'];

                $entryLevel = $this->getQualificationByCode($main['ERM_QUA_TYPE']);

                $programentryDet['IdProgramEntry'] = $programentryresult_id;
                $programentryDet['ref'] = $refEntry;
                $programentryDet['groupRef'] = $main['ERM_REQ_PKG'];
                $programentryDet['refMain'] = $main['ERM_REQ_ID'];
                $programentryDet['IdSpecialization'] = ($main['ERM_SF_TYPE'] == 'ANY') ? 0 : $this->getStudyField($main['ERM_SF_TYPE']);
                $programentryDet['Item'] = 38; //cgpa
                $programentryDet['Condition'] = 40; // == equal to
                $programentryDet['Value'] = ($main['ERM_CGPA'] != '0' ? $main['ERM_CGPA'] : 0);
                $programentryDet['EntryLevel'] = $entryLevel;
                $programentryDet['require_au'] = $main['ERM_REQ_AU'];
                $programentryDet['honors'] = ($main['ERM_HONS'] == 'Y') ? 1 : 0;
                $programentryDet['Validity'] = $main['ERM_YEAR'];
                $programentryDet['Mandatory'] = ($main['ERM_COMPULSORY']) ? 1 : 0;
                $programentryDet['requirement'] = null;
                $programentryDet['requirementMalay'] = null;
                $programentryDet['requirementCategory'] = null;
                $programentryDet['open_entry'] = 0;

                //how to get pid
                if ($programentryDet['Mandatory'] == 1) {
                    $pid = 0;
                }
                $programentryDet['pid'] = $pid ?? 0;

                //check existing
                $progEntry = $this->checkProgramEntryRequirementCode($programentryresult_id, $refEntry);

                if ($progEntry) {
                    $IdProgramEntry = $progEntry['IdProgramEntryReq'];
                } else {
                    $IdProgramEntry = $this->lobjprogramentry->addAcademicRequirement($programentryDet);

                }

                $pid = ($main['ERM_COMPULSORY']) ? $IdProgramEntry : 0;

//                echo $main['ERM_REQ_PKG'] . ' - ' . $main['ERM_COMPULSORY'] . ' + ' . $pid . '<br>';

                if ($IdProgramEntry) {
                    //tbl_programentryrequirement_subject
                    $selectDet = $db->select()
                        ->from(array("a" => "migrate_enty_req_det"))
                        ->where('ERD_REQ_ID = ?', $main['ERM_REQ_ID'])
                        ->where('migrated_at is null')
                        ->order('id');

                    $mainDets = $db->fetchAll($selectDet);

                    if ($mainDets) {
                        foreach ($mainDets as $det) {

                            $IdSubject = ($det['ERD_SUBJ_ID'] != 'ANY') ? $this->getSubjectByCode($det['ERD_SUBJ_ID']) : 0;

                            //check existing
                            $checkSubject = $this->checkProgramEntrySubject($IdProgramEntry, $IdSubject);

                            if (!$checkSubject) {
                                $programentrySubj['IdProgramEntryReq'] = $IdProgramEntry;
                                $programentrySubj['IdSubject'] = $IdSubject;
                                $programentrySubj['IdGrade'] = $this->getSubjectGradeByCode($det['ERD_GRD_ID']);
                                $programentrySubj['created_by'] = $auth->getIdentity()->iduser;
                                $programentrySubj['created_at'] = date('Y-m-d H:i:s');

                                $this->lobjprogramentry->addAcademicRequirementSubject($programentrySubj);

                                if ($det['ERD_SUBJ_NO']) {
                                    $this->lobjprogramentry->updateAcademicRequirement(array('subject_no' => $det['ERD_SUBJ_NO']), $IdProgramEntry);
                                }
                            }

                            $where = "id = $det[id]";
                            $db->update('migrate_enty_req_det', array('migrated_at' => date('Y-m-d H:i:s')), $where);

                        }
                    }
                }

                $where = "id = $main[id]";
                $db->update('migrate_entry_req_main', array('migrated_at' => date('Y-m-d H:i:s')), $where);

            }
        }

        exit;

    }

    /*
     * MIGRATION
     */

    public function migrateOeAction()
    {
        $auth = Zend_Auth::getInstance();
        $db = Zend_Db_Table::getDefaultAdapter();

        echo '<pre>';
        $select = $db->select()
            ->from(array("a" => "migrate_entry_req_main_oe"))
//            ->where('ERM_DEGREE = ? ', 'MBA')
            ->where('migrated_at is null')
            //->where('ERM_COMPULSORY is not null')
            ->order('ERM_PROG')
            ->order('ERM_DEGREE')
            ->order('ERM_STRUC')
            ->order('ERM_REQ_PKG')
            ->order('ISNULL(ERM_COMPULSORY)');
//            ->order('ERM_COMPULSORY');
//            ->group('ERM_REQ_PKG')
//            ->group('ERM_COMPULSORY')
//            ->group('ERM_QUA_TYPE');
//            ->limit(100);

        $mainReqs = $db->fetchAll($select);

        if ($mainReqs) {
            foreach ($mainReqs as $main) {

                //tbl_programentry
//                $schemeId = $this->getSchemeByCode($main['ERM_PROG']);
                $schemeId = 1;
                $programId = ($main['ERM_DEGREE'] == 'ALL') ? 0 : $this->getProgramByCode($main['ERM_DEGREE']);
                $programSchemeId = ($main['ERM_STRUC'] == 'ALL') ? 0 : $this->getProgramScheme($programId, $main['ERM_STRUC']);
//                $programSchemeId = $this->getRegistryValue('pg-structure', $main['ERM_STRUC']);

                //check existing
                $programentryresult_id = $this->checkProgramEntry($schemeId, $programId, $programSchemeId);

                if (!$programentryresult_id) {

                    //assuming exist

                    $programentry['IdProgram'] = $programId;
                    $programentry['IdScheme'] = $schemeId;
                    $programentry['IdSchemeOld'] = $main['ERM_PROG'];
                    $programentry['IdProgramScheme'] = $programSchemeId;
                    $programentry['pe_proposal'] = 0;
                    $programentry['pe_url'] = null;
                    $programentry['open_entry'] = 1;
                    $programentry['migrated_at'] = date('Y-m-d H:i:s');
                    $programentry['UpdDate'] = date('Y-m-d H:i:s');
                    $programentry['UpdUser'] = 1;

//                    dd($programentry);exit;

                    $programentryresult_id = $this->lobjprogramentry->fnAddPOEntry($programentry);
                }

                //tbl_programentryrequirement

                $refEntry = $main['ERM_COMPULSORY'];

                $programentryDet['IdProgramEntry'] = $programentryresult_id;
                $programentryDet['ref'] = $refEntry;
                $programentryDet['groupRef'] = $main['ERM_REQ_PKG'];
                $programentryDet['refMain'] = $main['ERM_REQ_ID'];
                $programentryDet['requirement'] = $main['ERM_REQ_PKG_DESC'];
                $programentryDet['requirementMalay'] = $main['ERM_REQ_PKG_DESC_MALAY'];
                $programentryDet['requirementCategory'] = $main['ERM_CATEGORY'];
                $programentryDet['IdSpecialization'] = 0;
                $programentryDet['Item'] = 38; //cgpa
                $programentryDet['Condition'] = 40; // == equal to
                $programentryDet['Value'] = 0;
                $programentryDet['EntryLevel'] = 0;
                $programentryDet['require_au'] = $main['ERM_REQ_AU'];
                $programentryDet['honors'] = 0;
                $programentryDet['Validity'] = 0;
                $programentryDet['Mandatory'] = 1;
                $programentryDet['compulsory'] = ($main['ERM_STATUS_COMP'] == 'Y') ? 1 : 0;
                $programentryDet['open_entry'] = 1;
                $programentryDet['pid'] = 0;

                //check existing

                if ($refEntry)
                    $progEntry = $this->checkProgramEntryRequirementCode($programentryresult_id, $refEntry, 1);

                if (!$progEntry) {
                    $this->lobjprogramentry->addAcademicRequirement($programentryDet);
                }

                $where = "id = $main[id]";
                $db->update('migrate_entry_req_main_oe', array('migrated_at' => date('Y-m-d H:i:s')), $where);

            }
        }

        exit;

    }

    public function getSchemeByCode($code)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("a" => "tbl_scheme"))
            ->where('SchemeCode = ?', $code);

        $result = $db->fetchRow($select);
        return $result['IdScheme'] ?? 0;
    }


    public function getProgramByCode($code)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("a" => "tbl_program"))
            ->where('ProgramCode = ?', $code);

        $result = $db->fetchRow($select);
        return $result['IdProgram'] ?? 0;
    }


    //todo
    public function getProgramScheme($programId, $mode)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

       $select = $db->select()
            ->from(array("a" => "tbl_program_scheme"))
            ->join(array('b' => 'registry_values'), 'b.id = a.program_type', array())
            ->join(array('c' => 'registry_types'), 'b.type_id = c.id', array())
            ->where('a.IdProgram = ?', $programId)
            ->where('c.code = ?', 'pg-structure')
            ->where('b.code = ?', $mode);

        $result = $db->fetchRow($select);
        return $result['IdProgramScheme'] ?? 0;
    }

    public function getRegistryValue($type, $code)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("a" => "registry_values"))
            ->join(array('b' => 'registry_types'), 'b.id = a.type_id', array())
            ->where('b.code = ?', $type)
            ->where('a.code = ?', $code);

        $result = $db->fetchRow($select);
        return $result['id'] ?? 0;
    }

    public function checkProgramEntry($schemeId, $programId, $programSchemeId = 0)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("a" => "tbl_programentry"))
            ->where('IdScheme = ?', $schemeId)
            ->where('IdProgram = ?', $programId)
            ->where('IdProgramScheme = ?', $programSchemeId);

        $result = $db->fetchRow($select);
        return $result['IdProgramEntry'] ?? 0;
    }

    public function checkProgramEntryRequirementCode($id, $ref, $oe = 0)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("a" => "tbl_programentryrequirement"))
            ->where('IdProgramEntry = ?', $id)
            ->where('open_entry = ?', $oe);

        if ($oe == 0) {
            $select->where('ref = ?', $ref);
        }

        $result = $db->fetchRow($select);
        return $result;
    }

    public function checkProgramEntryRequirement($id, $entryLevel, $mandatory)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("a" => "tbl_programentryrequirement"))
            ->where('IdProgramEntry = ?', $id)
            ->where('EntryLevel = ?', $entryLevel);
        //->where('Mandatory = ?', $mandatory);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function checkProgramEntrySubject($id, $IdSubject)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("a" => "tbl_programentryrequirement_subject"))
            ->where('IdProgramEntryReq = ?', $id)
            ->where('IdSubject = ?', $IdSubject);

        $result = $db->fetchRow($select);
        return $result['id'] ?? 0;
    }

    public function getStudyField($code)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("a" => "tbl_studyfield"))
            ->where('sf_id = ?', $code);

        $result = $db->fetchRow($select);
        return $result['id'] ?? 0;
    }

    public function getQualificationByCode($code)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("a" => "tbl_qualification_type"))
            ->where('qt_id = ?', $code);

        $result = $db->fetchRow($select);
        return $result['id'] ?? 0;
    }

    public function getSubjectByCode($code)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("a" => "school_subject"))
            ->where('ss_subject_code = ?', $code);

        $result = $db->fetchRow($select);
        return $result['ss_id'] ?? 0;
    }

    public function getSubjectGradeByCode($code)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array("a" => "tbl_subjectgrade"))
            ->where('sg_grade = ?', $code);

        $result = $db->fetchRow($select);
        return $result['id'] ?? 0;
    }
}
