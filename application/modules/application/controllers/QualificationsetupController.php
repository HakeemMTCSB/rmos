<?php
class Application_QualificationsetupController extends Base_Base {
	private $lobjQualificationsetup;
	private $_gobjlog;
	private $lobjQualificationsetupForm;
	private $lobjqualificationmap;
    private $lobjSubGradeForm;

	public function init() {
		$this->fnsetObj();
        $this->_DbObj = new Application_Model_DbTable_Qualificationsetup();
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->view->translate =Zend_Registry::get('Zend_Translate');
		Zend_Form::setDefaultTranslator($this->view->translate);
	}

	public function fnsetObj(){
		$this->lobjform = new App_Form_Search ();
		$this->lobjQualificationsetupForm = new Application_Form_Qualificationsetup();
		$this->lobjQualificationsetup = new Application_Model_DbTable_Qualificationsetup();
		$this->lobjquasubgradetypemap = new Application_Model_DbTable_QuaSubjectGradeType();
		$this->SubQualification = new Application_Model_DbTable_SubQualification();
        $this->lobjEditQualification = new Application_Form_EditQualification();
        $this->lobjSubGradeForm = new Application_Form_AddSubGrade();
        $this->lobjsubgrade = new Application_Model_DbTable_QuaSubjectGrade();
	}

	//Index Action to search and list Qualification Levels
	public function indexAction() {
		$this->view->lobjform = $this->lobjform;
		$larrresult = $this->lobjQualificationsetup->fngetQualificationDetails (); //get Qualification Details

        $pageCount = 50;

		if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->qualificationpaginatorresult);

		$lintpagecount =$this->gintPageCount;// Definitiontype model

		$lintpage = $this->_getParam('page',1); // Paginator instance
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		if(isset($this->gobjsessionsis->qualificationpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->qualificationpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjQualificationsetup->fnSearchQualification( $this->lobjform->getValues () ); //searching the values for the user

                $this->gobjsessionsis->qualificationpaginatorresult = $larrresult;
                $this->lobjform->populate($larrformData);
                $this->_redirect($this->baseUrl . '/application/qualificationsetup/index/search/1');
			}
		}


        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->qualificationpaginatorresult);
        }else{
            $larrresult = $this->gobjsessionsis->qualificationpaginatorresult;
        }

        $paginator = Zend_Paginator::factory($larrresult);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);

        $totalRecord = $paginator->getTotalItemCount();

        $this->view->paginator = $paginator;
        $this->view->totalRecord = $totalRecord;


	}

	//Action to add new Qualification Level
	public function newqualificationAction(){
		$this->view->lobjQualificationsetupForm = $this->lobjQualificationsetupForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjQualificationsetupForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjQualificationsetupForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

		$this->view->lobjQualificationsetupForm->qt_id->addValidator('Db_NoRecordExists', true, array(
		 'table' => 'tbl_qualification_type',
		 'field' => 'qt_id'
		    )
		 );
		 $this->view->lobjQualificationsetupForm->qt_id->getValidator('Db_NoRecordExists')->setMessage("Record already exists");

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjqualificationForm data from post
			
			unset ( $larrformData ['Save'] );
			if ($this->lobjQualificationsetupForm->isValid ( $larrformData )) {
			    //dd($larrformData);
			    //exit;
				///$IdQualification = $this->lobjQualificationsetup->fnaddQualification($larrformData); //instance for adding the lobjqualificationsetupForm values to DB

                $qualificationMasterDb = new Application_Model_DbTable_Qualificationsetup();
                $id = $qualificationMasterDb->addData($larrformData);

                $rowcount = count($larrformData['sgt_id']);


                if(!empty($larrformData['sgt_id'])){
                                  $rowcount = count($larrformData['sgt_id']);
                                  for($i=0;$i<$rowcount;$i++) {
                                          $mapping = array();
                                          $mapping['sgt_qua_type'] = $id;
                                          $mapping['sgt_id'] = $larrformData['sgt_id'][$i];
                                          $mapping['sgt_desc'] = $larrformData['sgt_desc'][$i];
                                          $mapping['sgt_createby'] = $larrformData['UpdUser'];
                                          $mapping['sgt_createddt'] = $larrformData['UpdDate'];
                                          $mapping['Active'] = 1;
                                          $this->lobjqualificationmap->fnadd($mapping);
                                  }
                                }

                //$this->SubQualification->updateSubs($larrformData['sub_qualifications'], $IdQualification);
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
									  'level' => $priority,
									  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
					                  'time' => date ( 'Y-m-d H:i:s' ),
					   				  'message' => 'New Qualification Add',
									  'Description' =>  Zend_Log::DEBUG,
									  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully added"));
				$this->_redirect( $this->baseUrl . '/application/qualificationsetup/index');
			}
		}
	}

	/*public function editqualificationAction(){

		$this->view->lobjEditQualification = $this->lobjEditQualification; //send the lobjqualificationsetupForm object to the view
		$this->view->lobjEditQualification->qt_eng_desc->addValidator('Db_NoRecordExists', true, array(
						'table' => 'tbl_qualification_type',
						'field' => 'qt_eng_desc',
                        'exclude' => array(
                            'field' => 'id',
                            'value' => $this->_getParam('id', 0)
                        )

					)
				);
		$this->view->lobjEditQualification->qt_eng_desc->getValidator('Db_NoRecordExists')->setMessage("Record already exists");

		$IdQualification = $this->_getParam('id', 0);
		$result = $this->lobjQualificationsetup->fetchAll('IdQualification ='.$IdQualification);
		$result = $result->toArray();

		foreach ($result as $qualificationresult) {
			$this->lobjQualificationsetupForm->populate($qualificationresult);
		}

		$this->view->mappings = $this->lobjqualificationmap->fngetmappings($IdQualification);
		$this->view->sub_qualifications = $this->SubQualification->getByQualification($IdQualification); 

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjQualificationsetupForm->IdQualification->setValue( $result[0]['IdQualification'] );
		$this->view->lobjQualificationsetupForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjQualificationsetupForm->UpdUser->setValue( $auth->getIdentity()->iduser);

		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			if ($this->lobjQualificationsetupForm->isValid($formData)) {
				$lintIdQualification= $formData ['IdQualification'];
				$this->lobjqualificationmap->fndeletemappings($lintIdQualification);
				$this->lobjQualificationsetup->fnupdateQualification($formData,$lintIdQualification);//update Qualification
                                if(!empty($formData['IdSubject'])){
                                  $rowcount = count($formData['IdSubject']);
                                  for($i=0;$i<$rowcount;$i++) {
                                          $mapping = array();
                                          $mapping['IdQualification'] = $lintIdQualification;
                                          $mapping['IdSubject'] = $formData['IdSubject'][$i];
                                          $this->lobjqualificationmap->fnadd($mapping);
                                  }
                                }

                $this->SubQualification->updateSubs($formData['sub_qualifications'], $formData ['IdQualification']);

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Qualification Edit Id=' . $IdQualification,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				//$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'semester', 'action'=>'index'),'default',true));
				$this->_redirect( $this->baseUrl . '/application/qualificationsetup/index');
			}
		}
	}*/

    public function editAction(){

        $this->view->lobjEditQualification = $this->lobjEditQualification;

        $ldtsystemDate = date ( 'Y-m-d H:i:s' );
        $this->view->lobjEditQualification->UpdDate->setValue ( $ldtsystemDate );
        $auth = Zend_Auth::getInstance();
        $this->view->lobjEditQualification->UpdUser->setValue ( $auth->getIdentity()->iduser);


        $id = $this->_getParam('id', 0);
        $this->view->id = $id;

        //title
        $this->view->title= $this->view->translate("Qualification")." - ".$this->view->translate("Edit");



        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($this->lobjEditQualification->isValid($formData)) {
                //dd($formData);
               // exit;

                $idqua = $formData["idqua"];

                $this->_DbObj->updateData($formData, $idqua);
                $this->lobjquasubgradetypemap->fndeletemappings($idqua);

                $rowcount = count($formData['sgt_id']);

                for ($i = 0; $i < $rowcount; $i++) {
                    $mapping = array();
                    $mapping['sgt_id'] = $formData['sgt_id'][$i];
                    $mapping['sgt_desc'] = $formData['sgt_desc'][$i];
                    $mapping['sgt_qua_type'] = $idqua;
                    $mapping['sgt_modifyby'] = $formData['UpdUser'];
                    $mapping['sgt_modifydate'] = $formData['UpdDate'];
                    $mapping['Active'] = 1;

                    //dd($mapping);
                    //exit;


                    //if ($formData['IdSubject']!='0' || $formData['IdSubject']!='') {
                    if (!empty($formData['sgt_id'])) {


                        $this->lobjquasubgradetypemap->fnadd($mapping);
                    }
                }


                $this->_redirect($this->view->url(array('module' => 'application', 'controller' => 'qualificationsetup', 'action' => 'detail', 'id' => $idqua), 'default', true));
                /*}else{
                    $this->lobjEditSchoolSubject->populate($formData); */
            }
        } else {
            if ($id > 0) {

                //$sgtDetails = $this->_DbObj->getData($id);
//                $quaDetailDB = new Application_Model_DbTable_Qualificationsetup();
//                $sgtDetails = $quaDetailDB->getData($id);
//                $this->view->sgtDetails= $sgtDetails;
//
//                if($sgtDetails["id"]){
//
//                    $qt_level=$sgtDetails["GradeDesc"];
//                    $qt_prog_type=$sgtDetails["desctype"];
//                    echo $qt_level;
//                    echo $qt_prog_type;
//                }
//
//                $this->lobjEditQualification->qt_prog_type->setvalue($qt_prog_type);
//                $this->lobjEditQualification->qt_prog_type->setAttrib('disabled','disabled');
//                $this->lobjEditQualification->qt_level->setValue($qt_level);
//                $this->lobjEditQualification->qt_level->setAttrib('disabled','disabled');
                $this->lobjEditQualification->populate($this->_DbObj->getData($id));


                $sgtresultDetails = $this->lobjquasubgradetypemap->fngetmappings($id);
                $this->view->sgtresultDetails = $sgtresultDetails;
            }
        }

        //$this->view->form = $form;
    }

    public function detailAction(){
        $this->view->title= $this->view->translate("Qualification")." - ".$this->view->translate("Detail");

        $this->view->searchForm = new Application_Form_QualificationSearch();

        $id = $this->_getParam('id', null);
        $this->view->id = $id;


        if($id) {
            //subject data
            $result = $this->_DbObj->getData($id);
            $this->view->paginator = array($result);

            if ($this->getRequest()->isPost()) {

                $formData = $this->getRequest()->getPost();

                //paginator
                $data = $this->_DbObj->getPaginateData($formData);

                $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
                $paginator->setItemCountPerPage(PAGINATION_SIZE);
                $paginator->setCurrentPageNumber($this->_getParam('page',1));
                $totalRecord = $paginator->getTotalItemCount();

                $this->view->paginator = $paginator;
                $this->view->totalRecord = $totalRecord;


            }

        }

        else{
            $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'highschool-subject', 'action'=>'index'),'default',true));
        }
    }

    public function viewsubgradetypeAction(){
        $this->view->title= $this->view->translate("Qualification")." - ".$this->view->translate("Detail");

        //$this->view->searchForm = new Application_Form_QualificationSearch();

        $id = $this->_getParam('quaid', null);
        $this->view->id = $id;


        if($id) {
            //subject data
            /*$result = $this->_DbObj->getData($id);
            $this->view->paginator = array($result);*/



            $quaModel = new Application_Model_DbTable_Qualificationsetup();
            $quaInfo = $quaModel->getData($id);
            $this->view->quaInfo = $quaInfo;

            $sgtModel = new Application_Model_DbTable_QuaSubjectGradeType();
            $sgtInfo = $sgtModel->getDatabyType($id);
            $this->view->sgtInfo = $sgtInfo;

           // $this->view->location_list = $this->regLocDB->getLocationList($IdIntake);

        }

        else{
            $this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'highschool-subject', 'action'=>'index'),'default',true));
        }
    }

    public function addsubgradeAction(){
        $this->view->lobjSubGradeForm = $this->lobjSubGradeForm;
        $ldtsystemDate = date ( 'Y-m-d H:i:s' );
        $this->view->lobjSubGradeForm->UpdDate->setValue ( $ldtsystemDate );
        $auth = Zend_Auth::getInstance();
        $this->view->lobjSubGradeForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

        $quaid = $this->_getParam('quaid', 0);
        $this->view->quaid = $quaid;
        $id = $this->_getParam('id', 0);
        $this->view->id = $id;


        if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
            $larrformData = $this->_request->getPost (); //getting the values of lobjqualificationForm data from post

            unset ( $larrformData ['Save'] );
            if ($this->lobjSubGradeForm->isValid ( $larrformData )) {
                /*dd($larrformData);
                echo $id;
                echo $quaid;
                exit;*/

               /* $subgradeMasterDb = new Application_Model_DbTable_QuaSubjectGrade();
                $id = $subgradeMasterDb->addData($larrformData);*/

                $rowcount = count($larrformData['sg_grade']);


                if(!empty($larrformData['sg_grade'])){
                    $this->lobjsubgrade->fndeletemappings($id);
                    $rowcount = count($larrformData['sg_grade']);
                    for($i=0;$i<$rowcount;$i++) {
                        $mapping = array();
                        $mapping['sg_type'] = $larrformData['id'];
                        $mapping['sg_grade'] = $larrformData['sg_grade'][$i];
                        $mapping['sg_grade_desc'] = $larrformData['sg_grade_desc'][$i];
                        $mapping['sg_grade_point'] = $larrformData['sg_grade_point'][$i];
                        $mapping['sg_mohe_code'] = $larrformData['sg_mohe_code'][$i];
                        $mapping['sg_createby'] = $larrformData['UpdUser'];
                        $mapping['sg_createdate'] = $larrformData['UpdDate'];
                        $mapping['Active'] = 1;
                        $this->lobjsubgrade->fnadd($mapping);
                    }
                }

                //$this->SubQualification->updateSubs($larrformData['sub_qualifications'], $IdQualification);
                // Write Logs
                $priority=Zend_Log::INFO;
                $larrlog = array ('user_id' => $auth->getIdentity()->iduser,
                    'level' => $priority,
                    'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'time' => date ( 'Y-m-d H:i:s' ),
                    'message' => 'Add subject grade',
                    'Description' =>  Zend_Log::DEBUG,
                    'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
                $this->_gobjlog->write ( $larrlog ); //insert to tbl_log

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully added"));
                //$this->_redirect( $this->baseUrl . '/application/qualificationsetup/viewsubgradetype/id/');
                $this->_redirect($this->view->url(array('module'=>'application','controller'=>'qualificationsetup', 'action'=>'viewsubgradetype', 'id'=>$id, 'quaid'=>$quaid),'default',true));
            }
        } else {
            if ($id > 0) {

                $sgresultDetails = $this->lobjsubgrade->fngetmappings($id);
                $this->view->sgresultDetails = $sgresultDetails;
            }
        }
    }

	public function deletequalificationAction() {
		$this->view->lobjQualificationsetupForm = $this->lobjQualificationsetupForm;
		//$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$IdQualification = $this->_getParam('id', 0);
		$larrDelete = $this->lobjQualificationsetup->fnDeleteQualification($IdQualification);
		echo "1";
		$this->_redirect( $this->baseUrl . '/application/qualificationsetup/index');
	}

    public function getQualificationTypeListAction() {
        $definition = new App_Model_General_DbTable_Definationms();
        $definitions = $definition->getByCode('Qualification Type');
        echo json_encode($definitions);
        exit;
    }

    public function getProgramDataAction(){


        $idProgramtype = $this->_getParam('idProgram', 0);
        //$type = $this->_getParam('type', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();


            $programlevelDB = new GeneralSetup_Model_DbTable_Awardlevel();
            $rows=$programlevelDB->getLevelByProgramType($idProgramtype);

            $row = array();
            foreach($rows as $index=>$r){
                $row[$index]['key']=$r['Id'];
                $row[$index]['name']=$r['GradeDesc'];
            }


        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();


        $json = Zend_Json::encode($row);

        echo $json;
        exit();
    }



}


