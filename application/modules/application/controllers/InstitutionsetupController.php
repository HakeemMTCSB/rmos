<?php
class Application_InstitutionsetupController extends Base_Base {
	private $lobjInstitutionsetup;
	private $_gobjlog;
	private $lobjInstitutionsetupForm;
	private $lobjinstitutionsubmap;

	public function init() {
		$this->fnsetObj();
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->view->translate =Zend_Registry::get('Zend_Translate');
		Zend_Form::setDefaultTranslator($this->view->translate);
	}
	public function fnsetObj(){
		$this->lobjform = new App_Form_Search ();
		$this->lobjInstitutionsetupForm = new Application_Form_Institutionsetup();
		$this->lobjInstitutionsetup = new Application_Model_DbTable_Institutionsetup();
		$this->lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
		$this->lobjinstitutionsubmap = new Application_Model_DbTable_InstitutionsubjectMapping();
        $this->lobjRegistryvalue = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
	}

	//Index Action to search and list Institution
	public function indexAction() {
        //$pageCount = 50;
		$this->view->lobjform = $this->lobjform;
		$larrresult = $this->lobjInstitutionsetup->fngetInstitutionDetails (); //get Institution Details

        $pageCount = 50;

                //load country
                $lobjUser = new GeneralSetup_Model_DbTable_User();
                $lobjcountry = $lobjUser->fnGetCountryList();
                $this->view->lobjform->field29->addMultiOptions($lobjcountry);;

		$lintpage = $this->_getParam('page',1); // Paginator instance
		$lobjPaginator = new App_Model_Common(); // Definitiontype model

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();

			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjInstitutionsetup->fnSearchInstitution( $this->lobjform->getValues () ); //searching the values for the user


                $this->gobjsessionsis->institutionpaginatorresult = $larrresult;
                $this->lobjform->populate($larrformData);
                $this->_redirect($this->baseUrl . '/application/institutionsetup/index/search/1');


			}
		}

        if(!$this->_getParam('search')){
            unset($this->gobjsessionsis->institutionpaginatorresult);
        }else{
            $larrresult = $this->gobjsessionsis->institutionpaginatorresult;
        }

        $paginator = Zend_Paginator::factory($larrresult);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($pageCount);

        $totalRecord = $paginator->getTotalItemCount();

        $this->view->paginator = $paginator;
        $this->view->totalRecord = $totalRecord;


    }

	//Action to add new Institution
	public function newinstitutionAction(){
		$this->view->lobjInstitutionsetupForm = $this->lobjInstitutionsetupForm;
		$lobjcountry = $this->lobjUser->fnGetCountryList();
		$this->lobjInstitutionsetupForm->Country->addMultiOptions($lobjcountry);
        $lobjinstype = $this->lobjRegistryvalue->getList('7');
        ///$lobjinstype = $this->lobjUser->fnGetInstitutionType();
        $this->lobjInstitutionsetupForm->InstitutionType->addMultiOptions($lobjinstype);
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjInstitutionsetupForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjInstitutionsetupForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$this->view->lobjInstitutionsetupForm->InstitutionName->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_institution',
				'field' => 'InstitutionName'
		)
		);
		$this->view->lobjInstitutionsetupForm->InstitutionName->getValidator('Db_NoRecordExists')->setMessage("Record already exists");
		$this->view->lobjInstitutionsetupForm->InstitutionCode->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_institution',
				'field' => 'InstitutionCode'
		)
		);
		$this->view->lobjInstitutionsetupForm->InstitutionCode->getValidator('Db_NoRecordExists')->setMessage("Record already exists");


		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjInstitutionsetupForm data from post
			unset ( $larrformData ['Save'] );
			if ($this->lobjInstitutionsetupForm->isValid ( $larrformData )) {
				$IdInstitution = $this->lobjInstitutionsetup->fnaddInstitution($larrformData); //instance for adding the lobjInstitutionsetupForm values to DB
				$rowcount = count($larrformData['IdSubject']);
				for($i=0;$i<$rowcount;$i++) {
					$mapping = array();
					$mapping['IdInstitution'] = $IdInstitution;
					$mapping['IdSubject'] = $larrformData['IdSubject'][$i];
					$this->lobjinstitutionsubmap->fnadd($mapping);
				}
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'New Institution Add',
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

                $this->_helper->flashMessenger->addMessage(array('success' => "Successfully added"));
				$this->_redirect( $this->baseUrl . '/application/institutionsetup/index');
			}
		}
	}

	//Action to update and list the Institution details
	public function editinstitutionAction(){

		$this->view->lobjInstitutionsetupForm = $this->lobjInstitutionsetupForm; //send the lobjInstitutionsetupForm object to the view
		$this->view->lobjInstitutionsetupForm->InstitutionName->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_institution',
				'field' => 'InstitutionName',
				'exclude' => array(
						'field' => 'idInstitution',
						'value' => $this->_getParam('id', 0)
				)
		)
		);
		$this->view->lobjInstitutionsetupForm->InstitutionCode->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_institution',
				'field' => 'InstitutionCode',
				'exclude' => array(
						'field' => 'idInstitution',
						'value' => $this->_getParam('id', 0)
				)
		)
		);
		$lobjcountry = $this->lobjUser->fnGetCountryList();
		//asd($lobjcountry,false);
		$this->lobjInstitutionsetupForm->Country->addMultiOptions($lobjcountry);
		$IdInstitution = $this->_getParam('id', 0);
		$institutionresult = $this->lobjInstitutionsetup->get_details($IdInstitution);

		$this->view->mappings = $this->lobjinstitutionsubmap->fngetmappings($IdInstitution);
		/*
		$lobjstate = $this->lobjUser->fnGetStateListcountry($institutionresult['Country']);
		$this->view->IdCountry = $institutionresult['Country'];
		$this->lobjInstitutionsetupForm->State->addMultiOptions($lobjstate);
		$lobjCommonModel = new App_Model_Common();
		$larrStateCityList = $lobjCommonModel->fnGetCityList($institutionresult['State']);
		$this->lobjInstitutionsetupForm->City->addMultiOptions($larrStateCityList);
        */

		$this->lobjInstitutionsetupForm->populate($institutionresult);
		$this->view->institution = $institutionresult;
		//dd($institutionresult);
		//			$this->view->lobjInstitutionsetupForm->State->setValue( $institutionresult['State'] );
		$arrPhone = explode("-",$institutionresult ['InstitutionPhoneNumber']);
		if(isset($arrPhone[0]) && $arrPhone[0] != ''){
			$this->view->lobjInstitutionsetupForm->Phonecountrycode->setValue ( $arrPhone [0] );
		}
		if(isset($arrPhone[1]) && $arrPhone[1] != ''){
			$this->view->lobjInstitutionsetupForm->Phonestatecode->setValue ( $arrPhone [1] );
		}
		if(isset($arrPhone[2]) && $arrPhone[2] != ''){
			$this->view->lobjInstitutionsetupForm->InstitutionPhoneNumber->setValue ( $arrPhone [2] );
		}

		$arrfax = explode("-",$institutionresult ['InstitutionFaxNumber']);
		if(isset($arrfax[0]) && $arrPhone[0] != ''){
			$this->view->lobjInstitutionsetupForm->faxcountrycode->setValue ( $arrfax[0] );
		}
		if(isset($arrfax[1]) && $arrPhone[1] != ''){
			$this->view->lobjInstitutionsetupForm->faxstatecode->setValue ( $arrfax[1] );
		}
		if(isset($arrfax[2]) && $arrPhone[2] != ''){
			$this->view->lobjInstitutionsetupForm->InstitutionFaxNumber->setValue ( $arrfax[2] );
		}
		$this->view->lobjInstitutionsetupForm->IdInstitution->setValue( $institutionresult['idInstitution'] );

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjInstitutionsetupForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjInstitutionsetupForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		if ($this->getRequest()->isPost()) {
			if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
				$formData = $this->_request->getPost (); //getting the values of lobjactivityForm data from post
				unset ( $formData ['Save'] );
			}
			if ($this->lobjInstitutionsetupForm->isValid($formData)) {
				$lintIdInstitution = $formData ['IdInstitution'];
				$this->lobjInstitutionsetup->fnupdateInstitution($formData,$lintIdInstitution);//update Institution
				$this->lobjinstitutionsubmap->fndeletemappings($lintIdInstitution);
				$rowcount = count($formData['IdSubject']);
				for($i=0;$i<$rowcount;$i++) {
					$mapping = array();
					$mapping['IdInstitution'] = $IdInstitution;
					$mapping['IdSubject'] = $formData['IdSubject'][$i];
					$this->lobjinstitutionsubmap->fnadd($mapping);
				}

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Institution Edit Id=' . $IdInstitution,
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				//$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'semester', 'action'=>'index'),'default',true));
				$this->_redirect( $this->baseUrl . '/application/institutionsetup/index');
			}
		}
	}

	public function editmainAction(){

		$this->view->lobjInstitutionsetupForm = $this->lobjInstitutionsetupForm; //send the lobjInstitutionsetupForm object to the view
		$this->view->lobjInstitutionsetupForm->InstitutionName->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_institution',
				'field' => 'InstitutionName',
				'exclude' => array(
						'field' => 'idInstitution',
						'value' => $this->_getParam('id', 0)
				)
		)
		);
		$this->view->lobjInstitutionsetupForm->InstitutionCode->addValidator('Db_NoRecordExists', true, array(
				'table' => 'tbl_institution',
				'field' => 'InstitutionCode',
				'exclude' => array(
						'field' => 'idInstitution',
						'value' => $this->_getParam('id', 0)
				)
		)
		);

        $lobjinstype = $this->lobjUser->fnGetInstitutionType();
        $this->lobjInstitutionsetupForm->InstitutionType->addMultiOptions($lobjinstype);

		$lobjcountry = $this->lobjUser->fnGetCountryList();
		//asd($lobjcountry,false);
                $this->lobjInstitutionsetupForm->Country->addMultiOption('', 'Select');
		$this->lobjInstitutionsetupForm->Country->addMultiOptions($lobjcountry);
		$IdInstitution = $this->_getParam('id', 0);
		$institutionresult = $this->lobjInstitutionsetup->get_details($IdInstitution);

		/*
                //var_dump($institutionresult); exit;
                $lobjstate = $this->lobjUser->fnGetStateListcountry($institutionresult['Country']);
		$this->view->IdCountry = $institutionresult['Country'];
                $this->lobjInstitutionsetupForm->State->addMultiOption('', 'Select');
		$this->lobjInstitutionsetupForm->State->addMultiOptions($lobjstate);
		$lobjCommonModel = new App_Model_Common();
		$larrStateCityList = $lobjCommonModel->fnGetCityList($institutionresult['State']);
                $this->lobjInstitutionsetupForm->City->addMultiOption('', 'Select');
		$this->lobjInstitutionsetupForm->City->addMultiOptions($larrStateCityList);
		*/

		$this->view->lobjInstitutionsetupForm->populate($institutionresult);
		$this->view->institution = $institutionresult;
		//dd($institutionresult);
		//			$this->view->lobjInstitutionsetupForm->State->setValue( $institutionresult['State'] );
		$arrPhone = explode("-",$institutionresult ['InstitutionPhoneNumber']);
		if(isset($arrPhone[0]) && $arrPhone[0] != ''){
			$this->view->lobjInstitutionsetupForm->Phonecountrycode->setValue ( $arrPhone [0] );
		}
		if(isset($arrPhone[1]) && $arrPhone[1] != ''){
			$this->view->lobjInstitutionsetupForm->Phonestatecode->setValue ( $arrPhone [1] );
		}
		if(isset($arrPhone[2]) && $arrPhone[2] != ''){
			$this->view->lobjInstitutionsetupForm->InstitutionPhoneNumber->setValue ( $arrPhone [2] );
		}

		$arrfax = explode("-",$institutionresult ['InstitutionFaxNumber']);
		if(isset($arrfax[0]) && $arrPhone[0] != ''){
			$this->view->lobjInstitutionsetupForm->faxcountrycode->setValue ( $arrfax[0] );
		}
		if(isset($arrfax[1]) && $arrPhone[1] != ''){
			$this->view->lobjInstitutionsetupForm->faxstatecode->setValue ( $arrfax[1] );
		}
		if(isset($arrfax[2]) && $arrPhone[2] != ''){
			$this->view->lobjInstitutionsetupForm->InstitutionFaxNumber->setValue ( $arrfax[2] );
		}
		$this->view->lobjInstitutionsetupForm->IdInstitution->setValue( $institutionresult['idInstitution'] );

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjInstitutionsetupForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjInstitutionsetupForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		if ($this->getRequest()->isPost()) {
			if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
				$formData = $this->_request->getPost (); //getting the values of lobjactivityForm data from post
				unset ( $formData ['Save'] );
			}
			if ($this->lobjInstitutionsetupForm->isValid($formData)) {
				$lintIdInstitution = $formData ['IdInstitution'];
				$this->lobjInstitutionsetup->fnupdateInstitution($formData,$lintIdInstitution);//update Institution

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Institution Edit Id=' . $IdInstitution,
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				//$this->_redirect($this->view->url(array('module'=>'generalsetup','controller'=>'semester', 'action'=>'index'),'default',true));
				$this->_redirect( $this->baseUrl . '/application/institutionsetup/index');
			}
		}
	}

	public function deleteinstitutionAction() {
		$this->view->lobjInstitutionsetupForm = $this->lobjInstitutionsetupForm;
		//$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$idInstitution = $this->_getParam('id', 0);
		$larrDelete = $this->lobjInstitutionsetup->fnDeleteInstitution($idInstitution);
		echo "1";
		$this->_redirect( $this->baseUrl . '/application/institutionsetup/index');
	}
}