<?php
class Application_ProgramchecklistController extends Base_Base {
	private $locale;
	private $registry;
	private $Programchecklistform;
	private $Programchecklist;
	private $_gobjlog;

	public function init() {
		$this->fnsetObj();
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}

	public function fnsetObj(){
		$this->Programchecklistform = new Application_Form_Programchecklist();
		$this->Programchecklist = new Application_Model_DbTable_Programchecklist();
		$this->lobjdeftype = new App_Model_Definitiontype();
	}

	//Index Action
	public function indexAction() 
	{
		$this->view->locale = $this->locale;
		$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
		$this->view->lobjform = $this->lobjform;

		$larrresult = $this->Programchecklist->fnGetProgramDetails();

		if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->Checklistpaginatorresult);

		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->Checklistpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->Checklistpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}

		//Maintenance Search
		if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrFormData = $this->_request->getPost();
			if ($this->lobjform->isValid($larrFormData)) {
				$larrResult = $this->Programchecklist->fnSearchProgram($this->lobjform->getValues ());
				$this->view->paginator = $this->lobjCommon->fnPagination($larrResult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Checklistpaginatorresult = $larrResult;

			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'maintenance', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/application/programchecklist/index');

		}
	}

	public function programchecklistAction() { //Action for the updation and view of the  details
		
		$this->view->lobjProgramchecklistform= $this->Programchecklistform;

		$larrChecklIstType = $this->lobjdeftype->fnGetDefinationMs('Checklist Type');
		$this->view->larrChecklIstType = $larrChecklIstType;
		foreach($larrChecklIstType as $larrprgmvalues) {
			$this->view->lobjProgramchecklistform->ChecklistType->addMultiOption($larrprgmvalues['key'],$larrprgmvalues['value']);
		}


		$lintIdProgram = ( int ) $this->_getParam ( 'id' );
		$this->view->Idprogram = $lintIdProgram;
		$this->view->lobjProgramchecklistform->IdProgram->setValue($lintIdProgram);
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjProgramchecklistform->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjProgramchecklistform->UpdUser->setValue( $auth->getIdentity()->iduser);
		$priority = new Cms_PriorityCheck();
		$this->view->lobjProgramchecklistform->Priority->addValidator($priority);
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$larrprogram = $this->Programchecklist->fngetprograms();
		$this->view->lobjProgramchecklistform->CopyFrom->addMultiOptions($larrprogram);
		$this->view->larrresult=$larrresult = $this->Programchecklist->fnViewProgramchecklist($lintIdProgram);
		$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		
		if(isset($lintIdProgram) && $lintIdProgram!=''){
			$result = $this->Programchecklist->fngetprogramname($lintIdProgram);
			if(count($result)>0){
				$this->view->lobjProgramchecklistform->ProgramName->setValue($result[0]['ProgramCode'].' - '.$result[0]['ProgramName']);
			}
		}
		
		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrFormData = $this->_request->getPost();			
			if ($this->Programchecklistform->isValid($larrFormData)) {
				unset($larrFormData['Save']);
				$this->Programchecklist->fnaddChecklistname($larrFormData);
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Program Check List add Id=' . $lintIdProgram,
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/application/programchecklist/programchecklist/id/'.$larrFormData['IdProgram']);
			}

		}

	}

	public function programchecklistdeleteAction(){
		$id = ( int ) $this->_getParam ( 'id' );
		$checkListId = ( int ) $this->_getParam ( 'checklistid' );
		$ret =  $this->Programchecklist->deleteprogramchecklist($id);
		$this->_redirect( $this->baseUrl . '/application/programchecklist/programchecklist/id/'.$checkListId);
	}

	public function checkexistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$id = ( int ) $this->_getParam ( 'id' );
		$checkregistered = $this->Programchecklist->fncheckregistered($id);
		if(count($checkregistered)<=0){
			$data = array('key'=>'false','value'=>'false');
		}
		else{
			$data = array('key'=>'true','value'=>'true');
		}
		echo Zend_Json_Encoder::encode($data);
		die;
	}

	public function programchecklisteditAction() { //Action for the updation and view of the  details

		$this->view->lobjProgramchecklistform= $this->Programchecklistform;
		$larrChecklIstType = $this->lobjdeftype->fnGetDefinationMs('Checklist Type');
		$this->view->larrChecklIstType = $larrChecklIstType;
		foreach($larrChecklIstType as $larrprgmvalues) {
			$this->view->lobjProgramchecklistform->ChecklistType->addMultiOption($larrprgmvalues['key'],$larrprgmvalues['value']);
		}

		$lintIdProgram = ( int ) $this->_getParam ( 'id' );
		$this->view->Idprogram = $lintIdProgram;

		$lintIdCheckList = ( int ) $this->_getParam ( 'IdCheckList' );
		$this->view->IdCheckList = $lintIdCheckList;
		$this->view->lobjProgramchecklistform->IdCheckList->setValue($lintIdCheckList);
		$auth = Zend_Auth::getInstance();
		
		$larrEditResult = $this->Programchecklist->fnviewChecklistDtls($lintIdCheckList);
		$this->view->result = $larrEditResult;
		
		$this->Programchecklistform->populate($larrEditResult);
		
		$priority = new Cms_PriorityCheck();
		$this->view->lobjProgramchecklistform->Priority->addValidator($priority);
		if(isset($lintIdProgram) && $lintIdProgram!=''){
			$result = $this->Programchecklist->fngetprogramname($lintIdProgram);
			if(count($result)>0){
			
				$this->view->lobjProgramchecklistform->ProgramName->setValue($result[0]['ProgramCode'].' - '.$result[0]['ProgramName']);
			}
		}
		//$this->view->lobjProgramchecklistform->ChecklistType->addValidator($priority);
		//$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		//$this->view->lobjProgramchecklistform->UpdDate->setValue( $ldtsystemDate );
		//$auth = Zend_Auth::getInstance();
		//$this->view->lobjProgramchecklistform->UpdUser->setValue( $auth->getIdentity()->iduser);

		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		$larrresult = $this->Programchecklist->fnViewProgramchecklist($lintIdProgram);
		$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		
	

		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrFormData = $this->_request->getPost();
			if ($this->Programchecklistform->isValid($larrFormData)) {
				unset($larrFormData['Save']);
				$result=$this->Programchecklist->fnupdateChecklistDtls($lintIdCheckList,$larrFormData );

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Program Check List edit Id=' . $lintIdProgram,
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/application/programchecklist/programchecklist/id/'.$larrFormData['IdProgram']);
			}

		}

	}
	/**
	 *
	 * Action to copy checklist of a program to other program
	 * Author: Dushyant Sharma
	 */
	public function copyfromprogramAction(){
		$IdSource = $this->_getParam('IdProgram');
		$auth = Zend_Auth::getInstance();
		if($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			$this->Programchecklist->fncopychecklists($IdSource,$formData['CopyFrom']);

			// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
						'level' => $priority,
						'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
						'time' => date ( 'Y-m-d H:i:s' ),
						'message' => 'Program Check List edit Id=' . $IdSource,
						'Description' =>  Zend_Log::DEBUG,
						'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/application/programchecklist/programchecklist/id/'.$IdSource);
		}
	}

}
