<?php 
class Application_NopesController extends Base_Base {
	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	public function indexAction() {
		//title
    	$this->view->title="USM : No Fomulir Setup";   
    	$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
    	$lobjintake = new GeneralSetup_Model_DbTable_Intake();
    	$larrresult = $lobjintake->fngetIntakeList();
    	
		unset($this->gobjsessionsis->intakepaginatorresult);
		$lintpagecount =$this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$lobjPaginator = new App_Model_Common(); // Definitiontype model

		if(isset($this->gobjsessionsis->intakepaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->intakepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}		
	}
	
	public function intakenopesAction(){
    	 
    	$idintake = $this->_getparam("idintake",0);
    	$intakedb = new GeneralSetup_Model_DbTable_Intake();
    	$intakedetail=$intakedb->fngetIntakeDetails($idintake);
    	$this->view->title="USM : No Fomulir by Intake (Online) - ".$intakedetail[0]["IntakeDefaultLanguage"];  
    	$ptbdb = new Application_Model_DbTable_Pintobank();
    	$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;	
    	if ($this->_request->isPost()) {
	    	for($i=$_POST["startid"];$i<=$_POST["endid"];$i++){
				$runnumber= $_POST["head"].str_pad($i, 5, "0", STR_PAD_LEFT);
	    		$data["billing_no"]=$runnumber;
	    		$data["PAYEE_ID"]=$runnumber;
	    		$data["intakeId"]=$idintake;
	    		$data["REGISTER_NO"]=strtoupper(substr(md5($runnumber.date("YmdHis")),0,10));
	    		//print_r($data);
	    		$ptbdb->addData($data);
	    	}
    	}	
    	$larrresult = $ptbdb->fngetNomorList($idintake);
    	if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->nomorpaginatorresult);
		
		$lintpagecount =50;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$lobjPaginator = new App_Model_Common(); // Definitiontype model

		if(isset($this->gobjsessionsis->nomorpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->nomorpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
    	$form_csv = new Application_Form_NopesCsvUpload(array('intakeval'=>$idintake));
    	$this->view->form_csv = $form_csv;    	
	}

	public function intakenopesagentAction(){
		$idintake = $this->_getparam("idintake",0);
    	$intakedb = new GeneralSetup_Model_DbTable_Intake();
    	$intakedetail=$intakedb->fngetIntakeDetails($idintake);
    	$this->view->title="USM : No Fomulir by Intake (Agent)- ".$intakedetail[0]["IntakeDefaultLanguage"]; 	
    	$ptbdb = new Application_Model_DbTable_AgentFormNumber();
    	if ($this->_request->isPost()) {
    		for($i=$_POST["startid"];$i<=$_POST["endid"];$i++){
				$runnumber= $_POST["head"].str_pad($i, 5, "0", STR_PAD_LEFT);
    			$data["afn_form_no"]=$runnumber;
    			$data["afn_taken_status"]=0;
    			$data["afn_intake"]=$idintake;
    			$data["afn_type"]=2;
    			
    			$ptbdb->addData($data);
    		}
    	}
    	
    	$this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;	
		
    	$larrresult = $ptbdb->fngetNomorList($idintake);
    	if(!$this->_getParam('search'))
			unset($this->gobjsessionsis->nomorpaginatorresult);
		
		$lintpagecount =50;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$lobjPaginator = new App_Model_Common(); // Definitiontype model

		if(isset($this->gobjsessionsis->nomorpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->nomorpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
    	//$form_csv = new Application_Form_NopesCsvUpload(array('intakeval'=>$idintake));
    	//$this->view->form_csv = $form_csv;    	
	}
	public function uploadCsvAction(){
		
		
		$save = $this->_getparam("save",null);
		
		//title
		
    	$this->view->title= $this->view->translate("No Fomulir - By CSV ");
    	
    	$form = new Application_Form_NopesCsvUpload();
    	
    	if ($this->_request->isPost()) {
    		$ptbdb = new Application_Model_DbTable_Pintobank();
       		$formData = $this->_request->getPost();
       		
       		//echo $formData["scid"];
       		if($formData["intake"]==""){
       			$this->_redirect($this->view->url(array('module'=>'application','controller'=>'nopes', 'action'=>'index'),'default',true));
       		}
       		
       		$this->view->remove_header = $formData['remove_header']==1?true:false;

            // success - do something with the uploaded file
            $uploadedData = $form->getValues();
            $fullFilePath = $form->file->getFileName();
            
            $header_skip = $this->view->remove_header;
            $data = array();
            $data_mapped = array();
            if (($handle = fopen($fullFilePath, 'r')) !== FALSE)
            {
            	$i=0;
            	while (($row = fgetcsv($handle, 1000, ",")) !== FALSE)
            	{
            		
            		$data=array(
            			"billing_no" => $row[0],
            			"PAYEE_ID" => $row[0],
            			"ADDRESS_1" => $row[1],
            			"BILL_REF_1" => $row[2],
            			"BILL_REF_2" => $row[3],
            			"BILL_REF_3" => $row[4],
            			"AMOUNT_TOTAL" => $row[5],
            			"REGISTER_NO" => $row[6],
            			"bank" => "BNI",
            			"status" => "E",
            			"intakeId" => $formData["intake"]
            		);

            		$ptbdb->addData($data);
            	}	
            }
    	}
	}  
}
?>