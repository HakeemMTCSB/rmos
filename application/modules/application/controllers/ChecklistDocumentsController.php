<?php
ini_set('max_execution_time', -1); //forever

class Application_ChecklistDocumentsController extends Base_Base
{
	public function init()
	{
		$this->locale = Zend_Registry::get('Zend_Locale');
	}

	public function indexAction(){
	
		$this->view->title = "Checklist Documents";
		
		$db = getDb();
		$applicantTxnDB = new App_Model_Application_DbTable_ApplicantTransaction();
		$chklistDocDB = new Application_Model_DbTable_ChecklistDocuments();
		$objdeftypeDB = new App_Model_Definitiontype();

	    $form = new Application_Form_SearchApplicant();
	    
	    $session = new Zend_Session_Namespace('sis');
		$this->view->formData = array();

		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
		
			$txnList = $chklistDocDB->getResults($formData);	
			
			
			$total = $db->query($applicantTxnDB->getResults($formData))->rowCount();
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($txnList));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(10000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$form->populate($formData);
			$this->view->formData = $formData;
		}
		else
		{
			
			$txnList = $chklistDocDB->getResults();	
			$total = $db->query($applicantTxnDB->getPaginateData())->rowCount();
		
		    
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($txnList));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(50);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
		}
		
		$this->view->form = $form;
		$this->view->paginator = $paginator;
		$this->view->total = $total;

		$auth = Zend_Auth::getInstance(); 
		$this->view->iduser = $auth->getIdentity()->iduser;

		//status
		$status = $objdeftypeDB->fnGetDefinations('Status');
		$this->view->statusCount = array();
		foreach ( $status as $sts )
		{
			$getTotal = $db->query($db->quoteInto('SELECT at_trans_id FROM applicant_transaction WHERE at_status=?',$sts['idDefinition']))->rowCount();
			
				$this->view->statusCount[$sts['DefinitionDesc']] = $getTotal;
			
			//idDefinition, DefinitionDesc
		}

		//intake
		$this->view->intakeCount = array();
		$intakeDb = new GeneralSetup_Model_DbTable_Intake();
		$listData = $intakeDb->fngetIntakeList();
		foreach ($listData as $list){
			$getTotal = $db->query($db->quoteInto('SELECT at_trans_id FROM applicant_transaction WHERE at_intake=?',$list['IdIntake']))->rowCount();
			$this->view->intakeCount[$list['IntakeDesc']] = $getTotal;
		}
		
	}

	public function viewAction()	
	{
		$id = $this->_getParam('id');
		
		$this->view->title = 'Applicant Files';

		$chklistDocDB = new Application_Model_DbTable_ChecklistDocuments();

		$files = $chklistDocDB->getData($id);

		$this->view->files = $files;
	}
}
?>