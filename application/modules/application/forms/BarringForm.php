<?php 
class Application_Form_BarringForm extends Zend_Dojo_Form{
	
    public function init(){   
        $this->setMethod('post');
        $category = $this->getAttrib('category');
        
        //model
        $model = new Application_Model_DbTable_Barringrelease();
        
        //barring category (student or applicant)
        if ($category){
            $StudentIDName = new Zend_Form_Element_Text('StudentIDName');
            $StudentIDName->setAttrib('class', 'span-7')
                //->setAttrib('onchange', 'findStudent(this.value);')
                ->removeDecorator("DtDdWrapper")
                ->removeDecorator("Label");

            //radio button id or name
            $tbr_category = new Zend_Form_Element_Select('tbr_category');
            $tbr_category->setRegisterInArrayValidator(false);
            $tbr_category->removeDecorator("DtDdWrapper");
            $tbr_category->setAttrib('required',"true");
            $tbr_category->setAttrib('onchange',"getIntake(this.value);");
            $tbr_category->setAttrib('class', 'select');
            $tbr_category->removeDecorator("Label");
            $tbr_category->setAttrib('disabled', 'true');
            $tbr_category->addMultiOption('', '-- Select --');
            $tbr_category->addMultiOption(1, 'Applicant');
            $tbr_category->addMultiOption(2, 'Student');
        }else{
            $tbr_category = new Zend_Form_Element_Radio('tbr_category');
            $tbr_category->removeDecorator("DtDdWrapper");
            $tbr_category->setAttrib('id', 'tbr_category');
            $tbr_category->setAttrib('onchange',"getIntake(this.value);");
            $tbr_category->removeDecorator("Label");
            $tbr_category->addMultiOptions(array(
                '1' => 'Applicant',
                '2' => 'Student'
            ));
            $tbr_category->setSeparator('&nbsp;&nbsp;');
            $tbr_category->setValue("1");
        }
        
        //student/applicant
        $tbr_appstud_id = new Zend_Form_Element_Text('tbr_appstud_id');
	$tbr_appstud_id->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        if ($category){
            $tbr_appstud_id->setAttrib('disabled', 'true');
        }
        
        /*$tbr_appstud_id = new Zend_Form_Element_Select('tbr_appstud_id');
        $tbr_appstud_id->setRegisterInArrayValidator(false);
	$tbr_appstud_id->removeDecorator("DtDdWrapper");
	$tbr_appstud_id->setAttrib('required',"true");
        $tbr_appstud_id->setAttrib('class', 'select');
	$tbr_appstud_id->removeDecorator("Label");
        
        $tbr_appstud_id->addMultiOption('', '-- Select --');
        
        if ($category){
            if ($category == 1){
                $list = $model->getApplicantList();
            }else{
                $list = $model->getStudentList(); 
            }
            
            if ($list){
                foreach ($list as $loop){
                    $tbr_appstud_id->addMultiOption($loop['id'], $loop['name']);
                }
            }
        }*/
        
        //type
        $tbr_type = new Zend_Form_Element_Select('tbr_type');
        $tbr_type->setRegisterInArrayValidator(false);
	$tbr_type->removeDecorator("DtDdWrapper");
	$tbr_type->setAttrib('required',"true");
        $tbr_type->setAttrib('class', 'select');
	$tbr_type->removeDecorator("Label");
        
        $tbr_type->addMultiOption('', '-- Select --');
        
        $typeList = $model->getBarringReleaseType();
        
        if ($typeList){
            foreach ($typeList as $typeLoop){
                $tbr_type->addMultiOption($typeLoop['idDefinition'], $typeLoop['DefinitionDesc']);
            }
        }
        
        //intake
        $tbr_intake = new Zend_Form_Element_Select('tbr_intake');
        $tbr_intake->setRegisterInArrayValidator(false);
	$tbr_intake->removeDecorator("DtDdWrapper");
	$tbr_intake->setAttrib('required',"true");
        $tbr_intake->setAttrib('class', 'select');
	$tbr_intake->removeDecorator("Label");
        
        $tbr_intake->addMultiOption('', '-- Select --');
        
        if ($category){
            if ($category == 1){
                $list = $model->getIntakeList();
            }else{
                $list = $model->getSemesterList(); 
            }
            
            if ($list){
                foreach ($list as $loop){
                    $tbr_intake->addMultiOption($loop['id'], $loop['name']);
                }
            }
        }else{
            $list = $model->getIntakeList();
            
            if ($list){
                foreach ($list as $loop){
                    $tbr_intake->addMultiOption($loop['id'], $loop['name']);
                }
            }
        }
        
        //reason
        $tbr_reason = new Zend_Form_Element_Textarea('tbr_reason');
	$tbr_reason->setAttrib('class', 'span-7')
            ->setAttrib("required", "true")
            ->setAttrib("style","width: 270px; height: 98px;")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $this->addElements(array(
            $tbr_category,
            $tbr_appstud_id,
            $tbr_type,
            $tbr_intake,
            $tbr_reason
        ));
    }
}
?>