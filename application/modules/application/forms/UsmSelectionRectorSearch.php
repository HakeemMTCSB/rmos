<?php
class Application_Form_UsmSelectionRectorSearch extends Zend_Form
{
	protected $_intake;
    protected $_programme;
	protected $_preference;
	
	public function setIntake($value) {
		$this->_intake = $value;
	}
	
	public function setProgramme($value) {
		$this->_programme = $value;
	}
	
	public function setPreference($value) {
		$this->_preference = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
				
  
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		
		//intake
		$this->addElement('select','intake', array(
			'label'=>'Intake',
			'onchange'=>"changeDate(this)",
		    'required'=>true
		));
		
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intakeList = $intakeDb->getData();		
    			
		$this->intake->addMultiOption(null,"-- Please Select --");
		foreach ($intakeList as $list){
			$this->intake->addMultiOption($list['IdIntake'],$list['IntakeDefaultLanguage']);
		}
		$this->intake->setValue($this->_intake);
		
		//date
		$this->addElement('select','date', array(
			'label'=>'Rector Decree Date',
			//'onchange'=>"changeLocation(this)",
		    'required'=>false
		));
		$this->date->addMultiOption("ALL","All");
		$this->date->setRegisterInArrayValidator(false);
		
		//location
		$this->addElement('select','location', array(
			'label'=>'Placement Test Location',
		    'required'=>false
		));
		
		$locationDb = new App_Model_Application_DbTable_PlacementTestLocation();
		$location = $locationDb->getData();
		
		$this->location->addMultiOption("ALL","All");
		foreach ($location as $list){
			$this->location->addMultiOption($list['al_id'],$list['al_location_name']," (".$list['al_location_code'].")");
		}
		
	    //decree number
	    /*
	    $this->addElement('select','rector_number', array(
			'label'=>'Decree Number',
		    'required'=>false
		));
		$this->rector_number->addMultiOption(null,"All");
		$this->rector_number->setRegisterInArrayValidator(false);
		*/
	    
		
		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
                    
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));

	}
	
}

?>