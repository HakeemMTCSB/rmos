<?php

class Application_Form_CopySection extends Zend_Dojo_Form
{

    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('id', 'checklist_search_form');
        $IdProgram = $this->getAttrib('IdProgram');

        $gstrtranslate = Zend_Registry::get('Zend_Translate');

        //scheme
        $field1 = new Zend_Form_Element_Select('IdScheme');
        $field1->setRegisterInArrayValidator(false);
        $field1->removeDecorator("DtDdWrapper");
        $field1->setAttrib('required', true);
        $field1->setAttrib('id', 'IdScheme');
        $field1->setAttrib('class', 'input-txt');
        $field1->removeDecorator("Label");

        $schemeDb = new GeneralSetup_Model_DbTable_Schemesetup();
        $listData = $schemeDb->fngetSchemes();

        $field1->addMultiOption('', '-- Select --');
        $field1->addMultiOptions($listData);

        //program scheme
        $field2 = new Zend_Form_Element_Select('IdProgramScheme');
        $field2->setRegisterInArrayValidator(false);
        $field2->removeDecorator("DtDdWrapper");
        $field2->setAttrib('required', false);
        $field2->setAttrib('id', 'IdProgramScheme');
        $field2->setAttrib('class', 'input-txt');
        $field2->removeDecorator("Label");

        $field2->addMultiOption('', '-- Select --');


        //program
        $field3 = new Zend_Form_Element_Select('IdProgram');
        $field3->setRegisterInArrayValidator(false);
        $field3->removeDecorator("DtDdWrapper");
        $field3->setAttrib('required', false);
        $field3->setAttrib('class', 'input-txt');
        $field3->setAttrib('onchange', 'getProgramScheme(this)');
        $field3->setAttrib('id', 'IdProgram');
        $field3->removeDecorator("Label");

        $progModel = new Application_Model_DbTable_ProgramScheme();
        $progList = $progModel->fnGetProgramList();

        $field3->addMultiOption('', '-- Select --');

        if (count($progList) > 0) {
            foreach ($progList as $progLoop) {
                $field3->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
            }
        }

        //student category
        $field4 = new Zend_Form_Element_Select('category');
        $field4->setRegisterInArrayValidator(false);
        $field4->removeDecorator("DtDdWrapper");
        $field4->setAttrib('id', 'category');
        $field4->setAttrib('required', false);
        $field4->setAttrib('class', 'input-txt');
        $field4->removeDecorator("Label");
        $field4->removeDecorator('HtmlTag');

        $registryDB = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $stdCtgyList = $registryDB->getListDataByCodeType('student-category');

        $field4->addMultiOption('', '-- Select --');

        if (count($stdCtgyList) > 0) {
            foreach ($stdCtgyList as $stdCtgyLoop) {
                $field4->addMultiOption($stdCtgyLoop['id'], $stdCtgyLoop['name']);
            }
        }

        $submit = new Zend_Form_Element_Button('Copy');
        $submit->dojotype = "dijit.form.Button";
        $submit->label = $gstrtranslate->_("Copy");
        $submit->removeDecorator("DtDdWrapper");
        $submit->setAttrib('onclick', 'return sureCopy()');
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
            ->class = "NormalBtn";

        $this->addElements(array(
            $field1,
            $field2,
            $field3,
            $field4,
            $submit
        ));

    }
}

?>