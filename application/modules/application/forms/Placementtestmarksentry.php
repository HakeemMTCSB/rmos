<?php
class Application_Form_Placementtestmarksentry extends Zend_Dojo_Form { //Formclass for the user module
	
    public function init() {
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        $IdProgram = $this->getAttrib('IdProgram');

        $Program = new Zend_Form_Element_Select('Program');
        $Program->removeDecorator("DtDdWrapper");
        //$Program->setAttrib('required',"false");
        $Program->setAttrib('class', 'span-7');
        $Program->removeDecorator("Label");
        //$Program->removeDecorator('HtmlTag');
        $Program->setAttrib('id',"program");
        $Program->setAttrib('onchange', 'get_programscheme()');
        //$Program->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
        $Program->addMultiOption('', '--All--');
        
        $progModel = new Application_Model_DbTable_ProgramScheme();
        $progList = $progModel->fnGetProgramList();
        
        if (count($progList)>0){
            foreach ($progList as $progLoop){
                $Program->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
            }
        }
        
        $Scheme = new Zend_Form_Element_Select('Scheme');
        $Scheme->removeDecorator("DtDdWrapper");
        //$Scheme->setAttrib('required',"false");
        $Scheme->setAttrib('id',"programscheme");
        $Scheme->removeDecorator("Label");
        $Scheme->setAttrib('class', 'span-7');
        //$Scheme->removeDecorator('HtmlTag');
        //$Scheme->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
        $Scheme->addMultiOption('', '--All--');
        
        if ($IdProgram){
            $defModel = new App_Model_General_DbTable_Definationms();
            $progSchemeList = $progModel->fnGetProgramschemeListBasedOnProgId($IdProgram);
            foreach($progSchemeList as $progSchemeListloop){
                $mod_of_study = $defModel->getData($progSchemeListloop['mode_of_study']);
                $mod_of_program = $defModel->getData($progSchemeListloop['mode_of_program']);
                $programType = $defModel->getData($progSchemeListloop['program_type']);

                $Scheme->addMultiOption($progSchemeListloop['IdProgramScheme'], $mod_of_study['DefinitionDesc'].' '.$mod_of_program['DefinitionDesc'].' '. $programType['DefinitionDesc']);
            }
        }

        $Intake = new Zend_Form_Element_Select('Intake');
        $Intake->removeDecorator("DtDdWrapper");
        //$Intake->setAttrib('required',"false");
        $Intake->removeDecorator("Label");
        $Intake->setAttrib('class', 'span-7');
        //$Intake->removeDecorator('HtmlTag');
        //$Intake->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
        $intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $intakeList = $intakeModel->fngetIntakeList();
        
        $Intake->addMultiOption('', '--All--');
        
        if (count($intakeList)>0){
            foreach ($intakeList as $intakeLoop){
                $Intake->addMultiOption($intakeLoop['IdIntake'], $intakeLoop['IntakeId']);
            }
        }

        $PlacementTest = new Zend_Form_Element_Select('PlacementTest');
        $PlacementTest->removeDecorator("DtDdWrapper");
        $PlacementTest->setAttrib('required',"true");
        $PlacementTest->removeDecorator("Label");
        $PlacementTest->setAttrib('class', 'span-7');
        //$PlacementTest->removeDecorator('HtmlTag');
        //$PlacementTest->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
        $PlacementTest->addMultiOption('', '--All--');
        
        $lobjplacementtest = new Application_Model_DbTable_Placementtest();
        $placementTestList = $lobjplacementtest->fngetPlacementtestDetails();
        
        if (count($placementTestList)>0){
            foreach ($placementTestList as $placementTestLoop){
                $PlacementTest->addMultiOption($placementTestLoop['IdPlacementTest'], $placementTestLoop['PlacementTestName']);
            }
        }
        
        $StudentCategory = new Zend_Form_Element_Select('StudentCategory');
        $StudentCategory->removeDecorator("DtDdWrapper");
        //$StudentCategory->setAttrib('required',"false");
        $StudentCategory->removeDecorator("Label");
        $StudentCategory->setAttrib('class', 'span-7');
        //$StudentCategory->removeDecorator('HtmlTag');
        //$StudentCategory->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
        $defModel = new App_Model_General_DbTable_Definationms();
        $stdCtgyList = $defModel->getDataByType(95);
        
        $StudentCategory->addMultiOption('', '--All--');
        
        if (count($stdCtgyList)>0){
            foreach ($stdCtgyList as $stdCtgyLoop){
                $StudentCategory->addMultiOption($stdCtgyLoop['idDefinition'], $stdCtgyLoop['DefinitionDesc']);
            }
        }

        $ApplicantId = new Zend_Form_Element_Text('ApplicantId');
        //$ApplicantId ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ApplicantId->setAttrib('class', 'span-7')
        ->removeDecorator("DtDdWrapper")
        ->removeDecorator("Label");
        //->removeDecorator('HtmlTag');

        $ApplicantName1 = new Zend_Form_Element_Text('ApplicantName1');
        //$ApplicantName1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ApplicantName1->setAttrib('class', 'span-7')
        ->removeDecorator("DtDdWrapper")
        ->removeDecorator("Label");
        //->removeDecorator('HtmlTag');
        
        $ApplicantName2 = new Zend_Form_Element_Text('ApplicantName2');
        //$ApplicantName2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ApplicantName2->setAttrib('class', 'span-7')
        ->removeDecorator("DtDdWrapper")
        ->removeDecorator("Label");
        //->removeDecorator('HtmlTag');
        
        $ApplicantName3 = new Zend_Form_Element_Text('ApplicantName3');
        //$ApplicantName3->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ApplicantName3->setAttrib('class', 'span-7')
        ->removeDecorator("DtDdWrapper")
        ->removeDecorator("Label");
        //->removeDecorator('HtmlTag');

        $Marks = new Zend_Form_Element_Text('Marks');
        //$ApplicantName3->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Marks->setAttrib('class', 'span-3')
        ->setAttrib('name', 'Marks[]')
        ->removeDecorator("DtDdWrapper")
        ->removeDecorator("Label")
        ->removeDecorator('HtmlTag');
        
        $Clear = new Zend_Form_Element_Submit('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
        $Clear->setAttrib('class', 'NormalBtn')
        ->removeDecorator("Label")
        ->removeDecorator("DtDdWrapper")
        ->removeDecorator('HtmlTag');

        $Search = new Zend_Form_Element_Submit('Search');
        $Search->dojotype="dijit.form.Button";
        $Search->label = $gstrtranslate->_("Search");
        $Search->removeDecorator("DtDdWrapper");
        $Search->removeDecorator("Label");
        $Search->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag');



        $this->addElements(array($Scheme,
            $Program,$Intake,$PlacementTest,
            $ApplicantId,$PlacementTest,$ApplicantName1,$ApplicantName1, 
            $ApplicantName2, $ApplicantName3,$Clear,$Search,$Save,$StudentCategory,$Marks
        ));

    }
}