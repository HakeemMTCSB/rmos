<?php

class Application_Form_EditItem extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('id', 'edit_item_form');

        $this->addElement('text', 'name', array(
            'label'    => 'Item Name',
            'required' => 'true',
            'class'    => 'input-txt'

        ));

        $this->addElement('text', 'itemMalayName', array(
            'label' => 'Item Malay Name',
            'class' => 'input-txt'
        ));

        $this->addElement('text', 'variable', array(
            'label'   => 'Item Variable',
            'disable' => 'true',
            'class'   => 'input-txt'
        ));

        $this->addElement('text', 'DefinitionDesc', array(
            'label'   => 'Item Form Type',
            'disable' => 'true',
            'class'   => 'input-txt'
        ));

        $this->addElement('textarea', 'description', array(
            'label' => 'Description',
            'class' => 'input-txt'
        ));

        //button
        $this->addElement('submit', 'save', array(
            'label'      => 'Save',
            'decorators' => array('ViewHelper')
        ));

        $this->addElement('submit', 'cancel', array(
            'label'      => 'Cancel',
            'decorators' => array('ViewHelper'),
            'onClick'    => "window.location ='" . $this->getView()->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'index'), 'default', true) . "'; return false;"
        ));

        $this->addDisplayGroup(array('save', 'cancel'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                array('HtmlTag', array('tag' => 'div', 'class' => 'buttons')),
                'DtDdWrapper'
            )
        ));

    }
}

?>