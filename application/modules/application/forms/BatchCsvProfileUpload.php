<?php
class Application_Form_BatchCsvProfileUpload extends Zend_Form {
	
	protected $scidval;
	
	public function setScidval($scidval){
		$this->scidval = $scidval;
	}
	
	public function init()
	{
        //parent::__construct($options);
        
		$this->addElement('hidden','scid');
		$this->scid->setValue($this->scidval);

        $this->setName('upload');
        $this->setAttrib('enctype', 'multipart/form-data');
		$this->setAttrib('action', $this->getView()->url(array('module'=>'application', 'controller'=>'batch-agent','action'=>'upload-csv'),'default',true) );
		$this->setAttrib('method', "POST" );
          
        $file = new Zend_Form_Element_File('file');
        $file->setLabel('File')
            ->setDestination(APPLICATION_PATH  . '/tmp')
            ->setRequired(true);
		$this->addElement($file);
		
		//ApplicantID

		$this->addElement('hidden', 'header', array(
			'description' => 'CSV Configuration Option',
		    'ignore' => true,
		    'decorators' => array(
		        	array('Description', array('escape'=>false, 'tag'=>'<h1>')),
		    	),
		));
		
		$format = new Zend_Form_Element_Checkbox('remove_header');
        $format->setLabel('Remove Header')
               ->setRequired(true);
        
		$this->addElement($format);		
	
	}
		
}