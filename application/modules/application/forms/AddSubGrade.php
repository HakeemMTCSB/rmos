<?php
class Application_Form_AddSubGrade extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $gstrtranslate = Zend_Registry::get('Zend_Translate');
        $lstrAmountConstraint = "{pattern:'0.##'}";

        /*$lobjprogtype = new Application_Model_DbTable_Subjectsetup();
        $lobjprogtypelist = $lobjprogtype->fngetprogtypeList();

        $lobjlevel = new Application_Model_DbTable_Subjectsetup();
        $lobjlevellist = $lobjlevel->fngetlevelList();*/

        $lobjprogramtype = new GeneralSetup_Model_DbTable_Registry_RegistryValue();

        /*$lobjqua = new Application_Model_DbTable_Qualificationsetup();
        $lobjqualist = $lobjqua->fngetQualificationList();*/



        $UpdDate  = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate  				->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser  				->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');



        $sg_grade = new Zend_Form_Element_Text('sg_grade');
        $sg_grade	->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $sg_grade ->setAttrib('required',"false");
        $sg_grade ->removeDecorator("DtDdWrapper");
        $sg_grade ->removeDecorator("Label");
        $sg_grade ->removeDecorator('HtmlTag');
        //$sgt_id->setRegisterInArrayValidator(false);

        $sg_grade_desc = new Zend_Form_Element_Text('sg_grade_desc');
        $sg_grade_desc	->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $sg_grade_desc ->setAttrib('required',"false");
        $sg_grade_desc ->removeDecorator("DtDdWrapper");
        $sg_grade_desc ->removeDecorator("Label");
        $sg_grade_desc ->removeDecorator('HtmlTag');
        //$sgt_desc->setRegisterInArrayValidator(false);

        $sg_grade_point = new Zend_Form_Element_Text('sg_grade_point');
        $sg_grade_point	->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $sg_grade_point ->setAttrib('required',"false");
        $sg_grade_point ->removeDecorator("DtDdWrapper");
        $sg_grade_point ->removeDecorator("Label");
        $sg_grade_point ->removeDecorator('HtmlTag');
        //$sgt_desc->setRegisterInArrayValidator(false);

        $sg_mohe_code = new Zend_Form_Element_Text('sg_mohe_code');
        $sg_mohe_code	->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $sg_mohe_code ->setAttrib('required',"false");
        $sg_mohe_code ->removeDecorator("DtDdWrapper");
        $sg_mohe_code ->removeDecorator("Label");
        $sg_mohe_code ->removeDecorator('HtmlTag');
        //$sgt_desc->setRegisterInArrayValidator(false);


        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->setAttrib('class','NormalBtn');
        $Save->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');



        $AddSub = new Zend_Form_Element_Button('AddSub');
        $AddSub->setAttrib('class', 'NormalBtn');
        $AddSub->label = $gstrtranslate->_("Add");
        $AddSub->setAttrib('dojoType',"dijit.form.Button");



        $Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');



        //form elements
        $this->addElements(array(
            $AddSub,
            $Active,
            $Save,
            $sg_grade,
            $sg_grade_desc,
            $sg_grade_point,
            $sg_mohe_code,
            $UpdUser,
            $UpdDate));

    }
}