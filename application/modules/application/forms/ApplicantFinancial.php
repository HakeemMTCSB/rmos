<?php 
class Application_Form_ApplicantFinancial extends Zend_Dojo_Form{
	
    public function init()
    {   
       $IdProgram = $this->getAttrib('IdProgram');
        
       $gstrtranslate =Zend_Registry::get('Zend_Translate');
		
        
        $feeplan = new Zend_Form_Element_Select('feeplan');
		$feeplan->removeDecorator("DtDdWrapper");
		$feeplan->setAttrib('class', 'select');
		$feeplan->removeDecorator("Label");
		
		$objdeftypeDB = new App_Model_Definitiontype();
		$feestrucDB = new Studentfinance_Model_DbTable_FeeStructure();

		$studcat = $objdeftypeDB->fnGetDefinations('Student Category');
		$getplan = array();
		$plans = array();

		foreach ( $studcat as $cat ) 
		{
			$getplan[$cat['idDefinition']] = $feestrucDB->getFeeStructureByCategory($cat['idDefinition']);
		}

		foreach ( $studcat as $cat ) 
		{
			foreach ( $getplan[$cat['idDefinition']] as $plan )
			{
				$plans[$plan['fs_student_category']][$plan['fs_id']] = $plan['fs_name'];
			}
		}
		
		$feeplan->addMultiOption('','-- Select --');
		foreach ( $studcat as $cat ) 
		{
			if ( !empty($plans[$cat['idDefinition']]) )
			{
				$feeplan->addMultiOption( $cat['DefinitionDesc'] , $plans[$cat['idDefinition']] );
			}
		}

		/* ----------------------------- */

		$discountplan = new Zend_Form_Element_Select('discountplan');
		$discountplan->removeDecorator("DtDdWrapper");
		$discountplan->setAttrib('class', 'select');
		$discountplan->removeDecorator("Label");

		$discountDB = new Studentfinance_Model_DbTable_DiscountType();
		$discountplan->addMultiOption('','-- Select --');
		$discountType = $discountDB->getData();
		if ( !empty($discountType) )
		{
			foreach( $discountType as $dt )
			{
				$discountplan->addMultiOption($dt['dt_id'], $dt['dt_discount']);
			}
		}
			
        $this->addElements(array(
            $feeplan,
			$discountplan
        ));
		
    }
}
?>