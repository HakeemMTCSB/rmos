<?php
class Application_Form_registrationlocation extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate');


		$RegistrationLocationName = new Zend_Form_Element_Text('RegistrationLocationName');
		$RegistrationLocationName->addValidator(new Zend_Validate_Db_NoRecordExists('tbl_registrationlocation', 'RegistrationLocationName'));
		$RegistrationLocationName->removeDecorator("DtDdWrapper");
		$RegistrationLocationName->setAttrib('required',"true") ;
		$RegistrationLocationName->removeDecorator("Label");
		$RegistrationLocationName->removeDecorator('HtmlTag');
		$RegistrationLocationName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RegistrationLocationName->setAttrib('propercase',"true");


		$IdRegistrationLocation = new Zend_Form_Element_Hidden('IdRegistrationLocation');
		$IdRegistrationLocation->removeDecorator("DtDdWrapper");
		$IdRegistrationLocation->removeDecorator("Label");
		$IdRegistrationLocation->removeDecorator('HtmlTag');
		$IdRegistrationLocation->setAttrib('dojoType',"dijit.form.ValidationTextBox");



		$RegistrationLocationCode = new Zend_Form_Element_Text('RegistrationLocationCode');
		$RegistrationLocationCode->removeDecorator("DtDdWrapper");
		$RegistrationLocationCode->setAttrib('required',"true") ;
		$RegistrationLocationCode->removeDecorator("Label");
		$RegistrationLocationCode->removeDecorator('HtmlTag');
		$RegistrationLocationCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");

		$RegistrationLocationShortName = new Zend_Form_Element_Text('RegistrationLocationShortName');
		$RegistrationLocationShortName->removeDecorator("DtDdWrapper");
		$RegistrationLocationShortName->setAttrib('required',"true") ;
		$RegistrationLocationShortName->removeDecorator("Label");
		$RegistrationLocationShortName->removeDecorator('HtmlTag');
		$RegistrationLocationShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RegistrationLocationShortName->setAttrib('propercase',"true");

		$RegistrationLocationDescription = new Zend_Form_Element_Text('RegistrationLocationDescription');
		$RegistrationLocationDescription->removeDecorator("DtDdWrapper");
		$RegistrationLocationDescription->setAttrib('required',"true") ;
		$RegistrationLocationDescription->removeDecorator("Label");
		$RegistrationLocationDescription->removeDecorator('HtmlTag');
		$RegistrationLocationDescription->setAttrib('dojoType',"dijit.form.ValidationTextBox");

		$IntaketableObject = new GeneralSetup_Model_DbTable_Intake();
		$Intakes = $IntaketableObject->fngetIntakes();
		$RegistrationLocationIntake = new Zend_Dojo_Form_Element_FilteringSelect('RegistrationLocationIntake');
		$RegistrationLocationIntake->removeDecorator("DtDdWrapper");
		$RegistrationLocationIntake->setAttrib('required',"true") ;
		$RegistrationLocationIntake->removeDecorator("Label");
		$RegistrationLocationIntake->removeDecorator('HtmlTag');
		$RegistrationLocationIntake->addMultiOptions( $Intakes);
		$RegistrationLocationIntake->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$Program = new GeneralSetup_Model_DbTable_Program();
		$program_list = $Program->fnGetProgramList();
		$RegistrationLocationProgram = new Zend_Dojo_Form_Element_FilteringSelect('RegistrationLocationProgram');
		$RegistrationLocationProgram->removeDecorator("DtDdWrapper");
		$RegistrationLocationProgram->setAttrib('required',"true") ;
		$RegistrationLocationProgram->removeDecorator("Label");
		$RegistrationLocationProgram->removeDecorator('HtmlTag');
		$RegistrationLocationProgram->addMultiOptions( $program_list);
		$RegistrationLocationProgram->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$Branch = new GeneralSetup_Model_DbTable_Intake();
		$branch_list = $IntaketableObject->fngetBranchList();
		$RegistrationLocationBranch = new Zend_Dojo_Form_Element_FilteringSelect('RegistrationLocationBranch');
		$RegistrationLocationBranch->removeDecorator("DtDdWrapper");
		$RegistrationLocationBranch->setAttrib('required',"true") ;
		$RegistrationLocationBranch->removeDecorator("Label");
		$RegistrationLocationBranch->removeDecorator('HtmlTag');
		$RegistrationLocationBranch->addMultiOptions( $branch_list);
		$RegistrationLocationBranch->setAttrib('dojoType',"dijit.form.FilteringSelect");

		/*$lobjdefination = new App_Model_Definitiontype();
        $program_scheme_list = $lobjdefination->fnGetDefinationMs( "Program Scheme" );
		$ProgramScheme = new Zend_Dojo_Form_Element_FilteringSelect('ProgramScheme');
		$ProgramScheme->removeDecorator("DtDdWrapper");
		$ProgramScheme->setAttrib('required',"true") ;
		$ProgramScheme->removeDecorator("Label");
		$ProgramScheme->removeDecorator('HtmlTag');
		$ProgramScheme->addMultiOptions( $program_scheme_list);
		$ProgramScheme->setAttrib('dojoType',"dijit.form.FilteringSelect");*/

		


		$RegistrationLocationStatus  = new Zend_Form_Element_Checkbox('RegistrationLocationStatus');
		$RegistrationLocationStatus->setAttrib('dojoType',"dijit.form.CheckBox");
		$RegistrationLocationStatus->setvalue('1');
		$RegistrationLocationStatus->removeDecorator("DtDdWrapper");
		$RegistrationLocationStatus->removeDecorator("Label");
		$RegistrationLocationStatus->removeDecorator('HtmlTag');

		$RegistrationLocationAddress1  = new Zend_Form_Element_Text('RegistrationLocationAddress1');
		$RegistrationLocationAddress1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RegistrationLocationAddress1->removeDecorator("DtDdWrapper");
		$RegistrationLocationAddress1->removeDecorator("Label");
		$RegistrationLocationAddress1->removeDecorator('HtmlTag');
		$RegistrationLocationAddress1->setAttrib('propercase',"true");

		$RegistrationLocationAddress2  = new Zend_Form_Element_Text('RegistrationLocationAddress2');
		$RegistrationLocationAddress2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RegistrationLocationAddress2->removeDecorator("DtDdWrapper");
		$RegistrationLocationAddress2->removeDecorator("Label");
		$RegistrationLocationAddress2->removeDecorator('HtmlTag');
		$RegistrationLocationAddress2->setAttrib('propercase',"true");


		$lobjuser = new GeneralSetup_Model_DbTable_User();
		$lobjcountry = $lobjuser->fnGetCountryList();
		$country = new Zend_Dojo_Form_Element_FilteringSelect('country');
		$country->removeDecorator("DtDdWrapper");
		//$country->setAttrib('required',"true") ;
		$country->removeDecorator("Label");
		$country->removeDecorator('HtmlTag');
		$country->setAttrib('OnChange', 'fnGetCountryStateList');
		$country->setRegisterInArrayValidator(false);
		$country->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$country->addMultiOptions($lobjcountry);


		$state = new Zend_Dojo_Form_Element_FilteringSelect('state');
		$state->removeDecorator("DtDdWrapper");
		//$state->setAttrib('required',"true") ;
		$state->removeDecorator("Label");
		$state->removeDecorator('HtmlTag');
		$state->setRegisterInArrayValidator(false);
		$state->setAttrib('OnChange', 'fnGetStateCityList');
		$state->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$states = new Zend_Dojo_Form_Element_FilteringSelect('states');
		$states->removeDecorator("DtDdWrapper");
		//$states->setAttrib('required',"true") ;
		$states->removeDecorator("Label");
		$states->removeDecorator('HtmlTag');
		$states->setRegisterInArrayValidator(false);
		$states->setAttrib('OnChange', 'fnGetStateCityList');
		$states->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$city = new Zend_Dojo_Form_Element_FilteringSelect('city');
		$city->removeDecorator("DtDdWrapper");
		$city->removeDecorator("Label");
		$city->removeDecorator('HtmlTag');
		$city->setAttrib('required',"false");
		$city->setRegisterInArrayValidator(false);
		$city->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$zipCode = new Zend_Form_Element_Text('zipCode');
		$zipCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$zipCode->setAttrib('maxlength','20');
		$zipCode->removeDecorator("DtDdWrapper");
		$zipCode->removeDecorator("Label");
		$zipCode->removeDecorator('HtmlTag');

		$Regcountrycode = new Zend_Form_Element_Text('Regcountrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$Regcountrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Regcountrycode->setAttrib('maxlength','3');
		$Regcountrycode->setAttrib('style','width:30px');
		$Regcountrycode->removeDecorator("DtDdWrapper");
		$Regcountrycode->removeDecorator("Label");
		$Regcountrycode->removeDecorator('HtmlTag');

		$Regstatecode = new Zend_Form_Element_Text('Regstatecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$Regstatecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Regstatecode->setAttrib('maxlength','2');
		$Regstatecode->setAttrib('style','width:30px');
		$Regstatecode->removeDecorator("DtDdWrapper");
		$Regstatecode->removeDecorator("Label");
		$Regstatecode->removeDecorator('HtmlTag');


		$RegPhone = new Zend_Form_Element_Text('RegPhone',array('regExp'=>"[0-9]+",'invalidMessage'=>"Not a valid Work Phone No."));
		$RegPhone->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RegPhone->setAttrib('maxlength','9');
		$RegPhone->setAttrib('style','width:93px');
		$RegPhone->removeDecorator("DtDdWrapper");
		$RegPhone->removeDecorator("Label");
		$RegPhone->removeDecorator('HtmlTag');


		$faxcountrycode = new Zend_Form_Element_Text('faxcountrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$faxcountrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$faxcountrycode->setAttrib('maxlength','3');
		$faxcountrycode->setAttrib('style','width:30px');
		$faxcountrycode->removeDecorator("DtDdWrapper");
		$faxcountrycode->removeDecorator("Label");
		$faxcountrycode->removeDecorator('HtmlTag');

		$faxstatecode = new Zend_Form_Element_Text('faxstatecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$faxstatecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$faxstatecode->setAttrib('maxlength','5');
		$faxstatecode->setAttrib('style','width:30px');
		$faxstatecode->removeDecorator("DtDdWrapper");
		$faxstatecode->removeDecorator("Label");
		$faxstatecode->removeDecorator('HtmlTag');

		$Fax = new Zend_Form_Element_Text('fax',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Fax"));
		$Fax->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Fax->setAttrib('style','width:93px');
		$Fax->setAttrib('maxlength','20');
		$Fax->removeDecorator("DtDdWrapper");
		$Fax->removeDecorator("Label");
		$Fax->removeDecorator('HtmlTag');


		$ScemetableObject = new GeneralSetup_Model_DbTable_Schemesetup();
		$Schemes = $ScemetableObject->fngetSchemes();
		$RegistrationLocationScheme = new Zend_Dojo_Form_Element_FilteringSelect('RegistrationLocationScheme');
		$RegistrationLocationScheme->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$RegistrationLocationScheme->setAttrib('required',"false") ;
		$RegistrationLocationScheme->removeDecorator("DtDdWrapper");
		$RegistrationLocationScheme->removeDecorator("Label");
		$RegistrationLocationScheme->removeDecorator('HtmlTag');
		$RegistrationLocationScheme->addMultiOptions( $Schemes );

		$RegistrationLocationDate = new Zend_Form_Element_Text('RegistrationLocationDate');
		$RegistrationLocationDate->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RegistrationLocationDate->setAttrib('maxlength','20');
		$RegistrationLocationDate->removeDecorator("DtDdWrapper");
		$RegistrationLocationDate->removeDecorator("Label");
		$RegistrationLocationDate->removeDecorator('HtmlTag');

		$RegistrationLocationTime = new Zend_Form_Element_Text('RegistrationLocationTime');
		$RegistrationLocationTime->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RegistrationLocationTime->setAttrib('maxlength','20');
		$RegistrationLocationTime->removeDecorator("DtDdWrapper");
		$RegistrationLocationTime->removeDecorator("Label");
		$RegistrationLocationTime->removeDecorator('HtmlTag');

		$RegistrationLocationRemarks = new Zend_Form_Element_Textarea('RegistrationLocationRemarks');
		//$RegistrationLocationRemarks->setAttrib('dojoType',"dijit.form.Textarea");
		$RegistrationLocationRemarks->setAttrib('rows','5');
		$RegistrationLocationRemarks->setAttrib('cols','30 ');
		$RegistrationLocationRemarks->removeDecorator("DtDdWrapper");
		$RegistrationLocationRemarks->removeDecorator("Label");
		$RegistrationLocationRemarks->removeDecorator('HtmlTag');

		$clear = new Zend_Form_Element_Button('Clear');
		$clear->setAttrib('class', 'NormalBtn');
		$clear->setAttrib('dojoType',"dijit.form.Button");
		$clear->label = $gstrtranslate->_("Clear");
		$clear->setAttrib('OnClick', 'clearregistrationpoint()');
		$clear->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$RegistrationLocationDate = new Zend_Dojo_Form_Element_DateTextBox('RegistrationLocationDate');
		$RegistrationLocationDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		//$RegistrationLocationDate->setAttrib('constraints', "$dateofbirth");
		$RegistrationLocationDate->setAttrib('required',"false");
		$RegistrationLocationDate->removeDecorator("DtDdWrapper");
		//$RegistrationLocationDate->setAttrib('title',"dd-mm-yyyy");
		$RegistrationLocationDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$RegistrationLocationDate->removeDecorator("Label");
		$RegistrationLocationDate->removeDecorator('HtmlTag');

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Save");
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag')
		->class = "NormalBtn";



		$this->addElements(array(
				$RegistrationLocationName,$RegistrationLocationCode,$RegistrationLocationShortName,
				$RegistrationLocationDescription,$RegistrationLocationIntake,$RegistrationLocationStatus,
				$RegistrationLocationAddress1,$RegistrationLocationAddress2,$country,$state,$states,
				$zipCode,$Regcountrycode,$Regstatecode,$RegPhone,$faxcountrycode,$faxstatecode,$Fax,
				$RegistrationLocationScheme,$RegistrationLocationDate,$RegistrationLocationTime,$RegistrationLocationRemarks,
				$RegistrationLocationProgram, $RegistrationLocationBranch,
				$clear,$Save,$city,$IdRegistrationLocation
		));


	}
}
?>
