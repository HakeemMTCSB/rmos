<?php

class Application_Form_AcademicRequirementSubject extends Zend_Form
{

    protected $id;

    public function setId($id){
        $this->id = $id;
    }

    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('id', 'formAddAcademicSubject');

        $this->addElement('hidden','IdProgramEntryReq');

        //school subject
        $this->addElement('select', 'IdSubject',
            array(
                'label'    => 'Subject',
                'required' => false,
                'class'    => 'input-txt'
            )
        );

        $schoolSubjectDB = new Application_Model_DbTable_SchoolSubject();
        $subjectList = $schoolSubjectDB->getList();

        $this->IdSubject->addMultiOption(null, "ANY");
        foreach ($subjectList as $list) {
            $this->IdSubject->addMultiOption($list['ss_id'], $list['ss_subject']);
        }

        //subject grade
        $this->addElement('select', 'IdGrade',
            array(
                'label'    => 'Grade',
                'required' => false,
                'class'    => 'input-txt'
            )
        );

        //number of subject
        $this->addElement('text', 'subject_no',
            array(
                'label'    => 'Number of Subject',
                'required' => false,
                'class'    => 'input-txt'
            )
        );


    }
}

?>