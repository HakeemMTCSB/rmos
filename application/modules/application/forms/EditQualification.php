<?php
class Application_Form_EditQualification extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $gstrtranslate = Zend_Registry::get('Zend_Translate');
        $lstrAmountConstraint = "{pattern:'0.##'}";

        /*$lobjprogtype = new Application_Model_DbTable_Subjectsetup();
        $lobjprogtypelist = $lobjprogtype->fngetprogtypeList();

        $lobjlevel = new Application_Model_DbTable_Subjectsetup();
        $lobjlevellist = $lobjlevel->fngetlevelList();*/

        $lobjprogramtype = new GeneralSetup_Model_DbTable_Registry_RegistryValue();

        /*$lobjqua = new Application_Model_DbTable_Qualificationsetup();
        $lobjqualist = $lobjqua->fngetQualificationList();*/

        /*$IdQualification  = new Zend_Form_Element_Hidden('IdQualification');
        $IdQualification  	->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');*/

        $UpdDate  = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate  				->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser  				->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $qt_id = new Zend_Form_Element_Text('qt_id');
        $qt_id	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
            ->setAttrib('required',"true")
            ->setAttrib('maxlength','10')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $qt_eng_desc = new Zend_Form_Element_Text('qt_eng_desc');
        $qt_eng_desc->setAttrib('dojoType',"dijit.form.ValidationTextBox")
            ->setAttrib('required',"true")
            ->setAttrib('maxlength','50')
            ->setAttrib('style','width:100%;')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $qt_mal_desc = new Zend_Form_Element_Text('qt_mal_desc');
        $qt_mal_desc->setAttrib('dojoType',"dijit.form.ValidationTextBox")
            //->setAttrib('required',"true")
            ->setAttrib('maxlength','50')
            ->setAttrib('style','width:100%;')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');


        $qt_subj_min = new Zend_Form_Element_Text('qt_subj_min');
        $qt_subj_min	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
            //->setAttrib('required',"false")
            ->setAttrib('maxlength','10')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $qt_mohe_code = new Zend_Form_Element_Text('qt_mohe_code');
        $qt_mohe_code	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
            //->setAttrib('required',"false")
            ->setAttrib('maxlength','10')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        /*$IdSubject = new Zend_Dojo_Form_Element_FilteringSelect('IdSubject');
        $IdSubject->removeDecorator("DtDdWrapper");
        $IdSubject->setAttrib('required',"false") ;
        $IdSubject->removeDecorator("Label");
        $IdSubject->removeDecorator('HtmlTag');
        $IdSubject->setRegisterInArrayValidator(false);
        $IdSubject->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $IdSubject->addMultiOptions($lobjsubjectlist);*/


        /*$registrytypes = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $qualificationtypelist = $registrytypes->getList('6');

        $qt_prog_type = new Zend_Dojo_Form_Element_FilteringSelect('qt_prog_type');
        $qt_prog_type->removeDecorator("DtDdWrapper");
        $qt_prog_type->setAttrib('required',"false") ;
        $qt_prog_type->removeDecorator("Label");
        $qt_prog_type->removeDecorator('HtmlTag');
        $qt_prog_type->setRegisterInArrayValidator(false);
        $qt_prog_type->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $qt_prog_type->addMultiOption(null,"-- Select --");
        $qt_prog_type->addMultiOptions($qualificationtypelist);

        $awardlevel = new GeneralSetup_Model_DbTable_Awardlevel();
        $awardlevellist = $awardlevel->getAwardList();

        $qt_level = new Zend_Dojo_Form_Element_FilteringSelect('qt_level');
        $qt_level->removeDecorator("DtDdWrapper");
        $qt_level->setAttrib('required',"false") ;
        $qt_level->removeDecorator("Label");
        $qt_level->removeDecorator('HtmlTag');
        $qt_level->setRegisterInArrayValidator(false);
        $qt_level->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $qt_level->addMultiOption(null,"-- Select --");
        $qt_level->addMultiOptions($awardlevellist);*/


        $qt_prog_type  = new Zend_Dojo_Form_Element_FilteringSelect('qt_prog_type');
        $qt_prog_type->removeDecorator("DtDdWrapper");
        //$qt_prog_type->setAttrib('required',"true") ;
        $qt_prog_type->removeDecorator("Label");
        $qt_prog_type->removeDecorator('HtmlTag');
        $qt_prog_type->setRegisterInArrayValidator(false);
        //$qt_prog_type->setAttrib('OnChange', 'getProgramSchemes()');
        $qt_prog_type->setAttrib('dojoType',"dijit.form.FilteringSelect");


        /*$registrytypes=$lobjprogramtype->getList('6');
        $qt_prog_type->addMultiOptions( $registrytypes);*/


        //Program level
        $qt_level  = new Zend_Dojo_Form_Element_FilteringSelect('qt_level');
        $qt_level->removeDecorator("DtDdWrapper");
        //$qt_level->setAttrib('required',"true") ;
        $qt_level->removeDecorator("Label");
        $qt_level->removeDecorator('HtmlTag');
        $qt_level->setRegisterInArrayValidator(false);
        $qt_level->setAttrib('dojoType',"dijit.form.FilteringSelect");

        $sgt_id = new Zend_Form_Element_Text('sgt_id');
        $sgt_id	->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $sgt_id ->setAttrib('required',"false");
        $sgt_id ->removeDecorator("DtDdWrapper");
        $sgt_id ->removeDecorator("Label");
        $sgt_id ->removeDecorator('HtmlTag');
        //$sgt_id->setRegisterInArrayValidator(false);

        $sgt_desc = new Zend_Form_Element_Text('sgt_desc');
        $sgt_desc	->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $sgt_desc ->setAttrib('required',"false");
        $sgt_desc ->removeDecorator("DtDdWrapper");
        $sgt_desc ->removeDecorator("Label");
        $sgt_desc ->removeDecorator('HtmlTag');
        //$sgt_desc->setRegisterInArrayValidator(false);

        /*$sgt_qua_type = new Zend_Dojo_Form_Element_FilteringSelect('sgt_qua_type');
        $sgt_qua_type->removeDecorator("DtDdWrapper");
        $sgt_qua_type->setAttrib('required',"false") ;
        $sgt_qua_type->removeDecorator("Label");
        $sgt_qua_type->removeDecorator('HtmlTag');
        $sgt_qua_type->setRegisterInArrayValidator(false);
        $sgt_qua_type->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $sgt_qua_type->addMultiOption('0', 'Select');
        $sgt_qua_type->addMultiOptions($lobjqualist);*/


        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->setAttrib('class','NormalBtn');
        $Save->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Search = new Zend_Form_Element_Submit('Search');
        $Search	->setAttrib('id', 'search')
            ->setAttrib('name', 'search')
            ->setAttrib('class', 'NormalBtn')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Add = new Zend_Form_Element_Button('Add');
        $Add->setAttrib('class', 'NormalBtn');
        $Add->setAttrib('dojoType',"dijit.form.Button");
        $Add->setAttrib('OnClick', 'addSubjectentry()')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $clear = new Zend_Form_Element_Button('Clear');
        $clear->setAttrib('class', 'NormalBtn');
        $clear->setAttrib('dojoType',"dijit.form.Button");
        $clear->label = $gstrtranslate->_("Clear");
        $clear->setAttrib('OnClick', 'clearpageAdd()');
        $clear->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $AddSub = new Zend_Form_Element_Button('AddSub');
        $AddSub->setAttrib('class', 'NormalBtn');
        $AddSub->label = $gstrtranslate->_("Add");
        $AddSub->setAttrib('dojoType',"dijit.form.Button")

            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $clearSub = new Zend_Form_Element_Button('ClearSub');
        $clearSub->setAttrib('class', 'NormalBtn');
        $clearSub->setAttrib('dojoType',"dijit.form.Button");
        $clearSub->label = $gstrtranslate->_("Clear");
        $clearSub->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');

        $SpecialTreatment  = new Zend_Form_Element_Checkbox('SpecialTreatment');
        $SpecialTreatment->setAttrib('dojoType',"dijit.form.CheckBox");
        $SpecialTreatment->setvalue('0');
        $SpecialTreatment->removeDecorator("DtDdWrapper");
        $SpecialTreatment->removeDecorator("Label");
        $SpecialTreatment->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($clear,$Add,
            $clearSub,
            $AddSub,
            //$IdSubject,
            $Search,
            $Active,
            $Save,
            $qt_eng_desc,
            $qt_id,
            $qt_mal_desc,
            $qt_prog_type,
            $qt_level,
            $qt_subj_min,
            $qt_mohe_code,
            $sgt_id,
            $sgt_desc,
            $UpdUser,
            $UpdDate));

    }
}