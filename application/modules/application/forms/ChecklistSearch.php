<?php 
class Application_Form_ChecklistSearch extends Zend_Dojo_Form{
	
    public function init()
    {   
        $this->setMethod('post');
	$this->setAttrib('id', 'checklist_search_form');
        $IdProgram = $this->getAttrib('IdProgram');
        
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
		
       /* $field1 = new Zend_Form_Element_Text('field1');
	//$field1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	$field1->setAttrib('class', 'span-7')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
            //->removeDecorator('HtmlTag');*/
        
        $field2 = new Zend_Form_Element_Select('field2');
	$field2->removeDecorator("DtDdWrapper");
	$field2->setAttrib('required',"false");
        $field2->setAttrib('id', 'programscheme');
        $field2->setAttrib('class', 'span-7');
	$field2->removeDecorator("Label");
	//$field2->removeDecorator('HtmlTag');
	//$field2->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
        $field2->addMultiOption('', '--All--');
        
        $progModel = new Application_Model_DbTable_ProgramScheme();
        
        if ($IdProgram){
            $defModel = new App_Model_General_DbTable_Definationms();
            $progSchemeList = $progModel->fnGetProgramschemeListBasedOnProgId($IdProgram);
            foreach($progSchemeList as $progSchemeListloop){
                $mod_of_study = $defModel->getData($progSchemeListloop['mode_of_study']);
                $mod_of_program = $defModel->getData($progSchemeListloop['mode_of_program']);
                $programType = $defModel->getData($progSchemeListloop['program_type']);

                $field2->addMultiOption($progSchemeListloop['IdProgramScheme'], $mod_of_study['DefinitionDesc'].' '.$mod_of_program['DefinitionDesc'].' '. $programType['DefinitionDesc']);
            }
        }
        
        $field3 = new Zend_Form_Element_Select('field3');
	$field3->removeDecorator("DtDdWrapper");
	$field3->setAttrib('required',"false");
        $field3->setAttrib('class', 'span-7');
        $field3->setAttrib('onchange', 'get_programscheme()');
        $field3->setAttrib('id', 'program');
	$field3->removeDecorator("Label");
	//$field3->removeDecorator('HtmlTag');
	//$field3->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
        $progList = $progModel->fnGetProgramListGotScheme();
        
        $field3->addMultiOption('', '--All--');
        
        if (count($progList)>0){
            foreach ($progList as $progLoop){
                $field3->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']." - ".$progLoop['ProgramCode']);
            }
        }
        
        $field4 = new Zend_Form_Element_Select('field4');
	$field4->removeDecorator("DtDdWrapper");
        $field4->setAttrib('id', 'stdCtgy');
	$field4->setAttrib('required',"false");
        $field4->setAttrib('class', 'span-7');
	$field4->removeDecorator("Label");
	$field4->removeDecorator('HtmlTag');
	//$field4->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
        $defModel = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $stdCtgyList = $defModel->getList(18);
        
        $field4->addMultiOption('', '--All--');
        
        if (count($stdCtgyList)>0){
            foreach ($stdCtgyList as $stdCtgyLoop){
                $field4->addMultiOption($stdCtgyLoop['key'], $stdCtgyLoop['name']);
            }
        }
	
        $submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
	$submit->label = $gstrtranslate->_("Search");
	$submit->removeDecorator("DtDdWrapper");
	$submit->removeDecorator("Label");
	$submit->removeDecorator('HtmlTag')
            ->class = "NormalBtn";
        
        $Clear = new Zend_Form_Element_Submit('Clear');
	$Clear->dojotype="dijit.form.Button";
	$Clear->label = $gstrtranslate->_("Clear");
	$Clear->setAttrib('class', 'NormalBtn')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');
        
        $this->addElements(array(
            $submit, 
            $Clear,
            //$field1,
            $field2,
            $field3,
            $field4
        ));
		
    }
}
?>