<?php 
class Application_Form_EditItemTagging extends Zend_Form{
	
    public function init()
    {
        $this->setMethod('post');
	$this->setAttrib('id', 'edit_item_tagging_form');
        
        $this->addElement('textarea','tagDesc', array(
            'label'=>'Desciption'
	));
        
        $this->addElement('checkbox', 'view', array(
            'label' => 'View',
            'name' => 'view',
            'checkedValue' => '1',
            'uncheckedValue' => '0',
            'disableHidden' => true
        ));
        
        $this->addElement('checkbox', 'compulsory', array(
            'label' => 'Compulsory',
            'name' => 'compulsory',
            'checkedValue' => '1',
            'uncheckedValue' => '0',
            'disableHidden' => true
        ));
        
        $this->addElement('checkbox', 'enable', array(
            'label' => 'Enable',
            'name' => 'enable',
            'checkedValue' => '1',
            'uncheckedValue' => '0',
            'disableHidden' => true
        ));
		
	//button
	$this->addElement('submit', 'save_item_tagging', array(
          'label'=>'Save',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'application', 'controller'=>'application-initial-configuration','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save_item_tagging','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	));		
    }
}
?>