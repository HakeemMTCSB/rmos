<?php 
class Application_Form_EditSection extends Zend_Form{
	
    public function init()
    {
        $this->setMethod('post');
	$this->setAttrib('id', 'edit_section_form');
		
	$this->addElement('text','name', array(
            'label'=>'Section Name',
            'required'=>'true'
	));
        
        $this->addElement('text','desc', array(
            'id'=>'malay',
            'label'=>'Malay Section Name'
	));
		
	$this->addElement('textarea','instruction', array(
            'id'=>'instruction',
            'label'=>'Instruction'
	));
		
	$this->addElement('radio','status', array(
            'label'=>'Status'));
		
	$this->status->addMultiOptions(array(
            '1' => 'Active',
            '0' => 'No'
	))->setSeparator('&nbsp;&nbsp;')->setValue("1");
		
	//button
	$this->addElement('submit', 'save', array(
          'label'=>'Save',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'application', 'controller'=>'application-initial-configuration','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	));
		
    }
}
?>