<?
class Application_Form_Main extends Zend_Form {

	protected $_lang;
	protected $_program;
	protected $_section;
	
	public function setLang($value) {
		$this->_lang = $value;
	}
	
	public function setProgram($value) {
		$this->_program = $value;
	}
	
	public function setSection($value) {
		$this->_section = $value;
	}

    
	public function init(){
		
		
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		$this->setName('application');
		$this->setMethod('post');	
		$this->setAttrib('enctype', 'multipart/form-data');
		
		
		 $this->addElement('text','ttt', array(	
				 	'decorators'=>array(
						'ViewHelper',
						'Description',
						'Errors'),
				 
					));
					
		$appInitConfigDB = new Application_Model_DbTable_ApplicationInitialConfig();;
		$listItem = $appInitConfigDB->getItem($this->_program,$this->_section);
				
		 foreach($listItem as $data){
		 	 $compulsory = $data['compulsory'];
		 	 $type = $data['form_type'];
		 	 $variable = $data['variable'];
		 	 $label = $data['name'];
		 	 
		 	 
		 	 if($type == 'select' || $type == 'text'){
		 	 
		 	 	 $functionInit = "initCap($variable);";
				 $this->addElement($type,$variable, array(
				 	'required'=>$compulsory,
				 	'decorators'=>array(
						'ViewHelper',
						'Description',
						'Errors'),
				 		'attribs'    => array('onkeyup'=>$functionInit)
				 
					));

		 	 }elseif($type == 'textarea'){
		 	 
		 	 	 $functionInit = "initCap($variable);";
				 $this->addElement($type,$variable, array(
				 	'required'=>$compulsory,
				 	'decorators'=>array(
						'ViewHelper',
						'Description',
						'Errors'),
				 		'attribs'    => array('onkeyup'=>$functionInit,'cols'=>50,'rows'=>4)
				 
					));

		 	 }elseif ($type == 'date'){
		 	 	
		 	 	$this->addElement('text',$variable, array(
				 	'required'=>$compulsory,
		 	 	    'class'=>'datepicker',		 	 		
		 	 		'decorators'=>array(
						'ViewHelper',
						'Description',
						'Errors'),
				 
					));
				
		 	 }elseif($type == 'radio'){
		 	 
				 $this->addElement($type,$variable, array(
				 	'required'=>$compulsory,
				 	'decorators'=>array(
						'ViewHelper',
						'Description',
						'Errors'),
				 
					));

		 	 }elseif($type == 'calendar'){
		 	 
				 $this->addElement('text',$variable, array(
				 	'required'=>$compulsory,
				 	'class'=>'datepicker',
				 	'decorators'=>array(
						'ViewHelper',
						'Description',
						'Errors'),
				 
					));

		 	 }elseif($type == 'checkbox'){
			 	$element = new Zend_Form_Element_MultiCheckbox($variable);
				$this->addElement($element);
				
		 	 }elseif($type == 'month'){
		 	 
				 $this->addElement('text',$variable, array(
				 	'required'=>$compulsory,
				 	'decorators'=>array(
						'ViewHelper',
						'Description',
						'Errors'),
				 'onfocus'=>'popupMonthCalendar('.$variable.');',
				 
					));

		 	 }
		 	 
		 	$this->removeDecorator("DtDdWrapper");
	        $this->removeDecorator("Label");
	        $this->removeDecorator('HtmlTag');
		 	
		 }
		
		//button
		$this->addElement('submit', 'save', array(
		'label'=>'submit',
		'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td', 'colspan'=>'5',
               'align'=>'left', 'openOnly'=>true)),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
               )
		));

		
		$this->addElement('submit', 'cancel', array(
		'label'=>'cancel',
		'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
               'closeOnly'=>true)),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
               ),
		'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'default', 'controller'=>'online-application','action'=>'biodata'),'default',true) . "'; return false;"
		));


		$this->setDecorators(array(

		'FormElements',

		array(array('data'=>'HtmlTag'),array('tag'=>'table')),

		'Form'



		));





	}
}
?>