<?php
class Application_Form_BlockApplicantSearch extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','form_search_block_applicant');

		//name
		$this->addElement('text','IcNo', array(
			'label'=>'IC Number',
			'class'=>'input-txt'
		));

        $this->addElement('text','Name', array(
            'label'=>'Name',
            'class'=>'input-txt'
        ));

        //button

		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}