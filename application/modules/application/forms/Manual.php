<?php
class Application_Form_Manual extends Zend_Form
{	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','manual_form');

      
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Save',
		  'class'=>'btn-submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('button', 'cancel', array(
          'label'=>'Cancel',
		  'class'=>'btn-cancel',
          'decorators'=>array('ViewHelper'),
          'onclick'=>"window.location ='" . $this->getView()->url(array('module'=>'application', 'controller'=>'index','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>