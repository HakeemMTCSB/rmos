<?php 
class Application_Form_ChecklistVerification extends Zend_Dojo_Form{
	
    public function init()
    {   
        $this->setMethod('post');
        
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
		
        $field1 = new Zend_Form_Element_Text('field1');
	$field1->setAttrib('class', 'span-7')
            ->setAttrib('name', 'comment[]')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $field2 = new Zend_Form_Element_Select('field2', array('multiple' => false ));
	$field2->removeDecorator("DtDdWrapper");
	$field2->setAttrib('required',"false");
        $field2->setAttrib('class', 'span-7');
	$field2->removeDecorator("Label");
        $field2->setIsArray(true);
        $field2->setRegisterInArrayValidator(false);
        
        $field2->addMultiOption('', '-- Select --');
        $field2->addMultiOption('1', 'Waiting');
        $field2->addMultiOption('2', 'Comfirm');
        
        $field7  = new Zend_Form_Element_Checkbox('field7');
        $field7->setAttrib('name', 'check[]');
        $field7->removeDecorator("DtDdWrapper");
        $field7->removeDecorator("Label");
	
        $submit = new Zend_Form_Element_Submit('Save');
        $submit->dojotype="dijit.form.Button";
	$submit->label = $gstrtranslate->_("Save");
	$submit->removeDecorator("DtDdWrapper");
	$submit->removeDecorator("Label");
	$submit->removeDecorator('HtmlTag')
            ->class = "NormalBtn";
        
        $email = new Zend_Form_Element_Submit('Email');
	$email->dojotype="dijit.form.Button";
	$email->label = $gstrtranslate->_("Email");
        $email->setAttrib('onclick', 'send_email()');
	$email->setAttrib('class', 'NormalBtn')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');
        
        $this->addElements(array(
            $submit, 
            $email,
            $field1,
            $field2,
            $field7
        ));
		
    }
}
?>