<?php

class Application_Form_Paymentplan extends Zend_Dojo_Form
{ //Formclass for the user module
    public function init()
    {
        $gstrtranslate = Zend_Registry::get('Zend_Translate');

        $id = new Zend_Form_Element_Hidden('id');
        $id->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $name = new Zend_Form_Element_Text('name');
        $name->setAttrib('dojoType', "dijit.form.ValidationTextBox")
            ->setAttrib('required', "true")
            ->setAttrib('maxlength', '150')
            //->setAttrib('autocapitalize','on')
            //->setAttrib('onchange','toUpperCase(this.value);')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $MalayName = new Zend_Form_Element_Text('MalayName');
        $MalayName->setAttrib('dojoType', "dijit.form.ValidationTextBox")
            ->setAttrib('required', "true")
            ->setAttrib('maxlength', '150')
            //->setAttrib('autocapitalize','on')
            //->setAttrib('onchange','toUpperCase(this.value);')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $code = new Zend_Form_Element_Text('code');
        $code->setAttrib('dojoType', "dijit.form.ValidationTextBox")
            ->setAttrib('required', "false")
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');


        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype = "dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->setAttrib('class', 'NormalBtn');
        $Save->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Search = new Zend_Form_Element_Submit('Search');
        $Search->setAttrib('id', 'search')
            ->setAttrib('name', 'search')
            ->setAttrib('class', 'NormalBtn')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Add = new Zend_Form_Element_Button('Add');
        $Add->setAttrib('class', 'NormalBtn');
        $Add->setAttrib('dojoType', "dijit.form.Button");
        $Add->setAttrib('OnClick', 'addSubjectentry()')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $clear = new Zend_Form_Element_Button('Clear');
        $clear->setAttrib('class', 'NormalBtn');
        $clear->setAttrib('dojoType', "dijit.form.Button");
        $clear->label = $gstrtranslate->_("Clear");
        $clear->setAttrib('OnClick', 'clearpageAdd()');
        $clear->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Active = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType', "dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($id,
            $name,
            $MalayName,
            $code,
            $UpdDate,
            $UpdUser,
            $Save,
            $Search,
            $Active,
            $Add,
            $clear));
    }
}