<?php 
class Application_Form_EditApplication extends Zend_Dojo_Form{
	
    public function init()
    {   
        $this->setMethod('post');
        $IdProgram = $this->getAttrib('ProgramId');
        $IdProgramScheme = $this->getAttrib('ProgramSchemeId');
        $StdCtgy = $this->getAttrib('StdCtgy');
        $statuscode = $this->getAttrib('statuscode');
        
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        $reason = new Zend_Form_Element_Select('at_reason');
	$reason->removeDecorator("DtDdWrapper");
	//$reason->setAttrib('required',"false");
        $reason->setAttrib('class', 'span-7');
	$reason->removeDecorator("Label");
        
        $definationDB = new App_Model_General_DbTable_Definationms();
        $reasonList = $definationDB->getDataByType(141);
        
        $reason->addMultiOption('', '-- Select --');
        
        if (count($reasonList)>0){
            foreach ($reasonList as $reasonLoop){
                $reason->addMultiOption($reasonLoop['idDefinition'], $reasonLoop['DefinitionDesc']);
            }
        }
        
        $status = new Zend_Form_Element_Select('at_status');
	$status->removeDecorator("DtDdWrapper");
	$status->setAttrib('required',"false");
        $status->setAttrib('class', 'span-7');
        
        if ($statuscode == 601){
            $status->setAttrib('disable', 'true');
        }
	
        $status->removeDecorator("Label");
        $statusModel = new Application_Model_DbTable_Status();
        $statusList = $statusModel->fnGetStatusListByProgramAndStdCtgy($IdProgramScheme, $StdCtgy);
        
        switch ($statuscode){
            case 591:
                if (count($statusList)>0){
                    foreach ($statusList as $statusLoop){
                        if ($statusLoop['idDefinition']==591 || $statusLoop['idDefinition']==602){
                            $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
                        }
                    }
                }
                break;
            case 592: //entry
                if (count($statusList)>0){
                    foreach ($statusList as $statusLoop){
                        if ($statusLoop['idDefinition']==592 || 
                            $statusLoop['idDefinition']==594 ||
                            $statusLoop['idDefinition']==596 || 
                            $statusLoop['idDefinition']==595 || 
                            $statusLoop['idDefinition']==597 || 
                            $statusLoop['idDefinition']==598 || 
                            $statusLoop['idDefinition']==602){
                            $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
                        }
                    }
                }
            break;
            case 595: //incomplete
                if (count($statusList)>0){
                    foreach ($statusList as $statusLoop){
                        if ($statusLoop['idDefinition']==595 || 
                            $statusLoop['idDefinition']==592 || 
                            $statusLoop['idDefinition']==594 ||
                            $statusLoop['idDefinition']==596 || 
                            $statusLoop['idDefinition']==597 || 
                            $statusLoop['idDefinition']==598 || 
                            $statusLoop['idDefinition']==602){
                            $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
                        }
                    }
                }
            break;
            case 596: //complete
                if (count($statusList)>0){
                    foreach ($statusList as $statusLoop){
                        if ($statusLoop['idDefinition']==596 || 
                            $statusLoop['idDefinition']==592 || 
                            $statusLoop['idDefinition']==594 ||
                            $statusLoop['idDefinition']==595 || 
                            $statusLoop['idDefinition']==597 || 
                            $statusLoop['idDefinition']==598 || 
                            $statusLoop['idDefinition']==602){
                            $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
                        }
                    }
                }
            break;
            case 597: //shorlisted
                if (count($statusList)>0){
                    foreach ($statusList as $statusLoop){
                        if ($statusLoop['idDefinition']==597 || 
                            $statusLoop['idDefinition']==592 || 
                            $statusLoop['idDefinition']==594 ||
                            $statusLoop['idDefinition']==595 || 
                            $statusLoop['idDefinition']==596 || 
                            $statusLoop['idDefinition']==598 || 
                            $statusLoop['idDefinition']==602){
                            $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
                        }
                    }
                }
            break;
            case 598: //kiv
                if (count($statusList)>0){
                    foreach ($statusList as $statusLoop){
                        if ($statusLoop['idDefinition']==598 || 
                            $statusLoop['idDefinition']==592 || 
                            $statusLoop['idDefinition']==594 ||
                            $statusLoop['idDefinition']==595 || 
                            $statusLoop['idDefinition']==596 || 
                            $statusLoop['idDefinition']==597 || 
                            $statusLoop['idDefinition']==602){
                            $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
                        }
                    }
                }
            break;
            case 593: //offered
                if (count($statusList)>0){
                    foreach ($statusList as $statusLoop){
                        if ($statusLoop['idDefinition']==593 || 
                            $statusLoop['idDefinition']==592 || 
                            $statusLoop['idDefinition']==594 ||
                            $statusLoop['idDefinition']==595 || 
                            $statusLoop['idDefinition']==596 || 
                            $statusLoop['idDefinition']==597 || 
                            $statusLoop['idDefinition']==598 ||
                            $statusLoop['idDefinition']==599 ||
                            $statusLoop['idDefinition']==600 ||
                            $statusLoop['idDefinition']==602){
                            $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
                        }
                    }
                }
            break;
            case 599: //accept
                if (count($statusList)>0){
                    foreach ($statusList as $statusLoop){
                        if ($statusLoop['idDefinition']==599 || 
                            $statusLoop['idDefinition']==592 ||
                            $statusLoop['idDefinition']==594 ||
                            $statusLoop['idDefinition']==595 || 
                            $statusLoop['idDefinition']==596 || 
                            $statusLoop['idDefinition']==597 ||
                            $statusLoop['idDefinition']==598 ||
                            $statusLoop['idDefinition']==600 ||
                            $statusLoop['idDefinition']==602){
                            $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
                        }
                    }
                }
            break;
            case 600: //declined
                if (count($statusList)>0){
                    foreach ($statusList as $statusLoop){
                        if ($statusLoop['idDefinition']==600 || 
                            $statusLoop['idDefinition']==592 || 
                            $statusLoop['idDefinition']==594 ||
                            $statusLoop['idDefinition']==595 || 
                            $statusLoop['idDefinition']==596 || 
                            $statusLoop['idDefinition']==597 ||
                            $statusLoop['idDefinition']==598 ||
                            $statusLoop['idDefinition']==599 ||
                            $statusLoop['idDefinition']==602){
                            $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
                        }
                    }
                }
            break;
            case 594: //rejected
                if (count($statusList)>0){
                    foreach ($statusList as $statusLoop){
                        if ($statusLoop['idDefinition']==594 || 
                            $statusLoop['idDefinition']==592 || 
                            $statusLoop['idDefinition']==600 ||
                            $statusLoop['idDefinition']==595 || 
                            $statusLoop['idDefinition']==596 || 
                            $statusLoop['idDefinition']==597 ||
                            $statusLoop['idDefinition']==598 ||
                            $statusLoop['idDefinition']==599 ||
                            $statusLoop['idDefinition']==602){
                            $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
                        }
                    }
                }
            break;
            default :
                if (count($statusList)>0){
                    foreach ($statusList as $statusLoop){
                        $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
                    }
                }
            break;
        }
        
        $program = new Zend_Form_Element_Select('IdProgram');
	$program->removeDecorator("DtDdWrapper");
	$program->setAttrib('required',"false");
        $program->setAttrib('id', 'program');
        $program->setAttrib('onchange', 'get_programscheme()');
        $program->setAttrib('class', 'span-7');
	$program->removeDecorator("Label");
        
        $progModel = new Application_Model_DbTable_ProgramScheme();
        $progList = $progModel->fnGetProgramList();
        
        $program->addMultiOption('', '--Select--');
        
        if (count($progList)>0){
            foreach ($progList as $progLoop){
                $program->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
            }
        }
        
        $programscheme = new Zend_Form_Element_Select('IdProgramScheme');
	$programscheme->removeDecorator("DtDdWrapper");
	$programscheme->setAttrib('required',"false");
        $programscheme->setAttrib('id', 'programscheme');
        $programscheme->setAttrib('class', 'span-7');
	$programscheme->removeDecorator("Label");
        
        $programscheme->addMultiOption('', '--Select--');
        
        if ($IdProgram){
            $defModel = new App_Model_General_DbTable_Definationms();
            $progSchemeList = $progModel->fnGetProgramschemeListBasedOnProgId($IdProgram);
            foreach($progSchemeList as $progSchemeListloop){
                $mod_of_study = $defModel->getData($progSchemeListloop['mode_of_study']);
                $mod_of_program = $defModel->getData($progSchemeListloop['mode_of_program']);
                $programType = $defModel->getData($progSchemeListloop['program_type']);

                $programscheme->addMultiOption($progSchemeListloop['IdProgramScheme'], $mod_of_study['DefinitionDesc'].' '.$mod_of_program['DefinitionDesc'].' '. $programType['DefinitionDesc']);
            }
        }
        
        $submit = new Zend_Form_Element_Submit('save');
        $submit->dojotype="dijit.form.Button";
	$submit->label = $gstrtranslate->_("Save");
	$submit->removeDecorator("DtDdWrapper");
	$submit->removeDecorator("Label");
	$submit->removeDecorator('HtmlTag')
            ->class = "NormalBtn";

        
        $this->addElements(array(
            $reason,
            $status,
            $program,
            $programscheme,
            $submit
        ));
    }
}
?>