<?php
class Application_Form_Placementtest extends Zend_Dojo_Form { //Formclass for the user module
	public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate');
			
		$IdPlacementTest = new Zend_Form_Element_Hidden('IdPlacementTest');
		$IdPlacementTest->removeDecorator("DtDdWrapper");
		$IdPlacementTest->removeDecorator("Label");
		$IdPlacementTest->removeDecorator('HtmlTag');

		$IdPlacementTestBranch = new Zend_Form_Element_Hidden('IdPlacementTestBranch');
		$IdPlacementTestBranch->removeDecorator("DtDdWrapper");
		$IdPlacementTestBranch->removeDecorator("Label");
		$IdPlacementTestBranch->removeDecorator('HtmlTag');

		$IdPlacementTestComponent = new Zend_Form_Element_Hidden('IdPlacementTestComponent');
		$IdPlacementTestComponent->removeDecorator("DtDdWrapper");
		$IdPlacementTestComponent->removeDecorator("Label");
		$IdPlacementTestComponent->removeDecorator('HtmlTag');

		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper");
		$UpdDate->removeDecorator("Label");
		$UpdDate->removeDecorator('HtmlTag');

		$UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->removeDecorator("DtDdWrapper");
		$UpdUser->removeDecorator("Label");
		$UpdUser->removeDecorator('HtmlTag');

		/*$IdProgram = new Zend_Dojo_Form_Element_FilteringSelect('IdProgram');
		 $IdProgram->removeDecorator("DtDdWrapper");
		$IdProgram->setAttrib('required',"true") ;
		$IdProgram->removeDecorator("Label");
		$IdProgram->removeDecorator('HtmlTag');
		$IdProgram->setRegisterInArrayValidator(false);
		$IdProgram->setAttrib('dojoType',"dijit.form.FilteringSelect");*/

		$PlacementTestName = new Zend_Form_Element_Text('PlacementTestName');
		$PlacementTestName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PlacementTestName->setAttrib('required',"true")
		->setAttrib('maxlength','100')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setAttrib('propercase',"true");

		$copyPlacementTestName = new Zend_Form_Element_Text('CopyPlacementTestName');
		$copyPlacementTestName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$copyPlacementTestName->setAttrib('required',"true")
		->setAttrib('maxlength','100')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');


		$PlacementTestDate = new Zend_Dojo_Form_Element_DateTextBox('PlacementTestDate');
		$PlacementTestDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		//$PlacementTestDate->setAttrib('constraints', "$dateofbirth");
		$PlacementTestDate->setAttrib('required',"true");
		$PlacementTestDate->removeDecorator("DtDdWrapper");
		$PlacementTestDate->setAttrib('title',"dd-mm-yyyy");
		$PlacementTestDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$PlacementTestDate->removeDecorator("Label");
		$PlacementTestDate->removeDecorator('HtmlTag');

		$copyPlacementTestDate = new Zend_Dojo_Form_Element_DateTextBox('CopyPlacementTestDate');
		$copyPlacementTestDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		//$PlacementTestDate->setAttrib('constraints', "$dateofbirth");
		$copyPlacementTestDate->setAttrib('required',"true");
		$copyPlacementTestDate->removeDecorator("DtDdWrapper");
		$copyPlacementTestDate->setAttrib('title',"dd-mm-yyyy");
		$copyPlacementTestDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$copyPlacementTestDate->removeDecorator("Label");
		$copyPlacementTestDate->removeDecorator('HtmlTag');



		$PlacementTestTime = new Zend_Form_Element_Text('PlacementTestTime');
		//$PlacementTestTime->setAttrib('maxlength','50');
		$PlacementTestTime->setAttrib('required',"true");
		$PlacementTestTime->removeDecorator("DtDdWrapper");
		$PlacementTestTime->removeDecorator("Label");
		$PlacementTestTime->removeDecorator('HtmlTag');
		$PlacementTestTime->setAttrib('dojoType',"dijit.form.TimeTextBox")
		->setAttrib('constraints',"{timePattern: 'HH:mm'}");

		$copyPlacementTestTime = new Zend_Form_Element_Text('CopyPlacementTestTime');
		//$PlacementTestTime->setAttrib('maxlength','50');
		$copyPlacementTestTime->setAttrib('required',"true");
		$copyPlacementTestTime->removeDecorator("DtDdWrapper");
		$copyPlacementTestTime->removeDecorator("Label");
		$copyPlacementTestTime->removeDecorator('HtmlTag');
		$copyPlacementTestTime->setAttrib('dojoType',"dijit.form.TimeTextBox")
		->setAttrib('constraints',"{timePattern: 'HH:mm'}");




			
		$IdCollege = new Zend_Form_Element_MultiCheckbox('IdCollege');
		$IdCollege->setAttrib('required',"true");
		$IdCollege->removeDecorator("DtDdWrapper");
		$IdCollege->removeDecorator("Label");
		$IdCollege->removeDecorator('HtmlTag');
		$IdCollege->setAttrib('dojoType',"dijit.form.CheckBox");
		$IdCollege->class = "IdCollege";

		//		$IdProgram = new Zend_Form_Element_MultiCheckbox('IdProgram');
		// 		$IdProgram->setAttrib('required',"true");
		//        $IdProgram->removeDecorator("DtDdWrapper");
		//        $IdProgram->removeDecorator("Label");
		//        $IdProgram->removeDecorator('HtmlTag');
		//		$IdProgram->setAttrib('dojoType',"dijit.form.CheckBox");
		//		$IdProgram->class = "IdProgram";

		$Zip = new Zend_Form_Element_Text('Zip');
		$Zip->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Zip->setAttrib('maxlength','20');
		$Zip->removeDecorator("DtDdWrapper");
		$Zip->removeDecorator("Label");
		$Zip->removeDecorator('HtmlTag');

		$City = new Zend_Dojo_Form_Element_FilteringSelect('City');
		$City->removeDecorator("DtDdWrapper");
		$City->removeDecorator("Label");
		$City->removeDecorator('HtmlTag');
		$City->setAttrib('required',"true");
		$City->setRegisterInArrayValidator(false);
		$City->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$IdProgram = new Zend_Dojo_Form_Element_FilteringSelect('IdProgram');
		$IdProgram->removeDecorator("DtDdWrapper");
		$IdProgram->removeDecorator("Label");
		$IdProgram->removeDecorator('HtmlTag');
		$IdProgram->setAttrib('required',"false");
		$IdProgram->setAttrib('onchange',"getScheme(this)");
		$IdProgram->setRegisterInArrayValidator(false);
		$IdProgram->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$IdScheme = new Zend_Dojo_Form_Element_FilteringSelect('IdScheme');
		$IdScheme->removeDecorator("DtDdWrapper");
		$IdScheme->removeDecorator("Label");
		$IdScheme->removeDecorator('HtmlTag');
		$IdScheme->setAttrib('required',"false");
		$IdScheme->setRegisterInArrayValidator(false);
		$IdScheme->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$Intake = new Zend_Dojo_Form_Element_FilteringSelect('Intake');
		$Intake->removeDecorator("DtDdWrapper");
		$Intake->removeDecorator("Label");
		$Intake->removeDecorator('HtmlTag');
		$Intake->setAttrib('required',"true");
		$Intake->setRegisterInArrayValidator(false);
		$Intake->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$PlacementTestType = new Zend_Dojo_Form_Element_FilteringSelect('PlacementTestType');
		$PlacementTestType->removeDecorator("DtDdWrapper");
		$PlacementTestType->removeDecorator("Label");
		$PlacementTestType->removeDecorator('HtmlTag');
		$PlacementTestType->setAttrib('required',"true");
		$PlacementTestType->setRegisterInArrayValidator(false);
		$PlacementTestType->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$MarkingType = new Zend_Dojo_Form_Element_FilteringSelect('MarkingType');
		$MarkingType->removeDecorator("DtDdWrapper");
		$MarkingType->removeDecorator("Label");
		$MarkingType->removeDecorator('HtmlTag');
		$MarkingType->setAttrib('required',"false");
		$MarkingType->setRegisterInArrayValidator(false);
		$MarkingType->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$ComponentName = new Zend_Form_Element_Text('ComponentName');
		$ComponentName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ComponentName //->setAttrib('required',"true")
		->setAttrib('maxlength','100')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$ComponentWeightage = new Zend_Form_Element_Text('ComponentWeightage',array('regExp'=>"^-?[0-9]{0,2}(\.[0-9]{1,2})?$|^-?(100)(\.[0]{1,2})?$",'invalidMessage'=>"Weightage Should not exceed 100%"));
		$ComponentWeightage->setAttrib('maxlength','12')
		->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
			
		$ComponentTotalMarks = new Zend_Form_Element_Text('ComponentTotalMarks',array('regExp'=>"[0-9]*[.]?[0-9]+",'invalidMessage'=>"Not a Valid Amount"));
		$ComponentTotalMarks->setAttrib('maxlength','12')
		->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$Addr1 = new Zend_Form_Element_Text('Addr1');
		$Addr1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Addr1->setAttrib('required',"true")
		->setAttrib('maxlength','20')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setAttrib('propercase',"true");

		$Addr2 = new Zend_Form_Element_Text('Addr2');
		$Addr2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Addr2->setAttrib('required',"false")
		->setAttrib('maxlength','20')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag')
		->setAttrib('propercase',"true");

		$idState = new Zend_Dojo_Form_Element_FilteringSelect('idState');
		$idState->removeDecorator("DtDdWrapper");
		$idState->setAttrib('required',"true") ;
		$idState->removeDecorator("Label");
		$idState->removeDecorator('HtmlTag');
		$idState->setAttrib('OnChange', 'fnGetStateCityListofUniversity');
		$idState->setRegisterInArrayValidator(false);
		$idState->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$idCountry = new Zend_Dojo_Form_Element_FilteringSelect('idCountry');
		$idCountry->removeDecorator("DtDdWrapper");
		$idCountry->setAttrib('required',"true") ;
		$idCountry->removeDecorator("Label");
		$idCountry->removeDecorator('HtmlTag');
		$idCountry->setAttrib('OnChange', "fnGetCountryStateList(this,'State')");
		$idCountry->setRegisterInArrayValidator(false);
		$idCountry->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$Active  = new Zend_Form_Element_Checkbox('Active');
		$Active->setAttrib('dojoType',"dijit.form.CheckBox");
		$Active->setvalue('1');
		$Active->removeDecorator("DtDdWrapper");
		$Active->removeDecorator("Label");
		$Active->removeDecorator('HtmlTag');

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->dojotype="dijit.form.Button";
		$Save->label = $gstrtranslate->_("Save");
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator("Label");
		$Save->removeDecorator('HtmlTag')
		->class = "NormalBtn";
			
		$countrycode = new Zend_Form_Element_Text('countrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$countrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$countrycode->setAttrib('maxlength','3');
		$countrycode->setAttrib('style','width:30px');
		$countrycode->removeDecorator("DtDdWrapper");
		$countrycode->removeDecorator("Label");
		$countrycode->removeDecorator('HtmlTag');

		$statecode = new Zend_Form_Element_Text('statecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$statecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$statecode->setAttrib('maxlength','2');
		$statecode->setAttrib('style','width:30px');
		$statecode->removeDecorator("DtDdWrapper");
		$statecode->removeDecorator("Label");
		$statecode->removeDecorator('HtmlTag');


		$Phone = new Zend_Form_Element_Text('Phone1',array('regExp'=>"[0-9]+",'invalidMessage'=>"Not a valid Home Phone No."));
		$Phone->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Phone->setAttrib('maxlength','9');
		$Phone->setAttrib('style','width:93px');
		$Phone->removeDecorator("DtDdWrapper");
		$Phone->removeDecorator("Label");
		$Phone->removeDecorator('HtmlTag');
			
			
			
		$Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class', 'NormalBtn')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');



		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
		$Add->label = $gstrtranslate->_("Add").' '.$gstrtranslate->_("Row");
		$Add->setAttrib('OnClick', 'AddPlacementTestComponentDetails()');
		$Add->setAttrib('class', 'NormalBtn')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');
			
		$Add1 = new Zend_Form_Element_Button('Add1');
		$Add1->dojotype="dijit.form.Button";
		$Add1->label = $gstrtranslate->_("Add").' '.$gstrtranslate->_("Row");
		$Add1->setAttrib('OnClick', 'AddProgramSchemeDetails()');
		$Add1->setAttrib('class', 'NormalBtn')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');
			
		$MinMark = new Zend_Form_Element_Text('MinMark',array('regExp'=>"[0-9]*[.]?[0-9]+",'invalidMessage'=>"Not a Valid Amount"));
		$MinMark->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$MinMark->setAttrib('required',"true")
		->setAttrib('maxlength','100')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$TotalMark = new Zend_Form_Element_Text('TotalMark',array('regExp'=>"[0-9]*[.]?[0-9]+",'invalidMessage'=>"Not a Valid Amount"));
		$TotalMark->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$TotalMark->setAttrib('required',"true")
		->setAttrib('maxlength','100')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
		$PassingMark = new Zend_Form_Element_Text('PassingMark',array('regExp'=>"[0-9]*[.]?[0-9]+",'invalidMessage'=>"Not a Valid Amount"));
		$PassingMark->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PassingMark->setAttrib('required',"true")
		->setAttrib('maxlength','100')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$MinimumMark = new Zend_Form_Element_Text('MinimumMark',array('regExp'=>"[0-9]*[.]?[0-9]+",'invalidMessage'=>"Not a Valid Amount"));
		$MinimumMark->setAttrib('maxlength','12')
		->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

			
		$MarksBasedOn  = new Zend_Form_Element_Radio('MarksBasedOn');
		$MarksBasedOn->addMultiOptions(array('0' => 'Total Marks','1' => 'Individual Marks'))
		->setvalue('1')
		->setSeparator('&nbsp;')
		->setAttrib('dojoType',"dijit.form.RadioButton")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$IdVenue = new Zend_Form_Element_Select('IdVenue');
		$IdVenue->removeDecorator("DtDdWrapper")
		->setAttrib('dojoType',"dijit.form.FilteringSelect")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
			
			
		$Year = new Zend_Dojo_Form_Element_FilteringSelect('Year');
		$Year->setAttrib('dojoType',"dijit.form.FilteringSelect")
		->setAttrib('required',"true");
		$Year->removeDecorator("DtDdWrapper");
		$Year->removeDecorator("Label");
		$Year->removeDecorator('HtmlTag');

			
		$Apptype = new Zend_Dojo_Form_Element_FilteringSelect('Apptype');
		$Apptype->setAttrib('required',"true");
		$Apptype->removeDecorator("DtDdWrapper");
		$Apptype->removeDecorator("Label");
		$Apptype->removeDecorator('HtmlTag');
		$Apptype->setRegisterInArrayValidator(false);
		$Apptype->setAttrib('dojoType',"dijit.form.FilteringSelect");
			
		$EnterNo = new Zend_Form_Element_Text('EnterNo');
		$EnterNo->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('required',"true") ;
		$EnterNo->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$Copy = new Zend_Form_Element_Submit('Copy');
		$Copy->label = $gstrtranslate->_("Copy");
		$Copy->dojotype="dijit.form.Button";
		$Copy->removeDecorator("DtDdWrapper");
		$Copy->removeDecorator('HtmlTag')
		->class = "NormalBtn";


		$FromPlacementTest = new Zend_Dojo_Form_Element_FilteringSelect('FromPlacementTest');
		$FromPlacementTest->removeDecorator("DtDdWrapper");
		$FromPlacementTest->setAttrib('required',"true");
		$FromPlacementTest->removeDecorator("Label");
		$FromPlacementTest->removeDecorator('HtmlTag');
		$FromPlacementTest->setRegisterInArrayValidator(false);
		$FromPlacementTest->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$lobjplacementtest = new Application_Model_DbTable_Placementtest();
		$lobjfromplacementtest = $lobjplacementtest->fnGeteditPlcaementList();
		$FromPlacementTest->addMultiOptions($lobjfromplacementtest);


		//form elements
		$this->addElements(array($IdPlacementTest,$MarksBasedOn,
				$IdPlacementTestComponent,
				$IdProgram,
				$IdCollege,
				$PlacementTestType,
				$Intake,
				$Addr1,
				$Addr2,
				$MarkingType,
				$Zip,
				$IdProgram,
				$IdScheme,
				$City,
				$idCountry,
				$idState,
				$Add1,
				$PlacementTestName,
				$PlacementTestDate,
				$countrycode,
				$statecode,
				$Phone,
				$PlacementTestTime,
				$ComponentName,
				$ComponentWeightage,
				$ComponentTotalMarks,
				$Active,
				$UpdDate,
				$UpdUser,
				$Save,
				$Clear,
				$Add,
				$IdPlacementTestBranch,$MinMark,$MinimumMark,$IdVenue,
				$Year,$Apptype,$EnterNo,$TotalMark,$PassingMark,$copyPlacementTestName,
				$copyPlacementTestDate,$copyPlacementTestTime,$Copy,$FromPlacementTest
		));

	}
}