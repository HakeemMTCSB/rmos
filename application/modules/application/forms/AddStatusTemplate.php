<?php
class Application_Form_AddStatusTemplate extends Zend_Form
{
	
    public function init()
    {
        $this->setMethod('post');
        $type = $this->getAttrib('type');
        $title = $this->getAttrib('title');
            
        if ($type == 604){
            $this->addElement('text','stp_title', 
                array(
                    'label'=>'Title/Subject',
                    'required'=>'true'
                )
            );
        }else{
            $this->addElement('text','stp_title', 
                array(
                    'label'=>'Title/Subject',
                    'required'=>'true',
                    'value'=>$title
                )
            );
        }
		
        $this->addElement('select','stp_tplId', 
            array(
                'label'=>'Template',
                'required'=>'true',
                'onchange'=>'changetpl(this); getTplTags(this);'
            )
        );
        
        $this->stp_tplId->addMultiOption(null,"-- Select --");
        
        $appModel = new Application_Model_DbTable_ApplicationInitialConfig();
        $tplList = $appModel->fnGetTemplateList();
        
        if(count($tplList)>0){
            foreach ($tplList as $list){		
		$this->stp_tplId->addMultiOption($list['tpl_id'],$list['tpl_name']);
            }
	}

        $this->addElement('select','stp_tplType', 
            array(
                'label'=>'Type',
                //'required'=>'false',
		'disable'=>'true'
            )
        );
        
        $this->stp_tplType->addMultiOption(null,"-- Select --");		
	
        $definationDB = new App_Model_General_DbTable_Definationms();
        $category = $definationDB->getDataByType(122);
		
	if(count($category)>0){
            foreach ($category as $list){		
		$this->stp_tplType->addMultiOption($list['idDefinition'],$list['DefinitionDesc']);
            }
	}
        
        $this->addElement('textarea','contentEng', array(
            'id'=>'contentEng'
	));
        
        $this->addElement('textarea','contentMy', array(
            'id'=>'contentMy'
	));
    }
}
