<?php
class Application_Form_UploadBatch extends Zend_Form
{
	protected $_facultyid;
	protected $_programcode;
	protected $_intakeid;
	
	
	public function setFacultyid($facultyid)
	{
		$this->_facultyid = $facultyid;
	}
	
	public function setProgramcode($programcode)
	{
		$this->_programcode = $programcode;
	}

	public function setIntakeid($intakeid)
	{
		$this->_intakeid = $intakeid;
	}
 
	public function init()
	{
		
		$this->setAttrib('enctype', 'multipart/form-data');	
		$this->setAttrib('id','upload_form');		 
		
				 
		$facultyid = $this->createElement('hidden', 'faculty');
        $facultyid->setValue($this->_facultyid);
        
		$programcode = $this->createElement('hidden', 'program_code');
        $programcode->setValue($this->_programcode);
        
        $academicyear = $this->createElement('hidden', 'intake_id');
        $academicyear->setValue($this->_intakeid);
        
        $nomor = $this->createElement('text', 'nomor');
        $nomor->setLabel('Nomor')
              ->setRequired(true);
        
        $decree_date = $this->createElement('text', 'decree_date');
        $decree_date->setLabel('Decree Date')
                    ->setRequired(true);
        
		  
		$filepath = DOCUMENT_PATH."/download/".date("mY")."/";
		//$filepath = "/data/apps/triapp/documents/download/112012/";
		
        //create directory to locate file			
		if (!is_dir($filepath)) {
			 mkdir($filepath, 0775);
		}
		
		 // creating object for Zend_Form_Element_File
		 $filename = new Zend_Form_Element_File('filename');
		 $filename->setLabel('Filename')
				  ->setRequired(true)
				  ->addValidator('Extension', true, 'csv')				
				  ->setDestination($filepath);			  
		
		//academic year
		$acadyear = new Zend_Form_Element_Select('academic_year');
		$acadyear->setLabel('Academic Year')
                 ->setRequired(true);
		
		$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
		$academicYearList = $academicYearDb->getAcademicYear(array('NEXT','FUTURE'));		
    			
		$acadyear->addMultiOption(null,"-- Select Academic Year --");
		foreach ($academicYearList as $list){
			$acadyear->addMultiOption($list['ay_id'],$list['ay_desc']."(".$list['ay_year_status'].")");
		}

	    //registration start date
	    $reg_start_date = $this->createElement('text','registration_start_date', array(
			'label'=>'Registration Start Date',
			'required'=>'true',
	    	'class'=>'from'
	    ));
	    
		//registration end date
		$reg_end_date = $this->createElement('text','registration_end_date', array(
			'label'=>'Registration End Date',
			'required'=>'true',
	    	'class'=>'to'
	    ));
		
		//1st payment start date
		$payment_start_date = $this->createElement('text','first_payment_start_date', array(
			'label'=>'1st Payment Start Date',
			'required'=>'true',
	    	'class'=>'from2'
	    ));
	    
		
		//1st payment end date
		$payment_end_date = $this->createElement('text','first_payment_end_date', array(
			'label'=>'1st Payment End Date',
			'required'=>'true',
	    	'class'=>'to2'
	    ));
	    

		// adding elements to form Object
		$this->addElements(array($filename,$facultyid,$programcode,$academicyear,$nomor,$decree_date, $acadyear,$reg_start_date,$reg_end_date,$payment_start_date,$payment_end_date));
		
		//add submit button
        $this->addElement('submit', 'Upload');

		
	}
	
}
?>