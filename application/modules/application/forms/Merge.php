<?php
class Application_Form_Merge extends Zend_Form
{
		
	public function init()
	{
	
		$this->setMethod('post');
		$this->setAttrib('id','mergeapplicant');

		$format1 = new Zend_Form_Element_Text('no_formulir1');
        $format1->setLabel('Nomor Formulir 1')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits');
		$this->addElement($format1);	

		$format2 = new Zend_Form_Element_Text('no_formulir2');
        $format2->setLabel('Nomor Formulir 2')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits');
		$this->addElement($format2);

		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('Next'),
          'decorators'=>array('ViewHelper')
        ));
	}
}