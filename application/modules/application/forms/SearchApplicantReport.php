<?php
class Application_Form_SearchApplicantReport extends Zend_Form
{
	protected $_facultyid;
	
	public function setFacultyid($facultyid) {
		$this->_facultyid = $facultyid;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');

		
		//intake	
		$this->addElement('multiselect','IdIntake', array(
			'label'=>'Intake',
			'required'=>true
		));
		
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intakeList = $intakeDb->getData();		
    			
		//$this->IdIntake->addMultiOption(null,"-- Please Select --");
		foreach ($intakeList as $list){
			$this->IdIntake->addMultiOption($list['IdIntake'],$list['IntakeDesc']);
		}
		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Program'),
			'onChange'=>'getProgramScheme(this);'
		));
		
		$programDb = new Registration_Model_DbTable_Program();		
		$this->IdProgram->addMultiOption(null,"-- All --");		
		foreach($programDb->getData() as $program){
			$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
		}		
						
		//program scheme
		$this->addElement('select','IdProgramScheme', array(
					      'label'=>$this->getView()->translate('Programme Scheme'),
						  'registerInArrayValidator'=>false
		));						
		$this->IdProgramScheme->addMultiOption(null,"-- All --");
		
		
		//student category	
		$this->addElement('select','appl_category', array(
			'label'=>'Applicant Category'
		));
		
		$definationDB = new App_Model_General_DbTable_Definationms();
		$this->appl_category->addMultiOption(null,"-- All --");
		foreach ($definationDB->getDataByType(95) as $cat){
			$this->appl_category->addMultiOption($cat['idDefinition'],$cat['DefinitionDesc']);
		}
		
		//application status
		$this->addElement('select','status', array(
			'label'=>'Application Status'
		));
	    $statusList = $definationDB->getDataByType(56);                
	    $this->status->addMultiOption('', '-- All --');
	    foreach ($statusList as $statusLoop){
	      	$this->status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
	    }
	               		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        //button
		$this->addElement('submit', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}	
?>