<?php
class Application_Form_NopesSearch extends Zend_Form
{
	
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
				
  
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		//intake
		$this->addElement('select','intake_id', array(
			'label'=>'Intake',
			'onchange'=>'getPeriod(this)',
		    'required'=>true
		));
		
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intakeList = $intakeDb->getData();		
    			
		$this->intake_id->addMultiOption(null,"-- Please Select --");
		foreach ($intakeList as $list){
			$this->intake_id->addMultiOption($list['IdIntake'],$list['IntakeDefaultLanguage']);
		}
		
	
		$this->addElement('submit', 'save', array(
          'label'=>'View',
          'decorators'=>array('ViewHelper')
        ));
        
        
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));

	}
}
?>