<?php

class Application_Form_SpecializationSearch extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('id','form_ss');


        $this->addElement('text','sft_eng_desc',
            array(
                'label'=>'Specialization Name:',
                'required'=>'true',
                'class'=>'input-txt'
            )
        );

        $this->addElement('text','sft_mal_desc',
            array(
                'label'=>'Specialization (Malay):',
                'class'=>'input-txt'
            )
        );




        //button
        $this->addElement('submit', 'save', array(
            'label'=>'Search',
            'decorators'=>array('ViewHelper')
        ));

        $this->addElement('reset', 'cancel', array(
            'label'=>'Clear',
            'decorators'=>array('ViewHelper'),
            'onClick'=>"window.location=window.location;",
        ));

        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
            'decorators'=>array(
                'FormElements',
                array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
                'DtDdWrapper'
            )
        ));

    }
}
?>