<?php 
class Application_Form_TooltipSetupForm extends Zend_Form{
	
    public function init(){
        
        $this->setMethod('post');
        $program_id = $this->getAttrib('program_id');
        
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        $item = new Zend_Form_Element_Select('item');
	$item->removeDecorator("DtDdWrapper");
	$item->setAttrib('required',"true");
        $item->setAttrib('class', 'span-7');
        $item->setAttrib('onchange', 'getvalue(this.value,'.$program_id.')');
        $item->setAttrib('id', 'item');
	$item->removeDecorator("Label");
        
        $model = new Application_Model_DbTable_ApplicationInitialConfiguration();
        
        $itemList = $model->fnGetItemBySection(1);
        
        $item->addMultiOption('', '-- Select --');
        
        if (count($itemList)>0){
            foreach ($itemList as $itemLoop){
                if ($itemLoop['id']!=1){
                    $item->addMultiOption($itemLoop['id'], $itemLoop['name']);
                }
            }
        }
        
        $value = new Zend_Form_Element_Select('value');
	$value->removeDecorator("DtDdWrapper");
	$value->setAttrib('required',"true");
        $value->setAttrib('class', 'span-7');
        $value->setAttrib('onchange', 'load('.$program_id.')');
        $value->setAttrib('id', 'value');
	$value->removeDecorator("Label");
        
        $value->addMultiOption('', '-- Select --');
        
        $tooltip = new Zend_Form_Element_Textarea('tooltiptextarea');
        $tooltip->removeDecorator("DtDdWrapper");
	//$tooltip->setAttrib('required',"false");
        //$tooltip->setAttrib('class', 'span-7');
        $tooltip->setAttrib('rows', '6');
        $tooltip->setAttrib('cols', '50');
        //$field3->setAttrib('onchange', 'get_programscheme()');
        $tooltip->setAttrib('id', 'tooltiptextarea');
	$tooltip->removeDecorator("Label");
        
        $program = new Zend_Form_Element_Select('program');
	$program->removeDecorator("DtDdWrapper");
	$program->setAttrib('required',"true");
        $program->setAttrib('class', 'span-7');
        //$program->setAttrib('onchange', 'load('.$program_id.')');
        $program->setAttrib('id', 'item');
	$program->removeDecorator("Label");
        
        $progModel = new Application_Model_DbTable_ProgramScheme();
        
        $programList = $progModel->fnGetProgramList();
        
        $program->addMultiOption('', '-- Select --');
        
        if (count($programList)>0){
            foreach ($programList as $programLoop){
                $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramName']);
            }
        }
        
        $program2 = new Zend_Form_Element_Select('program2');
	$program2->removeDecorator("DtDdWrapper");
	$program2->setAttrib('required',"true");
        $program2->setAttrib('class', 'span-7');
        //$program2->setAttrib('onchange', 'load('.$program_id.')');
        $program2->setAttrib('id', 'item');
	$program2->removeDecorator("Label");
        
        $programList2 = $progModel->fnGetProgramList();
        
        $program2->addMultiOption('', '-- Select --');
        
        if (count($programList2)>0){
            foreach ($programList2 as $programLoop2){
                $program2->addMultiOption($programLoop2['IdProgram'], $programLoop2['ProgramName']);
            }
        }
        
        $submit = new Zend_Form_Element_Button('Save');
        $submit->dojotype="dijit.form.Button";
	$submit->label = $gstrtranslate->_("Save");
        $submit->setAttrib('onclick', 'savetooltip()');
	$submit->removeDecorator("DtDdWrapper");
	$submit->removeDecorator("Label");
	$submit->removeDecorator('HtmlTag')
            ->class = "NormalBtn";
        
        $copy = new Zend_Form_Element_Button('Copy');
        $copy->dojotype="dijit.form.Button";
	$copy->label = $gstrtranslate->_("Copy");
        $copy->setAttrib('onclick', 'copytooltip()');
	$copy->removeDecorator("DtDdWrapper");
	$copy->removeDecorator("Label");
	$copy->removeDecorator('HtmlTag')
            ->class = "NormalBtn";
        
        $this->addElements(array(
            $item,
            $tooltip,
            $program,
            $program2,
            $submit,
            $value,
            $copy
        ));
    }
}
?>