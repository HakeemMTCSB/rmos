<?php
class Application_Form_SelectionSearch extends Zend_Form
{
	protected $_facultyid;
	
	public function setFacultyid($facultyid) {
		$this->_facultyid = $facultyid;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
				
  
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		//intake
		$this->addElement('select','intake_id', array(
			'label'=>'Intake',
			'onchange'=>'getPeriod(this)',
		    'required'=>true
		));
		
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intakeList = $intakeDb->getData();		
    			
		$this->intake_id->addMultiOption(null,"-- Select Intake --");
		foreach ($intakeList as $list){
			$this->intake_id->addMultiOption($list['IdIntake'],$list['IntakeDefaultLanguage']);
		}
		
		
		//period
		$this->addElement('select','period_id', array(
			'label'=>'Period',
		    'required'=>true	
		
		));
		
		$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period_data = $periodDB->getData();	
    			
		$this->period_id->addMultiOption(null,"-- Select Period --");
		foreach ($period_data as $list){
			$this->period_id->addMultiOption($list['ap_id'],$list['ap_desc']);
		}
		
		//load previous period
		$this->addElement('checkbox','load_previous_period', array(
			'label'=>'Include Previous Period',
			"checked" => "checked"
		));
		
		
		//faculty
		$this->addElement('select','faculty', array(
			'label'=>'Faculty',
		    'onChange'=>"getProgramme(this,$('#programme'))",
		    'required'=>false
		));
		
		$collegeDB = new App_Model_General_DbTable_Collegemaster();
		$college_data = $collegeDB->getFaculty();		
    			
		$this->faculty->addMultiOption(null,"-- Select Faculty --");
		foreach ($college_data as $list){
			if($locale=="id_ID"){
				$college_name = $list["ArabicName"];
			}elseif($locale=="en_US"){
				$college_name = $list["CollegeName"];
			}
			$this->faculty->addMultiOption($list['IdCollege'],strtoupper($college_name));
		}
		
		
		
		$this->addElement('select','programme', array(
			'label'=>'Programme'		
		
		));
		
		$programDB = new App_Model_Record_DbTable_Program();
    	$program_data = $programDB->searchProgramByFaculty($this->_facultyid);    	

		
		$this->programme->addMultiOption(null,"All");
		foreach ($program_data as $list){
			
			if($locale=="id_ID"){
				$program_name = $list["ArabicName"];
			}elseif($locale=="en_US"){
				$program_name = $list["ProgramName"];
			}
			
			
			$this->programme->addMultiOption($list['ProgramCode'],$program_name);
		}
	
		
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));

	}
}
?>