<?php
class Application_Form_AnswerSchemaUpload extends Zend_Form {

	protected $ptest_code;

	public function setAas_ptest_code($ptest_code){
		$this->ptest_code = $ptest_code;
	}
	
	public function init()
	{
        //parent::__construct($options);

        $this->setName('upload');
        $this->setAttrib('enctype', 'multipart/form-data');
		$this->setAttrib('action', $this->getView()->url(array('module'=>'application', 'controller'=>'placement-test-schema','action'=>'upload-schema'),'default',true) );

		/*** hidden element ***/
		$element = new Zend_Form_Element_Hidden('aas_ptest_code');
		$element->setValue($this->ptest_code);
		$this->addElement($element);

		$questionNumber = new Zend_Form_Element_Text('aas_total_quest');
        $questionNumber->setLabel('Total Question Number')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits');
		$this->addElement($questionNumber);
                  
        $file = new Zend_Form_Element_File('file');
        $file->setLabel('File')
            ->setDestination(APPLICATION_PATH  . '/tmp')
            ->setRequired(true);
		$this->addElement($file);

        //$this->addElements(array($hiddenElement, $questionNumber,$file));


    }

}