<?php

class Application_Form_ApplicationConfigs extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('id', 'add_config_form');

        //scheme
        $this->addElement('select', 'IdScheme',
            array(
                'label'    => 'Scheme',
                'required' => true
            )
        );
        $schemeDb = new GeneralSetup_Model_DbTable_Schemesetup();
        $listData = $schemeDb->fngetSchemes();
        $this->IdScheme->addMultiOptions($listData);

        //program

        $this->addElement('select', 'IdProgram',
            array(
                'label'    => 'Program',
                'required' => false,
                'onChange' => 'getProgramScheme(this)'
            )
        );
        //$programDB = new GeneralSetup_Model_DbTable_Program();
        //$listProgramData = $programDB->fnGetProgramList();
        $progModel = new Application_Model_DbTable_ProgramScheme();
        $listProgramData = $progModel->fnGetProgramListGotScheme();
        $this->IdProgram->addMultiOption('null', 'Select');
        //$this->IdProgram->addMultiOptions($listProgramData);
        foreach ($listProgramData as $progLoop) {
            $this->IdProgram->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName'] . " - " . $progLoop['ProgramCode']);
        }


        //program scheme
        $this->addElement('select', 'IdProgramScheme', array(
            'label'    => 'Program Scheme',
            'required' => false,
            'registerInArrayValidator' => false
        ));


        //category
        $this->addElement('select', 'category',
            array(
                'label'    => 'Student Category',
                'required' => false
            )
        );

        $registryDB = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $listCategory = $registryDB->getListDataByCodeType('student-category');
        $this->category->addMultiOption(null, "Select");

        foreach ($listCategory as $list) {
            $this->category->addMultiOption($list['id'], $list['name']);
        }

        $this->addElement('radio','source', array(
            'label'=>'Source'));

        $this->source->addMultiOptions(array(
            '0' => 'Online Application',
            '1' => 'Admin'
        ))->setSeparator('<br>')->setValue("0");


        $this->addElement('checkbox','active',
            array(
                'label'=>'Active',
                "checked" => "checked"
            )
        );

        //button
        $this->addElement('submit', 'save', array(
            'label'      => 'Save',
            'decorators' => array('ViewHelper')
        ));

        $this->addElement('submit', 'cancel', array(
            'label'      => 'Cancel',
            'decorators' => array('ViewHelper'),
            'onClick'    => "window.location ='" . $this->getView()->url(array('module' => 'application', 'controller' => 'application-initial-configuration', 'action' => 'index'), 'default', true) . "'; return false;"
        ));

        $this->addDisplayGroup(array('save', 'cancel'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                array('HtmlTag', array('tag' => 'div', 'class' => 'buttons')),
                'DtDdWrapper'
            )
        ));





    }
}

?>