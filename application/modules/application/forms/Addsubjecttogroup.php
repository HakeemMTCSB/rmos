<?php

class Application_Form_Addsubjecttogroup extends Zend_Dojo_Form {

  public function init() {
    $gstrtranslate = Zend_Registry::get('Zend_Translate');

    $lobjmodel = new App_Model_Definitiontype();

    $IdProgramEntry  = new Zend_Form_Element_Hidden('IdProgramEntry');
    $IdProgramEntry->removeDecorator("DtDdWrapper");
    $IdProgramEntry->setAttrib('dojoType',"dijit.form.TextBox");
    $IdProgramEntry->removeDecorator("Label");
    $IdProgramEntry->removeDecorator('HtmlTag');


    $IdProgramEntryReq  = new Zend_Form_Element_Hidden('IdProgramEntryReq');
    $IdProgramEntryReq->removeDecorator("DtDdWrapper");
    $IdProgramEntryReq->setAttrib('dojoType',"dijit.form.TextBox");
    $IdProgramEntryReq->removeDecorator("Label");
    $IdProgramEntryReq->removeDecorator('HtmlTag');


    $GroupId  = new Zend_Form_Element_Hidden('GroupId');
    $GroupId->removeDecorator("DtDdWrapper");
    $GroupId->setAttrib('dojoType',"dijit.form.TextBox");
    $GroupId->removeDecorator("Label");
    $GroupId->removeDecorator('HtmlTag');




    $GroupName = new Zend_Form_Element_Text('GroupName');
    $GroupName->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $GroupName->removeDecorator("DtDdWrapper");
    $GroupName->removeDecorator("Label");
    $GroupName->removeDecorator('HtmlTag');

    $subjectDbObj = new Application_Model_DbTable_Subjectsetup();
    $subjectList = $subjectDbObj->fngetsubjectList();
    $subjectList[1000] = array("key"=>"0","value"=>"Any");
    $IdSubject = new Zend_Dojo_Form_Element_FilteringSelect('IdSubject');
    $IdSubject->removeDecorator("DtDdWrapper");
    $IdSubject->setAttrib('required', "false");
    $IdSubject->removeDecorator("Label");
    $IdSubject->removeDecorator('HtmlTag');
    $IdSubject->setRegisterInArrayValidator(false);
    $IdSubject->setAttrib('dojoType', "dijit.form.FilteringSelect");
    $IdSubject->setAttrib('onchange','setsubject()');
    $IdSubject->addMultiOptions($subjectList);

    $clear = new Zend_Form_Element_Button('Clear');
    $clear->setAttrib('class', 'NormalBtn');
    $clear->setAttrib('dojoType', "dijit.form.Button");
    $clear->label = $gstrtranslate->_("Clear");
    $clear->setAttrib('OnClick', 'clearpageAdd()');
    $clear->removeDecorator("Label");
    $clear->removeDecorator("DtDdWrapper");
    $clear->removeDecorator('HtmlTag');

    $Add = new Zend_Form_Element_Button('Add');
    $Add->setAttrib('class', 'NormalBtn');
    $Add->setAttrib('dojoType', "dijit.form.Button");
    $Add->setAttrib('OnClick', 'addProgramentry()');
    $Add->removeDecorator("Label");
    $Add->removeDecorator("DtDdWrapper");
    $Add->removeDecorator('HtmlTag');

    $Save = new Zend_Form_Element_Submit('Save');
    $Save->label = $gstrtranslate->_("Save");
    $Save->dojotype = "dijit.form.Button";
    $Save->removeDecorator("DtDdWrapper");
    $Save->removeDecorator('HtmlTag');
    $Save->class = "NormalBtn";

    $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
    $UpdDate->removeDecorator("DtDdWrapper");
    $UpdDate->removeDecorator("Label");
    $UpdDate->removeDecorator('HtmlTag');

    $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
    $UpdUser->removeDecorator("DtDdWrapper");
    $UpdUser->removeDecorator("Label");
    $UpdUser->removeDecorator('HtmlTag');


    $QualificationName = new Zend_Form_Element_Text('QualificationName');
    $QualificationName->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $QualificationName->removeDecorator("DtDdWrapper");
    $QualificationName->removeDecorator("Label");
    $QualificationName->removeDecorator('HtmlTag');

    $QualificationId  = new Zend_Form_Element_Hidden('QualificationId');
    $QualificationId->removeDecorator("DtDdWrapper");
    $QualificationId->setAttrib('dojoType',"dijit.form.TextBox");
    $QualificationId->removeDecorator("Label");
    $QualificationId->removeDecorator('HtmlTag');


    
    $studyList = $lobjmodel->fnGetDefinationMs('Field of Study');
    //$studyList[1000] = array("key"=>"0","value"=>"Any");
    $subjectDbObj = new Application_Model_DbTable_Subjectsetup();
    $subjectList = $subjectDbObj->fngetsubjectList();
    $fieldofstudy = new Zend_Dojo_Form_Element_FilteringSelect('fieldofstudy');
    $fieldofstudy->removeDecorator("DtDdWrapper");
    $fieldofstudy->setAttrib('required', "false");
    $fieldofstudy->removeDecorator("Label");
    $fieldofstudy->removeDecorator('HtmlTag');
    $fieldofstudy->setRegisterInArrayValidator(false);
    $fieldofstudy->setAttrib('dojoType', "dijit.form.FilteringSelect");
    $fieldofstudy->addMultiOptions($studyList);

    $NoofSubject = new Zend_Form_Element_Text('NoofSubject');
    $NoofSubject->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $NoofSubject->setAttrib('invalidMessage', 'Only numbers are allowed');
    $NoofSubject->setAttrib('constraints', '{min:-10,max:10,places:0}');
    $NoofSubject->removeDecorator("DtDdWrapper");
    $NoofSubject->removeDecorator("Label");
    $NoofSubject->removeDecorator('HtmlTag');

    
    $MinGradePoint = new Zend_Dojo_Form_Element_FilteringSelect('MinGradePoint');
    $MinGradePoint->removeDecorator("DtDdWrapper");
    $MinGradePoint->setAttrib('required', "false");
    $MinGradePoint->removeDecorator("Label");
    $MinGradePoint->removeDecorator('HtmlTag');
    $MinGradePoint->setRegisterInArrayValidator(false);
    $MinGradePoint->setAttrib('dojoType', "dijit.form.FilteringSelect");


    $this->addElements(array($IdProgramEntry,$IdProgramEntryReq,$GroupId,$IdSubject,$clear,$Add,$Save,$UpdDate,$UpdUser,
          $GroupId,$GroupName,$QualificationId,$QualificationName,$fieldofstudy,$NoofSubject,$MinGradePoint
            ));
  }

}

?>
