<?php 
class Application_Form_ApplicantApproval extends Zend_Dojo_Form{
	
    public function init()
    {   
        $this->setMethod('post');
        
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
	
        $submit = new Zend_Form_Element_Submit('Approve');
        $submit->dojotype="dijit.form.Button";
	$submit->label = $gstrtranslate->_("Approve");
	$submit->removeDecorator("DtDdWrapper");
	$submit->removeDecorator("Label");
	$submit->removeDecorator('HtmlTag')
            ->class = "NormalBtn";
        
        $this->addElements(array(
            $submit,
        ));
		
    }
}
?>