<?php

class Application_Form_Programentry extends Zend_Dojo_Form
{ //Formclass for the Programmaster  module
    public function init()
    {
        $gstrtranslate = Zend_Registry::get('Zend_Translate');

        $IdProgramEntry = new Zend_Form_Element_Hidden('IdProgramEntry');
        $IdProgramEntry->removeDecorator("DtDdWrapper");
        $IdProgramEntry->setAttrib('dojoType', "dijit.form.TextBox");
        $IdProgramEntry->removeDecorator("Label");
        $IdProgramEntry->removeDecorator('HtmlTag');

        $IdProgramReq = new Zend_Form_Element_Hidden('IdProgramReq');
        $IdProgramReq->removeDecorator("DtDdWrapper");
        $IdProgramReq->setAttrib('dojoType', "dijit.form.TextBox");
        $IdProgramReq->removeDecorator("Label");
        $IdProgramReq->removeDecorator('HtmlTag');

        $lobjschememodel = new GeneralSetup_Model_DbTable_Schemesetup();
        $schemeList = $lobjschememodel->fngetSchemes();
        $IdScheme = new Zend_Dojo_Form_Element_FilteringSelect('IdScheme');
        $IdScheme->removeDecorator("DtDdWrapper");
        $IdScheme->setAttrib('required', "true");
        $IdScheme->removeDecorator("Label");
        $IdScheme->removeDecorator('HtmlTag');
        $IdScheme->setRegisterInArrayValidator(false);
        $IdScheme->setAttrib('OnChange', 'fnGetProgramList');
        $IdScheme->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $IdScheme->addmultioptions($schemeList);

        $lobjspecializationmodel = new Application_Model_DbTable_Specialization();
        $specializationList = $lobjspecializationmodel->fngetSpecialization();
        $specializationList[1000] = array("key" => "0", "value" => "Any");
        $IdSpecialization = new Zend_Dojo_Form_Element_FilteringSelect('IdSpecialization');
        $IdSpecialization->removeDecorator("DtDdWrapper");
        $IdSpecialization->setAttrib('required', false);
        $IdSpecialization->removeDecorator("Label");
        $IdSpecialization->removeDecorator('HtmlTag');
        $IdSpecialization->setRegisterInArrayValidator(false);
        $IdSpecialization->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $IdSpecialization->addmultioptions($specializationList);

        $IdProgram = new Zend_Dojo_Form_Element_FilteringSelect('IdProgram');
        $IdProgram->removeDecorator("DtDdWrapper");
        $IdProgram->setAttrib('required', "true");
        $IdProgram->removeDecorator("Label");
        $IdProgram->removeDecorator('HtmlTag');
        $IdProgram->setRegisterInArrayValidator(false);
        $IdProgram->setAttrib('OnChange', 'getProgramScheme()');
        $IdProgram->setAttrib('dojoType', "dijit.form.FilteringSelect");

        $IdProgramScheme = new Zend_Dojo_Form_Element_FilteringSelect('IdProgramScheme');
        $IdProgramScheme->removeDecorator("DtDdWrapper");
        $IdProgramScheme->setAttrib('required', "true");
        $IdProgramScheme->removeDecorator("Label");
        $IdProgramScheme->removeDecorator('HtmlTag');
        $IdProgramScheme->setRegisterInArrayValidator(false);
        $IdProgramScheme->setAttrib('dojoType', "dijit.form.FilteringSelect");

        /*$LearningMode = new Zend_Form_Element_MultiCheckbox('LearningMode');
        //$LearningMode->setAttrib('required',"true");
        $LearningMode->removeDecorator("DtDdWrapper");
        $LearningMode->removeDecorator("Label");
        $LearningMode->removeDecorator('HtmlTag');
        $LearningMode->setAttrib('dojoType',"dijit.form.CheckBox");
        $LearningMode->class = "LearningMode";*/

        $SSCRequired = new Zend_Form_Element_Checkbox('SSCRequired');
        $SSCRequired->setAttrib('dojoType', "dijit.form.CheckBox");
        $SSCRequired->removeDecorator("DtDdWrapper");
        $SSCRequired->removeDecorator("Label");
        $SSCRequired->removeDecorator('HtmlTag');

        $honors = new Zend_Form_Element_Checkbox('honors');
        $honors->setAttrib('dojoType', "dijit.form.CheckBox");
        $honors->removeDecorator("DtDdWrapper");
        $honors->removeDecorator("Label");
        $honors->removeDecorator('HtmlTag');

        $requireAu = new Zend_Form_Element_Checkbox('require_au');
        $requireAu->setAttrib('dojoType', "dijit.form.CheckBox");
        $requireAu->removeDecorator("DtDdWrapper");
        $requireAu->removeDecorator("Label");
        $requireAu->removeDecorator('HtmlTag');

        $openEntry = new Zend_Form_Element_Checkbox('open_entry');
        $openEntry->setAttrib('dojoType', "dijit.form.CheckBox");
        $openEntry->removeDecorator("DtDdWrapper");
        $openEntry->removeDecorator("Label");
        $openEntry->removeDecorator('HtmlTag');

        $proposalSubmission = new Zend_Form_Element_Checkbox('pe_proposal');
        $proposalSubmission->setAttrib('dojoType', "dijit.form.CheckBox");
        $proposalSubmission->removeDecorator("DtDdWrapper");
        $proposalSubmission->removeDecorator("Label");
        $proposalSubmission->removeDecorator('HtmlTag');

        $PlacementTestRequired = new Zend_Form_Element_Checkbox('PlacementTestRequired');
        $PlacementTestRequired->setAttrib('dojoType', "dijit.form.CheckBox");
        $PlacementTestRequired->removeDecorator("DtDdWrapper");
        $PlacementTestRequired->removeDecorator("Label");
        $PlacementTestRequired->removeDecorator('HtmlTag');

        $Active = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType', "dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');


        $Autoct = new Zend_Form_Element_Checkbox('Autoct');
        $Autoct->setAttrib('dojoType', "dijit.form.CheckBox");
        $Autoct->setvalue('1');
        $Autoct->removeDecorator("DtDdWrapper");
        $Autoct->removeDecorator("Label");
        $Autoct->removeDecorator('HtmlTag');


        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');

        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');


        $IdProgramEntryReq = new Zend_Form_Element_Text('IdProgramEntryReq');
        $IdProgramEntryReq->removeDecorator("DtDdWrapper");
        $IdProgramEntryReq->setAttrib('dojoType', "dijit.form.TextBox");
        $IdProgramEntryReq->removeDecorator("Label");
        $IdProgramEntryReq->removeDecorator('HtmlTag');

        $Desc = new Zend_Form_Element_Text('Desc');
        $Desc->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $Desc->setAttrib('maxlength', '100');
        $Desc->removeDecorator("DtDdWrapper");
        $Desc->removeDecorator("Label");
        $Desc->removeDecorator('HtmlTag');

        $programUrl = new Zend_Form_Element_Text('pe_url');
        $programUrl->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $programUrl->setAttrib('maxlength', '100');
        $programUrl->removeDecorator("DtDdWrapper");
        $programUrl->removeDecorator("Label");
        $programUrl->removeDecorator('HtmlTag');

        $lobjdefination = new App_Model_Definitiontype();
        $registryDB = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $ItemList = $registryDB->getListDataByCodeType("academic-qualification-item");

        $Item = new Zend_Dojo_Form_Element_FilteringSelect('Item');
        $Item->removeDecorator("DtDdWrapper");
        $Item->removeDecorator("Label");
        $Item->removeDecorator('HtmlTag');
        $Item->setAttrib('required', false);
        $Item->setRegisterInArrayValidator(false);
        $Item->setAttrib('dojoType', "dijit.form.FilteringSelect");

        if (count($ItemList) > 0) {
            foreach ($ItemList as $academicItem) {
                $Item->addMultiOption($academicItem['id'], $academicItem['name']);
            }
        }

        $Unit = new Zend_Dojo_Form_Element_FilteringSelect('Unit');
        $Unit->removeDecorator("DtDdWrapper");
        $Unit->removeDecorator("Label");
        $Unit->removeDecorator('HtmlTag');
        $Unit->setRegisterInArrayValidator(false);
        $Unit->setAttrib('dojoType', "dijit.form.FilteringSelect");

        $conditionList = $registryDB->getListDataByCodeType("condition");
        $Condition = new Zend_Dojo_Form_Element_FilteringSelect('Condition');
        $Condition->removeDecorator("DtDdWrapper");
        $Condition->removeDecorator("Label");
        $Condition->setAttrib('required', false);
        $Condition->removeDecorator('HtmlTag');
        $Condition->setRegisterInArrayValidator(false);
        $Condition->setAttrib('dojoType', "dijit.form.FilteringSelect");

        if (count($conditionList) > 0) {
            foreach ($conditionList as $cond) {
                $Condition->addMultiOption($cond['id'], $cond['name']);
            }
        }

        $Value = new Zend_Form_Element_Text('Value');
        $Value->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $Value->setAttrib('maxlength', '10');
        $Value->removeDecorator("DtDdWrapper");
        $Value->removeDecorator("Label");
        $Value->removeDecorator('HtmlTag');

        $Validity = new Zend_Form_Element_Text('Validity');
        $Validity->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $Validity->setAttrib('maxlength', '20');
        $Validity->removeDecorator("DtDdWrapper");
        $Validity->removeDecorator("Label");
        $Validity->removeDecorator('HtmlTag');

        $Mandatory = new Zend_Form_Element_Checkbox('Mandatory');
        $Mandatory->setAttrib('dojoType', "dijit.form.CheckBox");
        $Mandatory->removeDecorator("DtDdWrapper");
        $Mandatory->removeDecorator("Label");
        $Mandatory->removeDecorator('HtmlTag');

        $lobjmodelqualification = new Application_Model_DbTable_QualificationType();
        $qualificationList = $lobjmodelqualification->getQualificationList();

        $EntryLevel = new Zend_Dojo_Form_Element_FilteringSelect('EntryLevel');
        $EntryLevel->removeDecorator("DtDdWrapper");
        $EntryLevel->setAttrib('required', true);
        $EntryLevel->removeDecorator("Label");
        $EntryLevel->removeDecorator('HtmlTag');
        $EntryLevel->setRegisterInArrayValidator(false);
        $EntryLevel->setAttrib('dojoType', "dijit.form.FilteringSelect");

        foreach ($qualificationList as $qualification) {
            $EntryLevel->addMultiOption($qualification['id'], $qualification['qt_id'] . ' - ' . $qualification['qt_eng_desc']);
        }

        $lobjmodel = new App_Model_Definitiontype();
        $groupList = $lobjmodel->fnGetDefinationMs('Group');

//        $lobjdefination = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
//        $ItemList = $lobjdefination->getListDataByCodeType("academic-qualification-item");

        $Group = new Zend_Dojo_Form_Element_FilteringSelect('Group');
        $Group->removeDecorator("DtDdWrapper");
        $Group->setAttrib('required', "false");
        $Group->removeDecorator("Label");
        $Group->removeDecorator('HtmlTag');
        $Group->setRegisterInArrayValidator(false);
        $Group->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $Group->addMultiOptions($groupList);

        $groupOtherList = $lobjmodel->fnGetDefinationMs('Group');
        $GroupOther = new Zend_Dojo_Form_Element_FilteringSelect('GroupOther');
        $GroupOther->removeDecorator("DtDdWrapper");
        $GroupOther->setAttrib('required', "false");
        $GroupOther->removeDecorator("Label");
        $GroupOther->removeDecorator('HtmlTag');
        $GroupOther->setRegisterInArrayValidator(false);
        $GroupOther->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $GroupOther->addMultiOptions($groupOtherList);

        $ItemListOther = $lobjdefination->fnGetDefinationMs("EntryRequirementOther");
        $ItemOther = new Zend_Dojo_Form_Element_FilteringSelect('ItemOther');
        $ItemOther->removeDecorator("DtDdWrapper");
        $ItemOther->removeDecorator("Label");
        $ItemOther->removeDecorator('HtmlTag');
        $ItemOther->setAttrib('required', "false");
        $ItemOther->setRegisterInArrayValidator(false);
        $ItemOther->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $ItemOther->addMultiOptions($ItemListOther);

        $conditionList = $lobjdefination->fnGetDefinationMs("Condition");
        $ConditionOther = new Zend_Dojo_Form_Element_FilteringSelect('ConditionOther');
        $ConditionOther->removeDecorator("DtDdWrapper");
        $ConditionOther->removeDecorator("Label");
        $ConditionOther->setAttrib('required', "false");
        $ConditionOther->removeDecorator('HtmlTag');
        $ConditionOther->setRegisterInArrayValidator(false);
        $ConditionOther->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $ConditionOther->addMultiOptions($conditionList);

        $ValueOther = new Zend_Form_Element_Text('ValueOther');
        $ValueOther->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $ValueOther->setAttrib('maxlength', '10');
        $ValueOther->removeDecorator("DtDdWrapper");
        $ValueOther->removeDecorator("Label");
        $ValueOther->removeDecorator('HtmlTag');

        $cgpa = new Zend_Form_Element_Text('cgpa');
        $cgpa->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $cgpa->setAttrib('maxlength', '10');
        $cgpa->removeDecorator("DtDdWrapper");
        $cgpa->removeDecorator("Label");
        $cgpa->removeDecorator('HtmlTag');

        $clearOther = new Zend_Form_Element_Button('clearOther');
        $clearOther->setAttrib('class', 'NormalBtn');
        $clearOther->setAttrib('dojoType', "dijit.form.Button");
        $clearOther->label = $gstrtranslate->_("Clear");
        $clearOther->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $AddOther = new Zend_Form_Element_Button('AddOther');
        $AddOther->setAttrib('class', 'NormalBtn');
        $AddOther->label = $gstrtranslate->_("Add");
        $AddOther->setAttrib('dojoType', "dijit.form.Button")
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');


        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype = "dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
            ->class = "NormalBtn";

        $clear = new Zend_Form_Element_Button('Clear');
        $clear->setAttrib('class', 'NormalBtn');
        $clear->setAttrib('dojoType', "dijit.form.Button");
        $clear->label = $gstrtranslate->_("Clear");
        $clear->setAttrib('OnClick', 'clearpageAdd()');
        $clear->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Add = new Zend_Form_Element_Button('Add');
        $Add->setAttrib('class', 'NormalBtn');
        $Add->setAttrib('dojoType', "dijit.form.Button");
        $Add->setAttrib('OnClick', 'addProgramentry()')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $InternationalPlacementTest = new Zend_Form_Element_Checkbox('InternationalPlacementTest');
        $InternationalPlacementTest->setAttrib('dojoType', "dijit.form.CheckBox");
        $InternationalPlacementTest->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->setAttrib('onClick', 'disableCheck()')
            ->removeDecorator('HtmlTag');

        /*$InternationalCertification  = new Zend_Form_Element_Checkbox('InternationalCertification');
        $InternationalCertification->setAttrib('dojoType',"dijit.form.CheckBox");
        $InternationalCertification ->removeDecorator("DtDdWrapper")
                ->removeDecorator("Label")
                ->setAttrib('onClick','disableCheck()')
                ->removeDecorator('HtmlTag');     */


        /*$InternationalAndOr  = new Zend_Form_Element_Checkbox('InternationalAndOr');
        $InternationalAndOr->setAttrib('dojoType',"dijit.form.CheckBox");
        $InternationalAndOr ->removeDecorator("DtDdWrapper")
                ->removeDecorator("Label")
                ->removeDecorator('HtmlTag');*/

        $LocalPlacementTest = new Zend_Form_Element_Checkbox('LocalPlacementTest');
        $LocalPlacementTest->setAttrib('dojoType', "dijit.form.CheckBox");
        $LocalPlacementTest->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->setAttrib('onClick', 'disableCheck()')
            ->removeDecorator('HtmlTag');

        /*$LocalCertification  = new Zend_Form_Element_Checkbox('LocalCertification');
        $LocalCertification->setAttrib('dojoType',"dijit.form.CheckBox");
        $LocalCertification ->removeDecorator("DtDdWrapper")
                ->removeDecorator("Label")
                ->setAttrib('onClick','disableCheck()')
                ->removeDecorator('HtmlTag');

        $LocalAndOr  = new Zend_Form_Element_Checkbox('LocalAndOr');
        $LocalAndOr->setAttrib('dojoType',"dijit.form.CheckBox");
        $LocalAndOr ->removeDecorator("DtDdWrapper")
                ->removeDecorator("Label")
                ->removeDecorator('HtmlTag');*/

        $GroupId = new Zend_Form_Element_Text('GroupId');
        $GroupId->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        //$GroupId->setAttrib('maxlength','2');
        $GroupId->removeDecorator("DtDdWrapper");
        $GroupId->removeDecorator("Label");
        $GroupId->removeDecorator('HtmlTag');

        $subjectDbObj = new Application_Model_DbTable_Subjectsetup();
        $subjectList = $subjectDbObj->fngetsubjectList();

        //        echo "<pre>";
        //        print_r($subjectList);
        $IdSubject = new Zend_Dojo_Form_Element_FilteringSelect('IdSubject');
        $IdSubject->removeDecorator("DtDdWrapper");
        $IdSubject->setAttrib('required', "false");
        $IdSubject->removeDecorator("Label");
        $IdSubject->removeDecorator('HtmlTag');
        $IdSubject->setRegisterInArrayValidator(false);
        $IdSubject->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $IdSubject->addMultiOptions($subjectList);

        $dateformat = "{datePattern:'dd-MM-yyyy'}";

        $StartDate = new Zend_Form_Element_Text('StartDate');
        $StartDate->setAttrib('dojoType', "dijit.form.DateTextBox");
        $StartDate->setAttrib('onChange', "dijit.byId('EndDate').constraints.min = arguments[0];");
        $StartDate->setAttrib('required', "false")
            ->setAttrib('constraints', "$dateformat")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $EndDate = new Zend_Form_Element_Text('EndDate');
        $EndDate->setAttrib('dojoType', "dijit.form.DateTextBox");
        $EndDate->setAttrib('required', "false")
            ->setAttrib('constraints', "$dateformat")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $Description = new Zend_Form_Element_Textarea('Description');
        $Description->setAttrib('cols', '30')
            ->setAttrib('rows', '3')
            ->setAttrib('style', 'width = 10%;')
            ->setAttrib('maxlength', '250')
            ->setAttrib('dojoType', "dijit.form.SimpleTextarea")
            ->setAttrib('style', 'margin-top:10px;border:1px light-solid #666666;color:#666666;font-size:11px')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');


        //form elements
        /*$this->addElements(array($IdProgramEntry,$IdProgram,$LearningMode,$SSCRequired,$PlacementTestRequired,$Active,$UpdDate,$UpdUser,$IdProgramEntryReq,$Desc,$Item,$Unit,$Condition,
        $Value,$Validity,$Mandatory,$EntryLevel,$Save,$clear,$Add,$IdProgramReq,$InternationalPlacementTest,$InternationalCertification,$InternationalAndOr,$LocalPlacementTest,$LocalCertification,$LocalAndOr,
        $GroupId
            ));*/
        $this->addElements(array($IdProgramEntry, $IdProgram, $SSCRequired, $PlacementTestRequired, $Active, $UpdDate, $UpdUser, $IdProgramEntryReq, $Desc, $Item, $Unit, $Condition,
            $Value, $Validity, $Mandatory, $EntryLevel, $Save, $clear, $Add, $IdProgramReq, $InternationalPlacementTest, $LocalPlacementTest,
            $GroupId, $Autoct, $IdSubject, $IdScheme, $Group, $IdSpecialization, $StartDate, $EndDate, $Description, $GroupOther, $ItemOther, $ConditionOther, $ValueOther, $AddOther, $clearOther,
            $openEntry, $proposalSubmission, $programUrl, $IdProgramScheme, $cgpa, $honors, $requireAu
        ));

    }
}
