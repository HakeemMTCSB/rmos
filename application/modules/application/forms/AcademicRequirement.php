<?php

class Application_Form_AcademicRequirement extends Zend_Form
{

    protected $id;
    protected $oe;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setOe($oe)
    {
        $this->oe = $oe;
    }

    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('id', 'formAddAcademic');

        $this->addElement('hidden', 'idprogramentry');
        $this->idprogramentry->setValue($this->id);

        $this->addElement('hidden', 'pid');

        $this->addElement('hidden', 'open_entry');
        $this->open_entry->setValue($this->oe);

        $registryDB = new GeneralSetup_Model_DbTable_Registry_RegistryValue();

        if($this->oe == 1){

            //requirement category
            $this->addElement('select', 'requirementCategory',
                array(
                    'label'    => 'Category',
                    'required' => false,
                    'class'    => 'input-txt'
                )
            );

            $categoryList = $registryDB->getListDataByCodeType("entry-requirement-type");
            $this->requirementCategory->addMultiOption(null, "Please Select");

            foreach ($categoryList as $list) {
                $this->requirementCategory->addMultiOption($list['id'], $list['name']);
            }

            $this->addElement('textarea', 'requirement',
                array(
                    'label'    => 'Qualification',
                    'required' => false,
                    'class'    => 'input - txt'
                )
            );

            $this->addElement('textarea', 'requirementMalay',
                array(
                    'label'    => 'Qualification (Malay)',
                    'required' => false,
                    'class'    => 'input - txt'
                )
            );


        }else {
            $registryDB = new GeneralSetup_Model_DbTable_Registry_RegistryValue();

            //qualification
            $this->addElement('select', 'EntryLevel',
                array(
                    'label'    => 'Qualification',
                    'required' => true,
                    'class'    => 'input-txt'
                )
            );

            $lobjmodelqualification = new Application_Model_DbTable_QualificationType();
            $qualificationList = $lobjmodelqualification->getQualificationList();

            $this->EntryLevel->addMultiOption(null, "Please Select");
            foreach ($qualificationList as $list) {
                $this->EntryLevel->addMultiOption($list['id'], $list['qt_id'] . ' - ' . $list['qt_eng_desc']);
            }

            //specilization
            $this->addElement('select', 'IdSpecialization',
                array(
                    'label'    => 'Specialization',
                    'required' => false,
                    'class'    => 'input-txt'
                )
            );

            $lobjspecializationmodel = new Application_Model_DbTable_Specialization();
            $studyFieldList = $lobjspecializationmodel->getStudyFieldOptgroup();

            $this->IdSpecialization->setMultiOptions($studyFieldList);

            //result item
            $this->addElement('select', 'Item',
                array(
                    'label'    => 'Result Item',
                    'required' => false,
                    'class'    => 'input - txt'
                )
            );


            $ItemList = $registryDB->getListDataByCodeType("academic-qualification-item");

            $this->Item->addMultiOption(null, "Please Select");
            foreach ($ItemList as $list) {
                $this->Item->addMultiOption($list['id'], $list['name']);
            }

            //condition
            $this->addElement('select', 'Condition',
                array(
                    'label'    => 'Condition',
                    'required' => false,
                    'class'    => 'input - txt'
                )
            );

            $conditionList = $registryDB->getListDataByCodeType("condition");
            $this->Condition->addMultiOption(null, "Please Select");

            foreach ($conditionList as $list) {
                $this->Condition->addMultiOption($list['id'], $list['name']);
            }

            //result value
            $this->addElement('text', 'Value',
                array(
                    'label'    => 'Result Value',
                    'required' => false,
                    'class'    => 'input - txt'
                )
            );


            //validity year
            $this->addElement('text', 'Validity',
                array(
                    'label'    => 'Validity',
                    'required' => false,
                    'class'    => 'input - txt'
                )
            );
        }

        //honors
        $this->addElement('checkbox', 'honors',
            array(
                'label'   => 'Honors',
                'checked' => false
            )
        );

        //require AU
        $this->addElement('checkbox', 'require_au',
            array(
                'label'   => 'Require AU ? ',
                'checked' => false
            )
        );

        //compulsory
        $this->addElement('checkbox', 'Mandatory',
            array(
                'label'   => 'Compulsory ? ',
                'checked' => true
            )
        );

    }
}

?>