<?php
class Application_Form_SearchPool extends Zend_Form
{
    protected $_programme;
	protected $_preference;
	
	public function setProgramme($value) {
		$this->_programme = $value;
	}
	
	public function setPreference($value) {
		$this->_preference = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
				
  
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		
		//faculty
		$this->addElement('select','faculty', array(
			'label'=>'Faculty',
		    'onChange'=>"getProgramme(this,$('#programme'))",
		    'required'=>false
		));
		
		$collegeDB = new App_Model_General_DbTable_Collegemaster();
		$college_data = $collegeDB->getFaculty();		
    			
		$this->faculty->addMultiOption(null,"-- All --");
		foreach ($college_data as $list){
			if($locale=="id_ID"){
				$college_name = $list["ArabicName"];
			}elseif($locale=="en_US"){
				$college_name = $list["CollegeName"];
			}
			$this->faculty->addMultiOption($list['IdCollege'],strtoupper($college_name));
		}
		
		
		
		$this->addElement('select','programme', array(
			'label'=>'Programme',
		    'required'=>false		
		
		));
		
		$programDB = new App_Model_Record_DbTable_Program();
    	$program_data = $programDB->searchProgramByFaculty($this->_facultyid);    	

		
		$this->programme->addMultiOption(null,"-- All --");
		foreach ($program_data as $list){
			
			if($locale=="id_ID"){
				$program_name = $list["ArabicName"];
			}elseif($locale=="en_US"){
				$program_name = $list["ProgramName"];
			}
			
			
			$this->programme->addMultiOption($list['ProgramCode'],$program_name);
		}
		
		
		
		$this->addElement('select','preference', array(
				'label'=>'Program Preference',
				'required'=>false	        
		       ));
		
		$this->preference->addMultiOption(null,$this->getView()->translate('All'));
		$this->preference->addMultiOption(1,$this->getView()->translate('Preference 1'));
		$this->preference->addMultiOption(2,$this->getView()->translate('Preference 2'));
	
		/*$this->preference->setMultiOptions(array('1'=>' '.$this->getView()->translate('Preference 1'), '2'=>' '.$this->getView()->translate('Preference 2')));
		$this->preference->setValue("1");*/
				
		
		$this->addElement('radio','quota', array(
				'label'=>'Quota',
				'required'=>true	,
		        'onclick'=>"enabledBox(this);"        
		       ));
		
		$this->quota->setMultiOptions(array('1'=>' '.$this->getView()->translate('All'), '2'=>' '.$this->getView()->translate('Others')));
		$this->quota->setValue($this->_quota);
		
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('text','limit', array(
				'label'=>'',				
        		'disabled'=>'disabled',
				'size'=>'2'        
	    ));
		$this->limit->setValue($this->_limitt);
		$this->limit->addValidator('Digits');
		
		
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
                
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));

	}
	
}

?>