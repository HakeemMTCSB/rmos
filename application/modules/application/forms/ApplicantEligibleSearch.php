<?php 
class Application_Form_ApplicantEligibleSearch extends Zend_Dojo_Form{
	
    public function init()
    {   
        $this->setMethod('post');
        
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        $program = new Zend_Form_Element_Select('IdProgram');
	$program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'span-7');
	$program->removeDecorator("Label");
        
        $progModel = new Application_Model_DbTable_ProgramScheme();
        $progList = $progModel->fnGetProgramList();
        
        $program->addMultiOption('', '--Select--');
        
        if (count($progList)>0){
            foreach ($progList as $progLoop){
                $program->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
            }
        }
        
        $status = new Zend_Form_Element_Select('status');
	$status->removeDecorator("DtDdWrapper");
        $status->setAttrib('class', 'span-7');
	$status->removeDecorator("Label");
        
        $status->addMultiOption('', '--Select--');
        $status->addMultiOption('1', 'Eligible');
        $status->addMultiOption('0', 'Not Eligible');
        
        $submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
	$submit->label = $gstrtranslate->_("Search");
	$submit->removeDecorator("DtDdWrapper");
	$submit->removeDecorator("Label");
	$submit->removeDecorator('HtmlTag')
            ->class = "NormalBtn";
        
        $Clear = new Zend_Form_Element_Submit('Clear');
	$Clear->dojotype="dijit.form.Button";
	$Clear->label = $gstrtranslate->_("Clear");
	$Clear->setAttrib('class', 'NormalBtn')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        
        $this->addElements(array(
            $program,
            $status,
            $submit,
            $Clear
        ));
    }
}
?>