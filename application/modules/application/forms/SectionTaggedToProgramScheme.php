<?php

class Application_Form_SectionTaggedToProgramScheme extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('IdProgram', 'section_tagged_to_ptogram_scheme');

        $addoredit = $this->getAttrib('addoredit');

        if ($addoredit == 'add') {

            $this->addElement('select', 'sectionID', array(
                'label'    => 'Section',
                'required' => true,
                //'onChange'=>'getTitle()'
            ));

            $this->sectionID->addMultiOption(null, "-- Select --");

            $get_section_list = new Application_Model_DbTable_ApplicationInitialConfiguration();
            $section_list = $get_section_list->fnGetSectionListTagging();

            if (count($section_list) > 0) {
                foreach ($section_list as $list) {
                    $this->sectionID->addMultiOption($list['id'], $list['name'] . " - " . $list['desc']);
                }
            }
        } else {
            $this->addElement('text', 'name', array(
                //'id'=>'instruction',
                'disable' => true,
                'label'   => 'Section',
                'class'   => 'input-txt'
            ));
        }

        $this->addElement('select', 'section_maxEntry', array(
            'label'                    => 'Maximum Entry',
            'id'                       => 'section_maxEntry',
            'registerInArrayValidator' => false,
            'class'                    => 'input-txt'
        ));

        $entry = array();
        $entry[0]['no'] = 1;
        $entry[0]['select'] = '1';
        $entry[1]['no'] = 2;
        $entry[1]['select'] = '2';
        $entry[2]['no'] = 3;
        $entry[2]['select'] = '3';
        $entry[3]['no'] = 4;
        $entry[3]['select'] = '4';
        $entry[4]['no'] = 5;
        $entry[4]['select'] = '5';

        if (count($entry) > 0) {
            foreach ($entry as $list) {
                $this->section_maxEntry->addMultiOption($list['no'], $list['select']);
            }
        }

        $this->addElement('textarea', 'sectionTagInstruction', array(
            'id'    => 'instruction',
            'label' => 'Instruction',
            'class' => 'input-txt'
        ));

        $this->addElement('textarea', 'initialconfigdesc', array(
            'label' => 'Description',
            'class' => 'input-txt'
        ));

        $this->addElement('checkbox', 'section_mandatory', array(
            'label'          => 'Mandatory',
            'name'           => 'section_mandatory',
            'checkedValue'   => '1',
            'uncheckedValue' => '0',
            'disableHidden'  => true,
        ));

        //button
        $this->addElement('submit', 'save_section_tagging', array(
            'label'      => 'Save',
            'decorators' => array('ViewHelper')
        ));

        /*$this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'application', 'controller'=>'application-initial-configuration','action'=>'section-list'),'default',true) . "'; return false;"
        ));*/

        $this->addDisplayGroup(array('save_section_tagging'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                array('HtmlTag', array('tag' => 'div', 'class' => 'buttons')),
                'DtDdWrapper'
            )
        ));

    }
}

?>