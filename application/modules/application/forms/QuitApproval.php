
<?php
class Application_Form_QuitApproval extends Zend_Form
{
	protected $_transid;
	protected $_payment;
	
	public function setTransid($transid) {
		$this->_transid = $transid;
	}
	
	public function setPayment($payment) {
		$this->_payment = $payment;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','quit_form');				
  
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		$this->addElement('hidden','aq_trans_id',array('value'=>$this->_transid));
		$this->addElement('hidden','aq_payment_id',array('value'=>$this->_payment));
		
		$this->addElement('select','aq_status', array(
			'label'=>'Application Status',
		    'onchange'=>"showDiv();",
		    'required'=>true	
		));
						
		$this->aq_status->addMultiOption(null,"Please Select");
		$this->aq_status->addMultiOption(2,"Approve");
		$this->aq_status->addMultiOption(3,"Reject");
		$this->aq_status->addMultiOption(4,"Incomplete Documents");
		
	
			
		
		/* START DIV */
		$this->addElement(
			'hidden',
			'div_quit_detail',
			array(
				'required' => false,
			    'ignore' => true,
			    'autoInsertNotEmptyValidator' => false,				       
			    'decorators' => array(
			    	array(
			        	'HtmlTag', array(
				            'tag'  => 'div',
				            'id'   => 'quit_detail',
				            'openOnly' => true,
				            'style'=>'display:none',
			            )
			       	)
			    )
			)
		);
		$this->div_quit_detail->clearValidators();
		/* END END DIV */
		
				
		
		$this->addElement('text','aq_approveddt', array(
			'label'=>'Date Approved'
		));
		
		$this->addElement('text','aq_issueddt', array(
			'label'=>'Date Cheque will be issued'
		));
		
		$this->addElement('select','aq_bank', array(
			'label'=>'Bank'	
		));
		
		$definationDb = new App_Model_General_DbTable_Definationms();
		
		$this->aq_bank->addMultiOption(null,'Please Select');
		foreach ($definationDb->getDataByType(88) as $list){
			$this->aq_bank->addMultiOption($list['idDefinition'],$list['BahasaIndonesia']);
		}
	
		
		$this->addElement('text','aq_cheque_no', array(
			'label'=>'Cheque Number'
		));
		$this->aq_cheque_no->addValidator(new Zend_Validate_Alnum());
		
		$this->addElement('text','aq_cheque_amount', array(
			'label'=>'Cheque Amount'
		));
		
		/* START END DIV */
		$this->addElement(
			    'hidden',
			    'dummyy',
			    array(
			        'required' => false,
			        'ignore' => true,
			        'autoInsertNotEmptyValidator' => false,
			        'decorators' => array(
			            array(
			                'HtmlTag', array(
			                    'tag'  => 'div',
			                    'id'   => 'placement',
			                    'closeOnly' => true
			                )
			            )
			        )
			    )
		);
		$this->dummyy->clearValidators();
		/* END END DIV */
		
		
		$this->addElement('textarea','aq_remarks', array(
			'label'=>'Remarks'
		));
		
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',		
          'decorators'=>array('ViewHelper')
        ));
        
        
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));

	}
}
?>