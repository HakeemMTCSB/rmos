<?php
class Application_Form_Applicant extends Zend_Form
{
	protected $code;
	public function setCode($code){
		$this->code = $code;
	}	
	public function init()
	{
	

		$this->setMethod('post');
		$this->setAttrib('id','omrapplicant');

		$format = new Zend_Form_Element_Text('no_pes');
        $format->setLabel($this->getView()->translate('Nomor Peserta'))
                  ->setRequired(true)
                  ->addValidator('NotEmpty');
		$this->addElement($format);				
		$format = new Zend_Form_Element_Text('no_formulir');
        $format->setLabel('Nomor Formulir')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits');
		$this->addElement($format);	

		$format = new Zend_Form_Element_Text('appl_name');
        $format->setLabel($this->getView()->translate('Nama'))
                  ->setRequired(true)
                  ->addValidator('NotEmpty');
		$this->addElement($format);			
		
		$format = new Zend_Form_Element_Text('appl_email');
        $format->setLabel($this->getView()->translate('Email'))
                  ->setRequired(true)
                  ->addValidator('NotEmpty');
		$this->addElement($format);	

		$format = new Zend_Form_Element_Text('exam_code');
        $format->setLabel($this->getView()->translate('Kode Set'))
                  ->setRequired(true)
                  ->addValidator('NotEmpty');
		$this->addElement($format);	

		$this->addElement('select','app_id1', array(
			'label'=>"Programme Preference 1",
			'required'=>true
		));
		
		
        
        $placementTestProgramList = $this->getPlacementTestProgram();
        
        $registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		if ($locale=="en_US"){
			$this->app_id1->addMultiOption(null,$this->getView()->translate('please_select'));
			foreach ($placementTestProgramList as $list){
				$this->app_id1->addMultiOption($list['ProgramCode'],$list['ProgramName']." (".$list['ShortName'].")");
			}
		}else if ($locale=="id_ID"){
			$this->app_id1->addMultiOption(null,$this->getView()->translate('please_select'));
			foreach ($placementTestProgramList as $list){
				$this->app_id1->addMultiOption($list['ProgramCode'],$list['ArabicName']." (".$list['ShortName'].")");
			}
		}
		
		$this->addElement('select','app_id2', array(
			'label'=>"Programme Preference 2"
		));
		
		if ($locale=="en_US"){
			$this->app_id2->addMultiOption(null,$this->getView()->translate('please_select'));
			foreach ($placementTestProgramList as $list){
				$this->app_id2->addMultiOption($list['ProgramCode'],$list['ProgramName']." (".$list['ShortName'].")");
			}
		}else if ($locale=="id_ID"){
			$this->app_id2->addMultiOption(null,$this->getView()->translate('please_select'));
			foreach ($placementTestProgramList as $list){
				$this->app_id2->addMultiOption($list['ProgramCode'],$list['ArabicName']." (".$list['ShortName'].")");
			}
		}	
		
		$this->addElement('hidden', 'profileid', array( 	
		    'ignore' => true
		));		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('Save'),
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'application', 'controller'=>'placement-test','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));

	}
	
	private function getPlacementTestProgram(){
    	
		$db = Zend_Db_Table::getDefaultAdapter();
		
	        //get placementest program data
	  	$select = $db->select()
	                 ->from(array('app'=>'appl_placement_program'))
	                 ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode = app.app_program_code' )
	                 ->where('app.app_placement_code  = ?', $this->code)
	                 ->order('p.ArabicName ASC');
		//echo  $this->ptestCode."xx";
        $stmt = $db->query($select);
        $row = $stmt->fetchAll();
        
        if($row){
        	return $row;
        }else{
        	return null;
        }
       
	}	
}
?>