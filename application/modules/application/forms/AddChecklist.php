<?php
class Application_Form_AddChecklist extends Zend_Form{


    public function init()
    {
        $this->setMethod('post');
        //$this->setAttrib('IdProgram', 'Add_Checklist');
        $idprogram = $this->getAttrib('idprogram');
        $idstudtype = $this->getAttrib('idstudtype');
        $IdProgramScheme = $this->getAttrib('IdProgramScheme');

        //$data = $this->getAttrib('contactName');

       /* $idprogram = $this->idprogram;
        $idstudtype = $this->idstudtype;
        $idprogscheme = $this->idprogscheme;*/

       /* $ProgramID  = new Zend_Form_Element_Hidden('ProgramID');
        $ProgramID  				->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');*/

        /*$UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser  				->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');*/



        /*8$this->addElement('select','IdProgram', array(
                 'label'=>'Program',
                 'required'=>'true',
                 'id'=>'program'
                 //'onChange'=>'get_programscheme()'
         ));

         $this->IdProgram->addMultiOption(null,"-- Select --");

         $get_program_list = new Application_Model_DbTable_ProgramScheme();
         $program_list = $get_program_list->fnGetProgramList();

         if(count($program_list)>0){
                 foreach ($program_list as $list){
                     $this->IdProgram->addMultiOption($list['IdProgram'],$list['ProgramName']);
                 }
         }


              $this->addElement('select','IdProgramScheme', array(
                 'label'=>'Program Scheme',
                 'required'=>'true',
                 'id'=>'programscheme',
                 'registerInArrayValidator' => false,
                 //'onChange'=>'get_program()'
         ));

         $this->IdProgramScheme->addMultiOption(null,"-- Select --");

             if ($IdProgram){
                 $defModel = new App_Model_General_DbTable_Definationms();
                 $progSchemeList = $get_program_list->fnGetProgramschemeListBasedOnProgId($IdProgram);
                 foreach($progSchemeList as $progSchemeListloop){
                     $mod_of_study = $defModel->getData($progSchemeListloop['mode_of_study']);
                     $mod_of_program = $defModel->getData($progSchemeListloop['mode_of_program']);
                     $programType = $defModel->getData($progSchemeListloop['program_type']);

                     $this->IdProgramScheme->addMultiOption($progSchemeListloop['IdProgramScheme'], $mod_of_program['DefinitionDesc'].' '.$mod_of_study['DefinitionDesc'].' '. $programType['DefinitionDesc']);
                 }
             }

             $this->addElement('select','dcl_stdCtgy', array(
                 'label'=>'Student Category',
                 'required'=>'true',
                 'registerInArrayValidator' => false,
                 'id'=>'studentCategory',
                 'onChange'=>'get_section()'
         ));

         $this->dcl_stdCtgy->addMultiOption(null,"-- Select --");

         $definationDB = new App_Model_General_DbTable_Definationms();
             $category = $definationDB->getDataByType(95);

         if(count($category)>0){
                 foreach ($category as $list){
             $this->dcl_stdCtgy->addMultiOption($list['idDefinition'],$list['DefinitionDesc']);
                 }
         }8*/

        $this->addElement('select','dcl_sectionid', array(
            'label'=>'Section',
            //'required'=>'false',
            'registerInArrayValidator' => false,
            'id'=>'section'
        ));

        $this->dcl_sectionid->addMultiOption(null,"-- Select -- ");

       // if ($IdProgramScheme && $stdCtgy){



            $appInitConfModel = new Application_Model_DbTable_ApplicationInitialConfig();
            $sectionList = $appInitConfModel->getSectionChecklist($idprogram, $idstudtype);
            //var_dump($sectionList);

            if(count($sectionList)>0){
                foreach ($sectionList as $list){
                    $this->dcl_sectionid->addMultiOption($list['sectionID'],$list['name']);
                }
            }
       // }

        $this->addElement('text','dcl_name', array(
            'label'=>'Checklist Name',
            'required'=>'true'
        ));

        $this->addElement('text','dcl_malayName', array(
            'label'=>'Checklist Malay Name',
            //'required'=>'true'
        ));

        $this->addElement('text','dcl_priority', array(
            'label'=>'Sequence',
            //'required'=>'true'
        ));

        /*$this->addElement('select','dcl_type', array(
            'label'=>'Checklist Type',
            'required'=>'true',
            'id'=>'checklistType',
            //'onChange'=>'get_programscheme()'
	));*/

        $this->addElement('checkbox', 'dcl_activeStatus', array(
            'label' => 'Active',
            'name' => 'active',
            'checkedValue' => '1',
            'uncheckedValue' => '0',
            'disableHidden' => true
        ));

        $this->addElement('checkbox', 'dcl_mandatory', array(
            'label' => 'Mandatory',
            'name' => 'mandatory',
            'checkedValue' => '1',
            'uncheckedValue' => '0',
            'disableHidden' => true
        ));

        $this->addElement('checkbox', 'dcl_uplStatus', array(
            'label' => 'Upload',
            'name' => 'upload',
            'checkedValue' => '1',
            'uncheckedValue' => '0',
            'disableHidden' => true
        ));

        $this->addElement('checkbox', 'dcl_finance', array(
            'label' => 'Finance',
            'name' => 'dcl_finance',
            'checkedValue' => '1',
            'uncheckedValue' => '0',
            'disableHidden' => true
        ));

        $this->addElement('select','dcl_uplType', array(
            'label'=>'Document Type',
            'required'=>'true',
            'id'=>'docType',
            //'onChange'=>'get_programscheme()'
        ));

        $this->dcl_uplType->addMultiOption(null,"-- Select --");

        //$uplType = $definationDB->getDataByType(10);
        $defModel = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $uplType = $defModel->getList(43);

        if(count($uplType)>0){
            foreach ($uplType as $list){
                $this->dcl_uplType->addMultiOption($list['id'],$list['name']);
            }
        }

        $this->addElement('textarea','dcl_desc', array(
            'label'=>'Description'
        ));

        //button
        $this->addElement('submit', 'save_section_tagging', array(
            'label'=>'Save',
            'decorators'=>array('ViewHelper')
        ));

        $this->addElement('submit', 'cancel', array(
            'label'=>'Cancel',
            'decorators'=>array('ViewHelper'),
            'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'application', 'controller'=>'admission-checklist','action'=>'index'),'default',true) . "'; return false;"
        ));

        $this->addDisplayGroup(array('save_section_tagging','cancel'),'buttons', array(
            'decorators'=>array(
                'FormElements',
                array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
                'DtDdWrapper'
            )
        ));




    }
}
?>