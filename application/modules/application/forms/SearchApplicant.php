<?php
class Application_Form_SearchApplicant extends Zend_Form
{
	protected $_facultyid;
	
	public function setFacultyid($facultyid) {
		$this->_facultyid = $facultyid;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');

		
		//intake
		$this->addElement('select','program_id', array(
			'label'=>'Program',
                        'id'=>'program',
			'onchange'=>'get_programscheme()'
		));
		
		$programDb = new GeneralSetup_Model_DbTable_Program();
		$programList = $programDb->fnGetProgramList();		
    			
		$this->program_id->addMultiOption(null,"-- All --");
		foreach ($programList as $list){
			$this->program_id->addMultiOption($list['key'],$list['value']);
		}
			
		
		//period
		$this->addElement('select','program_scheme_id', array(
			'label'=>'Program Scheme',
                        'id'=>'programscheme',
                        'required'=>false	
		
		));						
		$this->program_scheme_id->addMultiOption(null,"-- All --");
		
		
		//name
		$this->addElement('text','name', array(
			'label'=>'applicant_name',
			'class'=>'input-txt'
		));
		
		//pes no
		$this->addElement('text','pes_no', array(
			'label'=>'applicantID',
			'class'=>'input-txt'
		));
		
	
		//personal ID
		$this->addElement('text','personal_id', array(
			'label'=>'Personal ID',
			'class'=>'input-txt'
		));
		
		//application status
		$this->addElement('select','application_status', array(
			'label'=>'Application Status'
		));
                
		$definationDB = new App_Model_General_DbTable_Definationms();
		$statusList = $definationDB->getDataByType(56);
		
		$this->application_status->addMultiOption('', '--All--');

		if (count($statusList)>0){
			foreach ($statusList as $statusLoop){
				$this->application_status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
			}
		}
		
		//Intake
		$this->addElement('select','intake_id', array(
			'label'=>'Intake'
		));
            
		$intakeDb = new GeneralSetup_Model_DbTable_Intake();
		$listData = $intakeDb->fngetIntakeList();
		$this->intake_id->addMultiOption('', '--All--');
		foreach ($listData as $list){
			$this->intake_id->addMultiOption($list['IdIntake'],$list['IntakeDesc']);
		}
                
		/*$app_status = array(
						'ALL'		=> '-- All --',
						'APPLY'		=> 'incomplete_app',
						'CLOSE'		=> 'complete_app',
						'PROCESS'	=> 'process_app',
						'OFFER'		=> 'offer',
						'REJECT'	=> 'reject',
						'REGISTERED'=> 'Offer Accepted',
					  );
		
		$this->application_status->addMultiOptions($app_status);*/
		
		
		
		
		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}	
?>