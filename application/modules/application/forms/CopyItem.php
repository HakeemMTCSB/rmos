<?php 
class Application_Form_CopyItem extends Zend_Dojo_Form{
	
    public function init()
    {   
        $this->setMethod('post');
	$this->setAttrib('id', 'checklist_search_form');
        $IdProgram = $this->getAttrib('IdProgram');
        $sectionID = $this->getAttrib('sectionID');
        
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
       
        $field2 = new Zend_Form_Element_Select('field2');
        $field2->setRegisterInArrayValidator(false);
	$field2->removeDecorator("DtDdWrapper");
	$field2->setAttrib('required',"true");
        $field2->setAttrib('id', 'programscheme');
        $field2->setAttrib('class', 'span-7');
	$field2->removeDecorator("Label");
        
        $field2->addMultiOption('', '-- Select --');
        
        $progModel = new Application_Model_DbTable_ProgramScheme();
        
        if ($IdProgram){
            $defModel = new App_Model_General_DbTable_Definationms();
            $progSchemeList = $progModel->fnGetProgramschemeListBasedOnProgId($IdProgram);
            foreach($progSchemeList as $progSchemeListloop){
                $mod_of_study = $defModel->getData($progSchemeListloop['mode_of_study']);
                $mod_of_program = $defModel->getData($progSchemeListloop['mode_of_program']);
                $programType = $defModel->getData($progSchemeListloop['program_type']);

                $field2->addMultiOption($progSchemeListloop['IdProgramScheme'], $mod_of_study['DefinitionDesc'].' '.$mod_of_program['DefinitionDesc'].' '. $programType['DefinitionDesc']);
            }
        }
        
        $field3 = new Zend_Form_Element_Select('field3');
        $field3->setRegisterInArrayValidator(false);
	$field3->removeDecorator("DtDdWrapper");
	$field3->setAttrib('required',"true");
        $field3->setAttrib('class', 'span-7');
        $field3->setAttrib('onchange', 'get_programscheme()');
        $field3->setAttrib('id', 'program');
	$field3->removeDecorator("Label");
        
        $progList = $progModel->fnGetProgramList();
        
        $field3->addMultiOption('', '-- Select --');
        
        if (count($progList)>0){
            foreach ($progList as $progLoop){
                $field3->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
            }
        }
        
        $field4 = new Zend_Form_Element_Select('field4');
        $field4->setRegisterInArrayValidator(false);
	$field4->removeDecorator("DtDdWrapper");
        $field4->setAttrib('id', 'stdCtgy');
        $field4->setAttrib('onchange', 'get_section('.$sectionID.')');
	$field4->setAttrib('required',"true");
        $field4->setAttrib('class', 'span-7');
	$field4->removeDecorator("Label");
	$field4->removeDecorator('HtmlTag');
        
        $defModel = new App_Model_General_DbTable_Definationms();
        $stdCtgyList = $defModel->getDataByType(95);
        
        $field4->addMultiOption('', '-- Select --');
        
        if (count($stdCtgyList)>0){
            foreach ($stdCtgyList as $stdCtgyLoop){
                $field4->addMultiOption($stdCtgyLoop['idDefinition'], $stdCtgyLoop['DefinitionDesc']);
            }
        }
        
        $field5 = new Zend_Form_Element_Select('field5');
        $field5->setRegisterInArrayValidator(false);
	$field5->removeDecorator("DtDdWrapper");
        $field5->setAttrib('id', 'section');
	$field5->setAttrib('required',"true");
        $field5->setAttrib('class', 'span-7');
	$field5->removeDecorator("Label");
	$field5->removeDecorator('HtmlTag');
        
        $field5->addMultiOption('', '-- Select --');
        
        $submit = new Zend_Form_Element_Button('Copy');
        $submit->dojotype="dijit.form.Button";
	$submit->label = $gstrtranslate->_("Copy");
	$submit->removeDecorator("DtDdWrapper");
        $submit->setAttrib('onclick', 'return sureCopy()');
	$submit->removeDecorator("Label");
        
	$submit->removeDecorator('HtmlTag')
            ->class = "NormalBtn";
        
        $this->addElements(array(
            $field2,
            $field3,
            $field4,
            $field5,
            $submit
        ));
		
    }
}
?>