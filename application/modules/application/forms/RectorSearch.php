<?php
class Application_Form_RectorSearch extends Zend_Form
{
    protected $_programme;
	protected $_preference;
	
	public function setProgramme($value) {
		$this->_programme = $value;
	}
	
	public function setPreference($value) {
		$this->_preference = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
				
  
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		
		//intake
		$this->addElement('select','intake', array(
			'label'=>'Intake',
		//	'onchange'=>'getPeriod(this)',
		    'required'=>true
		));
		
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intakeList = $intakeDb->getData();		
    			
		$this->intake->addMultiOption(null,"-- Select Intake --");
		foreach ($intakeList as $list){
			$this->intake->addMultiOption($list['IdIntake'],$list['IntakeDefaultLanguage']);
		}
		
		//period
		/*$this->addElement('select','period_id', array(
			'label'=>'Period',
		    'required'=>false	
		
		));
		
		$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period_data = $periodDB->getData();	
    			
		$this->period_id->addMultiOption(null,"-- Select Period --");
		foreach ($period_data as $list){
			$this->period_id->addMultiOption($list['ap_id'],$list['ap_desc']);
		}
		
		//load previous period
		$this->addElement('checkbox','load_previous_period', array(
			'label'=>'Include Previous Period',
			"checked" => "checked"
		));*/
		
		//faculty
		$this->addElement('select','faculty', array(
			'label'=>'Faculty',
		    'onChange'=>"getProgramme(this,$('#programme'))",
		    'required'=>false
		));
		
		$collegeDB = new App_Model_General_DbTable_Collegemaster();
		$college_data = $collegeDB->getFaculty();		
    			
		$this->faculty->addMultiOption(null,"-- All --");
		foreach ($college_data as $list){
			if($locale=="id_ID"){
				$college_name = $list["ArabicName"];
			}elseif($locale=="en_US"){
				$college_name = $list["CollegeName"];
			}
			$this->faculty->addMultiOption($list['IdCollege'],strtoupper($college_name));
		}
		
		
		
		$this->addElement('select','programme', array(
			'label'=>'Programme',
		    'required'=>false		
		
		));
		
		$programDB = new App_Model_Record_DbTable_Program();
    	$program_data = $programDB->searchProgramByFaculty($this->_facultyid);    	

		
		$this->programme->addMultiOption(null,"-- All --");
		foreach ($program_data as $list){
			
			if($locale=="id_ID"){
				$program_name = $list["ArabicName"];
			}elseif($locale=="en_US"){
				$program_name = $list["ProgramName"];
			}
			
			
			$this->programme->addMultiOption($list['ProgramCode'],$program_name);
		}
		
		
		
		$this->addElement('select','preference', array(
				'label'=>'Program Preference',
				'required'=>false	        
		       ));
		
		$this->preference->addMultiOption(null,$this->getView()->translate('All'));
		$this->preference->addMultiOption(1,$this->getView()->translate('Preference 1'));
		$this->preference->addMultiOption(2,$this->getView()->translate('Preference 2'));
		
				
		
		
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
     
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));

	}
	
}

?>