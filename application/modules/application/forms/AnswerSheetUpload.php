<?php
class Application_Form_AnswerSheetUpload extends Zend_Form {
	
	public function init()
	{
        //parent::__construct($options);

        $this->setName('upload');
        $this->setAttrib('enctype', 'multipart/form-data');
		$this->setAttrib('action', $this->getView()->url(array('module'=>'application', 'controller'=>'placement-test-marking','action'=>'upload-omr'),'default',true) );
          
        $file = new Zend_Form_Element_File('file');
        $file->setLabel('File')
            ->setDestination(APPLICATION_PATH  . '/tmp')
            ->setRequired(true);
		$this->addElement($file);
		
		$this->addElement('hidden', 'applicant_name', array(
			'description' => 'Applicant name',
		    'ignore' => true,
		    'decorators' => array(
		        	array('Description', array('escape'=>false, 'tag'=>'<h1>')),
		    	),
		));
		
	
		/*
		 * NAME
		 */
		$start = new Zend_Form_Element_Text('name_start');
        $start->setLabel('Start Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(52);
		$this->addElement($start);	
		
		$start = new Zend_Form_Element_Text('name_end');
        $start->setLabel('End Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(82);
		$this->addElement($start);	
		
		$element = new Zend_Form_Element_Checkbox('key_mapping_name');
		$element->setLabel('Mapping Key')
			->setAttrib('onchange','changeMapping(this)')
			->setCheckedValue(1)
			->setUncheckedValue(0)
			->setValue(1);
		$this->addElement($element);
		
		
		/*
		 * PES ID
		 */
		
		$this->addElement('hidden', 'applicant_pes_id', array(
			'description' => 'No Peserta',
		    'ignore' => true,
		    'decorators' => array(
		        	array('Description', array('escape'=>false, 'tag'=>'<h1>')),
		    	),
		));
		
		$start = new Zend_Form_Element_Text('pes_start');
        $start->setLabel('Start Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(0);
		$this->addElement($start);
		
		$start = new Zend_Form_Element_Text('pes_end');
        $start->setLabel('End Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(8);
		$this->addElement($start);
		
		$element = new Zend_Form_Element_Checkbox('key_mapping_pes');
		$element->setLabel('Mapping Key')
			->setAttrib('onchange','changeMapping(this)')
			->setCheckedValue(1)
			->setUncheckedValue(0);
		$this->addElement($element);
		
		/*
		 * FOMULIR ID
		 */
		
		$this->addElement('hidden', 'applicant_fomulir_id', array(
			'description' => 'No Fomulir',
		    'ignore' => true,
		    'decorators' => array(
		        	array('Description', array('escape'=>false, 'tag'=>'<h1>')),
		    	),
		));
		
		$start = new Zend_Form_Element_Text('fomulir_start');
        $start->setLabel('Start Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(0);
		$this->addElement($start);
		
		$start = new Zend_Form_Element_Text('fomulir_end');
        $start->setLabel('End Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(8);
		$this->addElement($start);
		
		$element = new Zend_Form_Element_Checkbox('key_mapping_fomulir');
		$element->setLabel('Mapping Key')
			->setAttrib('onchange','changeMapping(this)')
			->setCheckedValue(1)
			->setUncheckedValue(0);
		$this->addElement($element);
		
		/*
		 * SEATING ID
		 */
		
		$this->addElement('hidden', 'applicant_seating_id', array(
			'description' => 'No Seating',
		    'ignore' => true,
		    'decorators' => array(
		        	array('Description', array('escape'=>false, 'tag'=>'<h1>')),
		    	),
		));
		
		$start = new Zend_Form_Element_Text('seating_start');
        $start->setLabel('Start Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(1);
		$this->addElement($start);
		
		$start = new Zend_Form_Element_Text('seating_end');
        $start->setLabel('End Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(9);
		$this->addElement($start);
		
		
		/*
		 * PT CODE
		 */
		
		$this->addElement('hidden', 'pt_code', array(
			'description' => 'Placement Test Code',
		    'ignore' => true,
		    'decorators' => array(
		        	array('Description', array('escape'=>false, 'tag'=>'<h1>')),
		    	),
		));
		
		$start = new Zend_Form_Element_Text('pt_code_start');
        $start->setLabel('Start Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(40);
		$this->addElement($start);
		
		$start = new Zend_Form_Element_Text('pt_code_end');
        $start->setLabel('End Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(49);
		$this->addElement($start);
		
		/*
		 * QUESTION SET
		 */
		$this->addElement('hidden', 'set_code', array(
			'description' => 'Question Set Code',
		    'ignore' => true,
		    'decorators' => array(
		        	array('Description', array('escape'=>false, 'tag'=>'<h1>')),
		    	),
		));
		
		$start = new Zend_Form_Element_Text('q_code_start');
        $start->setLabel('Start Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(49);
		$this->addElement($start);
		
		$start = new Zend_Form_Element_Text('q_code_end');
        $start->setLabel('End Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(52);
		$this->addElement($start);
		
		/*
		 * QUESTION
		 */
		
		$this->addElement('hidden', 'question', array(
			'description' => 'Question',
		    'ignore' => true,
		    'decorators' => array(
		        	array('Description', array('escape'=>false, 'tag'=>'<h1>')),
		    	),
		));
		
		$start = new Zend_Form_Element_Text('q_start');
        $start->setLabel('Start Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(81);
		$this->addElement($start);
		
		$start = new Zend_Form_Element_Text('q_end');
        $start->setLabel('End Char')
        			->setAttrib('class','num')
                  ->setRequired(true)
                  ->addValidator('NotEmpty')
                  ->addValidator('Digits')
                  ->setValue(281);
		$this->addElement($start);
    }
}