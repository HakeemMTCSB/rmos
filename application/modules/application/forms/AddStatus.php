<?php

class Application_Form_AddStatus extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('sts_Id', 'Add_Checklist_Form');

        $this->addElement('select', 'sts_name', array(
            'label'    => 'Status',
            'required' => 'true'
        ));

        $this->sts_name->addMultiOption(null, "-- Select --");

        $definationDB = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $category = $definationDB->getListDataByCodeType('application-status');

        if (count($category) > 0) {
            foreach ($category as $list) {
                $this->sts_name->addMultiOption($list['id'], $list['name']);
            }
        }

        $this->addElement('text', 'sts_malayName', array(
            'label' => 'Malay Name',
            'class' => 'input-txt'
        ));

        $this->addElement('textarea', 'sts_desc', array(
            'label' => 'Information',
            'class' => 'input-txt'
        ));

        //button
        $this->addElement('submit', 'save_section_tagging', array(
            'label'      => 'Save',
            'decorators' => array('ViewHelper')
        ));

        $this->addDisplayGroup(array('save_section_tagging'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                array('HtmlTag', array('tag' => 'div', 'class' => 'buttons')),
                'DtDdWrapper'
            )
        ));

    }
}

?>

