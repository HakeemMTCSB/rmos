<?php
class Application_Form_Placementvenue extends Zend_Dojo_Form { //Formclass for the Placement venue module
	public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper");
		$UpdDate->removeDecorator("Label");
		$UpdDate->removeDecorator('HtmlTag');

		$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->removeDecorator("DtDdWrapper");
		$UpdUser->removeDecorator("Label");
		$UpdUser->removeDecorator('HtmlTag');

		$VenueName = new Zend_Form_Element_Text('VenueName');
		$VenueName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$VenueName->setAttrib('required',"true")
		->setAttrib('maxlength','50');
		$VenueName->removeDecorator("DtDdWrapper");
		$VenueName->removeDecorator("Label");
		$VenueName->removeDecorator('HtmlTag');

		$VenueAddress1 = new Zend_Form_Element_Text('VenueAddress1');
		$VenueAddress1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$VenueAddress1->setAttrib('required',"true")
		->setAttrib('maxlength','50');
		$VenueAddress1->removeDecorator("DtDdWrapper");
		$VenueAddress1->removeDecorator("Label");
		$VenueAddress1->removeDecorator('HtmlTag');
			
		$VenueAddress2 = new Zend_Form_Element_Text('VenueAddress2');
		$VenueAddress2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$VenueAddress2->setAttrib('maxlength','50');
		$VenueAddress2->removeDecorator("DtDdWrapper");
		$VenueAddress2->removeDecorator("Label");
		$VenueAddress2->removeDecorator('HtmlTag');

		$City = new Zend_Dojo_Form_Element_FilteringSelect('City');
		$City->removeDecorator("DtDdWrapper");
		$City->removeDecorator("Label");
		$City->removeDecorator('HtmlTag');
		$City->setAttrib('required',"false");
		$City->setRegisterInArrayValidator(false);
		$City->setAttrib('dojoType',"dijit.form.FilteringSelect");
			
		$Country = new Zend_Dojo_Form_Element_FilteringSelect('Country');
		$Country->removeDecorator("DtDdWrapper");
		$Country->setAttrib('required',"false") ;
		$Country->removeDecorator("Label");
		$Country->removeDecorator('HtmlTag');
		$Country->setAttrib('OnChange', 'fnGetCountryStateList');
		$Country->setRegisterInArrayValidator(false);
		$Country->setAttrib('dojoType',"dijit.form.FilteringSelect");
			
		$State = new Zend_Dojo_Form_Element_FilteringSelect('State');
		$State->removeDecorator("DtDdWrapper");
		$State->setAttrib('required',"false") ;
		$State->removeDecorator("Label");
		$State->removeDecorator('HtmlTag');
		$State->setRegisterInArrayValidator(false);
		$State->setAttrib('OnChange', 'fnGetStateCityList');
		$State->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$Phone = new Zend_Form_Element_Text('Phone',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Phone No."));
		$Phone->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Phone->setAttrib('maxlength','20');
		$Phone->setAttrib('style','width:93px');
		$Phone->removeDecorator("DtDdWrapper");
		$Phone->removeDecorator("Label");
		$Phone->removeDecorator('HtmlTag');

		$Contactperson = new Zend_Form_Element_Text('Contactperson');
		$Contactperson->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		->setAttrib('maxlength','100');
		$Contactperson->removeDecorator("DtDdWrapper");
		$Contactperson->removeDecorator("Label");
		$Contactperson->removeDecorator('HtmlTag');

		$Active  = new Zend_Form_Element_Checkbox('Active');
		$Active->setAttrib('dojoType',"dijit.form.CheckBox");
		$Active->setvalue('1');
		$Active->removeDecorator("DtDdWrapper");
		$Active->removeDecorator("Label");
		$Active->removeDecorator('HtmlTag');

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->dojotype="dijit.form.Button";
		$Save->label = $gstrtranslate->_("Save");
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator("Label");
		$Save->removeDecorator('HtmlTag')
		->class = "NormalBtn";

		$VenueCode = new Zend_Form_Element_Text('VenueCode');
		$VenueCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$VenueCode->setAttrib('required',"true")
		->setAttrib('maxlength','20')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

			
		$this->addElements(array($UpdDate,$UpdUser,$VenueName,$VenueAddress1,$VenueAddress2,$City,
				$Country,$State,$Phone,$Contactperson,$Active,$Save,$VenueCode));
	}
}