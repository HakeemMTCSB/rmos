<?php 
class Application_Form_CopyAdmissionChecklist extends Zend_Dojo_Form{
	
    public function init()
    {   
        $this->setMethod('post');
	$this->setAttrib('id', 'checklist_search_form');
        $IdProgram = $this->getAttrib('IdProgram');
        $sectionID = $this->getAttrib('sectionID');
        
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
       
        /*
         * copy from
         */
        $programSchemeFrom = new Zend_Form_Element_Select('programSchemeFrom');
        $programSchemeFrom->setRegisterInArrayValidator(false);
	$programSchemeFrom->removeDecorator("DtDdWrapper");
	$programSchemeFrom->setAttrib('required',"true");
        $programSchemeFrom->setAttrib('id', 'programschemefrom');
        $programSchemeFrom->setAttrib('class', 'span-7');
	$programSchemeFrom->removeDecorator("Label");
        
        $programSchemeFrom->addMultiOption('', '--All--');
        
        $progModel = new Application_Model_DbTable_ProgramScheme();
        
        if ($IdProgram){
            $defModel = new App_Model_General_DbTable_Definationms();
            $progSchemeList = $progModel->fnGetProgramschemeListBasedOnProgId($IdProgram);
            dd($progSchemeList);
            foreach($progSchemeList as $progSchemeListloop){
                $mod_of_study = $defModel->getData($progSchemeListloop['mode_of_study']);
                $mod_of_program = $defModel->getData($progSchemeListloop['mode_of_program']);
                $programType = $defModel->getData($progSchemeListloop['program_type']);

                $programSchemeFrom->addMultiOption($progSchemeListloop['IdProgramScheme'], $mod_of_study['DefinitionDesc'].' '.$mod_of_program['DefinitionDesc'].' '. $programType['DefinitionDesc']);
            }
        }
        
        $programFrom = new Zend_Form_Element_Select('programFrom');
        $programFrom->setRegisterInArrayValidator(false);
	$programFrom->removeDecorator("DtDdWrapper");
	$programFrom->setAttrib('required',"true");
        $programFrom->setAttrib('class', 'span-7');
        $programFrom->setAttrib('onchange', 'get_programschemefrom()');
        $programFrom->setAttrib('id', 'programfrom');
	$programFrom->removeDecorator("Label");
        
        //$progList = $progModel->fnGetProgramList();
        $progList = $progModel->fnGetProgramListGotScheme();
        
        $programFrom->addMultiOption('', '--All--');
        
        if (count($progList)>0){
            foreach ($progList as $progLoop){
                //$programFrom->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
                $programFrom->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName'] . " - " . $progLoop['ProgramCode']);
            }
        }
        
        $studentCategoryFrom = new Zend_Form_Element_Select('studentCategoryFrom');
        $studentCategoryFrom->setRegisterInArrayValidator(false);
	$studentCategoryFrom->removeDecorator("DtDdWrapper");
        $studentCategoryFrom->setAttrib('id', 'stdCtgyFrom');
        //$studentCategoryFrom->setAttrib('onchange', 'get_section('.$sectionID.')');
	$studentCategoryFrom->setAttrib('required',"true");
        $studentCategoryFrom->setAttrib('class', 'span-7');
	$studentCategoryFrom->removeDecorator("Label");
	$studentCategoryFrom->removeDecorator('HtmlTag');
        
        /*$defModel = new App_Model_General_DbTable_Definationms();
        $stdCtgyList = $defModel->getDataByType(95);*/
        $registryDB = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $stdCtgyList = $registryDB->getListDataByCodeType('student-category');
        
        $studentCategoryFrom->addMultiOption('', '--All--');
        
        if (count($stdCtgyList)>0){
            foreach ($stdCtgyList as $stdCtgyLoop){
                $studentCategoryFrom->addMultiOption($stdCtgyLoop['id'], $stdCtgyLoop['name']);
            }
        }
        
        /*
         * copy to
         */
        $programSchemeTo = new Zend_Form_Element_Select('programSchemeTo');
        $programSchemeTo->setRegisterInArrayValidator(false);
	$programSchemeTo->removeDecorator("DtDdWrapper");
	$programSchemeTo->setAttrib('required',"true");
        $programSchemeTo->setAttrib('id', 'programschemeto');
        $programSchemeTo->setAttrib('class', 'span-7');
	$programSchemeTo->removeDecorator("Label");
        
        $programSchemeTo->addMultiOption('', '--All--');
        
        if ($IdProgram){
            $defModel = new App_Model_General_DbTable_Definationms();
            $progSchemeList = $progModel->fnGetProgramschemeListBasedOnProgId($IdProgram);
            foreach($progSchemeList as $progSchemeListloop){
                $mod_of_study = $defModel->getData($progSchemeListloop['mode_of_study']);
                $mod_of_program = $defModel->getData($progSchemeListloop['mode_of_program']);
                $programType = $defModel->getData($progSchemeListloop['program_type']);

                $programSchemeTo->addMultiOption($progSchemeListloop['IdProgramScheme'], $mod_of_study['DefinitionDesc'].' '.$mod_of_program['DefinitionDesc'].' '. $programType['DefinitionDesc']);
            }
        }
        
        $programTo = new Zend_Form_Element_Select('programTo');
        $programTo->setRegisterInArrayValidator(false);
	$programTo->removeDecorator("DtDdWrapper");
	$programTo->setAttrib('required',"true");
        $programTo->setAttrib('class', 'span-7');
        $programTo->setAttrib('onchange', 'get_programschemeto()');
        $programTo->setAttrib('id', 'programto');
	$programTo->removeDecorator("Label");
        
        $programTo->addMultiOption('', '--All--');
        
        if (count($progList)>0){
            foreach ($progList as $progLoop){
                $programTo->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
            }
        }
        
        $studentCategoryTo = new Zend_Form_Element_Select('studentCategoryTo');
        $studentCategoryTo->setRegisterInArrayValidator(false);
	$studentCategoryTo->removeDecorator("DtDdWrapper");
        $studentCategoryTo->setAttrib('id', 'stdCtgyTo');
        //$studentCategoryTo->setAttrib('onchange', 'get_section('.$sectionID.')');
	$studentCategoryTo->setAttrib('required',"true");
        $studentCategoryTo->setAttrib('class', 'span-7');
	$studentCategoryTo->removeDecorator("Label");
	$studentCategoryTo->removeDecorator('HtmlTag');
        
        $studentCategoryTo->addMultiOption('', '--All--');
        
        if (count($stdCtgyList)>0){
            foreach ($stdCtgyList as $stdCtgyLoop){
                $studentCategoryTo->addMultiOption($stdCtgyLoop['id'], $stdCtgyLoop['name']);
            }
        }
        
        $submit = new Zend_Form_Element_Submit('Copy');
        $submit->dojotype="dijit.form.Button";
	$submit->label = $gstrtranslate->_("Copy");
	$submit->removeDecorator("DtDdWrapper");
	$submit->removeDecorator("Label");
	$submit->removeDecorator('HtmlTag')
            ->class = "NormalBtn";
        
        $this->addElements(array(
            $programFrom,
            $programSchemeFrom,
            $studentCategoryFrom,
            $programTo,
            $programSchemeTo,
            $studentCategoryTo,
            $submit
        ));
		
    }
}
?>