<?php
class Application_Form_SuratKeputusan extends Zend_Form
{
	protected $_academicyear;
    
	public function setAcademicyear($value) {
		$this->_academicyear = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
		     
        $registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
				
		
		 //academic year
		$this->addElement('select','academic_year', array(
			'label'=>'Academic Year',
			'onchange'=>'changeNomor(this);',
		    'required'=>true
		));
		
		$academicYearDb = new App_Model_Record_DbTable_AcademicYear();
		$academicYearList = $academicYearDb->getData();		
    			
		$this->academic_year->addMultiOption(null,"-- Please Select --");
		foreach ($academicYearList as $list){
			$this->academic_year->addMultiOption($list['ay_id'],$list['ay_code']);
		}
		$this->academic_year->setValue($this->_academicyear);
		
		
		//faculty
		$this->addElement('select','faculty', array(
			'label'=>'Faculty',		
		    'onchange'=>'changeNomor();',  
		    'required'=>false,
			'registerInArrayValidator' => false
		));
		
		$collegeDB = new App_Model_General_DbTable_Collegemaster();
		$college_data = $collegeDB->getFaculty();		
    			
		$this->faculty->addMultiOption(null,"All");
		foreach ($college_data as $list){
			if($locale=="id_ID"){
				$college_name = $list["ArabicName"];
			}elseif($locale=="en_US"){
				$college_name = $list["CollegeName"];
			}
			$this->faculty->addMultiOption($list['IdCollege'],strtoupper($college_name));
		}
	
		
		//rector decree
		$this->addElement('select','nomor', array(
			'label'=>'Rector Decree Number',
		));
		
		$this->nomor->addMultiOption(null,"All");
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
	    $select = $db ->select()
					  ->from(array('as'=>'applicant_assessment'), array())
					  ->join(array('asd'=>'applicant_selection_detl'), 'asd.asd_id = as.aar_rector_selectionid', array('DISTINCT(asd.asd_nomor)'));
		$stmt = $db->query($select);
        $row = $stmt->fetchAll();
		
		foreach ($row as $list){
			$this->nomor->addMultiOption($list['asd_nomor'],$list['asd_nomor']);
		}

		/* $this->addElement('select','academic_year', array(
			'label'=>'Academic Year',
		    'required'=>true,
	    	'onchange'=>"getPeriod(this);"	
		));
		
		$academicDB = new App_Model_Record_DbTable_AcademicYear();
		$academic_year_data = $academicDB->getData();		
    			
		$this->academic_year->addMultiOption(0,"-- Please Select --");
		foreach ($academic_year_data as $list){
			$this->academic_year->addMultiOption($list['ay_id'],$list['ay_code']);
		}
		
		
		$this->addElement('select','period', array(
			'label'=>'Period',
		    'required'=>true
		    
		
		));
		
		$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period_data = $periodDB->getData();	
    			
		$this->period->addMultiOption(0,"-- Please Select --");
		foreach ($period_data as $list){
			$this->period->addMultiOption($list['ap_id'],$list['ap_desc']);
		}
		
		
		*/

     /*	$this->addElement('select','nomor', array(
			'label'=>'Nomor',
		    'required'=>true
		));
		
		$selectionDetlDB = new App_Model_Application_DbTable_ApplicantSelectionDetl();
		$selectionDetl = $selectionDetlDB->getInfo(2);		//2:Rector
    			
		$this->nomor->addMultiOption(null,"-- Silahkan Pilih --");
		foreach ($selectionDetl as $list){
			$this->nomor->addMultiOption($list['asd_id'],$list['asd_nomor']);
		}*/
     
		
       
		
		
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
		  'onclick'=>"openList()",
          'decorators'=>array('ViewHelper')
        ));
        
        
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));

	}
	
}
?>