<?php

class Application_Form_ApplicationItemTagging extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('id', 'application_item_tagging');

        $addoredit = $this->getAttrib('addoredit');
        $section_id = $this->getAttrib('section_id');

        $this->addElement('hidden', 'sectionId', array(
            'id'      => 'key',
            'disable' => 'true',
        ));

        $this->addElement('hidden', 'formType', array(
            'id'      => 'formType',
            'disable' => 'true',
        ));

        if ($addoredit == 'add') {

            $this->addElement('select', 'idItem', array(
                'label'                    => 'Item',
                'required'                 => 'true',
                'id'                       => 'item',
                'registerInArrayValidator' => false,
                'onChange'                 => 'get_docList()'
            ));

            $this->idItem->addMultiOption(null, "-- Select --");

            $itemModel = new Application_Model_DbTable_ApplicationItem();
            $item_list = $itemModel->fnGetItemListDropDown($section_id);
            //var_dump($item_list);
            if (count($item_list) > 0) {
                foreach ($item_list as $list) {
                    $this->idItem->addMultiOption($list['id'], $list['name'] . " (" . $list['DefinitionCode'] . ')');
                }
            }
        } else {
            $this->addElement('text', 'itemname', array(
                //'id'=>'instruction',
                'disable' => 'true',
                'label'   => 'Item'
            ));

        }

        $this->addElement('select', 'item_dclId', array(
            'label'    => 'Admission Checklist',
            'required' => false,
            'id'       => 'checklist',
//            'registerInArrayValidator' => false,
        ));

        $this->item_dclId->addMultiOption(null, "-- Select --");

        $sectionConfigDB = new Application_Model_DbTable_ApplicationInitialConfig();
        $section_info = $sectionConfigDB->fnGetSectionConfById($section_id);

        $sdtCtgy = $section_info['category'] ?? 0;
        $progScheme = $section_info['IdProgramScheme'] ?? 0;

        $dclModel = new Application_Model_DbTable_DocumentChecklist();
        $dcl_list = $dclModel->getDclListByStudentCtgy($sdtCtgy, 1, $progScheme, $section_id);

        if (count($dcl_list) > 0) {
            foreach ($dcl_list as $list) {
                $this->item_dclId->addMultiOption($list['dcl_Id'], $list['dcl_name']);
            }
        }

        $this->addElement('select', 'item_docDownload', array(
            'label'                    => 'Document Download',
            'id'                       => 'item_docDownload',
            'registerInArrayValidator' => false
        ));

        $this->item_docDownload->addMultiOption(null, "-- Select --");

        $appModel = new Application_Model_DbTable_ApplicationInitialConfig();
        $tplList = $appModel->fnGetTemplateList();

        if (count($tplList) > 0) {
            foreach ($tplList as $list) {
                $this->item_docDownload->addMultiOption($list['tpl_id'], $list['tpl_name']);
            }
        }

        $this->addElement('radio', 'itemInstructionPosition', array(
            'label' => 'Instruction Position'));

        $this->itemInstructionPosition->addMultiOptions(array(
            '1' => 'Top',
            '2' => 'Bottom'
        ))->setSeparator('&nbsp;&nbsp;')->setValue("1");

        $this->addElement('textarea', 'itemTagInstruction', array(
            'id'    => 'instruction',
            'label' => 'Instruction'
        ));

        $this->addElement('textarea', 'tagDesc', array(
            'label' => 'Tooltip'
        ));

        $this->addElement('checkbox', 'view', array(
            'label'          => 'View',
            'name'           => 'view',
            'checkedValue'   => '1',
            'uncheckedValue' => '0',
            'disableHidden'  => true
        ));

        $this->addElement('checkbox', 'compulsory', array(
            'label'          => 'Compulsory',
            'name'           => 'compulsory',
            'checkedValue'   => '1',
            'uncheckedValue' => '0',
            'disableHidden'  => true
        ));

        $this->addElement('checkbox', 'enable', array(
            'label'          => 'Enable',
            'name'           => 'enable',
            'checkedValue'   => '1',
            'uncheckedValue' => '0',
            'disableHidden'  => true
        ));

        $this->addElement('checkbox', 'hide', array(
            'label'          => 'Hide',
            'name'           => 'hide',
            'checkedValue'   => '1',
            'uncheckedValue' => '0',
            'disableHidden'  => true
        ));

        //button
        $this->addElement('submit', 'save_item_tagging', array(
            'label'      => 'Save',
            'decorators' => array('ViewHelper')
        ));

        /*$this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'application', 'controller'=>'application-initial-configuration','action'=>'index'),'default',true) . "'; return false;"
        ));*/

        $this->addDisplayGroup(array('save_item_tagging'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                array('HtmlTag', array('tag' => 'div', 'class' => 'buttons')),
                'DtDdWrapper'
            )
        ));
    }
}

?>