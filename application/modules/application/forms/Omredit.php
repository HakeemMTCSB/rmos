<?php
class Application_Form_Omredit extends Zend_Form
{
	public function init()
	{
	

		$this->setMethod('post');
		$this->setAttrib('id','omrfilest');

		$format = new Zend_Form_Element_Text('tmp_ptest_code');
        $format->setLabel($this->getView()->translate('Nomor Peserta'))
                  ->setRequired(true)
                  ->addValidator('NotEmpty');
		$this->addElement($format);				
	

		$format = new Zend_Form_Element_Text('tmp_name');
        $format->setLabel($this->getView()->translate('Nama'))
                  ->setRequired(true)
                  ->addValidator('NotEmpty');
		$this->addElement($format);			
		
		$format = new Zend_Form_Element_Text('tmp_set_code');
        $format->setLabel($this->getView()->translate('Set Code'))
                  ->setRequired(true)
                  ->addValidator('NotEmpty');
		$this->addElement($format);	

		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('Save'),
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'application', 'controller'=>'placement-test','action'=>'index'),'default',true) . "'; return false;"
        ));	
        
	}
}
?>