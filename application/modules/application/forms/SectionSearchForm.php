<?php 
class Application_Form_SectionSearchForm extends Zend_Form{
	
    public function init()
    {
        $this->setMethod('post');
	$this->setAttrib('IdProgram', 'section_search_form');
		
	$this->addElement('select','IdProgram', array(
            'label'=>'Program',
            'required'=>'true',
            'id'=>'programsearch',
            'onChange'=>'get_programscheme()'
	));
				
	$this->IdProgram->addMultiOption(null,"-- All --");		
	
	$get_program_list = new Application_Model_DbTable_ProgramScheme();
	$program_list = $get_program_list->fnGetProgramList();
		
	if(count($program_list)>0){
            foreach ($program_list as $list){		
		$this->IdProgram->addMultiOption($list['IdProgram'],$list['ProgramName']);
            }
	}
        
        $this->addElement('select','IdProgramScheme', array(
            'label'=>'Program Scheme',
            'required'=>'true',
            'id'=>'programschemesearch',
            'registerInArrayValidator' => false
            //'onChange'=>'getTitle()'
	));
				
	$this->IdProgramScheme->addMultiOption(null,"-- All --");
        
        $this->addElement('select','sectionID', array(
            'label'=>'Section',
            'required'=>'true',
            'id'=>'sectionsearch'
	));
				
	$this->sectionID->addMultiOption(null,"-- All --");		
	
	$get_section_list = new Application_Model_DbTable_ApplicationInitialConfiguration();
	$section_list = $get_section_list->fnGetSectionList();
		
	if(count($section_list)>0){
            foreach ($section_list as $list){		
		$this->sectionID->addMultiOption($list['id'],$list['name']);
            }
	}
        
        $this->addElement('select','sdtCtgy', array(
            'label'=>'Student Category',
            'required'=>'true',
            'id'=>'studentcategorysearch',
            //'onChange'=>'get_programscheme()'
	));
				
	$this->sdtCtgy->addMultiOption(null,"-- All --");		
	
        $definationDB = new App_Model_General_DbTable_Definationms();
        $category = $definationDB->getDataByType(95);
		
	if(count($category)>0){
            foreach ($category as $list){		
		$this->sdtCtgy->addMultiOption($list['idDefinition'],$list['DefinitionDesc']);
            }
	}
        
        $this->addElement('textarea','desc', array(
            'label'=>'Desciption'
	));
        
        //button
	$this->addElement('button', 'search', array(
          'label'=>'Search',
          'onClick'=>'searchSection()',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addDisplayGroup(array('search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));	
	}
}
?>