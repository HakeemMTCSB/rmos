<?php 
class Application_Form_AddSpesificChecklist extends Zend_Dojo_Form{
	
    public function init()
    {   
        $this->setMethod('post');
        $stdCtgy = $this->getAttrib('std_Ctgy');
        $IdProgramScheme = $this->getAttrib('IdProgramScheme');
        
        $dcl_name = new Zend_Form_Element_Text('dcl_name');
	$dcl_name->setAttrib('class', 'span-7')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $dcl_malayName = new Zend_Form_Element_Text('dcl_malayName');
	$dcl_malayName->setAttrib('class', 'span-7')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $dcl_activeStatus = new Zend_Form_Element_Checkbox('dcl_activeStatus');
        $dcl_activeStatus->removeDecorator("DtDdWrapper");
        $dcl_activeStatus->setAttrib('id', 'dcl_activeStatus');
        $dcl_activeStatus->setAttrib('class', 'span-7');
        $dcl_activeStatus->setCheckedValue(1);
        $dcl_activeStatus->setUncheckedValue(0);
	$dcl_activeStatus->removeDecorator("Label");
        
        $dcl_mandatory = new Zend_Form_Element_Checkbox('dcl_mandatory');
        $dcl_mandatory->removeDecorator("DtDdWrapper");
        $dcl_mandatory->setAttrib('id', 'dcl_mandatory');
        $dcl_mandatory->setAttrib('class', 'span-7');
        $dcl_mandatory->setCheckedValue(1);
        $dcl_mandatory->setUncheckedValue(0);
	$dcl_mandatory->removeDecorator("Label");
        
        $dcl_finance = new Zend_Form_Element_Checkbox('dcl_finance');
        $dcl_finance->removeDecorator("DtDdWrapper");
        $dcl_finance->setAttrib('id', 'dcl_finance');
        $dcl_finance->setAttrib('class', 'span-7');
        $dcl_finance->setCheckedValue(1);
        $dcl_finance->setUncheckedValue(0);
	$dcl_finance->removeDecorator("Label");
        
        $dcl_uplStatus = new Zend_Form_Element_Checkbox('dcl_uplStatus');
        $dcl_uplStatus->removeDecorator("DtDdWrapper");
        $dcl_uplStatus->setAttrib('id', 'dcl_uplStatus');
        $dcl_uplStatus->setAttrib('class', 'span-7');
        $dcl_uplStatus->setCheckedValue(1);
        $dcl_uplStatus->setUncheckedValue(0);
	$dcl_uplStatus->removeDecorator("Label");
        
        $this->addElement('checkbox', 'dcl_uplStatus', array(
            //'label' => 'Upload',
            //'name' => 'upload',
            'checkedValue' => '1',
            'uncheckedValue' => '0',
            'disableHidden' => true
        ));
        
        $dcl_uplType = new Zend_Form_Element_Select('dcl_uplType');
	$dcl_uplType->removeDecorator("DtDdWrapper");
        $dcl_uplType->setAttrib('id', 'dcl_uplType');
        $dcl_uplType->setAttrib('class', 'span-7');
	$dcl_uplType->removeDecorator("Label");
        
        $dcl_uplType->addMultiOption('', '-- Select --');
        
        $definationDB = new App_Model_General_DbTable_Definationms();
        $uplType = $definationDB->getDataByType(10);
		
	if(count($uplType)>0){
            foreach ($uplType as $list){		
		$dcl_uplType->addMultiOption($list['idDefinition'],$list['DefinitionDesc']);
            }
	}
        
        $this->addElement('textarea','dcl_desc', array(
            'style'=>'width: 270px; height: 120px;'
	));
        
        $this->addElements(array(
            $dcl_name,
            $dcl_uplType,
            $dcl_malayName,
            $dcl_activeStatus,
            $dcl_mandatory,
            $dcl_finance,
            $dcl_uplStatus
        ));
    }
}
?>