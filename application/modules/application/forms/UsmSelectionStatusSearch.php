<?php
class Application_Form_UsmSelectionStatusSearch extends Zend_Form
{
	protected $_ptest;
	protected $_ptest_date;
	protected $_intake;
    protected $_programme;
	protected $_preference;
	
	
	public function setPtest($value) {
		$this->_ptest = $value;
	}
	
	public function setPtest_date($value) {
		$this->_ptest_date = $value;
	}
		
	public function setIntake($value) {
		$this->_intake = $value;
	}
	
	public function setProgramme($value) {
		$this->_programme = $value;
	}
	
	public function setPreference($value) {
		$this->_preference = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
				
  
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		
		//intake
		$this->addElement('select','intake', array(
			'label'=>'Intake',
			
		    'required'=>true
		));
		
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intakeList = $intakeDb->getData();		
    			
		$this->intake->addMultiOption(null,"-- Please Select --");
		foreach ($intakeList as $list){
			$this->intake->addMultiOption($list['IdIntake'],$list['IntakeDefaultLanguage']);
		}
		
		
		
		//period
		/*$this->addElement('select','period', array(
			'label'=>'Period',
		    'required'=>false	
		
		));

		$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
		$period = $periodDB->getDataByIntake($this->_intake);
		
		$this->period->addMultiOption(null,"All");
		foreach ($period as $list){
			$this->period->addMultiOption($list['ap_id'],$list['ap_desc']);
		}	*/
		
		
	   $this->addElement('select','ptest', array(
			'label'=>'Placement Test',
		    'required'=>true	,
		    'onChange'=>'getPtest(this)'
		
		));
		
		$placementDB = new App_Model_Application_DbTable_ApplPlacementHead();
    	$placement_data = $placementDB->getDatabyCode();	
    			
		$this->ptest->addMultiOption(null,"-- Select Placement Test --");
		foreach ($placement_data as $list){
			$this->ptest->addMultiOption($list['aph_placement_code'],$list['aph_placement_code']);
		}
		
		
	    $scheduleDB =  new App_Model_Application_DbTable_ApplicantPlacementSchedule();
		$schedule = $scheduleDB->getPtestDate($this->_ptest);
		
        $this->addElement('select','aps_test_date', array(
			'label'=>'Date',
			'required'=>false,
		    'onChange'=>"getLocation(this);"
		));
		$this->aps_test_date->setRegisterInArrayValidator(false);	
		
		$this->aps_test_date->addMultiOption(null,"-- All --");
		foreach ($schedule as $list){
			$this->aps_test_date->addMultiOption($list['aps_test_date'],$list['aps_test_date']);
		}
		
		
		
		$this->addElement('select','aps_id', array(
			'label'=>'Location'
			
		));	
		
		$this->aps_id->setRegisterInArrayValidator(false);
		
		$this->aps_id->addMultiOption(null,"-- All --");
		$placement_schedule = $scheduleDB->getLocationByDate($this->_ptest_date);
		foreach ($placement_schedule as $list){
			$this->aps_id->addMultiOption($list['aps_id'],$list['location_name']);
		}
		
		//faculty
		$this->addElement('select','faculty', array(
			'label'=>'Faculty',
		    'onChange'=>"getProgramme(this,$('#programme'))",
		    'required'=>false
		));
		
		$collegeDB = new App_Model_General_DbTable_Collegemaster();
		$college_data = $collegeDB->getFaculty();		
    			
		$this->faculty->addMultiOption(null,"-- All --");
		foreach ($college_data as $list){
			if($locale=="id_ID"){
				$college_name = $list["ArabicName"];
			}elseif($locale=="en_US"){
				$college_name = $list["CollegeName"];
			}
			$this->faculty->addMultiOption($list['IdCollege'],strtoupper($college_name));
		}
		
		
		
		$this->addElement('select','programme', array(
			'label'=>'Programme',
		    'required'=>false		
		
		));
		
		$programDB = new App_Model_Record_DbTable_Program();
    	$program_data = $programDB->searchProgramByFaculty($this->_facultyid);    	
		
		$this->programme->addMultiOption(null,"-- All --");
		foreach ($program_data as $list){
			
			if($locale=="id_ID"){
				$program_name = $list["ArabicName"];
			}elseif($locale=="en_US"){
				$program_name = $list["ProgramName"];
			}			
			
			$this->programme->addMultiOption($list['ProgramCode'],$program_name);
		}
		
		
		
		/*$this->addElement('select','preference', array(
				'label'=>'Program Preference'
		       ));
		
		$this->preference->addMultiOption(null,"-- All --");
		$this->preference->addMultiOption(1,"Preference 1");
		$this->preference->addMultiOption(2,"Preference 2");
		*/
		
		
		$this->addElement('select','selection_status', array(
			'label'=>'selection status'	
		));		
	
		$this->selection_status->addMultiOption(null,"-- All --");
		$this->selection_status->addMultiOption(0,"Waiting For Simulation");
		$this->selection_status->addMultiOption(4,"Waiting For Committee Verification");
		$this->selection_status->addMultiOption(1,"Waiting For Rector Approval");	
		$this->selection_status->addMultiOption(3,"Offer");	
		$this->selection_status->addMultiOption(5,"Reject");		
		
				
		
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
                    
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));

	}
	
}

?>