<?php 
class Application_Form_EditSectionTagging extends Zend_Form{
	
    public function init()
    {
        $this->setMethod('post');
	$this->setAttrib('id', 'edit_section_tagging_form');
		
	$this->addElement('select','IdProgram', array(
            'label'=>'Program',
            'required'=>'true',
            'id'=>'program',
            'onChange'=>'get_programscheme()'
	));
				
	$this->IdProgram->addMultiOption(null,"-- All --");		
	
	$get_program_list = new Application_Model_DbTable_ProgramScheme();
	$program_list = $get_program_list->fnGetProgramList();
		
	if(count($program_list)>0){
            foreach ($program_list as $list){		
		$this->IdProgram->addMultiOption($list['IdProgram'],$list['ProgramName']);
            }
	}
        
        $this->addElement('select','programID', array(
            'label'=>'Program Scheme',
            'required'=>'true',
            'id'=>'programscheme',
            'registerInArrayValidator' => false
            //'onChange'=>'getTitle()'
	));
				
	$this->programID->addMultiOption(null,"-- All --");
        
        $scholarshipModel = new Application_Model_DbTable_ApplicationInitialConfig;
        $defModel = new App_Model_General_DbTable_Definationms();
        
        $progSchemeList = $scholarshipModel->getProgSchemeList();
       
        $i = 0;
        
        foreach($progSchemeList as $progSchemeListloop){
            $mod_of_study = $defModel->getData($progSchemeListloop['mode_of_study']);
            $mod_of_program = $defModel->getData($progSchemeListloop['mode_of_program']);
            
            $progSchemeList[$i]['mos_name'] = $mod_of_study['DefinitionDesc'];
            $progSchemeList[$i]['mop_name'] = $mod_of_program['DefinitionDesc'];
            
            $i++;
        }
        
        if(count($progSchemeList)>0){
            foreach ($progSchemeList as $list){		
		$this->programID->addMultiOption($list['IdProgramScheme'],$list['mos_name'].$list['mop_name']);
            }
	}
        
        $this->addElement('select','sectionID', array(
            'label'=>'Section',
            'required'=>'true',
            //'onChange'=>'getTitle()'
	));
				
	$this->sectionID->addMultiOption(null,"-- All --");		
	
	$get_section_list = new Application_Model_DbTable_ApplicationInitialConfiguration();
	$section_list = $get_section_list->fnGetSectionList();
		
	if(count($section_list)>0){
            foreach ($section_list as $list){		
		$this->sectionID->addMultiOption($list['id'],$list['name']);
            }
	}
        
        $this->addElement('textarea','desc', array(
            'label'=>'Desciption'
	));
        
        //button
	$this->addElement('submit', 'save_section_tagging', array(
          'label'=>'Save',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'application', 'controller'=>'application-initial-configuration','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save_section_tagging','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	));
		
    }
}
?>