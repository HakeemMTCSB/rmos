<?php

class Application_Form_AgentProfile extends Zend_Dojo_Form
{ //Formclass for the user module
    public function init()
    {
        $gstrtranslate = Zend_Registry::get('Zend_Translate');
        $lstrAmountConstraint = "{pattern:'0.##'}";

        $lobjsubject = new Application_Model_DbTable_Subjectsetup();
        $lobjsubjectlist = $lobjsubject->fngetsubjectList();
        $lobjRegistryvalue = new GeneralSetup_Model_DbTable_Registry_RegistryValue();
        $lobjagenttype = $lobjRegistryvalue->getList('38');
        $lobjagentstatus = $lobjRegistryvalue->getList('39');

        $id = new Zend_Form_Element_Hidden('id');
        $id->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $created_at = new Zend_Form_Element_Hidden('created_at');
        $created_at->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $created_by = new Zend_Form_Element_Hidden('created_by');
        $created_by->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $name = new Zend_Form_Element_Text('name');
        $name->setAttrib('dojoType', "dijit.form.ValidationTextBox")
            ->setAttrib('required', "true")
            ->setAttrib('maxlength', '150')
            //->setAttrib('autocapitalize','on')
            //->setAttrib('onchange','toUpperCase(this.value);')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $code = new Zend_Form_Element_Text('code');
        $code->setAttrib('dojoType', "dijit.form.ValidationTextBox")
            ->setAttrib('required', "false")
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag')
            ->setAttrib('uppercase', "true");


        $type = new Zend_Dojo_Form_Element_FilteringSelect('type');
        $type->removeDecorator("DtDdWrapper");
        $type->setAttrib('required', "false");
        $type->removeDecorator("Label");
        $type->removeDecorator('HtmlTag');
        $type->setRegisterInArrayValidator(false);
        $type->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $type->addMultiOptions($lobjagenttype);

        $status_id = new Zend_Dojo_Form_Element_FilteringSelect('status_id');
        $status_id->removeDecorator("DtDdWrapper");
        $status_id->setAttrib('required', "false");
        $status_id->removeDecorator("Label");
        $status_id->removeDecorator('HtmlTag');
        $status_id->setRegisterInArrayValidator(false);
        $status_id->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $status_id->addMultiOptions($lobjagentstatus);

        $payment_plan_id = new Zend_Dojo_Form_Element_FilteringSelect('payment_plan_id');
        $payment_plan_id->removeDecorator("DtDdWrapper");
        $payment_plan_id->setAttrib('required', "false");
        $payment_plan_id->removeDecorator("Label");
        $payment_plan_id->removeDecorator('HtmlTag');
        $payment_plan_id->setRegisterInArrayValidator(false);
        $payment_plan_id->setAttrib('dojoType', "dijit.form.FilteringSelect");

        $ContactPerson = new Zend_Form_Element_Text('ContactPerson');
        $ContactPerson->setAttrib('dojoType', "dijit.form.ValidationTextBox")
            //->setAttrib('required',"true")
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');


        $email = new Zend_Form_Element_Text('email');
        $email->setAttrib('dojoType', "dijit.form.ValidationTextBox")
            //->setAttrib('required',"true")
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $Address1 = new Zend_Form_Element_Text('Address1');
        $Address1->setAttrib('dojoType', "dijit.form.ValidationTextBox")
            //->setAttrib('required',"true")
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $Address2 = new Zend_Form_Element_Text('Address2');
        $Address2->setAttrib('dojoType', "dijit.form.ValidationTextBox")
            //->setAttrib('required',"false")
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $lobjuser = new GeneralSetup_Model_DbTable_User();
        $lobjcountry = $lobjuser->fnGetCountryList();
        $Country = new Zend_Dojo_Form_Element_FilteringSelect('Country');
        $Country->removeDecorator("DtDdWrapper");
        $Country->setAttrib('required', "false");
        $Country->removeDecorator("Label");
        $Country->removeDecorator('HtmlTag');
        $Country->setAttrib('OnChange', "fnGetCountryStateList");
        $Country->setRegisterInArrayValidator(false);
        $Country->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $Country->addMultiOptions($lobjcountry);

        $State = new Zend_Dojo_Form_Element_FilteringSelect('State');
        $State->removeDecorator("DtDdWrapper");
        //$state->setAttrib('required',"true") ;
        $State->removeDecorator("Label");
        $State->removeDecorator('HtmlTag');
        $State->setRegisterInArrayValidator(false);
        $State->setAttrib('OnChange', 'fnGetStateCityList');
        $State->setAttrib('dojoType',"dijit.form.FilteringSelect");


        $City = new Zend_Dojo_Form_Element_FilteringSelect('City');
        $City->removeDecorator("DtDdWrapper");
        $City->removeDecorator("Label");
        $City->removeDecorator('HtmlTag');
        $City->setAttrib('required',"false");
        $City->setRegisterInArrayValidator(false);
        $City->setAttrib('dojoType',"dijit.form.FilteringSelect");

        $Zip = new Zend_Form_Element_Text('Zip');
        $Zip->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $Zip->setAttrib('maxlength', '20');
        $Zip->removeDecorator("DtDdWrapper");
        $Zip->removeDecorator("Label");
        $Zip->removeDecorator('HtmlTag');

        $phone_no = new Zend_Form_Element_Text('phone_no', array('regExp' => "[0-9]+", 'invalidMessage' => "Not a valid Phone No."));
        $phone_no->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $phone_no->setAttrib('maxlength', '9');
        $phone_no->setAttrib('style', 'width:93px');
        $phone_no->removeDecorator("DtDdWrapper");
        $phone_no->removeDecorator("Label");
        $phone_no->removeDecorator('HtmlTag');

        $fax_no = new Zend_Form_Element_Text('fax_no', array('regExp' => "[0-9()+-]+", 'invalidMessage' => "Not a valid Fax"));
        $fax_no->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $fax_no->setAttrib('style', 'width:93px');
        $fax_no->setAttrib('maxlength', '20');
        $fax_no->removeDecorator("DtDdWrapper");
        $fax_no->removeDecorator("Label");
        $fax_no->removeDecorator('HtmlTag');

        $password = new Zend_Form_Element_Text('password');
        $password->setAttrib('dojoType', "dijit.form.ValidationTextBox")
            //->setAttrib('required',"true")
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag')
            ->setAttrib('uppercase', "true");;

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype = "dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->setAttrib('class', 'NormalBtn');
        $Save->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Search = new Zend_Form_Element_Submit('Search');
        $Search->setAttrib('id', 'search')
            ->setAttrib('name', 'search')
            ->setAttrib('class', 'NormalBtn')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $Add = new Zend_Form_Element_Button('Add');
        $Add->setAttrib('class', 'NormalBtn');
        $Add->setAttrib('dojoType', "dijit.form.Button");
        $Add->setAttrib('OnClick', 'addSubjectentry()')
            ->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $clear = new Zend_Form_Element_Button('Clear');
        $clear->setAttrib('class', 'NormalBtn');
        $clear->setAttrib('dojoType', "dijit.form.Button");
        $clear->label = $gstrtranslate->_("Clear");
        $clear->setAttrib('OnClick', 'clearpageAdd()');
        $clear->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        $IdRegion = new Zend_Dojo_Form_Element_FilteringSelect('IdRegion');
        $IdRegion->removeDecorator("DtDdWrapper");
        $IdRegion->setAttrib('required', "false");
        $IdRegion->removeDecorator("Label");
        $IdRegion->removeDecorator('HtmlTag');
        $IdRegion->setRegisterInArrayValidator(false);
        $IdRegion->setAttrib('dojoType', "dijit.form.FilteringSelect");
        $IdRegion->addMultiOptions($lobjsubjectlist);

        $RegistrationLocationDate = new Zend_Form_Element_Text('RegistrationLocationDate');
        $RegistrationLocationDate->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $RegistrationLocationDate->setAttrib('maxlength','20');
        $RegistrationLocationDate->removeDecorator("DtDdWrapper");
        $RegistrationLocationDate->removeDecorator("Label");
        $RegistrationLocationDate->removeDecorator('HtmlTag');

        $RegistrationLocationTime = new Zend_Form_Element_Text('RegistrationLocationTime');
        $RegistrationLocationTime->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $RegistrationLocationTime->setAttrib('maxlength','20');
        $RegistrationLocationTime->removeDecorator("DtDdWrapper");
        $RegistrationLocationTime->removeDecorator("Label");
        $RegistrationLocationTime->removeDecorator('HtmlTag');

        $RegistrationLocationRemarks = new Zend_Form_Element_Textarea('RegistrationLocationRemarks');
        //$RegistrationLocationRemarks->setAttrib('dojoType',"dijit.form.Textarea");
        $RegistrationLocationRemarks->setAttrib('rows','5');
        $RegistrationLocationRemarks->setAttrib('cols','30 ');
        $RegistrationLocationRemarks->removeDecorator("DtDdWrapper");
        $RegistrationLocationRemarks->removeDecorator("Label");
        $RegistrationLocationRemarks->removeDecorator('HtmlTag');

        $clear = new Zend_Form_Element_Button('Clear');
        $clear->setAttrib('class', 'NormalBtn');
        $clear->setAttrib('dojoType',"dijit.form.Button");
        $clear->label = $gstrtranslate->_("Clear");
        $clear->setAttrib('OnClick', 'clearregistrationpoint()');
        $clear->removeDecorator("Label")
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($id,
            $name,
            $ContactPerson,
            $email,
            $Address1,
            $Address2,
            $Zip,
            $Country,
            $State,
            $City,
            $code,
            $type,
            $payment_plan_id,
            $status_id,
            $phone_no,
            $fax_no,
            $created_at,
            $created_by,
            $status_id,
            $password,
            $Save,
            $Search,
            $IdRegion,
            $Add,
            $clear));
    }
}