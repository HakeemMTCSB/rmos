<?php
class Application_Form_SearchSummaryUsm extends Zend_Form
{
	
	protected $_ptest;
	protected $_ptest_date;
	
	public function setPtest($value) {
		$this->_ptest = $value;
	}
	
	public function setPtest_date($value) {
		$this->_ptest_date = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
				
  
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		//intake
		$this->addElement('select','intake_id', array(
			'label'=>'Intake',
		//	'onchange'=>'getPeriod(this)',
		    'required'=>true
		));
		
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intakeList = $intakeDb->getData();		
    			
		$this->intake_id->addMultiOption(null,"-- Select Intake --");
		foreach ($intakeList as $list){
			$this->intake_id->addMultiOption($list['IdIntake'],$list['IntakeDefaultLanguage']);
		}
		
		
		//period
		/*$this->addElement('select','period_id', array(
			'label'=>'Period',
		    'required'=>false	
		
		));
		
		$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period_data = $periodDB->getData();	
    			
		$this->period_id->addMultiOption(null,"-- Select Period --");
		foreach ($period_data as $list){
			$this->period_id->addMultiOption($list['ap_id'],$list['ap_desc']);
		}
		
		//load previous period
		$this->addElement('checkbox','load_previous_period', array(
			'label'=>'Include Previous Period',
			"checked" => "checked"
		));*/
		
	   //period
		$this->addElement('select','ptest', array(
			'label'=>'Placement Test',
		    'required'=>true,
			'onchange'=>'getPtest(this)',
		
		));
		
		$placementDB = new App_Model_Application_DbTable_ApplPlacementHead();
    	$placement_data = $placementDB->getDatabyCode();	
    			
		$this->ptest->addMultiOption(null,"-- Select Placement Test --");
		foreach ($placement_data as $list){
			$this->ptest->addMultiOption($list['aph_placement_code'],$list['aph_placement_code']);
		}		
		

		$scheduleDB =  new App_Model_Application_DbTable_ApplicantPlacementSchedule();
		$schedule = $scheduleDB->getPtestDate($this->_ptest);
		
        $this->addElement('select','aps_test_date', array(
			'label'=>'Date',
			'required'=>true,
		    'onChange'=>"getLocation(this);"
		));
		$this->aps_test_date->setRegisterInArrayValidator(false);	
		
		foreach ($schedule as $list){
			$this->aps_test_date->addMultiOption($list['aps_test_date'],$list['aps_test_date']);
		}
		
		
		
		$this->addElement('select','aps_id', array(
			'label'=>'Location'
		));	
		
		$this->aps_id->setRegisterInArrayValidator(false);
		
		$this->aps_id->addMultiOption(null,"-- All --");
		$placement_schedule = $scheduleDB->getLocationByDate($this->_ptest_date);
		foreach ($placement_schedule as $list){
			$this->aps_id->addMultiOption($list['aps_id'],$list['location_name']);
		}
		
		
	
        
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
		
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));

	}
	
}

?>