<?php 
class Application_Form_ProgramItemSetup extends Zend_Form{
	
    public function init(){
        
        $this->setMethod('post');
	$this->setAttrib('id', 'application_item_tagging');
        
        /*$this->addElement('radio','pds_instruction_position', array(
            'label'=>'Instruction Position'
        ));
        
        $this->pds_instruction_position->addMultiOptions(array(
            '1' => 'Top',
            '2' => 'Bottom'
	))->setSeparator('&nbsp;&nbsp;')->setValue("1");
        
        $this->addElement('textarea','pds_instruction', array(
            'id'=>'instruction',
            'label'=>'Instruction'
	));
        
        $this->addElement('textarea','pds_tooltip', array(
            'label'=>'Tooltip'
	));*/
        
        $this->addElement('checkbox', 'pds_view', array(
            'label' => 'View',
            //'name' => 'view',
            'checkedValue' => '1',
            'uncheckedValue' => '0',
            'disableHidden' => true
        ));
        
        $this->addElement('checkbox', 'pds_compulsory', array(
            'label' => 'Compulsory',
            //'name' => 'compulsory',
            'checkedValue' => '1',
            'uncheckedValue' => '0',
            'disableHidden' => true
        ));
        
        $this->addElement('checkbox', 'pds_enable', array(
            'label' => 'Enable',
            //'name' => 'enable',
            'checkedValue' => '1',
            'uncheckedValue' => '0',
            'disableHidden' => true
        ));
        
        /*$this->addElement('checkbox', 'pds_hide', array(
            'label' => 'Hide',
            //'name' => 'hide',
            'checkedValue' => '1',
            'uncheckedValue' => '0',
            'disableHidden' => true
        ));*/
		
	//button
	$this->addElement('submit', 'save_item_tagging', array(
          'label'=>'Save',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addDisplayGroup(array('save_item_tagging'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	));		
    }
}
?>