<?php
class Application_Form_BlockApplicant extends Zend_Form
{


    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('Id', 'form_block_applicant');


        $this->addElement('text', 'IcNo',
            array(
                'label' => 'IC Number',
                'required' => 'true',
                'class' => 'input-txt'
            )
        );

        $this->addElement('text', 'Name',
            array(
                'label' => 'Name',
                'required' => 'true',
                'class' => 'input-txt'
            )
        );

        $this->addElement('text', 'Remark',
            array(
                'label' => 'Remark',
                'class' => 'input-txt'
            )
        );


        //button
        $this->addElement('submit', 'save', array(
            'label' => 'Submit',
            'decorators' => array('ViewHelper')
        ));

        $this->addElement('submit', 'cancel', array(
            'label' => 'Cancel',
            'decorators' => array('ViewHelper'),
            'onClick' => "window.location ='" . $this->getView()->url(array('module' => 'application', 'controller' => 'block-applicant', 'action' => 'index'), 'default', true) . "'; return false;"
        ));

        $this->addDisplayGroup(array('save', 'cancel'), 'buttons', array(
            'decorators' => array(
                'FormElements',
                array('HtmlTag', array('tag' => 'div', 'class' => 'buttons')),
                'DtDdWrapper'
            )
        ));

    }
}
?>