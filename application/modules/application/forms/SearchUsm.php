<?php
class Application_Form_SearchUsm extends Zend_Form
{
    protected $_ptest;
	protected $_ptest_date;
	protected $_quota;
	protected $_limitt;
	
	public function setPtest($value) {
		$this->_ptest = $value;
	}
	
	public function setPtest_date($value) {
		$this->_ptest_date = $value;
	}
	
	public function setQuota($value) {
		$this->_quota = $value;
	}
	
	public function setLimitt($value) {
		$this->_limitt = $value;
	}
	
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
				
  
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		//intake
		$this->addElement('select','intake_id', array(
			'label'=>'Intake',
			'onchange'=>'getPeriod(this)',
		    'required'=>true
		));
		
		$intakeDb = new App_Model_Record_DbTable_Intake();
		$intakeList = $intakeDb->getData();		
    			
		$this->intake_id->addMultiOption(null,"-- Select Intake --");
		foreach ($intakeList as $list){
			$this->intake_id->addMultiOption($list['IdIntake'],$list['IntakeDefaultLanguage']);
		}
		
		
		//period
		/*$this->addElement('select','period_id', array(
			'label'=>'Period',
		    'required'=>true	
		
		));
		
		$periodDB = new App_Model_Record_DbTable_AcademicPeriod();
    	$period_data = $periodDB->getData();	
    			
		$this->period_id->addMultiOption(null,"-- Select Period --");
		foreach ($period_data as $list){
			$this->period_id->addMultiOption($list['ap_id'],$list['ap_desc']);
		}
		
		//load previous period
		/*$this->addElement('checkbox','load_previous_period', array(
			'label'=>'Include Previous Period',
			"checked" => "checked"
		));*/
		
		//period
		$this->addElement('select','ptest', array(
			'label'=>'Placement Test',
		    'required'=>true	,
		    'onChange'=>'getPtest(this)'
		
		));
		
		$placementDB = new App_Model_Application_DbTable_ApplPlacementHead();
    	$placement_data = $placementDB->getDatabyCode(null,0);	
    			
		$this->ptest->addMultiOption(null,"-- Select Placement Test --");
		foreach ($placement_data as $list){
			$this->ptest->addMultiOption($list['aph_placement_code'],$list['aph_placement_code']);
		}
		
		
	    $scheduleDB =  new App_Model_Application_DbTable_ApplicantPlacementSchedule();
		$schedule = $scheduleDB->getPtestDate($this->_ptest,true);
		
        $this->addElement('select','aps_test_date', array(
			'label'=>'Date',
			'required'=>true,
		    'onChange'=>"getLocation(this);"
		));
		$this->aps_test_date->setRegisterInArrayValidator(false);	
		
		foreach ($schedule as $list){
			$this->aps_test_date->addMultiOption($list['aps_test_date'],$list['aps_test_date']);
		}
		
		
		
		$this->addElement('select','aps_id', array(
			'label'=>'Location'
			
		));	
		
		$this->aps_id->setRegisterInArrayValidator(false);
		
		$this->aps_id->addMultiOption(null,"-- All --");
		$placement_schedule = $scheduleDB->getLocationByDate($this->_ptest_date);
		foreach ($placement_schedule as $list){
			$this->aps_id->addMultiOption($list['aps_id'],$list['location_name']);
		}
		
		//faculty
		$this->addElement('select','faculty', array(
			'label'=>'Faculty',
		    'onChange'=>"getProgramme(this,$('#programme'))",
		    'required'=>false
		));
		
		$collegeDB = new App_Model_General_DbTable_Collegemaster();
		$college_data = $collegeDB->getFaculty();		
    			
		$this->faculty->addMultiOption(null,"-- All --");
		foreach ($college_data as $list){
			if($locale=="id_ID"){
				$college_name = $list["ArabicName"];
			}elseif($locale=="en_US"){
				$college_name = $list["CollegeName"];
			}
			$this->faculty->addMultiOption($list['IdCollege'],strtoupper($college_name));
		}
		
		
		
		$this->addElement('select','programme', array(
			'label'=>'Programme',
		    'required'=>false		
		
		));
		
		$programDB = new App_Model_Record_DbTable_Program();
    	$program_data = $programDB->searchProgramByFaculty($this->_facultyid);    	

		
		$this->programme->addMultiOption(null,"-- All --");
		foreach ($program_data as $list){
			
			if($locale=="id_ID"){
				$program_name = $list["ArabicName"];
			}elseif($locale=="en_US"){
				$program_name = $list["ProgramName"];
			}
			
			
			$this->programme->addMultiOption($list['ProgramCode'],$program_name);
		}
		
		
		
		$this->addElement('select','preference', array(
				'label'=>'Program Preference',
				'required'=>true	        
		       ));
		
		$this->preference->setMultiOptions(array('1'=>' '.$this->getView()->translate('Preference 1'), '2'=>' '.$this->getView()->translate('Preference 2')));
		$this->preference->setValue("1");
				
		$this->addElement('text','passmark', array(
				'label'=>$this->getView()->translate("Passing Mark"),
				'required'=>true
		       ));			
		
		       
		$this->addElement('select','aps_usm_attendance', array(
			'label'=>'Placement Test Attendance'		
		));
		$this->aps_usm_attendance->addMultiOption(null,"-- All --");
		$this->aps_usm_attendance->addMultiOption(1,"Attend"); //hadie
		$this->aps_usm_attendance->addMultiOption(2,"Absence"); //x hadir
		
		
		
		
		
		$this->addElement('radio','quota', array(
				'label'=>'Jumlah Pemohon',
				'required'=>true	,
		        'onclick'=>"enabledBox(this);"        
		       ));
		
		$this->quota->setMultiOptions(array('1'=>' '.$this->getView()->translate('All'), '2'=>' '.$this->getView()->translate('Others')));
		$this->quota->setValue($this->_quota);
		
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('text','limit', array(
				'label'=>'',				
        		'disabled'=>'disabled',
				'size'=>'2'        
	    ));
		$this->limit->setValue($this->_limitt);
		$this->limit->addValidator('Digits');
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));

	}
	
}

?>