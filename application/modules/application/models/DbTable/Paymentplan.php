<?php
class Application_Model_DbTable_Paymentplan extends Zend_Db_Table {
	protected $_name = 'tbl_paymentplan'; // table name
	private $lobjDbAdpt;

	public function init(){
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fnaddPaymentplan($larrformData) {
		$this->lobjDbAdpt->insert('tbl_paymentplan',$larrformData);//insert institute
		$larrreglistdata = $larrformData;
		unset($larrformData['name']);
        unset($larrformData['MalayName']);
		unset($larrformData['code']);
		unset($larrformData['Active']);
		unset($larrformData['UpdUser']);
		unset($larrformData['UpdDate']);

        $lid = $this->lobjDbAdpt->lastInsertId();
		$larrreglistdata['id'] =  $lid;

		$insertId = $this->lobjDbAdpt->lastInsertId($this->_name,'id');
		return $insertId;
	}

	public function fnupdatePaymentplan($larrformData,$lintIdInstitution) {
        $larrreglistdata = $larrformData;
        unset($larrformData['name']);
        unset($larrformData['MalayName']);
        unset($larrformData['code']);
        unset($larrformData['Active']);
        unset($larrformData['UpdUser']);
        unset($larrformData['UpdDate']);

		$where = 'id = '.$lintIdInstitution;
		$this->update($larrreglistdata,$where);
	}

	public function fnDeleteInstitution($idInstitution) {  // function to delete Institution details
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = 'tbl_institution';
		$where = $db->quoteInto('idInstitution = ?', $idInstitution);
		$db->delete('tbl_institution', $where);
	}

	public function fngetPaymentPlanDetails() { //Function to get the Institution details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from($this->_name);
        //echo $lstrSelect; exit;
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnSearchPaymentPlan($post = array()) { //Function for searching the Institution details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pp"=>"tbl_paymentplan"),array("pp.*"));
                        
                if (isset($post['field3']) && $post['field3']!=''){        
                    $lstrSelect->where('pp.name like "%" ? "%"',$post['field3']);
                }
                if (isset($post['field4']) && $post['field4']!=''){
                    $lstrSelect->where('pp.code like "%" ? "%"',$post['field4']);
                }

		$lstrSelect->order("pp.name");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetInstitutionList(){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_institution"),array("key"=>"a.idInstitution","value"=>"a.InstitutionName"))
		->order("a.InstitutionName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetInstituteCountry($IdInstitute){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_institution"),array("a.Country","a.State","a.City"))
		->where('a.idInstitution =?',$IdInstitute);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fngetSchoolListByCountry($countryId){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_institution"),array("key"=>"a.idInstitution","value"=>"a.InstitutionName"))
		->where('a.Country =?',$countryId);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function get_details($id) {
		$select = $this->lobjDbAdpt
			->select()
			->from(array('paymentplan'=>'tbl_paymentplan'), array('paymentplan.*'))
			->where('id = ?', $id)
			;

		$institute = $this->lobjDbAdpt->fetchRow($select);
		return($institute);

	}

        public function checkInstitutName($institutName){
            $select = $this->lobjDbAdpt
                ->select()
                ->from(array('Institution'=>'tbl_institution'), array('Institution.*'))
                ->where('Institution.IdInstitution like "%" ? "%"',$institutName);
            $institute = $this->lobjDbAdpt->fetchRow($select);
            return $institute;
        }

}