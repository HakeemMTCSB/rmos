<?php
class Application_Model_DbTable_InstitutionsubjectMapping extends Zend_Db_Table {
	protected $_name = 'tbl_institution_subject_mapping';
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function fnadd($data) {
		$this->insert($data);
	}
	
	public function fngetmappings($IdInstitution) {
		$select = $this->select()
		->setIntegrityCheck(false)
		->join(array('a' => 'tbl_institution'),'',array())
		->join(array('b'=>$this->_name),'a.idInstitution = b.IdInstitution',array())
		->join(array('c'=>"tbl_subject"),'c.IdSubject = b.IdSubject',array('c.SubjectName','c.IdSubject','c.SubjectCode'))
		->where("b.IdInstitution = ?",$IdInstitution);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	public function fndeletemappings($IdInstitution) {
		$where = $this->lobjDbAdpt->quoteInto('IdInstitution = ?', $IdInstitution);
		$this->lobjDbAdpt->delete($this->_name, $where);
	}


}