<?php 
class Application_Model_DbTable_Studenteducation extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_studenteducationdetails';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fnaddStudentEducation($result,$lobjFormData) {	
		
		/*echo "<pre>";
		print_r($lobjFormData);*/
		
		
			$gridcount = count($lobjFormData['InstitutionNamegrid']);
			for($i=0;$i<$gridcount;$i++){
			
			
			if($lobjFormData['subjectIdgrid'][$i]=="")$lobjFormData['subjectIdgrid'][$i]=0;
			if($lobjFormData['SubjectMarkgrid'][$i]=="" ||  $lobjFormData['SubjectMarkgrid'][$i] =="NaN")$lobjFormData['SubjectMarkgrid'][$i]=0;
			
				$data = array('IdApplication' => $result,
							  'InstitutionName' => $lobjFormData['InstitutionNamegrid'][$i],
							  'UniversityName' => $lobjFormData['UniversityNamegrid'][$i],
							  'ProgCheckListName'=>$lobjFormData['pgmchklistIdgrid'][$i],
							  'StudyPlace' => $lobjFormData['StudyPlacegrid'][$i],
							  'MajorFiledOfStudy' => $lobjFormData['MajorFiledOfStudygrid'][$i],
							  'YearOfStudyFrom' => $lobjFormData['YearOfStudyFromgrid'][$i],
							  'YearOfStudyTo' =>  $lobjFormData['YearOfStudyTogrid'][$i],
							  'DegreeType' => $lobjFormData['DegreeTypegrid'][$i],
				 			  'GradeOrCGPA' => $lobjFormData['GradeOrCGPAgrid'][$i],
							  'TypeOfSchool' => $lobjFormData['TypeofSchoolgrid'][$i],
							  'StatusOfSchool' => $lobjFormData['StatusOfSchoolgrid'][$i],
							  'TypeOfStudent' => $lobjFormData['TypeOfStudgrid'][$i],
							  'HomeTownSchool' => $lobjFormData['HomeTownSchoolgrid'][$i],
							  'CreditTransferFrom' => $lobjFormData['CreditTransferFromgrid'][$i],
							  'SubjectId' => $lobjFormData['subjectIdgrid'][$i],
							  'SubjectMark' => $lobjFormData['SubjectMarkgrid'][$i],
				 			  'UpdUser' => $lobjFormData['UpdUser'],
				 			  'UpdDate' => $lobjFormData['UpdDate']);			
				$this->insert($data);
			}		
			//exit;
		
/*		if($LocalStudent == 0 ) {

		}else{
			$gridcount = count($lobjFormData['InstitutionNameSelectgrid']);
			for($i=0;$i<$gridcount;$i++){
				$data = array('IdApplication' => $result,
							  'InstitutionName' => $lobjFormData['InstitutionNameSelectgrid'][$i],
							  'UniversityName' => $lobjFormData['UniversityNamegrid'][$i],
							  'StudyPlace' => $lobjFormData['StudyPlacegrid'][$i],
							  'MajorFiledOfStudy' => $lobjFormData['MajorFiledOfStudygrid'][$i],
							  'YearOfStudyFrom' => $lobjFormData['YearOfStudyFromgrid'][$i],
							  'YearOfStudyTo' =>  $lobjFormData['YearOfStudyTogrid'][$i],
							  'DegreeType' => $lobjFormData['DegreeTypegrid'][$i],
				 			  'GradeOrCGPA' => $lobjFormData['GradeOrCGPAgrid'][$i],
				 			  'UpdUser' => $lobjFormData['UpdUser'],
				 			  'UpdDate' => $lobjFormData['UpdDate']);			
			$this->insert($data);
			}
		}*/

	}	
	
	public function fnupdatestudenteducation($studenteducationdetails,$IdApplication,$LocalStudent) {  // function to insert po details
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_studenteducationdetails";
			
				foreach($studenteducationdetails as $studenteducationresult) {
				$larrcourse = array('IdApplication'=>$IdApplication,
									'InstitutionName'=>$studenteducationresult['InstitutionName'],
									'UniversityName'=>$studenteducationresult['UniversityName'],
									'ProgCheckListName'=>$studenteducationresult['ProgCheckListName'],
									'StudyPlace'=>$studenteducationresult['StudyPlace'],
									'MajorFiledOfStudy'=>$studenteducationresult['MajorFiledOfStudy'],
									'YearOfStudyFrom'=>$studenteducationresult['YearOfStudyFrom'],
									'YearOfStudyTo'=>$studenteducationresult['YearOfStudyTo'],
									'DegreeType'=>$studenteducationresult['DegreeType'],
									'GradeOrCGPA'=>$studenteducationresult['GradeOrCGPA'],
				
									'UpdUser'=>$studenteducationresult['UpdUser'],
									'UpdDate'=>$studenteducationresult['UpdDate']);
				$db->insert($table,$larrcourse);
				}		
			
		
			}

   public function fnGetStudenteducationdetails($IdApplication) { //Function for the view University 
	$select = $this->select()
			->setIntegrityCheck(false)  
			->join(array('a' => 'tbl_studenteducationdetails'),array('a.IdApplication'))
			->join(array('b'=>'tbl_definationms'),'a.DegreeType = b.idDefinition',array('b.DefinitionDesc', 'b.idDefinition'))
			->join(array('c'=>'tbl_definationms'),'a.GradeOrCGPA= c.idDefinition','c.DefinitionDesc AS DefinitionDesc1')
            ->where('a.IdApplication = ?',$IdApplication);	
	$result = $this->fetchAll($select);
	return $result->toArray();
    }
    
    public function fnDeleteTempstudenteducationDetailsBysession($sessionID) { //Function for Delete Purchase order terms
		$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_tempstudenteducationdetails";
	    	$where = $db->quoteInto('sessionId = ?', $sessionID);
			$db->delete($table, $where);
	}
	   public function fndeletetempstudenteducation($sessionID) { //Function for Delete Purchase order terms
		$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_tempstudenteducationdetails";
			$db->delete($table);
	}
	
	
	public function fnUpdateTempeducationdetails($IdTemp) {  // function to update po details
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_tempstudenteducationdetails";
			$larrresult = array('deleteFlag'=>'0');
			$where = "idTemp = '".$IdTemp."'";
			$db->update($table,$larrresult,$where);	
		}
	    
     public function fnviewEducationdetaillist($IdApplication) { //Function to get the Program Branch details
 		$select = $this->select()
                ->setIntegrityCheck(false)  
                ->join(array('a' => 'tbl_studenteducationdetails'),array('IdApplication'))
                ->join(array('c'=>'tbl_definationms'),'a.DegreeType = c.idDefinition')
                ->where('a.IdApplication = ?',$IdApplication);
       $result = $this->fetchAll($select);
       return $result->toArray();
     }
     
     public function fnstudenteducationtempdetails($IdApplication) { //Function to get the Program Branch details
 		$select = $this->select()
                ->setIntegrityCheck(false)  
                ->from(array('a' => 'tbl_tempstudenteducationdetails'),array('a.*'))
                ->where('a.unicode = ?',$IdApplication)
                ->where('a.deleteFlag = 1');                
       $result = $this->fetchAll($select);
       return $result->toArray();
     }
     
	public function fnUpdateTempEditEducationdetails($InstitutionName,$UniversityName,$MajorFiledOfStudy,$StudyPlace,$YearOfStudyFrom,$YearOfStudyTo,$DegreeType,$GradeOrCGPA,$IdStudEduDtl,$upddate,$upduser) {
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_tempstudenteducationdetails";
			$larridpo = array('InstitutionName'=>$InstitutionName,
								'UniversityName'=>$UniversityName,
								'MajorFiledOfStudy'=>$MajorFiledOfStudy,
								'StudyPlace'=>$StudyPlace,
								'YearOfStudyFrom'=>$YearOfStudyFrom,
								'YearOfStudyTo'=>$YearOfStudyTo,
								'DegreeType'=>$DegreeType,
								'GradeOrCGPA'=>$GradeOrCGPA,	
								'UpdUser'=>$upduser,
								'UpdDate'=>$upddate);
			$where = "idTemp = '$IdStudEduDtl'";
			$db->update($table,$larridpo,$where);	
		}

	public function fnInsertNewTempEducationDetails($InstitutionName,$UniversityName,$StudyPlace,$MajorFiledOfStudy,$YearOfStudyFrom,$YearOfStudyTo,$DegreeType,$GradeOrCGPA,$upddate,$upduser,$IdApplication) { 
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_tempstudenteducationdetails";
			$sessionID = Zend_Session::getId();
			$larrcourse = array('InstitutionName'=>$InstitutionName,
								'UniversityName'=>$UniversityName,
								'StudyPlace'=>$StudyPlace,
								'MajorFiledOfStudy'=>$MajorFiledOfStudy,
								'YearOfStudyFrom'=>$YearOfStudyFrom,
								'YearOfStudyTo'=>$YearOfStudyTo,
								'DegreeType'=>$DegreeType,
								'GradeOrCGPA'=>$GradeOrCGPA,
								'UpdUser'=>$upduser,
								'UpdDate'=>$upddate,
								'unicode'=>$IdApplication,
								'Date'=>date("Y-m-d"),
								'sessionId'=>$sessionID,
								'idExists'=>'0',
								'deleteFlag'=>'1');
				$db->insert($table,$larrcourse);	
		}
		
	public function fnDeletestudenteducationDetails($IdApplication) { //Function for Delete Purchase order terms
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_studenteducationdetails";
	    $where = $db->quoteInto('IdApplication = ?', $IdApplication);
		$db->delete($table, $where);
	}


}
?>