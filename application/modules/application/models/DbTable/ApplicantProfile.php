<?php 

class Application_Model_DbTable_ApplicantProfile extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_profile';
	protected $_primary = "appl_id";
	protected $_db;

    protected $_dependentTables = array(
        'App_Model_Application_DbTable_ApplicantEmployment',
        'App_Model_Application_DbTable_ApplicantFamily',
        'App_Model_Application_DbTable_ApplicantQualification',
    );

    protected $_referenceMap    = array(
        'Transaction' => array(
            'columns'           => 'appl_id',
            'refTableClass'     => 'App_Model_Application_DbTable_ApplicantTransaction',
            'refColumns'        => 'at_appl_id'
        ),
    );

	
	function App_Model_Application_DbTable_ApplicantProfile() {
		$this->_db = Zend_Registry::get('dbapp');
	}
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function uniqueEmail($email){
	
		$select = $this->_db->select()
					  ->from($this->_name)
					  ->where("appl_email = ?", $email);

		$row = $this->_db->fetchRow($select);	
		 
		if($row){
		 	return false;
		}else{
			return true;	
		}
	}
	
	public function getProfileProgram($transaction_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
	      $select = $db ->select()
					->from(array('ap'=>'applicant_program'))	
					->join(array('at'=>'applicant_transaction'),'at.at_trans_id = ap.ap_at_trans_id')
				    ->join(array('apr'=>'applicant_profile'),'apr.appl_id = at.at_appl_id')				
					->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=ap.ap_prog_id',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
                    ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ap.mode_study', array('StudyMode'=>'ds.DefinitionDesc'))
                      ->joinLeft(array("dss" => "tbl_definationms"),'dss.idDefinition=ap.program_mode', array('ProgramMode'=>'dss.DefinitionDesc'))
                    ->joinLeft(array("salute" => "tbl_definationms"), 'apr.appl_salutation=salute.idDefinition', array('Salutation'=>'DefinitionDesc'))
                      ->joinLeft(array("intake" => "tbl_intake"), 'intake.IdIntake=at.at_intake', array('Semester'=>'IntakeDesc'))
					->where("at.at_trans_id  = '".$transaction_id."'")				
					->order("ap.ap_preference Asc");
					
//        $stmt = $db->query($select);
//        $row = $stmt->fetchAll();
        
        $row = $db->fetchRow($select);
        
        if($row){
        	return $row;	
        }else{
        	return null;
        }
	}
	
	

	public function verifyData($username,$password){
	
		$select = $this->_db->select()
					  ->from($this->_name)
					  ->where("appl_email = ?", $username)
					  ->where("appl_password = ?", $password);
					  
		 $row = $this->_db->fetchRow($select);	
		 return $row;
	}
	
	public function getForgotPasswordData($email,$dob){
	
		$select = $this->_db->select()
					  ->from($this->_name)
					  ->where("appl_email = ?", $email)
					  ->where("appl_dob = ?", $dob);
					  
		 $row = $this->_db->fetchRow($select);	
		 
		 if($row){
		 	return $row;
		 }else{
		 	return null;	
		 }
		 
	}
	
	public function getData($id=""){
	
		$select = $this->_db->select()
					  ->from($this->_name);
					  
		if($id)	{			
			 $select->where("appl_id ='".$id."'");
			 $row = $this->_db->fetchRow($select);	
			 
		}	else{			
			$row = $this->_db->fetchAll($select);	
		}	  
		
		 return $row;
	}
	
	
	public function getProfile ($id=""){
	
		$select = $this->_db->select()
					  ->from(array('ap'=>$this->_name))
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('admission_type'=>'at_appl_type'))
					  ->joinleft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id',(array('education'=>'ae.ae_discipline_code')))
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id',(array('fee'=>'apt.apt_fee_amt','bill_no'=>'apt.apt_bill_no','currency'=>'apt.apt_currency','schedule_id'=>'apt.apt_aps_id')))
					  ->where("at.at_status = 'APPLY'")
					  ->where("ap.appl_id ='".$id."'");
		$row = $this->_db->fetchRow($select);	
		return $row;
	}
	
	public function search($name="", $id="", $id_type=0, $program_id=0){
		
		$select = $this->_db->select()
						->from(array('a'=>$this->_name))
						->where("a.ARD_PROGRAM != 0 and a.ARD_OFFERED = 1")
						->join(array('p'=>'r006_program'),'p.id = a.ARD_PROGRAM',array('program_code'=>'code'))
						->join(array('mp'=>'r005_program_main'),'mp.id = p.program_main_id',array('main_name'=>'name'));
						
		if($name!=""){
			$select->where("ARD_NAME like '%".$name."%'");	
		}						
		
		if($id!=""){
			$select->where("ARD_IC like '%".$id."%'");	
		}
		
		if($id_type!=0){
			$select->where("ARD_TYPE_IC = ".$id_type);	
		}
		
		if($program_id!=0){
			$select->where("ARD_PROGRAM = ".$program_id);	
		}
		
		$stmt = $this->_db->query($select);
	    $row = $stmt->fetchAll();
	    
	    return $row;
	}
	
	public function getPaginateData(){
		
		 $select = $this->_db->select()
					  ->from(array('ap'=>$this->_name))
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id')
					  ->joinleft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id',(array('education'=>'ae.ae_discipline_code')))
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id',(array('fee'=>'apt.apt_fee_amt','bill_no'=>'apt.apt_bill_no','currency'=>'apt.apt_currency','schedule_id'=>'apt.apt_aps_id')));
						
		return $select;
	}
	
	public function verify($transaction_id,$billing_no,$pin_no){
		
			 $select = $this->_db->select()
					  ->from(array('ap'=>$this->_name))	
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id')				 
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id')	
					  ->joinLeft(array('apb'=>'appl_pin_to_bank'),'apb.billing_no=apt.apt_bill_no')				  
					  ->where("at.at_trans_id ='".$transaction_id."'")
					  ->where("apt.apt_bill_no = '".$billing_no."'")
				      ->where("apb.REGISTER_NO = '".$pin_no."'");	//	entry yg belum pakai								
       
        $row = $this->_db->fetchRow($select);
		return $row;
	}
	
		public function viewkartu($transaction_id){
		
			 $select = $this->_db->select()
					  ->from(array('ap'=>$this->_name))	
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id')				 
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id')	
					  ->joinLeft(array('apb'=>'appl_pin_to_bank'),'apb.billing_no=apt.apt_bill_no')				  
					  ->where("at.at_trans_id ='".$transaction_id."'");	//	entry yg belum pakai								
       
        $row = $this->_db->fetchRow($select);
		return $row;
	}

public function getAllProfile ($id=""){
	
		 $select = $this->_db->select()
					  ->from(array('ap'=>$this->_name))
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('admission_type'=>'at_appl_type'))
					  ->joinleft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id',(array('education'=>'ae.ae_discipline_code')))
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id',(array('fee'=>'apt.apt_fee_amt','bill_no'=>'apt.apt_bill_no','currency'=>'apt.apt_currency','schedule_id'=>'apt.apt_aps_id')))	
					  ->joinleft(array('p'=>'tbl_city'),'p.idCity=ap.appl_province',array('CityName'=>'p.CityName'))
					  ->joinleft(array('s'=>'tbl_state'),'s.idState=ap.appl_state',array('StateName'=>'s.StateName'))					  			
					  ->where("at.at_trans_id ='".$id."'");
		$row = $this->_db->fetchRow($select);	
		return $row;
	}
	
	public function getPaginateDatabyProgram($condition=null){
		
		 $select = $this->_db->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date','status'=>'at.at_status'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'school_master'),'sm.sm_id=ae.ae_institution',array('school'=>'sm.sm_name'));
					   
					   if($condition!=null){
					   		if($condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					  		if($condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}
					   		if(isset($condition["status"]) && $condition["status"]!=''){
								$select->where("at.at_status  = '".$condition["status"]."'");	
							}
					   }
					   
		//echo $select;			  
		return $select;
	}
	
	public function getDatabyProgram($condition=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date','status'=>'at.at_status'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'tbl_schoolmaster'),'sm.idSchool=ae.ae_institution',array('school'=>'sm.SchoolName'));
					   
					   if($condition!=null){
					   		if(isset($condition["program_code"]) && $condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					   		if(isset($condition["admission_type"]) && $condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}
					  		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
					   			$select->where("at.at_trans_id ='".$condition["transaction_id"]."'");
					  		}
					  		
					   		if(isset($condition["academic_year"]) && $condition["academic_year"]!=''){
					   			$select->where("at.at_academic_year ='".$condition["academic_year"]."'");
					  		}
					  		
					  		if(isset($condition["status"]) && $condition["status"]!=''){
								$select->where("at.at_status  = '".$condition["status"]."'");
					  		}
					   		if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$period = explode('/',$condition["period"]);
					   							   		
								$select->where("MONTH(at.at_submit_date) = '".$period[0]."'");
								$select->where("YEAR(at.at_submit_date) = '".$period[1]."'");
					  		}
					   }
					   
		
		// echo $select;
		 
		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
			$row = $db->fetchRow($select);
		}else{		   
			$row = $db->fetchAll($select);		
		}		  
		return $row;
	}
	
	
	public function getDeanSelection($condition=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date','status'=>'at.at_status'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'tbl_schoolmaster'),'sm.idSchool=ae.ae_institution',array('school'=>'sm.SchoolName'))
					   ->where("at.at_selection_status = 0");
					  
					  
					   
					   if($condition!=null){
					   	
					   		if(isset($condition["faculty_id"]) && $condition["faculty_id"]!=''){
					   			$select->where("p.IdCollege ='".$condition["faculty_id"]."'");
					   		}
					   		if(isset($condition["program_code"]) && $condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					   		if(isset($condition["admission_type"]) && $condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}
					  		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
					   			$select->where("at.at_trans_id ='".$condition["transaction_id"]."'");
					  		}
					  		
					   		if(isset($condition["academic_year"]) && $condition["academic_year"]!=''){
					   			$select->where("at.at_academic_year ='".$condition["academic_year"]."'");
					  		}
					  		
					  		if(isset($condition["status"]) && $condition["status"]!=''){
								$select->where("at.at_status  = '".$condition["status"]."'");
					  		}					   	
					   		if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$select->where("at_period = '".$condition["period"]."'");
					  		}
					  		
					  	   
					   }
					   
		
		
		// echo $select;
		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
			$row = $db->fetchRow($select);
		}else{		   
			$row = $db->fetchAll($select);		
		}		  
		return $row;
	}
	
	
	public function getRectorSelection($condition=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'tbl_schoolmaster'),'sm.idSchool=ae.ae_institution',array('school'=>'sm.SchoolName'))
					   ->where("at.at_selection_status = 1");
					   
					   if($condition!=null){
					  		if(isset($condition["faculty_id"]) && $condition["faculty_id"]!=''){
					   			$select->where("p.IdCollege ='".$condition["faculty_id"]."'");
					   		}
					   		if(isset($condition["program_code"]) && $condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					   		if(isset($condition["admission_type"]) && $condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}
					  		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
					   			$select->where("at.at_trans_id ='".$condition["transaction_id"]."'");
					  		}					  		
					   		if(isset($condition["academic_year"]) && $condition["academic_year"]!=''){
					   			$select->where("at.at_academic_year ='".$condition["academic_year"]."'");
					  		}
					  		
					  		if(isset($condition["status"]) && $condition["status"]!=''){
								$select->where("at.at_status  = '".$condition["status"]."'");
					  		}					   	
					  	 	if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$select->where("at_period = '".$condition["period"]."'");
					  		}
					  		
					   }
					   
					   
		//echo $select; 
		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
			$row = $db->fetchRow($select);
		}else{		   
			$row = $db->fetchAll($select);		
		}		  
		return $row;
	}
	
	
	public function getApprovalSelection($condition=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'tbl_schoolmaster'),'sm.idSchool=ae.ae_institution',array('school'=>'sm.SchoolName'))
					   ->where("at.at_selection_status = 2");
					   
					   if($condition!=null){
					   		if(isset($condition["faculty_id"]) && $condition["faculty_id"]!=''){
					   			$select->where("p.IdCollege ='".$condition["faculty_id"]."'");
					   		}
					   		if(isset($condition["program_code"]) && $condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					   		if(isset($condition["admission_type"]) && $condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}
					  		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
					   			$select->where("at.at_trans_id ='".$condition["transaction_id"]."'");
					  		}
					  		
					   		if(isset($condition["academic_year"]) && $condition["academic_year"]!=''){
					   			$select->where("at.at_academic_year ='".$condition["academic_year"]."'");
					  		}
					  		
					  		if(isset($condition["status"]) && $condition["status"]!=''){
								$select->where("at.at_status  = '".$condition["status"]."'");
					  		}
					   		
					       if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$select->where("at_period = '".$condition["period"]."'");
					  		}
					  		
					  	   
					   }
					   
		
		
	  //echo $select;
		if(isset($condition["transaction_id"]) && $condition["transaction_id"]!=''){
			$row = $db->fetchRow($select);
		}else{		   
			$row = $db->fetchAll($select);		
		}		  
		return $row;
	}
	
	
	public function getResultSelection($condition=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'tbl_schoolmaster'),'sm.idSchool=ae.ae_institution',array('school'=>'sm.SchoolName'))
					   ->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege=p.IdCollege',array('faculty'=>'c.ArabicName'))
					   ->where("at.at_selection_status = 3");
					   
					   if($condition!=null){
					   		if(isset($condition["program_code"]) && $condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					   		if(isset($condition["admission_type"]) && $condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}		
					   		if(isset($condition["academic_year"]) && $condition["academic_year"]!=''){
					   			$select->where("at.at_academic_year ='".$condition["academic_year"]."'");
					  		}					  		
					  		if(isset($condition["status"]) && $condition["status"]!=''){
								$select->where("at.at_status  = '".$condition["status"]."'");
					  		}
					   		/*if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$period = explode('/',$condition["period"]);
					   							   		
								$select->where("MONTH(at.at_submit_date) = '".$period[0]."'");
								$select->where("YEAR(at.at_submit_date) = '".$period[1]."'");
					  		}*/
					  		if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$select->where("at_period = '".$condition["period"]."'");
					  		}
					  		
					  	   
					   }
			   
		$row = $db->fetchAll($select);	  
		return $row;
	}
	
	public function getAgentPaginateData($condition=null){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					  ->from(array('ap'=>$this->_name))
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id')
					  ->joinleft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id',(array('education'=>'ae.ae_discipline_code')))
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id',(array('fee'=>'apt.apt_fee_amt','bill_no'=>'apt.apt_bill_no','currency'=>'apt.apt_currency','schedule_id'=>'apt.apt_aps_id')));
					  
					  
	  					if($condition!=null){
					   		if(isset($condition["agent_id"]) && $condition["agent_id"]!=''){
					   			$select->where("at.agent_id ='".$condition["agent_id"]."'");
					   		}
					   		
					  	   
					   }			 
						
		return $select;
	}
	
	public function getAgentData($condition=null){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					  ->from(array('ap'=>$this->_name))
					  ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id')
					  ->joinleft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id',(array('education'=>'ae.ae_discipline_code')))
					  ->joinleft(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id',(array('fee'=>'apt.apt_fee_amt','bill_no'=>'apt.apt_bill_no','currency'=>'apt.apt_currency','schedule_id'=>'apt.apt_aps_id')));
					  
					  
	  					if($condition!=null){
					   		if(isset($condition["agent_id"]) && $condition["agent_id"]!=''){
					   			$select->where("at.agent_id ='".$condition["agent_id"]."'");
					   		}
					   		
					  	   
					   }			 
						
		return $select;
	}
	
	
	public function getStatusSelection($condition=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $db ->select()
					   ->from(array('ap'=>$this->_name))
					   ->joinleft(array('at'=>'applicant_transaction'),'at.at_appl_id=ap.appl_id',array('transaction_id'=>'at.at_trans_id','applicantID'=>'at.at_pes_id','submit_date'=>'at.at_submit_date','selection_status'=>'at.at_selection_status','status'=>'at.at_status'))
					   ->joinleft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id=at.at_trans_id')
					   ->joinLeft(array('p'=>'tbl_program'),'p.ProgramCode=apr.ap_prog_code',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
					   ->joinLeft(array('ae'=>'applicant_education'),'ae.ae_appl_id=ap.appl_id')
					   ->joinLeft(array('sm'=>'tbl_schoolmaster'),'sm.idSchool=ae.ae_institution',array('school'=>'sm.SchoolName'))
					   ->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege=p.IdCollege',array('faculty'=>'c.ArabicName'))
					   ->order("at.at_pes_id DESC");
					  
					   
					   if($condition!=null){
					   		if(isset($condition["faculty"]) && $condition["faculty"]!=''){
					   			$select->where("p.IdCollege ='".$condition["faculty"]."'");
					   		}
					   		if(isset($condition["program_code"]) && $condition["program_code"]!=''){
					   			$select->where("apr.ap_prog_code ='".$condition["program_code"]."'");
					   		}
					   		if(isset($condition["admission_type"]) && $condition["admission_type"]!=''){
					   			$select->where("at.at_appl_type ='".$condition["admission_type"]."'");
					   		}		
					   		if(isset($condition["academic_year"]) && $condition["academic_year"]!=''){
					   			$select->where("at.at_academic_year ='".$condition["academic_year"]."'");
					  		}					  		
					  		
					  		if(isset($condition["period"]) && $condition["period"]!=''){	
					   			$select->where("at_period = '".$condition["period"]."'");
					  		}					  		
					  		if(isset($condition["selection_status"]) && $condition["selection_status"]!=''){
								$select->where("at.at_selection_status  = '".$condition["selection_status"]."'");
					  		}
					   	
					  		
					  	   
					   }
		//echo $select;	   
		$row = $db->fetchAll($select);	  
		return $row;
	}
	
	
	public function getApplicantProfile($transaction_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
	      $select = $db ->select()						
					->from(array('at'=>'applicant_transaction'))
				    ->join(array('apr'=>'applicant_profile'),'apr.appl_id = at.at_appl_id')		
				    ->join(array('ap'=>'applicant_program'),'at.at_trans_id = ap.ap_at_trans_id')		
					->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=ap.ap_prog_id',array('program_id'=>'p.IdProgram','program_name'=>'p.ProgramName','program_name_indonesia'=>'p.ArabicName','program_code'=>'p.ProgramCode'))
                    ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ap.mode_study', array('StudyMode'=>'ds.DefinitionDesc'))
                    ->joinLeft(array("dss" => "tbl_definationms"),'dss.idDefinition=ap.program_mode', array('ProgramMode'=>'dss.DefinitionDesc'))
                    ->joinLeft(array("df" => "tbl_definationms"),'df.idDefinition=ap.program_type', array('ProgramType'=>'df.DefinitionDesc'))
                    ->joinLeft(array("salute" => "tbl_definationms"), 'apr.appl_salutation=salute.idDefinition', array('Salutation'=>'DefinitionDesc'))
                    ->joinLeft(array("intake" => "tbl_intake"), 'intake.IdIntake=at.at_intake', array('Semester'=>'IntakeDesc'))
                    ->joinLeft(array('city'=>'tbl_city'),'city.idCity=apr.appl_city',array('CityName'))
		            ->joinLeft(array('state'=>'tbl_state'),'state.IdState=apr.appl_state',array('StateName'))
		            ->joinLeft(array('ctrs'=>'tbl_countries'),'ctrs.idCountry=apr.appl_country',array('CountryName'))
					->where("at.at_trans_id  = '".$transaction_id."'")	;
					
        $row = $db->fetchRow($select);
        
        if($row){
        	return $row;	
        }else{
        	return null;
        }
	}
	
	public function getProgramSchemeInfo($idProgScheme){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
                ->where('a.IdProgramScheme = ?', $idProgScheme);

            $result = $db->fetchRow($select);
            return $result;
        }
}
?>