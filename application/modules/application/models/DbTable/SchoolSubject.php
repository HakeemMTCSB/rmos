<?php

class Application_Model_DbTable_SchoolSubject extends Zend_Db_Table
{
    protected $_name = 'school_subject'; // table name
    private $db;

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
    }

    public function getList()
    {
        $lstrSelect = $this->db->select()
            ->from(array("a" => $this->_name))
            ->order("a.ss_subject");

        $result = $this->db->fetchAll($lstrSelect);
        return $result;
    }


}