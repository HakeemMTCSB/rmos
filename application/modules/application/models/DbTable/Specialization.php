<?php

class Application_Model_DbTable_Specialization extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_studyfield_type';
    protected $_primary = "id";
    private $lobjDbAdpt;

    /**
     *
     * @see Zend_Db_Table_Abstract::init()
     */
    public function init()
    {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function getData($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('ss'=>$this->_name))
            ->where("ss.id = ?", (int)$id);

        $row = $db->fetchRow($selectData);
        return $row;
    }

    public function getPaginateData($search=null){
        $db = Zend_Db_Table::getDefaultAdapter();

        if($search){
            $selectData = $db->select()
                ->from(array('ss'=>$this->_name))
                ->where("ss.sft_eng_desc LIKE '%".$search['sft_eng_desc']."%'")
                ->where("ss.sft_mal_desc LIKE '%".$search['sft_mal_desc']."%'")
                ->order('ss.sft_eng_desc ASC');

            /*if($search['ss_core_subject']=='1'){
                $selectData->where("ss.ss_core_subject = 1");
            }*/

        }else{
            $selectData = $db->select()
                ->from(array('ss'=>$this->_name));
        }

        return $selectData;
    }


    public function addData($postData){
        $auth = Zend_Auth::getInstance();

        $data = array(
            'sft_id' => $postData['sft_id'],
            'sft_eng_desc' => $postData['sft_eng_desc'],
            'sft_mal_desc' => $postData['sft_mal_desc'],
            'Active' => $postData['Active'],
            'sft_create_user' => $postData['UpdUser'],
            'sft_create_date' => $postData['UpdDate']
        );

        return $this->insert($data);
    }


    public function updateData($postData,$id){

        $data = array(
            'sft_id' => $postData['sft_id'],
            'sft_eng_desc' => $postData['sft_eng_desc'],
            'sft_mal_desc' => $postData['sft_mal_desc'],
            'Active' => $postData['Active'],
            'sft_modify_user' => $postData['UpdUser'],
            'sft_modify_date' => $postData['UpdDate']
        );

        $this->update($data, "id = ".(int)$id);
    }

    public function deleteData($id){
        if($id!=0){
            $this->delete("id = ".(int)$id);
        }
    }



public function fngetSpecializationList()
    { //Function to get the Soecialization details
        $lstrSelect = $this->lobjDbAdpt->select()->from($this->_name);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fngetSpecialization()
    {
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a" => "tbl_specialization"), array("key" => "a.IdSpecialization", "value" => "a.Description"))
            ->order("a.Specialization");
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }


    public function getStudyFieldOptgroup()
    {
        $select = $this->lobjDbAdpt->select()
            ->from(array("a" => "tbl_studyfield_type"))
            ->order('sft_id');
        $resultFieldType = $this->lobjDbAdpt->fetchAll($select);

        $result = array('Please select');
        foreach ($resultFieldType as $index => $type) {

            $select2 = $this->lobjDbAdpt->select()
                ->from(array("a" => "tbl_studyfield"))
                ->where('sf_type = ?', $type['id'])
            ->order('sf_id');
            $resultField = $this->lobjDbAdpt->fetchAll($select2);

            foreach ($resultField as $field) {
                $result[$type['sft_id'] . ' - ' . $type['sft_eng_desc']][$field['id']] = $field['sf_id'].' - '.$field['sf_eng_desc'];
            }
        }

        return $result;
    }

    public function fnSearchSpecialization($post = array())
    { //Function for searching the Soecialization details

        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("is" => "tbl_specialization"), array("is.*"))
            ->where('is.Specialization like "%" ? "%"', $post['field3'])
            ->where('is.Description like "%" ? "%"', $post['field2'])
            ->order("is.Specialization");
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnaddSpecialization($data)
    {
        $this->insert($data);
    }

    public function fnupdateSpecialization($data, $IdSpecialization)
    {
        $where = 'IdSpecialization = ' . $IdSpecialization;
        $this->update($data, $where);
    }

    public function fndeleteSpecialiation($IdSpecialization)
    {
        $where = $this->lobjDbAdpt->quoteInto('IdSpecialization= ?', $IdSpecialization);
        $this->lobjDbAdpt->delete($this->_name, $where);
    }


}