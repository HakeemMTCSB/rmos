<?php
class Application_Model_DbTable_Autocredittransfer extends Zend_Db_Table {
	protected $_name = 'tbl_autocredittransfer';

	/**
	 *
	 * @see Zend_Db_Table_Abstract::init()
	 */
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fngetautocreditByid($IdAutoCredit) {

	}

	public function fnaddautocredit($formData) {
		unset($formData['MinGradPoints']);
		unset($formData['Remarks']);
		unset($formData['Subject']);
		unset($formData['Course']);
		unset($formData['IdSubject']);
		unset($formData['IdCourse']);
		unset($formData['Save']);
		unset($formData['Id']);
		$this->insert($formData);
		$recordid = $this->lobjDbAdpt->lastInsertId();
		return $recordid;
	}

	public function fnupdateautocredit($IdAutoCredit,$formData) {
		unset($formData['MinGradPoints']);
		unset($formData['Remarks']);
		unset($formData['Subject']);
		unset($formData['Course']);
		unset($formData['IdSubject']);
		unset($formData['IdCourse']);
		unset($formData['Save']);
		unset($formData['deletedrows']);
		unset($formData['Id']);
		$where = 'IdAutoCredit = '.$IdAutoCredit;
		$this->update($formData,$where);
	}

	public function fnSearchAutoCredit($post = array()) { //Function for searching the university details
		$select = $this->select()
		->setIntegrityCheck(false)
		->join(array('a' => 'tbl_autocredittransfer'),array('IdAutoCredit'))
		->join(array('b' => 'tbl_program'),'a.IdProgram = b.IdProgram',array('b.ProgramName as ProgramName'))
		->join(array('c' => 'tbl_institution'),'a.IdInstitution = c.idinstitution',array('c.InstitutionName as InstitutionName'))
		->where('a.IdProgram = "%" ? "%"',$post['field5'])
		->where('a.IdInstitution = "%" ? "%"',$post['field8']);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	public function fngetallautocredit() {
		$select = $this->select()
		->setIntegrityCheck(false)
		->join(array('a' => 'tbl_autocredittransfer'),array('IdAutoCredit'))
		->join(array('c' => 'tbl_institution'),'a.IdInstitution = c.idinstitution',array('c.InstitutionName as InstitutionName'))
		->join(array('b' => 'tbl_program'),'a.IdProgram = b.IdProgram',array('b.ProgramName as ProgramName'));
		$result = $this->fetchAll($select);
		return $result->toArray();
	}

        public function fngetInstitution(){
            $sql = $this->lobjDbAdpt->select()
               ->from(array('a' => 'tbl_autocredittransfer'),array('a.IdInstitution',))
               ->joinLeft(array('b' => 'tbl_institution'),"a.IdInstitution = b.idInstitution", array('key' => 'b.idInstitution', 'value' => 'b.InstitutionName'));
            return $result = $this->lobjDbAdpt->fetchAll($sql);
        }

        public function getQualification($idInstitution){
            $sql = $this->lobjDbAdpt->select()
               ->from(array('a' => 'tbl_autocredittransfer'),array())
               ->joinLeft(array('b' => 'tbl_qualificationmaster'),"a.IdQualification = b.IdQualification", array('key' => 'b.IdQualification', 'name' => 'b.QualificationLevel'))
               ->where('a.IdInstitution =?',$idInstitution);
            return $result = $this->lobjDbAdpt->fetchAll($sql);

        }

        public function getSpecialization($IdInstitute,$IdQualification){
            $sql = $this->lobjDbAdpt->select()
               ->from(array('a' => 'tbl_autocredittransfer'),array())
               ->joinLeft(array('b' => 'tbl_specialization'),"a.IdSpecialization = b.IdSpecialization", array('key' => 'b.IdSpecialization', 'name' => 'b.Specialization'))
               ->where('a.IdInstitution =?',$IdInstitute)
               ->where('a.IdQualification =?',$IdQualification);
            return $result = $this->lobjDbAdpt->fetchAll($sql);
        }

        public function getequivalentCourse($IdInstitute,$IdQualification,$IdSpecialization){
            $sql = $this->lobjDbAdpt->select()
               ->from(array('a' => 'tbl_autocredittransfer'),array())
               ->joinLeft(array('b' => 'tbl_autocredittransferdetails'),"a.IdAutoCredit = b.IdAutoCredit")
               ->joinLeft(array('c' => 'tbl_subjectmaster'),"b.IdSubject = c.IdSubject", array('key' => 'c.IdSubject','name' => 'c.SubjectName'))
               ->where('a.IdInstitution =?',$IdInstitute)
               ->where('a.IdQualification =?',$IdQualification)
               ->where('a.IdSpecialization =?',$IdSpecialization);
            return $result = $this->lobjDbAdpt->fetchAll($sql);
        }

}