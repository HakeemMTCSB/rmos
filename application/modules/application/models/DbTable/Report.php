<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_Report extends Zend_Db_Table_Abstract {

	
    
    public function getApplicantList($formData=null){
        $db = Zend_Db_Table::getDefaultAdapter();
        
    	
        
        $select = $db->select()
		            ->from(array('a'=>'applicant_transaction'), array('value'=>'*'))
		            ->join(array('b'=>'applicant_profile'), 'a.at_appl_id = b.appl_id')
		            ->join(array('c'=>'applicant_program'), 'a.at_trans_id = c.ap_at_trans_id')
		            ->joinLeft(array('d'=>'tbl_program'), 'c.ap_prog_id = d.IdProgram')
		            ->joinLeft(array('e'=>'tbl_program_scheme'), 'c.ap_prog_scheme = e.IdProgramScheme')
		            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
		            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
		            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
		            ->joinLeft(array('i'=>'tbl_definationms'), 'b.appl_category = i.idDefinition', array('StudentCategory'=>'i.DefinitionDesc'))		           
		            ->joinLeft(array('j'=>'tbl_intake'), 'a.at_intake=j.IdIntake',array('IntakeDesc'))
		            ->joinLeft(array('k'=>'tbl_definationms'), 'a.at_status = k.idDefinition', array('applicant_status'=>'k.DefinitionDesc'))
		            ->joinLeft(array('l'=>'tbl_definationms'), 'b.appl_idnumber_type = l.idDefinition', array('idtype'=>'l.DefinitionDesc'))
		            ->joinLeft(array('m'=>'tbl_countries'), 'b.appl_nationality = m.idCountry', array('nationality'=>'m.CountryName'))
		            ->joinLeft(array('n'=>'tbl_definationms'), 'b.appl_religion = n.idDefinition', array('religion'=>'n.DefinitionDesc'))
		            ->joinLeft(array('o'=>'tbl_definationms'), 'b.appl_race = o.idDefinition', array('race'=>'o.DefinitionDesc'))
		            ->joinLeft(array('p'=>'tbl_definationms'), 'b.appl_marital_status = p.idDefinition', array('marital_status'=>'p.DefinitionDesc'))
		            ->joinLeft(array('city'=>'tbl_city'),'city.idCity=b.appl_city',array('CityName'))
		            ->joinLeft(array('state'=>'tbl_state'),'state.IdState=b.appl_state',array('StateName'))
		            ->joinLeft(array('ctrs'=>'tbl_countries'),'ctrs.idCountry=b.appl_country',array('CountryName'))
		            ->joinLeft(array('ccity'=>'tbl_city'),'ccity.idCity=b.appl_ccity',array('CCityName'=>'CityName'))
		            ->joinLeft(array('cstate'=>'tbl_state'),'cstate.IdState=b.appl_cstate',array('CStateName'=>'StateName'))
		            ->joinLeft(array('cctrs'=>'tbl_countries'),'cctrs.idCountry=b.appl_ccountry',array('CCountryName'=>'CountryName'))
                            ->joinLeft(array('cp'=>'tbl_branchofficevenue'),'cp.IdBranch=a.branch_id',array('branchname'=>'cp.BranchName'))
		            ->order('a.at_pes_id')
		            ->order('d.IdProgram');        
		  
    
    	if (isset($formData['IdIntake']) && !empty($formData['IdIntake'])) { 
    		foreach($formData['IdIntake'] as $key=>$val){  
            	$select->orwhere("a.at_intake = ?",$val);
    		}
    		
        }
       
        if (isset($formData['IdProgram']) && !empty($formData['IdProgram'])) {            
            $select->where("c.ap_prog_id = ?",$formData['IdProgram']);
        }   

        if (isset($formData['IdProgramScheme']) && !empty($formData['IdProgramScheme'])) {            
            $select->where("c.ap_prog_scheme = ?",$formData['IdProgramScheme']);
        }

    	if (isset($formData['appl_category']) && !empty($formData['appl_category'])) {            
            $select->where("b.appl_category = ?",$formData['appl_category']);
        }
        
    	if (isset($formData['status']) && !empty($formData['status'])) {            
            $select->where("a.at_status = ?",$formData['status']);
        }
        
        $result = $db->fetchAll($select);
        
        foreach($result as $index=>$row){
        	//get age
        	if($row['appl_dob']!=''){
        		$result[$index]['age']=$this->calcutateAge($row['appl_dob']);
        	}
        	
        	//get qualification
        	$select_q = $db->select()
		            	->from(array('ae'=>'applicant_qualification'))
		            	->join(array('qm'=>'tbl_qualificationmaster'),'qm.IdQualification=ae.ae_qualification',array('QualificationLevel'))
		            	->where('ae.ae_transaction_id = ?',$row['at_trans_id']);
		    $result_q = $db->fetchAll($select_q);
		    $result[$index]['qualification'] =	$result_q;	
		    
		    //get highest qualification
        	$select_hq = $db->select()
		            	->from(array('ae'=>'applicant_qualification'))
		            	->join(array('qm'=>'tbl_qualificationmaster'),'qm.IdQualification=ae.ae_qualification',array('Level'=>'QualificationLevel'))
		            	->joinLeft(array('sm'=>'tbl_institution'), 'ae.ae_institution = sm.idInstitution', array('institution'=>'InstitutionName'))
		            	->joinLeft(array('d'=>'tbl_definationms'), 'ae.ae_class_degree = d.idDefinition', array('class_degree'=>'DefinitionDesc'))
		            	->where('ae.ae_transaction_id = ?',$row['at_trans_id'])
		            	->order('qm.QualificationRank ASC');
		    $result_hq = $db->fetchRow($select_hq);
		    $result[$index]['highest_qualification'] =	$result_hq;	
		   
		    
		    //get visa
		    $select_v = $db->select()
		    			   ->from(array('v'=>'applicant_visa'))
		    			   ->joinLeft(array('d'=>'tbl_definationms'), 'v.av_status = d.idDefinition', array('status'=>'DefinitionDesc'))
		    			   ->where('v.av_trans_id =?',$row['at_trans_id']);
		    $result_v = $db->fetchRow($select_v);
		    $result[$index]['av_malaysian_visa'] =	$result_v['av_malaysian_visa'];	
		    $result[$index]['av_status'] =	$result_v['status'];	
		    $result[$index]['av_expiry'] =	$result_v['av_expiry'];	
		    
		    //get financial
		    $select_f = $db->select()
		    			   ->from(array('f'=>'applicant_financial'))
		    			   ->joinLeft(array('d'=>'tbl_definationms'), 'f.af_method = d.idDefinition', array('method'=>'DefinitionDesc'))
		    			   ->joinLeft(array('df'=>'tbl_definationms'), 'f.af_type_scholarship = d.idDefinition', array('type_scholarship'=>'DefinitionDesc'))
		    			   ->joinLeft(array('da'=>'tbl_definationms'), 'f.af_scholarship_apply = da.idDefinition', array('scholarship_apply'=>'DefinitionDesc'))
		    			   ->joinLeft(array('st' => 'tbl_scholarshiptagging_sct'), 'f.af_scholarship_apply=st.sct_Id', array())
					   ->joinLeft(array('sc' => 'tbl_scholarship_sch'), 'st.sct_schId=sc.sch_Id', array('sc.sch_name'))
                                           ->where('f.af_trans_id =?',$row['at_trans_id']);
		  	$result_f = $db->fetchRow($select_f);
		    $result[$index]['financial'] =	$result_f;	
		      			   
		    //get health condition
		    $select_h = $db->select()
		    			   ->from(array('h'=>'applicant_health_condition'))
		    			   ->join(array('d'=>'tbl_definationms'), 'h.ah_status = d.idDefinition', array('condition'=>'DefinitionDesc'))
		    			   ->where('h.ah_trans_id =?',$row['at_trans_id']);
		    $result_h = $db->fetchAll($select_h);
		    $result[$index]['health'] =	$result_h;	
		  
		    //english proficiency
		    $select_ep = $db->select()
		    			   ->from(array('ep'=>'applicant_english_proficiency'))
		    			   //->join(array('d'=>'tbl_definationms'), 'ep.ep_test = d.idDefinition', array('engtest'=>'DefinitionDesc'))
		    			   ->join(array('qm'=>'tbl_qualificationmaster'),'qm.IdQualification=ep.ep_test',array('engtest'=>'QualificationLevel'))
		    			   ->where('ep.ep_transaction_id =?',$row['at_trans_id']);
		    $result_ep = $db->fetchRow($select_ep);
		    $result[$index]['engprof'] = $result_ep;	
		   
		    //get employement
		    $select_e = $db->select()
		    			   ->from(array('emp'=>'applicant_employment'))
		    			   ->join(array('d'=>'tbl_definationms'), 'emp.ae_status = d.idDefinition', array('emp_status'=>'DefinitionDesc'))
		    			   ->join(array('df'=>'tbl_definationms'), 'emp.ae_position = df.idDefinition', array('emp_position'=>'DefinitionDesc'))
		    			   ->join(array('dt'=>'tbl_definationms'), 'emp.ae_industry = dt.idDefinition', array('emp_industry'=>'DefinitionDesc'))
		    			   ->where('emp.ae_trans_id =?',$row['at_trans_id']);
		    $result_e = $db->fetchRow($select_e);
		    $result[$index]['employment'] = $result_e;	
        }
        
        return $result;
    }
    
	function calcutateAge($dob){

        $dob = date("Y-m-d",strtotime($dob));

        $dobObject = new DateTime($dob);
        $nowObject = new DateTime();

        $diff = $dobObject->diff($nowObject);

        return $diff->y;

}

    static function getDocChecklist($progSchemeId, $studentCat, $finance = 0){
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $select = $db->select()
            ->from(array('a'=>'tbl_documentchecklist_dcl'), array('value'=>'*'))
            ->where('a.dcl_programScheme = ?', $progSchemeId)
            ->where('a.dcl_stdCtgy = ?', $studentCat)
            ->where('a.dcl_activeStatus = ?', 1)
            ->where('a.dcl_finance = ?', $finance);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    static function getDocChecklistStatus($dclId, $txnId){
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $select = $db->select()
            ->from(array('a'=>'applicant_document_status'), array('value'=>'*'))
            ->where('a.ads_dcl_id = ?', $dclId)
            ->where('a.ads_txn_id = ?', $txnId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    static function getStatusName($status){
        switch ($status){
            case 0:
                $statusName = 'NULL';
                break;
            case 1:
                $statusName = 'New';
                break;
            case 2:
                $statusName = 'Viewed';
                break;
            case 3:
                $statusName = 'Incomplete';
                break;
            case 4:
                $statusName = 'Complete';
                break;
            case 5:
                $statusName = 'Not Applicable';
                break;
            default:
                $statusName = 'NULL';
                break;
        }
        
        return $statusName;
    }
    
    static function getStatusPayName($status){
        if ($status == 4){
            $statusName = 'Paid';
        }else if ($status == 5){
            $statusName = 'Not Applicable';
        }else{
            $statusName = 'Not Paid';
        }
        
        return $statusName;
    }
    
    static function checkUpl($txnid, $dclid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_documents'), array('value'=>'*'))
            ->where('a.ad_txn_id = ?', $txnid)
            ->where('a.ad_dcl_id = ?', $dclid);
        
        $result = $db->fetchAll($select);
        return $result;
    }
}
?>