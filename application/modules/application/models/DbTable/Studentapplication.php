<?php

class Application_Model_DbTable_Studentapplication extends Zend_Db_Table_Abstract {

  protected $_name = 'tbl_studentapplication';
  private $lobjDbAdpt;

  public function init() {
    $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  }

  public function fnaddNewStudent($data) {  
    $lastinsertid = $this->insert($data);
    return $lastinsertid;
  }

  public function fnupdateStudentdata($lobjFormData, $Idapplication) {
    $id = intval($Idapplication);
    $where = 'IdApplication = ' . $id;
    $this->update($lobjFormData, $where);
  }

  public function fnUpdateHistoryApplication($data){
    $this->lobjDbAdpt->Insert('tbl_application_history', $data);
  }

  public function fncheckextraid($extraid) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("b" => "tbl_studentregistration"), array('b.ExtraIdField1'))
                    ->where("b.ExtraIdField1= ?", $extraid);
    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
    return $larrResult;
  }

  public function fncheckextraid1($Idapplication, $extraid) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_applicant_personal_detail"), array("a.IdApplicant"))
                    ->join(array("b" => "tbl_studentapplication"), "a.ExtraIdField1 = b.ExtraIdField1 OR b.ExtraIdField1 = '$extraid' ", array("b.ExtraIdField1","b.IdApplication"))
                    ->where("a.ExtraIdField1 = ?", $extraid)
                    ->orwhere("b.ExtraIdField1 = ?", $extraid)
                    ->group('a.IdApplicant');
    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
    return $larrResult;
  }

  public function fngetregisterdate($IdApplicant) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_applicant"), array("a.RegisteredDate"))
                    ->joinLeft(array("b" => "tbl_application_config"), 'b.ApplicationVal != 0', array('b.ApplicationValidity', 'b.ApplicationVal'))
                    ->where("a.IdApplicant = ?", $IdApplicant);
    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
    return $larrResult;
  }

  public function fnupdatestudentDetails($lobjFormData, $Idapplication) {
    $db = Zend_Db_Table::getDefaultAdapter();
    //$data['Status'] = $status;
    $where = 'IdApplication= ' . $Idapplication;
    $db->update('tbl_studentapplication', $lobjFormData, $where);
  }

  public function fnaddStudentEmergencydetail($lobjFormData, $IdApplication) {
    $where = 'IdApplication = ' . $IdApplication;
    unset($lobjFormData['EmergencyHomePhonecountrycode']);
    unset($lobjFormData['EmergencyHomePhonestatecode']);
    unset($lobjFormData['EmergencyOffPhonecountrycode']);
    unset($lobjFormData['EmergencyOffPhonestatecode']);
    unset($lobjFormData['EmergencyCellPhonecountrycode']);
    $this->update($lobjFormData, $where);
  }

  public function fnTocheckUniqueEmail($email){
    $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_studentapplication"), array("a.IdApplication"))                   
                    ->where("a.PEmail = ?", $email);
    return $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
  }

  public function fnTocheckUniqueEmailinOnline($email){
    $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_applicant_personal_detail"), array("a.IdApplicantPersonalDetail"))
                    ->where("a.email = ?", $email);
    return $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
  }

  public function fnupdateStudent($lobjFormData, $IdApplication) {
    $where = 'IdApplication = ' . $IdApplication;
    $lobjFormData['HomePhone'] = $lobjFormData['HomePhonecountrycode'] . "-" . $lobjFormData['HomePhonestatecode'] . "-" . $lobjFormData['HomePhone'];
    unset($lobjFormData['HomePhonecountrycode']);
    unset($lobjFormData['HomePhonestatecode']);

    $lobjFormData['CellPhone'] = $lobjFormData['CellPhonecountrycode'] . "-" . $lobjFormData['CellPhone'];
    unset($lobjFormData['CellPhonecountrycode']);


    $lobjFormData['Fax'] = $lobjFormData['Faxcountrycode'] . "-" . $lobjFormData['Faxstatecode'] . "-" . $lobjFormData['Fax'];
    unset($lobjFormData['Faxcountrycode']);
    unset($lobjFormData['Faxstatecode']);
    $this->update($lobjFormData, $where);
  }

  public function getStudentdet($id) {
    $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_studentapplication"))
                    ->join(array("b" => "tbl_definationms"), "a.Status = b.idDefinition", array('b.DefinitionCode', 'b.DefinitionDesc'))
                    ->joinLeft(array("c" => "tbl_program"), "a.ProvisionalProgramOffered = c.IdProgram", array('c.ProgramName AS programprovisonaloffered'))
                    ->joinLeft(array("d" => "tbl_program"), "a.ProgramOfferred = d.IdProgram", array('d.ProgramName AS programoffered'))
                    ->joinLeft(array("e" => "tbl_branchofficevenue"), "a.ProvisionalBranchOffered = e.IdBranch", array('e.BranchName AS branchprovisionaloferred'))
                    ->joinLeft(array("f" => "tbl_branchofficevenue"), "a.BranchOfferred = f.IdBranch", array('f.BranchName AS branchoffered'))
                    ->joinLeft(array("g" => "tbl_intake"), "a.intake = g.IdIntake", array('g.IntakeDesc AS IntakeName'))
                    ->joinLeft(array("h" => "tbl_definationms"),"a.Source = h.idDefinition",array("h.DefinitionDesc AS ApplicationSource"))
                    ->where("a.IdApplication = ?", $id);
    $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult[0];
  }

  public function getstudentHistory($IdApplication){
    $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_application_history"), array('a.*'))
                    ->joinLeft(array("b" => "tbl_definationms"),"a.status = b.idDefinition",array("b.DefinitionDesc"))
                    ->joinLeft(array("c" => "tbl_user"),"a.UpdUser = c.iduser",array("c.loginName"))
                    ->where("a.IdApplication = ?", $IdApplication)
                    ->order("a.IdApplicationHistory");
    $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);    
    return $larrResult;
  }

  public function getExtraStudentdet($Idprogram, $Idapplication) {
    $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_studentapplication"), array('a.IdApplication'))                    
                    ->joinLeft(array("c" => "tbl_programchecklist"), "a.ProvisionalProgramOffered = c.IdProgram", array('c.CheckListName'))
                    ->joinLeft(array("d" => "tbl_programchecklist"), "a.ProgramOfferred = d.IdProgram", array('c.CheckListName'))
                    ->where("a.IdApplication = ?", $Idapplication)
                    ->where("c.IdProgram = ?", $Idprogram)
                    ->orwhere("d.IdProgram = ?", $Idprogram)
                    ->group('c.CheckListName');
    $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);    
    return $larrResult;
  }

  public function getAgentName($Idapplication){
    $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_studentapplication"), array(''))
                    ->joinLeft(array("b" => "tbl_agentapplicanttagging"), "a.IdApplicant = b.IdStudent", array('b.IdAgent'))
                    ->where("a.IdApplication = ?", $Idapplication);                   
    $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;

  }

  public function getstudentpreferredet($IdApplication) {
    $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_student_preffered"))
                    ->joinLeft(array('b' => 'tbl_definationms'), 'a.IdProgramLevel = b.idDefinition', array('b.DefinitionDesc AS programLevelName'))
                    ->joinLeft(array('c' => 'tbl_program'), 'a.IdProgram = c.IdProgram', array('c.ProgramName'))
                    ->joinLeft(array('d' => 'tbl_branchofficevenue'), 'a.IdBranch = d.IdBranch', array('d.BranchName'))
                    ->joinLeft(array('e' => 'tbl_scheme'), 'a.IdScheme = e.IdScheme', array('e.EnglishDescription'))
                    ->where("a.IdApplication = ?", $IdApplication)
                    ->order("a.IdPriorityNo");
    $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function fnEducationlist() { //Function to get the Program Branch details
    $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array("a" => "tbl_studentapplication"), array("a.IdApplication", "a.FName", "a.LName", "a.status","a.Source"))
                    //->joinLeft(array("b" => "tbl_studenteducationdetails"),'a.IdApplication = b.IdApplication')
                    ->joinLeft(array("c" => "tbl_student_preffered"), 'a.IdApplication = c.IdApplication', array("c.IdProgram", "c.IdBranch"))
                    ->joinLeft(array("d" => "tbl_program"), 'c.IdProgram = d.IdProgram', array("d.ProgramName"))
                    ->joinLeft(array("e" => "tbl_branchofficevenue"), 'c.IdBranch = e.IdBranch', array("e.BranchName"))
                    ->joinLeft(array("f" => 'tbl_definationms'), 'a.status = f.idDefinition', array('f.DefinitionDesc'))
                    ->where('c.IdPriorityNo = ? OR f.idDefinition = 193', 1)
                    ->group("a.IdApplication")
                    ->order("a.FName");
    $result = $this->fetchAll($select);
    return $result->toArray();
  }

  public function fngetapplicantid() {
    $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_applicant"), array("a.IdApplicant"));
    $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function fngetapplicantstatus($applicantid) {
  	$select = $this->select()
                    ->setIntegrityCheck(false)
                    ->join(array('a' => 'tbl_applicant_personal_detail'), array())
                    ->join(array('b' => 'tbl_definationms'), 'a.status = b.idDefinition', array('b.DefinitionDesc'))
                    ->where('a.IdApplicant = ?', $applicantid);
    $result = $this->fetchRow($select);
    return $result->toArray();
  }

  public function fnSearchEducationlist($post) { //Function to get the Program Branch details
    $field7 = "a.Active = " . $post["field7"];

    $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('a' => 'tbl_studentapplication'), array("a.IdApplication", "a.FName", "a.LName", "a.status","a.Source"))
                    ->joinLeft(array("b" => "tbl_studenteducationdetails"), 'a.IdApplication = b.IdApplication')
                    ->joinLeft(array("c" => "tbl_student_preffered"), 'a.IdApplication = c.IdApplication')
                    ->joinLeft(array("d" => "tbl_program"), 'c.IdProgram = d.IdProgram')
                    ->joinLeft(array("e" => "tbl_branchofficevenue"), 'c.IdBranch = e.IdBranch')
                    ->joinLeft(array("f" => 'tbl_definationms'), 'a.status = f.idDefinition', array('f.DefinitionDesc'))
                    ->where('a.FName  like "%" ? "%"', $post['field3'])
                    ->where('c.IdPriorityNo = ? OR f.idDefinition = 192', 1)
                    ->where($field7);
    if (isset($post['field5']) && !empty($post['field5'])) {
      $select = $select->where("c.IdProgram  = ?", $post['field5']);
    }
    if (isset($post['field2']) && !empty($post['field2'])) {
      $select = $select->where("a.ExtraIdField1  = ?", $post['field2']);
    }
  	if (isset($post['field1']) && !empty($post['field1'])) {
      $select = $select->where("a.Source  = ?", $post['field1']);
    }
    if (isset($post['field8']) && !empty($post['field8'])) {
      $select = $select->where("c.IdBranch  = ?", $post['field8']);
    }
    $result = $this->fetchAll($select);
    return $result->toArray();
  }

  public function fnGetSchoolMasterList() {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("sm" => "tbl_schoolmaster"), array("key" => "sm.idSchool", "value" => "sm.SchoolName"))
                    ->where("sm.Active = 1")
                    ->order("sm.SchoolName");
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function fnaddStudent($lobjFormData) {
    unset($lobjFormData['InstitutionName']);
    unset($lobjFormData['UniversityName']);
    unset($lobjFormData['StudyPlace']);
    unset($lobjFormData['MajorFiledOfStudy']);
    unset($lobjFormData['YearOfStudyFrom']);
    unset($lobjFormData['YearOfStudyTo']);
    unset($lobjFormData['DegreeType']);
    unset($lobjFormData['GradeOrCGPA']);
    unset($lobjFormData['InstitutionNamegrid']);
    unset($lobjFormData['InstitutionNameSelectgrid']);
    unset($lobjFormData['UniversityNamegrid']);
    unset($lobjFormData['StudyPlacegrid']);
    unset($lobjFormData['MajorFiledOfStudygrid']);
    unset($lobjFormData['YearOfStudyFromgrid']);
    unset($lobjFormData['YearOfStudyTogrid']);
    unset($lobjFormData['DegreeTypegrid']);
    unset($lobjFormData['GradeOrCGPAgrid']);
    unset($lobjFormData['InstitutionNameSelect']);


    unset($lobjFormData['TypeOfSchool']);
    unset($lobjFormData['StatusOfSchool']);
    unset($lobjFormData['TypeOfStud']);
    unset($lobjFormData['HomeTownSchoolTB']);
    unset($lobjFormData['CreditTransferFromTB']);
    unset($lobjFormData['HomeTownSchoolDD']);
    unset($lobjFormData['CreditTransferFromDD']);



    unset($lobjFormData['TypeofSchoolgrid']);
    unset($lobjFormData['StatusOfSchoolgrid']);
    unset($lobjFormData['TypeOfStudgrid']);
    unset($lobjFormData['HomeTownSchoolgrid']);
    unset($lobjFormData['CreditTransferFromgrid']);

    unset($lobjFormData['uploadedfiles']);
    unset($lobjFormData['uploadeddocuments']);

    unset($lobjFormData['comments']);
    unset($lobjFormData['documentcategory']);


    unset($lobjFormData['SubjectId']);
    unset($lobjFormData['subjectIdgrid']);
    unset($lobjFormData['SubjectMark']);
    unset($lobjFormData['SubjectMarkgrid']);
    unset($lobjFormData['pgmchklistIdgrid']);
    unset($lobjFormData['ProgCheckListName']);


    $lobjFormData['HomePhone'] = $lobjFormData['HomePhonecountrycode'] . "-" . $lobjFormData['HomePhonestatecode'] . "-" . $lobjFormData['HomePhone'];
    unset($lobjFormData['HomePhonecountrycode']);
    unset($lobjFormData['HomePhonestatecode']);

    $lobjFormData['CellPhone'] = $lobjFormData['CellPhonecountrycode'] . "-" . $lobjFormData['CellPhone'];
    unset($lobjFormData['CellPhonecountrycode']);


    $lobjFormData['Fax'] = $lobjFormData['Faxcountrycode'] . "-" . $lobjFormData['Faxstatecode'] . "-" . $lobjFormData['Fax'];
    unset($lobjFormData['Faxcountrycode']);
    unset($lobjFormData['Faxstatecode']);

    $lastinsertid = $this->insert($lobjFormData);
    return $lastinsertid;
  }

  public function fnaddStudentImageDetails($larrfilenameslist, $IdApplication) {

    $str = "";
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

    foreach ($larrfilenameslist as $larrfilenames) {
      $str .= "(" . $IdApplication . ",'" . $larrfilenames['FileLocation'] . "','" . $larrfilenames['filename'] . "','" . $larrfilenames['uploadedfilename'] . "','" . $larrfilenames['size'] . "','" . $larrfilenames['type'] . "','" . $larrfilenames['UpdDate'] . "'," . $larrfilenames['UpdUser'] . ",'" . $larrfilenames['Comments'] . "','" . $larrfilenames['documentcategory'] . "'),";
    }

    $lstrselectsql = "INSERT INTO tbl_documentdetails (IdApplication,FileLocation,FileName,UploadedFilename,FileSize,MIMEType,UpdDate,UpdUser,Comments,documentcategory) VALUES " . substr($str, 0, -1);
    $lobjDbAdpt->query($lstrselectsql);
  }

  public function fnaddStudentDocumentDetails($larrfilenameslist, $IdApplication) {
    $str = "";
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

    foreach ($larrfilenameslist as $larrfilenames) {
      $str .= "(" . $IdApplication . ",'" . $larrfilenames['FileLocation'] . "','" . $larrfilenames['filename'] . "','" . $larrfilenames['uploadedfilename'] . "','" . $larrfilenames['size'] . "','" . $larrfilenames['type'] . "','" . $larrfilenames['UpdDate'] . "'," . $larrfilenames['UpdUser'] . ",'" . $larrfilenames['Comments'] . "','" . $larrfilenames['documentcategory'] . "'),";
    }

    $lstrselectsql = "INSERT INTO tbl_documentdetails (IdApplication,FileLocation,FileName,UploadedFilename,FileSize,MIMEType,UpdDate,UpdUser,Comments,documentcategory) VALUES " . substr($str, 0, -1);

    $lobjDbAdpt->query($lstrselectsql);
  }

  function fnGenerateCode($idUniversity, $collageId, $page, $IdInserted) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $select = $db->select()
                    ->from('tbl_config')
                    ->where('idUniversity  = ?', $idUniversity);

    $result = $db->fetchRow($select);
    $sepr = $result[$page . 'Separator'];
    $str = $page . "CodeField";
    $CodeText = $page . "CodeText";
    for ($i = 1; $i <= 4; $i++) {
      $check = $result[$str . $i];
      $TextCode = $result[$CodeText . $i];
      switch ($check) {
        case 'Year':
          $code = date('Y');
          break;
        case 'Uniqueid':
          $code = $IdInserted;
          break;
        case 'College':
          $select = $db->select()
                          ->from('tbl_collegemaster')
                          ->where('IdCollege  = ?', $collageId);
          $resultCollage = $db->fetchRow($select);
          $code = $resultCollage['ShortName'];
          break;
        case 'University':
          $select = $db->select()
                          ->from('tbl_universitymaster')
                          ->where('IdUniversity  = ?', $idUniversity);
          $resultCollage = $db->fetchRow($select);
          $code = $resultCollage['ShortName'];
          break;
        case 'Text':
          $code = $TextCode;
          break;
        default:
          break;
      }
      if ($i == 1)
        $accCode = $code;
      else
        $accCode .= $sepr . $code;
    }
    return $accCode;
  }

  public function fnupdatestudentCode($IdApplication, $StudentCode) {
    $larrformData['StudentId'] = $StudentCode;
    $where = 'IdApplication = ' . $IdApplication;
    $this->update($larrformData, $where);
  }

  public function fninsertstudenteducation($studenteducationresult, $IdApplication, $LocalStudent) {  // function to insert po details
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_tempstudenteducationdetails";
    $sessionID = Zend_Session::getId();
    foreach ($studenteducationresult as $studenteducationresult) {
      $larrcourse = array('InstitutionName' => $studenteducationresult['InstitutionName'],
          'UniversityName' => $studenteducationresult['UniversityName'],
          'ProgCheckListName' => $studenteducationresult['ProgCheckListName'],
          'StudyPlace' => $studenteducationresult['StudyPlace'],
          'MajorFiledOfStudy' => $studenteducationresult['MajorFiledOfStudy'],
          'YearOfStudyFrom' => $studenteducationresult['YearOfStudyFrom'],
          'YearOfStudyTo' => $studenteducationresult['YearOfStudyTo'],
          'DegreeType' => $studenteducationresult['DegreeType'],
          'GradeOrCGPA' => $studenteducationresult['GradeOrCGPA'],
          'UpdUser' => $studenteducationresult['UpdUser'],
          'UpdDate' => $studenteducationresult['UpdDate'],
          'unicode' => $IdApplication,
          'Date' => date("Y-m-d"),
          'sessionId' => $sessionID,
          'deleteFlag' => 1,
          'idExists' => $studenteducationresult['IdStudEduDtl']
      );

      $db->insert($table, $larrcourse);
    }
  }

  public function fnGetTempStudenteducationdetails($IdApplication, $LocalStudent) { //Function for the view University
    if ($LocalStudent == 0) {
      $select = $this->select()
                      ->setIntegrityCheck(false)
                      ->join(array('a' => 'tbl_tempstudenteducationdetails'), array('a.IdApplication'))
                      ->join(array('b' => 'tbl_definationms'), 'a.DegreeType = b.idDefinition')
                      ->join(array('c' => 'tbl_definationms'), 'a.GradeOrCGPA = c.idDefinition', array('c.DefinitionDesc as GradeDefinitionDesc'))
                      ->where('a.unicode = ?', $IdApplication)
                      ->where('a.deleteFlag = 1');
    } else {
      $select = $this->select()
                      ->setIntegrityCheck(false)
                      ->join(array('a' => 'tbl_tempstudenteducationdetails'), array('a.IdApplication'))
                      ->join(array('b' => 'tbl_definationms'), 'a.DegreeType = b.idDefinition')
                      ->join(array('c' => 'tbl_definationms'), 'a.GradeOrCGPA = c.idDefinition', array('c.DefinitionDesc as GradeDefinitionDesc'))
                      ->join(array('d' => 'tbl_schoolmaster'), 'a.InstitutionName = d.idSchool')
                      ->where('a.unicode = ?', $IdApplication)
                      ->where('a.deleteFlag = 1');
    }
    $result = $this->fetchAll($select);
    return $result->toArray();
  }

  public function fnEducationdetaillist($id) { //Function to get the Program Branch details
    $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->join(array('a' => 'tbl_applicant_personal_detail'), array('a.IdApplicant'))
                    ->join(array('b' => 'tbl_applicant_preffered'), 'a.IdApplicant=b.IdApplicant', array('b.IdProgram', 'b.IdPriorityNo'))
                    ->join(array('c' => 'tbl_program'), 'b.IdProgram=c.IdProgram', array('c.ProgramName'))
                    ->where('a.IdApplicant = ?', $id)
                    ->where('b.IdPriorityNo =?', 1);
    $result = $this->fetchAll($select);
    return $result->toArray();
  }

  public function fnEducationdetailviewlist($icnumber) { //Function to get the Program Branch details
    $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->join(array('a' => 'tbl_studentapplication'), array('IdApplication'))
                    ->join(array('b' => 'tbl_program'), 'a.IDCourse  = b.IdProgram')
                    ->join(array('c' => 'tbl_definationms'), 'b.Award = c.idDefinition')
                    ->join(array('d' => 'tbl_collegemaster'), 'a.idCollege = d.IdCollege')
                    ->join(array('e' => 'tbl_placementtest'), 'a.IdPlacementtest = e.IdPlacementTest')
                    ->join(array('f' => 'tbl_countries'), 'a.PermCountry = f.idCountry', array("PermCountryName" => "f.CountryName"))
                    ->joinLeft(array('g' => 'tbl_countries'), 'a.CorrsCountry = g.idCountry', array("CorrCountryName" => "g.CountryName"))
                    ->join(array('h' => 'tbl_state'), 'a.PermState = h.idState', array("PermStatename" => "h.StateName"))
                    ->joinLeft(array('i' => 'tbl_state'), 'a.CorrsState = i.idState', array("CorrStatename" => "i.StateName"))
                    ->join(array('j' => 'tbl_sponsor'), 'a.idsponsor = j.idsponsor')
                    ->where('a.ICNumber = ?', $icnumber);
    $result = $this->fetchAll($select);
    return $result->toArray();
  }

  public function fnupdateconfirmation($IdApplication) { //Function for updating the user
    $larrformData = array('Accepted' => 1);
    $where = 'IdApplication = ' . $IdApplication;
    $this->update($larrformData, $where);
  }

  public function fnupdatestudentapplication($IdApplication, $larrformData) {
    unset($larrformData['InstitutionName']);
    unset($larrformData['UniversityName']);
    unset($larrformData['StudyPlace']);
    unset($larrformData['MajorFiledOfStudy']);
    unset($larrformData['YearOfStudyFrom']);
    unset($larrformData['YearOfStudyTo']);
    unset($larrformData['DegreeType']);
    unset($larrformData['GradeOrCGPA']);
    unset($larrformData['InstitutionNamegrid']);
    unset($larrformData['UniversityNamegrid']);
    unset($larrformData['StudyPlacegrid']);
    unset($larrformData['MajorFiledOfStudygrid']);
    unset($larrformData['YearOfStudyFromgrid']);
    unset($larrformData['YearOfStudyTogrid']);
    unset($larrformData['DegreeTypegrid']);
    unset($larrformData['GradeOrCGPAgrid']);
    unset($larrformData['IdStudEduDtl']);
    unset($larrformData['InstitutionNameSelect']);
    unset($larrformData['SameCorrespAddr']);
    unset($larrformData['TypeOfSchool']);
    unset($larrformData['StatusOfSchool']);
    unset($larrformData['TypeOfStud']);
    unset($larrformData['HomeTownSchoolDD']);
    unset($larrformData['HomeTownSchoolTB']);
    unset($larrformData['CreditTransferFromDD']);
    unset($larrformData['CreditTransferFromTB']);
    unset($larrformData['TypeofSchoolgrid']);
    unset($larrformData['StatusOfSchoolgrid']);
    unset($larrformData['TypeOfStudgrid']);
    unset($larrformData['HomeTownSchoolgrid']);
    unset($larrformData['CreditTransferFromgrid']);


    unset($larrformData['SubjectId']);
    unset($larrformData['subjectIdgrid']);
    unset($larrformData['SubjectMark']);
    unset($larrformData['SubjectMarkgrid']);
    unset($larrformData['ProgCheckListName']);
    unset($larrformData['pgmchklistIdgrid']);


    unset($larrformData['uploadedfiles']);
    unset($larrformData['uploadeddocuments']);

    unset($larrformData['comments']);
    unset($larrformData['documentcategory']);

    $larrformData['HomePhone'] = $larrformData['HomePhonecountrycode'] . "-" . $larrformData['HomePhonestatecode'] . "-" . $larrformData['HomePhone'];
    unset($larrformData['HomePhonecountrycode']);
    unset($larrformData['HomePhonestatecode']);

    $larrformData['CellPhone'] = $larrformData['CellPhonecountrycode'] . "-" . $larrformData['CellPhone'];
    unset($larrformData['CellPhonecountrycode']);


    $larrformData['Fax'] = $larrformData['Faxcountrycode'] . "-" . $larrformData['Faxstatecode'] . "-" . $larrformData['Fax'];
    unset($larrformData['Faxcountrycode']);
    unset($larrformData['Faxstatecode']);

    $where = 'IdApplication = ' . $IdApplication;
    $this->update($larrformData, $where);
  }

  public function fnUpdateTempEditEducationdetails($InstitutionName, $UniversityName, $StudyPlace, $YearOfStudyFrom, $YearOfStudyTo, $DegreeType, $GradeOrCGPA, $IdStudEduDtl, $upddate) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_tempstudenteducationdetails";
    $larridpo = array('InstitutionName' => $idItem,
        'UniversityName' => $unitPrice,
        'StudyPlace' => $quatnity,
        'MajorFiledOfStudy' => $UOM,
        'YearOfStudyFrom' => $idTax,
        'YearOfStudyTo' => $taxPercent,
        'DegreeType' => $DtReqrd,
        'GradeOrCGPA' => $idPoDtlCostCenter,
        'UpdUser' => $PoBatchNo,
        'UpdDate' => $PODtlNarration);
    $where = "idTemp = '$IdStudEduDtl'";
    $db->update($table, $larridpo, $where);
  }

  public function fnGetProgramChargesList($idCourse) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $select = $db->select()
                    ->from(array('a' => 'tbl_programcharges'), 'a.*')
                    ->join(array('b' => 'tbl_charges'), 'b.IdCharges=a.IdCharges AND b.Payment = 0')
                    ->where('IdProgram  = ?', $idCourse);
    return $db->fetchAll($select);
  }

  public function fnaddInvoice($larrresInvoice, $result) {

    $totamt = 0;
    for ($i = 0; $i < count($larrresInvoice); $i++) {
      $totamt = $totamt + $larrresInvoice[$i]['Rate'];
    }
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_invoicemaster";

    $larrcourse = array('IdStudent' => $result,
        'InvoiceNo' => 1234,
        'InvoiceDt' => date("Y-m-d"),
        'InvoiceAmt' => $totamt,
        'MonthYear' => date("MY"),
        'AcdmcPeriod' => $larrresInvoice[0]['IdStartSemester'],
        'Naration' => "Narration",
        'UpdDate' => date("Y-m-d"),
        'UpdUser' => 1,
        'Active' => 1,
        'Approved' => 0,
        'idsponsor' => 0
    );

    $db->insert($table, $larrcourse);
    $insertId = $db->lastInsertId('tbl_invoicemaster', 'IdInvoice');
    return $insertId;
  }

  public function fnaddInvDetails($larrresInvoice, $lastarrInv) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $table2 = "tbl_invoicedetails";
    for ($k = 0; $k < count($larrresInvoice); $k++) {

      $larrcourse = array('IdInvoice' => $lastarrInv,
          'idAccount' => $larrresInvoice[$k]['idAccount'],
          'Discount' => 0,
          'Amount' => $larrresInvoice[$k]['Rate'],
          'UpdDate' => date("Y-m-d"),
          'UpdUser' => 1,
          'Active' => 1
      );
      $db->insert($table2, $larrcourse);
    }
  }

  public function fnGetInvoicedetails($idprogram) {

    $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_charges"), array("a.Rate"))
                    ->join(array('b' => 'tbl_accountmaster'), 'a.IdAccountMaster=b.idAccount AND b.duringProcessing = 1')
                    ->join(array('c' => 'tbl_landscape'), 'a.IdProgram=c.IdProgram', array("c.IdStartSemester"))
                    ->where("a.Active = 1")
                    ->where("a.IdProgram= ?", $idprogram)
                    ->group("a.IdCharges");

    $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function fnGetLocalorinternamtional($idCourse) {
    $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_programentry"))
                    ->where("a.Active = 1")
                    ->where("a.IdProgram = ?", $idCourse);
    $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
    return $larrResult;
  }

  public function fnGetProgramList() {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_program"), array("key" => "a.IdProgram", "value" => "ProgramName"))
                    ->where("a.Active = 1")
                    ->order("a.ProgramName");
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function fnGetSubjectList($idcourse) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $consistantresult = 'SELECT i.IdSubject  from tbl_subjectprogram i where i.IdProgram=' . $idcourse;
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_subjectmaster"), array("key" => "a.IdSubject", "value" => "SubjectName"))
                    ->where('a.IdSubject IN (?)', new Zend_Db_Expr('(' . $consistantresult . ')'))
                    ->where("a.Active = 1")
                    ->order("a.SubjectName");
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function fnGetProgramcheckList($idcourse) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_programchecklist"), array("key" => "a.IdCheckList", "value" => "a.CheckListName"))
                    ->where("a.IdProgram = ?", $idcourse)
                    ->where("a.Active = 1")
                    ->order("a.CheckListName");
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }
  
  public function fnGetCountryCode($countryId){
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
 						     ->from(array("a" => "tbl_countries"), array("key" => "a.idCountry", "value" => "a.CountryCode"))
 						     ->where('a.idCountry = ?',$countryId);
    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
 	return $larrResult;					     
  }

  function fnGenerateCodes($universityId, $uniqId, $StudentId) {

    $db = Zend_Db_Table::getDefaultAdapter();
    $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from('tbl_config')
                    ->where('idUniversity = ?', $universityId);
    $result = $this->fetchRow($select);
    $sepr = $result['InvoiceSeparator'];
    $str = "InvoiceCodeField";
    for ($i = 1; $i <= 4; $i++) {
      $check = $result[$str . $i];
      switch ($check) {
        case 'Dates':
          $code = date('d');
          break;
        case 'StudentId':
          $code = $StudentId;
          break;
        case 'Year':
          $code = date('Y');
          break;
        case 'InvoiceId':
          $code = $uniqId;
          break;
        default:
          break;
      }
      if ($i == 1)
        $accCode = $code;
      else
        $accCode .= $sepr . $code;
    }

    $data = array('InvoiceNo' => $accCode);
    $where['IdInvoice  = ? '] = $uniqId;
    return $db->update('tbl_invoicemaster', $data, $where);
  }

}

?>