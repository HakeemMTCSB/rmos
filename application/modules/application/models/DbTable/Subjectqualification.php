<?php

class Application_Model_DbTable_Subjectqualification extends Zend_Db_Table_Abstract {

  protected $_name = 'tbl_education_subject';
  private $lobjDbAdpt;

  public function init() {
    $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  }

  public function fndeleteSubjectDetailMappings($Id){
    $where = $this->lobjDbAdpt->quoteInto('IdStudEduDtl = ?', $Id);
    $this->lobjDbAdpt->delete($this->_name, $where);
  }

  public function fnaddSubject($data){
    $this->insert($data);
  }

  function fngetsubjectdetails($IdQualification){
    $lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_education_subject'),array())
		->joinLeft(array('b' => 'tbl_subject'),'a.IdSubject = b.IdSubject',array('b.SubjectName as Subject','a.IdSubject as IdSubject'))
                ->joinLeft(array('c' => 'tbl_subjectgradepoint'),'a.IdSubjectGrade = c.Idsubjectgradepoint',array('c.subjectgrade as SubjectGrade','c.Idsubjectgradepoint as IdSubjectGrade'))
                ->where('a.IdStudEduDtl = ?',$IdQualification)
                ->order("a.IdStudEduDtl");
    $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }
}

?>
