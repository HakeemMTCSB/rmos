<?php
require_once 'Zend/Controller/Action.php';

class Application_Model_DbTable_ApplicantEntryRequirement extends Zend_Db_Table_Abstract
{

    protected $_name = 'applicant_entry_req';
    protected $_primary = 'ae_entry_id';

    public function getData($txn_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('ae' => $this->_name))
            ->join(array('at' => 'applicant_transaction'), 'at.at_trans_id = ae.ae_trans_id')
            ->join(array('e' => 'tbl_programentryrequirement'), 'e.IdProgramEntryReq = ae.entry_id', array('IdProgramEntryReq', 'IdProgramEntry'))
            ->where("ae.ae_trans_id = ? ", $txn_id);

        $row = $db->fetchAll($select);

        if ($row) {
            return $row;
        } else {
            return null;
        }
    }

    public function getProgramEntryApplicantById($transID, $id)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('ab' => $this->_name))
            ->where('ab.ae_trans_id = ? ', $transID)
            ->where('ab.entry_id = ? ', $id);

        $result = $db->fetchRow($select);
        return $result;
    }


    public function addData($data)
    {
        $id = $this->insert($data);
        return $id;
    }

    public function updateData($data, $id)
    {
        $this->update($data, $this->_primary . ' = ' . (int)$id);
    }

}

