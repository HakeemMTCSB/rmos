<?php
class Application_Model_DbTable_QualificationsubjectMapping extends Zend_Db_Table {
	protected $_name = 'tbl_qualification_subject_mapping';
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function fnadd($data) {
		$this->insert($data);
	}
	
	public function fngetmappings($IdSubject) {
		$select = $this->select()
		->setIntegrityCheck(false)
		->join(array('a' => 'tbl_qualification_type'),'',array())
		->joinleft(array('b'=>$this->_name),'a.id = b.IdQualification',array('a.qt_eng_desc','a.qt_id','a.id'))
		//->join(array('c'=>"tbl_subject"),'c.IdSubject = b.IdSubject',array('c.SubjectName','c.IdSubject','c.SubjectCode'))
		->where("b.IdSubject = ?",$IdSubject);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	public function fndeletemappings($IdSubject) {
		$where = $this->lobjDbAdpt->quoteInto('IdSubject = ?', $IdSubject);
		$this->lobjDbAdpt->delete($this->_name, $where);
	}


}