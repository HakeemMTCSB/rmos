<?php

class Application_Model_DbTable_Programentry extends Zend_Db_Table_Abstract
{

    protected $_name = 'tbl_programentry';
    private $lobjDbAdpt;

    public function init()
    {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    /* public function fnViewProgramEntry($lintIdProgramEntry ) { // Function to view the Purchase Order details based on id
      $result = $this->fetchRow( "IdProgramEntry  = '$lintIdProgramEntry'") ;
      return $result->toArray();
      } */

    public function fnViewProgramEntryreq($Id)
    { // Function to view the Purchase Order details based on id
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a" => "tbl_programentryrequirement"))
            ->joinLeft(array('b' => 'tbl_qualificationmaster'), 'a.EntryLevel = b.IdQualification', array('b.QualificationLevel'))
            ->where("a.IdProgramEntryReq = ?", $Id);

        $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function fnViewProgramEntry($lintIdProgramEntry)
    { //Function to get the Program Branch details
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a" => "tbl_programentry"))
            ->where("a.IdProgramEntry = ?", $lintIdProgramEntry);
        //echo $lstrSelect;die();
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fngetProgramEntryDetails()
    { //Function to get the Program Branch details
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'tbl_programentry'))
            ->join(array('c' => 'tbl_scheme'), 'a.IdScheme  = c.IdScheme')
            ->join(array('ca' => 'tbl_scheme'), 'ca.IdScheme  = a.IdSchemeOld', array('SchemeOld' => 'ca.EnglishDescription'))
            ->joinLeft(array('b' => 'tbl_program'), 'a.IdProgram  = b.IdProgram')
            ->joinLeft(array('d' => 'tbl_program_scheme'), 'd.IdProgramScheme = a.IdProgramScheme', array())
            ->joinLeft(array('e' => 'registry_values'), 'e.id = d.mode_of_program', array('mode_of_program' => 'e.name'))
            ->joinLeft(array('f' => 'registry_values'), 'f.id = d.mode_of_study', array('mode_of_study' => 'f.name'))
            ->joinLeft(array('g' => 'registry_values'), 'g.id = d.program_type', array('program_type' => 'g.name'))
            //tmp
//            ->joinLeft(array('t' => 'registry_values'), 't.id = a.IdProgramScheme', array('program_scheme' => 't.name'))
            ->order("b.ProgramName");

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getProgramEntryDetails($IdProgramEntry)
    { //Function to get the Program Branch details
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_programentry'))
            ->joinLeft(array('b' => 'tbl_program'), 'a.IdProgram  = b.IdProgram')
            ->join(array('c' => 'tbl_scheme'), 'a.IdScheme  = c.IdScheme', array('SchemeName' => 'c.EnglishDescription'))
            ->where('a.IdProgramEntry = ?', $IdProgramEntry);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function fnSearchProgramEntry($post = array())
    { //Function for searching the user details
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $this->select()
            ->setIntegrityCheck(false)
            ->join(array('a' => 'tbl_programentry'), array('IdProgramEntry'))
            ->joinLeft(array('b' => 'tbl_program'), 'a.IdProgram  = b.IdProgram')
            ->join(array('c' => 'tbl_scheme'), 'c.IdScheme  = a.IdScheme')
            ->join(array('ca' => 'tbl_scheme'), 'ca.IdScheme  = a.IdSchemeOld', array('SchemeOld' => 'ca.EnglishDescription'))
            ->order("b.ProgramName");

        if ($post['field1']) {
            $select->where($db->quoteInto('a.IdScheme = ?', $post['field1']));
        }

        if ($post['IdProgram']) {
            $select->where($db->quoteInto('a.IdProgram = ?', $post['IdProgram']));
        }

        $select->joinLeft(array('d' => 'tbl_program_scheme'), 'd.IdProgramScheme = a.IdProgramScheme', array())
            ->joinLeft(array('e' => 'registry_values'), 'e.id = d.mode_of_program', array('mode_of_program' => 'e.name'))
            ->joinLeft(array('f' => 'registry_values'), 'f.id = d.mode_of_study', array('mode_of_study' => 'f.name'))
            ->joinLeft(array('g' => 'registry_values'), 'g.id = d.program_type', array('program_type' => 'g.name'));
        //tmp
//            ->joinLeft(array('t' => 'registry_values'), 't.id = a.IdProgramScheme', array('program_scheme' => 't.name'));

        $result = $this->fetchAll($select);
        return $result->toArray();
    }

    public function fnAddPOEntry($post)
    {
        $post = friendly_columns($this->_name, $post);

        $this->insert($post);
        $lastinsertid = $this->getAdapter()->lastInsertId();

        return $lastinsertid;
    }

    public function fnAddLearningModeDetails($IdProgramEntry, $larrformData)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        for ($i = 0; $i < count($larrformData['LearningMode']); $i++) {
            $lstrTable = "tbl_programentrylearningmode";
            $larrInsertData = array('IdProgramEntry' => $IdProgramEntry,
                                    'LearningMode'   => $larrformData["LearningMode"][$i],
            );
            $lobjDbAdpt->insert($lstrTable, $larrInsertData);
        }
    }

    //function to insert po details
    public function fnAddPOEntryDetails($larrprogramresult, $formData)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirement";

        if (!empty($formData['Itemgrid'])) {
            foreach ($formData['Itemgrid'] as $i => $grid) {
                $larrcourse = array('IdProgramEntry'   => $larrprogramresult,
                                    'GroupId'          => $formData['groupgrid'][$i],
                                    'EntryLevel'       => $formData['EntryLevelgrid'][$i],
                                    'IdSpecialization' => $formData['Specializationgrid'][$i],
                                    'Validity'         => $formData['Validitygrid'][$i],
                                    'Item'             => $formData['Itemgrid'][$i],
                                    'Condition'        => $formData['Conditiongrid'][$i],
                                    'Value'            => $formData['Valuegrid'][$i],
                                    'UpdDate'          => $formData['UpdDate'],
                                    'UpdUser'          => $formData['UpdUser']
                );
                $db->insert($table, $larrcourse);

            }
        }
    }

    public function fnAddOtherDetails($larrprogramresult, $formData)
    {  // function to insert po details
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirementotherdetails";
        if (!empty($formData['Itemothergrid'])) {
            foreach ($formData['Itemothergrid'] as $i => $itemothergrid) {
                $larrcourse = array('IdProgramEntry' => $larrprogramresult,
                                    'GroupId'        => $formData['groupothergrid'][$i],
                                    'Item'           => $formData['Itemothergrid'][$i],
                                    'Condition'      => $formData['Conditionothergrid'][$i],
                                    'Value'          => $formData['Valueothergrid'][$i],
                                    'UpdDate'        => $formData['UpdDate'],
                                    'UpdUser'        => $formData['UpdUser']
                );
                $db->insert($table, $larrcourse);
            }
        }
    }

    public function fnCheckExistingEntry($idprogram, $idlearningmode)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array('a' => 'tbl_programentry'), array('a.*'))
            ->join(array("b" => "tbl_programentrylearningmode"), 'a.IdProgramEntry = b.IdProgramEntry')
            ->where("a.IdProgram  = $idprogram")
            ->where("b.LearningMode  = $idlearningmode");
        $result = $db->fetchAll($sql);
        return $result->toArray();
    }

    public function fndeleteProgramEntryLearningmode($lintIdProgramEntry)
    { //Function for Delete Purchase order terms
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentrylearningmode";
        $where = $db->quoteInto("IdProgramEntry = $lintIdProgramEntry");
        $db->delete($table, $where);
    }

    public function fnGetProgramEditEntryDetails($lintIdProgramEntry)
    { // Function to edit Purchase order details
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->join(array('a' => 'tbl_programentryrequirement'), array('a.*'))
            ->join(array('q' => 'tbl_qualification_type'), 'q.id = a.EntryLevel', array('EntryLevelname' => 'q.qt_eng_desc', 'EntryLevelcode' => 'q.qt_id'))
            ->joinLeft(array('s' => 'tbl_studyfield'), 's.id = a.IdSpecialization', array('SpecializationName' => "CONCAT(sf_id,' - ',sf_eng_desc)"))
            ->joinLeft(array('b' => 'registry_values'), 'a.Item =b.id', array('b.name as Itemname'))
            ->joinLeft(array('d' => 'registry_values'), 'a.Condition=d.id', array('d.name as Conditionname', 'd.code as Conditioncode'))
            ->joinLeft(array('f' => 'registry_values'), 'a.GroupId=f.id', array('f.name as GroupName'))
            ->where("a.IdProgramEntry = '$lintIdProgramEntry'");
        $result = $this->fetchAll($select);
        return $result;
    }

    public function fnGetProgramEditOtherDetails($lintIdProgramEntry)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->join(array('a' => 'tbl_programentryrequirementotherdetails'), array('a.*'))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.Item =b.idDefinition', array('b.DefinitionDesc as Itemname'))
            ->joinLeft(array('d' => 'tbl_definationms'), 'a.Condition=d.idDefinition', array('d.DefinitionDesc as Conditionname'))
            ->joinLeft(array('f' => 'tbl_definationms'), 'a.GroupId=f.idDefinition', array('f.DefinitionDesc as GroupName'))
            ->where("a.IdProgramEntry = '$lintIdProgramEntry'");
        $result = $this->fetchAll($select);
        return $result;
    }

    public function fninserttempother($larrmainprogramresult, $idpoentery)
    {  // function to insert po details
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_tempprogramentryrequirementotherdetail";
        $sessionID = Zend_Session::getId();
        foreach ($larrmainprogramresult as $formData) {
            $larrtepprogrmentryreq = array(
                'Item'       => $formData['Item'],
                'Condition'  => $formData['Condition'],
                'Value'      => $formData['Value'],
                'GroupId'    => $formData['GroupId'],
                'UpdDate'    => $formData['UpdDate'],
                'UpdUser'    => $formData['UpdUser'],
                'unicode'    => $idpoentery,
                'Date'       => date("Y-m-d"),
                'sessionId'  => $sessionID,
                'idExists'   => $formData['IdProgramEntryReqOtherDetail'],
                'deleteFlag' => '1'
            );
            $db->insert($table, $larrtepprogrmentryreq);
        }

    }


    public function fninserttempprogramentryrequriments($larrmainprogramresult, $idpoentery)
    {  // function to insert po details
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_tempprogramentryrequirement";
        $sessionID = Zend_Session::getId();
        foreach ($larrmainprogramresult as $formData) {
            $larrtepprogrmentryreq = array(
                'Item'             => $formData['Item'],
                'Condition'        => $formData['Condition'],
                'Value'            => $formData['Value'],
                'Validity'         => $formData['Validity'],
                'EntryLevel'       => $formData['EntryLevel'],
                'IdSpecialization' => $formData['IdSpecialization'],
                'GroupId'          => $formData['GroupId'],
                'UpdDate'          => $formData['UpdDate'],
                'UpdUser'          => $formData['UpdUser'],
                'unicode'          => $idpoentery,
                'Date'             => date("Y-m-d"),
                'StartDate'        => $formData['StartDate'],
                'EndDate'          => $formData['EndDate'],
                'Description'      => $formData['Description'],
                'sessionId'        => $sessionID,
                'idExists'         => $formData['IdProgramEntryReq'],
                'deleteFlag'       => '1'
            );
            $db->insert($table, $larrtepprogrmentryreq);
        }
        //print_r($larrcourse);die();
    }

    public function fndeleteoldentries()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = 'tbl_tempprogramentryrequirement';
        $db->delete($table);
    }

    public function fnGetTempprogramentryReqDetails($lintIdProgramentry, $sessionID = null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->joinLeft(array('a' => 'tbl_tempprogramentryrequirement'), array('a.IdTemp'))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.Item =b.idDefinition', array('b.DefinitionDesc as Itemname'))
            ->joinLeft(array('d' => 'tbl_definationms'), 'a.Condition=d.idDefinition', array('d.DefinitionDesc as Conditionname'))
            ->joinLeft(array('e' => 'tbl_qualificationmaster'), 'a.EntryLevel=e.IdQualification', array('e.QualificationLevel as EntryLevelname'))
            ->joinLeft(array('f' => 'tbl_definationms'), 'a.GroupId=f.idDefinition', array('f.DefinitionDesc as GroupName'))
            ->joinLeft(array('g' => 'tbl_specialization'), 'a.IdSpecialization=g.IdSpecialization OR a.IdSpecialization = 0  ', array('g.Specialization as SpecializationName'))
            ->where('a.deleteFlag =1')
            ->where("a.unicode  = '$lintIdProgramentry'")
            ->group("a.IdTemp");
        if (!empty($sessionID)) {

            $select->where('a.sessionId =?', $sessionID);

        }
        $result = $this->fetchAll($select);
        $result1 = $result->toArray();
        if (count($result1) != 0) {
            $i = 0;
            foreach ($result1 as $res) {
                $select = $db->select()
                    ->from(array('a' => 'tbl_tempprogramentryrequirement'), array('a.IdTemp'))
                    ->join(array('b' => 'tbl_group_subject'), 'b.ProgramEntry = a.unicode', array('b.IdGroupSubject'))
                    ->where('a.unicode = ?', $res['unicode'])
                    ->where('b.GroupId = ?', $res['GroupId'])
                    ->where('b.IdQualification = ?', $res['EntryLevel'])
                    ->group('b.IdGroupSubject');
                if (!empty($sessionID)) {
                    $select->where('a.sessionId =?', $sessionID);
                }

                $delFlag = $db->fetchAll($select);
                if (count($delFlag) != 0) {
                    $result1[$i]['CasDelete'] = 1;
                } else {
                    $result1[$i]['CasDelete'] = 0;
                }
                $i++;
            }
        }
        return $result1;
    }


    public function fnGetprogramentryReqDetails($lintIdProgramentry)
    { // Function to edit Purchase order details
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->joinLeft(array('a' => 'tbl_programentryrequirement'), array('a.IdProgramEntryReq'))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.Item =b.idDefinition', array('b.DefinitionDesc as Itemname'))
            ->joinLeft(array('d' => 'tbl_definationms'), 'a.Condition=d.idDefinition', array('d.DefinitionDesc as Conditionname'))
            ->joinLeft(array('e' => 'tbl_qualificationmaster'), 'a.EntryLevel=e.IdQualification', array('e.QualificationLevel as EntryLevelname'))
            ->joinLeft(array('f' => 'tbl_definationms'), 'a.GroupId=f.idDefinition', array('f.DefinitionDesc as GroupName'))
            ->joinLeft(array('g' => 'tbl_specialization'), 'a.IdSpecialization=g.IdSpecialization', array('g.Specialization as SpecializationName'))
            //->where('a.deleteFlag =1')
            //todo unremove ^
            ->where("a.  IdProgramEntry  = '$lintIdProgramentry'");
        $result = $this->fetchAll($select);
        return $result;
    }


    public function fnGetProgramentryDetail($GroupId, $EntryLevel, $IdSpecialization, $Validity, $Item, $Condition, $Value, $IdProgramEntry = '', $sessionID)
    {
        $sql = $this->lobjDbAdpt->select()
            ->from(array('a' => 'tbl_tempprogramentryrequirement'), array('a.*'))
            ->where("a.GroupId  =?", $GroupId)
            ->where("a.EntryLevel =?", $EntryLevel)
            ->where("a.IdSpecialization =?", $IdSpecialization)
            ->where("a.Validity =?", $Validity)
            ->where("a.Item =?", $Item)
            ->where("a.Condition =?", $Condition)
            ->where("a.Value =?", $Value)
            ->where("a.sessionID =?", $sessionID);
        //->where("a.idExists =?",0);
        if ($IdProgramEntry != '') {
            $sql->where('a.unicode =?', $IdProgramEntry);
        }
        $result = $this->lobjDbAdpt->fetchAll($sql);
        return $result;
    }


    public function fnGetTempprogramentryOtherDetails($lintIdProgramentry, $sessionID = null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->joinLeft(array('a' => 'tbl_tempprogramentryrequirementotherdetail'), array('a.IdTemp'))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.Item =b.idDefinition', array('b.DefinitionDesc as Itemname'))
            ->joinLeft(array('d' => 'tbl_definationms'), 'a.Condition=d.idDefinition', array('d.DefinitionDesc as Conditionname'))
            ->joinLeft(array('f' => 'tbl_definationms'), 'a.GroupId=f.idDefinition', array('f.DefinitionDesc as GroupName'))
            ->where('a.deleteFlag = 1')
            ->where('a.unicode = ?', $lintIdProgramentry)
            ->where('a.sessionId =?', $sessionID)
            ->group("a.IdTemp");

        $result = $this->fetchAll($select);
        $result1 = $result->toArray();
        if (count($result1) != 0) {
            $i = 0;
            foreach ($result1 as $res) {
                $select = $db->select()
                    ->from(array('a' => 'tbl_tempprogramentryrequirementotherdetail'), array('a.IdTemp'))
                    ->where('a.sessionId =?', $sessionID)
                    ->where('a.GroupId = ?', $res['GroupId']);


                $delFlag = $db->fetchAll($select);
                if (count($delFlag) != 0) {
                    $result1[$i]['CasDelete'] = 1;
                } else {
                    $result1[$i]['CasDelete'] = 0;
                }
                $i++;
            }
        }

        return $result1;
    }

    public function fnGetTemOtherDetails($idprogramentry, $sessionID)
    { // Function to edit Purchase order details
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->join(array('a' => 'tbl_tempprogramentryrequirementotherdetail'), array('a.*'))
            ->where("a.unicode = '$idprogramentry'")
            ->where("a.sessionId = '$sessionID'");
        $result = $this->fetchAll($select);
        return $result;
    }

    public function fnUpdateTempEditOtherDetail($IdProgramEntry, $IdProgramEntryReqOtherDetail, $Item, $Condition, $Value, $upddate, $upduser, $GroupId)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirementotherdetails";
        $larridpo = array(
            'Item'      => $Item,
            'Condition' => $Condition,
            'Value'     => $Value,
            'GroupId'   => $GroupId,
            'UpdDate'   => $upddate,
            'UpdUser'   => $upduser);
        $where = "IdProgramEntryReqOtherDetail = '$IdProgramEntryReqOtherDetail'";
        $db->update($table, $larridpo, $where);
    }

    public function fnInsertNewOtherDetail(
        $IdProgramEntry, $IdProgramReq, $Item, $Condition, $Value, $upddate, $upduser, $GroupId)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirementotherdetails";
        $sessionID = Zend_Session::getId();
        $larridpo = array(
            'IdProgramEntry' => $IdProgramEntry,
            'Item'           => $Item,
            'Condition'      => $Condition,
            'Value'          => $Value,
            'GroupId'        => $GroupId,
            'UpdDate'        => $upddate,
            'UpdUser'        => $upduser,


        );
        $db->insert($table, $larridpo);
    }

    public function fnDeleteMainOtherDetail($IdProgramEntryReq)
    { //Function for Delete Purchase order terms
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_tempprogramentryrequirementotherdetail";
        $where = $db->quoteInto('IdProgramEntryReq = ?', $IdProgramEntryReq);
        $db->delete($table, $where);
    }

    public function fnUpdateMainProgramentryOtherDetails($transferedpodetails, $programentry)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirementotherdetails";
        $where = "IdProgramEntryReqOtherDetail = '$programentry'";
        $db->update($table, $transferedpodetails, $where);
    }

    public function fnInsertMainProgramOtherDetails($transferedinsertpodetails)
    {  // function to insert po details

        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirementotherdetails";
        $db->insert($table, $transferedinsertpodetails);
    }


    public function fnUpdateEditProgramentryreqDetails($IdProgramEntry, $IdProgramReq, $IdSpecialization, $Item, $EntryLevel, $Condition, $Value, $Validity, $upddate, $upduser, $GroupId)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirement";
        $larridpo = array(
            'IdProgramEntry'   => $IdProgramEntry,
            'Item'             => $Item,
            'IdSpecialization' => $IdSpecialization,
            'Condition'        => $Condition,
            'Value'            => $Value,
            'EntryLevel'       => $EntryLevel,
            'Validity'         => $Validity,
            'GroupId'          => $GroupId,
            'UpdDate'          => $upddate,
            'UpdUser'          => $upduser);
        $where = "IdProgramEntryReq = '$IdProgramReq'";
        $db->update($table, $larridpo, $where);
        return (true);
    }

    public function fnInsertNewProgramentryreqDetails($IdProgramEntry, $IdProgramReq, $IdSpecialization, $Item, $EntryLevel, $Condition, $Value, $Validity, $upddate, $upduser, $GroupId)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirement";
        $sessionID = Zend_Session::getId();
        $larridpo = array(
            'IdProgramEntry'   => $IdProgramEntry,
            'Item'             => $Item,
            'IdSpecialization' => $IdSpecialization,
            'Condition'        => $Condition,
            'Value'            => $Value,
            'Validity'         => $Validity,
            'EntryLevel'       => $EntryLevel,
            'GroupId'          => $GroupId,
            'UpdDate'          => date("Y-m-d"),
            'UpdUser'          => $upduser
        );
        $db->insert($table, $larridpo);
    }

    public function fnUpdateProgramentry($idprogramentry, $post)
    { // Function to view the Purchase Order details based on id
        $db = Zend_Db_Table::getDefaultAdapter();
        unset($post['Item']);
        unset($post['Desc']);
        unset($post['IdProgramReq']);
        unset($post['IdProgramEntry']);
        unset($post['Group']);
        unset($post['EntryLevel']);
        unset($post['IdSpecialization']);
        unset($post['Validity']);
        unset($post['Condition']);
        unset($post['Value']);

        unset($post['GroupOther']);
        unset($post['ItemOther']);
        unset($post['ConditionOther']);
        unset($post['ValueOther']);
        unset($post['Save']);


        $where = 'IdProgramEntry = ' . $idprogramentry;

        $db->update('tbl_programentry', $post, $where);


    }

    public function fnGetProgramentryreqTemDetails($idprogramentry, $sessionID)
    { // Function to edit Purchase order details
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->join(array('a' => 'tbl_tempprogramentryrequirement'), array('a.*'))
            ->where("a.unicode = '$idprogramentry'")
            ->where("a.sessionId = '$sessionID'");
        $result = $this->fetchAll($select);
        return $result;
    }

    public function fnviewLearningModeDetails($lintidProgram)
    { //Function to get the user details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_definationms"), array("key" => "a.idDefinition", "value" => "a.DefinitionDesc"))
            ->join(array('b' => 'tbl_programlearningmodedetails'), 'a.idDefinition = b.IdLearningMode')
            ->where("b.IdProgram = ?", $lintidProgram);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnUpdateMainProgramentryreqDetails($transferedpodetails, $programentry)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirement";
        $where = "IdProgramEntryReq = '$programentry'";
        $db->update($table, $transferedpodetails, $where);
    }

    public function fnInsertMainProgramreqDetails($transferedinsertpodetails)
    {  // function to insert po details
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirement";
        $db->insert($table, $transferedinsertpodetails);
    }

    public function fnDeleteTempProgramentryreqDetails($programentry, $sessionID)
    { //Function for Delete Purchase order terms
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_tempprogramentryrequirement";
        $where = $db->quoteInto('unicode = ?', $programentry);
        $where = $db->quoteInto('sessionId = ?', $sessionID);
        $db->delete($table, $where);
    }

    public function fngetAllentry($IdProgramEntry)
    {
        $sql = $this->lobjDbAdpt->select()
            ->from(array('a' => 'tbl_programentryrequirement'), array('a.*'))
            ->joinLeft(array('e' => 'tbl_qualification_type'), 'a.EntryLevel=e.id  ', array('e.qt_eng_desc as EntryLevelname'))
            ->joinLeft(array('c' => 'registry_values'), 'a.GroupId =c.id', array('c.name as Groupname'))
            ->joinLeft(array('b' => 'registry_values'), 'a.Item =b.id', array('b.name as Itemname'))
            ->joinLeft(array('d' => 'registry_values'), 'a.Condition=d.id', array('d.code as Conditionname'))
            ->joinLeft(array('g' => 'tbl_studyfield_type'), 'a.IdSpecialization=g.id', array('g.sft_eng_desc as SpecializationName'))
            ->where('IdProgramEntry = ?', $IdProgramEntry);
        $result = $this->lobjDbAdpt->fetchAll($sql);

        return $result;
    }

    public function fngetallsubjectentry($lintIdProgramEntry, $lintIdprogramentryreq = false)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array('a' => 'tbl_group_subject'), array('a.*'))
            ->join(array('b' => 'tbl_subject'), 'a.IdSubject = b.IdSubject OR a.IdSubject = 0', array("b.SubjectName"))
            ->joinLeft(array('c' => 'tbl_definationms'), 'a.fieldofstudy = c.idDefinition', array("DefinitionDesc AS fosn"))
            ->where('a.ProgramEntry = ?', $lintIdProgramEntry)
            ->group('a.IdGroupSubject');
        if ($lintIdprogramentryreq) {
            $sql->where('a.IdProgramentryreq = ?', $lintIdprogramentryreq);
        }
        return $result = $db->fetchAll($sql);
    }


    public function delete_academic_requirement($IdProgramEntryReq)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirement";
        $where = $db->quoteInto('IdProgramEntryReq = ?', $IdProgramEntryReq);
        $db->delete($table, $where);
        return true;
    }

    public function fnDeleteTempProgramentryDetailsBysession($sessionID)
    { //Function for Delete Purchase order terms
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_tempprogramentryrequirement";
        $where = $db->quoteInto('sessionId = ?', $sessionID);
        $db->delete($table, $where);
    }

    public function fnDeleteTempProgramentryOtherDetailsBysession($sessionID)
    { //Function for Delete Purchase order terms
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_tempprogramentryrequirementotherdetail";
        $where = $db->quoteInto('sessionId = ?', $sessionID);
        $db->delete($table, $where);
    }

    public function delete_other_details($IdProgramEntryReqOtherDetail)
    {  // function to update po details
        $db = Zend_Db_Table::getDefaultAdapter();

        $table = "tbl_programentryrequirementotherdetails";
        $where = $db->quoteInto('IdProgramEntryReqOtherDetail = ?', $IdProgramEntryReqOtherDetail);
        $db->delete($table, $where);
    }

    public function fnDeleteTempOtherDetails($programentry, $sessionID)
    { //Function for Delete Purchase order terms
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_tempprogramentryrequirementotherdetail";
        $where = $db->quoteInto('sessionId = ?', $sessionID);
        $db->delete($table, $where);
    }


    public function fnUpdateTempprogramentrydetails($lintidtemp)
    {  // function to update po details
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_tempprogramentryrequirement";
        $larramounts = array('deleteFlag' => '0');
        $where = "idTemp = '" . $lintidtemp . "'";
        $db->update($table, $larramounts, $where);
    }

    public function fndeletesubject($GroupId, $ProgramEntry, $IdSubject)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_group_subject";
        $larramounts = array('deleteFlag' => '0');
        $where = "GroupId = '" . $GroupId . "' AND ProgramEntry = '" . $ProgramEntry . "' AND IdSubject = '" . $IdSubject . "'";
        $db->delete($table, $where);
        return;
    }

    public function fnCheckExistingItem($EntryLevel, $GroupId, $sessionID)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array('a' => 'tbl_tempprogramentryrequirement'), array('a.*'))
            ->where("a.EntryLevel = $EntryLevel")
            ->where("a.GroupId = $GroupId")
            ->where("a.deleteFlag != 0")
            ->where("a.sessionId = ?", $sessionID);

        $result = $db->fetchRow($sql);
        return $result;
    }

    public function fnCheckCurrentItem($EntryLevel, $GroupId, $IdProgramEntry)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array('a' => 'tbl_programentryrequirement'), array('a.*'))
            ->where("a.EntryLevel = $EntryLevel")
            ->where("a.GroupId = $GroupId")
            ->where("a.IdProgramEntry = ?", $IdProgramEntry);

        $result = $db->fetchRow($sql);

        return $result;
    }

    public function fnCheckExistingOtherItem($ItemOther, $GroupId, $sessionID)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array('a' => 'tbl_tempprogramentryrequirementotherdetail'), array('a.*'))
            ->where("a.Item = $ItemOther")
            ->where("a.GroupId = $GroupId")
            ->where("a.deleteFlag != 0")
            ->where("a.sessionId = ?", $sessionID);

        $result = $db->fetchRow($sql);
        return $result;
    }

    public function fnCheckCurrentOtherItem($ItemOther, $GroupId, $Condition, $IdProgramEntry)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array('a' => 'tbl_programentryrequirementotherdetails'), array('a.*'))
            ->where("a.Item = $ItemOther")
            ->where("a.GroupId = $GroupId")
            ->where("a.Condition = $Condition")
            ->where("a.IdProgramEntry = ?", $IdProgramEntry);

        $result = $db->fetchRow($sql);

        return $result;
    }

    public function fnDeleteMainProgramreqDetails($IdProgramEntryReq)
    { //Function for Delete Purchase order terms
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirement";
        $where = $db->quoteInto('IdProgramEntryReq = ?', $IdProgramEntryReq);
        $db->delete($table, $where);
    }

    public function fngetprogramentry()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_definationms"), array("key" => "a.idDefinition", "value" => "a.DefinitionDesc"))
            ->join(array('b' => 'tbl_programentryrequirement'), 'a.idDefinition = b.Item');
        //->where("b.IdProgram = ?",$lintidProgram);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * List Program Entry for Dropdown (select)
     * Date: 9/24/2013
     */
    public function fngetprogramentrylist()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_programentry'), array('key' => 'a.IdProgramEntry'))
            ->join(array('b' => 'tbl_program'), 'a.IdProgram  = b.IdProgram', array("CONCAT_WS(' - ',b.ProgramName,b.IdProgram) as value"))
            ->order("b.ProgramName");

        $result = $db->fetchAll($select);

        return $result;

        //return $result->toArray();

    }

    /*
     * Copy Program Entry
     * Date: 9/24/2013
     */
    public function fncopyprogramentry($source_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_programentry'), array('a.*'))
            ->where('a.IdProgramEntry = ?', $source_id);

        $programentry = $db->fetchRow($select);

        if (empty($programentry)) return false;

        //copy
        $data = $programentry;
        $auth = Zend_Auth::getInstance();
        $user_id = $auth->getIdentity()->iduser;
        unset($data['IdProgramEntry']);
        $data['UpdDate'] = date('Y-m-d H:i:s');

        $db->insert('tbl_programentry', $data);

        $id = $db->lastInsertId();

        return $id;
    }

    public function fnAddSubjectGroup($data)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_group_subject";
        $db->insert($table, $data);
    }

    public function fngetgroupsubject($groupId, $IdprogramEntry, $IdQualification, $Idpromentryreq = false)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array('a' => 'tbl_group_subject'), array('a.*', 'a.IdSubject AS SubjectId'))
            ->join(array('b' => 'tbl_subject'), 'a.IdSubject = b.IdSubject OR a.IdSubject = 0')
            ->join(array('c' => 'tbl_qualificationmaster'), 'a.IdQualification = c.IdQualification', array('c.QualificationLevel'))
            ->join(array('d' => 'tbl_definationms'), 'a.GroupId = d.idDefinition', array('d.DefinitionCode AS GroupName'))
            ->join(array('e' => 'tbl_definationms'), 'a.fieldofstudy = e.idDefinition OR a.fieldofstudy = 0', array('e.DefinitionDesc AS FieldofstudyName'))
            ->where("a.ProgramEntry = $IdprogramEntry")
            ->where("a.GroupId = '" . $groupId . "'")
            ->where("a.IdQualification ='" . $IdQualification . "'")
            ->group("IdGroupSubject");
        if ($Idpromentryreq) {
            $sql->where("a.IdProgramentryreq = '" . $Idpromentryreq . "'");
        }
        $result = $db->fetchAll($sql);
        return $result;
    }

    public function fndeleteSubjectGroup($groupId, $IdprogramEntry, $Idqualification, $lIntprogramentryreq = false)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_group_subject";
        $where = $db->quoteInto('ProgramEntry = ?', $IdprogramEntry);
        $where .= ' AND ' . $db->quoteInto('GroupId = ?', $groupId);
        $where .= ' AND ' . $db->quoteInto('IdQualification = ?', $Idqualification);
        if ($lIntprogramentryreq) {
            $where .= ' AND ' . $db->quoteInto('IdProgramentryreq = ?', $lIntprogramentryreq);
        }
        //$where = $db->quoteInto('IdSubject != ?',0);
        $db->delete($table, $where);
    }

    // Function to delete all subject of group of particular program entry
    public function fndeleteSubjectforGroup($groupId, $IdprogramEntry, $programentryreq)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_group_subject";
        $where = $db->quoteInto('ProgramEntry = ?', $IdprogramEntry);
        $where .= ' AND ' . $db->quoteInto('GroupId = ?', $groupId);
        $where .= ' AND ' . $db->quoteInto('IdProgramentryreq = ?', $programentryreq);
        $db->delete($table, $where);
    }

    public function find_existing_program_entry($IdProgram, $StartDate, $EndDate)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $raw_sql = "SELECT * FROM " . $this->_name . "
      WHERE
       IdProgram = " . (int)$IdProgram . "
       AND
      (
        (
        " . $this->_name . ".StartDate between '" . $StartDate . "'  and '" . $EndDate . "'
        OR
        " . $this->_name . ".EndDate between '" . $StartDate . "'  and '" . $EndDate . "'
        )
      OR
        (
          " . $this->_name . ".StartDate >= '" . $StartDate . "'
          AND
          " . $this->_name . ".EndDate <= '" . $EndDate . "'
        )
      OR
        (
          " . $this->_name . ".StartDate <= '" . $StartDate . "'
          AND
          " . $this->_name . ".EndDate >= '" . $EndDate . "'
        )
      )";

        $stmt = $db->query($raw_sql);

        //$select = $db->query($raw_sql);
        $lists = $stmt->fetchAll();

        return ($lists);

    }

    public function addAcademicRequirement($formData)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_programentryrequirement', $formData);
        return $lastinsertid = $this->getAdapter()->lastInsertId();

    }

    public function updateAcademicRequirement($data, $id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = "tbl_programentryrequirement";
        $where = "IdProgramEntryReq = '$id'";
        $db->update($table, $data, $where);
    }

    public function addAcademicRequirementSubject($formData)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_programentryrequirement_subject', $formData);
        return $lastinsertid = $this->getAdapter()->lastInsertId();

    }

    public function getAcademicRequirementsSql($id, $openEntry = 0)
    {
        $select = $this->lobjDbAdpt->select()
            ->from(array("a" => "tbl_programentryrequirement"))
            ->joinLeft(array('q' => 'tbl_qualification_type'), 'q.id = a.EntryLevel', array('EntryLevelname' => 'q.qt_eng_desc', 'EntryLevelcode' => 'q.qt_id'))
            ->joinLeft(array('s' => 'tbl_studyfield'), 's.id = a.IdSpecialization', array('SpecializationName' => "CONCAT(sf_id,' - ',sf_eng_desc)"))
            ->joinLeft(array('b' => 'registry_values'), 'a.Item =b.id', array('b.name as Itemname'))
            ->joinLeft(array('d' => 'registry_values'), 'a.Condition=d.id', array('d.name as Conditionname', 'd.code as Conditioncode'))
            ->joinLeft(array('f' => 'registry_values'), 'a.GroupId=f.id', array('f.name as GroupName'))
            ->where("a.IdProgramEntry = ?", $id)
            ->where("a.open_entry = ?", $openEntry);

        return $select;
    }

    public function getAcademicRequirements($id, $openEntry = 0)
    {

        $selectMain = $this->getAcademicRequirementsSql($id, $openEntry)
            ->where("((a.pid = 0 AND Mandatory = 1) OR a.pid = 0)")
//            ->where('a.pid = ? ',0)
//            ->where("a.Mandatory = ?", 1) //TBC ni sbb nk amik yg btl2 ada group C tu je
        ;

        $result = array();
        $resultMain = $this->lobjDbAdpt->fetchAll($selectMain);

        if ($resultMain) {
            $i = 0;
            foreach ($resultMain as $main) {

                $result[$i] = $main;
                $result[$i]['subject'] = $this->getAcademicSubject($main['IdProgramEntryReq']);
                $i++;

                $selectSub = $this->getAcademicRequirementsSql($id, $openEntry)
                    ->where('a.pid = ?', $main['IdProgramEntryReq']);

                $resultSub = $this->lobjDbAdpt->fetchAll($selectSub);

                foreach ($resultSub as $sub) {
                    $result[$i] = $sub;
                    $result[$i]['subject'] = $this->getAcademicSubject($sub['IdProgramEntryReq']);
                    $i++;
                }
            }
        }

        return $result;
    }

    public function getAcademicSubject($id)
    {

        $select = $this->lobjDbAdpt->select()
            ->from(array("a" => "tbl_programentryrequirement_subject"))
            ->joinLeft(array('s' => 'school_subject'), 's.ss_id = a.IdSubject', array('ss_subject', 'ss_subject_code', 'ss_subject_bahasa'))
            ->joinLeft(array('g' => 'tbl_subjectgrade'), 'g.id = a.IdGrade', array('sg_grade', 'sg_grade_desc', 'sg_grade_point'))
            ->where("a.IdProgramEntryReq = ?", $id);

        $result = $this->lobjDbAdpt->fetchAll($select);
        return $result;
    }


    public function getProgramEntryList($schemeId, $programId, $programSchemeId = 0, $openEntry = 0)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('ab' => 'tbl_programentryrequirement'))
            ->joinLeft(array('q' => 'tbl_qualification_type'), 'q.id = ab.EntryLevel')
            ->join(array('a' => 'tbl_programentry'), 'ab.IdProgramEntry = a.IdProgramEntry', array('IdProgramEntry'))
            ->join(array('c' => 'tbl_scheme'), 'c.IdScheme  = a.IdScheme', array())
            ->joinLeft(array('b' => 'tbl_program'), 'a.IdProgram  = b.IdProgram', array())
            ->joinLeft(array('d' => 'tbl_program_scheme'), 'd.IdProgramScheme = a.IdProgramScheme', array())
            ->joinLeft(array('r' => 'tbl_programentryrequirement_subject'), 'r.IdProgramEntryReq = ab.IdProgramEntryReq', array())
            ->joinLeft(array('s' => 'school_subject'), 's.ss_id = r.IdSubject', array('subjects' => "GROUP_CONCAT(sg_grade_desc ,' in ', CASE WHEN ss_subject IS NULL THEN concat('ANY ',subject_no,' subjects') ELSE ss_subject END separator ', ')", 'subjectsMalay' => "GROUP_CONCAT(sg_grade_desc ,' dalam ', CASE WHEN ss_subject_bahasa IS NULL THEN concat('MANA-MANA ',subject_no,' subjek') ELSE ss_subject_bahasa END separator ', ')"))
            ->joinLeft(array('g' => 'tbl_subjectgrade'), 'g.id = r.IdGrade', array('sg_grade', 'sg_grade_desc', 'sg_grade_point'))
            //            ->joinLeft(array('e' => 'registry_values'), 'e.id = d.mode_of_program', array('mode_of_program' => 'e.name'))
//            ->joinLeft(array('f' => 'registry_values'), 'f.id = d.mode_of_study', array('mode_of_study' => 'f.name'))
//            ->joinLeft(array('g' => 'registry_values'), 'g.id = d.program_type', array('program_type' => 'g.name'))
//            ->where('a.IdScheme = ? ', $schemeId) //todo tmp
            ->where('a.IdProgram = ? ', $programId)
            ->where('ab.open_entry = ? ', $openEntry)
            ->where('ab.pid = ? ', 0)
            ->order('ab.pid desc')
            ->group('ab.IdProgramEntryReq');

        if ($programSchemeId) {
            $select->where("a.IdProgramScheme = $programSchemeId OR a.IdProgramScheme = 0");
        }

        $result = $db->fetchAll($select);
        return $result;
    }


    public function getSubProgramEntryList($pid, $transaction_id = 0)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('ab' => 'tbl_programentryrequirement'))
            ->join(array('q' => 'tbl_qualification_type'), 'q.id = ab.EntryLevel')
            ->join(array('a' => 'tbl_programentry'), 'ab.IdProgramEntry = a.IdProgramEntry', array('IdProgramEntry'))
            ->joinLeft(array('r' => 'tbl_programentryrequirement_subject'), 'r.IdProgramEntryReq = ab.IdProgramEntryReq', array())
            ->joinLeft(array('s' => 'school_subject'), 's.ss_id = r.IdSubject', array('subjects' => "GROUP_CONCAT(sg_grade_desc ,' in ', CASE WHEN ss_subject IS NULL THEN concat('ANY ',subject_no,' subjects') ELSE ss_subject END separator ', ')", 'subjectsMalay' => "GROUP_CONCAT(sg_grade_desc ,' dalam ', CASE WHEN ss_subject_bahasa IS NULL THEN concat('MANA-MANA ',subject_no,' subjek') ELSE ss_subject_bahasa END separator ', ')"))
            ->joinLeft(array('g' => 'tbl_subjectgrade'), 'g.id = r.IdGrade', array('sg_grade', 'sg_grade_desc', 'sg_grade_point'))
            ->joinLeft(array('sf' => 'tbl_studyfield'), 'sf.id = ab.IdSpecialization', array('sf_eng_desc', 'sf_mal_desc'))
            ->where('ab.pid = ? ', $pid)
            ->order('ab.IdProgramEntryReq')
            ->group('ab.IdProgramEntryReq');

        if ($transaction_id) {
            $select->joinLeft(array('ae' => 'applicant_entry_req'), 'ae.entry_id = ab.IdProgramEntryReq', array('status'))
                ->where('ae.ae_trans_id = ? ', $transaction_id);
        }
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getProgramEntryListById($id)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('ab' => 'tbl_programentryrequirement'))
            ->joinLeft(array('q' => 'tbl_qualification_type'), 'q.id = ab.EntryLevel')
            ->join(array('a' => 'tbl_programentry'), 'ab.IdProgramEntry = a.IdProgramEntry', array('IdProgramEntry'))
            ->where('ab.IdProgramEntry = ? ', $id)
            ->order('ab.IdProgramEntryReq');

        $result = $db->fetchAll($select);
        return $result;
    }

}

?>
