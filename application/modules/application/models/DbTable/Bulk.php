<?php
class Application_Model_DbTable_Bulk extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_bulk_history';
	protected $_item = 'applicant_bulk_history_item';
	protected $_primary = "b_id";
		
	public function getData($id=0){
		$id = (int)$id;
		
		if($id!=0){
			$row = $this->fetchRow($this->_primary .' = '. $id);
		}else{
			$row = $this->fetchAll();
		}
		
		if(!$row){
			throw new Exception("Invalid Bulk ID");
		}
		
		return $row->toArray();
	}
	
	public function getPaginateData(){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
							->from($this->_name)
							->order('b_id DESC');
		
		return $select;
	}
	
	public function addData($data){
		
		return $this->insert($data);
	}

	
	public function getItems($bulkid)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
							->from($this->_item)
							->where('b_id = ?', $bulkid);
		
		$results = $db->fetchAll($select);
		
		return $results;
	}

	public function addItem($data){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->insert($this->_item, $data);
	}
	
	public function updateData($data,$id){
		
			
		$this->update($data,  'b_id= '.$id);
	}

}

