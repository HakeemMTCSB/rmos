<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_ApplicantEligible extends Zend_Db_Table_Abstract {
    //put your code here
    protected $_name = 'applicant_eligible';
    protected $_primary = "id";
    
    /*
     * list eligble applicant
     * 
     * @on 18/7/2014
     */
    public function getEligibleApplicantList(){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_eligible"),array("value"=>"a.*", "key"=>"a.id"))
            ->joinLeft(array('b'=>'tbl_program'), 'a.program = b.IdProgram')
            ->joinLeft(array('c'=>'tbl_qualificationmaster'), 'a.qualification = c.IdQualification')
            ->order("a.id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * list eligble applicant by searching
     * 
     * @on 18/7/2014
     */
    public function getEligibleApplicantSearch($formData){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_eligible"),array("value"=>"a.*", "key"=>"a.id"))
            ->joinLeft(array('b'=>'tbl_program'), 'a.program = b.IdProgram')
            ->joinLeft(array('c'=>'tbl_qualificationmaster'), 'a.qualification = c.IdQualification')
            ->order("a.id");
        
        if(isset($formData)){

            if (isset($formData['IdProgram']) && $formData['IdProgram']!=''){
                $lstrSelect->where("a.program = ?",$formData['IdProgram']);
            }
            if (isset($formData['status']) && $formData['status']!=''){
                $lstrSelect->where('a.status = ?',$formData['status']);
            }
        }
        
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
}
?>