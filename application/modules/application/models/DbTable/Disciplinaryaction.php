<?php
	class Application_Model_DbTable_Disciplinaryaction extends Zend_Db_Table {
		
		//Function To Get Email Address For Respective Disciplinary Action Type Id
		public function fnGetDisciplinaryActionTypeEmailByDisciplinaryActionTypeId($IdDisciplinaryActionType){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_disciplinaryactionmaster"),array("a.DiscMsgCollegeHead","a.DiscMsgParent","a.DiscMsgSponor","a.DiscMsgRegistar") )
       								->where("a.idDisciplinaryActionType = ?",$IdDisciplinaryActionType);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
		
		//Function To Get Email Sent For Respective Disciplinary Action Type Id
		public function fnGetDisciplinaryActionTypeEmailSentDetails($IdDisciplinaryAction){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_disciplinaryactionmailsentto"))
       								->where("a.IdDisciplinaryAction = ?",$IdDisciplinaryAction);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
		
		public function fnGetDisciplinaryActionTypeSubjectName($IdStudentRegistration)
		{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       							->from(array("a"=>"tbl_disciplinaryactionmailsentto"))
       							->where("a.IdDisciplinaryAction = ?",$IdDisciplinaryAction);
	    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
		
		}
		
		//Function To Get Max No Of Warnings For The Student
		public function fnGetMaxNoOfWarnings($idUniversity){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a" => "tbl_config"),array("a.NoofWarnings"))
    	   							->where("a.idUniversity = ?",$idUniversity);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult['NoofWarnings'];
		}
		
		//Function To Save The Terminated Student Hostel Bulk Details
		public function fnSaveStudentHostelBulkDetails($IdHistory,$IdAllotment){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrTable = "tbl_hostelbulkallotmenthistory";
			
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a" => "tbl_hostelbulkbeds"),array("a.idbed"))
    	   							->where("a.IDAllotment = ?",$IdAllotment);
			$PreviousData = $lobjDbAdpt->fetchRow($lstrSelect);
			
			for($lintI=0;$lintI<count($PreviousData);$lintI++){
				if($PreviousData[$lintI]["idbed"] != ""){
					$larrPreviousData = array(
										'idHistory'=>$IdHistory,
										'idBed'=>$PreviousData[$lintI]["idbed"],
										'UpdUser'=>$PreviousData["UpdUser"],
										'UpdDate'=>date("Y-m-d h:m:s")
									);
					$lobjDbAdpt->insert($lstrTable,$larrPreviousData);
				}
			}
		}
		
		//Function To Save The Terminated Student Hostel Details
		public function fnSaveStudentHostelDetails($IdStudent){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a" => "tbl_hostleallotment"))
    	   							->where("a.IDApplication = ?",$IdStudent);
			$PreviousData = $lobjDbAdpt->fetchRow($lstrSelect);
			
			$lstrTable = "tbl_hostelallotmenthistory";
			$larrPreviousData = array(
										'IDApplication'=>$IdStudent,
										'AllotmentType'=>$PreviousData["AllotmentType"],
										'idHostel'=>$PreviousData["idHostel"],
										'idBlock'=>$PreviousData["idBlock"],
										'idFloor'=>$PreviousData["FloorAlloted"],
										'idRoom'=>$PreviousData["idroom"],
										'BedAllotmentType'=>$PreviousData["BedAllotmentType"],
										'idBed'=>$PreviousData["BedAlloted"],
										'FeesPaidAmt'=>$PreviousData["FeesPaidAmt"],
										'AllotedFrom'=>$PreviousData["AllotedFrom"],
										'AllotedTo'=>$PreviousData["AllotedTo"],
										'UpdUser'=>$PreviousData["UpdUser"],
										'UpdDate'=>date("Y-m-d h:m:s")
									);
			$lobjDbAdpt->insert($lstrTable,$larrPreviousData);
			$lintIdHistory = $lobjDbAdpt->lastInsertId();
			self::fnSaveStudentHostelBulkDetails($lintIdHistory,$PreviousData["IDAllotment"]);
		}
		
		//Function To Terminate Student From The Hostel
		public function fnTerminateStudent($idStudent){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();			
			$UpdateData["Termination"] = "1";
			$lstrTable = "tbl_studentapplication";
			$lstrWhere = "IDApplication = ".$idStudent;
			$lstrMsg = $lobjDbAdpt->update($lstrTable,$UpdateData,$lstrWhere);
			return $lstrMsg;
		}
		
		//Function To Deactive The Student's Application
		public function fnDeactivateStudentapplication($idStudent){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$UpdateData["Termination"] = "1";
			$lstrTable = "tbl_studentapplication";
			$lstrWhere = "IDApplication = ".$idStudent;
			$lstrMsg = $lobjDbAdpt->update($lstrTable,$UpdateData,$lstrWhere);
			return $lstrMsg;
		}
		//Function To Count No Of Warnings Given To The Student
		public function fnGetNoOfWarnings($idStudent){
			$lintWarningCount = "";
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_disciplinaryaction"),array("a.IdDisciplinaryAction") )
					 				->where("a.IdApplication = ?",$idStudent)
					 				->where("a.WarningIssued = ?","1");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			$lintWarningCount = count($larrResult);
			return $lintWarningCount;
		}
		//Get Disciplinary Email Template
		
		
		//Get Definition Type Id For Curricular Activity
		public function fnGetDefinitionTypeId(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_definationtypems"),array("a.idDefType") )
					 				->where("a.defTypeDesc LIKE 'Disciplinary Action%'");
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult['idDefType'];
		}
		
		//Get Student Name
		public function fnGetStudentName($IdStudent){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a" => "tbl_studentapplication"),array("CONCAT(a.FName,' ', IFNULL(a.MName,' '),' ',IFNULL(a.LName,' ')) AS StudentName"))
    	   							->where("a.IDApplication = ?",$IdStudent)    	   							
    	   							->where("a.Active = 1");
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult['StudentName'];
		}
		
		//Get Student Id
		public function fnGetStudentId($IdStudent){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a" => "tbl_studentapplication"),array("a.StudentId"))
    	   							->where("a.IDApplication = ?",$IdStudent)
    	   							->where("a.Termination = 0")
    	   							->where("a.Active = 1");
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult['StudentId'];
		}
		
		//Get SMTP Settings Details
		public function fnGetSMTPSettings(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
			$idUniversity = $this->gstrsessionSIS->idUniversity;
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_config"),array("a.SMTPServer","a.SMTPUsername","a.SMTPPassword","a.SMTPPort","a.SSL","a.DefaultEmail") )
       								->where("a.idUniversity = ?",$idUniversity);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
		
		//Get List of Person To Send Disciplinary Msgs
		public function fnGetDisciplinaryMsgingPersonList(){
			$lobjsession = Zend_Registry::get('sam'); 
			$lintHostelId = $lobjsession->hostel;
			
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_sam_config"),array("a.DiscMsgWarden","a.DiscMsgCollegeHead","a.DiscMsgParent","a.DiscMsgSponor") )
       								->where("a.idHostel = ?",$lintHostelId);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
		
		//Get List of Person To Send Warning Msgs
		public function fnGetWarningMsgingPersonList(){
			$lobjsession = Zend_Registry::get('sam'); 
			$lintHostelId = $lobjsession->hostel;
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_sam_config"),array("a.WarnMsgWarden","a.WarnMsgCollegeHead","a.WarnMsgParent","a.WarnMsgSponor") )
       								->where("a.idHostel = ?",$lintHostelId);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
		
		//Get Warden Details
		public function fnGetWardenDetails($lintStudentId,$lintIdHostel){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_wardenmaster"),array("a.Email","CONCAT(a.fName,' ', IFNULL(a.mName,' '),' ',IFNULL(a.lName,' ')) AS WardenName") );
       								//->join(array("b"=>"tbl_studentapplication"),'a.idWarden = b.idDefType');
       								//->where("a.idHostel = ?",$lintIdHostel);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
		
		// Get College Head Details
		public function fnGetCollegeHeadDetails($lintStudentId){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_studentapplication"),array("") )
       								->join(array("b"=>"tbl_deanlist"),"a.idCollege=b.IdCollege",array("b.IdDeanList"))
       								->join(array("c"=>"tbl_staffmaster" ),"c.IdStaff=b.IdStaff",array("c.IdStaff","c.FirstName","c.Email"))
       								->where("a.IdApplication = ?",$lintStudentId)
       								->order("b.IdDeanList DESC");     								
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);			
			return $larrResult;
		}
		
		//Get Parent Details
		public function fnGetParentDetails($lintStudentId){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();			
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_studentapplication"),array("a.PEmail","a.NameOfFather") )
       								->where("a.IdApplication = ?",$lintStudentId);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
		
		//Get Sponsor Details
		public function fnGetSponsorDetails($lintStudentId){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									->from(array("a"=>"tbl_studentapplication"),array())
       								->join(array("b"=>"tbl_sponsor"),"a.idsponsor=b.idsponsor",array("b.Email","CONCAT(b.fName,' ', IFNULL(b.mName,' '),' ',IFNULL(b.lName,' ')) AS SponsorName") )
       								->where("a.IdApplication = ?",$lintStudentId);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
		
		//Get Students Ids List
		public function fnGetListOfStudentIds(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.IDApplication","value"=>"a.StudentId") )
       								->where("a.Termination = 0")
       								->where("a.Active = 1");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
		//Get Students List
		public function fnGetStudentsList(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.IDApplication","value"=>"CONCAT(a.FName,' ', IFNULL(a.MName,' '),' ',IFNULL(a.LName,' '))") )
       								->where("a.Termination = 0")
       								->where("a.Registered = 1")
       								->where("a.Active = 1")
       								->order("a.FName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		//Get Disciplinary Action List
		public function fnGetDisciplinaryActionTypeList(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc") )
					 				->join(array("b"=>"tbl_definationtypems"),'a.idDefType = b.idDefType')
					  				->where("b.defTypeDesc='Disciplinary Actions'")
					  				->order("a.DefinitionDesc");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
		//Get Details of Students Disciplinary Action
		public function fnGetStudentDisciplinaryActionDetails(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_disciplinaryaction"))
       								->join(array("b" => "tbl_studentapplication"),"a.IdApplication = b.IDApplication",array("CONCAT(b.FName,' ', IFNULL(b.MName,' '),' ',IFNULL(b.LName,' ')) AS StudentName","b.StudentId","b.IDApplication"))
       								->join(array("c" => "tbl_definationms"),"a.IdDispciplinaryActionType = c.idDefinition",array("c.DefinitionDesc AS DisciplinaryDesc"))
       								->where("b.Termination = 0")       								
       								->where("b.Active = 1");

			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
		//Search On Student Disciplinary Action Details
		public function fnSearchDsiciplinaryActionDetails($postData){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();			
			if($postData['field3'] == "" && $postData['field2'] == "" && $postData['field1'] == ""   ){
				$larrResult = self::fnGetStudentDisciplinaryActionDetails();
			}else{	
				$lstrSelect = $lobjDbAdpt->select()
    	   								->from(array("a"=>"tbl_disciplinaryaction"))
       									->join(array("b" => "tbl_studentapplication"),"a.IDApplication = b.IDApplication",array("CONCAT(b.FName,' ', IFNULL(b.MName,' '),' ',IFNULL(b.LName,' ')) AS StudentName","b.StudentId","b.IDApplication"))
       									->join(array("c" => "tbl_definationms"),"a.IdDispciplinaryActionType = c.idDefinition",array("c.DefinitionDesc AS DisciplinaryDesc"))
       									->where('b.FName  like "%" ? "%"',$postData['field3'])
			   							->where('b.StudentId like  "%" ? "%"',$postData['field2'])
       									->where("b.Termination = 0")
       									->where("b.Active = 1");
       									
       				if($postData['field1'] !="") {
			   			$lstrSelect->where('c.idDefinition = '.$postData['field1']);
						
       				}					
       									
       									
       									
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			}
			return $larrResult;
		}
		
		//Function To Get Email Address For Respective Disciplinary action Type
		public function fnGetDisciplinaryActionTypeEmail($IdDisciplinaryActionType){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_disciplinaryactionmaster"),array("a.DiscMsgWarden","a.DiscMsgCollegeHead","a.DiscMsgParent","a.DiscMsgSponor") );
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
		
		//Get Details of Students Disciplinary Action By Disciplinary Id
		public function fnGetStudentDisciplinaryActionDetailsById($idDisciplinary){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_disciplinaryaction"))
       								->join(array("b" => "tbl_studentapplication"),"a.IDApplication = b.IDApplication",array("CONCAT(b.FName,' ', IFNULL(b.MName,' '),' ',IFNULL(b.LName,' ')) AS StudentName","b.IDApplication"))
       								->join(array("c" => "tbl_definationms"),"a.IdDispciplinaryActionType = c.idDefinition",array("c.DefinitionDesc AS DisciplinaryDesc"))
       								->join(array("d" => "tbl_disciplinaryactionmailsentto"),"a.IdDisciplinaryAction  = d.IdDisciplinaryAction")
       								->where("a.IdDisciplinaryAction = ?",$idDisciplinary)
       								->where("b.Termination = 0")
       								->where("b.Active = 1");
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);			
			return $larrResult;
		}
		
		//Function To Save & Update Disciplinary Action Mail Sent Details
		public function fnSaveDisciplinaryActionEmailSentDetails($EmailList,$IdDisciplinaryAction){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$auth = Zend_Auth::getInstance();	
			$lintIdUser = $auth->getIdentity()->iduser;
			//$lobjSAMSession = Zend_Registry::get ( 'sam' );
			//$lintIdUser = $lobjSAMSession->primaryuserid;
			
			$lstrTable = "tbl_disciplinaryactionmailsentto";
			$lstrWhere = "IdDisciplinaryAction = ".$IdDisciplinaryAction;
			$lobjDbAdpt->delete($lstrTable,$lstrWhere);
			
			$larrNewFormData = array(
										"IdDisciplinaryAction"=>$IdDisciplinaryAction,										
										"DiscMsgCollegeHead"=>$EmailList["DiscMsgCollegeHead"],
										"DiscMsgParent"=>$EmailList["DiscMsgParent"],
										"DiscMsgSponor"=>$EmailList["DiscMsgSponor"],
										"DiscMsgRegistrar"=>$EmailList["DiscMsgRegistrar"],
										"UpdDate"=>date("Y-m-d H:i:s"),
										"UpdUser"=>$lintIdUser
									);
			$lobjDbAdpt->insert($lstrTable,$larrNewFormData);
		}
		
		
		//Function To Save Student's Disciplinary Action
		public function fnSaveDisciplinaryActionDetails($post,$filename,$larrFormData){

			unset($post['idSubject']);
			unset($post['idSemester']);
			unset($post['DisplinaryAction']);
			unset($post['Add']);
			unset($post['Clear']);

			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
			
			$lstrMsg = "";
			$lstrTable = "tbl_disciplinaryaction";
			$larrEmailList = array();
			
			$lstrStudentName = self::fnGetStudentName($post['IdStudent']);
			$post['IdApplication'] = $post['IdStudent'];
			$post['DateOfMistake'] = $post['DateOfMistake'];
			$post['ReportingDate'] = $post['ReportingDate'];			
			$larrEmailList["DiscMsgCollegeHead"] = $post['DiscMsgCollegeHead'];
			$larrEmailList["DiscMsgParent"] = $post['DiscMsgParent'];
			$larrEmailList["DiscMsgSponor"] = $post['DiscMsgSponor'];
			$larrEmailList["DiscMsgRegistrar"] = $post['DiscMsgRegistrar'];
			$larrEmailList["SemesterId"] = $post['SemesterId'];
			$post['UploadNotice']  = $filename;
			if($post['UploadNotice'] != ""){
				$post['UploadNotice'] = $post['UploadNotice'];
				/*$post['UploadNotice'] = str_replace(' ', '_', $post['UploadNotice']);
				$larrfilepath = split("[/\\.]", $post['UploadNotice']);
				$lstrfilepath = $larrfilepath[0]."_".date('Ymdhis').".".$larrfilepath[1];	
				$post['UploadNotice'] = $lstrfilepath;*/
			}else{
				$post['UploadNotice'] = "";
			}
			
			if($post['PenaltyAmount'] == ""){
				$post['PenaltyAmount'] = "0.00";
			}
			unset($post['IdStudent']);
			unset($post['DiscMsgWarden']);
			unset($post['DiscMsgCollegeHead']);
			unset($post['DiscMsgParent']);
			unset($post['DiscMsgSponor']);
			unset($post['DiscMsgRegistrar']);
			unset($post['StudentName']);
			unset($post['ReadOnlyField']);
			unset($post['RadioGroup']);
			unset($post['HiddenRadioGroup']);
			$lobjDbAdpt->insert($lstrTable,$post);
			$IdDiscActn = $lobjDbAdpt->lastInsertId();
			
			$table = "tbl_disciplinaryactiondetails";
			$countvar=count($larrFormData['idSubjectgrid']);	
			for($i=0;$i<$countvar;$i++) {
				$larrDistribution = array('IdDisciplinaryAction'=>$IdDiscActn,
									'idSubject'=>$larrFormData['idSubjectgrid'][$i],
									'idSemester'=>$larrFormData['idSemestergrid'][$i],
									'DisplinaryAction'=>$larrFormData['DisplinaryActiongrid'][$i],
									'deleteFlag'=>0								
							);
				$lobjDbAdpt->insert($table,$larrDistribution);	
			}
			
			
			self::fnSaveDisciplinaryActionEmailSentDetails($larrEmailList,$IdDiscActn);
			if($post['WarningIssued'] != "0"){
				$lstrMsg = self::fnSendDisciplinaryEmail($post["IdApplication"],$post["IdDispciplinaryActionType"],$larrEmailList,$lstrStudentName);
			}
			$lintNoOfWarnings = self::fnGetNoOfWarnings($post["IdApplication"]);
			$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
			$idUniversity = $this->gstrsessionSIS->idUniversity;
			$lintMaxNoOfWarnings = self::fnGetMaxNoOfWarnings($idUniversity);
			
			if($lintNoOfWarnings >= $lintMaxNoOfWarnings){
				$lstrMsg = "Exceeded";
			}
			return $lstrMsg;
		}
		
		
		//Function To Get Previous Uploaded Notice File
		public function fnGetPreviousUploadedFile($IdDiscActn){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_disciplinaryaction"),array("a.UploadNotice"))
       								->where("a.IdDisciplinaryAction = ?",$IdDiscActn);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult['UploadNotice'];
		}
		
		public function fnViewDistributionDetails($lintIdDisciplinary) { //Function to get the user details
	       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
	       				->from(array("a"=>"tbl_disciplinaryactiondetails"))
	       				->join(array('b' => 'tbl_definationms'),'a.DisplinaryAction = b.idDefinition')
	       				->join(array('c' => 'tbl_subjectmaster'),'a.idSubject = c.IdSubject',array("c.SubjectName"))
	       				->join(array('d' => 'tbl_semester'),'a.idSemester = d.IdSemester',array("CONCAT_WS(' ',IFNULL(e.SemesterMainName,''),IFNULL(d.year,'')) as semestername"))
	       				->join(array('e'=>'tbl_semestermaster'),'d.Semester = e.IdSemesterMaster')
	       				->where("a.IdDisciplinaryAction = ?",$lintIdDisciplinary)
						->where("a.deleteFlag = 0");
	
	       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	  }
	  
		public function fndeletedistributiondtls($iddisciplinayactiondetails) {  
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_disciplinaryactiondetails";
			$larramounts = array('deleteFlag'=>1);
			$where ='iddisciplinayactiondetails = '.$iddisciplinayactiondetails;
			$db->update($table,$larramounts,$where);	
		}
		
		//Function To Update Student's Disciplinary Action
		public function fnUpdateDisciplinaryActionDetails($IdDiscActn,$FormOtherDetails,$formData,$filename){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrMsg = "";
			$newFormData = "";
			$lstrTable = "tbl_disciplinaryaction";
			
			
			$table = "tbl_disciplinaryactiondetails";
			$countvar=count($FormOtherDetails['idSubjectgrid']);	
			for($i=0;$i<$countvar;$i++) {
				$larrDistribution = array('IdDisciplinaryAction'=>$IdDiscActn,
									'idSubject'=>$FormOtherDetails['idSubjectgrid'][$i],
									'idSemester'=>$FormOtherDetails['idSemestergrid'][$i],
									'DisplinaryAction'=>$FormOtherDetails['DisplinaryActiongrid'][$i],
									'deleteFlag'=>0								
							);
				$lobjDbAdpt->insert($table,$larrDistribution);	
			}
			
			unset($FormOtherDetails['idSubject']);
			unset($FormOtherDetails['idSemester']);
			unset($FormOtherDetails['DisplinaryAction']);
			unset($FormOtherDetails['Add']);
			unset($FormOtherDetails['Clear']);
			
			$larrEmailList = array();
			
			
			$larrEmailList["DiscMsgCollegeHead"] = $FormOtherDetails['DiscMsgCollegeHead'];
			$larrEmailList["DiscMsgParent"] = $FormOtherDetails['DiscMsgParent'];
			$larrEmailList["DiscMsgSponor"] = $FormOtherDetails['DiscMsgSponor'];
			$larrEmailList["DiscMsgRegistrar"] = $FormOtherDetails['DiscMsgRegistrar'];
			
			$newFormData['UpdDate'] = $formData['UpdDate'];
			$newFormData['UpdUser'] = $formData['UpdUser'];
			echo $newFormData['IdApplication'] = $FormOtherDetails['IdStudent'];
			$newFormData['IdDispciplinaryActionType'] = $FormOtherDetails['IdDispciplinaryActionType'];
			//$newFormData['UploadNotice'] = $formData['UploadNotice'];
			
			$newFormData['DateOfMistake'] = $FormOtherDetails['DateOfMistake'];
			$newFormData['ReportingDate'] = $FormOtherDetails['ReportingDate'];
			
			$newFormData['DetailedNarration'] = $FormOtherDetails['DetailedNarration'];
			$newFormData['PenaltyDescription'] = $FormOtherDetails['PenaltyDescription'];
			$newFormData['PenaltyAmount'] = $FormOtherDetails['PenaltyAmount'];
			
			$lstrStudentName = self::fnGetStudentName($FormOtherDetails['IdStudent']);
			$formData['UploadNotice'] = $filename;
			if($formData['UploadNotice'] == ""){
				$newFormData['UploadNotice'] = self::fnGetPreviousUploadedFile($IdDiscActn);
			}else{
				if($formData['UploadNotice']!= ""){
					$newFormData['UploadNotice'] = $formData['UploadNotice'];
					/*$formData['UploadNotice'] = str_replace(' ', '_', $formData['UploadNotice']);
					$larrfilepath = split("[/\\.]", $formData['UploadNotice']);
					$lstrfilepath = $larrfilepath[0]."_".date('Ymdhis').".".$larrfilepath[1];	
					$newFormData['UploadNotice'] = $lstrfilepath;*/
				}else{
					$newFormData['UploadNotice'] = "";
				}
			}	
			//echo $formData['UploadNotice']." <==> ".$newFormData['UploadNotice'];exit();
			if($newFormData['PenaltyAmount'] == ""){
				$newFormData['PenaltyAmount'] = "0.00";
			}
			
			$lstrWhere = "IdDisciplinaryAction = ".$IdDiscActn;
			
			
			$lobjDbAdpt->update($lstrTable,$newFormData,$lstrWhere);
			
			self::fnSaveDisciplinaryActionEmailSentDetails($larrEmailList,$IdDiscActn);
			if($FormOtherDetails['WarningIssued'] != "0"){
				$lstrMsg = self::fnSendDisciplinaryEmail($FormOtherDetails["IdStudent"],$formData["IdDispciplinaryActionType"],$larrEmailList,$lstrStudentName);
			}
			$lintNoOfWarnings = self::fnGetNoOfWarnings($FormOtherDetails["IdStudent"]);
				$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
			$idUniversity = $this->gstrsessionSIS->idUniversity;
			$lintMaxNoOfWarnings = self::fnGetMaxNoOfWarnings($idUniversity);
			
			if($lintNoOfWarnings >= $lintMaxNoOfWarnings){
				$lstrMsg = "Exceeded";
			}
			return $lstrMsg;
		} 
		
		//Function To Mail Disciplinary Action Details
		public function  fnSendDisciplinaryEmail($lintIdStudent,$lintIdDisciplinaryActionType,$DisciplinaryActionTypeDetails,$lstrStudentName){
			//$lobjSAMSession = Zend_Registry::get ( 'sam' );
			//$lintIdHostel = $lobjSAMSession->hostel;
			
			$lstrMsg = "";
			
			$larrWardenDetails = "";$lstrCollegeHeadDetials = "";
			$lstrParentDetails = "";$lstrSponsorDetails = "";
			
			$lstrWardenEmail = "";$lstrCollegeHeadEmail = "";
			$lstrParentEmail = "";$lstrSponsorEmail = "";
			$lstrDisciplinaryActionTypeEmail = "";
			$lstrWardenName = ""; $lstrCollegeHeadName= "";
			$lstrParentName = "";$lstrSponsorName = "";
			$lstrDisciplinaryActionTypeName = "";
			
			$larrDiscActnTpeCollegeHeadDetials = ""; $larrDiscActnTpeParentDetails = "";
			$larrDiscActnTpeSponsorDetails = ""; $larrDiscActnTpeWardenDetails = "";
			
			$lstrDiscActnTpeWardenEmail = ""; $lstrDiscActnTpeWardenName = "";
			$lstrDiscActnTpeCollegeHeadEmail = ""; $lstrDiscActnTpeCollegeHeadName = "";
			$lstrDiscActnTpeParentEmail = ""; $lstrDiscActnTpeParentName = "";
			$lstrDiscActnTpeSponsorEmail = ""; $lstrDiscActnTpeSponsorName = "";
			
			
			$lstrSMTPServer = "";$lstrSMTPUsername = "";
			$lstrSMTPPassword = "";$lstrSMTPPort = "";
			$lstrSSL = "";$lstrDefaultEmail = "";
			$lstrSMTPFromEmail = "";
			
			$larrSMTPDetails = self::fnGetSMTPSettings();
			$lstrSMTPServer = $larrSMTPDetails['SMTPServer'];
			$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
			$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
			$lstrSMTPPort = $larrSMTPDetails['SMTPPort'];
			$lstrSSL = $larrSMTPDetails['SSL'];
			$lstrSMTPFromEmail = $larrSMTPDetails['DefaultEmail'];
			
			$lobjTransport = new Zend_Mail_Transport_Smtp();
			$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
			
			//Get Appraisal Email Template
			$lobjDisciplianryEmailTemplate = self::fnGetEmailTemplateDescription('Disciplinary Action');			
			$lstrEmailTemplateFrom =  $lobjDisciplianryEmailTemplate['TemplateFrom'];
			$lstrEmailTemplateFromDesc =  $lobjDisciplianryEmailTemplate['TemplateFromDesc'];
			$lstrEmailTemplateSubject =  $lobjDisciplianryEmailTemplate['TemplateSubject'];
			$lstrEmailTemplateBody =  $lobjDisciplianryEmailTemplate['TemplateBody'];
			
			
			//Get Email Address For Respective Disciplinary Action Type
			//$DisciplinaryActionTypeDetails = self::fnGetDisciplinaryActionTypeEmail($lintIdDisciplinaryActionType);
			
			
			if($DisciplinaryActionTypeDetails['DiscMsgCollegeHead']!="0"){
				$larrDiscActnTpeCollegeHeadDetials = self::fnGetCollegeHeadDetails($lintIdStudent);				
				if(isset($larrDiscActnTpeCollegeHeadDetials['Email']) && $larrDiscActnTpeCollegeHeadDetials['Email']!="")
					$lstrDiscActnTpeCollegeHeadEmail = $larrDiscActnTpeCollegeHeadDetials['Email'];
				
				if(isset($larrDiscActnTpeCollegeHeadDetials['FirstName']) && $larrDiscActnTpeCollegeHeadDetials['FirstName']!="")
					$lstrDiscActnTpeCollegeHeadName = $larrDiscActnTpeCollegeHeadDetials['FirstName'];
			}
			
			if($DisciplinaryActionTypeDetails['DiscMsgParent']!="0"){
				$larrDiscActnTpeParentDetails = self::fnGetParentDetails($lintIdStudent);			
				if(isset($larrDiscActnTpeParentDetails['PEmail']) && $larrDiscActnTpeParentDetails['PEmail']!="")
					$lstrDiscActnTpeParentEmail = $larrDiscActnTpeParentDetails['PEmail'];
				if(isset($larrDiscActnTpeParentDetails['NameOfFather']) && $larrDiscActnTpeParentDetails['NameOfFather']!="")
					$lstrDiscActnTpeParentName = $larrDiscActnTpeParentDetails['NameOfFather'];
			}
			
			if($DisciplinaryActionTypeDetails['DiscMsgSponor']!="0"){
				$larrDiscActnTpeSponsorDetails = self::fnGetSponsorDetails($lintIdStudent);
				if(isset($larrDiscActnTpeSponsorDetails['Email']) && $larrDiscActnTpeSponsorDetails['Email']!="")
					$lstrDiscActnTpeSponsorEmail = $larrDiscActnTpeSponsorDetails['Email'];
				if(isset($larrDiscActnTpeSponsorDetails['SponsorName']) && $larrDiscActnTpeSponsorDetails['SponsorName']!="")	
					$lstrDiscActnTpeSponsorName = $larrDiscActnTpeSponsorDetails['SponsorName'];
			}
			
		
			$larrEmailIds[1] = $lstrDiscActnTpeCollegeHeadEmail;
			$larrEmailIds[2] = $lstrDiscActnTpeParentEmail;
			$larrEmailIds[3] = $lstrDiscActnTpeSponsorEmail;
		
			

			
			$larrNames[1] = $lstrDiscActnTpeCollegeHeadName;
			$larrNames[2] = $lstrDiscActnTpeParentName;
			$larrNames[3] = $lstrDiscActnTpeSponsorName;
		
						
			try{
				$lobjProtocol->connect();
		   		$lobjProtocol->helo($lstrSMTPUsername);
				$lobjTransport->setConnection($lobjProtocol);
			 
				//Intialize Zend Mailing Object
				$lobjMail = new Zend_Mail();
			
				$lobjMail->setFrom($lstrSMTPFromEmail,$lstrEmailTemplateFromDesc);
				$lobjMail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
				$lobjMail->addHeader('MIME-Version', '1.0');
				$lobjMail->setSubject($lstrEmailTemplateSubject);
			
				for($lintI=1;$lintI<=count($larrEmailIds);$lintI++){
					if($larrEmailIds[$lintI] != ""){
						//echo $lstrStudentName.":".$larrEmailIds[$lintI].":".$larrNames[$lintI]."<br/>";
						$lobjMail->addTo($larrEmailIds[$lintI],$larrNames[$lintI]);
				
						//replace tags with values				
						
						$lstrEmailTemplateBody1 = str_replace("[Representative]", $larrNames[$lintI] , $lstrEmailTemplateBody);                       	
						$lstrEmailTemplateBody1 = str_replace("[Candidate]", $lstrStudentName , $lstrEmailTemplateBody1);
						
						
						$lobjMail->setBodyHtml($lstrEmailTemplateBody1);
					
						//echo $lstrEmailTemplateBody."<br/>";
						try {
							$lobjMail->send($lobjTransport);
						} catch (Exception $e) {
							//echo '<br/>fnSendMailToReportingManager12==Exception caught: ', $e->getMessage() . "\n";
							$lstrMsg = "error";      				
						}	
						$lobjMail->clearRecipients();
						//unset($larrEmailIds[$lintI]);
					}
				}
			}catch(Exception $e){
				//echo '<br/>fnSendMailToReportingManager==34>Exception caught: ', $e->getMessage() . "\n";
				$lstrMsg = "error";
			}		
			return $lstrMsg;	
		}
		
	public function fngetSemesterNameCombo(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_semestermaster"),array("key"=>"b.IdSemester","value"=>"CONCAT_WS(' ',IFNULL(sa.SemesterMainName,''),IFNULL(b.year,''))"))
		 				 ->join(array('b'=>'tbl_semester'),'sa.IdSemesterMaster = b.Semester')		 				 
		 				 ->where("sa.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
		public function fngetDisceplinaryActionCombo(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc") )
					 				->join(array("b"=>"tbl_definationtypems"),'a.idDefType = b.idDefType')
					  				->where("b.defTypeDesc ='Disciplinary Action Types'")
					  				->order("a.DefinitionDesc");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
	public function  fnSendEmailToStudent($IdStudent,$TemplateName){
			
			$lintIdHostel = 1;
			
			$lstrMsg = "";
			
			$lstrSMTPServer = "";$lstrSMTPUsername = "";
			$lstrSMTPPassword = "";$lstrSMTPPort = "";
			$lstrSSL = "";$lstrDefaultEmail = "";
			$lstrSMTPFromEmail = "";
			
			//Get SMTP Mailing Server Setting Details
			$larrSMTPDetails = self::fnGetSMTPSettings();
			$lstrSMTPServer = $larrSMTPDetails['SMTPServer'];
			$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
			$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
			$lstrSMTPPort = $larrSMTPDetails['SMTPPort'];
			$lstrSSL = $larrSMTPDetails['SSL'];
			$lstrSMTPFromEmail = $larrSMTPDetails['DefaultEmail'];
			
			$lobjTransport = new Zend_Mail_Transport_Smtp();
			$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
			
			
			//Get Email Template Description
			$larrHostelAllotmentEmailTemplate = self::fnGetEmailTemplateDescription($TemplateName);
			
			//Get Student's Mailing Details
			$larrStudentMailingDetails = self::fnGetStudentMailingDetails($IdStudent);
		
			if($larrHostelAllotmentEmailTemplate['TemplateFrom']!=""){
					
				$lstrEmailTemplateFrom =  $larrHostelAllotmentEmailTemplate['TemplateFrom'];
				$lstrEmailTemplateFromDesc =  $larrHostelAllotmentEmailTemplate['TemplateFromDesc'];
				$lstrEmailTemplateSubject =  $larrHostelAllotmentEmailTemplate['TemplateSubject'];
				$lstrEmailTemplateBody =  $larrHostelAllotmentEmailTemplate['TemplateBody'];
			
			
				$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
				$larrNames[0] = $larrStudentMailingDetails["StudentName"];
				$lstrStudentName = $larrStudentMailingDetails["StudentName"];
				
				try{
					$lobjProtocol->connect();
			   		$lobjProtocol->helo($lstrSMTPUsername);
					$lobjTransport->setConnection($lobjProtocol);
			 	
					//Intialize Zend Mailing Object
					$lobjMail = new Zend_Mail();
			
					$lobjMail->setFrom($lstrSMTPFromEmail,$lstrEmailTemplateFromDesc);
					$lobjMail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
					$lobjMail->addHeader('MIME-Version', '1.0');
					$lobjMail->setSubject($lstrEmailTemplateSubject);
			
					for($lintI=0;$lintI<count($larrEmailIds);$lintI++){
						if($larrEmailIds[$lintI] != ""){
							$lobjMail->addTo($larrEmailIds[$lintI],$larrNames[$lintI]);
					
							//replace tags with values
							$lstrEmailTemplateBody = str_replace("[Representative]", $larrNames[$lintI] , $lstrEmailTemplateBody);                       	
							$lstrEmailTemplateBody = str_replace("[Candidate]", $larrNames[$lintI], $lstrEmailTemplateBody);
							
							$lobjMail->setBodyHtml($lstrEmailTemplateBody);
					
							try {
								$lobjMail->send($lobjTransport);
							} catch (Exception $e) {
								$lstrMsg = "error";      				
							}	
							$lobjMail->clearRecipients();
							unset($larrEmailIds[$lintI]);
						}
					}
				}catch(Exception $e){
					$lstrMsg = "error";
				}
			}else{
				$lstrMsg = "No Template Found";
			}
			return $lstrMsg;
		}
	public function fnGetEmailTemplateDescription($TemplateName){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))       								
       								->where("a.TemplateName LIKE '".$TemplateName."%'");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}
	public function fnGetStudentMailingDetails($IdStudent){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a" => "tbl_studentapplication"),array("CONCAT(a.FName,' ', IFNULL(a.MName,' '),' ',IFNULL(a.LName,' ')) AS StudentName","a.StudentId","a.EmailAddress"))
    	   							->where("a.IDApplication = ?",$IdStudent)    	   							
    	   							->where("a.Active = 1");
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}	
		
	public function fnGetSubjectDetails($idstudent){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_studentregistration"),array(""))
								 ->join(array("b"=>"tbl_studentregsubjects"),'a.IdStudentRegistration = b.IdStudentRegistration',array(""))
 				 				 ->join(array("c"=>"tbl_subjectmaster"),'b.IdSubject = c.IdSubject',array("key"=>"c.IdSubject","value"=>"c.SubjectName"))
				 				 ->where("a.IdApplication =".$idstudent);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetSemesterDetails($idstudent){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_studentregistration"),array(""))
								 ->join(array("b"=>"tbl_semester"),'a.IdSemester = b.IdSemester',array("key"=>"b.IdSemester","value"=>"b.ShortName"))
 				->where("a.IdApplication =".$idstudent);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
}