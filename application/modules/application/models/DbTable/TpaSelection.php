<?php
class Application_Model_DbTable_TpaSelection extends Zend_Db_Table_Abstract {
	
	public function getTpaCandidate($ptest_code,$intake,$period,$selection_status,$load_previous_period,$program,$aps_id,$aps_test_date,$limit=null,$passmark=null,$faculty=null,$attendance=null){
		$db = Zend_Db_Table::getDefaultAdapter();
			
			
		//get placement pass mark for particular program
		if($passmark==null){
			$select_ptest = $db ->select()
			->from(array('app'=>'appl_placement_program'))
			->where("app.app_program_code = ?",$program)
			->where("app.app_placement_code = ?",$ptest_code);
				
			$row = $db->fetchRow($select_ptest);
			$program_pass_mark = $row["app_pass_mark"];
		}else{
			$program_pass_mark=$passmark;
		}

		//get placement schedule id
		if(!$aps_id){
		
			$select_schedule = $db ->select()
			->from(array('aps'=>'appl_placement_schedule'),array('aps_id'))
			->where("aps.aps_placement_code ='".$ptest_code."'");
		
			if(isset($aps_test_date) && $aps_test_date!=''){
				$select_schedule->where("aps.aps_test_date='".$aps_test_date."'");
			}
				
		}
			
		$select = $db ->select()
			->from(array('at'=>'applicant_transaction'))
			->join(array('apt'=>'applicant_ptest'),'apt.apt_at_trans_id=at.at_trans_id')
			//->join(array('ap'=>'applicant_program'),"at.at_trans_id = ap.ap_at_trans_id")
			->where("at.at_status='PROCESS'")
			->where("at.at_selection_status = 5")
			->where("apt_mark >= ?",$program_pass_mark);
			
		
		if(isset($aps_id) && $aps_id!=''){
			$select->where("apt.apt_aps_id ='".$aps_id."'");
		}else{
			$select->where("apt.apt_aps_id IN (?)",$select_schedule);
		}
		if(isset($intake) && $intake!=''){
			$select->where("at.at_intake ='".$intake."'");
		}
		if(isset($period) && $period!=''){
			$select->where("at.at_period ='".$period."'");
		}
		if(isset($load_previous_period) && $load_previous_period!=''){
			$select->where("at.at_period ='".$load_previous_period."'");
		}
		if(isset($program) && $program!=''){
			$select->join(array('ap'=>'applicant_program'),"at.at_trans_id = ap.ap_at_trans_id");
			$select->where("((at_appl_type=1 AND ap.ap_prog_code ='".$program."' AND ap_usm_status=1) OR (at_appl_type=2 AND ap.ap_prog_code ='".$program."'))");
		}
		if(isset($attendance)){
			if($attendance==1){
				$select->where("apt.apt_usm_attendance=1");//hadir
			}elseif($attendance==2){
				$select->where("apt.apt_usm_attendance='0'");//tak hadir
			}
		}
		if(isset($limit) && $limit!=0){
			$select->limit($limit.", 0");
		}

		$row = $db->fetchAll($select);
		
		if($row){
			
			//get program offer
			$appl_programDb = new App_Model_Application_DbTable_ApplicantProgram();
			foreach ($row as $index=>$applicant){
			
				$row[$index]['program_offer'] = $appl_programDb->getOfferProgram($applicant['at_trans_id'], $applicant['at_appl_type']);
				
				//remove below passing mark
				/*if($row[$index]['program_offer']['ap_usm_mark'] < $program_pass_mark){
					unset($row[$index]);
				}*/
				
			}
		
		}
				
		return $row;

	}
}
?>
