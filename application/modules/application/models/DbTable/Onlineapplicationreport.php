<?php
/**
 * Description of Onlineapplicationreport
 *
 * @author IMFCORP\t.mervyn
 */
class Application_Model_DbTable_Onlineapplicationreport extends Zend_Db_Table {
    protected $_name = 'tbl_applicant';

    public function init() {
	$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function fngetonlinereport($searchdata) {
    	$IdScheme = $searchdata['field1'];
    	$IdProgram = $searchdata['field5'];
    	$IdIntake = $searchdata['field8'];
    	$FromDate = $searchdata['field14'];
    	$ToDate = $searchdata['field15'];
        $lstrSelect = $this->select()
                ->setIntegrityCheck(false)
                ->join(array('a' => 'tbl_applicant'),array(),array('Count(a.RegisteredDate) as app_count','MONTHNAME(a.RegisteredDate) AS month_name','MONTH(a.RegisteredDate) as order_field'))
                ->join(array('b'=>'tbl_applicant_preffered'),'a.IdApplicant = b.IdApplicant',array())
                ->join(array('c'=>'tbl_program'),'c.IdProgram = b.IdProgram',array('c.ProgramName'))
                ->joinLeft(array('d'=>'tbl_scheme'),'b.IdScheme = d.IdScheme',array('d.EnglishDescription'))
                ->joinLeft(array('e'=>'tbl_intake'),'b.IdIntake= e.IdIntake',array('e.IntakeId'))
                ->where('YEAR(a.RegisteredDate) = YEAR(NOW())');
		if($IdScheme!="") {
			$lstrSelect = $lstrSelect->where("b.IdScheme = ?",$IdScheme);
		}
		if($IdProgram!="") {
			$lstrSelect = $lstrSelect->where("b.IdProgram = ?",$IdProgram);
		}
		if($IdIntake!="") {
			$lstrSelect = $lstrSelect->where("b.IdIntake = ?",$IdIntake);
		}
		if($FromDate != "" && $ToDate == ""){
			$lstrSelect = $lstrSelect->where("a.RegisteredDate >= ?",$FromDate);
		}
		else if($ToDate != "" && $FromDate == ""){
			$lstrSelect = $lstrSelect->where("a.RegisteredDate <= ?",$ToDate);
		}
    	else if($ToDate != "" && $FromDate != ""){
			$lstrSelect = $lstrSelect
						  ->where("a.RegisteredDate >= ?",$FromDate)
						  ->where("a.RegisteredDate <= ?",$ToDate);
		}
        $lstrSelect = $lstrSelect->group('MONTH(a.RegisteredDate)')
        						->group('b.IdProgram')
        						->order('order_field ASC');
        $larrresult = $this->fetchAll($lstrSelect);
        return $larrresult->toArray();
    }
}
?>
