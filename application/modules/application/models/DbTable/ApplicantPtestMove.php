<?php 
class Application_Model_DbTable_ApplicantPtestMove extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'applicant_ptest_move';
	protected $_primary = "aptm_id";

	public function getData($id=0){
		$id = (int)$id;

		$db = Zend_Db_Table::getDefaultAdapter();
		
		if($id!=0){
				
			$select = $db->select()
						->from(array('a'=>$this->_name))
						->where('a.aptm_id = '.$id);
				
			$row = $db->fetchRow($select);
			
		}else{
			
			$select = $db->select()
							->from(array('apa'=>$this->_name));

			$row = $db->fetchAll($select);
		}

		return $row;
	}
	
	public function insert(array $data){
	
		$auth = Zend_Auth::getInstance();
	
		if(!isset($data['move_by'])){
			$data['move_by'] = $auth->getIdentity()->iduser;
		}
	
		
	
		if( !isset($data['move_date']) ){
			$data['move_date'] = date('Y-m-d H:i:s');
		}
			
		return parent::insert($data);
	}
	
}
?>