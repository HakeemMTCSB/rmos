<?php 
class Application_Model_DbTable_Studentplacementtest extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_studentplacementtest';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	public function fnGetPlacementTestlist(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("pt"=>"tbl_placementtest"),array("pt.IdPlacementTest","pt.PlacementTestName"))
		 				 ->where("pt.Active = 1")
		 				 ->order("pt.PlacementTestName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
    
 	public function fnEducationlist() { //Function to get the Program Branch details
 		$select = $this->select()
                ->setIntegrityCheck(false)  
                ->from(array('a' => 'tbl_studentapplication'),'a.*')
                ->join(array('b'=>'tbl_program'),'a.IDCourse  = b.IdProgram')
                ->joinLeft(array('spt'=>'tbl_studentplacementtest'),'spt.IdApplication  = a.IdApplication',array("spt.IdApplication as PlctIdApplication","spt.IdPlacementTest","spt.IdStudentPlacementTest"))
                ->join(array('c'=>'tbl_definationms'),'b.Award = c.idDefinition')
                ->join(array('d'=>'tbl_collegemaster'),'a.idCollege = d.IdCollege')
                //->where("a.Rejected = 0")
                ->where("a.Offered = 1")
                ->order("a.FName");
               
       $result = $this->fetchAll($select);
       return $result->toArray();
     }
     
     
	public function fnAddStudentPlacementtest($larrformData) { //Function for adding the user details to the table
				$db = Zend_Db_Table::getDefaultAdapter();
				unset($larrformData['IdStudentPlacementTestIds']);
				$table = "tbl_studentplacementtest";
				$countvar=count($larrformData['empid']);				
				for($i=0;$i<$countvar;$i++) {
				$larrcourse = array('IdApplication'=>$larrformData['empid'][$i],
									'IdPlacementTest'=>$larrformData['idplacement'][$i]									
							);
				
				$db->insert($table,$larrcourse);	
			}
	
		
	}
     
	/*public function fnUpdateStudentPlacementtest($larrformData) {
		
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_studentplacementtest";
			$countvar=count($larrformData['empid']);
			for($i=0;$i<$countvar;$i++) {
				$larrcourse = array('IdApplication'=>$larrformData['empid'][$i],
									'IdPlacementTest'=>$larrformData['idplacement'][$i]									
							);
			$where = 'IdStudentPlacementTest = '.$larrformData['IdStudentPlacementTestIds'][$i];
			$db->update($table,$larrcourse,$where);
				
				}
			
		}	*/
		
 public function fnUpdateStudentPlacementtest($IdStudentPlacementTestIds,$larrformData) { //Function for updating the user
	
		$where ='IdStudentPlacementTest = '.$IdStudentPlacementTestIds;
		$larrcourse = array('IdApplication'=>$larrformData['empid'][$i],
									'IdPlacementTest'=>$larrformData['idplacement'][$i]									
							);
		$this->update($larrcourse,$where);
 }
     public function fnSearchEducationlist($post) { //Function to get the Program Branch details
     	$field7 = "a.Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			     ->join(array('a' => 'tbl_studentapplication'),array('IdApplication'))
                ->join(array('b'=>'tbl_program'),'a.IDCourse  = b.IdProgram')
                ->join(array('c'=>'tbl_definationms'),'b.Award = c.idDefinition')
                ->join(array('d'=>'tbl_collegemaster'),'a.idCollege = d.IdCollege')
			   	->where('a.FName  like "%" ? "%"',$post['field3'])
			   	->where('a.ICNumber like  "%" ? "%"',$post['field2'])
			   	->where($field7);;
			   if(isset($post['field5']) && !empty($post['field5']) ){
				  $select = $select->where("b.IdProgram  = ?",$post['field5']);
			 }		
			 if(isset($post['field8']) && !empty($post['field8']) ){
				  $select = $select->where("d.IdCollege  = ?",$post['field8']);
			 }	
		$select ->where("a.Rejected = 0")
                ->where("a.Offered = 1");
		$select	->order("a.FName");
		$result = $this->fetchAll($select);
		return $result->toArray();
     }
	public function fnGetSchoolMasterList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sm"=>"tbl_schoolmaster"),array("key"=>"sm.idSchool","value"=>"sm.SchoolName"))
		 				 ->where("sm.Active = 1")
		 				 ->order("sm.SchoolName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnaddStudent($lobjFormData) {
		unset ($lobjFormData['InstitutionName']);
		unset ($lobjFormData['UniversityName']);
		unset ($lobjFormData['StudyPlace']);
		unset ($lobjFormData['MajorFiledOfStudy']);
		unset ($lobjFormData['YearOfStudyFrom']);
		unset ($lobjFormData['YearOfStudyTo']);
		unset ($lobjFormData['DegreeType']);
		unset ($lobjFormData['GradeOrCGPA']);
		unset ($lobjFormData['InstitutionNamegrid']);
		unset ($lobjFormData['InstitutionNameSelectgrid']);
		unset ($lobjFormData['UniversityNamegrid']);
		unset ($lobjFormData['StudyPlacegrid']);
		unset ($lobjFormData['MajorFiledOfStudygrid']);
		unset ($lobjFormData['YearOfStudyFromgrid']);
		unset ($lobjFormData['YearOfStudyTogrid']);
		unset ($lobjFormData['DegreeTypegrid']);
		unset ($lobjFormData['GradeOrCGPAgrid']);
		unset ($lobjFormData['InstitutionNameSelect']);
		
		
		unset ($lobjFormData['TypeOfSchool']);
		unset ($lobjFormData['StatusOfSchool']);
		unset ($lobjFormData['TypeOfStud']);
		unset ($lobjFormData['HomeTownSchoolTB']);
		unset ($lobjFormData['CreditTransferFromTB']);
		unset ($lobjFormData['HomeTownSchoolDD']);
		unset ($lobjFormData['CreditTransferFromDD']);
		
		
		
		unset ($lobjFormData['TypeofSchoolgrid']);
		unset ($lobjFormData['StatusOfSchoolgrid']);
		unset ($lobjFormData['TypeOfStudgrid']);
		unset ($lobjFormData['HomeTownSchoolgrid']);
		unset ($lobjFormData['CreditTransferFromgrid']);
		
		unset ($lobjFormData['uploadedfiles']);
		unset ($lobjFormData['uploadeddocuments']);
		
		unset ($lobjFormData['comments']);
		unset ($lobjFormData['documentcategory']);
		
		
		unset ($lobjFormData['SubjectId']);
		unset ($lobjFormData['subjectIdgrid']);
		unset ($lobjFormData['SubjectMark']);
		unset ($lobjFormData['SubjectMarkgrid']);
		
		
		$lobjFormData['HomePhone'] = $lobjFormData['HomePhonecountrycode']."-".$lobjFormData['HomePhonestatecode']."-".$lobjFormData['HomePhone'];
		unset($lobjFormData['HomePhonecountrycode']);
		unset($lobjFormData['HomePhonestatecode']);
		
		$lobjFormData['CellPhone'] = $lobjFormData['CellPhonecountrycode']."-".$lobjFormData['CellPhone'];
		unset($lobjFormData['CellPhonecountrycode']);
		
		
		$lobjFormData['Fax'] = $lobjFormData['Faxcountrycode']."-".$lobjFormData['Faxstatecode']."-".$lobjFormData['Fax'];
		unset($lobjFormData['Faxcountrycode']);
		unset($lobjFormData['Faxstatecode']);
		
		$lastinsertid = $this->insert($lobjFormData);
		return $lastinsertid;
	}
	
	
	public function fnaddStudentImageDetails($larrfilenameslist,$IdApplication)
	{
		$str = "";
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		foreach($larrfilenameslist as $larrfilenames) {
			$str .= "(".$IdApplication.",'".$larrfilenames['FileLocation']."','".$larrfilenames['filename']."','".$larrfilenames['uploadedfilename']."','".$larrfilenames['size']."','".$larrfilenames['type']."','".$larrfilenames['UpdDate']."',".$larrfilenames['UpdUser'].",'".$larrfilenames['Comments']."','".$larrfilenames['documentcategory']."'),";
		}
			
		$lstrselectsql = "INSERT INTO tbl_documentdetails (IdApplication,FileLocation,FileName,UploadedFilename,FileSize,MIMEType,UpdDate,UpdUser,Comments,documentcategory) VALUES ".substr($str,0,-1);
		$lobjDbAdpt->query($lstrselectsql);
	
	}
	
	public function fnaddStudentDocumentDetails($larrfilenameslist,$IdApplication)
	{
		$str = "";
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		foreach($larrfilenameslist as $larrfilenames) {
			$str .= "(".$IdApplication.",'".$larrfilenames['FileLocation']."','".$larrfilenames['filename']."','".$larrfilenames['uploadedfilename']."','".$larrfilenames['size']."','".$larrfilenames['type']."','".$larrfilenames['UpdDate']."',".$larrfilenames['UpdUser'].",'".$larrfilenames['Comments']."','".$larrfilenames['documentcategory']."'),";
		}
			
		$lstrselectsql = "INSERT INTO tbl_documentdetails (IdApplication,FileLocation,FileName,UploadedFilename,FileSize,MIMEType,UpdDate,UpdUser,Comments,documentcategory) VALUES ".substr($str,0,-1); 
		
		$lobjDbAdpt->query($lstrselectsql);
	}
	
	
	
	function fnGenerateCode($idUniversity,$collageId,$page,$IdInserted){	
			$db 	= 	Zend_Db_Table::getDefaultAdapter();			
		$select =   $db->select()
				->  from('tbl_config')
				->	where('idUniversity  = ?',$idUniversity);				 
		$result = 	$db->fetchRow($select);		
		$sepr	=	$result[$page.'Separator'];
		$str	=	$page."CodeField";
		$CodeText	=	$page."CodeText";
		for($i=1;$i<=4;$i++){
			$check = $result[$str.$i];
			$TextCode = $result[$CodeText.$i];
			switch ($check){
				case 'Year':
					  $code	= date('Y');
					  break;
				case 'Uniqueid':
					  $code	= $IdInserted;
					  break;
				case 'College':
					  $select =  $db->select()
					 		 -> from('tbl_collegemaster')
					 		 ->	where('IdCollege  = ?',$collageId);  				 
					  $resultCollage = $db->fetchRow($select);		
					  $code		   = $resultCollage['ShortName'];
				      break;
				case 'University':
					  $select =  $db->select()
					 		 -> from('tbl_universitymaster')
					 		 ->	where('IdUniversity  = ?',$idUniversity);  				 
					  $resultCollage = $db->fetchRow($select);		
					  $code	= $resultCollage['ShortName'];
				      break;
				 case 'Text':					 		
					  $code		   = $TextCode;
				      break;
				default:
				      break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}	 	
		return $accCode;			
	}
	 public function fnupdatestudentCode($IdApplication,$StudentCode) { 
	 		$larrformData['StudentId']   	 = $StudentCode;
			$where = 'IdApplication = '.$IdApplication;
			$this->update($larrformData,$where);
	    }
		public function fninsertstudenteducation($studenteducationresult,$IdApplication,$LocalStudent) {  // function to insert po details
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_tempstudenteducationdetails";
			$sessionID = Zend_Session::getId();
			foreach($studenteducationresult as $studenteducationresult) {
				$larrcourse = array('InstitutionName'=>$studenteducationresult['InstitutionName'],
									'UniversityName'=>$studenteducationresult['UniversityName'],
									'StudyPlace'=>$studenteducationresult['StudyPlace'],
									'MajorFiledOfStudy'=>$studenteducationresult['MajorFiledOfStudy'],
									'YearOfStudyFrom'=>$studenteducationresult['YearOfStudyFrom'],
									'YearOfStudyTo'=>$studenteducationresult['YearOfStudyTo'],
									'DegreeType'=>$studenteducationresult['DegreeType'],
									'GradeOrCGPA'=>$studenteducationresult['GradeOrCGPA'],
									'UpdUser'=>$studenteducationresult['UpdUser'],
									'UpdDate'=>$studenteducationresult['UpdDate'],
									'unicode'=>$IdApplication,
									'Date'=>date("Y-m-d"),
									'sessionId'=>$sessionID,
									'deleteFlag'=>1,
									'idExists'=>$studenteducationresult['IdStudEduDtl']
							);
							
				$db->insert($table,$larrcourse);	
			}
		}
		
   public function fnGetTempStudenteducationdetails($IdApplication,$LocalStudent) { //Function for the view University 
   	if($LocalStudent == 0) {
	$select = $this->select()
			->setIntegrityCheck(false)  
			->join(array('a' => 'tbl_tempstudenteducationdetails'),array('a.IdApplication'))
			->join(array('b' => 'tbl_definationms'),'a.DegreeType = b.idDefinition')
			->join(array('c' => 'tbl_definationms'),'a.GradeOrCGPA = c.idDefinition',array('c.DefinitionDesc as GradeDefinitionDesc'))
            ->where('a.unicode = ?',$IdApplication)
            ->where('a.deleteFlag = 1');
   	} else {
   			$select = $this->select()
			->setIntegrityCheck(false)  
			->join(array('a' => 'tbl_tempstudenteducationdetails'),array('a.IdApplication'))
			->join(array('b' => 'tbl_definationms'),'a.DegreeType = b.idDefinition')
			->join(array('c' => 'tbl_definationms'),'a.GradeOrCGPA = c.idDefinition',array('c.DefinitionDesc as GradeDefinitionDesc'))
			->join(array('d' => 'tbl_schoolmaster'),'a.InstitutionName = d.idSchool')
            ->where('a.unicode = ?',$IdApplication)
            ->where('a.deleteFlag = 1');
   	}	
	$result = $this->fetchAll($select);
	return $result->toArray();
    }
    
     public function fnEducationdetaillist($id) { //Function to get the Program Branch details
 		$select = $this->select()
                ->setIntegrityCheck(false)  
                ->join(array('a' => 'tbl_studentapplication'),array('IdApplication'))
                ->join(array('b'=>'tbl_program'),'a.IDCourse  = b.IdProgram')
                ->join(array('c'=>'tbl_definationms'),'b.Award = c.idDefinition')
                ->join(array('d'=>'tbl_collegemaster'),'a.idCollege = d.IdCollege')
                ->where('a.IdApplication = ?',$id);
       $result = $this->fetchAll($select);
       return $result->toArray();
     }
     
    public function fnEducationdetailviewlist($icnumber) { //Function to get the Program Branch details
 		$select = $this->select()
                ->setIntegrityCheck(false)  
                ->join(array('a' => 'tbl_studentapplication'),array('IdApplication'))
                ->join(array('b'=>'tbl_program'),'a.IDCourse  = b.IdProgram')
                ->join(array('c'=>'tbl_definationms'),'b.Award = c.idDefinition')
                ->join(array('d'=>'tbl_collegemaster'),'a.idCollege = d.IdCollege')
                ->join(array('e'=>'tbl_placementtest'),'a.IdPlacementtest = e.IdPlacementTest')
                ->join(array('f'=>'tbl_countries'),'a.PermCountry = f.idCountry',array("PermCountryName"=>"f.CountryName"))
                ->joinLeft(array('g'=>'tbl_countries'),'a.CorrsCountry = g.idCountry',array("CorrCountryName"=>"g.CountryName"))
                ->join(array('h'=>'tbl_state'),'a.PermState = h.idState',array("PermStatename" =>"h.StateName"))
                ->joinLeft(array('i'=>'tbl_state'),'a.CorrsState = i.idState',array("CorrStatename" =>"i.StateName"))
                ->join(array('j'=>'tbl_sponsor'),'a.idsponsor = j.idsponsor')
                ->where('a.ICNumber = ?',$icnumber);
       $result = $this->fetchAll($select);
       return $result->toArray();
     }
     
    public function fnupdateconfirmation($IdApplication) { //Function for updating the user
    	$larrformData = array('Accepted'=>1);
		$where = 'IdApplication = '.$IdApplication;
		$this->update($larrformData,$where);
    }
    
    public function fnupdatestudentapplication($IdApplication,$larrformData) { 
    	unset ($larrformData['InstitutionName']);
		unset ($larrformData['UniversityName']);
		unset ($larrformData['StudyPlace']);
		unset ($larrformData['MajorFiledOfStudy']);
		unset ($larrformData['YearOfStudyFrom']);
		unset ($larrformData['YearOfStudyTo']);
		unset ($larrformData['DegreeType']);
		unset ($larrformData['GradeOrCGPA']);
		unset ($larrformData['InstitutionNamegrid']);
		unset ($larrformData['UniversityNamegrid']);
		unset ($larrformData['StudyPlacegrid']);
		unset ($larrformData['MajorFiledOfStudygrid']);
		unset ($larrformData['YearOfStudyFromgrid']);
		unset ($larrformData['YearOfStudyTogrid']);
		unset ($larrformData['DegreeTypegrid']);
		unset ($larrformData['GradeOrCGPAgrid']);
		unset ($larrformData['IdStudEduDtl']);
		unset ($larrformData['InstitutionNameSelect']);
		unset ($larrformData['SameCorrespAddr']);
		unset ($larrformData['TypeOfSchool']);
		unset ($larrformData['StatusOfSchool']);
		unset ($larrformData['TypeOfStud']);
		unset ($larrformData['HomeTownSchoolDD']);
		unset ($larrformData['HomeTownSchoolTB']);
		unset ($larrformData['CreditTransferFromDD']);	
		unset ($larrformData['CreditTransferFromTB']);	
		unset ($larrformData['TypeofSchoolgrid']);	
		unset ($larrformData['StatusOfSchoolgrid']);
		unset ($larrformData['TypeOfStudgrid']);
		unset ($larrformData['HomeTownSchoolgrid']);
		unset ($larrformData['CreditTransferFromgrid']);
		
		
		unset ($larrformData['SubjectId']);
		unset ($larrformData['subjectIdgrid']);
		unset ($larrformData['SubjectMark']);
		unset ($larrformData['SubjectMarkgrid']);
		
		unset ($larrformData['uploadedfiles']);
		unset ($larrformData['uploadeddocuments']);
		
		unset ($larrformData['comments']);
		unset ($larrformData['documentcategory']);
		
		$larrformData['HomePhone'] = $larrformData['HomePhonecountrycode']."-".$larrformData['HomePhonestatecode']."-".$larrformData['HomePhone'];
		unset($larrformData['HomePhonecountrycode']);
		unset($larrformData['HomePhonestatecode']);
		
		$larrformData['CellPhone'] = $larrformData['CellPhonecountrycode']."-".$larrformData['CellPhone'];
		unset($larrformData['CellPhonecountrycode']);
		
		
		$larrformData['Fax'] = $larrformData['Faxcountrycode']."-".$larrformData['Faxstatecode']."-".$larrformData['Fax'];
		unset($larrformData['Faxcountrycode']);
		unset($larrformData['Faxstatecode']);
		
		$where = 'IdApplication = '.$IdApplication;
		$this->update($larrformData,$where);
    }
    
	public function fnUpdateTempEditEducationdetails($InstitutionName,$UniversityName,$StudyPlace,$YearOfStudyFrom,$YearOfStudyTo,$DegreeType,$GradeOrCGPA,$IdStudEduDtl,$upddate) {
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_tempstudenteducationdetails";
			$larridpo = array('InstitutionName'=>$idItem,
							  'UniversityName'=>$unitPrice,
							  'StudyPlace'=>$quatnity,
							  'MajorFiledOfStudy'=>$UOM,
							  'YearOfStudyFrom'=>$idTax,
							  'YearOfStudyTo'=>$taxPercent,
							  'DegreeType'=>$DtReqrd,
							  'GradeOrCGPA'=>$idPoDtlCostCenter,	
							  'UpdUser'=>$PoBatchNo,
							  'UpdDate'=>$PODtlNarration);
			$where = "idTemp = '$IdStudEduDtl'";
			$db->update($table,$larridpo,$where);	
		}
	public function fnGetProgramChargesList($idCourse){
		$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$select =  $db-> select()
					  -> from(array('a'=>'tbl_programcharges'),'a.*')
					  -> join(array('b'=>'tbl_charges'),'b.IdCharges=a.IdCharges AND b.Payment = 0')
					  -> where('IdProgram  = ?',$idCourse);  			 
		return $db->fetchAll($select);	
		
	}	
	public function fnaddInvoice($ProgramChargesList,$lobjFormData,$result){		
			if(!$lobjFormData['idsponsor'])$lobjFormData['idsponsor'] = 0;
			$db 	= Zend_Db_Table::getDefaultAdapter();
			$Amt = 0;
			for($k =0;$k<count($ProgramChargesList);$k++) {
				$Amt = $Amt+$ProgramChargesList[$k]['Charges'];
			}
			$table 		= "tbl_invoicemaster";			
			$larrcourse = array('IdStudent'=>$result,
									'InvoiceNo'=> date('M').'-'.$result.'-'.$lobjFormData['idsponsor'].'-'.rand(1000,9999),	
									'InvoiceDt'=> date("Y-m-d"),
									'InvoiceAmt'=> $Amt,
									'MonthYear'=> date("MY"),
									'AcdmcPeriod'=>0,
									'Naration'=>"Narration",
									'UpdDate'=>date("Y-m-d"),
									'UpdUser'=> 1,							
									'Active'=> 1,
									'Approved'=>0,
									'idsponsor'=>$lobjFormData['idsponsor']									
							);
			$db->insert($table,$larrcourse);	
			$insertId = $db->lastInsertId('tbl_invoicemaster','IdInvoice');		
			for($k =0;$k<count($ProgramChargesList);$k++) {				
					$table  	= "tbl_invoicedetails";			
					$larrcourse = array('IdInvoice'=>$insertId,
										'idAccount'=>$ProgramChargesList[$k]['IdCharges'],										
										'Discount'=> 0,
										'Amount'=>$ProgramChargesList[$k]['Charges'],											
										'UpdDate'=>date("Y-m-d"),
										'UpdUser'=> 1,							
										'Active'=> 1																		
									);
					$db->insert($table,$larrcourse);					
			}
		
	}
	
	public function fnGetLocalorinternamtional($idCourse){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programentry"))
				 				 ->where("a.Active = 1")
				 				 ->where("a.IdProgram = ?",$idCourse);
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
		public function fnGetProgramList(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_program"),array("key"=>"a.IdProgram","value"=>"ProgramName"))
					 				 ->where("a.Active = 1")
					 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
	
}
?>