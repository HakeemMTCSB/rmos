<?php
class Application_Model_DbTable_SubQualification extends Zend_Db_Table {
    protected $_name = 'tbl_qualificationsub'; // table name

    function getListByQualification($qualification_id) {
        $qualification = $this->select()
                            ->from($this, array("key"=>"id","name"=>"name"))
                            ->where('qualification_id = ?', $qualification_id);
        $result = $this->fetchAll($qualification);
        return $result;
    }

    function getByQualification($qualification_id) {
        $qualification = $this->select()
                            ->where('qualification_id = ?', $qualification_id);
        $result = $this->fetchAll($qualification);
        return $result;
    }


    function updateSubs($subs, $qualification_id) {
    	//clear old subs
    	$where = "qualification_id = $qualification_id" ;
		$this->delete($where);

    	//add new ones

	    if(!empty($subs)) {
	      foreach($subs as $sub) {
	              $qsub = array();
	              $qsub['qualification_id'] = $qualification_id;
	              $qsub['name'] = $sub;
	              $this->insert($qsub);
	      }
	    }
    }

}