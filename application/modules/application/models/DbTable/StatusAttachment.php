<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_StatusAttachment extends Zend_Db_Table_Abstract {
    //put your code here
    protected $_name = 'tbl_statusattachment_sth';
    protected $_primary = "sth_Id";
    
    /*
     * list status attachment list by id
     * 
     * @on 04/07/2014
     */
    public function fnGetStatusAttachListById($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_statusattachment_sth"),array("value"=>"a.*"))
            ->join(array('b'=>'comm_template'), 'b.tpl_id = a.sth_tplId')
            ->join(array('c'=>'tbl_definationms'), 'c.idDefinition = a.sth_type')
            ->where('sth_stpId = ?', $id)
            ->order("a.sth_Id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }

    public function updateStatusAttachment($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_statusattachment_sth', $data, 'sth_Id = '.$id);
        return $update;
    }
}
?>