<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_ProgramScheme extends Zend_Db_Table_Abstract {
    //put your code here
    protected $_name = 'tbl_program';
    
    /*
     * fetching all program
     * 
     * @on 6/6/2014
     */
    public function fnGetProgramList(){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_program"),array("value"=>"a.*"))
            //->where('a.IdProgram = ?',1)
            ->order("a.seq_no")
            ->order("a.ProgramName");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get program scheme
     * 
     * @on 19/6/2014
     */
    public function fnGetProgramschemeListBasedOnProgId($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_program_scheme"),array("value"=>"a.*"))
            /*->join(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mopName'=>'b.DefinitionDesc'))
            ->join(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mosName'=>'c.DefinitionDesc'))
            ->join(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('ptName'=>'d.DefinitionDesc'))*/
            ->joinleft(array('b'=>'registry_values'), 'a.mode_of_program = b.id', array('mopName'=>'b.name'))
            //->join(array('c'=>'registry_values'), 'a.mode_of_study = c.id', array('mosName'=>'c.name'))
            ->joinleft(array('d'=>'registry_values'), 'a.program_type = d.id', array('ptName'=>'d.name'))
            ->joinleft(array('e'=>'tbl_program'), 'a.IdProgram = e.IdProgram', array('awardID'=>'e.Award'))
            ->joinleft(array('f'=>'tbl_award_level'), 'e.Award = f.Id', array('awardName'=>'f.GradeDesc'))
            ->where('a.IdProgram = ?', $id)
            ->order("ptName")
            ->order("mopName");
            //->order("mosName");
        //echo $lstrSelect;
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get program based on id
     * 
     * @on 19/6/2014
     */
    public function fnGetProgramBasedOnProgId($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_program"),array("value"=>"a.*"))
            ->where('a.IdProgram = ?', $id)
            ->order("a.IdProgram");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get program scheme
     * 
     * @on 19/6/2014
     */
    public function fnGetProgramschemeListBasedOnId($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_program_scheme"),array("value"=>"a.*"))
            ->join(array('b'=>'tbl_program'), 'a.IdProgram = b.IdProgram')
            ->where('a.IdProgramScheme = ?', $id)
            ->order("a.IdProgramScheme");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }

    public function fnGetProgramListGotScheme(){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_program"),array("value"=>"a.*"))
            ->join(array('b' => 'tbl_program_scheme'), 'a.IdProgram = b.IdProgram')
            ->order("a.seq_no")
            ->order("a.ProgramName");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        //echo $lstrSelect;
        return $larrResult;
    }
}
?>
