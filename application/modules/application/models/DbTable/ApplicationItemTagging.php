<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_ApplicationItemTagging extends Zend_Db_Table_Abstract
{
    //put your code here
    protected $_name = 'tbl_application_item_tagging';
    protected $_primary = "id";

    /*
     * get item tagging list
     * 
     * @on 10/6/2014
     */
    public function fnGetItemTaggingList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item_tagging"), array("key" => "a.id", "value" => "a.*"))
            ->order("a.id");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get item by id
     * 
     * @on 10/6/2014
     */
    public function fnGetItemTaggingById($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item_tagging"), array("key" => "a.id", "sectionId" => "c.id", "itemname" => "b.name", "value" => "a.*"))
            ->join(array('b' => 'tbl_application_item'), 'a.idItem = b.id')
            ->join(array('c' => 'tbl_application_initialconfig'), 'a.idTagSection = c.id')
            ->join(array('ap' => 'tbl_application_configs'), 'c.IdConfig = ap.id')
            ->join(array('g' => 'tbl_scheme'), 'g.IdScheme = ap.IdScheme', array('SchemeName' => 'g.EnglishDescription'))
            ->joinLeft(array('d' => 'tbl_program'), 'd.IdProgram = ap.IdProgram')
            ->joinLeft(array('e' => 'tbl_program_scheme'), 'e.IdProgramScheme = ap.IdProgramScheme')
            ->joinLeft(array('h' => 'registry_values'), 'h.id = ap.category', array('studentCategory' => 'h.name'))
            ->join(array('f' => 'tbl_application_section'), 'f.id = c.sectionID')
            //->join(array('g'=>'tbl_documentchecklist_dcl'), 'g.dcl_Id = a.item_dclId')
            ->where('a.id = ?', $id)
            ->order("a.id");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    /*
     * get item tagging based on section
     * 
     * @on 19/6/2014
     */
    public function fnGetItemTaggingBasedOnSection($sectionid)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item_tagging"), array("key" => "a.id", "value" => "a.*"))
            ->join(array('b' => 'tbl_application_item'), 'a.idItem = b.id')
            ->joinLeft(array('c' => 'tbl_documentchecklist_dcl'), 'a.item_dclId = c.dcl_Id')
            ->where('a.idTagSection = ?', $sectionid)
            ->order("a.seq_order");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get item by id
     * 
     * @on 23/6/2014
     */
    public function fnGetItemTaggingByIdForMove($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item_tagging"), array("key" => "a.id", "value" => "a.*"))
            ->join(array('b' => 'tbl_application_item'), 'a.idItem = b.id')
            ->where('a.id = ?', $id)
            ->order("a.id");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }
}

?>