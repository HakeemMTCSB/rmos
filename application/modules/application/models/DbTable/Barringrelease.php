<?php
class Application_Model_DbTable_Barringrelease extends Zend_Db_Table_Abstract {
    
    public function getStudentList($search){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'concat(a.registrationId, " - ",b.appl_fname, " ", b.appl_lname)', 'label'=>'concat(a.registrationId, " - ",b.appl_fname, " ", b.appl_lname)', 'id'=>'a.IdStudentRegistration'))
            ->join(array('b'=>'student_profile'), 'a.sp_id = b.id', array())
            ->where('a.profileStatus IN (?)', array(92,248))
            ->where('concat(a.registrationId, " - ",b.appl_fname, " ", b.appl_lname) like "%'.$search.'%"');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getApplicantList($search){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_transaction'), array('value'=>'concat(a.at_pes_id, " - ",b.appl_fname, " ", b.appl_lname)', 'label'=>'concat(a.at_pes_id, " - ",b.appl_fname, " ", b.appl_lname)', 'id'=>'a.at_trans_id'))
            ->join(array('b'=>'applicant_profile'), 'a.at_appl_id = b.appl_id', array())
            ->join(array('c'=>'applicant_program'), 'a.at_trans_id = c.ap_at_trans_id', array())
            ->where('concat(a.at_pes_id, " - ",b.appl_fname, " ", b.appl_lname) like "%'.$search.'%"');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getIntakeList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_intake'), array('name'=>'concat(a.IntakeId, " - ", a.IntakeDesc)', 'id'=>'a.IdIntake'));
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSemesterList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('name'=>'concat(SemesterMainCode, " - ", a.SemesterMainName)', 'id'=>'a.IdSemesterMaster'));
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getBarringReleaseType(){
        $auth = Zend_Auth::getInstance();
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_barringrole'), array('value'=>'*'))
            ->join(array('b'=>'tbl_definationms'), 'a.brole_type = b.idDefinition')
            ->where('b.idDefType = ?', 165)
            ->where('a.brole_role = ?', $auth->getIdentity()->IdRole)
            ->group('a.brole_type')
            ->order('b.idDefinition asc');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function insertBarring($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_barringrelease', $bind);
        $id = $db->lastInsertId('tbl_barringrelease', 'tbr_id');
        return $id;
    }
    
    public function getBarring($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_barringrelease'), array('value'=>'*'))
            ->joinLeft(array('d'=>'applicant_transaction'), 'a.tbr_appstud_id = d.at_trans_id')
            ->joinLeft(array('e'=>'applicant_profile'), 'd.at_appl_id = e.appl_id', array('appName'=>'concat(d.at_pes_id, " - ",e.appl_fname, " ", e.appl_lname)'))
            ->joinLeft(array('f'=>'tbl_studentregistration'), 'a.tbr_appstud_id = f.IdStudentRegistration')
            ->joinLeft(array('g'=>'student_profile'), 'f.sp_id = g.id', array('studName'=>'concat(f.registrationId, " - ",g.appl_fname, " ", g.appl_lname)'))
            ->where('a.tbr_id = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function updateBarring($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_barringrelease', $bind, 'tbr_id = '.$id);
        return $update;
    }
    
    public function getBarringList(){
        $auth = Zend_Auth::getInstance();
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_barringrelease'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_intake'), 'a.tbr_intake = b.IdIntake', array('intakeName'=>'b.IntakeDesc'))
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.tbr_intake = c.IdSemesterMaster', array('semesterName'=>'c.SemesterMainName'))
            ->joinLeft(array('d'=>'applicant_transaction'), 'a.tbr_appstud_id = d.at_trans_id')
            ->joinLeft(array('e'=>'applicant_profile'), 'd.at_appl_id = e.appl_id', array('appName'=>'concat(e.appl_fname, " ", e.appl_lname)'))
            ->joinLeft(array('f'=>'tbl_studentregistration'), 'a.tbr_appstud_id = f.IdStudentRegistration')
            ->joinLeft(array('g'=>'student_profile'), 'f.sp_id = g.id', array('studName'=>'concat(g.appl_fname, " ", g.appl_lname)'))
            ->joinLeft(array('h'=>'tbl_definationms'), 'a.tbr_type = h.idDefinition', array('typeName'=>'h.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_user'), 'j.iduser=a.tbr_createby', array('createby'=>'j.loginName'))
            ->joinLeft(array('k'=>'tbl_user'), 'k.iduser=a.tbr_createby', array('updateby'=>'k.loginName'))
            ->where('a.tbr_type IN (SELECT i.brole_type FROM tbl_barringrole as i WHERE i.brole_role = ?)', $auth->getIdentity()->IdRole)
            ->order('a.tbr_id DESC');
        
        if ($auth->getIdentity()->IdRole != 1){
            $select->where('k.IdRole = ?', $auth->getIdentity()->IdRole);
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function deleteBarring($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_barringrelease', 'tbr_id = '.$id);
        return $delete;
    }
    
    public function insertBarringHistory($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_barringreleasehistory', $bind);
        $id = $db->lastInsertId('tbl_barringreleasehistory', 'id');
        return $id;
    }
    
 	public function checkReleaseStudent($IdStudentRegistration,$type,$semesterId=0){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_barringrelease'))
            ->where('a.tbr_appstud_id = ? ', $IdStudentRegistration)
            ->where('a.tbr_type = ? ', $type)
            ->where('tbr_status = 1')//release only
            ->where('tbr_category = 2');//release only

            if($semesterId){
            	 $select->where('a.tbr_intake = ? ', $semesterId);
            	 $result = $db->fetchRow($select);
            }else{
            	$result = null;
            }
//        echo $select.'<hr>';
        
        return $result;
    }
}