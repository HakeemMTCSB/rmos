<?php 

class Application_Model_DbTable_AgentFormNumber extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'agent_form_number';
	protected $_primary = "afn_id";
	
	
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function getData($id=""){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from($this->_name)
					  ->where("at_status IN ('APPLY','CLOSE','PROCESS')")
					  ->order("at_trans_id desc");
					  
		if($id)	{			
			 $select->where("at_appl_id ='".$id."'");
			 $row = $db->fetchRow($select);				 
		}	 
		
		 return $row;
	}
	
	public function checkUnusedFormNo($form_no,$intake){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from($this->_name)
					  ->where("afn_form_no = ?", $form_no)
					  ->where("afn_taken_status = 0");
					  
		$row = $db->fetchRow($select);
		
		$select2 = $db ->select()
					  ->from('applicant_transaction')
					  ->where("at_pes_id = ?", $form_no);					  
		$row2 = $db->fetchRow($select2);
		
		if($row && !$row2){		
			return true;
		}else{		
			return false;
		}
	}
	
	public function checkValidFormNo($form_no){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from($this->_name)
					  ->where("afn_form_no = ?", $form_no);
					  
		$row = $db->fetchRow($select);
		
		if($row){
			return true;
		}else{
			return false;
		}
	}
	
	public function updateTakenFormNo($data,$form_no){
		 $this->update($data, "afn_form_no = '".$form_no."'");
	}
	
	public function addData($data){
		 $this->insert($data);
	}	
	
	
	public function fngetNomorList($intake){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from($this->_name)
					  ->where("afn_intake = ?",$intake)
					  ->order("afn_form_no");	

		$rows = $db->fetchall($select);	
		return $rows;			  
	}
	public function getTotalVacant($intakeid){
			$lstrSelect = $this->select()
						->from($this->_name,array("total"=>"count(*)"))
						->where("afn_intake = ?",$intakeid)
						->where("afn_taken_status = 0");
			$larrResult = $this->fetchrow($lstrSelect);
		return $larrResult["total"];	
	}
	
	public function getTotalUsed($intakeid){
			$lstrSelect = $this->select()
						->from($this->_name,array("total"=>"count(*)"))
						->where("afn_intake = ?",$intakeid)
						->where("afn_taken_status = 1");
			$larrResult = $this->fetchrow($lstrSelect);
		return $larrResult["total"];		
	}		
}	

	
?>