<?php 
class Application_Model_DbTable_Subjectsetup extends Zend_Db_Table { 
 protected $_name = 'tbl_subject'; // table name
	  private $lobjDbAdpt;
	
	  public function init(){
	    $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  }	
//Get Institution List
	public function fnGetInstitutionList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_institution"),array("key"=>"a.idInstitution","value"=>"a.InstitutionName"))
				 				 ->order("a.InstitutionName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetsubjectList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				->from(array("a"=>$this->_name),array("key"=>"a.IdSubject","value"=>"CONCAT_WS(' - ',IFNULL(a.SubjectName,''),IFNULL(a.SubjectCode,''))"))
				->order("a.SubjectName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnaddSubject($larrformData) {
	    $this->insert($larrformData);
	}
  
 	public function fngetSubjectDetails() { //Function to get the Subject details
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
	    		->from(array('a'=>'tbl_subject'),array("key"=>"a.*"));
	    		//->join(array('b'=>'tbl_institution'),'a.Institution = b.idInstitution',array('b.InstitutionName'));
	    			
	    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	    return $larrResult;
	}
	
 	public function fnupdateSubject($formData,$lintIdSubject) { //Function for updating the Subjectdetails
	    unset ( $formData ['Save'] );
	    $where = 'idSubject= '.$lintIdSubject;
	    $this->update($formData,$where);
	}
	
	public function fnSearchSubject($post = array()) { //Function for searching the Subject details
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  			$lstrSelect = $lobjDbAdpt->select()
       					->from(array("s"=>"tbl_subject"),array("s.*"))
       					//->join(array('b'=>'tbl_institution'),'s.Institution = b.idInstitution',array('b.InstitutionName'))
       					->where('s.SubjectCode like "%" ? "%"',$post['field3'])
       					->where('s.SubjectName like "%" ? "%"',$post['field4'])
       					//->where('b.InstitutionName like "%" ? "%"',$post['field2'])
       					->order("s.SubjectName");
      		 	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

        public function getConfigDetail($universityId){
          $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
          $select = $lobjDbAdpt->select()                        
                        ->from('tbl_config')
                        -> where('idUniversity = ?',$universityId);
          $larrResult = $lobjDbAdpt->fetchAll($select);
          return $larrResult;
        }
}