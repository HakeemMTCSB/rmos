<?php

/**
 * Placementtestmarks
 *
 * @author Arun
 * @version
 */

//require_once 'Zend/Db/Table/Abstract.php';

class Application_Model_DbTable_Placementtestmarks extends Zend_Db_Table_Abstract {
	
    /*
     * The default table name
     */
    protected $_name = 'tbl_placementtestmarks';

    public function init()
    {
            $this->lobjdbAdpt = Zend_Db_Table::getDefaultAdapter();

            $registry = Zend_Registry::getInstance();
            $this->currLocale = $registry->get('Zend_Locale');
    }

    /*
     * @on 14/7/2014
     */
    public function getPlacementMarksEntryList($formData){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_transaction"),array("value"=>"a.*"))
            ->join(array('b'=>'applicant_program'), 'a.at_trans_id = b.ap_at_trans_id')
            ->join(array('c'=>'applicant_profile'), 'a.at_appl_id = c.appl_id')
            ->join(array('d'=>'tbl_placementtestprogram'), 'b.ap_prog_scheme = d.IdProgramScheme')
            ->join(array('e'=>'tbl_intake'), 'e.IdIntake = a.at_intake')
            //->join(array('e'=>'tbl_placementtestcomponent'), 'd.IdPlacementTest = e.IdPlacementTest')
            ->join(array('f'=>'tbl_program_scheme'), 'b.ap_prog_scheme = f.IdProgramScheme')
            ->join(array('g'=>'tbl_program'), 'g.IdProgram = f.IdProgram')
            ->order("a.at_trans_id");

        if(isset($formData)){

            if (isset($formData['Program']) && $formData['Program']!=''){
                $lstrSelect->where('f.IdProgram = ?',$formData['Program']);
            }
            if (isset($formData['Scheme']) && $formData['Scheme']!=''){
                $lstrSelect->where('b.ap_prog_scheme = ?',$formData['Scheme']);
            }
            if (isset($formData['Intake']) && $formData['Intake']!=''){
                $lstrSelect->where('a.at_intake = ?',$formData['Intake']);
            }
            if (isset($formData['PlacementTest']) && $formData['PlacementTest']!=''){
                $lstrSelect->where('d.IdPlacementTest = ?',$formData['PlacementTest']);
            }
            if (isset($formData['StudentCategory']) && $formData['StudentCategory']!=''){
                $lstrSelect->where('c.appl_type_nationality = ?',$formData['StudentCategory']);
            }
            if (isset($formData['ApplicantId']) && $formData['ApplicantId']!=''){
                $lstrSelect->where("a.at_pes_id LIKE '%".$formData['ApplicantId']."%'");
            }
            if (isset($formData['ApplicantName1']) && $formData['ApplicantName1']!=''){
                $lstrSelect->where("c.appl_fname LIKE '%".$formData['ApplicantName1']."%'");
            }
            if (isset($formData['ApplicantName2']) && $formData['ApplicantName2']!=''){
                $lstrSelect->where("c.appl_mname LIKE '%".$formData['ApplicantName2']."%'");
            }
            if (isset($formData['ApplicantName3']) && $formData['ApplicantName3']!=''){
                $lstrSelect->where("c.appl_lname LIKE '%".$formData['ApplicantName3']."%'");
            }
        }

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }
    
    /*
     * get placement test mark component
     * 
     * @on 16/7/2014
     */
    public function getPlacementTestComp($IdPlacementTest){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_placementtestcomponent"),array("value"=>"a.*"))
            ->where('a.IdPlacementTest = ?', $IdPlacementTest)
            ->order("a.IdPlacementTestComponent");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * cek placement test mark
     * 
     * @on 16/7/2014
     */
    public function cekPlacemntTestMark($IdPlacementTest, $IdPlacementTestComponent, $IdApplication){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_placementtestmarks"),array("value"=>"a.*"))
            ->where('a.IdPlacementTest = ?', $IdPlacementTest)
            ->where('a.IdPlacementTestComponent = ?', $IdPlacementTestComponent)
            ->where('a.IdApplication = ?', $IdApplication);
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
}
