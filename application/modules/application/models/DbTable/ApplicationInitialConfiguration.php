<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_ApplicationInitialConfiguration extends Zend_Db_Table_Abstract {
    //put your code here
    protected $_name = 'tbl_application_section';
    
    /*
     * fetching all Section
     * 
     * @on 5/6/2014
     */
    public function fnGetSectionList(){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_application_section"),array("value"=>"a.*"))
            ->order("a.name");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    public function fnGetSectionListTagging(){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_application_section"),array("value"=>"a.*"))
            ->order("a.name")
            ->where('a.status = 1');
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    public function fnGetSectionListSearch($data=NULL){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_application_section"),array("value"=>"a.*"))
            ->order("a.name");
        
        if ($data != NULL){
            if (isset($data['field1']) && $data['field1']!=''){
                $lstrSelect->where("a.name LIKE '%".$data['field1']."%'");
            }
            if (isset($data['field2']) && $data['field2']!=''){
                $lstrSelect->where("a.desc LIKE '%".$data['field2']."%'");
            }
            if (isset($data['field3']) && $data['field3']!=''){
                $lstrSelect->where('a.status = ?',$data['field3']);
            }
        }
        
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get section by id
     * 
     * @on 5/6/2014
     */
    public function fnGetSectionById($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_application_section"),array("value"=>"a.*"))
            ->where('a.id = ?',$id)
            ->order("a.id");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get program by id
     * 
     * @on 12/8/2014
     */
    public function fnGetProgramById($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_program"),array("value"=>"a.*"))
            ->where('a.IdProgram = ?',$id)
            ->order("a.IdProgram");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get item list based on section id
     * 
     * @on 12/8/2014
     */
    public function fnGetItemBySection($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_application_item"),array("value"=>"a.*"))
            //->joinLeft(array('b'=>'tbl_program_details_section'), 'a.')
            ->where('a.item_section_id = ?',$id)
            ->order("a.id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get tooltip info
     * 
     * @on 12/8/2014
     */
    public function fnGetTooltipInfo($itemId, $programId, $valueId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_tooltip_setup"),array("value"=>"a.*"))
            ->where('a.tts_item_id = ?',$itemId)
            ->where('a.tts_program_id = ?',$programId)
            ->where('a.tts_value_id = ?',$valueId)
            ->order("a.tts_id");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * insert tooltip
     */
    public function insertTooltip($data){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $insert = $lobjDbAdpt->insert('tbl_tooltip_setup', $data);
        return $insert;
    }
    
    /*
     * update tooltip
     */
    public function updateTooltip($data, $where){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $update = $lobjDbAdpt->update('tbl_tooltip_setup', $data, $where);
        return $update;
    }
    
    /*
     * get copy tooltip
     */
    public function getCopyTooltip($programId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_tooltip_setup"),array("value"=>"a.*"))
            ->where('a.tts_program_id = ?',$programId)
            ->order("a.tts_id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get program mode
     */
    public function getProgramMode($programId, $mode){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_program_scheme"),array("value"=>"a.*"))
            ->join(array('b'=>'tbl_definationms'), 'a.'.$mode.' = b.idDefinition')
            ->where('a.IdProgram = ?',$programId)
            ->group('a.'.$mode)
            ->order("a.IdProgramScheme");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get intake
     */
    public function getIntake($programId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_intake_program"),array("value"=>"a.*"))
            ->join(array('b'=>'tbl_intake'), 'a.IdIntake = b.IdIntake')
            ->where('a.IdProgram = ?',$programId)
            ->order("a.ip_id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    public function getTransForPreview($progScheme, $stdCtgy){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_transaction"),array("value"=>"a.at_trans_id"))
            ->join(array('b'=>'applicant_profile'), 'a.at_appl_id = b.appl_id')
            ->join(array('c'=>'applicant_program'), 'a.at_trans_id = c.ap_at_trans_id')
            ->where('b.appl_category = ?',$stdCtgy)
            ->where('c.ap_prog_scheme = ?',$progScheme)
            ->order("a.at_trans_id DESC");
	$larrResult = $lobjDbAdpt->fetchOne($lstrSelect);
	return $larrResult;
    }
    
    public function getProgramSectionDetailBasedOnProgram($programId, $itemId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_program_details_section"),array("value"=>"a.*"))
            ->where('a.pds_program_id = ?',$programId)
            ->where('a.pds_item_id = ?',$itemId)
            ->order('a.pds_id DESC');
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    public function updateProgramSectionDetail($bind, $pdsId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $update = $lobjDbAdpt->update('tbl_program_details_section', $bind, 'pds_id = '.$pdsId);
        return $update;
    }
    
    public function insertProgramSectionDetail($bind){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $insert = $lobjDbAdpt->insert('tbl_program_details_section', $bind);
        return $insert;
    }
}
?>