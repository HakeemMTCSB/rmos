<?php
class Application_Model_DbTable_Placementtestmarksbulk extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_placementtest';

	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}


	public function fnSearchEducationlistBulk($post) {



		//            $select = $this->select()
		//                        ->setIntegrityCheck(false)
		//			     ->from(array('a' => 'tbl_placementtestcomponent'),array('a.*'))
		//                ->joinLeft(array('b'=>'tbl_placementtest'),'b.IdPlacementTest = a.IdPlacementTest')
		//                ->joinLeft(array('tp'=>'tbl_placementtestprogram'),'tp.IdPlacementTest  = b.IdPlacementTest',array())
		//                ->joinLeft(array('c'=>'tbl_studentapplication'),'c.ProvisionalProgramOffered = tp.IdProgram ',array('c.*'))
		//                //->joinLeft(array('d'=>'tbl_program'),'c.ProvisionalProgramOffered = d.IdProgram')
		//                //->joinLeft(array('e'=>'tbl_definationms'),'d.Award = e.idDefinition')
		//               // ->joinLeft(array('f'=>'tbl_collegemaster'),'c.idCollege = f.IdCollege')
		//               // ->joinLeft(array('g'=>'tbl_placementtestmarks'),'g.IdPlacementTestComponent = a.IdPlacementTestComponent AND g.IdApplication = c.IdApplication',array('g.Marks'))
		//               // ->joinLeft(array("h"=>"tbl_city"),"h.idCity  = c.PermCity ",array('h.CityName'))
		//                ->where('c.Registered = 0 OR c.Registered = ""')
		//                ->where('c.Status = 196');


		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('a' => 'tbl_studentapplication'),array('a.FName','a.PermAddressDetails','a.ExtraIdField1','a.IdApplication','a.PermCity'))
		->join(array('tp'=>'tbl_placementtestprogram'),'a.ProvisionalProgramOffered = tp.IdProgram',array('tp.IdPlacementTest'))
		->join(array('d'=>'tbl_program'),' d.IdProgram = a.ProvisionalProgramOffered ',array('d.ProgramName'))
		->join(array('b'=>'tbl_placementtest'),'b.IdPlacementTest = tp.IdPlacementTest',array('b.PlacementTestName','b.PlacementTestDate','b.PlacementTestTime'))
		->join(array('c'=>'tbl_placementtestcomponent'),'c.IdPlacementTest = tp.IdPlacementTest',array('c.*'))
		->join(array('g'=>'tbl_placementtestmarks'),'g.IdPlacementTest = tp.IdPlacementTest AND g.IdPlacementTestComponent = c.IdPlacementTestComponent AND g.IdApplication = a.IdApplication',array('g.Marks'))
		->joinLeft(array('e'=>'tbl_definationms'),'d.Award = e.idDefinition')
		->joinLeft(array('f'=>'tbl_collegemaster'),'a.idCollege = f.IdCollege')
		->joinLeft(array("h"=>"tbl_city"),"h.idCity  = a.PermCity ",array('h.CityName'))
		->where('a.Registered = 0 OR a.Registered = ""')
		->where('a.Status = 196');




		if(isset($post['field7']) && !empty($post['field7']) ){
			$select = $select->where('a.Active = ? ',$post['field7']);
		}
		if(isset($post['field1']) && !empty($post['field1']) ){
			$select = $select->where('tp.IdPlacementTest = ? ',$post['field1']);
		}
		if(isset($post['field3']) && !empty($post['field3']) ){
			$select = $select->where('a.FName  like "%" ? "%"',$post['field3']);
		}
		if(isset($post['field2']) && !empty($post['field2']) ){
			$select = $select->where('a.ExtraIdField1 like  "%" ? "%"',$post['field2']);
		}

		if(isset($post['field5']) && !empty($post['field5']) ){
			$select = $select->where("a.ProvisionalProgramOffered  = ?",$post['field5']);
		}
		if(isset($post['field8']) && !empty($post['field8']) ){
			$select = $select->where("a.IdCollege  = ?",$post['field8']);
		}

		$select	->order("a.FName");
		$result = $this->fetchAll($select);
		return $result->toArray();
	}


	public function fnSearchEducationlist($post) { //Function to get the Program Branch details

		$field7 = "c.Active = ".$post["field7"];
		/*	$select = $this->select()
		 ->setIntegrityCheck(false)
		->from(array('a' => 'tbl_studentapplication'),array('IdApplication'))
		->join(array('b'=>'tbl_program'),'a.IDCourse  = b.IdProgram')
		->join(array('c'=>'tbl_definationms'),'b.Award = c.idDefinition')
		->join(array('d'=>'tbl_collegemaster'),'a.idCollege = d.IdCollege')
		->join(array('e'=>'tbl_placementtest'),' e.IdPlacementTest = '.$post["field1"])
		->join(array('f'=>'tbl_placementtestcomponent'),'e.IdPlacementTest = f.IdPlacementTest')
		->where('a.FName  like "%" ? "%"',$post['field3'])
		->where('a.ICNumber like  "%" ? "%"',$post['field2'])
		->where($field7);
		if(isset($post['field5']) && !empty($post['field5']) ){
		$select = $select->where("b.IdProgram  = ?",$post['field5']);
		}
		if(isset($post['field8']) && !empty($post['field8']) ){
		$select = $select->where("d.IdCollege  = ?",$post['field8']);
		}
		$select	->order("a.FName");*/



		/*$select =	"SELECT `a`.*,b.*,c.FName,c.MName,c.LName,c.PermCity,c.idCollege,c.FName,c.FName,c.FName,c.FName,c.FName,c.FName,d.ProgramName,e.DefinitionCode,e.DefinitionDesc,f.CollegeName
		 FROM `tbl_placementtestcomponent` AS `a`
		INNER JOIN `tbl_placementtest` AS `b` ON b.IdPlacementTest = a.IdPlacementTest
		INNER JOIN  tbl_studentapplication AS c
		INNER JOIN `tbl_program` AS `d` ON c.IDCourse = d.IdProgram
		INNER JOIN `tbl_definationms` AS `e` ON d.Award = e.idDefinition
		INNER JOIN `tbl_collegemaster` AS `f` ON c.idCollege = f.IdCollege
		WHERE a.IdPlacementTest = 29
		AND (c.FName like "%" '' "%") AND (c.ICNumber like "%" '' "%")
		AND (c.Active = 1) AND (d.IdProgram = '13') AND (f.IdCollege = '1')
		ORDER BY `c`.`FName` ASC";*/



		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('a' => 'tbl_placementtestcomponent'),array('a.*'))
		->joinLeft(array('b'=>'tbl_placementtest'),'b.IdPlacementTest = a.IdPlacementTest')
		->joinLeft(array('c'=>'tbl_studentapplication'),'')
		->joinLeft(array('d'=>'tbl_program'),'c.IDCourse = d.IdProgram')
		->joinLeft(array('e'=>'tbl_definationms'),'d.Award = e.idDefinition')
		->joinLeft(array('f'=>'tbl_collegemaster'),'c.idCollege = f.IdCollege')
		->joinLeft(array('g'=>'tbl_placementtestmarks'),'g.IdPlacementTestComponent = a.IdPlacementTestComponent AND g.IdApplication = c.IdApplication',array('g.Marks'))
		->joinLeft(array("h"=>"tbl_city"),"h.idCity  = c.PermCity ",array('h.CityName'))
		//->where('a.IdPlacementTest = ? "%"',$post['field1'])
		//->where('c.FName  like "%" ? "%"',$post['field3'])
		->where('c.Registered =0');
		//->where('c.ICNumber like  "%" ? "%"',$post['field2'])
		//->where($field7);
		if(isset($post['field7']) && !empty($post['field7']) ){
			$select = $select->where('c.Active = ? ',$post['field7']);
		}
		if(isset($post['field1']) && !empty($post['field1']) ){
			$select = $select->where('a.IdPlacementTest = ? ',$post['field1']);
		}
		if(isset($post['field3']) && !empty($post['field3']) ){
			$select = $select->where('c.FName  like "%" ? "%"',$post['field3']);
		}
		if(isset($post['field2']) && !empty($post['field2']) ){
			$select = $select->where('c.ICNumber like  "%" ? "%"',$post['field2']);
		}

		if(isset($post['field5']) && !empty($post['field5']) ){
			$select = $select->where("d.IdProgram  = ?",$post['field5']);
		}
		if(isset($post['field8']) && !empty($post['field8']) ){
			$select = $select->where("c.IdCollege  = ?",$post['field8']);
		}

		$select	->order("c.FName");
		$result = $this->fetchAll($select);
		return $result->toArray();
	}

	public function fngetComponents(){

		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('a' => 'tbl_placementtestcomponent'))  ;
			
		$result = $this->fetchAll($select);
		print_r( $result->toArray());exit;
		return $result;

	}

	public function fnViewPlacementTestCombo($IdProgram){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pm"=>"tbl_placementtestprogram"),array())
		->join(array("pt"=>"tbl_placementtest"),'pt.IdPlacementTest = pm.IdPlacementTest',array("key"=>"pt.IdPlacementTest","value"=>"pt.PlacementTestName"))
		->where("pm.IdProgram = ".$IdProgram)
		->where("pt.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}





	public function fnViewPlacementTest($IdPlacementTest){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pt"=>"tbl_placementtest"),array("pt.*"))
		->join(array("ptb"=>"tbl_placementtestbranch"),'pt.IdPlacementTest = ptb.IdPlacementTest')
		->where('pt.IdPlacementTest = ?',$IdPlacementTest)
		->where("pt.Active = 1");

		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnViewPlacementComponentDetails($IdPlacementTest){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("ptc"=>"tbl_placementtestcomponent"),array("ptc.*"))
		->where('ptc.IdPlacementTest = ?',$IdPlacementTest)	;

		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnupdateplacementtest($IdPlacementTest,$larrformData) { //Function for updating the user
		unset ($larrformData['IdPlacementTestBranch']);
		unset ($larrformData['IdPlacementTestComponent']);
		unset ($larrformData['IdCollege']);
		unset ( $larrformData ['ComponentName']);
		unset ( $larrformData ['ComponentWeightage'] );
		unset ( $larrformData ['ComponentTotalMarks'] );
		//unset ( $larrformData ['IdPlacementTest'] );
		unset ($larrformData['ComponentNamegrid']);
		unset ($larrformData['ComponentWeightagegrid']);
		unset ($larrformData['ComponentTotalMarksgrid']);
		unset ($larrformData['Save']);
		$where = 'IdPlacementTest = '.$IdPlacementTest;
		$this->update($larrformData,$where);
	}
	public function fndeleteplacementbranch($IdPlacementTest) { //Function for Delete Purchase order terms
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_placementtestbranch";
		$where = $db->quoteInto("IdPlacementTest = $IdPlacementTest");
		$db->delete($table, $where);
	}

	public function fndeleteplacementtestComponent($IdPlacementTestComponent) { //Function for Delete Purchase order terms
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_placementtestcomponent";
		$where = $db->quoteInto('IdPlacementTestComponent  = ?', $IdPlacementTestComponent);
		$db->delete($table, $where);
	}

	public function fngetPlacementtestDetails() { //Function to get the user details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pt"=>"tbl_placementtest"),array("pt.*"))
		->join(array("pm"=>"tbl_program"),'pm.IdProgram = pt.IdProgram',array("pm.*"))
		->join(array("ptb"=>"tbl_placementtestbranch"),'ptb.IdPlacementTest= pt.IdPlacementTest')
		->join(array("cm"=>"tbl_collegemaster"),'cm.IdCollege= ptb.IdCollege',array("cm.*"))
		->order("pt.PlacementTestName");
		//->where("cm.CollegeType = 1")
		//->where("pt.Active = 1")
		//->where("cm.Active = 12");
		//->where("dm.DepartmentType  = 0");
		//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetProgramList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pm"=>"tbl_program"),array("key"=>"pm.IdProgram","value"=>"pm.ProgramName"))
		->join(array("pt"=>"tbl_placementtest"),'pt.IdProgram = pm.IdProgram')
		->where("pm.Active = 1")
		->where("pt.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnGetProgramMaterList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pm"=>"tbl_program"),array("key"=>"pm.IdProgram","value"=>"pm.ProgramName"))
		->where("pm.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetBranchList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("a"=>"tbl_collegemaster"),array("key"=>"a.IdCollege","value"=>"a.CollegeName"))
		//->where("a.CollegeType = 1")
		->where("a.Active = 1");
		// echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnSearchPlacementTest($post = array()) { //Function to get the user details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pt"=>"tbl_placementtest"),array("pt.*"))
		->join(array("pm"=>"tbl_program"),'pm.IdProgram = pt.IdProgram',array("pm.*"))
		->join(array("ptb"=>"tbl_placementtestbranch"),'ptb.IdPlacementTest= pt.IdPlacementTest')
		->join(array("cm"=>"tbl_collegemaster"),'cm.IdCollege= ptb.IdCollege',array("cm.*"));
			
		if(isset($post['field5']) && !empty($post['field5']) ){
			$lstrSelect = $lstrSelect->where("pt.IdProgram  = ?",$post['field5']);
		}
		$lstrSelect	->where('pt.PlacementTestName like "%" ? "%"',$post['field3'])
		->where("pt.Active = ".$post["field7"])
		->order("pt.PlacementTestName");
			
		//echo $lstrSelect;die();

		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}


	public function fnaddplacementtest($larrformData) { //Function for adding the user details to the table
		unset ( $larrformData ['IdCollege'] );
		unset ( $larrformData ['IdPlacementTestBranch'] );
		unset ( $larrformData ['IdPlacementTestComponent'] );

		unset ( $larrformData ['ComponentName']);
		unset ( $larrformData ['ComponentWeightage'] );
		unset ( $larrformData ['ComponentTotalMarks'] );
		unset ($larrformData['ComponentNamegrid']);
		unset ($larrformData['ComponentWeightagegrid']);
		unset ($larrformData['ComponentTotalMarksgrid']);
		$this->insert($larrformData);
		$lobjdb = Zend_Db_Table::getDefaultAdapter();
		return $lobjdb->lastInsertId();
	}

	public function fnaddplacementtestBranchDtls($larrformData,$IdPlacementTest) { //Function for adding the user details to the table
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		unset ( $larrformData ['IdPlacementTestComponent'] );
		unset ( $larrformData ['IdProgram'] );
		unset ( $larrformData ['PlacementTestName'] );
		unset ( $larrformData ['PlacementTestDate'] );
		unset ( $larrformData ['PlacementTestTime'] );
		unset ( $larrformData ['Active'] );

		unset ( $larrformData ['ComponentName']);
		unset ( $larrformData ['ComponentWeightage'] );
		unset ( $larrformData ['ComponentTotalMarks'] );
		unset ($larrformData['ComponentNamegrid']);
		unset ($larrformData['ComponentWeightagegrid']);
		unset ($larrformData['ComponentTotalMarksgrid']);

		$count = count($larrformData['IdCollege']);
		for($i = 0 ;$i<$count ; $i++) {
			$lstrTable = "tbl_placementtestbranch";
			$larrInsertData = array('IdPlacementTest' => $IdPlacementTest,
					'IdCollege' => $larrformData["IdCollege"][$i],
					'UpdDate' => $larrformData["UpdDate"],
					'UpdUser' => $larrformData["UpdUser"]
			);
			$lobjDbAdpt->insert($lstrTable,$larrInsertData);
		}
	}
	public function fnaddNewplacementtestComponentDtls($larrformData,$result) { //Function for adding the user details to the table
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		unset ( $larrformData ['IdProgram'] );
		unset ( $larrformData ['PlacementTestName'] );
		//unset ( $larrformData ['IdPlacementTest'] );
		unset ( $larrformData ['IdPlacementTestBranch'] );
		unset ( $larrformData ['PlacementTestDate'] );
		unset ( $larrformData ['PlacementTestTime'] );
		unset ( $larrformData ['IdCollege'] );
		unset ( $larrformData ['Save'] );
		unset ( $larrformData ['Active'] );

		unset ( $larrformData ['ComponentName']);
		unset ( $larrformData ['ComponentWeightage'] );
		unset ( $larrformData ['ComponentTotalMarks'] );
		$count = count($larrformData['ComponentNamegrid']);
		for($i = 0 ;$i<$count ; $i++) {
			$lstrTable = "tbl_placementtestcomponent";
			$larrInsertData = array('IdPlacementTestComponent' => $larrformData["IdPlacementTestComponent"],
					'IdPlacementTest' => $result,
					'ComponentName' => $larrformData["ComponentNamegrid"][$i],
					'ComponentWeightage' => $larrformData["ComponentWeightagegrid"][$i],
					'ComponentTotalMarks' => $larrformData["ComponentTotalMarksgrid"][$i],
					'UpdDate' => $larrformData["UpdDate"],
					'UpdUser' => $larrformData["UpdUser"]
			);
			$lobjDbAdpt->insert($lstrTable,$larrInsertData);
		}
	}
	public function fnaddplacementtestComponentDtls($larrformData) { //Function for adding the user details to the table
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		unset ( $larrformData ['IdProgram'] );
		unset ( $larrformData ['PlacementTestName'] );
		//unset ( $larrformData ['IdPlacementTest'] );
		unset ( $larrformData ['IdPlacementTestBranch'] );
		unset ( $larrformData ['PlacementTestDate'] );
		unset ( $larrformData ['PlacementTestTime'] );
		unset ( $larrformData ['IdCollege'] );
		unset ( $larrformData ['Save'] );
		unset ( $larrformData ['Active'] );

		unset ( $larrformData ['ComponentName']);
		unset ( $larrformData ['ComponentWeightage'] );
		unset ( $larrformData ['ComponentTotalMarks'] );
		$count = count($larrformData['ComponentNamegrid']);
		for($i = 0 ;$i<$count ; $i++) {
			$lstrTable = "tbl_placementtestcomponent";
			$larrInsertData = array('IdPlacementTestComponent' => $larrformData["IdPlacementTestComponent"],
					'IdPlacementTest' => $larrformData ['IdPlacementTest'],
					'ComponentName' => $larrformData["ComponentNamegrid"][$i],
					'ComponentWeightage' => $larrformData["ComponentWeightagegrid"][$i],
					'ComponentTotalMarks' => $larrformData["ComponentTotalMarksgrid"][$i],
					'UpdDate' => $larrformData["UpdDate"],
					'UpdUser' => $larrformData["UpdUser"]
			);
			$lobjDbAdpt->insert($lstrTable,$larrInsertData);
		}
	}

	public function fnviewSubject($lintisubject) { //Function for the view user
		//echo $lintidepartment;die();
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
		->from(array("a" => "tbl_subjectmaster"),array("a.*"))
		->where("a.IdSubject= ?",$lintisubject);
		return $result = $lobjDbAdpt->fetchRow($select);
	}

	public function fnupdateSubject($lintIdDepartment,$larrformData) { //Function for updating the user
		$where = 'IdSubject = '.$lintIdDepartment;
		$this->update($larrformData,$where);
	}

	public function fnUpdateTempprogramentrydetails($idplacementtestcomponent) { //Function for Delete Purchase order terms
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_placementtestcomponent";
		$where = $db->quoteInto('IdPlacementTestComponent  = ?', $idplacementtestcomponent);
		$db->delete($table, $where);
	}

	public function fnGetPlcaementList($idCourse){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_placementtest"),array("key"=>"a.IdPlacementTest","value"=>"PlacementTestName"))
		->where("a.Active = 1")
		->where("a.IdProgram = ?",$idCourse)
		->order("a.PlacementTestName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetPlacementtime($IdPlacementTest){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_placementtest"))
		->where("a.Active = 1")
		->where("a.IdPlacementTest = ?",$IdPlacementTest);
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}

	public function fnGeteditPlcaementList(){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_placementtest"),array("key"=>"a.IdPlacementTest","value"=>"PlacementTestName"))
		->where("a.Active = 1")
		->order("a.PlacementTestName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}


}