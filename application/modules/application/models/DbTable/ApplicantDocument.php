<?php 
class Application_Model_DbTable_ApplicantDocument extends Zend_Db_Table_Abstract {

/**
	 * The default table name 
	 */
	protected $_name = 'applicant_documents';
	protected $_primary = "ad_id";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){		
		 $this->update($data,"ad_id =".(int)$id); 		 
	}
		
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getData($txn_id,$type){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from($this->_name)
					 // ->where("ad_type = '".$type."'")
					  ->where("ad_txn_id='".$txn_id."'")
					  ->order('ad_id desc limit 1');
		 $row = $db->fetchRow($select);	
		
		 return $row;
	}
	
	public function getDocumentType($type){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from(array('d'=>'tbl_definationms'))
					  ->where("d.idDefType=?",$type)
					  ->where("d.Status=1")
					  ->order('d.defOrder ASC');
		 $row = $db->fetchAll($select);	
		
		 return $row;
	}
	
	public function getApplicantDocumentByType($txn_id,$type){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					   ->from(array('tgd'=>'applicant_generated_document'))	
					   ->joinLeft(array('sth'=>'tbl_statusattachment_sth'),'sth.sth_Id=tgd.sth_id',array('sth_type'))
					   ->joinLeft(array('u'=>'tbl_user'),'u.iduser=tgd.tgd_updUser',array('fName'))
					   ->where("tgd.tgd_txn_id = ?",$txn_id)				
					   ->where("sth.sth_type = ".$type." or tgd.tgd_filetype = ".$type)
					   ->order('tgd_updDate DESC');					  
		
		$row = $db->fetchAll($select);		
		return $row;
	}
	
	public function getDataArray($txn_id,$tbl_id,$tbl_name=null){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('ad'=>$this->_name))	
					  ->joinleft(array('dcl'=>'tbl_documentchecklist_dcl'),'dcl.dcl_Id=ad.ad_dcl_id',array('dcl_name'))				
					  ->where("ad.ad_table_id = '".$tbl_id."'")					
					  ->where("ad.ad_txn_id='".$txn_id."'");
					  
		 if(isset($tbl_name) && $tbl_name!=''){
		 	  $select->where("ad.ad_table_name = '".$tbl_name."'");
		 }
		 $row = $db->fetchAll($select);	
		
		 return $row;
	}
	
	
	public function getDataArrayBySection($txn_id,$section){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $select = $db ->select()
					  ->from(array('ad'=>$this->_name))	
					  ->joinleft(array('dcl'=>'tbl_documentchecklist_dcl'),'dcl.dcl_Id=ad.ad_dcl_id',array('dcl_name'))				
					  ->where("ad.ad_section_id = '".$section."'")					  
					  ->where("ad.ad_txn_id='".$txn_id."'");
		 $row = $db->fetchAll($select);	
		
		 if( $row ) {
		 	return $row;
		 } else {
		 	return null;
		 }
	}
	
	
	}
?>