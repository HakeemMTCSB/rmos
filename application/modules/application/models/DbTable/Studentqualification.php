<?php

class Application_Model_DbTable_Studentqualification extends Zend_Db_Table_Abstract {

  protected $_name = 'tbl_studenteducationdetails';
  private $lobjDbAdpt;

  public function init() {
    $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $this->lobjsubjectdetail = new  Application_Model_DbTable_Subjectqualification();
  }

  public function fnaddQualification($data){	
    $this->insert($data);
    $insertId = $this->lobjDbAdpt->lastInsertId($this->_name,'IdStudEduDtl');
    return $insertId;
  }

  public function fnupdate($data, $IdQualification){
    $where = 'IdStudEduDtl = '.$IdQualification;
    $this->update($data,$where);
  }

  public function fngetapplicantQualification($IdApplication){
    $lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_studenteducationdetails'))
                ->joinLeft(array('c' => 'tbl_qualificationmaster'),'a.IdQualification = c.IdQualification')
                ->joinLeft(array('d' => 'tbl_definationms'),'a.IdResultItem = d.idDefinition')
                ->joinLeft(array('f' => 'tbl_institution'),'a.IdInstitute = f.idInstitution')
                ->joinLeft(array('g' => 'tbl_specialization'),'a.IdSpecialization = g.IdSpecialization',array('g.Specialization'))
		->where('a.IdApplication = ?',$IdApplication);
    $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);   
    $subjectdetails = array();
    $rowid = 0;
    foreach($larrResult as $item) {
      $larrResult[$rowid]['subjectdetails'] = $this->lobjsubjectdetail->fngetsubjectdetails($item['IdStudEduDtl']);
      $rowid++;
    }
    return $larrResult;
  }

  public function fngetapplicantId($IdApplicant){
      $lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_studentapplication'))
                ->where('a.IdApplicant = ?',$IdApplicant);
      return $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
      
  }
  
  public function fndeletequalification($Id,$IdApplicant) {
        $where = 'IdStudEduDtl NOT IN ('.implode(",",$Id).') AND IdApplication ='.$IdApplicant;
        $this->lobjDbAdpt->delete($this->_name, $where);
  }
}

?>
