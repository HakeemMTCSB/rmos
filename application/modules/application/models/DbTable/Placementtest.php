<?php

class Application_Model_DbTable_Placementtest extends Zend_Db_Table { //Model Class for Users Details

	protected $_name = 'tbl_placementtest';

	public function init() 
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$registry = Zend_Registry::getInstance();
        $translate = $registry->get('Zend_Translate');
        $this->currLocale = $translate->getLocale();
	}

	public function fnViewPlacementTest($IdPlacementTest) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pt" => "tbl_placementtest"), array("pt.*"))
		->join(array("ptb" => "tbl_placementtestprogram"), 'pt.IdPlacementTest = ptb.IdPlacementTest', array('ptb.*'))
		->where('pt.IdPlacementTest = ?', $IdPlacementTest);
		//->where("pt.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fngetPlacementTestProgramDetails($IdPlacementTest) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pt" => "tbl_placementtest"), array())
		->join(array("ptb" => "tbl_placementtestprogram"), 'pt.IdPlacementTest = ptb.IdPlacementTest', array('ptb.*'))
		->joinLeft(array("p" => "tbl_program_scheme"),'ptb.IdProgramScheme = p.IdProgramScheme')
		->joinLeft(array("s" => "tbl_program"),'p.IdProgram = s.IdProgram')
		->where('pt.IdPlacementTest = ?', $IdPlacementTest);
		//->where("pt.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnViewPlacementComponentDetails($IdPlacementTest) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("ptc" => "tbl_placementtestcomponent"), array("ptc.*"))
		->where('ptc.IdPlacementTest = ?', $IdPlacementTest);

		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnupdateplacementtest($IdPlacementTest, $larrformData) { //Function for updating the user
		//echo $IdPlacementTest;die();
		//$larrformData ['IdPlacementTest']=$IdPlacementTest;
		//echo "<pre>";
		//print_r($larrformData);die();
		$larrformData['Phone'] = $larrformData['countrycode'] . "-" . $larrformData['statecode'] . "-" . $larrformData['Phone1'];
		//		echo "<pre>";
		//		print_r($larrformData);die;
		unset($larrformData['countrycode']);
		unset($larrformData['statecode']);
		unset($larrformData['Phone1']);
		unset($larrformData['IdPlacementTestBranch']);
		unset($larrformData['IdPlacementTestComponent']);
		unset($larrformData['IdProgram']);
		unset($larrformData['IdScheme']);
		unset($larrformData['IdProgramNamegrid']);
		unset($larrformData['IdSchemeNamegrid']);
		unset($larrformData ['ComponentName']);
		unset($larrformData ['MarkingType']);
		unset($larrformData ['ComponentWeightage']);
		unset($larrformData ['ComponentTotalMarks']);
		unset($larrformData ['MinimumMark']);
		//unset ( $larrformData ['IdPlacementTest'] );
		unset($larrformData['MarkingTypegrid']);
		unset($larrformData['ComponentNamegrid']);
		unset($larrformData['ComponentWeightagegrid']);
		unset($larrformData['ComponentTotalMarksgrid']);
		unset($larrformData ['MinimumMarkgrid']);
		unset($larrformData['Save']);
		//echo "<pre>";
		//print_r($larrformData);die();
		$where = 'IdPlacementTest = ' . $IdPlacementTest;
		$this->update($larrformData, $where);
	}

	public function fndeleteplacementbranch($IdPlacementTest) { //Function for Delete Purchase order terms
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_placementtestprogram";
		$wheres = "IdPlacementTest = $IdPlacementTest";
		$db->delete($table, $wheres);
	}

	public function fndeleteplacementtestComponent($IdPlacementTestComponent) { //Function for Delete Purchase order terms
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_placementtestcomponent";
		$where = 'IdPlacementTest   = ' . $IdPlacementTestComponent;
		$db->delete($table, $where);
	}

	public function fngetPlacementtestDetails() { //Function to get the user details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pt" => "tbl_placementtest"), array("pt.*"))
		->join(array("ptb" => "tbl_placementtestprogram"), 'ptb.IdPlacementTest= pt.IdPlacementTest')
		->join(array("pm" => "tbl_program_scheme"), 'pm.IdProgramScheme = ptb.IdProgramScheme', array("pm.*"))
		->join(array("cm"=>"tbl_program"),'cm.IdProgram= pm.IdProgram',array("cm.*"))
		->order("pt.PlacementTestName");
		//->group("pt.PlacementTestName");
		//->where("cm.CollegeType = 1")
		//->where("pt.Active = 1")
		//->where("cm.Active = 12");
		//->where("dm.DepartmentType  = 0");
		//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetPlcaementList($idCourse) {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_placementtest"), array("key" => "a.IdPlacementTest", "value" => "PlacementTestName"))
		->join(array("b" => "tbl_placementtestprogram"), 'a.IdPlacementTest = b.IdPlacementTest')
		->where("a.Active = 1")
		->where("b.IdProgram = ?", $idCourse)
		->order("a.PlacementTestName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetProgramList() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pm" => "tbl_program"), array("key" => "pm.IdProgram", "value" => "pm.ProgramName"))
		->join(array("pt" => "tbl_placementtest"), 'pt.IdProgram = pm.IdProgram')
		->where("pm.Active = 1")
		->where("pt.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetProgramMaterList() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		if ( $this->currLocale == 'ar_YE' )
		{
			$lstrSelect = $lobjDbAdpt->select()
			->from(array("pm" => "tbl_program"), array("key" => "pm.IdProgram", "value" => "CONCAT_WS('-',pm.ProgramCode,pm.ArabicName)"))
			->where("pm.Active = 1")
			->where("pm.ProgramName != ''")
			->order("pm.ProgramName");
		}
		else
		{
			$lstrSelect = $lobjDbAdpt->select()
			->from(array("pm" => "tbl_program"), array("key" => "pm.IdProgram", "value" => "CONCAT_WS('-',pm.ProgramCode,pm.ProgramName)"))
			->where("pm.Active = 1")
			->where("pm.ProgramName != ''")
			->order("pm.ProgramName");
		}
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetBranchList() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("a" => "tbl_collegemaster"), array("key" => "a.IdCollege", "value" => "a.CollegeName"))
		//->where("a.CollegeType = 1")
		->where("a.Active = 1")
		->order("a.CollegeName");
		// echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetIdVenu() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("a" => "tbl_branchofficevenue"), array("key" => "a.IdBranch", "value" => "a.BranchName"))
		->where("a.IdType = 3");
		//->where("a.Active = 1")
		//->order("a.CollegeName");
		// echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnSearchPlacementTest($post = array()) { //Function to get the user details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pt" => "tbl_placementtest"), array("pt.*"))
		->join(array("ptb" => "tbl_placementtestprogram"), 'ptb.IdPlacementTest= pt.IdPlacementTest')
		->join(array("pm" => "tbl_program"), 'pm.IdProgram = ptb.IdProgram', array("pm.*"));
		//->join(array("cm"=>"tbl_collegemaster"),'cm.IdCollege= ptb.IdCollege',array("cm.*"));

		if (isset($post['field14']) && !empty($post['field14'])) {
			$lstrSelect = $lstrSelect->where("pt.PlacementTestDate  = ?", $post['field14']);
		}
		$lstrSelect->where('pt.PlacementTestName like "%" ? "%"', $post['field3'])
		->where("pt.Active = " . $post["field7"])
		->order("pt.PlacementTestName");

		//echo $lstrSelect;die();

		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnaddplacementtest($larrformData) { //Function for adding the user details to the table
		$larrformData['Phone'] = $larrformData['countrycode'] . "-" . $larrformData['statecode'] . "-" . $larrformData['Phone1'];
		unset($larrformData['countrycode']);
		unset($larrformData['statecode']);
		unset($larrformData['Phone1']);
		unset($larrformData ['IdProgram']);
		unset($larrformData ['IdScheme']);
		unset($larrformData ['IdProgramNamegrid']);
		unset($larrformData ['IdSchemeNamegrid']);
		unset($larrformData ['IdPlacementTestBranch']);
		unset($larrformData ['IdPlacementTestComponent']);

		unset($larrformData ['Year']);
		unset($larrformData ['Apptype']);
		unset($larrformData ['EnterNo']);

		unset($larrformData ['ComponentName']);
		unset($larrformData ['MarkingType']);
		unset($larrformData ['ComponentWeightage']);
		unset($larrformData ['ComponentTotalMarks']);
		unset($larrformData ['MinimumMark']);

		unset($larrformData['ComponentNamegrid']);
		unset($larrformData['ComponentWeightagegrid']);
		unset($larrformData['ComponentTotalMarksgrid']);
		unset($larrformData['MarkingTypegrid']);
		unset($larrformData ['MinimumMarkgrid']);
		//$this->insert($larrformData);
		$this->lobjDbAdpt->insert('tbl_placementtest', $larrformData);
		return ($this->lobjDbAdpt->lastInsertId());
	}

	public function fnGetmaxid() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("a" => "tbl_placementtestnopes"), array("max(a.IdPlacementtestNopes) as maxid'"));

		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnInsertintoplacementtestnopes($larrformData, $IdPlacementTest, $FamulirNo, $password) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrTable = "tbl_placementtestnopes";
		$larrInsertData = array("IdPlacementTest" => $IdPlacementTest,
				"Year" => $larrformData['Year'],
				"ApplicationType" => $larrformData['Apptype'],
				"EnterNo" => $larrformData['EnterNo'],
				"FamulirNo" => $FamulirNo,
				"Password" => $password);
		$lobjDbAdpt->insert($lstrTable, $larrInsertData);
	}

	public function fnaddplacementtestBranchDtls($larrformData, $IdPlacementTest) { //Function for adding the user details to the table
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		unset($larrformData ['IdPlacementTestComponent']);
		unset ( $larrformData ['IdProgram'] );
		unset($larrformData ['PlacementTestName']);
		unset($larrformData ['PlacementTestDate']);
		unset($larrformData ['PlacementTestTime']);
		unset($larrformData ['Active']);

		unset($larrformData ['ComponentName']);
		unset($larrformData ['MarkingType']);
		unset($larrformData ['ComponentWeightage']);
		unset($larrformData ['ComponentTotalMarks']);
		unset($larrformData ['MinimumMark']);
		unset($larrformData['ComponentNamegrid']);
		unset($larrformData['ComponentWeightagegrid']);
		unset($larrformData['ComponentTotalMarksgrid']);
		unset($larrformData ['MinimumMarkgrid']);

		$count = count($larrformData['IdProgramNamegrid']);
		for ($i = 0; $i < $count; $i++) {
			$lstrTable = "tbl_placementtestprogram";
			$larrInsertData = array('IdPlacementTest' => $IdPlacementTest,
					'IdProgramScheme' => $larrformData["IdSchemeNamegrid"][$i],
					'IdScheme' => $larrformData["IdSchemeNamegrid"][$i],
					'UpdDate' => $larrformData["UpdDate"],
					'UpdUser' => $larrformData["UpdUser"]
			);



			$lobjDbAdpt->insert($lstrTable, $larrInsertData);
		}
	}

	public function fnaddNewplacementtestComponentDtls($larrformData, $result) { //Function for adding the user details to the table
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		unset($larrformData ['IdProgram']);
		unset($larrformData ['PlacementTestName']);
		//unset ( $larrformData ['IdPlacementTest'] );
		unset($larrformData ['IdPlacementTestBranch']);
		unset($larrformData ['PlacementTestDate']);
		unset($larrformData ['PlacementTestTime']);
		unset($larrformData ['IdCollege']);
		unset($larrformData ['Save']);
		unset($larrformData ['Active']);

		unset($larrformData ['ComponentName']);
		unset($larrformData ['MarkingType']);
		unset($larrformData ['ComponentWeightage']);
		unset($larrformData ['ComponentTotalMarks']);
		unset($larrformData ['MinimumMark']);
		$count = count($larrformData['ComponentNamegrid']);
		for ($i = 0; $i < $count; $i++) {
			$lstrTable = "tbl_placementtestcomponent";
			$larrInsertData = array(
					'IdPlacementTest' => $result,
					'ComponentName' => $larrformData["ComponentNamegrid"][$i],
					'MarkingType' => $larrformData["MarkingTypegrid"][$i],
					'ComponentWeightage' => $larrformData["ComponentWeightagegrid"][$i],
					'ComponentTotalMarks' => $larrformData["ComponentTotalMarksgrid"][$i],
					'MinimumMark' => $larrformData["MinimumMarkgrid"][$i],
					'UpdDate' => $larrformData["UpdDate"],
					'UpdUser' => $larrformData["UpdUser"]
			);
			$lobjDbAdpt->insert($lstrTable, $larrInsertData);
		}
	}

	public function fnaddplacementtestComponentDtls($larrformData, $IdPlacementTest) { //Function for adding the user details to the table
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		unset($larrformData ['IdProgram']);
		unset($larrformData ['PlacementTestName']);
		$larrformData ['IdPlacementTest'] = $IdPlacementTest;
		unset($larrformData ['IdPlacementTestBranch']);
		unset($larrformData ['PlacementTestDate']);
		unset($larrformData ['PlacementTestTime']);
		unset($larrformData ['IdCollege']);
		unset($larrformData ['Save']);
		unset($larrformData ['Active']);

		unset($larrformData ['ComponentName']);
		unset($larrformData ['MarkingType']);
		unset($larrformData ['ComponentWeightage']);
		unset($larrformData ['ComponentTotalMarks']);
		unset($larrformData ['MinimumMark']);
		$count = count($larrformData['ComponentNamegrid']);
		for ($i = 0; $i < $count; $i++) {
			$lstrTable = "tbl_placementtestcomponent";
			$larrInsertData = array(
					'IdPlacementTest' => $larrformData ['IdPlacementTest'],
					'ComponentName' => $larrformData["ComponentNamegrid"][$i],
					'MarkingType' => $larrformData["MarkingTypegrid"][$i],
					'ComponentWeightage' => $larrformData["ComponentWeightagegrid"][$i],
					'ComponentTotalMarks' => $larrformData["ComponentTotalMarksgrid"][$i],
					'MinimumMark' => $larrformData["MinimumMarkgrid"][$i],
					'UpdDate' => $larrformData["UpdDate"],
					'UpdUser' => $larrformData["UpdUser"]
			);
			$lobjDbAdpt->insert($lstrTable, $larrInsertData);
		}
	}

	public function fnviewSubject($lintisubject) { //Function for the view user
		//echo $lintidepartment;die();
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a" => "tbl_subjectmaster"), array("a.*"))
		->where("a.IdSubject= ?", $lintisubject);
		return $result = $lobjDbAdpt->fetchRow($select);
	}

	public function fnupdateSubject($lintIdDepartment, $larrformData) { //Function for updating the user
		$where = 'IdSubject = ' . $lintIdDepartment;
		$this->update($larrformData, $where);
	}

	public function fnUpdateTempprogramentrydetails($idplacementtestcomponent) { //Function for Delete Purchase order terms
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_placementtestcomponent";
		$where = $db->quoteInto('IdPlacementTestComponent  = ?', $idplacementtestcomponent);
		$db->delete($table, $where);
	}

	/* public function fnGetPlcaementList($idCourse){
	 $lstrSelect = $this->lobjDbAdpt->select()
	->from(array("a"=>"tbl_placementtest"),array("key"=>"a.IdPlacementTest","value"=>"PlacementTestName"))
	->where("a.Active = 1")
	->where("a.IdProgram = ?",$idCourse)
	->order("a.PlacementTestName");
	$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
	} */

	public function fnGetPlacementtime($IdPlacementTest) {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_placementtest"))
		->where("a.Active = 1")
		->where("a.IdPlacementTest = ?", $IdPlacementTest);
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}

	public function fnGeteditPlcaementList() {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_placementtest"), array("key" => "a.IdPlacementTest", "value" => "PlacementTestName"))
		->where("a.Active = 1")
		->order("a.PlacementTestName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fngetPlacementTest($IdPlacementTest) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pt" => "tbl_placementtest"), array("pt.*"))
		->join(array("ptb" => "tbl_placementtestcomponent"), 'pt.IdPlacementTest = ptb.IdPlacementTest', array('ptb.*'))
		->where('pt.IdPlacementTest = ?', $IdPlacementTest);
		//->where("pt.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnaddcopyplacementTest($data) {
		$this->lobjDbAdpt->insert('tbl_placementtest', $data);
		return ($this->lobjDbAdpt->lastInsertId());
	}

	public function fnaddcopyplacementcomponent($data) {
		$this->lobjDbAdpt->insert('tbl_placementtestcomponent', $data);
	}

	public function getplcamenttestprogrammappingdet($idplacementtest) {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("ptb" => "tbl_placementtestprogram"), array("ptb.*"))
		->join(array("pt" => "tbl_placementtest"), 'pt.IdPlacementTest = ptb.IdPlacementTest', array('ptb.*'))
		->where('pt.IdPlacementTest = ?', $idplacementtest);
		//->where("pt.Active = 1");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult[0];
	}

	public function addprogramplacementtestmapping($data) {
		$this->lobjDbAdpt->insert('tbl_placementtestprogram', $data);
	}

	/**
	 * Function to get count whether on condition provided the record exists or not
	 * @author: Vipul
	 */
	public function fnGetPlacementDetails($condition) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pm" => "tbl_placementtest"), array("pm.IdPlacementTest", "pm.PlacementTestName", "pm.PlacementTestDate", "pm.PlacementTestTime"))
		->where($condition);
		//echo $lstrSelect; echo '</br>';
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	/**
	 * Function to get placement program details
	 * @author: Vipul
	 */
	public function fnGetPlacementProgramDetails($id) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("pm" => "tbl_placementtestprogram"))
		->where("pm.IdPlacementTest  = ?", $id);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function getPlacementTest($id){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_placementtest"), array("a.IdPlacementTest","a.PlacementTestName","a.PlacementTestDate","a.Addr1","a.Addr2","a.PlacementTestTime"))
		->joinLeft(array("c" => "tbl_city"),"a.City = c.idCity",array("c.CityName"))
		->joinLeft(array("d" => "tbl_state"),"a.idState = d.idState",array("d.StateName"))
		->joinLeft(array("e" => "tbl_countries"),"a.idCountry = e.idCountry",array("e.CountryName"))
		->where('a.IdPlacementTest = ?', $id);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;

	
}
	public function getPlacementTestofstud($prg,$intake){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_placementtest"), array("a.IdPlacementTest","a.PlacementTestName","a.PlacementTestDate","a.Addr1","a.Addr2","a.PlacementTestTime"))
		->joinLeft(array("c" => "tbl_city"),"a.City = c.idCity",array("c.CityName"))
		->joinLeft(array("d" => "tbl_state"),"a.idState = d.idState",array("d.StateName"))
		->joinLeft(array("e" => "tbl_countries"),"a.idCountry = e.idCountry",array("e.CountryName"))
		->join(array("f" => "tbl_placementtestprogram"),'f.IdPlacementTest = a.IdPlacementTest',array(''))
		->where('a.Intake = ?', $intake)
		->where('f.IdProgram =?',$prg);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
		
	}
}
