<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_DocumentChecklist extends Zend_Db_Table_Abstract
{
    //put your code here
    protected $_name = 'tbl_documentchecklist_dcl';
    protected $_primary = "dcl_Id";

    public function getDclList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_documentchecklist_dcl"), array("value" => "a.*"))
            //->join(array('b'=>'tbl_application_initialconfig'), 'a.dcl_sectionId = b.id')
            ->joinLeft(array('c' => 'tbl_application_section'), 'a.dcl_sectionid = c.id')
            ->join(array('d' => 'tbl_program_scheme'), 'a.dcl_programScheme = d.IdProgramScheme')
            ->join(array('e' => 'tbl_program'), 'd.IdProgram = e.IdProgram')
            ->where('a.dcl_specific = ?', 0)
            ->order("e.IdProgram")
            ->order("a.dcl_name")
            ->group('e.IdProgram')
            ->group('a.dcl_programScheme');
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        //echo $lstrSelect;
        return $larrResult;
    }

    public function getDclListById($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_documentchecklist_dcl"), array("value" => "a.*"))
            //->join(array('b'=>'tbl_application_initialconfig'), 'a.dcl_sectionId = b.id')
            //->join(array('c'=>'tbl_application_section'), 'b.sectionID = c.id')
            ->join(array('d' => 'tbl_program_scheme'), 'a.dcl_programScheme = d.IdProgramScheme')
            ->join(array('e' => 'tbl_program'), 'd.IdProgram = e.IdProgram')
            ->where('a.dcl_Id = ?', $id)
            ->order("a.dcl_Id");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        //echo $lstrSelect;
        return $larrResult;
    }

    public function getDclListByStudentCtgy($stdCtgy = 0, $uplStatus = 0, $schemeId = 0, $sectionId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_documentchecklist_dcl"), array("value" => "a.*"))
            ->where('a.dcl_stdCtgy = ?', $stdCtgy)
            ->where('a.dcl_uplStatus = ?', $uplStatus)
            ->where('a.dcl_programScheme = ?', $schemeId)
            ->where('a.dcl_sectionid = ?', $sectionId)
            ->where('a.dcl_specific = ?', 0)
            ->order("a.dcl_Id");

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getDclListByStudentCtgyProgScheme($stdCtgy = 0, $uplStatus = 0, $progSchemeId = 0, $sectionID)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_documentchecklist_dcl"), array("value" => "a.*"))
            ->where('a.dcl_stdCtgy = ?', $stdCtgy)
            ->where('a.dcl_uplStatus = ?', $uplStatus)
            ->where('a.dcl_programScheme = ?', $progSchemeId ?? 0)
            ->where('a.dcl_sectionid = ?', $sectionID)
            ->where('a.dcl_specific = ?', 0)
            ->order("a.dcl_Id");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getDclSearchList($formData = null)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
           /* ->from(array("a" => "tbl_documentchecklist_dcl"), array("value" => "a.*"))
            ->joinLeft(array('c' => 'tbl_application_section'), 'a.dcl_sectionid = c.id')
            ->join(array('d' => 'tbl_program_scheme'), 'a.dcl_programScheme = d.IdProgramScheme')
            ->join(array('e' => 'tbl_program'), 'd.IdProgram = e.IdProgram')
            ->order("a.dcl_priority")
            ->order("a.dcl_name")
            ->group('e.IdProgram')
            ->group('a.dcl_programScheme');*/

           ->from(array('a' => 'tbl_documentchecklist_main'))
            ->join(array('c' => 'tbl_scheme'), 'c.IdScheme = a.IdScheme', array('SchemeName' => 'c.EnglishDescription','SchemeID' => 'c.IdScheme'))
            ->joinLeft(array('d' => 'tbl_program'), 'd.IdProgram = a.IdProgram', array('ProgramID' => 'a.IdProgram','ProgramName' => 'd.ProgramName'))
            ->joinLeft(array('e' => 'tbl_program_scheme'), 'e.IdProgramScheme = a.IdProgramScheme')
            ->joinLeft(array('f' => 'registry_values'), 'f.id = a.category', array('studentCategory' => 'f.name'))
            ->joinLeft(array('g' => 'tbl_award_level'), 'd.Award = g.Id', array('awardName' => 'g.GradeDesc'))
            ->joinleft(array('h'=>'registry_values'), 'e.mode_of_program = h.id', array('mopName'=>'h.name'))
            ->joinleft(array('i'=>'registry_values'), 'e.program_type = i.id', array('ptName'=>'i.name'))
            /*->where('a.IdScheme = ?', $IdScheme)
            ->where('a.IdProgram = ?', $IdProgram)
            ->where('a.IdProgramScheme = ?', $IdProgramScheme)
            ->where('a.category = ?', $category)*/
            ->order('a.created_at desc');


        if (isset($formData)) {

           /* if (isset($formData['field1']) && $formData['field1'] != '') {
                $lstrSelect->where($lobjDbAdpt->quoteInto("dcl_name LIKE ?", "%" . $formData['field1'] . "%"));
            }*/
            if (isset($formData['field2']) && $formData['field2'] != '') {
                $lstrSelect->where($lobjDbAdpt->quoteInto('e.IdProgramScheme = ?', $formData['field2']));
            }
            if (isset($formData['field3']) && $formData['field3'] != '') {
                $lstrSelect->where($lobjDbAdpt->quoteInto('d.IdProgram = ?', $formData['field3']));
            }
            if (isset($formData['field4']) && $formData['field4'] != '') {
                $lstrSelect->where($lobjDbAdpt->quoteInto('a.category = ?', $formData['field4']));
            }
        }

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        //echo $lstrSelect;
        //exit;
        return $larrResult;
    }

    public function getDclCopy($progScheme, $stdCtgy)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_documentchecklist_dcl"), array("value" => "a.*"))
            ->where('a.dcl_programScheme = ?', $progScheme)
            ->where('a.dcl_stdCtgy = ?', $stdCtgy)
            ->order("a.dcl_Id");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getDclMainCopy($programid, $progScheme, $stdCtgy)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_documentchecklist_main"), array("value" => "a.*"))
            ->where('a.IdProgram = ?', $programid)
            ->where('a.IdProgramScheme = ?', $progScheme)
            ->where('a.category = ?', $stdCtgy)
            ->order("a.id");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }


    public function checkUsingChecklist($dclId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item_tagging"), array("value" => "a.*"))
            ->where('a.item_dclId = ?', $dclId);
        //->where('a.dcl_stdCtgy = ?', $stdCtgy)
        //->order("a.dcl_Id");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getSubDclList($idscheme,$idprogram,$idprogscheme,$idstudtype)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_documentchecklist_dcl"), array("value" => "a.*"))
            //->join(array('b'=>'tbl_application_initialconfig'), 'a.dcl_sectionId = b.id')
            ->joinLeft(array('c' => 'tbl_application_section'), 'a.dcl_sectionid = c.id')
            ->join(array('d' => 'tbl_program_scheme'), 'a.dcl_programScheme = d.IdProgramScheme')
            ->join(array('e' => 'tbl_program'), 'd.IdProgram = e.IdProgram')
            ->joinLeft(array('f' => 'registry_values'), 'f.id = a.dcl_stdCtgy', array('studentCategory' => 'f.name'))
            ->joinLeft(array('j' => 'registry_values'), 'j.id = a.dcl_uplType', array('docType' => 'j.name'))
            ->joinLeft(array('g' => 'tbl_award_level'), 'e.Award = g.Id', array('awardName' => 'g.GradeDesc'))
            ->joinleft(array('h'=>'registry_values'), 'd.mode_of_program = h.id', array('mopName'=>'h.name'))
            ->joinleft(array('i'=>'registry_values'), 'd.program_type = i.id', array('ptName'=>'i.name'))
            ->where('a.dcl_specific = ?', 0)
            ->where('e.IdProgram = ?', $idprogram)
            ->where('a.dcl_programScheme = ?', $idprogscheme)
            ->where('a.dcl_stdCtgy = ?', $idstudtype)
            ->order("e.IdProgram")
            ->order("a.dcl_name");
            //->group('e.IdProgram')
            //->group('a.dcl_programScheme');
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        //echo $lstrSelect;
        return $larrResult;
    }

    public function getSubDclSearchList($formData = null)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_documentchecklist_dcl"), array("value" => "a.*"))
            ->joinLeft(array('c' => 'tbl_application_section'), 'a.dcl_sectionid = c.id')
            ->join(array('d' => 'tbl_program_scheme'), 'a.dcl_programScheme = d.IdProgramScheme')
            ->join(array('e' => 'tbl_program'), 'd.IdProgram = e.IdProgram')
            ->order("a.dcl_priority")
            ->order("a.dcl_name");
            //->group('e.IdProgram')
            //->group('a.dcl_programScheme');


        if (isset($formData)) {

            if (isset($formData['field1']) && $formData['field1'] != '') {
                $lstrSelect->where($lobjDbAdpt->quoteInto("dcl_name LIKE ?", "%" . $formData['field1'] . "%"));
            }
            if (isset($formData['field2']) && $formData['field2'] != '') {
                $lstrSelect->where($lobjDbAdpt->quoteInto('dcl_programScheme = ?', $formData['field2']));
            }
            if (isset($formData['field3']) && $formData['field3'] != '') {
                $lstrSelect->where($lobjDbAdpt->quoteInto('d.IdProgram = ?', $formData['field3']));
            }
            if (isset($formData['field4']) && $formData['field4'] != '') {
                $lstrSelect->where($lobjDbAdpt->quoteInto('dcl_stdCtgy = ?', $formData['field4']));
            }
        }

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getConfig($IdScheme=0, $IdProgram = 0 , $IdProgramScheme = 0, $category = 0)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'tbl_documentchecklist_main'))
            ->join(array('c' => 'tbl_scheme'), 'c.IdScheme = a.IdScheme', array('SchemeName' => 'c.EnglishDescription','SchemeID' => 'c.IdScheme'))
            ->joinLeft(array('d' => 'tbl_program'), 'd.IdProgram = a.IdProgram', array('ProgramID' => 'a.IdProgram','ProgramName' => 'd.ProgramName'))
            ->joinLeft(array('e' => 'tbl_program_scheme'), 'e.IdProgramScheme = a.IdProgramScheme')
            ->joinLeft(array('f' => 'registry_values'), 'f.id = a.category', array('studentCategory' => 'f.name'))
            ->where('a.IdScheme = ?', $IdScheme)
            ->where('a.IdProgram = ?', $IdProgram)
            ->where('a.IdProgramScheme = ?', $IdProgramScheme)
            ->where('a.category = ?', $category)
            ->order('a.created_at desc');
        //echo $select;
        //exit;
        $result = $db->fetchRow($select);

        return $result;
    }

    public function getConfigcheckcopy($IdProgram = 0 , $IdProgramScheme = 0, $category = 0)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'tbl_documentchecklist_main'))
            ->joinLeft(array('d' => 'tbl_program'), 'd.IdProgram = a.IdProgram', array('ProgramID' => 'a.IdProgram','ProgramName' => 'd.ProgramName'))
            ->joinLeft(array('e' => 'tbl_program_scheme'), 'e.IdProgramScheme = a.IdProgramScheme')
            ->joinLeft(array('f' => 'registry_values'), 'f.id = a.category', array('studentCategory' => 'f.name'))
            ->where('a.IdProgram = ?', $IdProgram)
            ->where('a.IdProgramScheme = ?', $IdProgramScheme)
            ->where('a.category = ?', $category)
            ->order('a.created_at desc');
        //echo $select;
        //exit;
        $result = $db->fetchRow($select);

        return $result;
    }

    public function getAllConfig($formData = null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'tbl_documentchecklist_main'))
            ->join(array('c' => 'tbl_scheme'), 'c.IdScheme = a.IdScheme', array('SchemeName' => 'c.EnglishDescription','SchemeID' => 'c.IdScheme'))
            ->joinLeft(array('d' => 'tbl_program'), 'd.IdProgram = a.IdProgram', array('ProgramID' => 'a.IdProgram','ProgramName' => 'd.ProgramName'))
            ->joinLeft(array('e' => 'tbl_program_scheme'), 'e.IdProgramScheme = a.IdProgramScheme')
            ->joinLeft(array('f' => 'registry_values'), 'f.id = a.category', array('studentCategory' => 'f.name'))
            ->joinLeft(array('g' => 'tbl_award_level'), 'd.Award = g.Id', array('awardName' => 'g.GradeDesc'))
            ->joinleft(array('h'=>'registry_values'), 'e.mode_of_program = h.id', array('mopName'=>'h.name'))
            ->joinleft(array('i'=>'registry_values'), 'e.program_type = i.id', array('ptName'=>'i.name'))
            /*->where('a.IdScheme = ?', $IdScheme)
            ->where('a.IdProgram = ?', $IdProgram)
            ->where('a.IdProgramScheme = ?', $IdProgramScheme)
            ->where('a.category = ?', $category)*/
            ->order('a.created_at desc');


        //echo $select;
        //exit;
        $result = $db->fetchAll($select);

        return $result;
    }

    public function add($data) {
        $this->insert($data);
    }

    public function updateData($data,$Id) {
        $where = 'dcl_Id = '.$Id;
        $this->update($data,$where);
    }

    public function deleteData($id){
        if($id!=0){
            $this->delete("dcl_Id = ".(int)$id);
        }
    }
}



?>
