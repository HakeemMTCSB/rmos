<?php

class Application_Model_DbTable_Candidate extends Zend_Db_Table { //Model Class for Users Details

  protected $_name = 'applicant_ptest';

  public function init() {
    $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  }

  public function fnGetPtest($IdSchedule,$order="appl_fname",$room_id="") {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
/*    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("pt" => "applicant_ptest"), array("pt.*"))
                    ->where('apt_aps_id = ?', $IdSchedule)
                    ->where('apt_sit_no is not null');*/
    $swhere="";
    if($room_id!=""){
    	$swhere = "and apt_room_id='$room_id'";
    }
    $lstrSelect=" SELECT `pt`.*,ap.*, at.at_appl_id, at.at_pes_id FROM `applicant_ptest` AS `pt` 
    			inner join 	 applicant_transaction as at ON pt.apt_at_trans_id=at.at_trans_id 
    			inner join 	applicant_profile as ap On at.at_appl_id=ap.appl_id
    			WHERE (apt_aps_id = '$IdSchedule') AND (apt_sit_no is not null) $swhere order by  $order "; 
    //->where("pt.Active = 1");
    //echo $lstrSelect;
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }
  
  public function fnGetPhoto($trnID,$fullpath=false){
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$sql="select pathupload from appl_upload_file where auf_appl_id='$trnID' and auf_file_type=33";
  
  	$larrResult = $lobjDbAdpt->fetchAll($sql);
    
  	
    //return str_replace("/var/www/html/triapp","",$larrResult[0]["pathupload"]);
    if($fullpath){
    	return $larrResult[0]["pathupload"];
    }else{
    	return str_replace("/var/www/html/triapp","http://www.spmb.trisakti.ac.id",$larrResult[0]["pathupload"]);
    }  
  }

}