<?php
class Application_Model_DbTable_Verifyprogramchecklist extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_varifiedprogramchecklist';
	
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	
	public function geteducationdetails(){
		$select = $this->select()
			  	 ->setIntegrityCheck(false) 
			  	->from(array('a' => 'tbl_studentapplication'),array('a.FName','a.MName','a.LName','a.IdApplication','a.IdApplicant','a.PermAddressDetails','a.ProvisionalProgramOffered','a.HomePhone','a.CellPhone','a.Fax')) 
			  	->join(array('e' => 'tbl_programchecklist'),'e.IdProgram = a.ProvisionalProgramOffered AND e.Active = "1"',array('e.IdCheckList','e.Description AS Desc','e.IdProgram AS CheckProgram','e.Mandatory'))	
                ->join(array('b'=>'tbl_program'),'a.ProvisionalProgramOffered  = b.IdProgram',array('b.ProgramName','b.IdProgram'))
                ->join(array('ptm'=>'tbl_placementtestmarks'),'a.IdApplication = ptm.IdApplication',array())
                //->join(array('c'=>'tbl_definationms'),'b.Award = c.idDefinition')
                //->joinLeft(array('d'=>'tbl_collegemaster'),'a.idCollege = d.IdCollege')
                ->joinLeft(array('f'=>'tbl_varifiedprogramchecklist'),'a.IdApplication = f.IdApplication AND f.IdCheckList= e.IdCheckList',array('f.Varified','f.Comments','f.Status AS Stat'))
				->joinLeft(array('g'=>'tbl_city'),'a.PermCity = g.idCity',array('g.CityName'))
				->where('a.registrationId IS NULL')
                ->order("a.FName");
                $select	->order("e.Description");
		$result = $this->fetchAll($select);
		return $result->toArray();	
		
	
	}
	
	
	
public function fnSearchEducationlist($post) { //Function to get the Program Branch details
		$field7 = "a.Active = ".$post["field7"];
		$select = $this->select()
			  	 ->setIntegrityCheck(false)  
			  	->from(array('a' => 'tbl_studentapplication'),array('a.FName','a.MName','a.LName','a.IdApplication','a.IdApplicant','a.PermAddressDetails','a.ProvisionalProgramOffered','a.HomePhone','a.CellPhone','a.Fax')) 
			  	->join(array('e' => 'tbl_programchecklist'),'e.IdProgram = a.ProvisionalProgramOffered AND e.Active = "1"',array('e.IdCheckList','e.Description AS Desc','e.IdProgram AS CheckProgram','e.Mandatory'))	
                ->join(array('b'=>'tbl_program'),'a.ProvisionalProgramOffered  = b.IdProgram',array('b.ProgramName','b.IdProgram'))
                ->join(array('ptm'=>'tbl_placementtestmarks'),'a.IdApplication = ptm.IdApplication',array())
                //->join(array('c'=>'tbl_definationms'),'b.Award = c.idDefinition')
                //->joinLeft(array('d'=>'tbl_collegemaster'),'a.idCollege = d.IdCollege')
                ->joinLeft(array('f'=>'tbl_varifiedprogramchecklist'),'a.IdApplication = f.IdApplication AND f.IdCheckList= e.IdCheckList',array('f.Varified','f.Comments','f.Status AS Stat'))
				->joinLeft(array('g'=>'tbl_city'),'a.PermCity = g.idCity',array('g.CityName'))
				->where('a.registrationId IS NULL');
				if(isset($post['field3']) && !empty($post['field3'])){
				 $select =  $select->where('a.FName like "%" ? "%"',$post['field3']);   
				}
				if(isset($post['field2']) && !empty($post['field2']) ){
				  $select = $select->where('a.IdApplicant like "%" ? "%"',$post['field2']);
			    }	
				if(isset($post['field5']) && !empty($post['field5']) ){
				  $select = $select->where("e.IdProgram  = ?",$post['field5']);
			    }
			    	
//			    if(isset($post['field8']) && !empty($post['field8']) ){
//				  $select = $select->where("a.IdCollege  = ?",$post['field8']);
//			    }	
		$select	->order("a.FName");
		$select	->order("e.Description");		
		$result = $this->fetchAll($select);
		return $result->toArray();
     }	

public function fngetStatus()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("a"=>"tbl_definationtypems"),array("a.idDefType"))	
						->join(array("b"=>"tbl_definationms"),'b.idDefType = a.idDefType',array("b.DefinitionDesc"))
						->where('a.defTypeDesc = "Status"');	
		return $result = $lobjDbAdpt->fetchAll($select);
	}
	
public function fninsertNewMarks($larrformdata)
	{		
		$lstrinsert = "";
		$thjdbAdpt = Zend_Db_Table::getDefaultAdapter();
		for($icount = 0;$icount < count($larrformdata['IdCheckList']);$icount++ )
			$lstrinsert .= "(".$larrformdata['IdApplication'][$icount].",".$larrformdata['IdCheckList'][$icount].",".$larrformdata['IdProgram'][$icount].",
			1,".$larrformdata['Stat'][$icount].",".$larrformdata['Comments'][$icount].",".$larrformdata['UpdUser'].",'".$larrformdata['UpdDate']."'),";

		
		
		$thjdbAdpt->query("INSERT INTO tbl_varifiedprogramchecklist(IdApplication,IdCheckList,IdProgram,Varified,Status,Comments,UpdUser,UpdDate) 
		VALUES ".substr($lstrinsert,0,-1));	
	}
	
	
	
public function fndeleteOldMarks($IdPrograms,$IdApplication)
	{
		$thjdbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrwhere = "IdApplication = ".$IdApplication." AND IdProgram = ".$IdPrograms;
		
		$thjdbAdpt->delete('tbl_varifiedprogramchecklist',$lstrwhere);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	  
	
}