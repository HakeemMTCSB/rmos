<?php 

class Application_Model_DbTable_ApplicantDocumentStatus extends Zend_Db_Table_Abstract {
	
	/**
	 * The default table name 
	 */
	
	protected $_name = 'applicant_document_status';
	protected $_primary = "ads_id";


	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
 	public function getApplicantDocumentStatus($txnid,$dcl_id){
    	    	
    	$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				->from(array('ads'=>$this->_name) )
				->joinLeft(array('ad'=>'applicant_documents'),'ad.ad_id = ads.ads_ad_id AND ad.ad_txn_id=ads.ads_txn_id' )
				->where("ads.ads_txn_id = ?",$txnid)
				->where("ads.ads_dcl_id = ?",$dcl_id);
		$row = $db->fetchRow($select);
		return $row;    	
    }
    
	public function getApplicantDocumentType($txnid,$fee_id){
    	    	
    	$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				->from(array('ads'=>$this->_name) )
				->joinLeft(array('ap'=>'applicant_program'),'ap.ap_at_trans_id = ads.ads_txn_id' )
				->joinLeft(array('ad'=>'applicant_documents'),'ad.ad_id = ads.ads_ad_id AND ad.ad_txn_id=ads.ads_txn_id' )
				->joinLeft(array('adcl'=>'tbl_documentchecklist_dcl'),'adcl.dcl_Id = ads.ads_dcl_id' )
				->joinLeft(array('fsp'=>'fee_structure_program'),'fsp.fsp_program_id = ap.ap_prog_id AND fsp.fsp_idProgramScheme = ap.ap_prog_scheme' )
				->joinLeft(array('fspi'=>'fee_structure_program_item'),'fspi.fspi_program_id = fsp.fsp_id AND fspi.fspi_document_type = adcl.dcl_uplType' )
				->where("ads.ads_txn_id = ?",$txnid)
				->where("fspi.fspi_fee_id = ?",$fee_id)
				->group('fspi.fspi_document_type');
				
				//->where("ads.ads_dcl_id = ?",$dcl_id);
		$row = $db->fetchRow($select);
		return $row;    	
    }
    
	public function getApplicantDocumentList($txnid){
    	    	
    	$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				->from(array('ads'=>$this->_name) )
				->where("ads.ads_txn_id = ?",$txnid);
				
		$row = $db->fetchAll($select);
		return $row;    	
    }
    
}

?>