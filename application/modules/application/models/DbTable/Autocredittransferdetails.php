<?php
class Application_Model_DbTable_Autocredittransferdetails extends Zend_Db_Table {
	protected $_name = 'tbl_autocredittransferdetails';

	/**
	 *
	 * @see Zend_Db_Table_Abstract::init()
	 */
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fnaddautocreditdetails($data) {
		$this->insert($data);
	}
	
	public function fnupdateautocredit($Id,$data) {
		$where = 'Id = '.$Id;
		$this->update($data,$where);
	}

	public function fngetautocreditdetails($IdAutoCredit) {
		$lstrSelect = $this->select()
		->setIntegrityCheck(false)
		->from(array("a"=>"tbl_autocredittransferdetails"))
		->join(array('b' => 'tbl_subjectmaster'),'a.IdCourse =b.IdSubject',array('b.SubjectName as Course','a.IdSubject as IdCourse'))
		->join(array('c' => 'tbl_subject'),'a.IdSubject	 =c.IdSubject',array('c.SubjectName as Subject','a.IdSubject as IdSubject','c.SubjectCode as SubjectCode'))
		->where("a.IdAutoCredit = $IdAutoCredit");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fndeleteautocreditdetails($deletedrows) {
		$table = "tbl_autocredittransferdetails";
		$temp = explode(",",$deletedrows);
		foreach($temp as $id) {
			$where = $this->lobjDbAdpt->quoteInto('Id = ?', $id);
			$this->lobjDbAdpt->delete($table, $where);
		}
	}

}