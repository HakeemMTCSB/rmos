<?php

class Application_Model_DbTable_Migrate_AdmRawAddress extends Zend_Db_Table_Abstract
{
    /**
     * The default table name
     */
    protected $_name = 'migrate_adm_raw_address';
    protected $_primary = "id";

    public function getData()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name));

        $row = $db->fetchAll($select);

        return $row;
    }

}

