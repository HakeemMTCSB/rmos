<?php

class Application_Model_DbTable_Migrate_AdmRawData extends Zend_Db_Table_Abstract
{
    /**
     * The default table name
     */
    protected $_name = 'migrate_adm_raw_data';
    protected $_primary = "id";

    public function getData($count = 0)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name), array('*', 'total' => 'COUNT(*)', 'total_branch' => 'COUNT(DISTINCT ARD_BRANCH_ID)'))
            ->joinLeft(array('b' => 'tbl_intake'), 'a.ARD_INTAKE = b.IntakeId',array('b.IdIntake','b.IntakeDesc'))
            ->order('b.ApplicationStartDate DESC');

        if ($count == 1) {
            $select->group('a.ARD_INTAKE');
        }

        $row = $db->fetchAll($select);

        return $row;
    }

    public function getDataApplicant($where = array())
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name), array('*'))
            ->joinLeft(array('b' => 'tbl_intake'), 'a.ARD_INTAKE = b.IntakeId',array('b.IdIntake','b.IntakeDesc'));

        if(count($where) > 0){
            foreach ($where as $item=>$value){
                $select->where($item.' = ? ',$value);
            }
        }

        $row = $db->fetchAll($select);

        return $row;
    }

}

