<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_ChecklistVerification extends Zend_Db_Table_Abstract {
    //put your code here
    protected $_name = 'applicant_document_status';
    protected $_primary = "ads_id";
    
    /*
     * list aplicant
     * 
     * @on 7/7/2014
     */
    public function fnGetApplicantList($where=null){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_transaction"),array("value"=>"a.*"))
            ->join(array('b'=>'applicant_profile'), 'a.at_appl_id = b.appl_id')
            ->join(array('c'=>'applicant_program'), 'c.ap_at_trans_id = a.at_trans_id')
            ->join(array('d'=>'tbl_intake'), 'd.IdIntake = a.at_intake')
            ->join(array('e'=>'tbl_program_scheme'), 'e.IdProgramScheme = c.ap_prog_scheme')
            ->join(array('f'=>'tbl_program'), 'f.IdProgram = e.IdProgram')
            //->join(array('g'=>'tbl_status_sts'), 'g.sts_Id = a.at_status')
            ->order("a.at_trans_id DESC");
        
        if ($where!=null){
            $lstrSelect->where($where);
        }
        
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * list document checklist
     * 
     * @on 7/7/2014
     */
    public function fnGetDocCheckList($programSchemeId, $stdCtgy, $userId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_documentchecklist_dcl"),array("value"=>"a.*"))
            ->joinLeft(array('b'=>'tbl_application_section'), 'b.id = a.dcl_sectionid')
            ->where("a.dcl_programScheme = ?", $programSchemeId)
            ->where("a.dcl_stdCtgy = ?", $stdCtgy)
            ->where("a.dcl_activeStatus = ?", 1)
            ->where("a.dcl_specific = 0 or a.dcl_specific = ?", $userId)
            ->order("a.dcl_priority")
            ->order("a.dcl_name");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }
    
    /*
     * get defination
     * 
     * @on 8/7/2014
     */
    static function fnGetDefination($id){
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
        ->from(array("a"=>"tbl_definationms"),array("value"=>"a.DefinitionDesc"))
        ->where("a.idDefinition = ?", $id);
    $larrResult = $lobjDbAdpt->fetchOne($lstrSelect);
    return $larrResult;
    }

    static function fnGetAwardDesc($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_award_level"),array("value"=>"a.GradeDesc"))
            ->where("a.id = ?", $id);
        $larrResult = $lobjDbAdpt->fetchOne($lstrSelect);
        return $larrResult;
    }
    
    /*
     * get upload status
     * 
     * @on 8/7/2014
     */
    static function fnGetUploadStatus($txnId, $dclId, $column, $tableName, $sectionId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_documents"),array("value"=>"a.".$column))
            ->where("a.ad_dcl_id = ?", $dclId)
            ->where("a.ad_table_id = ?", $tableName)
            ->where("a.ad_section_id = ?", $sectionId)
            ->where("a.ad_txn_id = ?", $txnId);
	$larrResult = $lobjDbAdpt->fetchOne($lstrSelect);
	return $larrResult;
    }
    
    /*
     * search applicant
     * 
     * @on 8/7/14
     */
    public function getApplicantSearchList($formData=null, $where=null){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_transaction"),array("value"=>"a.*"))
            ->join(array('b'=>'applicant_profile'), 'a.at_appl_id = b.appl_id')
            ->join(array('c'=>'applicant_program'), 'c.ap_at_trans_id = a.at_trans_id')
            ->join(array('d'=>'tbl_intake'), 'd.IdIntake = a.at_intake')
            ->join(array('e'=>'tbl_program_scheme'), 'e.IdProgramScheme = c.ap_prog_scheme')
            ->join(array('f'=>'tbl_program'), 'f.IdProgram = e.IdProgram')
            ->order("a.at_trans_id DESC");
        
        if ($where!=null){
            $lstrSelect->where($where);
        }
        
        if(isset($formData)){

            if (isset($formData['field1']) && $formData['field1']!=''){
                $lstrSelect->where($lobjDbAdpt->quoteInto("CONCAT_WS(' ', appl_fname, appl_lname) LIKE ?", "%".$formData['field1']."%"));
            }
            if (isset($formData['field5']) && $formData['field5']!=''){
                $lstrSelect->where($lobjDbAdpt->quoteInto("appl_mname LIKE ?", "%".$formData['field5']."%"));
            }
            if (isset($formData['field6']) && $formData['field6']!=''){
                $lstrSelect->where($lobjDbAdpt->quoteInto("appl_lname LIKE ?", "%".$formData['field6']."%"));
            }
            if (isset($formData['field7']) && $formData['field7']!=''){
                $lstrSelect->where($lobjDbAdpt->quoteInto("at_pes_id LIKE ?", "%".$formData['field7']."%"));
            }
            if (isset($formData['field2']) && $formData['field2']!=''){
                $lstrSelect->where($lobjDbAdpt->quoteInto('ap_prog_scheme = ?', $formData['field2']));
            }
            if (isset($formData['field3']) && $formData['field3']!=''){
                $lstrSelect->where($lobjDbAdpt->quoteInto('e.IdProgram = ?', $formData['field3']));
            }
            if (isset($formData['field4']) && $formData['field4']!=''){
                $lstrSelect->where($lobjDbAdpt->quoteInto('appl_category = ?', $formData['field4']));
            }
            if (isset($formData['field9']) && $formData['field9']!=''){
                $lstrSelect->where($lobjDbAdpt->quoteInto('at_intake = ?', $formData['field9']));
            }
            if (isset($formData['at_status']) && $formData['at_status']!=''){
                $lstrSelect->where($lobjDbAdpt->quoteInto('at_status = ?', $formData['at_status']));
            }
	}
        
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get applicant doc checklist status by trans id
     * 
     * @on 8/7/2014
     */
    public function fnGetChecklistStatusByTxnId($txnId, $dclId, $tblId, $sectionId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_document_status"),array("value"=>"a.*"))
            ->where("a.ads_txn_id = ?", $txnId)
            ->where("a.ads_dcl_id = ?", $dclId)
            ->where("a.ads_table_id = ?", $tblId)
            ->where("a.ads_section_id = ?", $sectionId)
            ->order("a.ads_id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    public function fnGetChecklistStatusByTxnId2($txnId, $dclId, $tblId, $sectionId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_document_status"),array("value"=>"a.*"))
            ->where("a.ads_txn_id = ?", $txnId)
            ->where("a.ads_dcl_id = ?", $dclId)
            //->where("a.ads_table_id = ?", $tblId)
            ->where("a.ads_section_id = ?", $sectionId)
            ->order("a.ads_id");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get applicant doc checklist status by trans id
     * 
     * @on 8/7/2014
     */
    public function fnGetChecklistStatus($txnId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_document_status"),array("value"=>"a.*"))
            ->join(array("b"=>"tbl_documentchecklist_dcl"), "b.dcl_Id = a.ads_dcl_id")
            ->where("a.ads_txn_id = ?", $txnId)
            ->where("b.dcl_mandatory = ?", 1)
            ->order("a.ads_dcl_id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * universal query once
     * 
     * @on 8/7/2014
     */
    static function universalQuery($table, $column, $where=null){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>$table),array("value"=>"a.".$column));
            
            if ($where!=null){
                $lstrSelect->where($where);
            }
            
	$larrResult = $lobjDbAdpt->fetchOne($lstrSelect);
	return $larrResult;
    }
    
    /*
     * universal query row
     * 
     * @on 6/8/2014
     */
    static function universalQueryRow($table, $column, $where=null, $order=null){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>$table),array("value"=>"a.".$column));
            
            if ($where!=null){
                $lstrSelect->where($where);
            }
            if ($order!=null){
                $lstrSelect->order($order);
            }
            
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * universal query row
     * 
     * @on 6/8/2014
     */
    static function universalQueryAll($table, $column, $where = null, $tableJoin, $columnJoin1, $columnJoin2){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>$table),array("value"=>"a.".$column));
        
            if ($tableJoin!='0' && $columnJoin1!='0' && $columnJoin2!='0'){
                $lstrSelect->joinLeft(array("b"=>$tableJoin), "a.".$columnJoin1." = b.".$columnJoin2);
            }
            
            if ($where!=null){
                $lstrSelect->where($where);
            }
         
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
	public function universalQueryAllTable($table, $column, $where = null, $tableJoin, $columnJoin1, $columnJoin2){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

		$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>$table),array("value"=>"a.".$column));
        
            if ($tableJoin!='0' && $columnJoin1!='0' && $columnJoin2!='0'){
                $lstrSelect->joinLeft(array("b"=>$tableJoin), "a.".$columnJoin1." = b.".$columnJoin2);
            }
            
            if ($where!=null){
                $lstrSelect->where($where);
            }
 
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * update applicant trans
     * 
     * @on 9/7/2014
     */
    static function updateAppTrans($data, $id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrUpdate = $lobjDbAdpt->update('applicant_transaction', $data, 'at_trans_id = '.$id);
	return $lstrUpdate;
    }
    
    /*
     * info box
     * 
     * @on 9/7/2014
     */
    public function infoBox($txnId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_transaction"),array("value"=>"a.*"))
            ->join(array('b'=>'applicant_profile'), 'a.at_appl_id = b.appl_id')
            ->join(array('c'=>'applicant_program'), 'c.ap_at_trans_id = a.at_trans_id')
            ->join(array('d'=>'tbl_intake'), 'd.IdIntake = a.at_intake')
            ->join(array('e'=>'tbl_program_scheme'), 'e.IdProgramScheme = c.ap_prog_scheme')
            ->join(array('f'=>'tbl_program'), 'f.IdProgram = e.IdProgram')
            ->where('a.at_trans_id = ?', $txnId)
            ->order("a.at_trans_id");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * store status update history
     * 
     * @on 17/7/2014
     */
    public function storeStatusUpdateHistory($data){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrInsert = $lobjDbAdpt->insert('applicant_status_history', $data);
        return $lstrInsert;
    }
    
    /*
     * get download info
     * 
     * @on 6/8/2014
     */
    static function fnGetDownloadInfo($adId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_documents"),array("value"=>"a.*"))
            ->where("a.ad_id = ?", $adId);
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    public function getDocumentUpload($txnId, $dclId, $sectionId, $tableId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_documents"),array("value"=>"a.*"))
            ->where("a.ad_txn_id = ?", $txnId)
            ->where("a.ad_dcl_id = ?", $dclId)
            ->where("a.ad_section_id = ?", $sectionId)
            ->where("a.ad_table_id = ?", $tableId);
        //echo $lstrSelect; exit;
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    public function insertUploadData($data){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $insert = $lobjDbAdpt->insert("applicant_documents", $data);

		$insert_id = $lobjDbAdpt->lastInsertId();
        return $insert_id;
    }
    
    public function deleteUploadFileInfo($ad_id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_documents"),array("value"=>"a.*"))
            ->where("a.ad_id = ?", $ad_id);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    public function deleteUploadFile($ad_id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $delete = $lobjDbAdpt->delete('applicant_documents', 'ad_id = '.$ad_id);
        return $delete;
    }
    
    public function insertNewChecklist($bind){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $insert = $lobjDbAdpt->insert('tbl_documentchecklist_dcl', $bind);
        return $insert;
    }
    
    public function getLastStatusChange($trnid){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_status_history"),array("value"=>"a.*"))
            ->where("a.ash_trans_id = ?", $trnid)
            ->order("a.ash_id DESC");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    public function getStatus($txnId, $dclId, $secId, $tblId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_document_status'))
            ->where('a.ads_txn_id = ?', $txnId)
            ->where('a.ads_dcl_id = ?', $dclId)
            ->where('a.ads_section_id = ?', $secId)
            ->where('a.ads_table_id = ?', $tblId);

        $result  = $db->fetchRow($select);
        return $result;
    }

    public function updateStatus($bind, $adsId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('applicant_document_status', $bind, 'ads_id = '.$adsId);
        return $update;
    }

    public function insertStatus($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('applicant_document_status', $bind);
        $id = $db->lastInsertId('applicant_document_status', 'ads_id');
        return $id;
    }

    public function checkUplFile($txnId, $dclId, $secId, $tblId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_documents'), array('value'=>'*'))
            ->where('a.ad_txn_id = ?', $txnId)
            ->where('a.ad_dcl_id = ?', $dclId)
            ->where('a.ad_section_id = ?', $secId)
            ->where('a.ad_table_id = ?', $tblId);

        $result  = $db->fetchRow($select);
        return $result;
    }

    public function getUplInfo($adId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_documents'), array('value'=>'*'))
            ->where('a.ad_id = ?', $adId);

        $result  = $db->fetchRow($select);
        return $result;
    }

    public function deleteStatusData($adId){
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->delete('applicant_document_status', 'ads_id = '.$adId);
    }
    
    public function getReceiptId($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'receipt'), array('value'=>'*'))
            ->where('a.rcp_trans_id = ?', $id)
            ->where("a.rcp_status = 'APPROVE'");
        
        $result  = $db->fetchAll($select);
        return $result;
    }
    
    public function getMigrateApplicant(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_profile'), array('value'=>'*'))
            ->join(array('b'=>'applicant_transaction'), 'a.appl_id = b.at_appl_id')
            ->where('a.MigrateCode is not null');
        
        $result  = $db->fetchAll($select);
        return $result;
    }
    
    public function deleteStatusByTrx($trxId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('applicant_document_status', 'ads_txn_id = '.$trxId.' AND ads_table_id = 0');
        return $delete;
    }
    
    public function deleteStatusByTrx2($trxId, $tableid=0){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('applicant_document_status', 'ads_txn_id = '.$trxId.' AND ads_table_id = '.$tableid);
        return $delete;
    }
    
    public function getWaivedProforma($transId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'proforma_invoice_main'), array('value'=>'*'))
            ->where('a.trans_id = ?', $transId)
            ->where('a.status = "E" OR a.status = "W" OR a.status = "S"');
        
        $result  = $db->fetchAll($select);
        return $result;
    }
    
    public function getDclById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_documentchecklist_dcl'), array('value'=>'*'))
            ->where('a.dcl_Id = ?', $id);
        
        $result  = $db->fetchRow($select);
        return $result;
    }
    
    public function getChecklistStatus($txnid, $dclid, $secid, $tblid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_document_status'), array('value'=>'*'))
            ->where('a.ads_txn_id = ?', $txnid)
            ->where('a.ads_dcl_id = ?', $dclid)
            ->where('a.ads_section_id = ?', $secid)
            ->where('ads_table_id = ?', $tblid);
        
        $result  = $db->fetchRow($select);
        return $result;
    }
    
    public function deleteChecklistSesat($id, $idarr){
        $arr = implode(',', $idarr);
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('applicant_document_status', 'ads_txn_id = '.$id.' AND ads_id NOT IN ('.$arr.')');
        return $delete;
    }
}
?>