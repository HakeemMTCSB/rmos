<?php
class Application_Model_DbTable_AgentProfile extends Zend_Db_Table {
	protected $_name = 'tbl_agent'; // table name
	private $lobjDbAdpt;

	public function init(){
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fnaddAgent($larrformData) {
		if(isset($larrformData['phone_no']) && $larrformData['phone_no'] != ''){
            unset($larrformData['phone_no']);
		}

		if(isset($larrformData['fax_no']) && $larrformData['fax_no'] != ''){
            unset($larrformData['fax_no']);
		}

		$this->lobjDbAdpt->insert('tbl_agent',$larrformData);//insert agent

		$larrreglistdata = $larrformData;
		unset($larrformData['name']);
		unset($larrformData['code']);
        unset($larrformData['type']);
        unset($larrformData['payment_plan_id']);
        unset($larrformData['status_id']);
        unset($larrformData['ContactPerson']);
		unset($larrformData['Address1']);
		unset($larrformData['Address2']);
		unset($larrformData['City']);
        unset($larrformData['State']);
        unset($larrformData['Country']);
        unset($larrformData['Zip']);
        unset($larrformData['email']);
        unset($larrformData['password']);
        unset($larrformData['created_at']);
        unset($larrformData['created_by']);

		$lid = $this->lobjDbAdpt->lastInsertId();
		$larrreglistdata['id'] =  $lid;

		$insertId = $this->lobjDbAdpt->lastInsertId($this->_name,'id');
		return $insertId;
	}

	public function fnupdateInstitution($formData,$lintIdInstitution) {
		if(isset($formData['InstitutionPhoneNumber']) && $formData['InstitutionPhoneNumber'] != ''){
			$formData['InstitutionPhoneNumber'] = $formData['Phonecountrycode']."-".$formData['Phonestatecode']."-".$formData['InstitutionPhoneNumber'];
		}
		unset($formData['Phonecountrycode']);
		unset($formData['Phonestatecode']);
		if(isset($formData['InstitutionFaxNumber']) && $formData['InstitutionFaxNumber'] != ''){
			$formData['InstitutionFaxNumber'] = $formData['faxcountrycode']."-".$formData['faxstatecode']."-".$formData['InstitutionFaxNumber'];
		}
		unset($formData['faxcountrycode']);
		unset($formData['faxstatecode']);

		$larrreglistdata = $formData;
		unset($formData['InstitutionName']);
		unset($formData['InstitutionShortName']);
		unset($formData['InstitutionAddress1']);
		unset($formData['InstitutionAddress2']);
		unset($formData['InstitutionCity']);
		unset($formData['Country']);
		unset($formData['State']);
		unset($formData['Zip']);
		unset($formData['City']);
		unset($larrreglistdata['IdSubject']);

		$where = 'IdInstitution = '.$lintIdInstitution;
		$this->update($larrreglistdata,$where);
	}

	public function fnDeleteInstitution($idInstitution) {  // function to delete Institution details
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = 'tbl_institution';
		$where = $db->quoteInto('idInstitution = ?', $idInstitution);
		$db->delete('tbl_institution', $where);
	}

	public function fngetAgentDetails() { //Function to get the Institution details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from($this->_name);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnSearchAgent($post = array()) { //Function for searching the Institution details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("ag"=>"tbl_agent"),array("ag.*"));
                        
                if (isset($post['field3']) && $post['field3']!=''){        
                    $lstrSelect->where('ag.name like "%" ? "%"',$post['field3']);
                }
                if (isset($post['field4']) && $post['field4']!=''){
                    $lstrSelect->where('ag.code like "%" ? "%"',$post['field4']);
                }
                if (isset($post['field29']) && $post['field29']!=''){
                    $lstrSelect->where('ag.Country = ?',$post['field29']);
                }
                
		$lstrSelect->order("ag.name");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetInstitutionList(){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_institution"),array("key"=>"a.idInstitution","value"=>"a.InstitutionName"))
		->order("a.InstitutionName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetInstituteCountry($IdInstitute){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_institution"),array("a.Country","a.State","a.City"))
		->where('a.idInstitution =?',$IdInstitute);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fngetSchoolListByCountry($countryId){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_institution"),array("key"=>"a.idInstitution","value"=>"a.InstitutionName"))
		->where('a.Country =?',$countryId);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function get_details($id) {
		$select = $this->lobjDbAdpt
			->select()
			->from(array('ag'=>'tbl_agent'), array('ag.*'))
			->joinLeft(array('Country' => 'tbl_countries'), 'ag.Country = Country.idCountry')
			->joinLeft(array('State' => 'tbl_state'), 'ag.State = State.idState')
			->where('id = ?', $id)
			;

		$agent = $this->lobjDbAdpt->fetchRow($select);
		return($agent);

	}

        public function checkInstitutName($institutName){
            $select = $this->lobjDbAdpt
                ->select()
                ->from(array('Institution'=>'tbl_institution'), array('Institution.*'))
                ->where('Institution.IdInstitution like "%" ? "%"',$institutName);
            $institute = $this->lobjDbAdpt->fetchRow($select);
            return $institute;
        }

}