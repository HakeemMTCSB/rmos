<?php
class Application_Model_DbTable_Studentapproval extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_placementtest';
	private $lobjstudentapplication;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table :: getDefaultAdapter();
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
	}

	public function fnSearchEducationlist($post) { //Function to get the Program Branch details
		$lobjplacementtest = new Application_Model_DbTable_Placementtestmarks();
		$lintscheme = $post['field1'];
		$lstrapplicantId = $post['field2'];
		$lintintake = $post['field5'];
		$lstrapplicantname = $post['field3'];
		$lintprogram = $post['field8'];
		$lstrextraid = $post['field4'];
		$lintapplicationstatus = $post['field24'];
		$lintagent = $post['field11'];
		$select = $this->select()->setIntegrityCheck(false)->from(array (
			'a' => 'tbl_studentapplication'
		), array (
			'a.IdApplicant',
			'a.IdApplication',
			'a.ExtraIdField1',
			'a.FName',
			'a.DateRejected',
			'a.DateCreated',
			'a.DateIncompleted',
			'a.Dateofferred',
			'a.OfferredBy',
			'a.DateRegistered',
			'a.RejectedBy',
			'a.IncompleteBy',
			'a.DateProvisionalOffered',
			'a.DateProvisionalOfferedBy',
			'a.OfferredBy',
			'a.RegisteredBy',
			'a.RejectedReason',
			'a.ReasonIncomplete',
			'a.ProvisionalProgramOffered',
			'a.Source'
		))->join(array (
			'b' => 'tbl_definationms'
		), 'a.Status = b.idDefinition', array (
			'b.DefinitionDesc AS Status',
		))->join(array (
			'c' => 'tbl_student_preffered'
		), 'a.IdApplication = c.IdApplication', array (
			'c.IdProgram',
			'c.IdIntake'
		))->join(array (
			'd' => 'tbl_program'
		), 'c.IdProgram = d.IdProgram', array (
			'd.ProgramName'
		))->joinLeft(array (
			'e' => 'tbl_intake'
		), 'c.IdIntake = e.IdIntake', array (
			'e.IntakeDesc'
		))->joinLeft(array (
			'f' => 'tbl_definationms'
		), 'a.RejectedReason = f.idDefinition', array (
			'f.DefinitionDesc AS RejectedReason'
		))->joinLeft(array (
			'g' => 'tbl_definationms'
		), 'a.ReasonIncomplete = g.idDefinition', array (
			'f.DefinitionDesc AS ReasonIncomplete'
		))->joinLeft(array (
			'h' => 'tbl_definationms'
		), 'a.Source = h.idDefinition', array (
			'h.DefinitionDesc AS SourceName'
		))->joinLeft(array (
			'i' => 'tbl_program'
		), 'a.ProvisionalProgramOffered = i.IdProgram', array (
			'i.ProgramName AS ProvisionalProgramOffered','i.IdProgram AS IdProvisionalProgram'
		))->joinLeft(array (
			'j' => 'tbl_program'
		), 'a.ProgramOfferred = j.IdProgram', array (
			'j.ProgramName AS ProgramOfferred'
		))->joinLeft(array (
			'k' => 'tbl_user'
		), 'a.OfferredBy = k.iduser OR a.RejectedBy = k.iduser OR a.IncompleteBy = k.iduser OR a.RegisteredBy = k.iduser OR a.DateProvisionalOffered = k.iduser', array (
			'k.loginName AS UserName'
		))->where('c.IdPriorityNo = 1');
		if(trim($lintscheme)!="") {
			$select->where("c.IdScheme = $lintscheme");
		}
		if(trim($lstrapplicantId) != "") {
			$select->where("a.IdApplicant LIKE '%$lstrapplicantId%'");
		}
		if(trim($lintintake) != "") {
			$select->where("c.IdIntake = $lintintake");
		}
		if(trim($lstrapplicantname) != "") {
			$select->where("a.FName LIKE '%$lstrapplicantname%'");
		}
		if(trim($lintprogram) != "") {
			$select->where("a.ProvisionalProgramOffered = $lintprogram OR a.ProgramOfferred = $lintprogram");
		}
		if(trim($lstrextraid) != "") {
			$select->where("a.ExtraIdField1 LIKE '%$lstrextraid%'");
		}
		if(trim($lintapplicationstatus) != "") {
			$select->where("b.idDefinition = $lintapplicationstatus");
		}
		$select->distinct("a.IdApplication");

		$larrResult = $this->lobjDbAdpt->fetchAll($select);
		$larrfinalresult = array();
		$db = Zend_Db_Table :: getDefaultAdapter();
		foreach($larrResult as $row) {
			$larrtemp = $lobjplacementtest->fngetAllMarksDetails($row['IdApplication']);
			$row['PlacementTestCount'] = "N";
			if(count($larrtemp) > 0) {
				$row['PlacementTestCount'] = "Y";
			}
			$larrfinalresult[] = $row;
		}
		return $larrfinalresult;
	}

	public function fngetapplicationinfo($IdApplication) {
		$select = $this->select()->setIntegrityCheck(false)->from(array (
			'a' => 'tbl_studentapplication'
		), array (
			'a.IdApplicant',
			'a.IdApplication',
			'a.ExtraIdField1',
			'a.FName',
			'a.DateRejected',
			'a.DateCreated',
			'a.DateIncompleted',
			'a.Dateofferred',
			'a.OfferredBy',
			'a.DateRegistered',
			'a.RejectedBy',
			'a.IncompleteBy',
			'a.DateProvisionalOffered',
			'a.DateProvisionalOfferedBy',
			'a.OfferredBy',
			'a.RegisteredBy',
			'a.RejectedReason',
			'a.ReasonIncomplete',
			'a.ProvisionalProgramOffered',
			'a.ProgramOfferred'
		))->join(array (
			'b' => 'tbl_definationms'
		), 'a.Status = b.idDefinition', array (
			'b.DefinitionDesc AS Status',
		))->join(array (
			'c' => 'tbl_student_preffered'
		), 'a.IdApplication = c.IdApplication', array (
			'c.IdProgram',
			'c.IdIntake'
		))->join(array (
			'd' => 'tbl_program'
		), 'c.IdProgram = d.IdProgram', array (
			'd.ProgramName'
		))->joinLeft(array (
			'e' => 'tbl_intake'
		), 'c.IdIntake = e.IdIntake', array (
			'e.IntakeDesc'
		))->joinLeft(array (
			'f' => 'tbl_definationms'
		), 'a.RejectedReason = f.idDefinition', array (
			'f.DefinitionDesc AS RejectedReason'
		))->joinLeft(array (
			'g' => 'tbl_definationms'
		), 'a.ReasonIncomplete = g.idDefinition', array (
			'f.DefinitionDesc AS ReasonIncomplete'
		))->joinLeft(array (
			'h' => 'tbl_definationms'
		), 'a.Source = h.idDefinition', array (
			'h.DefinitionDesc AS Source'
		))->joinLeft(array (
			'i' => 'tbl_program'
		), 'a.ProvisionalProgramOffered = i.IdProgram', array (
			'i.ProgramName AS ProvisionalProgramOffered','i.IdProgram AS IdProvisionalProgram'
		))->joinLeft(array (
			'j' => 'tbl_program'
		), 'a.ProgramOfferred = j.IdProgram ', array (
			'j.ProgramName AS ProgramOfferred'
		))->joinLeft(array (
			'k' => 'tbl_user'
		), 'a.OfferredBy = k.iduser OR a.RejectedBy = k.iduser OR a.IncompleteBy = k.iduser OR a.RegisteredBy = k.iduser OR a.DateProvisionalOfferedBy = k.iduser', array (
			'k.loginName AS UserName'
		))->joinLeft(array (
			'l' => 'tbl_collegemaster'
		), 'l.IdCollege = j.IdCollege OR l.IdCollege = i.IdCollege', array (
			'l.CollegeName AS FacultyName'
		))->where('c.IdPriorityNo = 1')
			->where('a.IdApplication = ?',$IdApplication);
		$larrResult = $this->lobjDbAdpt->fetchAll($select);
		return $larrResult[0];
	}

	public function fninsertStudentapproval($larrformdata) {
		$lobjDbAdpt = Zend_Db_Table :: getDefaultAdapter();
		$lstrTable = "tbl_sendoffer";
		$lobjDbAdpt->insert($lstrTable, $larrformdata);
	}

	public function fnOtherDocumentVarified($lintIdApplication) {
		$db = Zend_Db_Table :: getDefaultAdapter();
		$select = $db->select()->from(array (
			'a' => 'tbl_paymentvarification'
		), array (
			'a.*'
		))->where('a.IdApplication = ' .
		$lintIdApplication);
		$result = $db->fetchRow($select);
		return $result;
	}

	public function fngetchecklistdetails($lintidapplication, $lintidprogram) { //Function to get the Program Branch details
		$select = $this->select()->setIntegrityCheck(false)->from(array (
			'a' => 'tbl_varifiedprogramchecklist'
		), array (
			'a.Varified',
			'a.Comments'
		))->join(array (
			'b' => 'tbl_studentapplication'
		), 'a.IdApplication = b.IdApplication', array (
			'b.FName',
			'b.LName',
			'b.IdApplication',
			'b.StudentId'
		))->join(array (
			'c' => 'tbl_program'
		), 'b.IDCourse = c.IdProgram', array (
			'c.ProgramName'
		))->join(array (
			'd' => 'tbl_programchecklist'
		), 'a.IdCheckList = d.IdCheckList', array (
			'd.IdCheckList',
			'd.CheckListName'
		))->where('b.IdApplication = ' .
		$lintidapplication);

		$result = $this->fetchAll($select);
		return $result->toArray();
	}

	public function fnDeleteSendOffer($idapproved) { //Function for Delete Purchase order terms
		$db = Zend_Db_Table :: getDefaultAdapter();
		$table = "tbl_sendoffer";
		$where = $db->quoteInto('IdApprove = ?', $idapproved);
		$db->delete($table, $where);
	}

	/**
	 * Method to update applications which are in offered state
	 */
	public function fnupdateofferedapplicationstatus($lstrstatus, $larrformdata) {
		//echo "<pre>";print_r($larrformdata);die;
		$auth = Zend_Auth :: getInstance();
		$upduser = $auth->getIdentity()->iduser;
		$lintidapplication = $larrformdata['IdApplication'];
		$new_status = $this->fngetstatus($larrformdata['ApplicationStatus']);
		//echo $new_status;die;
		$db = Zend_Db_Table :: getDefaultAdapter();
		$table = "tbl_studentapplication";
		switch(trim($new_status)) {
			case "ENTRY":
				$larrdata['DateCreated'] = date('Y-m-d H:i:s');
				$larrdata['UpdUser'] = $upduser;
				$larrdata['UpdDate'] = date('Y-m-d H:i:s');
				$larrdata['Status'] = $larrformdata['ApplicationStatus'];
				$larrdata['ProvisionalProgramOffered'] = 0;
				$larrdata['ProgramOfferred'] = 0;
				$where = $db->quoteInto('IdApplication = ?', $lintidapplication);
				$db->update($table,$larrdata,$where);
				break;
			case "REJECT":
				$larrdata['DateRejected'] = date('Y-m-d H:i:s');
				$larrdata['RejectedBy'] = $upduser;
				$larrdata['RejectedReason'] = $larrformdata['RejectReason'];
				$larrdata['UpdUser'] = $upduser;
				$larrdata['UpdDate'] = date('Y-m-d H:i:s');
				$larrdata['Status'] = $larrformdata['ApplicationStatus'];
				$larrdata['ProvisionalProgramOffered'] = 0;
				$larrdata['ProgramOfferred'] = 0;
				$where = $db->quoteInto('IdApplication = ?', $lintidapplication);
				$db->update($table,$larrdata,$where);
				break;
			case "INCOMPLETE":
				$larrdata['DateIncompleted'] = date('Y-m-d H:i:s');
				$larrdata['IncompleteBy'] = $upduser;
				$larrdata['ReasonIncomplete'] = $larrformdata['RejectReason'];
				$larrdata['UpdUser'] = $upduser;
				$larrdata['UpdDate'] = date('Y-m-d H:i:s');
				$larrdata['ProvisionalProgramOffered'] = 0;
				$larrdata['ProgramOfferred'] = 0;
				$larrdata['Status'] = $larrformdata['ApplicationStatus'];
				$where = $db->quoteInto('IdApplication = ?', $lintidapplication);
				$db->update($table,$larrdata,$where);
				break;
			case "PROV_OFFERED":
				$larrdata['DateProvisionalOffered'] = date('Y-m-d H:i:s');
				$larrdata['DateProvisionalOfferedBy'] = $upduser;
				$larrdata['ProvisionalProgramOffered'] = $larrformdata['IdProgram'];
				$larrdata['UpdUser'] = $upduser;
				$larrdata['Offered'] = '1';
				$larrdata['UpdDate'] = date('Y-m-d H:i:s');
				$larrdata['Status'] = $larrformdata['ApplicationStatus'];
				$where = $db->quoteInto('IdApplication = ?', $lintidapplication);
				$db->update($table,$larrdata,$where);
				break;

		}
		$historyArray = array('IdApplication' => $lintidapplication,'status' => $larrformdata['ApplicationStatus'],'UpdDate' => date('Y-m-d H:i:s') , 'UpdUser' => $upduser);
       	$this->lobjstudentapplication->fnUpdateHistoryApplication($historyArray);
	}

	/**
	 * Method to update applications which are in reject state
	 */
	public function fnupdaterejectapplicationstatus($lstrstatus, $larrformdata) {
		//echo "<pre>";print_r($larrformdata);die;
		$auth = Zend_Auth :: getInstance();
		$upduser = $auth->getIdentity()->iduser;
		$lintidapplication = $larrformdata['IdApplication'];
		$new_status = $this->fngetstatus($larrformdata['ApplicationStatus']);
		//echo $new_status;die;
		$db = Zend_Db_Table :: getDefaultAdapter();
		$table = "tbl_studentapplication";
		switch(trim($new_status)) {
			case "ENTRY":
				$larrdata['DateCreated'] = date('Y-m-d H:i:s');
				$larrdata['UpdUser'] = $upduser;
				$larrdata['UpdDate'] = date('Y-m-d H:i:s');
				$larrdata['Status'] = $larrformdata['ApplicationStatus'];
				$larrdata['ProvisionalProgramOffered'] = 0;
				$larrdata['ProgramOfferred'] = 0;
				$where = $db->quoteInto('IdApplication = ?', $lintidapplication);
				$db->update($table,$larrdata,$where);
				break;
			case "PROV_OFFERED":
				$larrdata['DateProvisionalOffered'] = date('Y-m-d H:i:s');
				$larrdata['DateProvisionalOfferedBy'] = $upduser;
				$larrdata['ProvisionalProgramOffered'] = $larrformdata['IdProgram'];
				$larrdata['UpdUser'] = $upduser;
				$larrdata['UpdDate'] = date('Y-m-d H:i:s');
				$larrdata['Offered'] = '1';
				$larrdata['Status'] = $larrformdata['ApplicationStatus'];
				$larrdata['ProvisionalProgramOffered'] = 0;
				$larrdata['ProgramOfferred'] = 0;
				$where = $db->quoteInto('IdApplication = ?', $lintidapplication);
				$db->update($table,$larrdata,$where);
				break;

		}
		$historyArray = array('IdApplication' => $lintidapplication,'status' => $larrformdata['ApplicationStatus'],'UpdDate' => date('Y-m-d H:i:s') , 'UpdUser' => $upduser);
       	$this->lobjstudentapplication->fnUpdateHistoryApplication($historyArray);
	}

	/**
	 * Method to update applications which are in provisional state
	 */
	public function fnupdateprovisionalapplicationstatus($lstrstatus, $larrformdata) {
		//echo "<pre>";print_r($larrformdata);die;
		$auth = Zend_Auth :: getInstance();
		$upduser = $auth->getIdentity()->iduser;
		$lintidapplication = $larrformdata['IdApplication'];
		$new_status = $this->fngetstatus($larrformdata['ApplicationStatus']);
		//echo $new_status;die;
		$db = Zend_Db_Table :: getDefaultAdapter();
		$table = "tbl_studentapplication";
		switch(trim($new_status)) {
			case "ENTRY":
				$larrdata['DateCreated'] = date('Y-m-d H:i:s');
				$larrdata['UpdUser'] = $upduser;
				$larrdata['UpdDate'] = date('Y-m-d H:i:s');
				$larrdata['ProvisionalProgramOffered'] = 0;
				$larrdata['ProgramOfferred'] = 0;
				$larrdata['Status'] = $larrformdata['ApplicationStatus'];
				$where = $db->quoteInto('IdApplication = ?', $lintidapplication);
				$db->update($table,$larrdata,$where);
				break;
			case "REJECT":
				$larrdata['DateRejected'] = date('Y-m-d H:i:s');
				$larrdata['RejectedBy'] = $upduser;
				$larrdata['RejectedReason'] = $larrformdata['RejectReason'];
				$larrdata['UpdUser'] = $upduser;
				$larrdata['UpdDate'] = date('Y-m-d H:i:s');
				$larrdata['ProvisionalProgramOffered'] = 0;
				$larrdata['ProgramOfferred'] = 0;
				$larrdata['Status'] = $larrformdata['ApplicationStatus'];
				$where = $db->quoteInto('IdApplication = ?', $lintidapplication);
				$db->update($table,$larrdata,$where);
				break;
			case "INCOMPLETE":
				$larrdata['DateIncompleted'] = date('Y-m-d H:i:s');
				$larrdata['IncompleteBy'] = $upduser;
				$larrdata['ReasonIncomplete'] = $larrformdata['RejectReason'];
				$larrdata['UpdUser'] = $upduser;
				$larrdata['ProvisionalProgramOffered'] = 0;
				$larrdata['ProgramOfferred'] = 0;
				$larrdata['UpdDate'] = date('Y-m-d H:i:s');
				$larrdata['Status'] = $larrformdata['ApplicationStatus'];
				$where = $db->quoteInto('IdApplication = ?', $lintidapplication);
				$db->update($table,$larrdata,$where);
				break;

		}
		$historyArray = array('IdApplication' => $lintidapplication,'status' => $larrformdata['ApplicationStatus'],'UpdDate' => date('Y-m-d H:i:s') , 'UpdUser' => $upduser);
       	$this->lobjstudentapplication->fnUpdateHistoryApplication($historyArray);
	}

	/**
	 * Method to update applications which are in entry state
	 */
	public function fnupdateentryapplicationstatus($lstrstatus, $larrformdata) {
		$auth = Zend_Auth :: getInstance();
		$upduser = $auth->getIdentity()->iduser;
		$lintidapplication = $larrformdata['IdApplication'];
		$new_status = $this->fngetstatus($larrformdata['ApplicationStatus']);
		$db = Zend_Db_Table :: getDefaultAdapter();
		$table = "tbl_studentapplication";
		switch(trim($new_status)) {
			case "PROV_OFFERED":
				$larrdata['DateProvisionalOffered'] = date('Y-m-d H:i:s');
				$larrdata['DateProvisionalOfferedBy'] = $upduser;
				$larrdata['ProvisionalProgramOffered'] = $larrformdata['IdProgram'];
				$larrdata['UpdUser'] = $upduser;
				$larrdata['Offered'] = '1';
				$larrdata['UpdDate'] = date('Y-m-d H:i:s');
				$larrdata['Status'] = $larrformdata['ApplicationStatus'];
				$where = $db->quoteInto('IdApplication = ?', $lintidapplication);
				$db->update($table,$larrdata,$where);
				break;
			case "REJECT":
				$larrdata['DateRejected'] = date('Y-m-d H:i:s');
				$larrdata['RejectedBy'] = $upduser;
				$larrdata['ProvisionalProgramOffered'] = 0;
				$larrdata['ProgramOfferred'] = 0;
				$larrdata['RejectedReason'] = $larrformdata['RejectReason'];
				$larrdata['UpdUser'] = $upduser;
				$larrdata['UpdDate'] = date('Y-m-d H:i:s');
				$larrdata['Status'] = $larrformdata['ApplicationStatus'];
				$where = $db->quoteInto('IdApplication = ?', $lintidapplication);
				$db->update($table,$larrdata,$where);
				break;
			case "INCOMPLETE":
				$larrdata['DateIncompleted'] = date('Y-m-d H:i:s');
				$larrdata['IncompleteBy'] = $upduser;
				$larrdata['ProvisionalProgramOffered'] = 0;
				$larrdata['ProgramOfferred'] = 0;
				$larrdata['ReasonIncomplete'] = $larrformdata['RejectReason'];
				$larrdata['UpdUser'] = $upduser;
				$larrdata['UpdDate'] = date('Y-m-d H:i:s');
				$larrdata['Status'] = $larrformdata['ApplicationStatus'];
				$where = $db->quoteInto('IdApplication = ?', $lintidapplication);
				$db->update($table,$larrdata,$where);
				break;
		}
		$historyArray = array('IdApplication' => $lintidapplication,'status' => $larrformdata['ApplicationStatus'],'UpdDate' => date('Y-m-d H:i:s') , 'UpdUser' => $upduser);
       	$this->lobjstudentapplication->fnUpdateHistoryApplication($historyArray);
	}

	public function fngetstatus($lintdefi) {
		$lobjselect = $this->select()
						->setIntegrityCheck(false)
						->from(array('a' => "tbl_definationms"),array('a.DefinitionCode AS Code'))
						->where("idDefinition = ?",$lintdefi);
		$larrresult = $this->fetchAll($lobjselect);
		$larrresult = $larrresult->toArray();
		return $larrresult[0]['Code'];
	}

	public function fnapproveapplicant($IdApplication,$ldtsystemDate,$upduser) {
		$query = "Update tbl_studentapplication SET ProgramOfferred = ProvisionalProgramOffered, Status = '197', Dateofferred='$ldtsystemDate', OfferredBy = '$upduser' where IdApplication = ".$IdApplication;
		$db = Zend_Db_Table :: getDefaultAdapter();
		$db->query($query);
	}
}