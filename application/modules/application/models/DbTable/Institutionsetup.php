<?php
class Application_Model_DbTable_Institutionsetup extends Zend_Db_Table {
	protected $_name = 'tbl_institution'; // table name
	private $lobjDbAdpt;

	public function init(){
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fnaddInstitution($larrformData) {
		if(isset($larrformData['InstitutionPhoneNumber']) && $larrformData['InstitutionPhoneNumber'] != ''){
			$larrformData['InstitutionPhoneNumber'] = $larrformData['Phonecountrycode']."-".$larrformData['Phonestatecode']."-".$larrformData['InstitutionPhoneNumber'];
		}
		unset($larrformData['Phonecountrycode']);
		unset($larrformData['Phonestatecode']);
		if(isset($larrformData['InstitutionFaxNumber']) && $larrformData['InstitutionFaxNumber'] != ''){
			$larrformData['InstitutionFaxNumber'] = $larrformData['faxcountrycode']."-".$larrformData['faxstatecode']."-".$larrformData['InstitutionFaxNumber'];
		}
		unset($larrformData['faxcountrycode']);
		unset($larrformData['faxstatecode']);
		unset($larrformData['IdSubject']);
		$this->lobjDbAdpt->insert('tbl_institution',$larrformData);//insert institute
		$larrreglistdata = $larrformData;
		unset($larrformData['InstitutionName']);
		unset($larrformData['InstitutionShortName']);
		unset($larrformData['InstitutionAddress1']);
		unset($larrformData['InstitutionAddress2']);
		unset($larrformData['InstitutionCity']);


		$lintinstitutionid = $this->lobjDbAdpt->lastInsertId();
		$larrreglistdata['idInstitution'] =  $lintinstitutionid;

		$insertId = $this->lobjDbAdpt->lastInsertId($this->_name,'idInstitution');
		return $insertId;
	}

	public function fnupdateInstitution($formData,$lintIdInstitution) {
		if(isset($formData['InstitutionPhoneNumber']) && $formData['InstitutionPhoneNumber'] != ''){
			$formData['InstitutionPhoneNumber'] = $formData['Phonecountrycode']."-".$formData['Phonestatecode']."-".$formData['InstitutionPhoneNumber'];
		}
		unset($formData['Phonecountrycode']);
		unset($formData['Phonestatecode']);
		if(isset($formData['InstitutionFaxNumber']) && $formData['InstitutionFaxNumber'] != ''){
			$formData['InstitutionFaxNumber'] = $formData['faxcountrycode']."-".$formData['faxstatecode']."-".$formData['InstitutionFaxNumber'];
		}
		unset($formData['faxcountrycode']);
		unset($formData['faxstatecode']);

		$larrreglistdata = $formData;
		unset($formData['InstitutionName']);
		unset($formData['InstitutionShortName']);
		unset($formData['InstitutionAddress1']);
		unset($formData['InstitutionAddress2']);
		unset($formData['InstitutionCity']);
		unset($formData['Country']);
		unset($formData['State']);
		unset($formData['Zip']);
		unset($formData['City']);
		unset($larrreglistdata['IdSubject']);

		$where = 'IdInstitution = '.$lintIdInstitution;
		$this->update($larrreglistdata,$where);
	}

	public function fnDeleteInstitution($idInstitution) {  // function to delete Institution details
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = 'tbl_institution';
		$where = $db->quoteInto('idInstitution = ?', $idInstitution);
		$db->delete('tbl_institution', $where);
	}

	public function fngetInstitutionDetails() { //Function to get the Institution details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from($this->_name);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnSearchInstitution($post = array()) { //Function for searching the Institution details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("is"=>"tbl_institution"),array("is.*"));
                        
                if (isset($post['field3']) && $post['field3']!=''){        
                    $lstrSelect->where('is.InstitutionName like "%" ? "%"',$post['field3']);
                }
                if (isset($post['field4']) && $post['field4']!=''){
                    $lstrSelect->where('is.InstitutionCode like "%" ? "%"',$post['field4']);
                }
                if (isset($post['field29']) && $post['field29']!=''){
                    $lstrSelect->where('is.Country = ?',$post['field29']);
                }
                
		$lstrSelect->order("is.InstitutionName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetInstitutionList(){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_institution"),array("key"=>"a.idInstitution","value"=>"a.InstitutionName"))
		->order("a.InstitutionName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetInstituteCountry($IdInstitute){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_institution"),array("a.Country","a.State","a.City"))
		->where('a.idInstitution =?',$IdInstitute);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fngetSchoolListByCountry($countryId){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_institution"),array("key"=>"a.idInstitution","value"=>"a.InstitutionName"))
		->where('a.Country =?',$countryId);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function get_details($IdInstitute) {
		$select = $this->lobjDbAdpt
			->select()
			->from(array('Institution'=>'tbl_institution'), array('Institution.*'))
            ->joinLeft(array('Type' => 'registry_values'), 'Institution.InstitutionType = Type.id')
			->joinLeft(array('Country' => 'tbl_countries'), 'Institution.Country = Country.idCountry')
			->joinLeft(array('State' => 'tbl_state'), 'Institution.State = State.idState')
			->where('IdInstitution = ?', $IdInstitute)
			;

		$institute = $this->lobjDbAdpt->fetchRow($select);
		return($institute);

	}

        public function checkInstitutName($institutName){
            $select = $this->lobjDbAdpt
                ->select()
                ->from(array('Institution'=>'tbl_institution'), array('Institution.*'))
                ->where('Institution.IdInstitution like "%" ? "%"',$institutName);
            $institute = $this->lobjDbAdpt->fetchRow($select);
            return $institute;
        }

}