<?php
class Application_Model_DbTable_SpecializationfieldMapping extends Zend_Db_Table {
    protected $_name = 'tbl_specialization_field_mapping';
    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function fnadd($data) {
        $this->insert($data);
    }

    public function fngetmappings($IdSubject) {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->join(array('a' => 'tbl_studyfield'),'',array())
            ->joinleft(array('b'=>$this->_name),'a.id = b.IdField',array('a.sf_eng_desc','a.sf_type','a.id'))
            //->join(array('c'=>"tbl_subject"),'c.IdSubject = b.IdSubject',array('c.SubjectName','c.IdSubject','c.SubjectCode'))
            ->where("b.IdFieldType = ?",$IdSubject);
        $result = $this->fetchAll($select);
        return $result->toArray();
    }

    public function fndeletemappings($IdSubject) {
        $where = $this->lobjDbAdpt->quoteInto('IdFieldType = ?', $IdSubject);
        $this->lobjDbAdpt->delete($this->_name, $where);
    }


}