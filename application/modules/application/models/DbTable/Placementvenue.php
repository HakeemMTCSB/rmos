<?php
class Application_Model_DbTable_Placementvenue extends Zend_Db_Table { //Modelclass for the Placement venue module
	protected $_name = 'tbl_placementtestvenue'; // table name

	public function fnGetVenueDetails() {//function to get venue details from database and to search
		$select = $this->select()
		->setIntegrityCheck(false)
		->join(array('a' => 'tbl_placementtestvenue'),array('IdVenue'))
		->where("Active = 1");
		$result = $this->fetchAll($select);
		return $result->toArray();
	}

	/*public function fnGetBankList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
	->from(array("a"=>"tbl_bank"),array("key"=>"a.IdBank","value"=>"a.BankName"))
	->where("a.Active = 1")
	->order("a.BankName");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
	}*/

	public function fnSearchVenue($post = array()) {//searching the values for the Venue
		$db = Zend_Db_Table::getDefaultAdapter();
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
		->setIntegrityCheck(false)
		->join(array('a' => 'tbl_placementtestvenue'),array('IdVenue'))
		->where("a.VenueName LIKE '%".$post['field3']."%'")
		->where("a.VenueCode LIKE '%".$post['field2']."%'")
		->where($field7);
		$result = $this->fetchAll($select);

		return $result->toArray();
	}

	public function fnAddVenue($formData) {	//instance for adding the lobjVenueForm values to DB


		$data = array('VenueName'=>$formData['VenueName'],
				'VenueAddress1'=>$formData['VenueAddress1'],
				'VenueAddress2'=>$formData['VenueAddress2'],
				'City'=>$formData['City'],
				'State'=>$formData['State'],
				'Country'=>$formData['Country'],
				'Phone'=>$formData['Phone'],
				'Contactperson'=>$formData['Contactperson'],
				'VenueCode'=>$formData['VenueCode'],
				'UpdDate'=>$formData['UpdDate'],
				'Active'=>$formData['Active'],
				'UpdUser'=>$formData['UpdUser']);

		$this->insert($data);
	}


	public function fnViewVenu($lintIdBank) {//function to view particular placement test venue and to populate while editing
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
		->from(array('a' => 'tbl_placementtestvenue'),array('a.*'))
		->where('a.IdVenue = '.$lintIdBank);
		$result = $db->fetchRow($select);
		return $result;
	}

	public function fnUpdateVenue($lintIdVenue, $formData) {// function to update a Venue
			
		$where = 'IdVenue = '.$lintIdVenue;
		$data = array('VenueName'=>$formData['VenueName'],
				'VenueAddress1'=>$formData['VenueAddress1'],
				'VenueAddress2'=>$formData['VenueAddress2'],
				'City'=>$formData['City'],
				'State'=>$formData['State'],
				'Country'=>$formData['Country'],
				'Phone'=>$formData['Phone'],
				'Contactperson'=>$formData['Contactperson'],
				'VenueCode'=>$formData['VenueCode'],
				'UpdDate'=>$formData['UpdDate'],
				'Active'=>$formData['Active'],
				'UpdUser'=>$formData['UpdUser']);
		$this->update($data,$where);
	}
}
