<?php
class Application_Model_DbTable_Sendoffer extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_sendoffer';

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table :: getDefaultAdapter();
	}

	public function fnGetStudlist() {
		$lobjDbAdpt = Zend_Db_Table :: getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from(array (
			"sa" => "tbl_studentapplication"
		), array (
			"sa.FName",
			"sa.IdApplication"
		))//->join(array ("so" => "tbl_sendoffer"), 'sa.IdApplication = so.IdApplication AND so.status = 0', array ())
		->join(array (
			"sp" => "tbl_student_preffered"
		), 'sp.IdApplication = sa.IdApplication', array ())->join(array (
			"pg" => "tbl_program"
		), 'sp.IdProgram = pg.IdProgram', array (
			"pg.ProgramName"
		))
		->where('sa.Offered != 1')
		->where('sa.Status = ?',197)
		->where("sp.IdPriorityNo = ?",1)->group("sa.FName");

		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;

	}

	public function fnGetSentStudlist() {

		$lobjDbAdpt = Zend_Db_Table :: getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from(array (
			"sa" => "tbl_studentapplication"
		), array (
			"sa.FName",
			"sa.LName"
		))->join(array (
			"so" => "tbl_sendoffer"
		), 'sa.IdApplication = so.IdApplication AND so.status = 1', array (
			"so.*"
		))->join(array (
			'e' => 'tbl_student_preffered'
		), 'sa.IdApplication = e.IdApplication', array ())->join(array (
			'f' => 'tbl_program'
		), 'f.IdProgram = e.IdProgram', array (
			'f.ProgramName'
		))->join(array (
			'b' => 'tbl_definationms'
		), 'b.idDefinition = sa.status', array (
			"b.DefinitionDesc"
		))->join(array (
			'g' => 'tbl_scheme'
		), 'g.IdScheme = e.IdScheme', array (
			'g.EnglishDescription'
		))->join(array (
			'h' => 'tbl_intake'
		), 'h.IdIntake = e.IdIntake', array (
			'h.IntakeId'
		))
		->where('sa.Status = ?',197)
		->where('e.IdPriorityNo = ?', 1)->where("sa.Active=1")->group("sa.FName");

		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;

	}

	public function fnSearchStudent($post = array ()) {
		$lobjDbAdpt = Zend_Db_Table :: getDefaultAdapter();
		$field7 = "sa.Active = " . $post["field7"];
		$select = $this->select()->setIntegrityCheck(false)->join(array (
			"sa" => "tbl_studentapplication"
		), array (
			"sa.*"
		))//->join(array ("so" => "tbl_sendoffer"), 'sa.IdApplication = so.IdApplication AND so.status = 0', array ())
		->join(array (
			"sp" => "tbl_student_preffered"
		), 'sp.IdApplication = sa.IdApplication', array ())->join(array (
			"pg" => "tbl_program"
		), 'sp.IdProgram = pg.IdProgram', array (
			"pg.ProgramName"
		))
		->where('sa.Status = ?',197)
		->where('sa.Offered != 1')
		->where('sp.IdPriorityNo = ?', 1)->where($field7)->group("sa.FName")->order("sa.FName");
		if(trim($post['field6']) != "") {
		 $select = $select->where('LOWER(sa.FName)  like "%" ? "%"', strtolower($post['field6']));
		}
		if(trim($post['field3']) != "") {
		 $select = $select->where('sa.ExtraIdField1  like "%" ? "%"', $post['field3']);
		}

		$result = $lobjDbAdpt->fetchAll($select);
		return $result;

	}

	public function getScheme() {
		$lobjDbAdpt = Zend_Db_Table :: getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from(array (
			'a' => 'tbl_scheme'
		), array (
			'key' => 'IdScheme',
			'value' => 'a.EnglishDescription'
		))->where('a.Active = ?', 1);
		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}
	public function fnSearchSentofferlist($post = array ()) {

		$select = $this->select()->setIntegrityCheck(false)->from(array (
			"sa" => "tbl_studentapplication"
		), array (
			"sa.FName",
			"sa.LName"
		))->join(array (
			"so" => "tbl_sendoffer"
		), 'sa.IdApplication = so.IdApplication AND so.status = 1', array (
			"so.*"
		))->join(array (
			'e' => 'tbl_student_preffered'
		), 'sa.IdApplication = e.IdApplication', array ())->join(array (
			'f' => 'tbl_program'
		), 'f.IdProgram = e.IdProgram', array (
			'f.ProgramName'
		))->join(array (
			'b' => 'tbl_definationms'
		), 'b.idDefinition = sa.status', array (
			"b.DefinitionDesc"
		))->join(array (
			'g' => 'tbl_scheme'
		), 'g.IdScheme = e.IdScheme', array (
			'g.EnglishDescription'
		))->join(array (
			'h' => 'tbl_intake'
		), 'h.IdIntake = e.IdIntake', array (
			'h.IntakeId'
		))->where("sa.Active=1")->where('LOWER(b.idDefinition) like "%" ? "%"', strtolower($post['field11']));
		if ($post['field8'] != "") {
			$select->where('h.IdIntake = ? ', $post['field8']);
		}
		if ($post['field1'] != "") {
			$select->where('g.IdScheme =  ? ', $post['field1']);
		}
		if ($post['field3'] != "") {
			$select->where('LOWER(sa.FName)  like "%" ? "%"', strtolower($post['field3']));
		}
		if ($post['field2'] != "") {
			$select->where('LOWER(sa.ExtraIdField1)  like "%" ? "%"', strtolower($post['field2']));
		}
		if ($post['field5'] != ""){
			$select->where("sa.ProgramOfferred  = ?", $post['field5']);
		}
		$select->group("sa.FName")->where('e.IdPriorityNo = ?', 1)->order("sa.FName");

		$result = $this->fetchAll($select);
		return $result->toArray();

	}

	public function fnSendOffer() {
		$lobjDbAdpt = Zend_Db_Table :: getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from(array (
			"sa" => "tbl_studentapplication"
		), 'so.IdApplication  = sa.IdApplication')->join(array (
			"so" => "tbl_sendoffer"
		), 'sa.IdApplication = so.IdApplication', array (
			"so.*"
		))->join(array (
			"pg" => "tbl_program"
		), 'so.IdProgram = pg.IdProgram')->lIMIT('5,0')->where("so.status = 0");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetstudent($empid) {

		$lobjDbAdpt = Zend_Db_Table :: getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from(array (
			"sa" => "tbl_studentapplication"
		), array (
			"sa.*"
		))->joinLeft(array (
			"so" => "tbl_sendoffer"
		), 'so.IdApplication  = sa.IdApplication AND so.status = 0', array (
			"so.IdApprove"
		))
		->joinLeft(array (
			"pp" => "tbl_student_preffered"
		), 'sa.IdApplication = pp.IdApplication', array (
		))->joinLeft(array (
			"pg" => "tbl_program"
		), 'pp.IdProgram = pg.IdProgram', array (
			"pg.ProgramName","pg.IdProgram"
		))->joinLeft(array (
			"pc" => "tbl_collegemaster"
		), 'sa.idCollege = pc.IdCollege', array (
			"pc.CollegeName"
		))->joinLeft(array (
			"pu" => "tbl_universitymaster"
		), 'pc.AffiliatedTo = pu.IdUniversity', array (
			"pu.Univ_Name"
		))->joinLeft(array (
			"pd" => "tbl_departmentmaster"
		), 'pc.IdCollege = pd.IdCollege', array (
			"pd.DepartmentName"
		))
		->where("pp.IdPriorityNo = ?",1)
		->where("sa.IdApplication in($empid)");
		//echo $lstrSelect;exit;
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnGetEmailTemplateDescription($TemplateName) {

		$lobjDbAdpt = Zend_Db_Table :: getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from(array (
			"a" => "tbl_emailtemplate"
		))->join(array (
			"b" => "tbl_definationms"
		), "a.idDefinition = b.idDefinition", array (
			""
		))->where("b.DefinitionDesc LIKE '" .
		$TemplateName . "%'");
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}

	public function fnUpdateStudentOffer($applid) {
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$auth = Zend_Auth::getInstance();
		$AssignedBy = $auth->getIdentity()->iduser;
		$data = array (
			'IdApplication' => $applid['IdApplication'],
			'IdProgram' => $applid['IdProgram'],
			'Approved'  => 1,
			'status' => 1,
			'Update' => $ldtsystemDate,
			'UpdUser' => $AssignedBy
		);
		//$where = 'IdApplication  = ' . $applid;
		$this->insert($data);

	}

	public function fnUpdatestudentapp($applid) {
		$db = Zend_Db_Table :: getDefaultAdapter();
		$table = "tbl_studentapplication";
		$larridpo = array (
			'Offered' => 1
		);
		$where = "IdApplication = '$applid'";
		$db->update($table, $larridpo, $where);
	}

}