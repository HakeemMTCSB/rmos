<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_ApplicationInitialConfig extends Zend_Db_Table_Abstract
{
    //put your code here
    protected $_name = 'tbl_application_initialconfig';
    protected $_primary = "id";

    /*
     * fetching all Section Configuration
     * 
     * @on 9/6/2014
     */
    public function fnGetSectionConfList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"), array("value" => "a.*"))
            ->order("a.id");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get Section Configuration based on id
     * 
     * @on 9/6/2014
     */
    public function fnGetSectionConfById($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"), array("value" => "a.*"))
            ->join(array('s' => 'tbl_application_section'), 's.id = a.sectionID')
            ->join(array('ap' => 'tbl_application_configs'), 'a.IdConfig = ap.id')
            ->join(array('c' => 'tbl_scheme'), 'c.IdScheme = ap.IdScheme', array('SchemeName' => 'c.EnglishDescription'))
            ->joinLeft(array('d' => 'tbl_program'), 'd.IdProgram = ap.IdProgram')
            ->joinLeft(array('e' => 'tbl_program_scheme'), 'e.IdProgramScheme = ap.IdProgramScheme')
            ->joinLeft(array('f' => 'registry_values'), 'f.id = ap.category', array('studentCategory' => 'f.name'))
            ->where('a.id = ?', $id)
            ->order("a.id");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    /*
     * get Section Configuration based on id
     * 
     * @on 9/6/2014
     */
    public function fnGetSectionConfJoinWithAppById($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"), array("key" => "a.id", "sectionId" => "a.id", "value" => "a.*"))
            ->join(array('b' => 'tbl_application_section'), 'a.sectionID = b.id')
            ->join(array('ap' => 'tbl_application_configs'), 'a.IdConfig = ap.id')
            ->join(array('c' => 'tbl_scheme'), 'c.IdScheme = ap.IdScheme', array('SchemeName' => 'c.EnglishDescription'))
            ->joinLeft(array('d' => 'tbl_program'), 'd.IdProgram = ap.IdProgram')
            ->joinLeft(array('e' => 'tbl_program_scheme'), 'e.IdProgramScheme = ap.IdProgramScheme')
            ->joinLeft(array('f' => 'registry_values'), 'f.id = ap.category', array('studentCategory' => 'f.name'))
            ->where('a.id = ?', $id);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    /*
     * fetching all program
     * 
     * @on 6/6/2014
     */
    public function fnGetAppInitConfById($programID, $sectionID)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"), array("value" => "a.*"))
            ->where('a.programID = ?', $programID)
            ->where('a.sectionID = ?', $sectionID)
            ->order("a.id");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    /*
     * update section tagging
     */
    public function updateApplicationConfig($postData, $id)
    {
        //var_dump($data); exit;
        //$auth = Zend_Auth::getInstance();

        $data = array(
            'programID' => $postData['programID'],
            'sectionID' => $postData['sectionID']
        );

        $this->update($data, $this->_primary . ' = ' . (int)$id);
    }

    /*
     * get section based on program
     * 
     * @on 9/6/2014
     */
    public function getSectionBasedOnProgram($programID)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"),
                array(
                    "key"   => "a.id",
                    "value" => "a.*"
                ))
            ->join(array('b' => 'tbl_application_section'), 'a.sectionID = b.id')
            ->join(array('c' => 'tbl_program_scheme'), 'a.programID = c.IdProgramScheme')
            ->where('c.IdProgram = ?', $programID)
            ->order("a.programID")
            ->order("a.seq_order");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get section based on program scheme
     * 
     * @on 19/6/2014
     */
    public function getSectionBasedOnProgramScheme($programID, $stdCtgy)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"),
                array(
                    "key"   => "a.id",
                    "value" => "a.*"
                ))
            ->join(array('b' => 'tbl_application_section'), 'a.sectionID = b.id')
            ->join(array('c' => 'tbl_program_scheme'), 'a.programID = c.IdProgramScheme')
            ->where('a.programID = ?', $programID)
            ->where('a.sdtCtgy = ?', $stdCtgy)
            ->order("a.seq_order");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
    * get section based on program
    *
    * @on 9/6/2014
    */
    public function getSectionBasedOnProgramList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"),
                array(
                    "key"   => "a.id",
                    "value" => "a.*"
                ))
            ->join(array('b' => 'tbl_application_section'), 'a.sectionID = b.id')
            ->order("a.seq_order");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get item based on section
     * 
     * @on 9/6/2014
     */
    public function getItemBasedOnSection($sectionID)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item_tagging"),
                array(
                    "key"   => "a.id",
                    "value" => "a.*"
                ))
            ->join(array('b' => 'tbl_application_item'), 'a.idItem = b.id')
            ->joinLeft(array('d' => 'registry_values'), 'd.id = b.formType', array('form_type' => 'code'))
            ->joinLeft(array('e' => 'tbl_application_initialconfig'), 'e.id = a.idTagSection')
            ->where('a.idTagSection = ?', $sectionID)
            ->order("a.seq_order");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get item list based on section id
     * 
     * @on 12/8/2014
     */
    public function fnGetItemBySection($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item"), array("value" => "a.*"))
            ->where('a.item_section_id = ?', $id)
            ->order("a.id");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get username based on user id
     * 
     * on 12/6/2014
     */
    static function getUserName($userId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_user"),
                array(
                    "value" => "a.loginName"
                ))
            ->where('a.iduser = ?', $userId);
        $larrResult = $lobjDbAdpt->fetchOne($lstrSelect);
        return $larrResult;
    }

    /*
     * get program scheme list
     * 
     * @on 19/6/2014 
     */
    public function getProgSchemeList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_program_scheme"), array("value" => "a.*"))
            ->order("a.IdProgramScheme");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get program scheme
     * 
     * @on 19/6/2014
     */
    public function fngetprogramsscheme($programId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_program_scheme"), array("a.*"))
            ->where("a.IdProgram = ?", $programId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get section based on program scheme & student category
     * 
     * @on 19/6/2014
     */
    public function getSectionBasedOnTag($programSchemeID, $studentctgy)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"),
                array(
                    "key"   => "a.id",
                    "value" => "a.*"
                ))
            ->join(array('b' => 'tbl_application_section'), 'a.sectionID = b.id')
            ->join(array('c' => 'tbl_program_scheme'), 'a.programID = c.IdProgramScheme')
            ->where('a.programID = ?', $programSchemeID)
            ->where('a.sdtCtgy = ?', $studentctgy)
            ->order("a.seq_order");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * fetching all Template
     * 
     * @on 27/6/2014
     */
    public function fnGetTemplateList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "comm_template"), array("value" => "a.*"))
            ->order("a.tpl_name");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getItem($IdConfig, $section)
    { // Function for pagination
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('a' => 'tbl_application_item_tagging'), array('*'))
            ->joinLeft(array('b' => 'tbl_application_initialconfig'), 'b.id = a.idTagSection')
            ->joinLeft(array('c' => 'tbl_application_item'), 'c.id = a.idItem')
            ->joinLeft(array('d' => 'registry_values'), 'd.id = c.formType', array('form_type' => 'code'))
            ->where('a.view = 1')
            ->where('a.idTagSection = ?', $section)
            ->where('b.IdConfig = ?', $IdConfig)
            ->order('a.seq_order asc');
        $result = $lobjDbAdpt->fetchAll($select);
        return $result;
    }

    public function fnGetDefination($sectionID)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array("a" => "tbl_application_initialconfig"))
            ->join(array('s' => 'tbl_application_section'), 's.id = a.sectionID', array('*'))
            ->where("a.id = ?", $sectionID)
            ->order("a.seq_order");
        $result = $this->fetchRow($select);
        return $result;
    }

    /*
     * copy section function area
     */
    public function getCopySection($IdScheme, $IdProgram = 0, $IdProgramScheme = 0, $category = 0)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"), array("value" => "a.*", "key" => "a.id"))
            ->join(array('ap' => 'tbl_application_configs'), 'ap.id = a.IdConfig')
            ->join(array('s' => 'tbl_application_section'), 's.id = a.sectionID')
            ->where("ap.IdScheme = ?", $IdScheme)
            ->where("ap.IdProgram = ?", $IdProgram ?? 0)
            ->where("ap.IdProgramScheme = ?", $IdProgramScheme ?? 0)
            ->where("ap.category = ?", $category ?? 0)
            ->order("a.seq_order");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getCopyItem($idTagSection)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item_tagging"), array("value" => "a.*"))
            ->join(array('s' => 'tbl_application_item'), 's.id = a.idItem')
            ->order("a.seq_order")
            ->where("idTagSection = ?", $idTagSection);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getDclCopy($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_documentchecklist_dcl"), array("value" => "a.*"))
            //->join(array('s'=>'tbl_application_item'),'s.id = a.idItem')
            ->order("a.dcl_Id")
            ->where("a.dcl_Id = ?", $id);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function checkDclCopy($progScheme, $stdCtgy, $section, $dclType)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_documentchecklist_dcl"), array("value" => "a.*"))
            //->join(array('s'=>'tbl_application_item'),'s.id = a.idItem')
            ->order("a.dcl_Id")
            ->where("a.dcl_programScheme = ?", $progScheme)
            ->where("a.dcl_stdCtgy = ?", $stdCtgy)
            ->where("a.dcl_sectionid = ?", $section)
            ->where("a.dcl_uplType = ?", $dclType);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function copyDcl($data)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lobjDbAdpt->insert('tbl_documentchecklist_dcl', $data);
        $id = $lobjDbAdpt->lastInsertId('tbl_documentchecklist_dcl', 'dcl_Id');
        return $id;
    }

    /*
     * get section ajax
     * 
     * @on 25/7/2014
     */
    public function getSectionAjax($programID, $stdCtgy, $sectionID)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"),
                array(
                    "key"   => "a.id",
                    "value" => "a.*"
                ))
            ->join(array('b' => 'tbl_application_section'), 'a.sectionID = b.id')
            ->join(array('c' => 'tbl_program_scheme'), 'a.programID = c.IdProgramScheme')
            ->where('a.programID = ?', $programID)
            ->where('a.sdtCtgy = ?', $stdCtgy)
            ->where('a.sectionID = ?', $sectionID)
            ->order("a.seq_order");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get section ajax for addmissnio checklist
     * 
     * @on 5/8/2014
     */
    public function getSectionAjaxChecklist($programID, $stdCtgy)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"),
                array(
                    "key"   => "a.id",
                    "value" => "a.*"
                ))
            ->join(array('b' => 'tbl_application_section'), 'a.sectionID = b.id')
            ->join(array('c' => 'tbl_program_scheme'), 'a.programID = c.IdProgramScheme')
            ->where('a.programID = ?', $programID)
            ->where('a.sdtCtgy = ?', $stdCtgy)
            ->order("a.seq_order");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);

        return $larrResult;
    }

    public function getSectionChecklist($programID, $stdCtgy)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"),
                array(
                    "key"   => "a.id",
                    "value" => "a.*"
                ))
            ->join(array('b' => 'tbl_application_section'), 'a.sectionID = b.id')
            ->join(array('c' => 'tbl_program_scheme'), 'a.programID = c.IdProgramScheme')
            ->where('a.programID = ?', $programID)
            ->where('a.sdtCtgy = ?', $stdCtgy)
            ->order("a.seq_order");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        //echo $lstrSelect;
        return $larrResult;
    }

    public function getSectionIdMax()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_initialconfig"), array("value" => "max(a.id)"))
            ->order("a.id");
        $id = $lobjDbAdpt->fetchOne($lstrSelect);
        return $id;
    }

    public function checkDuplicateItem($sectionId, $itemId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item_tagging"), array("value" => "a.*"))
            ->where("a.idTagSection = ?", $sectionId)
            ->where("a.idItem = ?", $itemId);
        $id = $lobjDbAdpt->fetchRow($lstrSelect);
        return $id;
    }

    //get list available configuration
    public function getListConfigs($id=0)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'tbl_application_configs'))
            ->joinLeft(array('b' => 'tbl_application_initialconfig'), 'b.IdConfig = a.id', array('totalSection' => 'count(b.IdConfig)'))
            ->join(array('c' => 'tbl_scheme'), 'c.IdScheme = a.IdScheme', array('SchemeName' => 'c.EnglishDescription'))
            ->joinLeft(array('d' => 'tbl_program'), 'd.IdProgram = a.IdProgram')
            ->joinLeft(array('e' => 'tbl_program_scheme'), 'e.IdProgramScheme = a.IdProgramScheme')
            ->joinLeft(array('f' => 'registry_values'), 'f.id = a.category', array('studentCategory' => 'f.name'))
            ->group('a.id')
            ->order('a.created_at desc');

        if($id){
            $select->where('a.id = ?', $id);
            $result = $db->fetchRow($select);
        }else{
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    public function getSectionBasedOnId($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a" => "tbl_application_initialconfig"),
                array(
                    "key"   => "a.id",
                    "value" => "a.*"
                ))
            ->join(array('b' => 'tbl_application_section'), 'a.sectionID = b.id')
            ->join(array('ap' => 'tbl_application_configs'), 'a.IdConfig = ap.id')
            ->join(array('c' => 'tbl_scheme'), 'c.IdScheme = ap.IdScheme', array('SchemeName' => 'c.EnglishDescription'))
            ->joinLeft(array('d' => 'tbl_program'), 'd.IdProgram = ap.IdProgram')
            ->joinLeft(array('e' => 'tbl_program_scheme'), 'e.IdProgramScheme = ap.IdProgramScheme')
            ->joinLeft(array('f' => 'registry_values'), 'f.id = ap.category', array('studentCategory' => 'f.name'))
            ->where('a.IdConfig = ?', $id)
            ->order("a.seq_order");

        $result = $db->fetchAll($select);
        //echo $select;
        //exit;
        return $result;

    }

    public function getConfig($IdScheme, $IdProgram = 0 , $IdProgramScheme = 0, $category = 0, $active = 1, $source = 0)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'tbl_application_configs'))
            ->join(array('c' => 'tbl_scheme'), 'c.IdScheme = a.IdScheme', array('SchemeName' => 'c.EnglishDescription'))
            ->joinLeft(array('d' => 'tbl_program'), 'd.IdProgram = a.IdProgram')
            ->joinLeft(array('e' => 'tbl_program_scheme'), 'e.IdProgramScheme = a.IdProgramScheme')
            ->joinLeft(array('f' => 'registry_values'), 'f.id = a.category', array('studentCategory' => 'f.name'))
            ->where('a.IdScheme = ?', $IdScheme)
            ->where('a.IdProgram = ?', $IdProgram)
            ->where('a.IdProgramScheme = ?', $IdProgramScheme)
            ->where('a.category = ?', $category)
            ->where('a.active = ?', $active)
            ->where('a.source = ?', $source)
            ->order('a.created_at desc');

        $result = $db->fetchRow($select);

        return $result;
    }

}

?>