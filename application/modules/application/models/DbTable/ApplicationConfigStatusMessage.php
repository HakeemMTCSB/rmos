<?php
class Application_Model_DbTable_ApplicationConfigStatusMessage extends Zend_Db_Table {
	protected $_name = "tbl_application_status_message";
	
	/**
	 * 
	 * @see Zend_Db_Table_Abstract::init()
	 */
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function fnaddApplicationConfigStatusMessage($data) {
		$this->insert($data);
	}
	
	public function fnupdateApplicationConfigStatusMessage($data,$Id) {
		$where = 'Id = '.$Id;
		$this->update($data,$where);
	}
	
	public function fngetallApplicationConfigStatusMessageByAppConfig($IdAppConfig) {
		$lstrSelect = $this->select()
									 ->setIntegrityCheck(false)
					 				 ->from(array("a"=>"tbl_application_status_message"))
					 				 ->join(array('b' => 'tbl_definationms'),'a.ApplicationStatus =b.idDefinition',array('b.DefinitionDesc as ApplicationStatus','a.ApplicationStatus as IdApplicationStatus'))
					 				 ->where("a.IdAppConfig = $IdAppConfig");
			
			$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	public function applicationstatusmsg($applicantId){
		$lstrSelect = $this->select()
									 ->setIntegrityCheck(false)
					 				 ->from(array("a"=>"tbl_application_status_message"),array('a.Message'))
					 				 ->join(array("b" => "tbl_applicant_personal_detail"),'b.status =a.ApplicationStatus')
					 				 ->where('b.IdApplicant = ?',$applicantId);
					 				 
			
			$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	public function fnDeleteApplicationConfigStatusMessage($IdAppConfig) {
		$table = "tbl_application_status_message";
		$where = $this->lobjDbAdpt->quoteInto('IdAppConfig = ?', $IdAppConfig);
		$this->lobjDbAdpt->delete($table, $where);
	}
	
}