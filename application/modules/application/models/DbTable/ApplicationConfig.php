<?php
class Application_Model_DbTable_ApplicationConfig extends Zend_Db_Table {
	protected $_name = 'tbl_application_config';
	
	/**
	 * 
	 * @see Zend_Db_Table_Abstract::init()
	 */
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function fnaddApplicationConfig($data) {
		unset($data['ApplicationStatus']);
		unset($data['Message']);
		unset($data['Email']);
		unset($data['Description']);
		unset($data['Id']);
		unset($data['Save']);
		$this->insert($data);
		$insertId = $this->lobjDbAdpt->lastInsertId($this->_name,'IdAppConfig');	
	  return $insertId;
	}
	
	public function fnupdateApplicationConfig($data, $IdAppConfig) {
		unset($data['ApplicationStatus']);
		unset($data['Message']);
		unset($data['Email']);
		unset($data['Description']);
		unset($data['Id']);
		unset($data['Save']);
		$where = 'IdAppConfig = '.$IdAppConfig;
		$this->update($data,$where);
	}

        public function fngetApplicationconfig($Iduniversity){
          $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_application_config"))
                    ->where("a.IdUniversity = ?", $Iduniversity);
          $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
          return $larrResult;
        }
}
