<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_StatusTemplate extends Zend_Db_Table_Abstract {
    //put your code here
    protected $_name = 'tbl_statustemplate_stp';
    protected $_primary = "stp_Id";
    
    /*
     * list status tenplate
     * 
     * @on 02/07/2014
     */
    public function fnGetStatusTemplateList(){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_statustemplate_stp"),array("value"=>"a.*"))
            ->join(array('b'=>'tbl_definationms'), 'a.stp_tplType = b.idDefinition')
            ->join(array('c'=>'comm_template'), 'c.tpl_id = a.stp_tplId')
            ->order("a.stp_Id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * list status tenplate
     * 
     * @on 02/07/2014
     */
    public function fnGetStatusTemplateListById($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_statustemplate_stp"),array("value"=>"a.*"))
            ->join(array('b'=>'tbl_definationms'), 'a.stp_tplType = b.idDefinition')
            ->join(array('c'=>'comm_template'), 'c.tpl_id = a.stp_tplId')
            ->where('a.stp_Id = ?', $id)
            ->order("a.stp_Id");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * list status tenplate content by tpl id
     * 
     * @on 02/07/2014
     */
    public function fnGetStatusContentByTplId($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"comm_template_content"),array("value"=>"a.*"))
            ->where('a.tpl_id = ?', $id)
            ->order("a.content_id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * list status template by type
     * 
     * @on 02/07/2014
     */
    public function fnGetStatusTemplateListByType($id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_statustemplate_stp"),array("value"=>"a.*"))
            ->join(array('b'=>'tbl_definationms'), 'a.stp_tplType = b.idDefinition')
            //->join(array('c'=>'comm_template'), 'c.tpl_id = a.stp_tplId')
            ->where('a.stp_tplType = ?', $id)
            ->order("a.stp_Id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get max id
     * 
     * @on 04/07/2014
     */
    public function fnGetMaxStpId(){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_statustemplate_stp"),array("value"=>"a.stp_Id"))
            ->order("a.stp_Id DESC");
	$larrResult = $lobjDbAdpt->fetchOne($lstrSelect);
	return $larrResult;
    }
}
?>