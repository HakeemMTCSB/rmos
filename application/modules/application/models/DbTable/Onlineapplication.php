<?php 
class Application_Model_DbTable_Onlineapplication extends Zend_Db_Table_Abstract{
	 protected $_name = 'tbl_applicant_personal_detail';
     
	
	public function fngetallonlineapplication(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_applicant_personal_detail"),array("a.*"))
		 				 ->join(array('b'=>'tbl_definationms'),'b.idDefinition = a.status',array("b.DefinitionDesc"))
		 				 ->where('a.FName != " "')
		 				 ->order("a.FName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetpersonaldetails($idapplicant){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_applicant_personal_detail"),array("a.*"))
		 				 ->where('a.IdApplicant = ?',$idapplicant);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetapplicantpreferred($idapplicant){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_applicant_preffered"),array("a.*"))
		 				 ->where('a.IdApplicant = ?',$idapplicant);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetpersonalqualification($idapplicant){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_applicant_qualification"),array("a.*"))
		 				 ->where('a.IdApplicant = ?',$idapplicant);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetpersonalsubject($idapplicant){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_applicant_personal_detail"),array())
		 				 ->join(array("b"=>"tbl_applicant_qualification"),'a.IdApplicant = b.IdApplicant',array())
		 				 ->join(array("c"=>"tbl_applicant_subject_detail"),'b.IdApplicationQualification = c.IdApplicationQualification',array('c.*'))
		 				 ->where('a.IdApplicant = ?',$idapplicant);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fninsertstudentpreferred($applicantpreferred,$idapplicant){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 	unset($applicantpreferred[0]['IdApplicationPreferred']);
	 	$applicantpreferred[0]['IdApplication'] = $idapplicant;
		unset($applicantpreferred[0]['IdApplicant']);
		$applicantpreferred[0]['CreatedAt'] = date ('Y-m-d h:i:s');
		$auth = Zend_Auth::getInstance();
		$applicantpreferred[0]['CreatedBy'] = $auth->getIdentity()->iduser;
	 	$lobjDbAdpt->insert('tbl_student_preffered',$applicantpreferred[0]);
	 	
	 	$table2 = 'tbl_studentapplication';
	 	$where = 'IdApplication = '.$idapplicant;
    	$data = array('intake' => $applicantpreferred[0]['IdIntake']);
    	$lobjDbAdpt->update($table2,$data,$where);
	 	
	}
	
	public function fninsertstudentqualification($applicantqualification,$idapplicant){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 	unset($applicantqualification['IdApplicationQualification']);
	 	$applicantqualification['IdApplication'] = $idapplicant;
		unset($applicantqualification['IdApplicant']);
		unset($applicantqualification['IdPlaceObtained']);
		$applicantqualification['YearGraduated'] = $applicantqualification['yearobtained'];
		unset($applicantqualification['yearobtained']);
		$applicantqualification['IdQualification'] = $applicantqualification['IdEducationalLevel'];
		unset($applicantqualification['IdEducationalLevel']);
		$applicantqualification['DegreeType'] = $applicantqualification['certificatename'];
		$applicantqualification['IdSpecialization'] = $applicantqualification['certificatename'];
		unset($applicantqualification['certificatename']);
		unset($applicantqualification['otherinstitute']);
		unset($applicantqualification['instituteaddress1']);
		unset($applicantqualification['instituteaddress2']);
		unset($applicantqualification['country']);
		unset($applicantqualification['state']);
		unset($applicantqualification['city']);
		unset($applicantqualification['zipcode']);
		unset($applicantqualification['phonecountrycode']);
		unset($applicantqualification['phone']);
		unset($applicantqualification['phonestatecode']);
		unset($applicantqualification['fax']);
		unset($applicantqualification['faxcountrycode']);
		unset($applicantqualification['faxstatecode']);
		$lobjDbAdpt->insert('tbl_studenteducationdetails',$applicantqualification);
	 	$lintstudentedudtlid = $lobjDbAdpt->lastInsertId();
		return $lintstudentedudtlid;
	}
	
	public function fninsertstudentdetail($applicantdetails,$idapplicant){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		unset($applicantdetails[0]['IdApplicantPersonalDetail']);
		$applicantdetails[0]['IdApplicant'] = $applicantdetails[0]['ApplicantCode'];
		unset($applicantdetails[0]['ApplicantCode']);
		unset($applicantdetails[0]['defaultlangname']);
		$applicantdetails[0]['HomePhone'] = $applicantdetails[0]['HomeNumber'];
		unset($applicantdetails[0]['HomeNumber']);
		$applicantdetails[0]['Telephone'] = $applicantdetails[0]['OfficeNumber'];
		unset($applicantdetails[0]['OfficeNumber']);
		$applicantdetails[0]['CellPhone'] = $applicantdetails[0]['MobileNumber'];
		unset($applicantdetails[0]['MobileNumber']);
		$applicantdetails[0]['PEmail'] = $applicantdetails[0]['email'];
		unset($applicantdetails[0]['email']);
		unset($applicantdetails[0]['IdProgram']);
		$applicantdetails[0]['DateOfBirth'] = $applicantdetails[0]['DOB'];
		unset($applicantdetails[0]['DOB']);
		$applicantdetails[0]['CorrsAddressDetails'] = $applicantdetails[0]['CorrespondenceAdd'];
		unset($applicantdetails[0]['CorrespondenceAdd']);
		$applicantdetails[0]['CorrsCountry'] = $applicantdetails[0]['CorrespondenceCountry'];
		unset($applicantdetails[0]['CorrespondenceCountry']);
		$applicantdetails[0]['CorrsState'] = $applicantdetails[0]['CorrespondenceProvince'];
		unset($applicantdetails[0]['CorrespondenceProvince']);
		$applicantdetails[0]['CorrsCity'] = $applicantdetails[0]['CorrespondenceCity'];
		unset($applicantdetails[0]['CorrespondenceCity']);
		$applicantdetails[0]['CorrsZip'] = $applicantdetails[0]['CorrespondenceZip'];
		unset($applicantdetails[0]['CorrespondenceZip']);
		$applicantdetails[0]['IdApplicantFormat'] = $applicantdetails[0]['ApplicantCodeFormat'];
		unset($applicantdetails[0]['ApplicantCodeFormat']);
		$applicantdetails[0]['DateCreated'] = date ('Y-m-d h:i:s');
		$applicantdetails[0]['is_migrated'] = "1";
//		$applicantdetails[0]['MaritalStatus'] = $applicantdetails[0]['MaritalStatus'];
//		unset($applicantdetails[0]['MaritalStatus']);
		$applicantdetails[0]['Source'] = 214;
    	$lobjDbAdpt->insert('tbl_studentapplication',$applicantdetails[0]);
    	$applicationId = $lobjDbAdpt->lastInsertId();
    	$table1 = 'tbl_applicant_personal_detail';
    	$data = array('status' => 196);
    	$where = 'IdApplicant = '.$idapplicant;
    	$lobjDbAdpt->update($table1,$data,$where);
    	$table2 = 'tbl_studentapplication';
    	$data = array('Status' => 196);
    	$lobjDbAdpt->update($table2,$data,$where);
    	return $applicationId;
	}
	
	public function fninsertstudentsubject($applicantsubject,$lintstudentedudtlid){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		unset($applicantsubject['IdApplicationSubjectDetail']);
		$applicantsubject['IdStudEduDtl'] = $lintstudentedudtlid;
		unset($applicantsubject['IdApplicationQualification']);
		$lobjDbAdpt->insert('tbl_education_subject',$applicantsubject);
	}
	
   	/**
   	 * 
   	 * Fetches list of all the applicant
   	 */
	public function fngetallstudentlist(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_studentapplication"),array("a.*"))
		 				 ->join(array('b' => 'tbl_student_preffered'),'a.IdApplication=b.IdApplication')
			     		 ->join(array('c'=>'tbl_program'),'b.IdProgram  = c.IdProgram')
			             ->join(array('d'=>'tbl_branchofficevenue'),'b.IdBranch = d.IdBranch')
			             ->join(array('e'=>'tbl_definationms'),'b.IdProgramLevel = e.idDefinition')			            
		 				 ->where('a.FName != " "')
		 				 ->where('b.IdPriorityNo = ?',1)
		 				 ->where('a.Active = ?',1)
		 				 ->order('a.FName');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
//		echo "<pre>";
//		print_r($larrResult);die;
		return $larrResult;
	}
//	 ->join(array('f'=>'tbl_agentapplicanttagging'),'f.IdStudent = a.IdApplicant',array('f.IdAgentApplicantTag'))
//			             ->join(array('g'=>'tbl_agentmaster'),'g.AgentName = f.IdAgent OR g.AgentType = 3',array('g.AgentName1'))
//			             ->join(array('h'=>'tbl_staffmaster'),'h.IdStaff = g.AgentName',array("h.FirstName AS AgentName"))
    /**
     * 
     * 
     * @param  $post
     */
	public function fnSearchapplication($post) { //Function to get the Program Branch details
    	if(($post['field11']!=NULL)||($post['field17']!=NULL)||($post['field5']!=NULL)){
    	$select = $this->select()
			   ->setIntegrityCheck(false)  	
			      ->from(array("a"=>"tbl_applicant_personal_detail"),array("a.*"))
		 		  ->join(array('b' => 'tbl_applicant_preffered'),'a.IdApplicant=b.IdApplicant')
			      ->join(array('c'=>'tbl_program'),'b.IdProgram  = c.IdProgram')
			      ->join(array('d'=>'tbl_branchofficevenue'),'b.IdBranch = d.IdBranch')
			      ->join(array('e'=>'tbl_definationms'),'b.IdProgramLevel = e.idDefinition')
			      ->join(array('f'=>'tbl_definationms'),'f.idDefinition = a.status',array("f.DefinitionDesc"))
			      ->where('a.FName  like "%" ? "%"',$post['field3'])	
			   	  ->where('b.IdProgram like "%" ? "%"',$post['field11'])
			   	  ->where('b.IdBranch like "%" ? "%"',$post['field17'])
			   	  ->where('b.IdProgramLevel like "%" ? "%"',$post['field5'])
			   	  ->where('a.status != ?',192 OR 'a.status != ?',191)
			   	  ->where('b.IdPriorityNo = ?',1)
				  ->order("a.FName");
		
		$result = $this->fetchAll($select);
		return $result->toArray();
	   	}
    	else{
    		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			     ->join(array('a' => 'tbl_applicant_personal_detail'),array('IdApplicant'))
				 ->join(array('b'=>'tbl_definationms'),'b.idDefinition = a.status',array("b.DefinitionDesc"))
			     ->where('a.FName  like "%" ? "%"',$post['field3'])
			     ->where('a.FName != " "');
		$select	->order("a.FName");
		$result = $this->fetchAll($select);
		return $result->toArray();
    	}
     }
     
   	public function fngetStudentName($id){
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_studentapplication"),array("a.FName","a.LName"))
		 				 ->where('a.IdApplicant = ?',$id);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
   		
   	}
     
 	public function fnSearchagentapplicant($post) { //Function to get the Program Branch details
    	
 		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			      ->from(array("a"=>"tbl_studentapplication"),array("a.*"))
		 		  ->join(array('b' =>'tbl_student_preffered'),'a.IdApplication=b.IdApplication')
			      ->join(array('c'=>'tbl_program'),'b.IdProgram  = c.IdProgram')
			      ->join(array('d'=>'tbl_branchofficevenue'),'b.IdBranch = d.IdBranch')
			      ->join(array('e'=>'tbl_definationms'),'b.IdProgramLevel = e.idDefinition')
			     // ->join(array('f'=>'tbl_definationms'),'f.idDefinition = a.status',array("f.DefinitionDesc"))
			      ->joinLeft(array('g'=>'tbl_agentapplicanttagging'),'g.IdStudent = a.IdApplicant',array('g.IdAgent AS AgentName'))
			      ->where('a.FName  like "%" ? "%"',$post['field3'])	
			   	  ->where('b.IdProgram like "%" ? "%"',$post['field11'])
			   	  ->where('b.IdBranch like "%" ? "%"',$post['field1'])
			   	  ->where('b.IdProgramLevel like "%" ? "%"',$post['field5'])
				  ->where('a.FName != " "')
		 		  ->where('b.IdPriorityNo = ?',1)
		 		  ->where('a.Active = ?',1)
				  ->order("a.FName");
		 if(isset($post['field8']) && $post['field8'] != ''){
		 	$select =  $select->where('g.IdAgent like "%" ? "%"',$post['field8']);
		 }
		$result = $this->fetchAll($select);
		return $result->toArray();
	   	}
     
	public function getApplicantDetails($id){
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$idUniversity = 1;
    $result = array();
    $select = $lobjDbAdpt->select()
                  ->from(array('a' => 'tbl_config'))
                  ->where('a.idUniversity = ?',$idUniversity);
    $confdet = $lobjDbAdpt->fetchAll($select);

    $lstrSelect = $lobjDbAdpt->select()
                  ->from(array('b' => 'tbl_applicant_personal_detail'))
                  ->join(array('a' => 'tbl_applicant'),'a.IdApplicant = b.IdApplicant',array())
                  //->join(array('c' => 'tbl_program'),'c.IdProgram  = b.IdProgram',array())
                  ->join(array('d' => 'tbl_countries'),'d.idCountry = b.Nationality',array())
                  ->where('b.IdApplicant = ?',$id);
    $personalDetail = $lobjDbAdpt->fetchAll($lstrSelect);    
    $result[0]['personaldet'] = $personalDetail;
    $result[0]['conf'] = $confdet[0];
    
    $selectpreffered = $lobjDbAdpt->select()
                  ->from(array('a'=>'tbl_applicant_preffered'))
                  ->join(array('b' => 'tbl_intake'),'b.IdIntake = a.IdIntake')
                  ->join(array('c' => 'tbl_definationms'),'a.IdProgramLevel = c.idDefinition')
                  ->join(array('d' => 'tbl_program'),'a.IdProgram = d.IdProgram')
                  ->join(array('e' => 'tbl_branchofficevenue'),'a.IdBranch = e.IdBranch')
                  ->join(array('f' => 'tbl_scheme'),'a.IdScheme = f.IdScheme')
                  ->where('a.IdApplicant = ?',$id)
                  ->order("a.IdPriorityNo");
    $preffered = $lobjDbAdpt->fetchAll($selectpreffered);
    $result[0]['preferreddet'] = $preffered;

    $qualificationmodelObj = new App_Model_OnlineapplicationQualification();

    $result[0]['qualificationdet'] = $qualificationmodelObj->fngetapplicantQualification($id);
    return $result[0];
  }

  public function fngetstudeApplicationDet($IdApplicant){
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("a"=>"tbl_studentapplication"),array("a.IdApplication"))
                                     ->where('a.IdApplicant = ?',$IdApplicant);
    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
    return $larrResult['IdApplication'];
  }

  public function fngetedudetail($IdApplication){
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("a"=>"tbl_studenteducationdetails"),array("a.IdStudEduDtl"))
                                     ->where('a.IdApplication = ?',$IdApplication);
    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
    return $larrResult;
  }

  public function fndeletesubject($IdeduDet){
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_education_subject";
    $wheres = "IdStudEduDtl = $IdeduDet";
    $db->delete($table, $wheres);
  }

  public function fndeletedudet($idapplicant){
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_studenteducationdetails";
    $wheres = "IdApplication = $idapplicant";
    $db->delete($table, $wheres);
  }

  public function fndeletepreferredProgram($idapplicant){
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_student_preffered";
    $wheres = "IdApplication = $idapplicant";
    $db->delete($table, $wheres);
  }

  public function fndeletestudentapplication($idapplicant){
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_studentapplication";
    $wheres = "IdApplicant = $idapplicant";
    $db->delete($table, $wheres);
  }
  
}