<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_ApplicantApproval extends Zend_Db_Table_Abstract {
    //put your code here
    protected $_name = 'applicant_transaction';
    protected $_primary = "at_trans_id";
    
    /*
     * get status info
     * 
     * @on 10/7/14
     */
    public function getStatusInfo($stsId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_status_sts"),array("value"=>"a.*"))
            ->where('a.sts_Id = ?', $stsId)
            ->order("a.sts_Id");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get status template
     * 
     * @on 10/7/14
     */
    public function getStatusTemplate($stpId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_statustemplate_stp"),array("value"=>"a.*"))
            ->where('a.stp_Id = ?', $stpId)
            ->order("a.stp_Id");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get status template attachment
     * 
     * @on 10/7/14
     */
    public function getStatusAttachment($stpId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_statusattachment_sth"),array("value"=>"*"))
            ->joinLeft(array("b"=>"tbl_definationms"), "a.sth_type = b.idDefinition", array('typeName'=>'b.DefinitionDesc'))
            ->where('a.sth_stpId = ?', $stpId)
            ->order("a.sth_Id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get status attachment template
     * 
     * @on 10/7/14
     */
    public function getStatusAttachmenTemplate($tplId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"comm_template"),array("value"=>"*", "key"=>"a.tpl_id"))
            ->join(array('b'=>'comm_template_content'), 'a.tpl_id = b.tpl_id')
            ->where('a.tpl_id = ?', $tplId)
            ->order("a.tpl_id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get template tag
     * 
     * @on 11/7/14
     */
    public function getTemplateTag($tplId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"comm_template_tags"),array("value"=>"a.*"))
            ->where('a.tpl_id = '.$tplId.' or a.tpl_id = 0')
            ->order("a.tpl_id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * update applicant program
     * 
     * @on 18/7/2014
     */
    public function updateAppProgram($data, $id){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrUpdate = $lobjDbAdpt->update('applicant_program', $data, 'ap_at_trans_id = '.$id);
	return $lstrUpdate;
    }
    
    /*
     * store change scheme history
     * 
     * @on 22/7/2014
     */
    public function insertChangeSchemeHistory($data){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrUpdate = $lobjDbAdpt->insert('applicant_changescheme_history', $data);
	return $lstrUpdate;
    }
    
    /*
     * get max ach_id
     */
    static function getMaxAchId(){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_changescheme_history"),array("value"=>"max(a.ach_id)"));
	$larrResult = $lobjDbAdpt->fetchOne($lstrSelect);
	return $larrResult;
    }
    
    /*
     * store change scheme attachment
     * 
     * @on 22/7/2014
     */
    public function insertChangeSchemeAttachment($data){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrUpdate = $lobjDbAdpt->insert('applicant_changescheme_attachment', $data);
	return $lstrUpdate;
    }
    
    /*
     * get status based on program scheme
     * 
     * @on 18/7/2014
     */
    public function getStatus($progScheme, $defCode){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_status_sts"),array("value"=>"a.*"))
            ->join(array('b'=>'tbl_definationms'), 'a.sts_name = b.idDefinition')
            ->where('a.sts_programScheme = ?',$progScheme)
            ->where('b.DefinitionCode = ?',$defCode)
            ->order("a.sts_id");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get applicant info
     * 
     * @on 21/7/2014
     */
    public function getApplicantInfo($txnId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_transaction"),array("value"=>"a.*"))
            ->join(array('b'=>'applicant_profile'), 'a.at_appl_id = b.appl_id')
            ->join(array('c'=>'applicant_program'), 'a.at_trans_id = c.ap_at_trans_id')
            ->where('a.at_trans_id = ?',$txnId)
            ->order("a.at_trans_id");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get applicant doc checklist status by trans id
     * 
     * @on 21/7/2014
     */
    public function fnGetApplicantChecklistStatus($txnId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"applicant_document_status"),array("value"=>"a.*"))
            ->join(array('b'=>'tbl_documentchecklist_dcl'), 'a.ads_dcl_id = b.dcl_Id')
            ->where("a.ads_txn_id = ?", $txnId)
            ->order("a.ads_dcl_id");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
    }
    
    /*
     * get doc checklist info for list checklist file
     * 
     * @on 6/7/2014
     */
    public function getDocChecklistInfo($dclId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_documentchecklist_dcl"),array("value"=>"a.*"))
            ->join(array('b'=>'tbl_application_section'), 'a.dcl_sectionid = b.id')
            ->where("a.dcl_Id = ?", $dclId)
            ->order("a.dcl_Id");
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
    }
    
    /*
     * store pdf document generate history
     * 
     * @on 7/8/2014
     */
    public function storePdfDocGenHistory($data){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrInsert = $lobjDbAdpt->insert('applicant_generated_document', $data);
        return $lstrInsert;
    }
    
    /*
     * add to email que
     * 
     * @on 7/8/2014
     */
    public function addToEmailQue($data){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lobjDbAdpt->insert('email_que', $data);
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"email_que"),array("value"=>"max(a.id)"))
            ->order("a.id");
	$id = $lobjDbAdpt->fetchOne($lstrSelect);
        return $id;
    }
    
    /*
     *  add email que attachment
     * 
     * @on 8/8/2014
     */
    public function addEmailQueAtttachment($data){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrInsert = $lobjDbAdpt->insert('email_que_attachment', $data);
        return $lstrInsert;
    }
    
    public function cancelProforma($bind, $txnId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->update('proforma_invoice_main', $bind, 'trans_id = '.$txnId.' AND invoice_type = "TUTION_FEE"');
        $db->update('applicant_transaction', array('at_tution_fee' => null), 'at_trans_id = '.$txnId);
    }
    
    
}
?>