<?php
class Application_Model_DbTable_QuaSubjectGrade extends Zend_Db_Table {
    protected $_name = 'tbl_subjectgrade';
    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function fnadd($data) {
        $this->insert($data);
    }

    public function fngetmappings($IdSubtype) {
        $select = $this->select()
            ->setIntegrityCheck(false)
            //->join(array('a' => 'tbl_subjectgrade_type'),'',array())
            //->joinleft(array('b'=>$this->_name),'b.sg_type = a.sgt_id',array('a.sgt_desc','a.sgt_id'))
            //->join(array('c'=>"tbl_subject"),'c.IdSubject = b.IdSubject',array('c.SubjectName','c.IdSubject','c.SubjectCode'))
            ->where("sg_type = ?",$IdSubtype)
            ->order("sg_grade");
        $result = $this->fetchAll($select);

        return $result->toArray();
    }

    public function fndeletemappings($IdSubtype) {
        $where = $this->lobjDbAdpt->quoteInto('sg_type = ?', $IdSubtype);
        $this->lobjDbAdpt->delete($this->_name, $where);
    }

    /*public function getData($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('qt'=>$this->_name))
            ->joinleft(array('b'=>'tbl_award_level'),'b.Id = qt.qt_level',array('GradeDesc'=>'b.GradeDesc'))
            ->joinleft(array('c'=>'registry_values'),'c.id = qt.qt_prog_type',array('desctype'=>'c.name'))
            ->where("qt.id = ?", (int)$id);

        $row = $db->fetchRow($selectData);

        return $row;
    }*/

    public function getDatabyType($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('sg'=>$this->_name))
            ->where("sg.sg_type = ?", (int)$id);

        $row = $db->fetchAll($selectData);

        return $row;
    }

    public function addData($postData){
        $auth = Zend_Auth::getInstance();

        $data = array(
            'sg_grade' => $postData['sg_grade'],
            'sg_grade_desc' => $postData['sg_grade_desc'],
            'sg_grade_point' => $postData['sg_grade_point'],
            'sg_createby' => $postData['UpdUser'],
            'sg_createdate' => $postData['UpdDate'],
            'sg_mohe_code' => $postData['sg_mohe_code'],
            'Active' => $postData['Active']
        );

        return $this->insert($data);
    }

    public function updateData($postData,$id){

        $data = array(
            'sg_grade' => $postData['sg_grade'],
            'sg_grade_desc' => $postData['sg_grade_desc'],
            'sg_grade_point' => $postData['sg_grade_point'],
            'Active' => $postData['Active']
        );

        $this->update($data, "id = ".(int)$id);
    }




}