<?php
class Application_Model_DbTable_Placementteststat extends Zend_Db_Table {
	protected $_name = 'tbl_studentapplication';


	/**
	 *
	 * function to obtain placement test statistics
	 */
	public function fngetplacementteststat(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("a"=>"tbl_studentapplication"),array("a.IdApplication","a.FName","a.LName","a.intake",'a.IdApplicant'))
		->join(array("b"=>"tbl_placementtestmarks"),'a.IdApplication = b.IdApplication')
		->join(array('e'=>'tbl_student_preffered'),'a.IdApplication = e.IdApplication',array())
		->join(array("c"=>"tbl_intake"),'e.IdIntake = c.IdIntake',array('c.IntakeDesc'))
		->join(array('d'=>'tbl_placementtestcomponent'),'b.IdPlacementTestComponent = d.IdPlacementTestComponent',array('d.ComponentName'))
		->join(array('f'=>'tbl_program'),'f.IdProgram = e.IdProgram',array('f.ProgramName'))
		->join(array('g'=>'tbl_placementtest'),'g.IdPlacementTest = b.IdPlacementTest',array('g.TotalMark'))
		->order("a.IdApplication")
		->distinct("a.IdApplication")
		//->group("a.IdApplication")
		->where('e.IdPriorityNo = ?',1);
		$larrResult = $input = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	/**
	 *
	 * function to search the list of placement test statistics
	 * @param  $post
	 */
	public function fnSearchplacementteststat($post){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()
		->from(array("a"=>"tbl_studentapplication"),array("a.IdApplication","a.FName","a.LName","a.intake",'a.IdApplicant'))
		->join(array("b"=>"tbl_placementtestmarks"),'a.IdApplication = b.IdApplication')
		->join(array('e'=>'tbl_student_preffered'),'a.IdApplication = e.IdApplication',array())
		->join(array("c"=>"tbl_intake"),'e.IdIntake = c.IdIntake',array('c.IntakeDesc'))
		->join(array('d'=>'tbl_placementtestcomponent'),'b.IdPlacementTestComponent = d.IdPlacementTestComponent',array('d.ComponentName'))
		->join(array('f'=>'tbl_program'),'f.IdProgram = e.IdProgram',array('f.ProgramName'))
		->join(array('g'=>'tbl_placementtest'),'g.IdPlacementTest = b.IdPlacementTest',array('g.TotalMark','g.PassingMark'))
		->order("a.IdApplication")
		->distinct("a.IdApplication")
		->where('e.IdPriorityNo = ?',1);
		$result = $lobjDbAdpt->fetchAll($select);
		if(isset($post['field3']) && $post['field3'] != ''){
			$select = $select->where('b.Marks =?',$post['field3']);
		}
		if(isset($post['field2']) && $post['field2'] != ''){
			$select = $select->where('g.TotalMark =?',$post['field2']);
		}
		if($post['field11'] != NULL){
			if($post['field11'] == "Pass"){
				foreach($result as $key => $val){
					if(intval($val['Marks']) < $val['PassingMark']){
						unset($result[$key]);
					}
				}
			}
			else if($post['field11'] == "Fail"){
				foreach($result as $key => $val){
					if(intval($val['Marks']) >= $val['PassingMark']){
						unset($result[$key]);
					}
				}
			}
		}
		return $result;
	}

}