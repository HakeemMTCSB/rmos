<?php
class Application_Model_DbTable_Programchecklist extends Zend_Db_Table {
	
	protected $_name = 'tbl_programchecklist';

	//Function to Get Maintenance Details
	public function fnGetProgramDetails() {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("pc"=>"tbl_program"),array("pc.*"))
       								->where("pc.Active = 1")
       								->order("pc.ProgramName");
      					
		    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	public function fnSearchProgram($post = array()) { //Function for searching the university details
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_program'),array('IdProgram'))
			   ->where('a.ProgramName  like "%" ? "%"',$post['field2'])
			   ->where('a.ShortName  like "%" ? "%"',$post['field3'])
			   ->where('a.ArabicName   like "%" ? "%"',$post['field4'])
			   ->where($field7)
			   ->group("a.ProgramName")
		   	   ->order("a.ProgramName");
		$result = $this->fetchAll($select);
		return $result->toArray();
	}

   public static function checkpriority($priority,$formdata){
		  
   		  $programId = $formdata['IdProgram'];
          $table = "tbl_programchecklist";
          if(isset($formdata['IdCheckList'])){
          	$checklistId = $formdata['IdCheckList'];
          $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
          $lstrSelect = $lobjDbAdpt->select()
                  ->from(array("a"=>"tbl_programchecklist"),array('a.Priority'))
                  ->where('a.IdCheckList != ?',$checklistId)
                  ->where('a.IdProgram  = ?',$programId);
          $priorityDetail = $lobjDbAdpt->fetchAll($lstrSelect);
          $count = count($priorityDetail);  
   			for($i=0;$i<$count;$i++){
	   			$priority1 = $priorityDetail[$i]['Priority'];
	          if ($priority1 == $priority){
	            return false;
	          }
	          else{
	            $flag = 0;
	          }
   			}
   			return true;
		}
		else{
          $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
          $lstrSelect = $lobjDbAdpt->select()
                  ->from(array("a"=>"tbl_programchecklist"),array('a.Priority'))
                  ->where('a.IdProgram  = ?',$programId);
          $priorityDetail = $lobjDbAdpt->fetchAll($lstrSelect);
          $count = count($priorityDetail);  
   			for($i=0;$i<$count;$i++){
	   			$priority1 = $priorityDetail[$i]['Priority'];
	          if ($priority1 == $priority){
	            return false;
	          }
	          else{
	            $flag = 0;
	          }
   			}
   			return true;
		}
   }

    //Function To View Maintenace Type Ms
	public function fnViewProgramchecklist($lintIdProgram) {
		$lstrSelect = $this	->select()
						->setIntegrityCheck(false)  
						->join(array('a' => 'tbl_programchecklist'),array('a.*'))
						->join(array('b'=>'tbl_program'),'a.IdProgram = b.IdProgram')
						->joinLeft(array('c'=>'tbl_varifiedprogramchecklist'),'a.IdCheckList = c.IdCheckList',array('c.IdVarifiedProgramChecklist'))
        	            ->joinLeft(array('d'=>'tbl_studentapplication'),'c.IdApplication = d.IdApplication AND d.registrationId IS NULL',array())
						->where('a.IdProgram  = ?',$lintIdProgram)
						//->where("a.Active = 1")
						->order('a.Priority') 
						->group('a.IdCheckList');                  
		$larrResult = $this->fetchAll($lstrSelect);
		return $larrResult->toArray();
	}
	
    public function fnaddChecklistname($larrformData) { //Function for adding the Checklist details to the table    	   	
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$this->insert($larrformData);
	}
	
	
	public function fnviewChecklistDtls($lintIdChecklistType) { //Function for the view user 
    	
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("pc"=>"tbl_programchecklist"),array("pc.*"))
						->where("pc.IdCheckList = ?",$lintIdChecklistType);	
		return $result = $lobjDbAdpt->fetchRow($select);
    }
	
 	public function fnupdateChecklistDtls($lintIdChecklistType,$larrformData) { //Function for updating the checklist
 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("pc"=>"tbl_programchecklist"),array("pc.Priority","pc.IdCheckList"));
		$result = $lobjDbAdpt->fetchAll($select);
 		$where = 'IdCheckList = '.$lintIdChecklistType; 
		$this->update($larrformData,$where);
	}

        public function deleteprogramchecklist($id){          
          $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_programchecklist";
	  $where = $db->quoteInto("IdCheckList = $id");
          $db->delete($table, $where);
        }
        
        public function fncheckregistered($checkListId){
        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        	$select = $lobjDbAdpt->select()
        	                     ->from(array('a'=>'tbl_varifiedprogramchecklist'),array('a.IdVarifiedProgramChecklist'))
        	                     ->join(array('b'=>'tbl_studentapplication'),'a.IdApplication = b.IdApplication',array())
        	                     ->where('b.registrationId IS NULL')
        	                     ->where('a.IdCheckList = ?',$checkListId);
        	 $result = $lobjDbAdpt->fetchAll($select);
        	 return $result;           
        }
	
	
	


}
