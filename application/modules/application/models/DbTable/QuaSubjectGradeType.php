<?php
class Application_Model_DbTable_QuaSubjectGradeType extends Zend_Db_Table {
    protected $_name = 'tbl_subjectgrade_type';
    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function fnadd($data) {
        $this->insert($data);
    }

    public function fngetmappings($IdSubject) {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->join(array('a' => 'tbl_qualification_type'),'',array())
            ->joinleft(array('b'=>$this->_name),'a.id = b.sgt_qua_type',array('b.sgt_desc','b.sgt_id','b.id'))
            //->join(array('c'=>"tbl_subject"),'c.IdSubject = b.IdSubject',array('c.SubjectName','c.IdSubject','c.SubjectCode'))
            ->where("b.sgt_qua_type = ?",$IdSubject);
        $result = $this->fetchAll($select);

        return $result->toArray();
    }

    public function fndeletemappings($IdSubject) {
        $where = $this->lobjDbAdpt->quoteInto('sgt_qua_type = ?', $IdSubject);
        $this->lobjDbAdpt->delete($this->_name, $where);
    }

    public function getData($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('qt'=>$this->_name))
            ->joinleft(array('b'=>'tbl_award_level'),'b.Id = qt.qt_level',array('GradeDesc'=>'b.GradeDesc'))
            ->joinleft(array('c'=>'registry_values'),'c.id = qt.qt_prog_type',array('desctype'=>'c.name'))
            ->where("qt.id = ?", (int)$id);

        $row = $db->fetchRow($selectData);

        return $row;
    }

    public function getDatabyType($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('qt'=>$this->_name))
            ->where("qt.sgt_qua_type = ?", (int)$id);

        $row = $db->fetchAll($selectData);

        return $row;
    }




}