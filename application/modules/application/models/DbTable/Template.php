<?php 
class Application_Model_DbTable_Template extends Zend_Db_Table_Abstract
{
  
    protected $_name = 'tbl_statustemplate_stp';
	protected $_primary = "stp_Id";
	protected $_subname = 'tbl_status_sts';
	protected $_subprimary = "sts_Id";
	
	
			
	public function getEmailTemplate($status,$scheme,$appl_cat){ //status =>DRAFT
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('stp'=>$this->_name))
	                ->join(array('sts'=>$this->_subname),'sts.`sts_emailNotification`=stp.`stp_Id`')
	                ->where('stp.stp_tplType = 604')
	                ->where('sts.sts_name = ?',$status)
	                ->where('sts.sts_programScheme = ?',$scheme)
	                ->where('sts.sts_stdCtgy  = ?',$appl_cat);
	   
	    $row = $db->fetchRow($select);	
		return $row;           
	}
	
	public function getMessageTemplate($status,$scheme,$appl_cat){ //status =>DRAFT
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('stp'=>$this->_name))
	                ->join(array('sts'=>$this->_subname),'sts.`sts_message`=stp.`stp_Id`')
	                ->where('stp.stp_tplType = 603')
	                ->where('sts.sts_name = ?',$status)
	                ->where('sts.sts_programScheme = ?',$scheme)
	                ->where('sts.sts_stdCtgy  = ?',$appl_cat);
	  
	    $row = $db->fetchRow($select);	
		return $row;           
	}
	
	public function checkExistMessageAcceptance($scheme,$appl_cat){ 
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()	            
	                ->from(array('sts'=>$this->_subname))	            
	                ->where('(sts.sts_name = 599 OR sts.sts_name = 600)') //accept or decline
	                ->where('sts.sts_programScheme = ?',$scheme)
	                ->where('sts.sts_stdCtgy  = ?',$appl_cat);
	   
	    $row = $db->fetchRow($select);	
	    
	    if($row)
			return $row;  
		else
			return null;    
	}
	
	
	public function addData($data){
		
		$this->insert($data);
	}
	
	public function updateData($data,$id){
		
		$this->update($data, $this->_primary . ' = ' . (int)$id);
	}
	
	public function deleteData($id){
		$this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getEmailTemplatebyProgram($status,$program){ //status =>DRAFT
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('stp'=>$this->_name))
	                ->join(array('sts'=>$this->_subname),'sts.`sts_emailNotification`=stp.`stp_Id`')
	                ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme=sts.sts_programScheme',array())
	                ->where('stp.stp_tplType = 604')
	                ->where('sts.sts_name = ?',$status)
	                ->where('ps.IdProgram  = ?',$program);
	   
	    $row = $db->fetchRow($select);	
		return $row;           
	}
	
	public function getEmailAttachment($stp_id,$locale){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('sth'=>'tbl_statusattachment_sth'))
	                ->join(array('ct'=>'comm_template'),'ct.tpl_id = sth.sth_tplId')
	                ->join(array('ctc'=>'comm_template_content'),'ctc.tpl_id=ct.tpl_id')	              
	                ->where('sth.sth_stpId  = ?',$stp_id)
	                ->where('ctc.locale = ?',$locale);
	   
	    $row = $db->fetchAll($select);	
		return $row;      
	}
		
 	public function getProformaContent($sts, $proSchemeId, $studCat, $locale='en_US'){
        $db = Zend_Db_Table::getDefaultAdapter();
    	    $select = $db->select()
            ->from(array('a'=>'tbl_status_sts'), array('value'=>'*'))
            ->join(array('b'=>'tbl_statustemplate_stp'), 'a.sts_emailNotification = b.stp_Id')
            ->join(array('c'=>'tbl_statusattachment_sth'), 'b.stp_Id = c.sth_stpId')
            ->join(array('d'=>'comm_template'), 'c.sth_tplId = d.tpl_id')
            ->join(array('e'=>'comm_template_content'), 'd.tpl_id = e.tpl_id')
            ->joinLeft(array('f'=>'tbl_definationms'), 'c.sth_type = f.idDefinition', array('attachmentTypeName'=>'DefinitionDesc'))
            ->where('a.sts_name = ?', $sts)
            ->where('a.sts_programScheme = ?', $proSchemeId)
            ->where('a.sts_stdCtgy = ?', $studCat)
            ->where('b.stp_tplType = ?', 604)
            ->where('e.locale = ?', $locale)
            ->where('c.sth_type = 605'); // 605 Proforma
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    
	public function getAppendixContent($sts, $proSchemeId, $studCat, $locale='en_US'){
        $db = Zend_Db_Table::getDefaultAdapter();
    	    $select = $db->select()
            ->from(array('a'=>'tbl_status_sts'), array('value'=>'*'))
            ->join(array('b'=>'tbl_statustemplate_stp'), 'a.sts_emailNotification = b.stp_Id')
            ->join(array('c'=>'tbl_statusattachment_sth'), 'b.stp_Id = c.sth_stpId')
            ->join(array('d'=>'comm_template'), 'c.sth_tplId = d.tpl_id')
            ->join(array('e'=>'comm_template_content'), 'd.tpl_id = e.tpl_id')
            ->joinLeft(array('f'=>'tbl_definationms'), 'c.sth_type = f.idDefinition', array('attachmentTypeName'=>'DefinitionDesc'))
            ->where('a.sts_name = ?', $sts)
            ->where('a.sts_programScheme = ?', $proSchemeId)
            ->where('a.sts_stdCtgy = ?', $studCat)
            ->where('b.stp_tplType = ?', 604)
            ->where('e.locale = ?', $locale)
            ->where('c.sth_type = 851'); // Appendeix
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getTemplateContent($sts, $proSchemeId, $studCat, $type, $locale='en_US'){
        $db = Zend_Db_Table::getDefaultAdapter();
    	$select = $db->select()
            ->from(array('a'=>'tbl_status_sts'), array('value'=>'*'))
            ->join(array('b'=>'tbl_statustemplate_stp'), 'a.sts_emailNotification = b.stp_Id')
            ->join(array('c'=>'tbl_statusattachment_sth'), 'b.stp_Id = c.sth_stpId')
            ->join(array('d'=>'comm_template'), 'c.sth_tplId = d.tpl_id')
            ->join(array('e'=>'comm_template_content'), 'd.tpl_id = e.tpl_id')
            ->joinLeft(array('f'=>'tbl_definationms'), 'c.sth_type = f.idDefinition', array('attachmentTypeName'=>'DefinitionDesc'))
            ->where('a.sts_name = ?', $sts)
            ->where('a.sts_programScheme = ?', $proSchemeId)
            ->where('a.sts_stdCtgy = ?', $studCat)
            ->where('b.stp_tplType = ?', 604)
            ->where('e.locale = ?', $locale)
            ->where('c.sth_type = ?', $type); // 605 Proforma
        
        $result = $db->fetchRow($select);
        return $result;
    }
	
}
?>