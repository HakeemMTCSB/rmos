<?php 
class Application_Model_DbTable_Studentrejection extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_studentapplication';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

     
     public function fnEducationRejectionlist() { //Function to get the Program Branch details
 		$select = $this->select()
                ->setIntegrityCheck(false)  
                ->join(array('a' => 'tbl_studentapplication'),array('IdApplication'))
                ->join(array('b'=>'tbl_program'),'a.IDCourse  = b.IdProgram')
                ->join(array('c'=>'tbl_definationms'),'b.Award = c.idDefinition')
                ->join(array('d'=>'tbl_collegemaster'),'a.idCollege = d.IdCollege')
                ->where('a.Registered = 0')
                 ->where('a.Rejected = 0')
                ->order("a.FName");
       $result = $this->fetchAll($select);
       return $result->toArray();
     }
     
public function fnUpdateStudentrejection($larrformData) {
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_studentapplication";
			$gridcount = count($larrformData['empid']);
			for($i=0;$i<$gridcount;$i++){
			$larridpo = array('Rejected'=>1,
							  'RejectedComments' =>$larrformData['comments'][$i]);
			
			$where = "IdApplication = ".$larrformData['empid'][$i];
			$db->update($table,$larridpo,$where);
			}
			}	
		
}
?>