<?php
class Application_Model_DbTable_BlockApplicant extends Zend_Db_Table {
	
	protected $_name = 'block_applicant';
    protected $_primary = "Id";

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

    public function addData($data){
        $this->insert($data);
        $db = Zend_Db_Table::getDefaultAdapter();
        $id = $db->lastInsertId();
        return $id;
    }

    public function getBlockList($search = array(), $type=null){
        $db = Zend_Db_Table::getDefaultAdapter();

        $selectData = $db->select()
            ->from(array('a'=>$this->_name),array("a.*"));

        if (isset($search['IcNo']) && !empty($search['IcNo'])) {
            $selectData = $selectData->where("a.IcNo  like '%' ? '%'", $search['IcNo']);
        }

        if (isset($search['Name']) && !empty($search['Name'])) {
            $selectData = $selectData->where("a.Name  like '%' ? '%'", $search['Name']);
        }

        if ($type == 'sql') {
            return $selectData;
        } elseif ($type == 'count') {

        } else {
            return $result = $db->fetchAll($selectData);
        }

    }


    public function getLevelByProgramType($idProgramtype) {

        $db = Zend_Db_Table::getDefaultAdapter();

        $selectData = $db->select()
            ->from(array('aw'=>$this->_name),array("aw.*","key"=>"aw.Id","value"=>"aw.GradeDesc"))
            ->where("aw.GradeType = ?", $idProgramtype);

        return $result = $db->fetchAll($selectData);

    }

	
}

?>