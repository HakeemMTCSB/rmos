<?php
class Application_Model_DbTable_ChecklistHistory extends Zend_Db_Table_Abstract {
    
    public function getOldChecklist($studentCat, $programScheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_documentchecklist_dcl'), array('value'=>'*'))
            ->where('a.dcl_stdCtgy = ?', $studentCat)
            ->where('a.dcl_programScheme = ?', $programScheme);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getOldChecklistStatus($txnId, $dclId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_document_status'), array('value'=>'*'))
            ->where('a.ads_txn_id = ?', $txnId)
            ->where('a.ads_dcl_id = ?', $dclId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getOldChecklistDocument($txnId, $dclId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_documents'), array('value'=>'*'))
            ->where('a.ad_txn_id = ?', $txnId)
            ->where('a.ad_dcl_id = ?', $dclId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function insertChecklistHistory($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('applicant_document_history', $bind);
        $id = $db->lastInsertId('applicant_document_history', 'adh_id');
        return $id;
    }
    
    public function insertChecklistStatusHistory($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('applicant_document_statushistory', $bind);
        $id = $db->lastInsertId('applicant_document_statushistory', 'adt_id');
        return $id;
    }
    
    public function insertCheclistDocumentHistory($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('applicant_document_uploadhistory', $bind);
        $id = $db->lastInsertId('applicant_document_uploadhistory', 'adu_id');
        return $id;
    }
    
    public function insertChecklistDclHistory($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('applicant_document_checklisthistory', $bind);
        $id = $db->lastInsertId('applicant_document_checklisthistory', 'adc_id');
        return $id;
    }
    
    public function getApplicantProfileById($applId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_profile'), array('value'=>'*'))
            ->where('a.appl_id = ?', $applId);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getHistoryChecklist($txnId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_document_history'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_program_scheme'), 'a.adh_oldprogramscheme = b.IdProgramScheme')
            ->joinLeft(array('c'=>'tbl_program_scheme'), 'a.adh_newprogramscheme = c.IdProgramScheme')
            ->joinLeft(array('d'=>'tbl_definationms'), 'b.mode_of_program = d.idDefinition', array('oldMop'=>'d.DefinitionDesc'))
            ->joinLeft(array('e'=>'tbl_definationms'), 'b.mode_of_study = e.idDefinition', array('oldMos'=>'e.DefinitionDesc'))
            ->joinLeft(array('f'=>'tbl_definationms'), 'b.program_type = f.idDefinition', array('oldPt'=>'f.DefinitionDesc'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'c.mode_of_program = g.idDefinition', array('newMop'=>'g.DefinitionDesc'))
            ->joinLeft(array('h'=>'tbl_definationms'), 'c.mode_of_study = h.idDefinition', array('newMos'=>'h.DefinitionDesc'))
            ->joinLeft(array('i'=>'tbl_definationms'), 'c.program_type = i.idDefinition', array('newPt'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'a.adh_oldstudentcat = j.idDefinition', array('oldStdCtgy'=>'j.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_definationms'), 'a.adh_newstudentcat = k.idDefinition', array('newStdCtgy'=>'k.DefinitionDesc'))
            ->joinLeft(array('l'=>'tbl_user'), 'a.adh_updby = l.iduser', array('loginName'=>'l.loginName'))
            ->where('a.adh_txn_id = ?', $txnId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getViewChecklistHistory($adhId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_document_checklisthistory'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_application_section'), 'a.adc_sectionid = b.id')
            ->joinLeft(array('c'=>'applicant_document_statushistory'), 'a.adc_id = c.adt_adc_id')
            ->where('a.adc_adh_id = ?', $adhId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getChecklistStatus($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_document_statushistory'), array('value'=>'*'))
            ->where('a.adt_adc_id = ?', $id);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getUploadHistory($adhId, $adcId, $txnId, $dclId, $sectionId, $tableId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_document_uploadhistory'), array('value'=>'*'))
            ->where('a.adu_adh_id = ?', $adhId)
            ->where('a.adu_adc_id = ?', $adcId)
            ->where('a.adu_txn_id = ?', $txnId)
            ->where('a.adu_dcl_id = ?', $dclId)
            ->where('a.adu_section_id = ?', $sectionId)
            ->where('a.adu_table_id = ?', $tableId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function deleteOldchecklistStatus($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('applicant_document_status', 'ads_txn_id = '.$id);
        return $delete;
    }
    
    public function deleteOldchecklistDocument($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('applicant_documents', 'ad_txn_id = '.$id);
        return $delete;
    }
}
?>