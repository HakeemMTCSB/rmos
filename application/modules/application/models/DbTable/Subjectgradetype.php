<?php

class Application_Model_DbTable_Subjectgradetype extends Zend_Db_Table
{
    protected $_name = 'tbl_subjectgradetype';

    /**
     *
     * @see Zend_Db_Table_Abstract::init()
     */
    public function init()
    {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function fnaddsubjectgradeType($data)
    {
        $this->insert($data);
        $insertId = $this->lobjDbAdpt->lastInsertId($this->_name, 'Idsubjectgradetype');
        return $insertId;
    }

    public function fnaddsubjectgradePoint($data)
    {
        $activityMappingtable = new Zend_Db_Table('tbl_subjectgradepoint');
        $activityMappingtable->insert($data);
    }

    public function fngetgradeTypeList($univId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_subjectgradetype'))->where('a.Iduniversity = ?', $univId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }


    public function fnSearchSubjectgrade($post = array())
    { //Function for searching the Activity details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("is" => "tbl_subjectgradetype"), array("is.*"))
            ->where('is.subjectcodetype like "%" ? "%"', $post['field3'])
            ->where('is.subjectcodedescription like "%" ? "%"', $post['field2'])
            ->order("is.subjectcodetype");
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fngetsubjectgradepoints($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_subjectgradepoint"), array("a.*"))
            ->where('a.Idsubjectgradetype = ?', $id);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fngetsubjectgradeType($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_subjectgradetype"), array("a.*"))
            ->where('a.Idsubjectgradetype = ?', $id);
        $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function fnupdatesubjectgradeType($subjectcodeTypeArray, $id)
    {
        $where = 'Idsubjectgradetype = ' . $id;
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->update('tbl_subjectgradetype', $subjectcodeTypeArray, $where);
    }


    public function fndeletegradepoint($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = $db->quoteInto('Idsubjectgradetype = ?', $id);
        $db->delete('tbl_subjectgradepoint', $where);
    }


    public function checkexistenceofgradepoint($subjectgrade, $subjectgradepoint, $id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_subjectgradepoint"), array("a.*"))
            ->where('a.subjectgrade = ?', $subjectgrade)
            ->where('a.subjectgradepoint = ?', $subjectgradepoint)
            ->where('a.Idsubjectgradetype = ?', $id);

        $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
        if (!empty($larrResult)) {
            return true;
        }
        return false;
    }

    public function fngetsubjectgradeList()
    {
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a" => "$this->_name"), array("key" => "a.Idsubjectgradetype", "value" => "a.subjectcodetype"))
            ->order("a.subjectcodetype");
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getGradePoint($Idqualification)
    {
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a" => 'tbl_subjectgradetype'))
            ->joinLeft(array("b" => 'tbl_subjectgradepoint'), 'a.Idsubjectgradetype = b.Idsubjectgradetype', array("key" => "subjectgradepoint", "value" => "subjectgradepoint"))
            ->where('a.Idqualification = ?', $Idqualification);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;

    }

    public function fngetGradePointForDropdown($Idqualification)
    {
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a" => 'tbl_subjectgradetype'), array())
            ->joinLeft(array("b" => 'tbl_subjectgradepoint'), 'a.Idsubjectgradetype = b.Idsubjectgradetype', array("key" => "b.Idsubjectgradepoint", "name" => "b.subjectgrade"))
            ->where('a.Idqualification = ?', $Idqualification);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;

    }

    public function fngetSubjectList($Idqualification)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => 'tbl_subject'), array("key" => "a.IdSubject", "name" => "CONCAT_WS(' - ',IFNULL(a.SubjectName,''),IFNULL(a.SubjectCode,''))"))
            ->joinLeft(array("b" => 'tbl_qualification_subject_mapping'), 'a.IdSubject = b.IdSubject', array())
            ->where('b.IdQualification =?', $Idqualification)
            ->order("a.SubjectName");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /**
     * Function toCHECK that no qualification is added twice for any grade Code
     * @author Vipul
     */

    public function fncheckQualification($condition_grade = NULL, $gby = NULL)
    {
        $lstrSelect = $this->lobjDbAdpt->select()->from(array("a" => 'tbl_subjectgradetype'));
        if ($gby) {
            $lstrSelect->group('a.Idqualification');
        } else {
            $lstrSelect->where($condition_grade);
        }
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnmatchcountry($countryId)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => 'tbl_universitymaster'), array('a.Univ_Name'))
            ->where('a.Active =?', "1")
            ->where('a.Country =?', $countryId);
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }


    public function getAllgradePoint()
    {
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("b" => 'tbl_subjectgradepoint'), array("key" => "b.Idsubjectgradepoint", "value" => "b.subjectgrade"));

        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getGradeByQualification($idQualification = 0)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a" => "tbl_subjectgrade"))
            ->join(array("b" => 'tbl_subjectgrade_type'), 'b.id = a.sg_type', array('sgt_desc'))
            ->join(array("c" => 'tbl_qualification_type'), 'c.id = b.sgt_qua_type', array('qt_eng_desc', 'qt_mal_desc', 'qt_id'))
            ->group('a.id')
            ->order('a.sg_grade');

        if ($idQualification) {
            $select->where('b.sgt_qua_type = ?', $idQualification);
        }

        $result = $db->fetchAll($select);
        return $result;
    }

}
