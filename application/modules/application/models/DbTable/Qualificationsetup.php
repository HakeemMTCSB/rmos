<?php
class Application_Model_DbTable_Qualificationsetup extends Zend_Db_Table {
	//protected $_name = 'tbl_qualificationmaster'; // table name
    protected $_name = 'tbl_qualification_type';
	private $lobjDbAdpt;

	public function init(){
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

    public function getData($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('qt'=>$this->_name))
            ->joinleft(array('b'=>'tbl_award_level'),'b.Id = qt.qt_level',array('GradeDesc'=>'b.GradeDesc'))
            ->joinleft(array('c'=>'registry_values'),'c.id = qt.qt_prog_type',array('desctype'=>'c.name'))
            ->where("qt.id = ?", (int)$id);

        $row = $db->fetchRow($selectData);

        return $row;
    }

    public function getPaginateData($search=null){
        $db = Zend_Db_Table::getDefaultAdapter();

        if($search){
            $selectData = $db->select()
                ->from(array('qt'=>$this->_name))
                ->joinleft(array('b'=>'tbl_award_level'),'b.Id = qt.qt_level',array('GradeDesc'=>'b.GradeDesc'))
                ->joinleft(array('c'=>'registry_values'),'c.id = qt.qt_prog_type',array('desctype'=>'c.name'))
                ->where("qt.qt_eng_desc LIKE '%".$search['qt_eng_desc']."%'")
                ->where("qt.qt_id LIKE '%".$search['qt_id']."%'")
                ->order('qt.id ASC');

            /*if($search['ss_core_subject']=='1'){
                $selectData->where("ss.ss_core_subject = 1");
            }*/

        }else{
            $selectData = $db->select()
                ->from(array('qt'=>$this->_name));
        }

        return $selectData;
    }

    public function addData($postData){
        $auth = Zend_Auth::getInstance();

        $data = array(
            'qt_id' => $postData['qt_id'],
            'qt_eng_desc' => $postData['qt_eng_desc'],
            'qt_mal_desc' => $postData['qt_mal_desc'],
            'qt_prog_type' => $postData['qt_prog_type'],
            'qt_level' => $postData['qt_level'],
            'qt_subj_min' => $postData['qt_subj_min'],
            'qt_mohe_code' => $postData['qt_mohe_code'],
            'qt_create_date' => $postData['UpdDate'],
            'qt_create_user' => $postData['UpdUser'],
            'Active' => $postData['Active']
        );

        return $this->insert($data);
    }

    public function updateData($postData,$id){

        $data = array(
            'qt_id' => $postData['qt_id'],
            'qt_eng_desc' => $postData['qt_eng_desc'],
            'qt_mal_desc' => $postData['qt_mal_desc'],
            'qt_subj_min'  => $postData['qt_subj_min'],
            'qt_mohe_code'  => $postData['qt_mohe_code'],
            'Active' => $postData['Active'],
            'qt_modify_user' => $postData['UpdUser'],
            'qt_modify_date' => $postData['UpdDate']
        );

        $this->update($data, "id = ".(int)$id);
    }

	/*public function fnaddQualification($formData) {
		unset($formData['IdSubject']);
		unset($formData['sub_qualifications']);
		unset( $formData['sub_qualification']);
		$this->insert($formData);
		$insertId = $this->lobjDbAdpt->lastInsertId($this->_name,'IdQualification');
    	return $insertId;
	}*/

	public function fnupdateQualification($formData,$lintIdQualification) { //Function for updating the Qualification details
		unset ( $formData ['Save'] );
		unset ( $formData ['IdSubject'] );
		unset( $formData['sub_qualification']);
		unset( $formData['sub_qualifications']);
		$where = 'idQualification= '.$lintIdQualification;
		$this->update($formData,$where);
	}

	public function fngetQualificationDetails_old() { //Function to get the Qualification details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from($this->_name);
                $lstrSelect->order('qualification_type_id');
                $lstrSelect->order('QualificationRank');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

    public function fngetQualificationDetails() { //Function to get the Qualification details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array( "a" => "tbl_qualification_type" ));
        //$lstrSelect->joinLeft(array('b' => 'tbl_award_level'), 'a.qt_level = b.Id');
        $lstrSelect->joinLeft( array( 'b' => 'tbl_award_level' ), 'a.qt_level = b.id', array( 'b.GradeDesc' ) );
        $lstrSelect->order('a.id');
        $lstrSelect->order('a.qt_level');
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        //echo $lstrSelect;
        return $larrResult;
    }

	public function fngetQualificationDetail(){//Function to get the subject semester list
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array('a'=>'tbl_qualificationmaster'),array("key"=>"a.IdQualification","value"=>"a.QualificationLevel"));
		 
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnSearchQualification($post = array()) { //Function for searching the Qualification details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("qm"=>"tbl_qualification_type"),array("qm.*"))
		->where('qm.qt_eng_desc like "%" ? "%"',$post['field3'])
		->where('qm.qt_id like "%" ? "%"',$post['field2'])
        ->order("qm.id");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}



	public function fnDeleteQualification($IdQualification) {  // function to delete qualification details
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = 'tbl_qualificationmaster';
		$where = $db->quoteInto('idQualification = ?', $IdQualification);
		$db->delete('tbl_qualificationmaster', $where);
	}

	/*public function fngetQualificationListold(){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_qualificationmaster"),array("key"=>"a.IdQualification","value"=>"a.QualificationLevel"))
		->where('a.Active = ?',1)
                ->order("a.QualificationLevel");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}*/

    public function fngetQualificationList(){
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a"=>"tbl_qualification_type"),array("key"=>"a.id","value"=>"a.qt_eng_desc"))
            ->where('a.Active = ?',1)
            ->order("a.qt_level");
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);

        return $larrResult;
    }

    /*public function fngetsubjectList(){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>$this->_name),array("key"=>"a.IdSubject","value"=>"CONCAT_WS(' - ',IFNULL(a.SubjectName,''),IFNULL(a.SubjectCode,''))"))
            ->order("a.SubjectName");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }*/

}