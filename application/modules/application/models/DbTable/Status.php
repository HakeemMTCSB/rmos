<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_Status extends Zend_Db_Table_Abstract
{
    //put your code here
    protected $_name = 'tbl_status_sts';
    protected $_primary = "sts_Id";

    /*
     * list status
     * 
     * @on 28/06/2014
     */
    public function fnGetStatusList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_status_sts"), array("value" => "a.*"))
            ->join(array('b' => 'tbl_program_scheme'), 'a.sts_programScheme = b.IdProgramScheme')
            ->join(array('c' => 'tbl_program'), 'c.IdProgram = b.IdProgram')
            ->order("a.sts_sequence");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * list status by id
     * 
     * @on 28/06/2014
     */
    public function fnGetStatusListById($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_status_sts"), array("value" => "a.*"))
            ->join(array('b' => 'tbl_application_configs'), 'b.id = a.IdConfig')
            ->where('a.sts_Id = ?', $id)
            ->order("a.sts_Id");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    /*
     * list status
     * 
     * @on 28/06/2014
     */
    public function fnGetStatusListByProgramAndStdCtgy($sts_programScheme, $sts_stdCtgy)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_status_sts"), array("value" => "a.*"))
            ->join(array('b' => 'tbl_program_scheme'), 'a.sts_programScheme = b.IdProgramScheme')
            ->join(array('c' => 'tbl_program'), 'c.IdProgram = b.IdProgram')
            ->join(array('d' => 'tbl_definationms'), 'd.idDefinition = a.sts_name')
            ->where('a.sts_programScheme = ?', $sts_programScheme)
            ->where('a.sts_stdCtgy = ?', $sts_stdCtgy)
            ->order("a.sts_Id");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get status basud on program scheme and status type
     * 
     * @on 18/07/2014
     */
    public function fnGetStatusByProgramAndStsType($sts_programScheme, $sts_name, $stdCtgy)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_status_sts"), array("value" => "a.*"))
            ->join(array('b' => 'tbl_program_scheme'), 'a.sts_programScheme = b.IdProgramScheme')
            ->join(array('c' => 'tbl_program'), 'c.IdProgram = b.IdProgram')
            ->where('a.sts_programScheme = ?', $sts_programScheme)
            ->where('a.sts_name = ?', $sts_name)
            ->where('a.sts_stdCtgy = ?', $stdCtgy)
            ->order("a.sts_Id");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function fnGetMaxId()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_status_sts"), array("value" => "max(a.sts_Id)"));
        $larrResult = $lobjDbAdpt->fetchOne($lstrSelect);
        return $larrResult;
    }

    public function getCopyStatus($IdScheme, $programId = 0, $sts_programScheme = 0, $sts_stdCtgy = 0)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_status_sts"), array("value" => "a.*"))
            ->join(array('b' => 'tbl_application_configs'), 'a.IdConfig = b.id')
            ->join(array('c' => 'registry_values'), 'a.sts_name = c.id')
            ->where('b.IdScheme = ?', $IdScheme)
            ->where('b.IdProgram = ?', $programId ?? 0)
            ->where('b.IdProgramScheme = ?', $sts_programScheme ?? 0)
            ->where('b.category = ?', $sts_stdCtgy ?? 0)
            ->order("a.sts_Id")
            ->group('a.sts_Id');
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function checkStatusDuplicate($id, $sts_name)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_status_sts"), array("value" => "a.*"))
            ->join(array('b' => 'tbl_definationms'), 'a.sts_name = b.IdDefinition')
            ->where('a.IdConfig = ?', $id)
            ->where('a.sts_name = ?', $sts_name)
            ->order("a.sts_Id");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    public function getCopyStatusTemplate($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $select = $lobjDbAdpt->select()
            ->from(array('a' => 'tbl_statustemplate_stp'), array('value' => '*'))
            ->where('a.stp_Id = ?', $id);

        $result = $lobjDbAdpt->fetchRow($select);
        return $result;
    }

    public function insertCopyStatusTemplate($bind)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_statustemplate_stp', $bind);
        $id = $db->lastInsertId('tbl_statustemplate_stp', 'stp_Id');
        return $id;
    }

    public function getCopyStatusAttachment($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'tbl_statusattachment_sth'), array('value' => '*'))
            ->where('a.sth_stpId = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function insertCopyStatusAttachment($bind)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_statusattachment_sth', $bind);
        $id = $db->lastInsertId('tbl_statusattachment_sth', 'sth_Id');
        return $id;
    }

    public function getStatusList($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a" => "tbl_status_sts"), array("value" => "a.*"))
            ->join(array('b' => 'tbl_application_configs'), 'a.IdConfig = b.id')
            ->joinLeft(array('e' => 'tbl_statustemplate_stp'), 'e.stp_Id = a.sts_emailNotification', array('emailTitle' => 'e.stp_title', 'emailContentEng' => 'e.contentEng', 'emailContentMy' => 'e.contentMy', 'emailStp_tplType' => 'e.stp_tplType'))
            ->joinLeft(array('et' => 'registry_values'), 'et.id = e.stp_tplType', array('emailType' => 'et.name'))
            ->joinLeft(array('m' => 'tbl_statustemplate_stp'), 'm.stp_Id = a.sts_message', array('messageTitle' => 'm.stp_title', 'messsageContentEng' => 'm.contentEng', 'messageContentMy' => 'm.contentMy', 'messageStp_tplType' => 'm.stp_tplType'))
            ->joinLeft(array('mt' => 'registry_values'), 'mt.id = m.stp_tplType', array('messageType' => 'mt.name'))
            ->join(array('d' => 'registry_values'), 'd.id = a.sts_name', array('statusName' => 'd.name'))
            ->where('a.IdConfig = ?', $id)
            ->order("a.sts_sequence");
        $result = $db->fetchAll($select);
        return $result;
    }

}

?>