<?php
//require_once 'Zend/Controller/Action.php';
class Application_Model_DbTable_Specializationstudyfield extends Zend_Db_Table_Abstract {
    /**
     * The default table name
     */
    protected $_name = 'tbl_studyfield';
    protected $_primary = "id";

    public function init(){
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function getData($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('ss'=>$this->_name))
            ->where("ss.id = ?", (int)$id);

        $row = $db->fetchRow($selectData);
        return $row;
    }

    public function getPaginateData($search=null){
        $db = Zend_Db_Table::getDefaultAdapter();

        if($search){
            $selectData = $db->select()
                ->from(array('ss'=>$this->_name))
                ->where("ss.sf_eng_desc LIKE '%".$search['sf_eng_desc']."%'")
                ->where("ss.sf_mal_desc LIKE '%".$search['sf_mal_desc']."%'")
                ->order('ss.sf_eng_desc ASC');

            /*if($search['ss_core_subject']=='1'){
                $selectData->where("ss.ss_core_subject = 1");
            }*/

        }else{
            $selectData = $db->select()
                ->from(array('ss'=>$this->_name));
        }

        return $selectData;
    }


    public function addData($postData){
        $auth = Zend_Auth::getInstance();

        $data = array(
            //'ss_id' => $postData['ss_id'],
            'ss_subject_code' => $postData['ss_subject_code'],
            'ss_subject' => $postData['ss_subject'],
            'ss_subject_bahasa' => $postData['ss_subject_bahasa'],
            'ss_core_subject' => $postData['ss_core_subject'],
            'Active' => $postData['Active']
        );

        return $this->insert($data);
    }


    public function updateData($postData,$id){

        $data = array(
            'ss_subject_code' => $postData['ss_subject_code'],
            'ss_subject' => $postData['ss_subject'],
            'ss_subject_bahasa' => $postData['ss_subject_bahasa'],
            'ss_core_subject' => $postData['ss_core_subject'],
            'Active' => $postData['Active']
        );

        $this->update($data, "ss_id = ".(int)$id);
    }

    public function deleteData($id){
        if($id!=0){
            $this->delete("ss_id = ".(int)$id);
        }
    }

    public function fngetStudyFieldList(){
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a"=>"tbl_studyfield"),array("key"=>"a.id","value"=>"a.sf_eng_desc"))
            ->where('a.Active = ?',1)
            ->order("a.id");
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);

        return $larrResult;
    }

    /*public function fngetmappings($id) {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->join(array('a' => $this->_name),'',array())
            ->joinleft(array('b'=>'tbl_studyfield_type'),'a.sf_type = b.id',array('a.sf_eng_desc','a.sf_type','a.sf_id'))
            //->join(array('c'=>"tbl_subject"),'c.IdSubject = b.IdSubject',array('c.SubjectName','c.IdSubject','c.SubjectCode'))
            ->where("a.sf_type = ?",$id);
        $result = $this->fetchAll($select);
        return $result->toArray();
    }

    public function fndeletemappings($id) {
        $where = $this->lobjDbAdpt->quoteInto('sf_type = ?', $id);
        $this->lobjDbAdpt->delete($this->_name, $where);
    }

    public function fnadd($data) {
        $this->insert($data);
    }*/


}

