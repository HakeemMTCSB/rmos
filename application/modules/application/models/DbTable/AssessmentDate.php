<?php

class Application_Model_DbTable_AssessmentDate extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_assessment_date';
	protected $_primary = "id";
		
	public function getData($id=0){
		$id = (int)$id;
		
		if($id!=0){
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('aad'=>$this->_name))
					->where('aas.id = '.$id);
							
			$row = $db->fetchRow($select);
		}else{
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('aad'=>$this->_name));
								
			$row = $db->fetchAll($select);
		}
		
		return $row;
		
	}
	
	public function getLockData(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('aad'=>$this->_name))
				->where('aad.lock_status = 1');

							
		$row = $db->fetchAll($select);
				
		return $row;
		
	}
	
	
	
	public function addData($postData){
		$auth = Zend_Auth::getInstance(); 
		
		$data = array(
		        'date' => $postData['date'],
		        'lock_status' => $postData['lock_status'],
				'lock_by' => $postData['lock_by'],
				'lock_date' => $postData['lock_date']
				);
		
		return $this->insert($data);
	}
	
	public function updateData($postData,$id){
		
		$auth = Zend_Auth::getInstance(); 
		
		$data = array(
		        'aas_set_code' => $postData['aas_set_code'],
				'aas_total_quest' => $postData['aas_total_quest']
		);
			
		$this->update($data, 'aas_id = '. (int)$id);
	}
	
	public function deleteData($id){
		if($id!=0){
			$data = array(
		        'aas_status' => 0
			);
			
			$this->update($data, 'aas_id = '. (int)$id);
		}
	}
	
	
	

}

