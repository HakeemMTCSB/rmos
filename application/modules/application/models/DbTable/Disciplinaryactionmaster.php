<?php
	class Application_Model_DbTable_Disciplinaryactionmaster extends Zend_Db_Table {
		
		
		//Get Definition Type Id For Curricular Activity
		public function fnGetDefinitionTypeId(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_definationtypems"),array("a.idDefType") )
					 				->where("a.defTypeDesc LIKE 'Disciplinary Action%'");
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult['idDefType'];
		}
		
		
		//Get Disciplinary Action List
		public function fnGetDisciplinaryActionTypeList(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc") )
					 				->join(array("b"=>"tbl_definationtypems"),'a.idDefType = b.idDefType')
					  				->where("b.defTypeDesc LIKE 'Disciplinary Actions%'")
					  				->order("a.DefinitionDesc");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
		
		public function fnCheckDisciplinaryActionSettings($IdDisciplinaryType,$DisciplinaryName){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrMsg = "";
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_disciplinaryactionmaster"),array("a.DiscMsg".$DisciplinaryName) )
					 				->where("a.idDisciplinaryActionType = ?",$IdDisciplinaryType);
			//echo $DisciplinaryName."=>".$lstrSelect."<br/>";
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			if($larrResult["DiscMsg".$DisciplinaryName] == "" || $larrResult["DiscMsg".$DisciplinaryName] == "0"){
				$lstrMsg = "No";
			}else{
				$lstrMsg = "Yes";
			}
			//echo $DisciplinaryName."=>".$lstrMsg."<br/>";
			return $lstrMsg;
		}
		
		//Get Details of Disciplinary Action Master
		public function fnGetDisciplinaryActionMasterDetails(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$larrNewResult = array();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc") )
					 				->join(array("b"=>"tbl_definationtypems"),'a.idDefType = b.idDefType')
					  				->where("b.defTypeDesc LIKE 'Disciplinary Actions%'");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			for($lintI=0;$lintI<count($larrResult);$lintI++){
				$larrNewResult[$lintI]["idDisciplinaryActionType"] = $larrResult[$lintI]["key"];
				$larrNewResult[$lintI]["DisciplinaryDesc"] = $larrResult[$lintI]["value"];				
				$larrNewResult[$lintI]["CollegeHeadSetting"] = self::fnCheckDisciplinaryActionSettings($larrResult[$lintI]["key"],"CollegeHead");
				$larrNewResult[$lintI]["ParentSetting"] = self::fnCheckDisciplinaryActionSettings($larrResult[$lintI]["key"],"Parent");
				$larrNewResult[$lintI]["SponsorSetting"] = self::fnCheckDisciplinaryActionSettings($larrResult[$lintI]["key"],"Sponor");
				$larrNewResult[$lintI]["RegistrarSetting"] = self::fnCheckDisciplinaryActionSettings($larrResult[$lintI]["key"],"Registar");
			}
			
			return $larrNewResult;
		}
		
		//Search On Disciplinary Action Master Details
		public function fnSearchDisciplinaryActionMasterDetails($postData){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lintDisciplinaryType = $postData['field1'];
			$larrNewResult = array();
			
			if($lintDisciplinaryType!= ""){
				$lstrSelect = $lobjDbAdpt->select()
    	   								->from(array("a"=>"tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc") )
					 					->join(array("b"=>"tbl_definationtypems"),'a.idDefType = b.idDefType')
       									->where("a.idDefinition = ?",$lintDisciplinaryType);
       			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				$larrNewResult["0"]["idDisciplinaryActionType"] = $larrResult["key"];
				$larrNewResult["0"]["DisciplinaryDesc"] = $larrResult["value"];				
				$larrNewResult["0"]["CollegeHeadSetting"] = self::fnCheckDisciplinaryActionSettings($larrResult["key"],"CollegeHead");
				$larrNewResult["0"]["ParentSetting"] = self::fnCheckDisciplinaryActionSettings($larrResult["key"],"Parent");
				$larrNewResult["0"]["SponsorSetting"] = self::fnCheckDisciplinaryActionSettings($larrResult["key"],"Sponor");
				$larrNewResult["0"]["RegistrarSetting"] = self::fnCheckDisciplinaryActionSettings($larrResult["key"],"Registar");
			}else{
				$larrResult = self::fnGetDisciplinaryActionTypeList();
				for($lintI=0;$lintI<count($larrResult);$lintI++){
					$larrNewResult[$lintI]["idDisciplinaryActionType"] = $larrResult[$lintI]["key"];
					$larrNewResult[$lintI]["DisciplinaryDesc"] = $larrResult[$lintI]["value"];					
					$larrNewResult[$lintI]["CollegeHeadSetting"] = self::fnCheckDisciplinaryActionSettings($larrResult[$lintI]["key"],"CollegeHead");
					$larrNewResult[$lintI]["ParentSetting"] = self::fnCheckDisciplinaryActionSettings($larrResult[$lintI]["key"],"Parent");
					$larrNewResult[$lintI]["SponsorSetting"] = self::fnCheckDisciplinaryActionSettings($larrResult[$lintI]["key"],"Sponor");
					$larrNewResult[$lintI]["RegistrarSetting"] = self::fnCheckDisciplinaryActionSettings($larrResult[$lintI]["key"],"Registar");
				}
			}	
			
			
			
			return $larrNewResult;
		}
		
		//Function To Get Disciplinary Action Master Details
		public function fnGetDisciplinaryActionMasterDetailsByDefintionId($idDefinition){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_disciplinaryactionmaster"))
       								->join(array("b" => "tbl_definationms"),"a.idDisciplinaryActionType = b.idDefinition",array("b.DefinitionDesc AS DisciplinaryDesc"))
       								->where("a.idDisciplinaryActionType = ?",$idDefinition);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			
			return $larrResult;
		}
		
		//Function To Save Student's Disciplinary Action
		public function fnSaveDisciplinaryActionDetails($post){			
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrMsg = "";
			$lstrTable = "tbl_disciplinaryactionmaster";
			unset($post['idDisciplinaryActionMaster']);			
			$lstrMsg = $lobjDbAdpt->insert($lstrTable,$post);
			return $lstrMsg;
		}
		
		
		//Function To Update Student's Disciplinary Action Master
		public function fnUpdateDisciplinaryActionMasterDetails($IdDiscActnMst,$formData){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrMsg = "";
			$lstrTable = "tbl_disciplinaryactionmaster";
			if(strlen($IdDiscActnMst) == "0"){
				unset($formData['idDisciplinaryActionMaster']);
				$lstrMsg = $lobjDbAdpt->insert($lstrTable,$formData);
			}else{
				$lstrWhere = "idDisciplinaryActionMaster = ".$IdDiscActnMst;
				$lstrMsg = $lobjDbAdpt->update($lstrTable,$formData,$lstrWhere);
			}
			return $lstrMsg;
		} 
		
		
}