<?php

class Application_Model_DbTable_ProposedProgram extends Zend_Db_Table_Abstract 
{

	protected $_name = 'propose_program';

	public function list_all($program_id = null) {

	    $select  = $this->select($this->_name)
	    	   ->setIntegrityCheck(false)
	          ->join(array("Program" => "tbl_program"), 'pp_IdProgram = Program.IdProgram', array('IdProgram','ProgramName','ProgramCode','Award'))
	          ->join(array("Definition" => "tbl_definationms"), 'Definition.idDefinition=Program.Award ',array('award'=>'DefinitionDesc'))
	          ->order('Program.ProgramName');

	    if($program_id != null) {
	      $select->where( $this->_name . '.pp_IdProgramEntry = ?', $program_id);
	    }

	    $rows = $this->fetchAll($select);
	    return $rows; 
	}

	public function listByProgramEntry($programentry_id) {

	    $select  = $this->select($this->_name)
	    	   ->setIntegrityCheck(false)
	          ->join(array("Program" => "tbl_program"), 'pp_IdProgram = Program.IdProgram', array('IdProgram','ProgramName','ProgramCode','Award'))
	          ->join(array("Definition" => "tbl_definationms"), 'Definition.idDefinition=Program.Award ',array('award'=>'DefinitionDesc'))
	          ->where( $this->_name . '.pp_IdProgramEntry = ?', $programentry_id)
	          ->order('Program.ProgramName');

	    $rows = $this->fetchAll($select);
	    return $rows; 
	}


	public function list_selected($programs) {

	    $select  = $this->select()
	    	   ->setIntegrityCheck(false)
	          ->join(array("Program" => "tbl_program"), 'pp_IdProgram = Program.IdProgram', array('IdProgram','ProgramName','ProgramCode','Award'))
	          ->join(array("Definition" => "tbl_definationms"), 'Definition.idDefinition=Program.Award ',array('award'=>'DefinitionDesc'))
	          ->where('Program.IdProgram IN (?)', $programs)
	          ->order('Program.ProgramName');

	    $rows = $this->fetchAll($select);
	    return $rows; 
	}

  public function clear_program($program_id) {

  	$where = $this->getAdapter()->quoteInto('pp_IdProgramEntry = ?',$program_id);
	$this->delete($where);
	return (true);

  }

  public function delete_program($pp_id) {

  	$where = $this->getAdapter()->quoteInto('pp_id = ?',$pp_id);
	$this->delete($where);
	return (true);
	
  }


}