<?php 
class Application_Model_DbTable_Batchagent extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'applicant_profile';
	protected $_primary = "appl_id";
	protected $_db;
	
	function App_Model_Application_DbTable_Batchagent(){
		$this->_db = Zend_Registry::get('dbapp');
	}
	
	function getlookupID($ltype="SEX",$code){
		$sql="select ssd_id from sis_setup_detl where ssd_code='$ltype' and ssd_seq='$code'";
		//echo $sql;
		$row = $this->_db->fetchRow($sql);	
		if($row){
			return $row["ssd_id"];
		}else{
			return 1;
		}
	}
	
	function getstateID($code){
		$sql="select idState from tbl_state where StateCode='$code'";
		$row = $this->_db->fetchRow($sql);	
		return $row["idState"];		
	}
	
	function getcityID($stateid,$citycode){
		$sql="select idCity from tbl_city where idState='$stateid' and CityCode='$citycode'";
		$row = $this->_db->fetchRow($sql);	
		return $row["idCity"];				
	}
	
	function gethsID($hscode){
		$sql= "SELECT sm_id
			FROM `school_master`
			WHERE `sm_school_code` = '$hscode'";
		$row = $this->_db->fetchRow($sql);	
		return $row["sm_id"];			
	}
	
	function getAcademicYear($code){
		$sql = "select ay_id from tbl_academic_year where ay_code like '%$code'";
		//echo $sql;
		$row = $this->_db->fetchRow($sql);	
		return $row["ay_id"];		
	}
	function getIntake($code){
		$sql = "select IdIntake from tbl_intake where IntakeId like '%$code'";
		//echo $sql;
		$row = $this->_db->fetchRow($sql);	
		return $row["IdIntake"];		
	}	
	
	function checkApplicantExist($applid){
		
		$sql = "select * from applicant_transaction where at_pes_id='$applid'";
		//echo $sql;
		$row = $this->_db->fetchRow($sql);	
		if($row){
			return $row;	
		}else{
			return null;
		}
		
	}
}
?>