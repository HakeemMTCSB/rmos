<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationInitialConfiguration
 *
 * @author mtcsb
 */
class Application_Model_DbTable_ApplicationItem extends Zend_Db_Table_Abstract
{
    //put your code here
    protected $_name = 'tbl_application_item';

    /*
     * get item list
     * 
     * @on 6/6/2014
     */
    public function fnGetItemList()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item"), array("value" => "a.*"))
            ->join(array('c' => 'registry_values'), 'c.id = a.formType', array("formDesc" => "c.name"))
            ->joinLeft(array('b' => 'tbl_application_section'), 'b.id = a.item_section_id', array("sectionName" => "b.name"))
            ->order("a.name");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnGetItemListSearch($data = NULL)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item"), array("value" => "a.*"))
            ->order("a.name");

        if ($data != NULL) {
            if (isset($data['field1']) && $data['field1'] != '') {
                $lstrSelect->where("a.name LIKE '%" . $data['field1'] . "%'");
            }
            if (isset($data['field2']) && $data['field2'] != '') {
                $lstrSelect->where("a.itemMalayName LIKE '%" . $data['field2'] . "%'");
            }
            if (isset($data['field3']) && $data['field3'] != '') {
                $lstrSelect->where('a.description = ?', $data['field3']);
            }
        }

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    /*
     * get item by id
     * 
     * @on 6/6/2014
     */
    public function fnGetItemById($id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'registry_values'), 'a.formType = b.id', array('DefinitionDesc'=>'code'))
            ->where('a.id = ?', $id)
            ->order("a.id");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

    /*
     * get item list for dropdown
     * 
     * @on 6/6/2014
     */
    public function fnGetItemListDropDown($section_id)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "tbl_application_item"), array("value" => "a.*"))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.formType = b.idDefinition')
            ->where('a.item_section_id = ?', $section_id)
            ->order("a.name");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }
}

?>