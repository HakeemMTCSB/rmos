<?php

/**
 * This is for migrated applicants' documents
 */
class Application_Model_DbTable_ChecklistDocuments extends Zend_Db_Table_Abstract {
    //put your code here
	protected $_name = 'applicant_transaction';
	protected $_primary = "at_trans_id";

	public function getData($at_pes_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select() ->from(array('a'=>'migrate_applicant_files'))
								 ->joinLeft(array('mt' => 'migrate_applicant_mapping'), 'mt.applicant_id=a.applicant_id', array())
								 ->where('mt.ref_id = ?',$at_pes_id);
		
		$row = $db->fetchAll($select);
		
		return $row;
	}

	public function getSingle($id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select() ->from(array('a'=>'migrate_applicant_files'))
								 ->joinLeft(array('mt' => 'migrate_applicant_mapping'), 'mt.applicant_id=a.applicant_id', array())
								 ->where('a.id = ?',$id);
		
		$row = $db->fetchRow($select);
		
		return $row;
	}

	public function updateData($data, $id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$db->update('migrate_applicant_files', $data, array('id = ?' => $id) );
	}

    public function getResults($form=array()){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from(array('at'=>$this->_name),array('*',new Zend_Db_Expr("(select count(*) from migrate_applicant_files as ma where ma.applicant_id=mt.applicant_id) as total_docs ")))
					  ->joinleft(array('ap'=>'applicant_profile'),'ap.appl_id=at.at_appl_id',array('ap.appl_fname','ap.appl_mname','ap.appl_lname','ap.appl_idnumber','appl_type_nationality','appl_email','appl_password','appl_category'))
					  ->joinLeft(array('apr'=>'applicant_program'),'apr.ap_at_trans_id = at.at_trans_id',array('apr.ap_prog_scheme'))
					  ->joinLeft(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme=apr.ap_prog_scheme')
                      ->joinLeft(array('p'=>'tbl_program'),'ps.IdProgram=p.IdProgram',array('ProgramName','ProgramCode'))
					  ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=apr.program_mode', array('ProgramMode'=>'DefinitionDesc'))
					  ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=apr.mode_study', array('StudyMode'=>'DefinitionDesc'))
					  ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=apr.program_type', array('ProgramType'=>'DefinitionDesc'))
					  ->joinLeft(array("dc" => "tbl_definationms"),'dc.idDefinition=ap.appl_category', array('appl_category_name'=>'DefinitionDesc'))
					  ->joinLeft(array("dd" => "tbl_definationms"),'dd.idDefinition=at.at_status', array('at_status_name'=>'DefinitionDesc'))
					  ->joinLeft(array('it' => 'tbl_intake'), 'at.at_intake=it.IdIntake', array('IntakeId'))
						->joinLeft(array('mt' => 'migrate_applicant_mapping'), 'at.at_pes_id=mt.ref_id', array())
					  ->order("total_docs desc")
					  ->order("at.at_trans_id desc")
					  ->where('ap.MigrateCode IS NOT NULL');

			
							  
					  
		if( isset($form['name']) && $form['name']!="" )	{			
			 $select->where("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) like '%".$form['name']."%'");	 				 
		}
		
		if( isset($form['pes_no']) && $form['pes_no']!="" )	{
			$select->where("at.at_pes_id like '".$form['pes_no']."'");
		}
		
		if( isset($form['personal_id']) && $form['personal_id']!="" )	{
			$select->where("ap.appl_idnumber like '".$form['personal_id']."'");
		}
		
		if( isset($form['program_id']) && $form['program_id']!="" && $form['program_id']!="0" )	{
			$select->where("p.IdProgram= '".$form['program_id']."'");
		}
		
		if( isset($form['program_scheme_id']) && $form['program_scheme_id']!="" && $form['program_scheme_id']!="0" )	{
			$select->where("apr.ap_prog_scheme = '".$form['program_scheme_id']."'");
		}
		
		if( isset($form['application_status']) && $form['application_status']!="" && $form['application_status']!="ALL" )	{
			$select->where("at.at_status = '".$form['application_status']."'");
		}

		if( isset($form['intake_id']) && $form['intake_id']!=""){
			$select->where("at.at_intake = '".$form['intake_id']."'");
		}

		echo $select;
		exit;

		return $select;

		//$row = $db->fetchAll($select);
		
		//return $row;
	}


}
?>