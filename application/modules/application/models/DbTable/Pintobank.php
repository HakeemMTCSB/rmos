<?php 
class Application_Model_DbTable_Pintobank extends Zend_Db_Table {
	protected $_name = 'appl_pin_to_bank'; // table name
	
	function addData($data){

		$dbapd = Zend_Db_Table::getDefaultAdapter();
		$sql = $dbapd->select()
			->from(array("a"=>"appl_pin_to_bank"),array("a.*"))
			->where ("billing_no = ?",$data["billing_no"]);
			
		$record=$dbapd->fetchrow($sql);
		if(!is_array($record)){
			$this->insert($data);
		}
		
	}
	public function fngetNomorList($intakeid) { //Function to get the Activity details
		$lstrSelect = $this->select()
						->from($this->_name)
						->where("intakeId = ?",$intakeid);
		$larrResult = $this->fetchAll($lstrSelect);
		return $larrResult;
	}	
	
	public function getTotalVacant($intakeid){
			$lstrSelect = $this->select()
						->from($this->_name,array("total"=>"count(*)"))
						->where("intakeId = ?",$intakeid)
						->where("status = 'E'");
			$larrResult = $this->fetchrow($lstrSelect);
		return $larrResult["total"];	
	}
	
	public function getTotalUsed($intakeid){
			$lstrSelect = $this->select()
						->from($this->_name,array("total"=>"count(*)"))
						->where("intakeId = ?",$intakeid)
						->where("status = 'P'");
			$larrResult = $this->fetchrow($lstrSelect);
		return $larrResult["total"];	
	}	
}
?>