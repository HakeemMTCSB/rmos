<?php 

class Application_Model_DbTable_EmailBlastHistory extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'email_blast_history';
	protected $_primary = "id";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getData($id=""){
	
		$select = $this->_db->select()
					  ->from($this->_name)
					  ->order('send_date desc');
					  
		if($id)	{			
			 $select->where($this->_primary." ='".$id."'");
			 $row = $this->_db->fetchRow($select);	
			 
		}	else{			
			$row = $this->_db->fetchAll($select);	
		}	  
		
		 return $row;
	}
	
	public function getPaginateData(){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $this->_db->select()
					  ->from($this->_name)
					  ->order('send_date desc');
						
		return $select;
	}
	
	public function getPaginateDataCollege($collegeId){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $this->_db->select()
			->from(array('ebh'=>$this->_name))
			->joinLeft(array('u'=>'tbl_user'), 'u.iduser = ebh.send_by', array())
			->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('ebt_create_by_name'=>'Fullname'))
			->where('ts.idCollege = '.$collegeId)
			->order('send_date desc');
	
		return $select;
	}
}
?>