<?php
class Application_Model_DbTable_Agentapplicanttagging extends Zend_Db_Table { 	
	
	protected $_name = 'tbl_agentapplicanttagging';
	private $lobjDbAdpt;

  	public function init(){
    	$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	}	
	
  	public function fnassignedagent($id){
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_agentapplicanttagging"),array("a.*"))
		 				 ->where("a.IdStudent = ?",$id);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
  	}
  	
  	public function fngetagentName($agentid){
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  		$lstrSelect = $lobjDbAdpt->select()
				->from(array("a"=>"tbl_agentmaster"),array("a.*"))
				->join(array("b"=>"tbl_staffmaster"),'b.IdStaff = a.AgentName',array("b.FirstName AS AgentName"))
				->where("b.IdStaff = ?",$agentid)
				->order("b.FirstName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
  	}
  	
	public function fnassignagent($larrformData){
		unset($larrformData['Save']);
		unset($larrformData['StudentName']);
		$this->insert($larrformData);
	}
	
	public function fnupdateassignagent($larrformData,$IdAgentTag){
		unset($larrformData['Save']);
		unset($larrformData['StudentName']);
    	$where = 'IdAgentApplicantTag = '.$IdAgentTag;
    	$this->update($larrformData,$where);
	}
}
	