<?php

class Application_Model_DbTable_QualificationType extends Zend_Db_Table
{
    protected $_name = 'tbl_qualification_type'; // table name
    private $lobjDbAdpt;

    public function init()
    {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function getQualificationList()
    {
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a" => $this->_name))
            ->where('a.Active = ?', 1);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }



    public function fnaddQualification($formData)
    {
        unset($formData['IdSubject']);
        unset($formData['sub_qualifications']);
        unset($formData['sub_qualification']);
        $this->insert($formData);
        $insertId = $this->lobjDbAdpt->lastInsertId($this->_name, 'IdQualification');
        return $insertId;
    }

    public function fnupdateQualification($formData, $lintIdQualification)
    { //Function for updating the Qualification details
        unset ($formData ['Save']);
        unset ($formData ['IdSubject']);
        unset($formData['sub_qualification']);
        unset($formData['sub_qualifications']);
        $where = 'idQualification= ' . $lintIdQualification;
        $this->update($formData, $where);
    }

    public function fngetQualificationDetails()
    { //Function to get the Qualification details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()->from($this->_name);
        $lstrSelect->order('qualification_type_id');
        $lstrSelect->order('QualificationRank');
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fngetQualificationDetail()
    {//Function to get the subject semester list
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $this->lobjDbAdpt->select()
            ->from(array('a' => 'tbl_qualificationmaster'), array("key" => "a.IdQualification", "value" => "a.QualificationLevel"));

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function fnSearchQualification($post = array())
    { //Function for searching the Qualification details
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("qm" => "tbl_qualificationmaster"), array("qm.*"))
            ->where('qm.QualificationLevel like "%" ? "%"', $post['field3'])
            ->where('qm.QualificationRank like "%" ? "%"', $post['field2'])
            ->order("qm.qualification_type_id")
            ->order("qm.QualificationRank");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }


    public function fnDeleteQualification($IdQualification)
    {  // function to delete qualification details
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = 'tbl_qualificationmaster';
        $where = $db->quoteInto('idQualification = ?', $IdQualification);
        $db->delete('tbl_qualificationmaster', $where);
    }



}