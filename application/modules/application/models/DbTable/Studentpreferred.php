<?php

class Application_Model_DbTable_Studentpreferred extends Zend_Db_Table_Abstract {

  protected $_name = 'tbl_student_preffered';
  private $lobjDbAdpt;

  public function init() {
    $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  }

  public function addPreferred($data){    
    $this->insert($data);
  }

  public function deletePreferred($idApplicantion){
    $where = $this->lobjDbAdpt->quoteInto('IdApplication= ?', $idApplicantion);
    $this->lobjDbAdpt->delete($this->_name, $where);
  }

  public function checkPreferredexist($idApplicantion){
    $select = $this->select()
                    ->from(array('a' => 'tbl_student_preffered'))
                    ->where('a.IdApplication = ?', $idApplicantion);
    return $result = $this->lobjDbAdpt->fetchAll($select);

  }

}

?>
