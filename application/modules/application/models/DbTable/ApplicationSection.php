<?php 
class Application_Model_DbTable_ApplicationSection extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_application_section';
	protected $_primary = "id";
	
	/*
     * Get Sidebar for onlineapplication
     */
	public function getSection($IdConfig){
			
		    $db = Zend_Db_Table::getDefaultAdapter();
		    
		    $sql = $db->select()
		              ->from(array("a" => "tbl_application_initialconfig") )
		              ->join(array('ap'=>'tbl_application_configs'),'a.IdConfig = ap.id',array())
		              ->join(array('s'=>'tbl_application_section'),'s.id = a.sectionID',array('main_section_id'=>'id','name','instruction','status'))
//		              ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = a.programID')
		              ->where("ap.id  = ?", $IdConfig)
		              ->order("a.seq_order");

		   return $sidebarList = $db->fetchAll($sql);
		    
	}
	
	public function getFirstSection($programID=0){	
			
		    $db = Zend_Db_Table::getDefaultAdapter();
		    
		    $sql = $db->select()
			              ->from(array("a" => "tbl_application_initialconfig")) 
			              ->join(array('s'=>'tbl_application_section'),'s.id = a.sectionID',array('name','instruction','status'))              
			              ->where("a.programID = ?", $programID)
			              ->order("a.seq_order asc")
			              ->limit(1);
	      
		   return $sidebarList = $db->fetchRow($sql);
		    
	}
	
}

?>