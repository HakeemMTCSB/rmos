<?php 
class Application_Model_DbTable_Studentdocument extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_studentappfilesupload';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fnaddStudentdoc($data) {	
			$this->insert($data);
	}
	
     public function fnstudentdoclist($IdApplication) { //Function to get the Program Branch details
 		$select = $this->select()
                ->setIntegrityCheck(false)  
                ->join(array('a' => 'tbl_studentappfilesupload'),array('IdApplication'))
                ->join(array('c'=>'tbl_definationms'),'a.DocumentType = c.idDefinition')
                ->where('IdApplication = ?',$IdApplication);
       $result = $this->fetchAll($select);
       return $result->toArray();
     }
     
	public function deletedoc($IdStudentAppFilesUpload){
		$this->delete('IdStudentAppFilesUpload = ' .(int)$IdStudentAppFilesUpload);
	}
  public function fnstudentdocumentlist($IdApplication) { //Function to get the Program Branch details
 		$select = $this->select()
                ->setIntegrityCheck(false)  
                ->join(array('a' => 'tbl_documentdetails'),array('IdApplication'))                
                ->where('IdApplication = ?',$IdApplication);
       $result = $this->fetchAll($select);
       return $result->toArray();
     }

    

}
?>