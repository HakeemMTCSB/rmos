<?php
class Registration_Form_ItemRegistrationReportSearchForm extends Zend_Dojo_Form {
    
    public function init() {
        $this->setMethod('post');
        $programId = $this->getAttrib('programid');
        
        $model = new Registration_Model_DbTable_Report();
        
        //program
        $program = new Zend_Form_Element_Select('program');
	$program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('required', 'true');
        $program->setAttrib('onchange', 'get_programscheme(); getSemester(this.value);');
	$program->removeDecorator("Label");
        
        $progList = $model->getProgramList();
        
        $program->addMultiOption('', '--Select--');
        
        if ($progList){
            foreach ($progList as $progLoop){
                $program->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
            }
        }
        
        //program scheme
        $programscheme = new Zend_Form_Element_Select('programscheme');
	$programscheme->removeDecorator("DtDdWrapper");
        $programscheme->setAttrib('class', 'select');
        $programscheme->setAttrib('multiple', 'true');
	$programscheme->removeDecorator("Label");
        
        if ($programId){
            $progSchemeList = $model->getProgScheme($programId);
            
            if ($progSchemeList){
                foreach ($progSchemeList as $progSchemeLoop){
                    $programscheme->addMultiOption($progSchemeLoop['IdProgramScheme'], $progSchemeLoop['mop']." ".$progSchemeLoop['mos']." ".$progSchemeLoop['pt']);
                }
            }
        }
        
        //intake
        $intake = new Zend_Form_Element_Select('intake');
	$intake->removeDecorator("DtDdWrapper");
        $intake->setAttrib('class', 'select');
        $intake->setAttrib('multiple', 'true');
	$intake->removeDecorator("Label");
        
        $intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $intakeList = $intakeModel->fngetIntakeList();
        
        if ($intakeList){
            foreach ($intakeList as $intakeLoop){
                $intake->addMultiOption($intakeLoop['IdIntake'], $intakeLoop['IntakeDesc']);
            }
        }
        
        //item register
        $itemregister = new Zend_Form_Element_Select('itemregister');
	$itemregister->removeDecorator("DtDdWrapper");
        $itemregister->setAttrib('class', 'select');
        $itemregister->setAttrib('required', 'true');
	$itemregister->removeDecorator("Label");
        
        $itemList = $model->getItemRegisterList();
        
        $itemregister->addMultiOption('', '--Select--');
        
        if ($itemList){
            foreach($itemList as $itemLoop){
                $itemregister->addMultiOption($itemLoop['idDefinition'], $itemLoop['DefinitionDesc']);
            }
        }
        
        //registered semester
        $semester = new Zend_Form_Element_Select('semester');
	$semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('class', 'select');
        $semester->setAttrib('required', 'true');
	$semester->removeDecorator("Label");
        
        $semester->addMultiOption('', '--Select--');
        
        if ($programId){
            $progDet = $model->getProgramDetail($programId);
            $semList = $model->getSemester($progDet['IdScheme']);
            
            if ($semList){
                foreach ($semList as $semLoop){
                    $semester->addMultiOption($semLoop['IdSemesterMaster'], $semLoop['SemesterMainName']." - ".$semLoop['SemesterMainCode']);
                }
            }
        }
        
        //student branch
        $studentbranch = new Zend_Form_Element_Select('studentbranch');
	$studentbranch->removeDecorator("DtDdWrapper");
        $studentbranch->setAttrib('class', 'select');
	$studentbranch->removeDecorator("Label");
        
        $studentbranch->addMultiOption('', '--All--');
        
        $branchList = $model->getBranchList();
        
        //subject branch
        $subjectbranch = new Zend_Form_Element_Select('subjectbranch');
	$subjectbranch->removeDecorator("DtDdWrapper");
        $subjectbranch->setAttrib('class', 'select');
	$subjectbranch->removeDecorator("Label");
        
        $subjectbranch->addMultiOption('', '--All--');
        
        if ($branchList){
            foreach ($branchList as $branchLoop){
                $subjectbranch->addMultiOption($branchLoop['IdBranch'], $branchLoop['BranchName']);
                $studentbranch->addMultiOption($branchLoop['IdBranch'], $branchLoop['BranchName']);
            }
        }
        
        $status = new Zend_Form_Element_Select('status');
	$status->removeDecorator("DtDdWrapper");
        $status->setAttrib('class', 'select');
	$status->removeDecorator("Label");
        
        $definationDB = new App_Model_General_DbTable_Definationms();
        $statusList = $definationDB->getDataByType(20);
        
        $status->addMultiOption('', '--All--');
        
        if ($statusList){
            foreach($statusList as $statusLoop){
                $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
            }
        }
        
        //form elements
        $this->addElements(array(
            $program,
            $programscheme,
            $intake,
            $itemregister,
            $semester,
            $studentbranch,
            $subjectbranch,
            $status
        ));
    }
}