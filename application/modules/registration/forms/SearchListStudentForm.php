<?php 

class Registration_Form_SearchListStudentForm extends Zend_Form
{
  	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
		$this->setAttrib('onsubmit','return validate_form();');

		//Intake
		$this->addElement('select','IdIntake', array(
			'label'=>$this->getView()->translate('Intake')
		));
		
		$intakeDB = new App_Model_Record_DbTable_Intake();
		
		$this->IdIntake->addMultiOption(null,"-- All --");		
		foreach($intakeDB->fngetlatestintake() as $intake){
			$this->IdIntake->addMultiOption($intake["key"],$intake["value"]);
		}
		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme Name'),		   
		    'onchange'=>'getProgramScheme(this);'
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- All --");		
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}
				
		
		 //Program Scheme
		$this->addElement('select','IdProgramScheme', array(
			'label'=>$this->getView()->translate('Programme Scheme')
		));
		
		$this->IdProgramScheme->addMultiOption(null,$this->getView()->translate('-- All --'));			
		
		$this->IdProgramScheme->setRegisterInArrayValidator(false);
		
		
				
	
			//Branch
		$this->addElement('select','IdBranch', array(
			'label'=>$this->getView()->translate('Branch')
		));
		
		$branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();
				
		$this->IdBranch->addMultiOption(null,$this->getView()->translate('-- All --'));		
		foreach($branchDb->fnGetAllBranchList() as $branch){
			$this->IdBranch->addMultiOption($branch["key"],$branch["value"]);
		}
	
		
		//Student Name
		$this->addElement('text','student_name', array(
			'label'=>$this->getView()->translate('Student Name')
		));		
			
		//Student Registration ID from
		$this->addElement('text','registrationId', array(
			'label'=>$this->getView()->translate('Student ID')
		));
		
		
		/*//Student Registration ID to
		$this->addElement('text','registrationId_to', array(
			'label'=>$this->getView()->translate('Student ID To')
		));
		*/
		
		 //Sorting
		$this->addElement('select','Sorting', array(
			'label'=>$this->getView()->translate('Sort by')
		));
				
		$this->Sorting->addMultiOption(1,$this->getView()->translate('Student Name'));
		$this->Sorting->addMultiOption(2,$this->getView()->translate('Student ID'));
		$this->Sorting->addMultiOption(3,$this->getView()->translate('Programme'));			
		$this->Sorting->addMultiOption(4,$this->getView()->translate('Registration Date'));
		$this->Sorting->setValue(4);
	
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        //button
		$this->addElement('submit', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>