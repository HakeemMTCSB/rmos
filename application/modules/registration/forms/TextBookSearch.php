<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 23/10/2015
 * Time: 9:59 AM
 */
class Registration_Form_TextBookSearch extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');
        $programid = $this->getAttrib('programid');

        //load model
        $model = new Registration_Model_DbTable_Studentregistration();
        $definationDB = new App_Model_General_DbTable_Definationms();
        $model2 = new Records_Model_DbTable_Report();
        $lobjstudentregistrationModel = new Registration_Model_DbTable_Studentregistration();

        //program
        $Program = new Zend_Form_Element_Select('Program');
        $Program->removeDecorator("DtDdWrapper");
        $Program->setAttrib('class', 'select');
        $Program->setAttrib('required', true);
        $Program->removeDecorator("Label");
        $Program->setAttrib('onchange', 'getProgramScheme(this.value); loadSemesterDropdown(this.value);');

        $Program->addMultiOption('', '-- Select --');

        $ProgramList = $model->getProgram();

        if ($ProgramList){
            foreach ($ProgramList as $ProgramLoop){
                $Program->addMultiOption($ProgramLoop['IdProgram'], $ProgramLoop['ProgramName']);
            }
        }

        //program scheme
        $ProgramScheme = new Zend_Form_Element_Select('ProgramScheme');
        $ProgramScheme->removeDecorator("DtDdWrapper");
        $ProgramScheme->setAttrib('class', 'select');
        //$ProgramScheme->setAttrib('required', true);
        $ProgramScheme->removeDecorator("Label");

        $ProgramScheme->addMultiOption('', '-- Select --');

        if ($programid){
            $programSchemeList = $model2->getProgramScheme($programid);

            if ($programSchemeList){
                foreach ($programSchemeList as $programSchemeLoop){
                    $ProgramScheme->addMultiOption($programSchemeLoop['IdProgramScheme'], $programSchemeLoop['mop'].' '.$programSchemeLoop['mos'].' '.$programSchemeLoop['pt']);
                }
            }
        }

        //semester
        $Semester = new Zend_Form_Element_Select('Semester');
        $Semester->removeDecorator("DtDdWrapper");
        $Semester->setAttrib('class', 'select');
        $Semester->setAttrib('required', true);
        $Semester->removeDecorator("Label");

        $Semester->addMultiOption('', '-- Select --');

        if ($programid){
            $programInfo = $lobjstudentregistrationModel->getProgramForSem($programid);
            $semList = $lobjstudentregistrationModel->getSemBasedOnList($programInfo['IdScheme']);

            if ($semList){
                foreach ($semList as $semLoop){
                    $Semester->addMultiOption($semLoop['IdSemesterMaster'], $semLoop['SemesterMainName']);
                }
            }
        }

        //Method
        $Method = new Zend_Form_Element_Select('Method');
        $Method->removeDecorator("DtDdWrapper");
        $Method->setAttrib('class', 'select');
        $Method->removeDecorator("Label");

        $Method->addMultiOption('', '-- Select --');
        $Method->addMultiOption(1, 'Self Collect');
        $Method->addMultiOption(2, 'Courier');

        //student name
        $Name = new Zend_Form_Element_Text('Name');
        $Name->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //student id
        $Studentid = new Zend_Form_Element_Text('Studentid');
        $Studentid->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $this->addElements(array(
            $Program,
            $ProgramScheme,
            $Semester,
            $Method,
            $Name,
            $Studentid
        ));
    }
}