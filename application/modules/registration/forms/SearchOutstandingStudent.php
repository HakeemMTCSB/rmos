<?php 

class Registration_Form_SearchOutstandingStudent extends Zend_Form
{
		
    protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
		$terminate = $this->getAttrib('terminate');				      
				
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme Name'),
                        'required'=>false,
                        'onchange'=>'getProgramScheme(this.value)'
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- All --");		
		foreach($programDb->getData()  as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}
				
		 //Program Scheme
		$this->addElement('select','IdProgramScheme', array(
			'label'=>$this->getView()->translate('Programme Scheme'),
		    'required'=>false
		));
		
		$this->IdProgramScheme->addMultiOption(null,$this->getView()->translate('-- All --'));		
				
		
		//Intake
		$this->addElement('select','IdIntake', array(
			'label'=>$this->getView()->translate('Intake'),
		    'required'=>false
		));
		
		$intakeDB = new App_Model_Record_DbTable_Intake();
		
		$this->IdIntake->addMultiOption(null,"-- All --");		
		foreach($intakeDB->fngetlatestintake() as $intake){
			$this->IdIntake->addMultiOption($intake["key"],$intake["value"]);
		}
				
		//Branch
		$this->addElement('select','IdBranch', array(
			'label'=>$this->getView()->translate('Branch'),
		    'required'=>false
		));
		
		$branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();
				
		$this->IdBranch->addMultiOption(null,$this->getView()->translate('-- All --'));		
		foreach($branchDb->fnGetAllBranchList() as $branch){
			$this->IdBranch->addMultiOption($branch["key"],$branch["value"]);
		}
	
		//Applicant Name
		$this->addElement('text','applicant_name', array(
			'label'=>$this->getView()->translate('Student Name'),
			'class'=>'input-txt'

		));
		
		//Student ID
		$this->addElement('text','student_id', array(
			'label'=>$this->getView()->translate('Student ID'),
			'class'=>'input-txt'
		));
				
		
		//Status
		$this->addElement('select','profile_status', array(
			'label'=>$this->getView()->translate('Profile Status')
		));
		
		$maintenanceModelObj =  new GeneralSetup_Model_DbTable_Maintenance();
		$status = $maintenanceModelObj->fnfetchProfileStatus("a.idDefType = '20'");
		
		$this->profile_status->addMultiOption(null,"-- All --");	
                if ($terminate==true){
                    foreach($status as $s){
                        if ($s["key"] == 92 || $s["key"]==94){
                            $this->profile_status->addMultiOption($s["key"],$s["value"]);
                        }
                    }
                }else{
                    foreach($status as $s){
                            $this->profile_status->addMultiOption($s["key"],$s["value"]);
                    }
                }
                
                //semester Status
		$this->addElement('select','semester_status', array(
			'label'=>$this->getView()->translate('Semester Status')
		));
                
                $this->semester_status->addMultiOption('',"-- All --");
                
                $semesterstatus = $maintenanceModelObj->fnfetchProfileStatus("a.idDefType = '32'");
		
                if ($semesterstatus){
                    foreach($semesterstatus as $semesterstatusloop){
                        $this->semester_status->addMultiOption($semesterstatusloop["key"],$semesterstatusloop["value"]);
                    }
                }
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        	//button
		$this->addElement('submit', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>