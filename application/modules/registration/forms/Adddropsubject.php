<?php
class Registration_Form_Adddropsubject extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
       
        
		$IdStudentRegistration  = new Zend_Form_Element_Hidden('IdStudentRegistration');
        $IdStudentRegistration->removeDecorator("DtDdWrapper");
        $IdStudentRegistration->setAttrib('dojoType',"dijit.form.TextBox");
        $IdStudentRegistration->removeDecorator("Label");
        $IdStudentRegistration->removeDecorator('HtmlTag');
        
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
	
   
        $Save = new Zend_Form_Element_Submit('Save');
       	$Save->label = $gstrtranslate->_("Drop Course");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtnAuto";
         		
        $Add = new Zend_Form_Element_Submit('Add');
       	$Add->label = $gstrtranslate->_("Add Course");
        $Add->dojotype="dijit.form.Button";
        $Add->removeDecorator("DtDdWrapper");
        $Add->removeDecorator('HtmlTag')
         		->class = "NormalBtnAuto";
        //form elements
        $this->addElements(array($Save,$UpdDate,$UpdUser,$IdStudentRegistration,$Add));

    }
}