<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 12/12/2015
 * Time: 1:11 PM
 */
class Registration_Form_WorkloadReportForm extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');

        //year
        $year = new Zend_Form_Element_Text('year');
        $year->setAttrib('required', 'true');
        $year->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $this->addElements(array(
            $year
        ));
    }
}