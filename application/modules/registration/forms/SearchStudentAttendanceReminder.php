<?php 

class Registration_Form_SearchStudentAttendanceReminder extends Zend_Form
{
  	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

		$type = $this->getAttrib('type');

		//Semester
		if ($type){
			$this->addElement('select','IdSemester', array(
				'label'=>$this->getView()->translate('Semester'),
				//'required'=>true,
				'onchange'=>'getCourse(); getMonth(this.value)'
			));
		}else{
			$this->addElement('select','IdSemester', array(
				'label'=>$this->getView()->translate('Semester'),
				'required'=>true,
				'onchange'=>'getCourse(); getMonth(this.value)'
			));
		}
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Select --");
		foreach($semesterDB->fnGetSemesterList() as $semester){
			if($this->_locale=='ms_MY'){
				$this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester['value2']);
			}else{
				$this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester["value"]);
			}
		}
		
		
		//Program
		if ($type){
			$this->addElement('select','IdProgram', array(
				'label'=>$this->getView()->translate('Programme Name'),
				//'required'=>true,
				'onchange'=>'getCourse();'
			));
		}else{
			$this->addElement('select','IdProgram', array(
				'label'=>$this->getView()->translate('Programme Name'),
				'required'=>true,
				'onchange'=>'getCourse();'
			));
		}
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- Select --");
		foreach($programDb->getData() as $program){
			if($this->_locale=='ms_MY'){
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}
				
		
		//Course
		if ($type){
			$this->addElement('select','IdSubject', array(
				'label'=>$this->getView()->translate('Course Name'),
				//'required'=>true,
				'onchange'=>'getCourseGroup();',
				'registerInArrayValidator'=>false
			));
		}else{
			$this->addElement('select','IdSubject', array(
				'label'=>$this->getView()->translate('Course Name'),
				'required'=>true,
				'onchange'=>'getCourseGroup();',
				'registerInArrayValidator'=>false
			));
		}

		$this->IdSubject->addMultiOption(null,"-- Select --");

		//Setup Type
		if ($type){
			$this->addElement('select','att_type', array(
				'label'=>$this->getView()->translate('Type'),
				//'required'=>true,
				'onchange'=> 'viewMonth(this.value);'
			));
		}else{
			$this->addElement('select','att_type', array(
				'label'=>$this->getView()->translate('Type'),
				'required'=>true,
				'onchange'=> 'viewMonth(this.value);'
			));
		}

		$this->att_type->addMultiOption('', '-- Select --');
		$this->att_type->addMultiOption(1, 'Attendance Warning');
		$this->att_type->addMultiOption(2, 'Exam Bar');

		if ($type){

		}else {
			//month
			$this->addElement('select', 'month', array(
				'label' => $this->getView()->translate('Month'),
				'required' => false,
				//'disabled'=>true,
				'multiple' => true,
				'registerInArrayValidator' => false
			));

			$this->month->getDecorator('Label')->setOption('class', 'required');
		}
		
		//Group
		
		$this->addElement('select','IdGroup', array(
			'label'=>$this->getView()->translate('Section Name'),
			'registerInArrayValidator'=>false
		));
		$this->IdGroup->addMultiOption(null,"-- Select --");
		
		
		//Item

		if ($type){

		}else {
			$this->addElement('select', 'IdItem', array(
				'label' => $this->getView()->translate('Item Name'),
				'required' => true
			));
			$this->IdItem->addMultiOption(null, "-- Select --");

			$defDb = new App_Model_General_DbTable_Definationms();

			foreach ($defDb->getDataByType(160) as $item) {
				$this->IdItem->addMultiOption($item["idDefinition"], $item["DefinitionDesc"]);
			}
		}

		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
      
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>


<script type="text/javascript">
	function getCourse(){

		var semester_id = $('#IdSemester').val();
		var program_id = $('#IdProgram').val();
		
		if(semester_id!=='' && program_id!=''){
			$.ajax({
				url: "/default/ajax-utility/search-program-course",
			    type: "post",
			    async : false,
			    data: {'semester_id':semester_id,'program_id':program_id},
			    dataType: 'json',
			    success: function(data){
				    
			    	$('#IdSubject').empty();
					$('#IdSubject').append('<option value="">-- Select --</option>');
				    	
				    	$.each(data, function(index) {
				    		$('#IdSubject').append('<option value="'+data[index].IdSubject+'">'+data[index].SubCode+' - '+data[index].SubjectName+'</option>');	
				    	});
			          
				},
				error:function(){
			          alert("failure");
				},
				beforeSend: function() {
					showLoading('IdSubject');
			      
				},
				complete: function() {
					hideLoading('IdSubject');
				}  
			});
		}
	}


	function getCourseGroup(){

		var semester_id = $('#IdSemester').val();
		var program_id = $('#IdProgram').val();
		var subject_id = $('#IdSubject').val();
		
		if(semester_id!=='' && program_id!='' && subject_id!=''){
			$.ajax({
				url: "/default/ajax-utility/search-course-group",
			    type: "post",
			    async : false,
			    data: {'subject_id':subject_id,'semester_id':semester_id},
			    dataType: 'json',
			    success: function(data){
				    
			    	$('#IdGroup').empty();
					$('#IdGroup').append('<option value="">-- Select --</option>');
				    	
				    	$.each(data, function(index) {
				    		$('#IdGroup').append('<option value="'+data[index].IdCourseTaggingGroup+'">'+data[index].GroupName+'</option>');	
				    	});
			          
				},
				error:function(){
			          alert("failure");
				},
				beforeSend: function() {
					showLoading('IdGroup');
			      
				},
				complete: function() {
					hideLoading('IdGroup');
				}  
			});
		}
	}

	function showLoading(target){
		$('#'+target).hide();
		$('#'+target).after("<div id='loading'><img src='/images/spinner.gif' width='30' heigth='30' /></div>");
	}

	function hideLoading(target){
		$('#loading').remove();
		$('#'+target).show();
	}
</script>