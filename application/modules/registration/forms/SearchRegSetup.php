<?php 

class Registration_Form_SearchRegSetup extends Zend_Form
{
		
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
						       
		
		
		//Intake
		$this->addElement('select','IdIntake', array(
			'label'=>$this->getView()->translate('Intake'),
		    'required'=>false
		));
		
		$intakeDB = new App_Model_Record_DbTable_Intake();
		
		$this->IdIntake->addMultiOption(null,"-- All --");		
		foreach($intakeDB->fngetlatestintake() as $intake){
			$this->IdIntake->addMultiOption($intake["key"],$intake["value"]);
		}
				
	
		//Registration Date
		$this->addElement('text','registration_date', array(
			'label'=>$this->getView()->translate('Registration Date')
		));
			
				
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>