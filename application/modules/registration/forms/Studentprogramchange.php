<?php
class Registration_Form_Studentprogramchange extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate');
    	
    	$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day-1),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
    	 
		$IdStudentRegistration  = new Zend_Form_Element_Hidden('IdStudentRegistration');
        $IdStudentRegistration->removeDecorator("DtDdWrapper");
        $IdStudentRegistration->setAttrib('dojoType',"dijit.form.TextBox");
        $IdStudentRegistration->removeDecorator("Label");
        $IdStudentRegistration->removeDecorator('HtmlTag');
        
        

        
		$IdProgram  = new Zend_Dojo_Form_Element_FilteringSelect('IdProgram');
        $IdProgram->removeDecorator("DtDdWrapper");
        $IdProgram->setAttrib('required',"true") ;
        $IdProgram->removeDecorator("Label");
        $IdProgram->removeDecorator('HtmlTag');
        $IdProgram->setRegisterInArrayValidator(false);
        //$IdProgram->setAttrib('OnChange', "fnGetSubjectList(this)");
		$IdProgram->setAttrib('dojoType',"dijit.form.FilteringSelect");
        

		$Description = new Zend_Form_Element_Textarea('Description');	
        $Description	->setAttrib('cols', '30')
        				->setAttrib('rows','3')
        				->setAttrib('style','width = 10%;')
        				->setAttrib('maxlength','250')
        				->setAttrib('dojoType',"dijit.form.SimpleTextarea")
        				->setAttrib('style','margin-top:10px;border:1px light-solid #666666;color:#666666;font-size:11px')
        				->removeDecorator("DtDdWrapper")
        				->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
	
        				
        				
			$EffectiveDate = new Zend_Dojo_Form_Element_DateTextBox('EffectiveDate');
			$EffectiveDate->setAttrib('required',"true")
						->setAttrib('dojoType',"dijit.form.DateTextBox")
						->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "$dateofbirth")
						->setAttrib('title',"dd-mm-yyyy")		 				
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
    

        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
       	$Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";


        //form elements
        $this->addElements(array($IdStudentRegistration,$Description,$EffectiveDate,$UpdDate,$UpdUser,$Save,$IdProgram));

    }
}