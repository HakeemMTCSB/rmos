<?php 

class Registration_Form_SearchCourseItemHistory extends Zend_Form
{
  	protected $_locale;
  	protected $_idstudentregsubjects;
  	protected $_idstudentregistration;
  	protected $_idsemester;
  	protected $_idsubject;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	public function setIdstudentregsubjects($value) {
		$this->_idstudentregsubjects = $value;
	}
	public function setIdstudentregistration($value) {
		$this->_idstudentregistration = $value;
	}
	public function setIdsemester($value) {
		$this->_idsemester = $value;
	}
	public function setIdsubject($value) {
		$this->_idsubject = $value;
	}
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

		
		$this->addElement('hidden','idSemester',array('value'=>$this->_idsemester));		
		$this->addElement('hidden','idSubject',array('value'=>$this->_idsubject));		
		$this->addElement('hidden','id',array('value'=>$this->_idstudentregistration));		
		$this->addElement('hidden','idRegSubject',array('value'=>$this->_idstudentregsubjects));			
		
		 //Status
		$this->addElement('select','status', array(
			'label'=>$this->getView()->translate('Status')
		));
				
		$this->status->addMultiOption(null,$this->getView()->translate('-- All --')); 
		$this->status->addMultiOption(0,$this->getView()->translate('Add'));
		$this->status->addMultiOption(2,$this->getView()->translate('Drop'));
		$this->status->addMultiOption(3,$this->getView()->translate('Withdraw'));
				
		
		 //Sorting
		$this->addElement('select','Sorting', array(
			'label'=>$this->getView()->translate('Sort by')
		));
				
		$this->Sorting->addMultiOption(3,$this->getView()->translate('Created Date Asc'));
		$this->Sorting->addMultiOption(4,$this->getView()->translate('Created Date Desc'));
		$this->Sorting->setValue(1);
	
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>