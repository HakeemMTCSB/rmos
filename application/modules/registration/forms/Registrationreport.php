<?php
class Registration_Form_Registrationreport extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate');
    	
    	
    	$scheme = new Zend_Dojo_Form_Element_FilteringSelect('Scheme');
        $scheme->setAttrib('required',"false");
        $scheme->setAttrib('onChange', 'getsemester(this)');
        $scheme->removeDecorator("DtDdWrapper");
        $scheme->removeDecorator("Label");
        $scheme->removeDecorator('HtmlTag');
		$scheme->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$Generate = new Zend_Form_Element_Submit('Generate');
		$Generate->label = $gstrtranslate->_("Generate");
        $Generate->dojotype="dijit.form.Button";
        $Generate->removeDecorator("DtDdWrapper");
        $Generate->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateformat = "{datePattern:'dd-MM-yyyy'}";
		
		$RegistrationDateFrom = new Zend_Form_Element_Text('RegistrationDateFrom');
		$RegistrationDateFrom->setAttrib('dojoType',"dijit.form.DateTextBox");
		$RegistrationDateFrom->setAttrib('onChange',"dijit.byId('RegistrationDateTo').constraints.min = arguments[0];");
		$RegistrationDateFrom->setAttrib('required',"false")
		->setAttrib('constraints', "$dateformat")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
			
		$RegistrationDateTo = new Zend_Form_Element_Text('RegistrationDateTo');
		$RegistrationDateTo->setAttrib('dojoType',"dijit.form.DateTextBox");
		$RegistrationDateTo->setAttrib('onChange',"dijit.byId('RegistrationDateFrom').constraints.max = arguments[0];") ;
		$RegistrationDateTo->setAttrib('required',"false")
		->setAttrib('constraints', "$dateformat")
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
		
		$this->addElements(array($scheme,
								 $Generate,
								 $RegistrationDateFrom,
								 $RegistrationDateTo
						   ));
		
    }
}