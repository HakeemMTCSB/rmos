<?php 

class Registration_Form_SearchCourseHistory extends Zend_Form
{
  	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

		//Student ID
		$this->addElement('text','student_id', array(
			'label'=>$this->getView()->translate('Student ID'),
		    'required'=>true,
		));	
		
		
		//Semester
		$this->addElement('select','IdSemester', array(
			'label'=>$this->getView()->translate('Semester')
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- All --");		
		foreach($semesterDB->fnGetSemesterList() as $semester){
			if($this->_locale=='ms_MY'){
				$this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester['value2']);
			}else{
				$this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester["value"]);
			}
		}
					
		
		 //Status
		$this->addElement('select','status', array(
			'label'=>$this->getView()->translate('Status')
		));
				
		$this->status->addMultiOption(null,$this->getView()->translate('-- All --'));
		$this->status->addMultiOption(0,$this->getView()->translate('Pre-Register'));
		$this->status->addMultiOption(1,$this->getView()->translate('Register'));
		$this->status->addMultiOption(2,$this->getView()->translate('Drop'));
		$this->status->addMultiOption(3,$this->getView()->translate('Withdraw'));	
		$this->status->addMultiOption(9,$this->getView()->translate('Pre-Repeat'));
		$this->status->addMultiOption(4,$this->getView()->translate('Repeat'));
		
		
		//Subject Code
		$this->addElement('text','subject_code', array(
			'label'=>$this->getView()->translate('Course Code')
		));	
		
		 //Sorting
		$this->addElement('select','Sorting', array(
			'label'=>$this->getView()->translate('Sort by')
		));
				
		$this->Sorting->addMultiOption(1,$this->getView()->translate('Semester Date Asc'));		
		$this->Sorting->addMultiOption(2,$this->getView()->translate('Semester Date Desc'));
		$this->Sorting->addMultiOption(3,$this->getView()->translate('Created Date Asc'));
		$this->Sorting->addMultiOption(4,$this->getView()->translate('Created Date Desc'));
		$this->Sorting->setValue(1);
	
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
      
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>