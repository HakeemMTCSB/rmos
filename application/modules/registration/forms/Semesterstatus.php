<?php
class Registration_Form_Semesterstatus extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate');
    	 
		
		
		$IdSemester = new Zend_Dojo_Form_Element_FilteringSelect('IdSemester[]');
        $IdSemester->removeDecorator("DtDdWrapper");
        $IdSemester->setAttrib('required',"true") ;
        $IdSemester->removeDecorator("Label");
        $IdSemester->removeDecorator('HtmlTag');
        $IdSemester->setRegisterInArrayValidator(false);
       	//$IdSemester->setAttrib('OnChange', "fnGetSubjectList(this)");
		$IdSemester->setAttrib('dojoType',"dijit.form.FilteringSelect");

        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
        $IdSemesterStatus  = new Zend_Form_Element_Hidden('IdSemesterStatus[]');
        $IdSemesterStatus->removeDecorator("DtDdWrapper");
        $IdSemesterStatus->removeDecorator("Label");
        $IdSemesterStatus->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
       	$Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
   			$SemesterStatus  = new Zend_Form_Element_Radio('SemesterStatus[]');
        	$SemesterStatus->addMultiOptions(array('0' => 'Previous','1' => 'Present','2' => 'Future'))
        				->setvalue('1')
        				->setSeparator('&nbsp;')
        				->setAttrib('dojoType',"dijit.form.RadioButton")
        				->removeDecorator("DtDdWrapper")
        				->removeDecorator("Label")
        				->removeDecorator('HtmlTag');	


        //form elements
        $this->addElements(array($IdSemester,$UpdDate,$UpdUser,$Save,$SemesterStatus,$IdSemesterStatus));

    }
}