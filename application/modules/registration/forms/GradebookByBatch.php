<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 2/3/2016
 * Time: 9:17 AM
 */
class Registration_Form_GradebookByBatch extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');

        $programid = $this->getAttrib('programid');
        $formData = $this->getAttrib('formData');

        $model = new Registration_Model_DbTable_GradebookByBatch();
        $model2 = new Registration_Model_DbTable_ForecastReport();
        $model3 = new GeneralSetup_Model_DbTable_AttendanceReport();

        //programme
        $IdProgram = new Zend_Form_Element_Select('IdProgram');
        $IdProgram->removeDecorator("DtDdWrapper");
        $IdProgram->setAttrib('class', 'select');
        $IdProgram->setAttrib('required', '');
        $IdProgram->setAttrib('onchange', 'getSemester(this.value); getCourse(this.value);');
        $IdProgram->removeDecorator("Label");

        $IdProgram->addMultiOption('', '-- Select --');

        $programList = $model->getProgram();

        if ($programList){
            foreach ($programList as $programLoop){
                $IdProgram->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramName']);
            }
        }

        //semester
        $IdSemester = new Zend_Form_Element_Select('IdSemester');
        $IdSemester->removeDecorator("DtDdWrapper");
        $IdSemester->setAttrib('class', 'select');
        $IdSemester->setAttrib('required', '');
        $IdSemester->removeDecorator("Label");

        $IdSemester->addMultiOption('', '-- Select --');

        if ($programid){
            $programInfo = $model2->getProgramById($programid);
            $semesterList = $model2->getSemester($programInfo['IdScheme']);

            if ($semesterList){
                foreach ($semesterList as $semesterLoop){
                    $IdSemester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName']);
                }
            }
        }

        //subject
        $IdSubject = new Zend_Form_Element_Select('IdSubject');
        $IdSubject->removeDecorator("DtDdWrapper");
        $IdSubject->setAttrib('class', 'select');
        ///$IdSubject->setAttrib('required', '');
        $IdSubject->removeDecorator("Label");
        $IdSubject->setAttrib('onchange', 'getCourseSection(this.value);');

        $IdSubject->addMultiOption('', '-- Select --');

        //group
        $IdGroup = new Zend_Form_Element_Select('IdGroup');
        $IdGroup->removeDecorator("DtDdWrapper");
        $IdGroup->setAttrib('class', 'select');
        //$IdGroup->setAttrib('required', '');
        $IdGroup->removeDecorator("Label");
        $IdGroup->setAttrib('onchange', 'getLecturer(this.value);');

        $IdGroup->addMultiOption('', '-- Select --');

        //lecturer
        $IdLecturer = new Zend_Form_Element_Select('IdLecturer');
        $IdLecturer->removeDecorator("DtDdWrapper");
        $IdLecturer->setAttrib('class', 'select');
        //$IdLecturer->setAttrib('required', '');
        $IdLecturer->removeDecorator("Label");

        $IdLecturer->addMultiOption('', '-- Select --');

        //course code
        $subject_code = new Zend_Form_Element_Text('subject_code');
        $subject_code->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //course name
        $subject_name = new Zend_Form_Element_Text('subject_name');
        $subject_name->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        if ($formData){
            if (isset($formData['IdProgram'])){
                $courseList = $model3->getSubjectFromLandscape($formData['IdProgram']);

                if ($courseList){
                    foreach ($courseList as $courseLoop){
                        $IdSubject->addMultiOption($courseLoop['IdSubject'], $courseLoop['SubCode'].' - '.$courseLoop['SubjectName']);
                    }
                }
            }

            if (isset($formData['IdSubject']) && isset($formData['IdSemester']) || isset($formData['IdProgram'])){
                $courseSectionList = $model3->getCourseSection($formData['IdProgram'], $formData['IdSemester'], $formData['IdSubject']);

                if ($courseSectionList){
                    foreach ($courseSectionList as $courseSectionLoop){
                        $IdGroup->addMultiOption($courseSectionLoop['IdCourseTaggingGroup'], $courseSectionLoop['GroupCode'].' - '.$courseSectionLoop['GroupName']);
                    }
                }
            }

            if (isset($formData['IdGroup'])){
                $lecturerList = $model3->getLecturer($formData['IdGroup']);

                if ($lecturerList){
                    foreach ($lecturerList as $lecturerLoop){
                        $IdLecturer->addMultiOption($lecturerLoop['IdStaff'], $lecturerLoop['FullName']);
                    }
                }
            }
        }


        //form elements
        $this->addElements(array(
            $IdProgram,
            $IdSemester,
            $IdSubject,
            $IdGroup,
            $IdLecturer,
            $subject_code,
            $subject_name
        ));
    }
}