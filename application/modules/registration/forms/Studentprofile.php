<?php
class Registration_Form_Studentprofile extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate');
    	$DefmodelObj = new App_Model_Definitiontype();
    	
    	$PermAddressDetails = new Zend_Form_Element_Text('PermAddressDetails');
		$PermAddressDetails->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->setAttrib('maxlength', '100')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PermCountry = new Zend_Form_Element_Select('PermCountry');
		$PermCountry->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('OnChange', "fnGetPermCountryStateList(this,'PermState')")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PermState = new Zend_Form_Element_Select('PermState');
		$PermState->setAttrib('required', "true")->setAttrib('dojoType', "dijit.form.FilteringSelect")->setRegisterInArrayValidator(false)->setAttrib('OnChange', 'fnGetStateCityListPerm')->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$PermCity = new Zend_Dojo_Form_Element_FilteringSelect('PermCity');
		$PermCity->removeDecorator("DtDdWrapper");
		$PermCity->removeDecorator("Label");
		$PermCity->removeDecorator('HtmlTag');
		$PermCity->setAttrib('required', "true");
		$PermCity->setRegisterInArrayValidator(false);
		$PermCity->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$PermZip = new Zend_Form_Element_Text('PermZip');
		$PermZip->setAttrib('required', "true")->setAttrib('maxlength', '10')->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$CorrsAddressDetails = new Zend_Form_Element_Text('CorrsAddressDetails');
		$CorrsAddressDetails->setAttrib('maxlength', '100')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator("DtDdWrapper")->removeDecorator('HtmlTag');
		
		$CorrsCity = new Zend_Dojo_Form_Element_FilteringSelect('CorrsCity');
		$CorrsCity->removeDecorator("DtDdWrapper");
		$CorrsCity->removeDecorator("Label");
		$CorrsCity->removeDecorator('HtmlTag');
		$CorrsCity->setAttrib('required', "false");
		$CorrsCity->setRegisterInArrayValidator(false);
		$CorrsCity->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$CorrsState = new Zend_Form_Element_Select('CorrsState');
		$CorrsState->setRegisterInArrayValidator(false)->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('OnChange', 'fnGetStateCityListCorrs')->setAttrib('required', "false")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$CorrsCountry = new Zend_Form_Element_Select('CorrsCountry');
		$CorrsCountry->setAttrib('dojoType', "dijit.form.FilteringSelect")->setAttrib('OnChange', "fnGetCorrsCountryStateList(this,'CorrsState')")->setAttrib('required', "false")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$CorrsZip = new Zend_Form_Element_Text('CorrsZip');
		$CorrsZip->setAttrib('maxlength', '20')->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$OutcampusAddressDetails = new Zend_Form_Element_Text('OutcampusAddressDetails');
		$OutcampusAddressDetails->setAttrib('maxlength', '100');
		$OutcampusAddressDetails->removeDecorator("DtDdWrapper");
		$OutcampusAddressDetails->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$OutcampusAddressDetails->removeDecorator("Label");
		$OutcampusAddressDetails->removeDecorator("DtDdWrapper");
		$OutcampusAddressDetails->removeDecorator('HtmlTag');

		$OutcampusCity = new Zend_Dojo_Form_Element_FilteringSelect('OutcampusCity');
		$OutcampusCity->removeDecorator("DtDdWrapper");
		$OutcampusCity->removeDecorator("Label");
		$OutcampusCity->removeDecorator('HtmlTag');
		$OutcampusCity->setAttrib('required', "false");
		$OutcampusCity->setRegisterInArrayValidator(false);
		$OutcampusCity->setAttrib('dojoType', "dijit.form.FilteringSelect");

		$OutcampusState = new Zend_Form_Element_Select('OutcampusState');
		$OutcampusState->setRegisterInArrayValidator(false);
		$OutcampusState->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$OutcampusState->setAttrib('OnChange', 'fnGetStateCityListOutcampus');
		$OutcampusState->setAttrib('required', "false");
		$OutcampusState->removeDecorator("DtDdWrapper");
		$OutcampusState->removeDecorator("Label");
		$OutcampusState->removeDecorator('HtmlTag');

		$OutcampusCountry = new Zend_Form_Element_Select('OutcampusCountry');
		$OutcampusCountry->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$OutcampusCountry->setAttrib('OnChange', "fnGetOutcampusCountryStateList(this,'OutcampusState')");
		$OutcampusCountry->setAttrib('required', "false");
		$OutcampusCountry->removeDecorator("DtDdWrapper");
		$OutcampusCountry->removeDecorator("Label");
		$OutcampusCountry->removeDecorator('HtmlTag');

		$OutcampusZip = new Zend_Form_Element_Text('OutcampusZip');
		$OutcampusZip->setAttrib('maxlength', '20');
		$OutcampusZip->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$OutcampusZip->removeDecorator("DtDdWrapper");
		$OutcampusZip->removeDecorator("Label");
		$OutcampusZip->removeDecorator('HtmlTag');

		$HomePhonecountrycode = new Zend_Form_Element_Text('HomePhonecountrycode', array (
			'regExp' => "[0-9]+",
			'invalidMessage' => "Only digits"
		));
		$HomePhonecountrycode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$HomePhonecountrycode->setAttrib('maxlength', '3');
		$HomePhonecountrycode->setAttrib('style', 'width:30px');
		$HomePhonecountrycode->removeDecorator("DtDdWrapper");
		$HomePhonecountrycode->removeDecorator("Label");
		$HomePhonecountrycode->removeDecorator('HtmlTag');

		$HomePhonestatecode = new Zend_Form_Element_Text('HomePhonestatecode', array (
			'regExp' => "[0-9]+",
			'invalidMessage' => "Only digits"
		));
		$HomePhonestatecode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$HomePhonestatecode->setAttrib('maxlength', '2');
		$HomePhonestatecode->setAttrib('style', 'width:30px');
		$HomePhonestatecode->removeDecorator("DtDdWrapper");
		$HomePhonestatecode->removeDecorator("Label");
		$HomePhonestatecode->removeDecorator('HtmlTag');

		$HomePhone = new Zend_Form_Element_Text('HomePhone', array (
			'regExp' => '[0-9]+'
		));
		$HomePhone->setAttrib('style', 'width:93px')->setAttrib('maxlength', '9')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$CellPhonecountrycode = new Zend_Form_Element_Text('CellPhonecountrycode', array (
			'regExp' => "[0-9+]+",
			'invalidMessage' => "Only digits"
		));
		$CellPhonecountrycode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$CellPhonecountrycode->setAttrib('maxlength', '4');
		$CellPhonecountrycode->setAttrib('style', 'width:50px');
		$CellPhonecountrycode->removeDecorator("DtDdWrapper");
		$CellPhonecountrycode->removeDecorator("Label");
		$CellPhonecountrycode->removeDecorator('HtmlTag');

		$CellPhone = new Zend_Form_Element_Text('CellPhone', array (
			'regExp' => '[\0-9()+-]+'
		));
		$CellPhone->setAttrib('style', 'width:109px')->setAttrib('maxlength', '20')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$Faxcountrycode = new Zend_Form_Element_Text('Faxcountrycode', array (
			'regExp' => "[0-9]+",
			'invalidMessage' => "Only digits"
		));
		$Faxcountrycode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$Faxcountrycode->setAttrib('maxlength', '3');
		$Faxcountrycode->setAttrib('style', 'width:30px');
		$Faxcountrycode->removeDecorator("DtDdWrapper");
		$Faxcountrycode->removeDecorator("Label");
		$Faxcountrycode->removeDecorator('HtmlTag');

		$Faxstatecode = new Zend_Form_Element_Text('Faxstatecode', array (
			'regExp' => "[0-9]+",
			'invalidMessage' => "Only digits"
		));
		$Faxstatecode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
		$Faxstatecode->setAttrib('maxlength', '2');
		$Faxstatecode->setAttrib('style', 'width:30px');
		$Faxstatecode->removeDecorator("DtDdWrapper");
		$Faxstatecode->removeDecorator("Label");
		$Faxstatecode->removeDecorator('HtmlTag');

		$Fax = new Zend_Form_Element_Text('Fax', array (
			'regExp' => '[0-9]+'
		));
		$Fax->setAttrib('style', 'width:93px')->setAttrib('maxlength', '9')->removeDecorator("DtDdWrapper")->setAttrib('dojoType', "dijit.form.ValidationTextBox")->removeDecorator("Label")->removeDecorator('HtmlTag');
		
		$SameCorrespAddr = new Zend_Form_Element_Checkbox('SameCorrespAddr');
		$SameCorrespAddr->setAttrib('onClick', 'showcorrespondanceaddr(this.checked)')->setAttrib('dojoType', "dijit.form.CheckBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');

		$SameOutcampusAddr = new Zend_Form_Element_Checkbox('SameOutcampusAddr');
		$SameOutcampusAddr->setAttrib('onClick', 'showOutcampusaddr(this.checked)')->setAttrib('dojoType', "dijit.form.CheckBox")->removeDecorator("DtDdWrapper")->removeDecorator("Label")->removeDecorator('HtmlTag');
		
		$larrrelationlist = $DefmodelObj->fnGetDefinationMs('Relationship Type');    
    $RelationshipType = new Zend_Dojo_Form_Element_FilteringSelect('RelationshipType');
    $RelationshipType->setAttrib('required', "true");
    $RelationshipType->removeDecorator("DtDdWrapper");
    $RelationshipType->removeDecorator("Label");
    $RelationshipType->removeDecorator('HtmlTag');
    $RelationshipType->setAttrib('dojoType', "dijit.form.FilteringSelect");
    $RelationshipType->addMultiOptions($larrrelationlist);

    $RelativeName = new Zend_Form_Element_Text('RelativeName');
    $RelativeName->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $RelationshipType->setAttrib('required', "true");
    $RelativeName->removeDecorator("Label");
    $RelativeName->removeDecorator("DtDdWrapper");
    $RelativeName->removeDecorator('HtmlTag');

    $EmergencyAddressDetails = new Zend_Form_Element_Text('EmergencyAddressDetails');
    $EmergencyAddressDetails->setAttrib('required', "false");
    $EmergencyAddressDetails->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $EmergencyAddressDetails->setAttrib('maxlength', '100');
    $EmergencyAddressDetails->removeDecorator("DtDdWrapper");
    $EmergencyAddressDetails->removeDecorator("Label");
    $EmergencyAddressDetails->removeDecorator('HtmlTag');

    $EmergencyCountry = new Zend_Form_Element_Select('EmergencyCountry');
    $EmergencyCountry->setAttrib('required', "false");
    $EmergencyCountry->setAttrib('dojoType', "dijit.form.FilteringSelect");
    $EmergencyCountry->setAttrib('OnChange', "fnGetEmergencyCountryStateList(this,'PermState')");
    $EmergencyCountry->removeDecorator("DtDdWrapper");
    $EmergencyCountry->removeDecorator("Label");
    $EmergencyCountry->removeDecorator('HtmlTag');

    $EmergencyState = new Zend_Form_Element_Select('EmergencyState');
    $EmergencyState->setAttrib('required', "false");
    $EmergencyState->setAttrib('dojoType', "dijit.form.FilteringSelect");
    $EmergencyState->setRegisterInArrayValidator(false);
    $EmergencyState->setAttrib('OnChange', 'fnGetStateCityListEmergency');
    $EmergencyState->removeDecorator("DtDdWrapper");
    $EmergencyState->removeDecorator("Label");
    $EmergencyState->removeDecorator('HtmlTag');

    $EmergencyCity = new Zend_Dojo_Form_Element_FilteringSelect('EmergencyCity');
    $EmergencyCity->removeDecorator("DtDdWrapper");
    $EmergencyCity->removeDecorator("Label");
    $EmergencyCity->removeDecorator('HtmlTag');
    $EmergencyCity->setAttrib('required', "false");
    $EmergencyCity->setRegisterInArrayValidator(false);
    $EmergencyCity->setAttrib('dojoType', "dijit.form.FilteringSelect");

    $EmergencyZip = new Zend_Form_Element_Text('EmergencyZip');
    $EmergencyZip->setAttrib('required', "true");
    $EmergencyZip->setAttrib('maxlength', '10');
    $EmergencyZip->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $EmergencyZip->removeDecorator("DtDdWrapper");
    $EmergencyZip->removeDecorator("Label");
    $EmergencyZip->removeDecorator('HtmlTag');

    $EmergencyHomePhonecountrycode = new Zend_Form_Element_Text('EmergencyHomePhonecountrycode', array(
                'regExp' => "[0-9]+",
                'invalidMessage' => "Only digits"
            ));

    $EmergencyHomePhonecountrycode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $EmergencyHomePhonecountrycode->setAttrib('maxlength', '3');
    $EmergencyHomePhonecountrycode->setAttrib('style', 'width:30px');
    $EmergencyHomePhonecountrycode->removeDecorator("DtDdWrapper");
    $EmergencyHomePhonecountrycode->removeDecorator("Label");
    $EmergencyHomePhonecountrycode->removeDecorator('HtmlTag');

    $EmergencyHomePhonestatecode = new Zend_Form_Element_Text('EmergencyHomePhonestatecode', array(
                'regExp' => "[0-9]+",
                'invalidMessage' => "Only digits"
            ));
    $EmergencyHomePhonestatecode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $EmergencyHomePhonestatecode->setAttrib('maxlength', '2');
    $EmergencyHomePhonestatecode->setAttrib('style', 'width:30px');
    $EmergencyHomePhonestatecode->removeDecorator("DtDdWrapper");
    $EmergencyHomePhonestatecode->removeDecorator("Label");
    $EmergencyHomePhonestatecode->removeDecorator('HtmlTag');

    $EmergencyHomePhone = new Zend_Form_Element_Text('EmergencyHomePhone', array(
                'regExp' => '[0-9]+'
            ));
    $EmergencyHomePhone->setAttrib('style', 'width:93px');
    $EmergencyHomePhone->setAttrib('maxlength', '9');
    $EmergencyHomePhone->removeDecorator("DtDdWrapper");
    $EmergencyHomePhone->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $EmergencyHomePhone->removeDecorator("Label");
    $EmergencyHomePhone->removeDecorator('HtmlTag');

    $EmergencyCellPhonecountrycode = new Zend_Form_Element_Text('EmergencyCellPhonecountrycode', array(
                'regExp' => "[0-9+]+",
                'invalidMessage' => "Only digits"
            ));
    $EmergencyCellPhonecountrycode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $EmergencyCellPhonecountrycode->setAttrib('maxlength', '4');
    $EmergencyCellPhonecountrycode->setAttrib('style', 'width:50px');
    $EmergencyCellPhonecountrycode->removeDecorator("DtDdWrapper");
    $EmergencyCellPhonecountrycode->removeDecorator("Label");
    $EmergencyCellPhonecountrycode->removeDecorator('HtmlTag');

    $EmergencyCellPhone = new Zend_Form_Element_Text('EmergencyCellPhone', array(
                'regExp' => '[\0-9()+-]+'
            ));
    $EmergencyCellPhone->setAttrib('style', 'width:109px');
    $EmergencyCellPhone->setAttrib('maxlength', '20');
    $EmergencyCellPhone->removeDecorator("DtDdWrapper");
    $EmergencyCellPhone->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $EmergencyCellPhone->removeDecorator("Label")->removeDecorator('HtmlTag');


    $EmergencyOffPhonecountrycode = new Zend_Form_Element_Text('EmergencyOffPhonecountrycode', array(
                'regExp' => "[0-9]+",
                'invalidMessage' => "Only digits"
            ));

    $EmergencyOffPhonecountrycode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $EmergencyOffPhonecountrycode->setAttrib('maxlength', '3');
    $EmergencyOffPhonecountrycode->setAttrib('style', 'width:30px');
    $EmergencyOffPhonecountrycode->removeDecorator("DtDdWrapper");
    $EmergencyOffPhonecountrycode->removeDecorator("Label");
    $EmergencyOffPhonecountrycode->removeDecorator('HtmlTag');

    $EmergencyOffPhonestatecode = new Zend_Form_Element_Text('EmergencyOffPhonestatecode', array(
                'regExp' => "[0-9]+",
                'invalidMessage' => "Only digits"
            ));
    $EmergencyOffPhonestatecode->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $EmergencyOffPhonestatecode->setAttrib('maxlength', '2');
    $EmergencyOffPhonestatecode->setAttrib('style', 'width:30px');
    $EmergencyOffPhonestatecode->removeDecorator("DtDdWrapper");
    $EmergencyOffPhonestatecode->removeDecorator("Label");
    $EmergencyOffPhonestatecode->removeDecorator('HtmlTag');

    $EmergencyOffPhone = new Zend_Form_Element_Text('EmergencyOffPhone', array(
                'regExp' => '[0-9]+'
            ));
    $EmergencyOffPhone->setAttrib('style', 'width:93px');
    $EmergencyOffPhone->setAttrib('maxlength', '9');
    $EmergencyOffPhone->removeDecorator("DtDdWrapper");
    $EmergencyOffPhone->setAttrib('dojoType', "dijit.form.ValidationTextBox");
    $EmergencyOffPhone->removeDecorator("Label");
    $EmergencyOffPhone->removeDecorator('HtmlTag');
		
    $email = new Zend_Form_Element_Text('email',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
	$email->setAttrib('dojoType',"dijit.form.ValidationTextBox");
    $email->setAttrib('required',"true")  			 
        		->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
		$this->addElements(array ($PermAddressDetails,
			$PermCity,
			$PermState,
			$PermCountry,
			$PermZip,
			$CorrsAddressDetails,
			$CorrsCity,
			$CorrsState,
			$CorrsCountry,
			$CorrsZip,
			$email,
			$HomePhonecountrycode,
			$HomePhonestatecode,
			$HomePhone,
			$CellPhonecountrycode,
			$CellPhone,
			$Faxcountrycode,
			$Faxstatecode,
			$Fax,
			$OutcampusAddressDetails,
			$OutcampusCity,
			$OutcampusState,
			$OutcampusCountry,
			$OutcampusZip,
			$SameOutcampusAddr,
			$SameCorrespAddr,$RelationshipType, $RelativeName, $EmergencyAddressDetails, $EmergencyCountry, $EmergencyState,
        $EmergencyCity, $EmergencyZip,$EmergencyHomePhonecountrycode, $EmergencyHomePhonestatecode,
        $EmergencyHomePhone, $EmergencyCellPhone,$EmergencyCellPhonecountrycode,$EmergencyOffPhonecountrycode,
        $EmergencyOffPhonestatecode,$EmergencyOffPhone));
    }
}