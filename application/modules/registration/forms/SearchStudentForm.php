<?php 

class Registration_Form_SearchStudentForm extends Zend_Form
{
		
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
		$this->setAttrib('onsubmit','return validate_form();');
					
	       
		
		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme Name')
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,"-- All --");		
		foreach($programDb->getData() as $program){
			$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
		}
				
		
		//Intake
		$this->addElement('select','IdIntake', array(
			'label'=>$this->getView()->translate('Intake')
		));
		
		$intakeDB = new App_Model_Record_DbTable_Intake();
		
		$this->IdIntake->addMultiOption(null,"-- All --");		
		foreach($intakeDB->fngetlatestintake() as $intake){
			$this->IdIntake->addMultiOption($intake["key"],$intake["value"]);
		}
                
                //student status
                $this->addElement('select','profileStatus', array(
			'label'=>$this->getView()->translate('Student Status')
		));
                
                $defModel = new App_Model_Definitiontype();
                
                $this->profileStatus->addMultiOption(null,"-- All --");		
		foreach($defModel->getDefinationByType(20) as $status){
			$this->profileStatus->addMultiOption($status["idDefinition"], $status["DefinitionDesc"]);
		}
				
		
	    //Type
		$this->addElement('select','tagging_status', array(
			'label'=>$this->getView()->translate('Tagging Status')
		));
				
		$this->tagging_status->addMultiOption(null,"-- All --");		
		$this->tagging_status->addMultiOption(1,"Tagged Student");
		$this->tagging_status->addMultiOption(2,"Not Tagged Student");
                
                //advisor
                $this->addElement('select','advisor', array(
                    'label'=>$this->getView()->translate('Advisor')
		));
                $this->advisor->addMultiOption(null,"-- All --");
                
                $staffDB = new GeneralSetup_Model_DbTable_Staffmaster();
		$staff = $staffDB->getListAcademicStaff();
                
                if ($staff){
                    foreach ($staff as $staffLoop){
                        $this->advisor->addMultiOption($staffLoop['IdStaff'], $staffLoop['FullName']);
                    }
                }
		
		//Applicant Name
		$this->addElement('text','applicant_name', array(
			'label'=>$this->getView()->translate('Student Name')
		));		
			
		//NIM From
		$this->addElement('text','registrationId_from', array(
			'label'=>$this->getView()->translate('Student ID From')
		));
		
		
		//NIM To
		$this->addElement('text','registrationId_to', array(
			'label'=>$this->getView()->translate('Student ID To')
		));
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>