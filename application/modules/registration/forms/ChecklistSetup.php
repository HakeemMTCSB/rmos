<?php 

class Registration_Form_ChecklistSetup extends Zend_Form
{
	protected $id;
	
	public function setId($id){
		$this->id = $id;
	}
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','checklist_setup');
					
	
		//Name
		$this->addElement('text','rcs_name', array(
			'label'=>$this->getView()->translate('Checklist Name'),
			'required'=> true
		));
		
		//Description
		$this->addElement('textarea','rcs_description', array(
				'label'=>$this->getView()->translate('Description')
		));
		
		//status
		$this->addElement('radio', 'rcs_enable', array(
				'label'=>'Enable',
				'multiOptions'=>array(
						'0' => 'Disable',
						'1' => 'Enable',
				),
		));
		$this->rcs_enable->setValue("1");
		
		$this->addElement('hidden','rcs_id');
		$this->rcs_id->setValue($this->intake);
		
		
		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>