<?php 

class Registration_Form_AttendanceSetupForm extends Zend_Form
{
  	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

		$this->addElement('text','att_effective_date', array(
			'label'=>$this->getView()->translate('Effective Date'),
		    'class'=>'datepicker',
			'required'=>true
		));	
		
		
		//Program
		$this->addElement('select','att_program_id', array(
			'label'=>$this->getView()->translate('Programme Name'),
                        'required'=>true,
                        'onchange'=>'getProgramScheme()'
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->att_program_id->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData()  as $program){
			if($this->_locale=='ms_MY'){
				$this->att_program_id->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->att_program_id->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}
			
		
		
		 //Program Scheme
		$this->addElement('multiselect','att_program_scheme', array(
			'label'=>$this->getView()->translate('Programme Scheme'),
		 	'required'=>true,
			'registerInArrayValidator'=>false
		));
		$this->att_program_scheme->setAttrib( 'size',5);
    	$this->att_program_scheme->setAttrib(  'style','width: 250px' );
	
    	//Reg Item
		$this->addElement('multiselect','att_reg_item', array(
			'label'=>$this->getView()->translate('Registration Item'),
                        'required'=>true
		));
				
		$defDB =  new App_Model_General_DbTable_Definationms();		
		foreach($defDB->getDataByType(160)  as $item){
			$this->att_reg_item->addMultiOption($item["idDefinition"],$item["DefinitionDesc"]);
		}
		$this->att_reg_item->setAttrib( 'size',5);
    	$this->att_reg_item->setAttrib(  'style','width: 250px' );

		//Setup Type
		$this->addElement('select','att_type', array(
			'label'=>$this->getView()->translate('Type'),
			'required'=>true
		));

		$this->att_type->addMultiOption('', '-- Select --');
		$this->att_type->addMultiOption(1, 'Attendance Warning');
		$this->att_type->addMultiOption(2, 'Exam Bar');

		//$this->att_type->setAttrib('size', 5);
		//$this->att_type->setAttrib('style', 'width: 250px');
	
    	
		//Attendance
		$this->addElement('text','att_percentage', array(
			'label'=>$this->getView()->translate('Min. Requirement (%)'),
			'required'=>true
		));	
		
				
		//button
		$this->addElement('submit', 'Submit', array(
          'label'=>$this->getView()->translate('Submit'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        
        
        $this->addDisplayGroup(array('Submit'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>