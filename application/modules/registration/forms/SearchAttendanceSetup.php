<?php 

class Registration_Form_SearchAttendanceSetup extends Zend_Form
{
  	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
		
		//Program
		$this->addElement('select','att_program_id', array(
			'label'=>$this->getView()->translate('Programme Name'),
                        'onchange'=>'getProgramScheme()'
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->att_program_id->addMultiOption(null,"-- All --");		
		foreach($programDb->getData()  as $program){
			if($this->_locale=='ms_MY'){
				$this->att_program_id->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ArabicName"]);
			}else{
				$this->att_program_id->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
			}
		}
		
		 //Program Scheme
		$this->addElement('select','att_program_scheme', array(
			'label'=>$this->getView()->translate('Programme Scheme'),
			'registerInArrayValidator'=>false
		));
		$this->att_program_scheme->addMultiOption(null,"-- All --");	
    
    	
    	//Reg Item
		$this->addElement('select','att_reg_item', array(
			'label'=>$this->getView()->translate('Registration Item')
		));
				
		$defDB =  new App_Model_General_DbTable_Definationms();	
		$this->att_reg_item->addMultiOption(null,"-- All --");		
		foreach($defDB->getDataByType(160)  as $item){
			$this->att_reg_item->addMultiOption($item["idDefinition"],$item["DefinitionDesc"]);
		}

		//Setup Type
		$this->addElement('select','att_type', array(
			'label'=>$this->getView()->translate('Type')
		));

		$this->att_type->addMultiOption('', '-- All --');
		$this->att_type->addMultiOption(1, 'Attendance Warning');
		$this->att_type->addMultiOption(2, 'Exam Bar');
    	
    	
		$this->addElement('text','att_date_from', array(
			'label'=>$this->getView()->translate('Date From'),
		    'class'=>'datepicker'
		));	
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
			
	}
	
	
	
}

?>