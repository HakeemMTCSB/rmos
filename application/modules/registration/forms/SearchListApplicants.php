<?php 

class Registration_Form_SearchListApplicants extends Zend_Form
{
		
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
					
	       
		
		
		//Program
		$this->addElement('select','program_code', array(
			'label'=>$this->getView()->translate('Program Name'),
		    'required'=>true
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->program_code->addMultiOption(null,"-- All --");		
		foreach($programDb->getData() as $program){
			$this->program_code->addMultiOption($program["ProgramCode"],$program["ProgramCode"].' - '.$program["ArabicName"]);
		}
				
		
		//Intake
		$this->addElement('select','IdIntake', array(
			'label'=>$this->getView()->translate('Intake'),
		    'required'=>true
		));
		
		$intakeDB = new App_Model_Record_DbTable_Intake();
		
		$this->IdIntake->addMultiOption(null,"-- All --");		
		foreach($intakeDB->fngetlatestintake() as $intake){
			$this->IdIntake->addMultiOption($intake["key"],$intake["value"]);
		}
				
	
		//Applicant Name
		$this->addElement('text','applicant_name', array(
			'label'=>$this->getView()->translate('Applicant Name')
		));
		
		//Applicant Nomor
		$this->addElement('text','applicant_nomor', array(
			'label'=>$this->getView()->translate('Applicant Nomor')
		));
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>