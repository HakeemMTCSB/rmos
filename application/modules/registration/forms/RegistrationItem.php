<?php 

class Registration_Form_RegistrationItem extends Zend_Form
{
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

		//Semester
		$this->addElement('select','semester_id', array(
			'label'=>$this->getView()->translate('Semester'),	
		    'required'=>true,
			'class'=>'reqfield select'
		));
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->semester_id->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemestermasterList() as $semester){
			$this->semester_id->addMultiOption($semester["key"],$semester["value"].' - '.$semester['SemesterMainCode']);
		}
		
		
		//Program
		$this->addElement('select','program_id', array(
			'label'=>$this->getView()->translate('Programme Name'),
		    'required'=>true,
			'onchange' => 'getProgramScheme(this);',
			'class'=>'reqfield select'
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->program_id->addMultiOption(null,"-- Please Select --");		
		foreach($programDb->getData() as $program){
				$this->program_id->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
		}
		
		//ProgramScheme
		$this->addElement('select','program_scheme_id', array(
			'label'=>$this->getView()->translate('Programme Scheme'),
		    'required'=>true,
			'class'=>'reqfield select'
		));

		//Item
		$this->addElement('select','item_id', array(
			'label'=>$this->getView()->translate('Item'),
		    'required'=>true,
			'class'=>'reqfield select'
		));
		
		$definationDb = new App_Model_General_DbTable_Definationms();
		
		$this->item_id->addMultiOption(null,'Please Select');
		foreach ($definationDb->getByCode('Registration Item') as $list){
			$this->item_id->addMultiOption($list['idDefinition'],$list['BahasaIndonesia']);
		}

		//chargable
		$this->addElement('checkbox','chargable', array(
			'label'=>$this->getView()->translate('Chargeable?')
		));
		
		//mandatory
		$this->addElement('checkbox','mandatory', array(
			'label'=>$this->getView()->translate('Mandatory?')
		));
		
		
		//button
		$this->addElement('submit', 'Submit', array(
          'label'=>$this->getView()->translate('Submit'),
          'decorators'=>array('ViewHelper')
        ));
        
        //button
		$this->addElement('reset', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Submit','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>