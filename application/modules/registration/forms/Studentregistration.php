<?php
class Registration_Form_Studentregistration extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate');

		$IdStudentRegistration  = new Zend_Form_Element_Hidden('IdStudentRegistration');
        $IdStudentRegistration->removeDecorator("DtDdWrapper");
        $IdStudentRegistration->setAttrib('dojoType',"dijit.form.TextBox");
        $IdStudentRegistration->removeDecorator("Label");
        $IdStudentRegistration->removeDecorator('HtmlTag');

        $IdStudentRegistration  = new Zend_Form_Element_Hidden('IdStudentApplication');
        $IdStudentRegistration->removeDecorator("DtDdWrapper");
        $IdStudentRegistration->setAttrib('dojoType',"dijit.form.TextBox");
        $IdStudentRegistration->removeDecorator("Label");
        $IdStudentRegistration->removeDecorator('HtmlTag');

		$IdLandscape = new Zend_Dojo_Form_Element_FilteringSelect('IdLandscape');
        $IdLandscape->removeDecorator("DtDdWrapper");
        $IdLandscape->setAttrib('required',"true") ;
        $IdLandscape->removeDecorator("Label");
        $IdLandscape->removeDecorator('HtmlTag');
        $IdLandscape->setRegisterInArrayValidator(false);
        $IdLandscape->setAttrib('OnChange', "fnGetSyllabusList(this.value)");
		$IdLandscape->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$IdIntake = new Zend_Dojo_Form_Element_FilteringSelect('IdIntake');
        $IdIntake->removeDecorator("DtDdWrapper");
        $IdIntake->setAttrib('required',"true") ;
        $IdIntake->removeDecorator("Label");
        $IdIntake->removeDecorator('HtmlTag');
        $IdIntake->setRegisterInArrayValidator(false);
       // $IdIntake->setAttrib('OnChange', "fnGetSyllabusList(this)");
		$IdIntake->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$IdProgram  = new Zend_Form_Element_Hidden('IdProgram');
        $IdProgram->removeDecorator("DtDdWrapper");
        $IdProgram->setAttrib('dojoType',"dijit.form.TextBox");
        $IdProgram->removeDecorator("Label");
        $IdProgram->removeDecorator('HtmlTag');

		$IdLandscapeType  = new Zend_Form_Element_Hidden('IdLandscapeType');
        $IdLandscapeType->removeDecorator("DtDdWrapper");
        $IdLandscapeType->setAttrib('dojoType',"dijit.form.TextBox");
        $IdLandscapeType->removeDecorator("Label");
        $IdLandscapeType->removeDecorator('HtmlTag');

		$IdSemestersyllabus = new Zend_Dojo_Form_Element_FilteringSelect('IdSemestersyllabus');
		$IdSemestersyllabus->setAttrib('dojoType',"dijit.form.FilteringSelect");
        //$IdSemestersyllabus->setAttrib('OnChange', "fnGetSubjectList(this)");
        $IdSemestersyllabus->setRegisterInArrayValidator(false);
        $IdSemestersyllabus->removeDecorator("DtDdWrapper");
        $IdSemestersyllabus->setAttrib('required',"true") ;
        $IdSemestersyllabus->removeDecorator("Label");
        $IdSemestersyllabus->removeDecorator('HtmlTag');

        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');

        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
       	$Save->label = $gstrtranslate->_("Register");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";


        $Revert = new Zend_Form_Element_Submit('Revert');
       	$Revert->label = $gstrtranslate->_("Revert");
        $Revert->dojotype="dijit.form.Button";
        $Revert->removeDecorator("DtDdWrapper");
        $Revert->removeDecorator('HtmlTag')
         		->class = "NormalBtn";


   		$registrationId = new Zend_Form_Element_Text('registrationId');
		$registrationId->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$registrationId  //->setAttrib('required',"true")
        				->setAttrib('maxlength','75')
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');


        //form elements
        $this->addElements(array($IdStudentRegistration,$IdSemestersyllabus,$UpdDate,$UpdUser,$Save,$Revert,$IdProgram,$IdLandscape,$IdIntake,$IdLandscapeType,$registrationId));

    }
}