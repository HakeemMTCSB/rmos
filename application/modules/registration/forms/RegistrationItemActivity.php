<?php 

class Registration_Form_RegistrationItemActivity extends Zend_Form
{
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

		//activity
		$this->addElement('select','ria_activity_id', array(
			'label'=>$this->getView()->translate('Activity'),	
		    'required'=>true,
			'class'=>'reqfield select'
		));
		
		$activityDB = new GeneralSetup_Model_DbTable_Activity();
		
		$this->ria_activity_id->addMultiOption(null,"-- Please Select --");		
		foreach($activityDB->fngetActivityList() as $semester){
			$this->ria_activity_id->addMultiOption($semester["key"],$semester["value"]);
		}
		
		//calculation type
		$this->addElement('select','ria_calculation_type', array(
			'label'=>$this->getView()->translate('Calculation Type'),
		    'required'=>true,
			'class'=>'reqfield select'
		));
		
		$this->ria_calculation_type->addMultiOption(0,'Percentage');
		$this->ria_calculation_type->addMultiOption(1,'Amount');
		
		$this->addElement('text','ria_amount', array(
			'label'=>$this->getView()->translate('Amount'),
			'required'=> true
		));
		
		//button
		$this->addElement('submit', 'Submit', array(
          'label'=>$this->getView()->translate('Submit'),
          'decorators'=>array('ViewHelper')
        ));
        
        //button
		$this->addElement('reset', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Submit','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>