<?php
class Registration_Form_ItemRegistrationSearchForm extends Zend_Dojo_Form {
    
    public function init() {
        $this->setMethod('post');
        
        $model = new Registration_Model_DbTable_Report();
        
        //name
        $name = new Zend_Form_Element_Text('name');
	$name->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //id
        $id = new Zend_Form_Element_Text('id');
	$id->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //program
        $program = new Zend_Form_Element_Select('program');
	$program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('onchange', 'get_programscheme();');
	$program->removeDecorator("Label");
        
        $progList = $model->getProgramList();
        
        $program->addMultiOption('', '--Select--');
        
        if ($progList){
            foreach ($progList as $progLoop){
                $program->addMultiOption($progLoop['IdProgram'], $progLoop['ProgramName']);
            }
        }
        
        //program scheme
        $programscheme = new Zend_Form_Element_Select('programscheme');
	$programscheme->removeDecorator("DtDdWrapper");
        $programscheme->setAttrib('class', 'select');
	$programscheme->removeDecorator("Label");
        
        $programscheme->addMultiOption('', '--Select--');
        
        //intake
        $intake = new Zend_Form_Element_Select('intake');
	$intake->removeDecorator("DtDdWrapper");
        $intake->setAttrib('class', 'select');
	$intake->removeDecorator("Label");
        
        $intake->addMultiOption('', '--Select--');
        
        $intakeModel = new GeneralSetup_Model_DbTable_Intake();
        $intakeList = $intakeModel->fngetIntakeList();
        
        if ($intakeList){
            foreach ($intakeList as $intakeLoop){
                $intake->addMultiOption($intakeLoop['IdIntake'], $intakeLoop['IntakeDesc']);
            }
        }
        
        $status = new Zend_Form_Element_Select('status');
	$status->removeDecorator("DtDdWrapper");
        $status->setAttrib('class', 'select');
	$status->removeDecorator("Label");
        
        $definationDB = new App_Model_General_DbTable_Definationms();
        $statusList = $definationDB->getDataByType(20);
        
        $status->addMultiOption('', '--Select--');
        
        if ($statusList){
            foreach($statusList as $statusLoop){
                $status->addMultiOption($statusLoop['idDefinition'], $statusLoop['DefinitionDesc']);
            }
        }
        
        //form elements
        $this->addElements(array(
            $name,
            $id,
            $program,
            $programscheme,
            $intake,
            $status
        ));
    }
}