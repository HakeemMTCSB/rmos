<?php
class Registration_Form_Credittransfer extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$IdApplication  = new Zend_Form_Element_Hidden('IdApplication');
        $IdApplication->removeDecorator("DtDdWrapper");
        $IdApplication->setAttrib('dojoType',"dijit.form.TextBox");
        $IdApplication->removeDecorator("Label");
        $IdApplication->removeDecorator('HtmlTag');
        
		$IdProgram  = new Zend_Form_Element_Hidden('IdProgram');
        $IdProgram->removeDecorator("DtDdWrapper");
        $IdProgram->setAttrib('dojoType',"dijit.form.TextBox");
        $IdProgram->removeDecorator("Label");
        $IdProgram->removeDecorator('HtmlTag');
        
		$IdSemester  = new Zend_Form_Element_Hidden('IdSemester');
        $IdSemester->removeDecorator("DtDdWrapper");
        $IdSemester->setAttrib('dojoType',"dijit.form.TextBox");
        $IdSemester->removeDecorator("Label");
        $IdSemester->removeDecorator('HtmlTag');
        
        $Comments  = new Zend_Form_Element_Hidden('Comments');
        $Comments->removeDecorator("DtDdWrapper");
        $Comments->setAttrib('dojoType',"dijit.form.TextBox");
        $Comments->removeDecorator("Label");
        $Comments->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
	    $landscape = new Zend_Dojo_Form_Element_FilteringSelect('landscape');	
        $landscape->setAttrib('required',"true");
        $landscape->removeDecorator("DtDdWrapper");
        $landscape->setAttrib('OnChange', 'fnGetSubjectList(this.value)');
        $landscape->removeDecorator("Label");
        $landscape->removeDecorator('HtmlTag');
	    $landscape->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    
	    $select = new Zend_Form_Element_Checkbox('select');
		$select	->setChecked(true)
					->removeDecorator("DtDdWrapper")
					->setAttrib('dojoType',"dijit.form.CheckBox")
					->removeDecorator("Label") 					
					->removeDecorator('HtmlTag');
   
        $Save = new Zend_Form_Element_Submit('Save');
       	$Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
         		
        
        //form elements
        $this->addElements(array($Save,$IdApplication,$IdProgram,$IdSemester,$UpdDate,$UpdUser,$landscape,$Comments,$select));

    }
}