<?php 

class Registration_Form_SearchApplicants extends Zend_Form
{
		
		
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

		//Intake
		$this->addElement('select','IdIntake', array(
			'label'=>$this->getView()->translate('Intake'),
			'required'=>false,		  
            'registerInArrayValidator' => false
		));
		
		$intakeDB = new App_Model_Record_DbTable_Intake();
		
		$this->IdIntake->addMultiOption(null,$this->getView()->translate('-- All --'));		
		foreach($intakeDB->fngetlatestintake() as $intake){
			$this->IdIntake->addMultiOption($intake["key"],$intake["value"]);
		}
		
		
		//Program
		$this->addElement('select','IdProgram', array(
			'label'=>$this->getView()->translate('Programme Name'),		   
			'onchange'=>'getProgramScheme(this);',
                    'registerInArrayValidator' => false
		));
		
		$programDb = new Registration_Model_DbTable_Program();
		
		$this->IdProgram->addMultiOption(null,$this->getView()->translate('-- All --'));		
		foreach($programDb->getData()  as $program){
			$this->IdProgram->addMultiOption($program["IdProgram"],$program["ProgramCode"].' - '.$program["ProgramName"]);
		}
		
	    //Program Scheme
		$this->addElement('select','IdProgramScheme', array(
			'label'=>$this->getView()->translate('Programme Scheme'),
                    'registerInArrayValidator' => false
		));
		
		$this->IdProgramScheme->addMultiOption(null,$this->getView()->translate('-- All --'));			
		
		

		
		//Branch
		$this->addElement('select','IdBranch', array(
			'label'=>$this->getView()->translate('Branch'),		    
                    'registerInArrayValidator' => false
		));
		
		$branchDb = new GeneralSetup_Model_DbTable_Branchofficevenue();
				
		$this->IdBranch->addMultiOption(null,$this->getView()->translate('-- All --'));		
		foreach($branchDb->fnGetAllBranchList() as $branch){
			$this->IdBranch->addMultiOption($branch["key"],$branch["value"]);
		}
	
		 //Payment Status
		 /*
				 *  1=no outstanding
					2=outstanding
					3=no invoice
				 */
		$this->addElement('select','payment_status', array(
			'label'=>$this->getView()->translate('Payment Status')
		));
		$this->payment_status->addMultiOption(null,$this->getView()->translate('-- All --'));
		$this->payment_status->addMultiOption(1,$this->getView()->translate('Paid'));
		$this->payment_status->addMultiOption(2,$this->getView()->translate('Pending'));
		$this->payment_status->addMultiOption(3,$this->getView()->translate('No Invoice'));
		
		
		//Applicant Name
		$this->addElement('text','applicant_name', array(
			'label'=>$this->getView()->translate('Applicant Name')
		));
		
		//Applicant ID
		$this->addElement('text','applicant_nomor', array(
			'label'=>$this->getView()->translate('Applicant ID')
		));
		
		 
		 //Sorting
		$this->addElement('select','Sorting', array(
			'label'=>$this->getView()->translate('Sort by')
		));
		
		$this->Sorting->addMultiOption(0,$this->getView()->translate('Application Date'));
		$this->Sorting->addMultiOption(1,$this->getView()->translate('Applicant Name'));
		$this->Sorting->addMultiOption(2,$this->getView()->translate('Applicant ID'));
		$this->Sorting->addMultiOption(3,$this->getView()->translate('Programme'));			
		
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper'),
		  'class'=>'btn-submit'
        ));
        
        //button
		$this->addElement('submit', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
          'decorators'=>array('ViewHelper'),
		 'class'=>'btn-submit'
        ));

		
        $this->addDisplayGroup(array('Search', 'Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>