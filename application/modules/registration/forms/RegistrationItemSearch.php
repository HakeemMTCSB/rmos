<?php
class Registration_Form_RegistrationItemSearch extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    
    public function init() {
    	$this->setMethod('post');
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        $programId = $this->getAttrib('programid');
        
        $model = new Registration_Model_DbTable_Studentregistration();
        $model2 = new Records_Model_DbTable_ChangeMode();
        
        //programme
        $program = new Zend_Form_Element_Select('program');
	$program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('onchange', 'getProgramScheme(this.value); getSemester(this.value);');
	$program->removeDecorator("Label");
        
        $program->addMultiOption('', '-- Select --');
        
        $ProgramList = $model->getProgram();
        
        if ($ProgramList){
            foreach ($ProgramList as $ProgramLoop){
                $program->addMultiOption($ProgramLoop['IdProgram'], $ProgramLoop['ProgramName']);
            }
        }
        
        //program scheme
        $programscheme = new Zend_Form_Element_Select('programscheme');
	$programscheme->removeDecorator("DtDdWrapper");
        $programscheme->setAttrib('class', 'select');
        //$programscheme->setAttrib('onchange', 'getCourseToTransfer();');
	$programscheme->removeDecorator("Label");
        
        $programscheme->addMultiOption('', '-- Select --');
        
        //semester
        $semester = new Zend_Form_Element_Select('semester');
	$semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('class', 'select');
        //$programscheme->setAttrib('onchange', 'getCourseToTransfer();');
	$semester->removeDecorator("Label");
        
        $semester->addMultiOption('', '-- Select --');
        
        if ($programId){
            $programSchemeList = $model2->getProgramSchemeBasedOnProgram($programId);
            
            if ($programSchemeList){
                foreach ($programSchemeList as $programSchemeLoop){
                    $programscheme->addMultiOption($programSchemeLoop['IdProgramScheme'], $programSchemeLoop['mop_name'].' '.$programSchemeLoop['mos_name'].' '.$programSchemeLoop['programType']);
                }
            }
            
            $programInfo = $model->getProgramForSem($programId);
            $semList = $model->getSemBasedOnList($programInfo['IdScheme']);
            
            if ($semList){
                foreach ($semList as $semLoop){
                    $semester->addMultiOption($semLoop['IdSemesterMaster'], $semLoop['SemesterMainName'].' - '.$semLoop['SemesterMainCode']);
                }
            }
        }
        
        //item
        $item = new Zend_Form_Element_Select('item');
	$item->removeDecorator("DtDdWrapper");
        $item->setAttrib('class', 'select');
        //$programscheme->setAttrib('onchange', 'getCourseToTransfer();');
	$item->removeDecorator("Label");
        
        $item->addMultiOption('', '-- Select --');
        
        $itemList = $model->getDefination(160);
        
        if ($itemList){
            foreach ($itemList as $itemLoop){
                $item->addMultiOption($itemLoop['idDefinition'], $itemLoop['DefinitionDesc']);
            }
        }
        
        $this->addElements(array(
            $program,
            $programscheme,
            $semester,
            $item
        ));
    }
}