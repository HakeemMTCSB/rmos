<?php
class Registration_Form_Registrationconfiguration extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate');


		$registrationType = new Zend_Dojo_Form_Element_FilteringSelect('registrationType');
        $registrationType->addMultiOptions(array('1' => 'Senior Student',
									   '2' => 'New Student'));
        $registrationType->setAttrib('required',"true");
        $registrationType->setAttrib('onChange', 'selecttype(this.value)');
        $registrationType->removeDecorator("DtDdWrapper");
        $registrationType->removeDecorator("Label");
        $registrationType->removeDecorator('HtmlTag');
		$registrationType->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$minFinanceBalanceSenior  = new Zend_Form_Element_Checkbox('minFinanceBalanceSenior');
        $minFinanceBalanceSenior->setAttrib('dojoType',"dijit.form.CheckBox");
        $minFinanceBalanceSenior->setvalue('1');
        $minFinanceBalanceSenior->removeDecorator("DtDdWrapper");
        $minFinanceBalanceSenior->removeDecorator("Label");
        $minFinanceBalanceSenior->removeDecorator('HtmlTag');

        $minFinanceBalanceNew  = new Zend_Form_Element_Checkbox('minFinanceBalanceNew');
        $minFinanceBalanceNew->setAttrib('dojoType',"dijit.form.CheckBox");
        $minFinanceBalanceNew->setvalue('1');
        $minFinanceBalanceNew->removeDecorator("DtDdWrapper");
        $minFinanceBalanceNew->removeDecorator("Label");
        $minFinanceBalanceNew->removeDecorator('HtmlTag');


        $academicLandscapeReqt = new Zend_Form_Element_Checkbox('academicLandscapeReqt');
        $academicLandscapeReqt->setAttrib('dojoType',"dijit.form.CheckBox");
        $academicLandscapeReqt->setvalue('1');
        $academicLandscapeReqt->removeDecorator("DtDdWrapper");
        $academicLandscapeReqt->removeDecorator("Label");
        $academicLandscapeReqt->removeDecorator('HtmlTag');

        $minCgpa = new Zend_Form_Element_Checkbox('minCgpa');
        $minCgpa->setAttrib('dojoType',"dijit.form.CheckBox");
        $minCgpa->setvalue('1');
        $minCgpa->removeDecorator("DtDdWrapper");
        $minCgpa->removeDecorator("Label");
        $minCgpa->removeDecorator('HtmlTag');

        $studentDisciplinary = new Zend_Form_Element_Checkbox('studentDisciplinary');
        $studentDisciplinary->setAttrib('dojoType',"dijit.form.CheckBox");
        $studentDisciplinary->setvalue('1');
        $studentDisciplinary->removeDecorator("DtDdWrapper");
        $studentDisciplinary->removeDecorator("Label");
        $studentDisciplinary->removeDecorator('HtmlTag');


        $Save = new Zend_Form_Element_Submit('Save');
       	$Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";


        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->label = $gstrtranslate->_("Clear");
        $Clear->dojotype="dijit.form.Button";
        $Clear->removeDecorator("DtDdWrapper");
        $Clear->removeDecorator('HtmlTag')
         		->class = "NormalBtn";

        //form elements
        $this->addElements(array($registrationType,$minFinanceBalanceSenior,$minFinanceBalanceNew,$academicLandscapeReqt,$minCgpa,$studentDisciplinary,$Clear,$Save));

    }
}