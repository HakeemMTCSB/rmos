<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 1/12/2015
 * Time: 10:54 AM
 */
class Registration_Form_ForecastReportSearch extends Zend_Dojo_Form {

    public function init(){
        $this->setMethod('post');

        $programid = $this->getAttrib('programid');

        $model = new Registration_Model_DbTable_ForecastReport();

        //programme
        $program = new Zend_Form_Element_Select('program');
        $program->removeDecorator("DtDdWrapper");
        $program->setAttrib('class', 'select');
        $program->setAttrib('required', '');
        $program->setAttrib('onchange', 'getProgramScheme(this.value), getSemester(this.value)');
        $program->removeDecorator("Label");

        $program->addMultiOption('', '-- Select --');

        $programList = $model->getProgram();

        if ($programList){
            foreach ($programList as $programLoop){
                $program->addMultiOption($programLoop['IdProgram'], $programLoop['ProgramName']);
            }
        }

        //semester
        $semester = new Zend_Form_Element_Select('semester');
        $semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('required', '');
        $semester->setAttrib('class', 'select');
        $semester->removeDecorator("Label");

        $semester->addMultiOption('', '-- Select --');

        //programme scheme
        $programscheme = new Zend_Form_Element_Select('programscheme');
        $programscheme->removeDecorator("DtDdWrapper");
        $programscheme->setAttrib('required', '');
        $programscheme->setAttrib('multiple', 'true');
        $programscheme->setAttrib('class', 'select');
        $programscheme->removeDecorator("Label");

        //$programscheme->addMultiOption('', '-- Select --');

        if ($programid){
            $programInfo = $model->getProgramById($programid);
            $semesterList = $model->getSemester($programInfo['IdScheme']);

            if ($semesterList){
                foreach ($semesterList as $semesterLoop){
                    $semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName']);
                }
            }

            $programSchemeList = $model->getProgramScheme($programid);

            if ($programSchemeList){
                foreach ($programSchemeList as $programSchemeLoop){
                    $programscheme->addMultiOption($programSchemeLoop['IdProgramScheme'], $programSchemeLoop['mop'].' '.$programSchemeLoop['mos'].' '.$programSchemeLoop['pt']);
                }
            }
        }

        //form elements
        $this->addElements(array(
            $program,
            $semester,
            $programscheme
        ));
    }
}