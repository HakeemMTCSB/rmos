<?php 

class Registration_Form_AddRegDateForm extends Zend_Form
{
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');

			//Intake
		$this->addElement('select','rds_intake', array(
			'label'=>$this->getView()->translate('Intake'),
		    'required'=>false
		));
		
		$intakeDB = new App_Model_Record_DbTable_Intake();
		
		$this->rds_intake->addMultiOption(null,"-- All --");		
		foreach($intakeDB->fngetlatestintake() as $intake){
			$this->rds_intake->addMultiOption($intake["key"],$intake["value"]);
		}
				
	
		//Registration Date
		$this->addElement('text','rds_date', array(
			'label'=>$this->getView()->translate('Registration Date'),
		    'required'=>true
		));
		
		//Start Time
		$this->addElement('text','rds_start_time', array(
			'label'=>$this->getView()->translate('Start Time'),
		    'required'=>true
		));
		
		//End Time
		$this->addElement('text','rds_end_time', array(
			'label'=>$this->getView()->translate('End Time'),
		    'required'=>true
		));
		
		//Capacity
		$this->addElement('text','rds_capacity', array(
			'label'=>$this->getView()->translate('Capacity'),
		    'required'=>true,
		    'validators'=>array('Digits')	    
		));
						
		//button
		$this->addElement('submit', 'Save', array(
          'label'=>$this->getView()->translate('Save'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Save'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}

?>