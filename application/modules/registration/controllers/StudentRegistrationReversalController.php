<?php

class Registration_StudentRegistrationReversalController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
        $this->view->title = $this->view->translate("NIM Reversal");
        
    	$msg = $this->_getParam('msg', null);
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Data has been saved successfully");
		}
        
        $form = new Registration_Form_SearchListStudentForm();
		$this->view->form = $form;

		 if($form->isValid($_POST)) {
			if ($this->_request->isPost()){
				
				$formData = $this->_request->getPost ();
				
					
				$form->populate($formData);
				$this->view->form = $form;
				
				//get list of registered student
				$studentDB = new Registration_Model_DbTable_Studentregistration();
				$student_list = $studentDB->getListStudent($formData);
				$this->view->student_list = $student_list;
				
				
			}
		}
    }
    
    
    
    public function reversalAction(){
		
		$auth = Zend_Auth::getInstance();
		
		$stack_failed = array();
		
					
		if ($this->_request->isPost()){
			
			    $formData =  $this->_request->getPost();
			   
			     for($i=0; $i<count($formData["IdStudentRegistration"]);  $i++){					
				 	    
			     
						$IdStudentRegistration = $formData["IdStudentRegistration"][$i];
						
						$appl_id = $formData["appl_id"][$IdStudentRegistration];
					
						$transaction_id  = $formData["txn_id"][$IdStudentRegistration];
						
						
						
						
						/* -------------------
						 *  START REVERSAL 
						 * -------------------*/
						
						
						//tbl_studentregistration
						$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
						$studentRegistrationDb->deleteData($IdStudentRegistration);
						
						//applicant_profile
						$profileDB = new Application_Model_DbTable_ApplicantProfile();				        
				        $profileDB->updateData(array('appl_role' => 0),$appl_id);
				       
						//applicant_transaction
						$transDB = new App_Model_Application_DbTable_ApplicantTransaction();
						$updtrans = array('at_IdStudentRegistration' => '', 
				       					  'at_registration_status' => 0, 				       						
				       					  'at_registration_date'=>'');				        
				        $transDB->updateData($updtrans,$transaction_id);
				       
						//tbl_student_status_history
						$lobjstudentHistoryModel = new Registration_Model_DbTable_Studenthistory();
				        $lobjstudentHistoryModel->deleteData($IdStudentRegistration);	
				        
				        //student_profile				     
				        //kene checking jika student ada register lebih dari satu jgn delete
				        $total = $studentRegistrationDb->getTotalRegistration($appl_id);
				          
				        if($total == 0){
					        $studentDB = new App_Model_Student_DbTable_StudentProfile();
					        $studentDB->deleteStudentData($appl_id);
				        }
				        
				        
						//sequence no - lantak						
				        
				        
				        /* jika ada buat course registration */				        
				        
				        //tbl_studentregsubjects
				        $studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
						$studentRegSubjectDB->deleteRegisterSubjects($IdStudentRegistration);
				        
				        //student_semester_status
				        $studentSemesterStatusDb =  new Registration_Model_DbTable_Studentsemesterstatus();						
						$studentSemesterStatusDb->deleteRegisterData($IdStudentRegistration);
						
				        /* -------------------
						 *  END REVERSAL 
						 * -------------------*/
						
						
			     }
			     
		}
	
		$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'student-registration-reversal', 'action'=>'index','msg'=>1),'default',true));
		
    }
    
      

}

?>