<?php
class Registration_RegistrationhistoryController extends Base_Base {
	
	public function indexAction(){
		
		$this->view->title = $this->view->translate("Student Registration History");
		
		$form = new Registration_Form_SearchStudent();
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();

			$formData['IdProgram'] = $this->view->escape(strip_tags($formData['IdProgram']));
			$formData['IdProgramScheme'] = $this->view->escape(strip_tags($formData['IdProgramScheme']));
			$formData['IdIntake'] = $this->view->escape(strip_tags($formData['IdIntake']));
			$formData['IdBranch'] = $this->view->escape(strip_tags($formData['IdBranch']));
			$formData['applicant_name'] = $this->view->escape(strip_tags($formData['applicant_name']));
			$formData['student_id'] = $this->view->escape(strip_tags($formData['student_id']));
			$formData['profile_status'] = $this->view->escape(strip_tags($formData['profile_status']));
			$formData['semester_status'] = $this->view->escape(strip_tags($formData['semester_status']));

			$studentRegDB = new Registration_Model_DbTable_Studentregistration();
		    $student_list = $studentRegDB->getListStudent($formData);
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(1000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$form->populate($formData);
			
			
		}else{			 
		    
		    $studentRegDB = new Registration_Model_DbTable_Studentregistration();
		    $student_list = $studentRegDB->getListStudent();
		    
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(1000);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
		}
				
		$this->view->paginator = $paginator;
	}

   
	
	public function studenthistorydetailsAction() {
				
		$this->view->title = $this->view->translate("Student Registration History");
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
		
		$registrationId = $this->_getParam('id');
		$this->view->registrationId = $registrationId;
			
		//get student info
		$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
		$studentdetails = $studentRegistrationDB->fetchStudentHistoryDetails($registrationId);
 		$this->view->studentdetails = $studentdetails;
		
 		
		/*// display name for sysmester syllabus chosen
        $IdSemesterMain = $studentdetails['IdSemesterMain'];
        $IdSemesterDetails= $studentdetails['IdSemesterDetails'];
        
        if($IdSemesterMain!='' && $IdSemesterMain!=0) {
             $semMasterObj = new GeneralSetup_Model_DbTable_Semestermaster();
             $semResultData = $semMasterObj->fngetSemestermainDetails($IdSemesterMain);
        	 $this->view->semesterNameData = $semResultData[0]['SemesterMainName'];
        }

        if($IdSemesterDetails!='' && $IdSemesterDetails !=0) {
        	 $semDetailObj = new GeneralSetup_Model_DbTable_Semester();
             $semResultData = $semDetailObj->fngetsemdetailByid($IdSemesterDetails);
        	 $this->view->semesterNameData = $semResultData[0]['SemesterCode'];
         }*/
          
		
         
         //cek landscape
         $landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
         $landscape = $landscapeDb->getData($studentdetails["IdLandscape"]);
         $this->view->landscape = $landscape;
        
         if($landscape["LandscapeType"]==43 || $landscape["LandscapeType"]==42) {//Semester Based      & Level    	
         	
         	//get total registered semester 
         	$studentSemesterDB = new Registration_Model_DbTable_Studentsemesterstatus();
         	$semester = $studentSemesterDB->getRegisteredSemester($registrationId);
         	$this->view->semester = $semester;
         	
         	//get subjects
  		
         }elseif($landscape["LandscapeType"]==44){
         	
         	//get registered blocks
         	$studentSemesterDB = new Registration_Model_DbTable_Studentsemesterstatus();
         	$blocks = $studentSemesterDB->getRegisteredBlock($registrationId);
         	$this->view->blocks = $blocks;
         }
         
        
  		
  		// GET PROFILE STATUS HISTORY  		
        $this->view->studentProfileHistory = $studentRegistrationDB->fetchStudentProfileHistory($registrationId);
                        
        // GET SEMESTER HISTORY
         $this->view->studentSemesterHistory = $studentRegistrationDB->fetchStudentSemesterHistory($registrationId);         
        
        
	}
	
	public function burekolVerificationAction(){
		
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->_helper->layout->disableLayout();
		}
		
		$registrationId = $this->_getParam('id');
		$from = $this->_getParam('from');
		$this->view->registrationId = $registrationId;
		$this->view->from = $from;
		
		if ($this->getRequest()->isPost()) {
			$auth = Zend_Auth::getInstance();
			
			$formData = $this->getRequest()->getPost();
						
			//profile
			$studentProfileDb = new Records_Model_DbTable_Studentprofile();
			$data = array(
					'appl_fname' => $formData['appl_fname'],
					'appl_mname' => $formData['appl_mname'],
					'appl_lname' => $formData['appl_lname'],
					'appl_name_kartu' => $formData['appl_name_kartu'],
					'appl_dob' => $formData['appl_dob'],
					'appl_birth_place' => $formData['appl_birth_place'],
					'appl_gender' => $formData['appl_gender'],
					'appl_religion' => $formData['appl_religion'],
					'appl_marital_status' => $formData['appl_marital_status'],
					'appl_nationality' => $formData['appl_nationality'],
					'appl_address_rw' => $formData['appl_address_rw'],
					'appl_address_rt' => $formData['appl_address_rt'],
					'appl_address1' => $formData['appl_address1'],
					'appl_province' => $formData['appl_province'],
					'appl_state' => $formData['appl_state'],
					'appl_city' => $formData['appl_city'],
					'appl_kecamatan' => $formData['appl_kecamatan'],
					'appl_kelurahan' => $formData['appl_kelurahan'],
					'appl_postcode' => $formData['appl_postcode'],
					'appl_caddress_rw' => $formData['appl_caddress_rw'],
					'appl_caddress_rt' => $formData['appl_caddress_rt'],
					'appl_caddress1' => $formData['appl_caddress1'],
					'appl_cprovince' => $formData['appl_cprovince'],
					'appl_cstate' => $formData['appl_cstate'],
					'appl_ccity' => $formData['appl_ccity'],
					'appl_ckecamatan' => $formData['appl_ckecamatan'],
					'appl_ckelurahan' => $formData['appl_ckelurahan'],
					'appl_cpostcode' => $formData['appl_cpostcode'],
					'appl_phone_home' => $formData['appl_phone_home'],
					'appl_phone_mobile' => $formData['appl_phone_mobile'],
					'appl_email' => $formData['appl_email'],
					'burekol_verify_by' => $auth->getIdentity()->id,
					'burekol_verify_date' =>date('Y-m-d H:i:s')
					);
			
			$studentProfileDb->update($data,'id = '.$formData['profile_id']);
			
			//mother's name
			
			$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
			$studentdetails = $studentRegistrationDB->fetchStudentHistoryDetails($formData['registration_id']);
			
			$familyDb = new App_Model_Application_DbTable_ApplicantFamily();
			$mother = $familyDb->getData($studentdetails['IdApplication'], 21);
			
			if($mother){
				$data = array(
							'af_name' => $formData['af_name']
						);
				
				$familyDb->update($data,'af_appl_id = '.$studentdetails['IdApplication'].' and af_relation_type = 21');
			}else{
				$data = array(
						'af_name' => $formData['af_name'],
						'af_appl_id' => $studentdetails['IdApplication'],
						'af_relation_type' => 21,
						'af_address1' => $formData['appl_address1'],
						'af_address2' => '',
						'af_city' => $formData['appl_city'],
						'af_postcode' => $formData['appl_postcode'],
						'af_state' => $formData['appl_state'],
						'af_phone' => '',
						'af_email' => '',
						'af_job' => ''
				);
				
				$familyDb->insert($data);
			}
			
			
			//registration checklist
			$checklistStudentDb = new Registration_Model_DbTable_ChecklistStudent();
			$checklist = $checklistStudentDb->getChecklistStudentData($formData['registration_id']);
			
			foreach ($checklist as $list){
				
				if( in_array($list['rcs_id'], $formData['checklist']) ){
					if(!isset($list['rcstd_id'])){
						//insert checklist
						$data_ckl = array(
								'rcstd_idStudentRegistration' => $formData['registration_id'],
								'rcstd_rcs_id' => $list['rcs_id'],
								'rcstd_status' => 1
						);
						$checklistStudentDb->insert($data_ckl);
					}else{
						//update checklist
						if($list['rcstd_status']==0){
							$data_ckl = array(
									'rcstd_status' => 1
							);
							$checklistStudentDb->update($data_ckl,'rcstd_id='.$list['rcstd_id']);
						}
					}
				}else{
					if(isset($list['rcstd_id'])){
						//update disabled
						$data_ckl = array(
								'rcstd_status' => 0
						);
						$checklistStudentDb->update($data_ckl,'rcstd_id='.$list['rcstd_id']);
					}else{
						//ignore
					}
				}
			}
			
			
			
			
			if($from=="records"){
				$this->_redirect( $this->baseUrl . '/records/studentprofile/studentprofilelist/id/'.$formData['registration_id'].'/#Tab6');
			}else{
				$this->_redirect( $this->baseUrl . '/registration/registrationhistory/studenthistorydetails/id/'.$formData['registration_id'].'/#Tab6');
			}
		}
		
		//lookup table list
		$sisSetupDetailDb = new App_Model_General_DbTable_SisSetupDetail();
		$religionList = $sisSetupDetailDb->getDataList("RELIGION");
		$this->view->religionList = $religionList;
		
		$maritalList = $sisSetupDetailDb->getDataList("MARITAL");
		$this->view->maritalList = $maritalList;
		
		$cityDb = new App_Model_General_DbTable_City();
		$cityList = $cityDb->getData();
		$this->view->cityList = $cityList;
		
		$stateDb = new App_Model_General_DbTable_State();
		$stateList = $stateDb->getData();
		$this->view->stateList = $stateList;
		
		$countryDb = new App_Model_General_DbTable_Country();
		$countryList = $countryDb->getData();
		$this->view->countryList = $countryList;
		
		//applicant profile
		$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
		$studentdetails = $studentRegistrationDB->fetchStudentHistoryDetails($registrationId);
		$this->view->appl_data = $studentdetails;
		
		//student profile
		$profileDb = new Records_Model_DbTable_Studentprofile();
		$profile = $profileDb->fnGetStudentProfileByApplicationId($studentdetails['IdApplication']);
		$this->view->profile = $profile;
				
		//parent (mother)
		$familyDb = new App_Model_Application_DbTable_ApplicantFamily();
		$motherData = $familyDb->getData($profile['appl_id'],'21');
		$this->view->motherData = $motherData;
		
				
		//document
		$documentDb = new App_Model_Application_DbTable_ApplicantUploadFile();
		
		
		$doc = array();
		
		$doc[] = array(
					'type_id' => 33,
					'type_name' => 'Gambar',
					'data' => $documentDb->getTxnFileArray($studentdetails['transaction_id'],33)
				);
		
		$doc[] = array(
				'type_id' => 48,
				'type_name' => 'KTP/SIM',
				'data' => $documentDb->getTxnFileArray($studentdetails['transaction_id'],48)
		);
		
		$doc[] = array(
				'type_id' => 35,
				'type_name' => 'Passport',
				'data' => $documentDb->getTxnFileArray($studentdetails['transaction_id'],35)
		);
		
		$doc[] = array(
				'type_id' => 36,
				'type_name' => 'Report Perubatan',
				'data' => $documentDb->getTxnFileArray($studentdetails['transaction_id'],36)
		);
		
		$doc[] = array(
				'type_id' => 37,
				'type_name' => 'Rapor untuk PSSB',
				'data' => $documentDb->getTxnFileArray($studentdetails['transaction_id'],37)
		);
		
		$this->view->documentList = $doc;
		
		//photo
		$photo = $documentDb->getTxnFile($studentdetails['transaction_id'],51);
		$this->view->photo = $photo;
		
		//registration checklist
		$checklistStudentDb = new Registration_Model_DbTable_ChecklistStudent();
		$checklist = $checklistStudentDb->getChecklistStudentData($registrationId);
		$this->view->checklist = $checklist;
				
	}

	


}
