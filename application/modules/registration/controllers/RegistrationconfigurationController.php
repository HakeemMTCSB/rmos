<?php
class Registration_RegistrationconfigurationController extends Base_Base {
	private $lobjSeniorstudentregistrationModel;
	private $lobjSeniorstudentregistrationForm;
	private $lobjinitialconfig;
	private $_gobjlogger;


	public function init() {
		$this->fnsetObj();
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
	}

	public function fnsetObj()
	{
			$this->lobjConfigurationForm = new Registration_Form_Registrationconfiguration();
			$this->lobjRegistrationConfigurationModel = new Registration_Model_DbTable_Registrationsetup();
	}


    /**
     * Function to Configure Registration setup for senior and new student
     * @auhor: vipul
     */

	public function indexAction() {
		$this->view->lobjform = $this->lobjConfigurationForm;

        if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$formData = $this->_request->getPost ();
			//asd($formData,false);
			// FIRST DELETE the record based on reg type
			$this->lobjRegistrationConfigurationModel->fnDeleteConfiguration( $formData ['registrationType'] );
			// insert the record
			$larrresult = $this->lobjRegistrationConfigurationModel->fnAddregistrationsetup( $formData );
			$this->_redirect( $this->baseUrl . '/registration/registrationconfiguration');
        }

        // FETCH RECORDS Senior
		$resultDataSenior = $this->lobjRegistrationConfigurationModel->fnGetConfiguration($regType=1 );
		if(count($resultDataSenior)>0) {
		$this->view->lobjform->minFinanceBalanceSenior->setValue($resultDataSenior[0]['minFinanceBalance']);
		$this->view->lobjform->academicLandscapeReqt->setValue($resultDataSenior[0]['academicLandscapeReqt']);
		$this->view->lobjform->minCgpa->setValue($resultDataSenior[0]['minCgpa']);
		$this->view->lobjform->studentDisciplinary->setValue($resultDataSenior[0]['studentDisciplinary']);
		}

		// FETCH RECORDS New
		$resultDataNew= $this->lobjRegistrationConfigurationModel->fnGetConfiguration($regType=2 );
		if(count($resultDataNew)>0) {
		$this->view->lobjform->minFinanceBalanceNew->setValue($resultDataNew[0]['minFinanceBalance']);
		}


	}

}