<?php

class Registration_AttendanceSetupController extends Base_Base
{
	
	 public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
        $this->view->title = $this->view->translate("Attendance Setup");
        
        $form = new Registration_Form_SearchAttendanceSetup();
        
        $attendanceSetupDB = new Registration_Model_DbTable_AttendanceSetup();
        
    	if ($this->getRequest()->isPost()){
    		
            	$formData = $this->getRequest()->getPost();
          		
            	$form->populate($formData);
            	
				$list  = $attendanceSetupDB->getList($formData);
				
          		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($list));				
                $paginator->setItemCountPerPage(100);
                $paginator->setCurrentPageNumber($this->_getParam('page',1));
                $this->view->paginator = $paginator; 
                               
        }else{
        	
        		$list  = $attendanceSetupDB->getList();
				
          		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($list));				
                $paginator->setItemCountPerPage(100);
                $paginator->setCurrentPageNumber($this->_getParam('page',1));
                $this->view->paginator = $paginator; 
        }
        
        $this->view->form = $form;
    }
	
    public function addAction()
    {
        // action body
        $this->view->title = $this->view->translate("Attendance Setup : Add");
        
        $form = new Registration_Form_AttendanceSetupForm();
       
        
        if ($this->_request->isPost()){
			
				if($form->isValid($_POST)) {
					
					$formData = $this->_request->getPost ();
					//var_dump($formData); exit;
					
					$auth = Zend_Auth::getInstance();
					$attendanceSetupDB = new Registration_Model_DbTable_AttendanceSetup();
					
					
					$data['att_effective_date'] = date('Y-m-d',strtotime($formData['att_effective_date']));
					$data['att_program_id'] = $formData['att_program_id'];
					$data['att_percentage'] = $formData['att_percentage'];
					$data['att_type'] = $formData['att_type'];

					for($i=0; $i<count($formData['att_program_scheme']); $i++){
						
						$data['att_program_scheme'] = $formData['att_program_scheme'][$i];
						
						for($x=0; $x<count($formData['att_reg_item']); $x++){
							$data['att_reg_item'] = $formData['att_reg_item'][$x];
							
							//check duplocate
							$result = $attendanceSetupDB->checkDuplicate($formData['att_effective_date'],$formData['att_program_id'],$data['att_program_scheme'],$data['att_reg_item'], $formData['att_type']);
							
							if(!$result){
								$data['att_createddt']=date('Y-m-d H:i:s');
								$data['att_createdby']=$auth->getIdentity()->id;
								$attendanceSetupDB->addData($data);
							}else{
								//duplicate entry
							}
						}
					}
					
					$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));					

					$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'attendance-setup', 'action'=>'add'),'default',true));
				}
        }
        
         $this->view->form = $form;
    }
    
    public function deletedataAction(){
    	
    		$att_id = $this->_getParam('id',null); 
    		
    		$attendanceSetupDB = new Registration_Model_DbTable_AttendanceSetup();
					
    		$attendanceSetupDB->deleteData($att_id);
    		
    		$this->_helper->flashMessenger->addMessage(array('success' => "Data has been deleted"));					

			$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'attendance-setup', 'action'=>'index'),'default',true));
    
    }
	
}

?>