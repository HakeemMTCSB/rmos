<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 28/7/2016
 * Time: 10:59 AM
 */
class Registration_CwapController extends Base_Base{

    private $_gobjlog;
    private $model;
    private $auth;

    public function init(){ //initialization function
        $this->_gobjlog = Zend_Registry::get('log');
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
    }

    public function indexAction(){
        $model = new Registration_Model_DbTable_Cwap();
        $bmodel = new Studentfinance_Model_DbTable_PaymentAmount();

        $advList = $model->getAdvancePayment();

        $table = '';
        if ($advList){
            $no = 0;
            $table .= '<table class="table" width="100%">';
            $table .= '<tr>';
            $table .= '<th>No</th>';
            $table .= '<th>Student ID</th>';
            $table .= '<th>Adv Payment No</th>';
            $table .= '<th>Invoice No</th>';
            $table .= '<th>Balance</th>';
            $table .= '</tr>';
            foreach ($advList as $key => $advLoop){
                $balance = $bmodel->getBalanceMain($advLoop['advpy_invoice_id']);

                if (str_replace(',', '', $balance['bill_balance']) > 0.00){
                    $no++;

                    $table .= '<tr>';
                    $table .= '<td>'.$no.'</td>';
                    $table .= '<td>'.$advLoop['registrationId'].'</td>';
                    $table .= '<td>'.$advLoop['advpy_fomulir'].'</td>';
                    $table .= '<td>'.$balance['bill_number'].'</td>';
                    $table .= '<td>'.$balance['bill_balance'].'</td>';
                    $table .= '</tr>';
                }else{
                    unset($advList[$key]);
                }
            }
            $table .= '</table>';
        }
        echo $table;
        exit;
    }
}