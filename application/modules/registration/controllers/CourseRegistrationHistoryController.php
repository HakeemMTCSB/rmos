<?php

class Registration_CourseRegistrationHistoryController extends Base_Base
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    	$this->view->title = $this->view->translate("Course Registration History");
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
	
		$form = new Registration_Form_SearchCourseHistory(array ('locale' => $locale));
		$this->view->form = $form;
	    	
		$session = Zend_Registry::get('sis');
		
		$subjectItemDB = new Registration_Model_DbTable_StudentRegistrationItem();

    	$regSubjectDB = new Records_Model_DbTable_StudentRegSubjectsHistory();
    	
    	if ($this->getRequest()->isPost()) 
		{ 
			if($form->isValid($_POST)) {
			    $formData = $this->getRequest()->getPost();
			    $form->populate($formData);
			    $session->history = $formData;
			    
			    $course = $regSubjectDB->searchCourseRegistration($formData);
			}
		    	    
	    	
		}else{ 
    	
    		//populate by session 
	    	/*if (isset($session->history)) {
	    		$form->populate($session->history);
	    		$formData = $session->history;			
	    		$course = $regSubjectDB->searchCourseRegistration($formData);
	    		//$this->view->publish_data = $publishDb->getPublishResultData($formData["IdProgram"],$formData["IdSemester"]);
	    	}*/
    	
    	}
    	
    	if(isset($course) && count($course)>0){
	    	foreach($course as $index=>$subject){
				  				
	    		//get created by
	    		$course[$index]['user']='';
	    		
	    		if($subject['createdrole']=='admin'){
	    			//get user name

					if (isset($subject['createdby']) && $subject['createdby'] != null) {
						$user = $regSubjectDB->getUser($subject['createdby']);
						if ($user) {
							$subject[$index]['user'] = $user['FullName'];
						}else{
							$subject[$index]['user'] = '';
						}
					}else{
						$subject[$index]['user'] = '';
					}
	    		}
	    		
		  		if($subject["Active"]==0) $status =  $this->view->translate("Pre Registered");
		  		if($subject["Active"]==1) $status = $this->view->translate("Registered"); 
		  		if($subject["Active"]==2) $status = $this->view->translate("Drop"); 			  				 
		  		if($subject["Active"]==3) $status = $this->view->translate("Withdraw");
		  		if($subject["Active"]==4) $status = $this->view->translate("Repeat");			  			
		  		if($subject["Active"]==6) { $status = $this->view->translate("Repeat-Replace"); echo '<br>'.$course['subcode_replace'];}
		  		if($subject["Active"]==9) $status = $this->view->translate("Pre-Repeat");
		  		if($subject["Active"]==10) $status = $this->view->translate("Pre-Replace");	
		  			
		  		$exam_status = $subject['exam_status'];
		  		if(isset($exam_status) && $exam_status!=''){
					if($exam_status == 'CT' || $exam_status == 'EX' || $exam_status == 'U'){
						
						if($subject["Active"]==0){
							$status = 'Applied';
						}else if($subject["Active"]==1){
							$status = $exam_status;
						}
					}
				}
	
			$course[$index]['status']=$status;				  		
    		}
    		
    		//echo '<pre>';
    		//print_r($course);
    		$this->view->course_list = $course;
    	}
    	
    	
    	
    }
    
    
    public function itemAction()
    {
    	$this->view->title = $this->view->translate("Course Registration History : Item");
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
    	$IdStudentRegSubjects = $this->_getParam('idRegSubject');
    	$IdStudentRegistration = $this->_getParam('id');
    	$idSemester = $this->_getParam('idSemester');
    	$idSubject = $this->_getParam('idSubject');
    	$this->view->id = $IdStudentRegistration;
    	
    	$studentRegDb = new Registration_Model_DbTable_Studentregistration();
    	$this->view->profile =$studentRegDb->getData($IdStudentRegistration,0);
    	
    	//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);
    	    	
    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    	
    	$form = new Registration_Form_SearchCourseItemHistory(array ('locale' => $locale,'idstudentregsubjects' => $IdStudentRegSubjects,'idstudentregistration' => $IdStudentRegistration,'idsemester' => $idSemester,'idsubject' => $idSubject));
		
		
		$regSubjectDB = new Records_Model_DbTable_StudentRegSubjectsHistory();
		
		if ($this->getRequest()->isPost()) 
		{ 
		    $formData = $this->getRequest()->getPost();
		    
		    $form->populate($formData);
		    //get detail    	
    		$items = $regSubjectDB->getDetailItemByCourse($IdStudentRegistration,$idSemester,$idSubject,$formData);
		    
		}else{
			$items = $regSubjectDB->getDetailItemByCourse($IdStudentRegistration,$idSemester,$idSubject);
		}
		    
    	$this->view->form = $form;
    	
    	if(count($items)>0){
    		$creditNoteDB = new Studentfinance_Model_DbTable_CreditNote();
	    	foreach($items as $index=>$item){
	    		
	    		//check
	    		
	    		if($item['status']==2 || $item['status']==3){
		    		if(isset($item['createdby']) && $item['createdby']==1){
		    			
		    			if($item['refund']){
							$refund = 	$item['refund'].'%';
						}else{
							$refund = 'No Refund';
						}													
								
					}else{
						//22-6-2015
						//check invoice...buat ni bukan ape sebab bila subject di drop dari student portal xde record refund disimpan
						//so just check jika CN tu wujud maka just display ada refund	
									
						
						if($item['invoice_id']){
							$cn_status = $creditNoteDB->getDataByInvoice($item['invoice_id'],null,0);											
							if($cn_status){
								$refund = 'With Refund';
							}else{
								$refund = 'No Refund';
							}
						}else{
							$refund = 'N/A';
						}		
					}
					
	    		}else{
	    			$refund = '';
	    		}		
	    		
				$items[$index]['refund']=$refund;	
	    	}
    	}
    
    	$this->view->item_list = $items;
    	//echo '<pre>';
    	//print_r($items);
 
    }
    
    
    
}
?>