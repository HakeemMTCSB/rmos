<?php

class Registration_BulkCourseRegistrationController extends Base_Base
{

   
    public function indexAction()
    {
    		
        $this->view->title = $this->view->translate("Bulk Course Registration");
        		
        $session = Zend_Registry::get('sis');
        
        $registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		$form = new Registration_Form_SearchBulkCourseRegistration();
		$this->view->form = $form;
		
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		$this->view->semester = $semesterDB->fnGetSemesterList();
		
		$programDb = new Registration_Model_DbTable_Program();
		$this->view->programs = $programDb->getData();
		
		if ($this->_request->isPost()){
			
			$formData = $this->_request->getPost ();
			
			if ($form->isValid($formData)) {
				
				$this->view->formData = $formData;
				//var_dump($formData);
				
				$form->populate($formData);
			
				$landscapeSubjectDb  = new GeneralSetup_Model_DbTable_Landscapesubject();	
				$registrationItemDB =new Registration_Model_DbTable_RegistrationItem();
				$courseGroupDB = new GeneralSetup_Model_DbTable_CourseGroup();
				$examRegDB = new Registration_Model_DbTable_ExamRegistration();
				$countryDB = new GeneralSetup_Model_DbTable_Country();
				$programDb = new Registration_Model_DbTable_Program();
				
				//program
    			$program = $programDb->getProgram($formData['IdProgram']);
    			$this->view->program = $program;
    						
				$register_item = $registrationItemDB->getRepeatRegItems($formData['IdProgram'],$formData['IdSemester']);
				$this->view->register_item = $register_item;
		
				//get course offer
				list($idLandscape,$subject_list) = $landscapeSubjectDb->getActiveLandscapeByIntake($formData);
				
				if(!$idLandscape){
					$this->view->err_message = 'Landscape not found';
				}else{
					$this->view->idLandscape = $idLandscape;
					
					foreach($subject_list as $index=>$subject){
						
						//get course group				
		  				$subject_list[$index]['group'] = $courseGroupDB->getGroupQuota($formData['IdSemester'],$subject['IdSubject'],$formData['IdProgram'],$formData['IdProgramScheme'],null,null);
		  									
					}				
				 	$this->view->subject_list = $subject_list;
	
				 	$countryList = $countryDB->fnGetCountryListDetails();
					$this->view->countryList = $countryList;
				}
			}
			
			
		}		
    }
    
     public function previewAction(){
    	
    	$this->view->title = 'Preview course registration';
	    if ($this->_request->isPost()){
	  
	    	$session = Zend_Registry::get('sis');	    	
	    	
			$landscapeSubjectDb  = new GeneralSetup_Model_DbTable_Landscapesubject();	
			$registrationItemDB =new Registration_Model_DbTable_RegistrationItem();
			$courseGroupDB = new GeneralSetup_Model_DbTable_CourseGroup();
			$examRegDB = new Registration_Model_DbTable_ExamRegistration();
			$countryDB = new GeneralSetup_Model_DbTable_Country(); 
			$cityDB =  new GeneralSetup_Model_DbTable_City();

			$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();   
			$intakeDB = new App_Model_Record_DbTable_Intake();
			$programDb = new Registration_Model_DbTable_Program();
			$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster(); 
			$icampus = new icampus_Function_Studentfinance_Invoice();		   
			$definationDB = new App_Model_General_DbTable_Definationms();
			$courseGroupProgram = new GeneralSetup_Model_DbTable_CourseGroupProgram();	
			$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
			$landscapeDetailDB = new GeneralSetup_Model_DbTable_LandscapeDetail();
			$landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
			$requisiteDB = new Registration_Model_DbTable_SubjectPrerequisite();
			$studentRegSubDB = new Registration_Model_DbTable_Studentregsubjects();
			$icms = new icampus_Function_Application_AcademicProgress();
			$progSchemeDb =  new GeneralSetup_Model_DbTable_Programscheme();
					    		    	
	    	$formData = $this->_request->getPost ();
			//var_dump($formData); exit;

			if ($formData['IdIntake']==''){
				$formData['IdIntake'] = 0;
			}
		    		    	
    		//intake
    		$intake = $intakeDB->getData($formData['IdIntake']);
    		$this->view->intake = $intake;
    		
    		//semester
    		$semester = $semesterDB->getData($formData['IdSemester']);			    	    	
			$this->view->semester = $semester;
			
			//program
    		$program = $programDb->getProgram($formData['IdProgram']);
    		$this->view->program = $program;
    		
    		//program Scheme
    		$program_scheme = $progSchemeDb->getSchemeById2($formData['IdProgramScheme']);
    		$this->view->program_scheme = $program_scheme;
    		
    		//get course info
    		$added_subject_list = array();
    		for($s=0; $s<count($formData["IdSubject"]);  $s++){
    			
    			$idSubject     		  = $formData["IdSubject"][$s];
    			
    			//get subject info
			 	$subject = $subjectDB->getData($idSubject);				 	
    			
			 	$subject['idGroup']   = isset($formData["IdGroup"][$idSubject]) ? $formData["IdGroup"][$idSubject]:null;	
			 	$subject['idCountry'] = isset($formData["country"][$idSubject]) ? $formData["country"][$idSubject]:null;
			 	$subject['idCity']    = isset($formData["city"][$idSubject]) ? $formData["city"][$idSubject]:null;	
			 	$subject['regItemArr']= (isset($formData["IdRegItem"][$idSubject]) && count($formData["IdRegItem"][$idSubject])>0) ? $formData["IdRegItem"][$idSubject]:null;	
			 	
    			//get selected group info
			 	if(isset($subject['idGroup']) && $subject['idGroup']!=''){
			 		$group = $courseGroupDB->getInfo($subject['idGroup']);
			 		$subject['group']=$group;
			 	}			 	
			 	
			 	//get selected exam center country/city
			 	$CountryReg = $countryDB->getDatabyId($subject['idCountry']);
			 	$CityReg = $cityDB->getDatabyId($subject['idCity']);
			 	$subject['country']=$CountryReg;
			 	$subject['city']=$CityReg;	
			 				 	
			 	$formData['exam_registration'][$idSubject] = $this->getExamRegistrationInfo($formData,$idSubject,$subject);
			 	
 				//check requisite
	    		$requisites = $requisiteDB->getPrerequisite($formData['IdLandscape'],$idSubject);
				$subject['requisites']=$requisites; 
								   					 	
    			array_push($added_subject_list,$subject);
    			
    		}//end for subject
    			
    		//echo '<pre>';
    		//print_r($formData);    		
	    	
    		$session->bulk_register = $formData;	
    		
    		//get student list
    		$student_list = array();
    		$subject_list = array();
    		
    		for($i=0; $i<count($formData['IdStudentRegistration']); $i++){
    			
    			$IdStudentRegistration = $formData['IdStudentRegistration'][$i];
    		
    			//get student info
    			$student = $studentRegistrationDb->getData($IdStudentRegistration,1);
    			    			
    			//get remaining credit hours or course				
				$summary = $icms->getExpectedGraduateStudent($IdStudentRegistration,'portal');
				$remaining = $summary['remaining'];
			
				//yati to get solid remaining paper for student
				if($formData['IdProgram']==5){ //CIFP
					//check if its includes PPP/Artcleship
					$status_taken = $studentRegSubDB->isRegisteredPaper($IdStudentRegistration);
					$remaining = ($status_taken) ? ($remaining) : ($remaining-1);
				}
				$student['remaining']  = $remaining;
				//end get summary credit hours 

				//umpukkan subject ke nama lain
				$subject_list = $added_subject_list;
				
    			foreach($subject_list as $key_subject=>$subject){
    				
    				$idSubject = $subject['IdSubject'];
    				
				 	
    				//check if registered at previous semester
		    		$isRegisteredPrevSem = $studentRegSubDB->isRegisteredPrevSemester($IdStudentRegistration,$idSubject,$formData['IdSemester']);
		    		if($isRegisteredPrevSem){ 	 
		    			   			
		    			$subject_list[$key_subject]['isRegisteredPrevSem']=1;
		    			
		    			//PS only Fail subject can repeat
				  		if($formData['IdScheme']==1){
				  			
					  		if( ($isRegisteredPrevSem['exam_status']=='I') || ($isRegisteredPrevSem['exam_status']=='C' && $isRegisteredPrevSem['grade_name']=='F') ){
					  			//allow register/repeat
					  			$allow_register = true;
					  			$subject_list[$key_subject]['allow']=1;
					  		}else{
					  			//not allow	
					  			$subject_list[$key_subject]['allow']=0;
					  			$allow_register = false;	  			
					  		}
					  		
				  		}//end PS allow repeat
				  		
		    			//to allow register for all proram ehere exam_status = I tapi status Active => ini case absent with valid reason
				  		if(($isRegisteredPrevSem['exam_status']=='I' && $isRegisteredPrevSem['Active']==1)){
				  			$allow_register = true;				  			
				  			$subject_list[$key_subject]['isRegisteredPrevSem']=2;
				  		}
				  		
		    		}else{
					  		$subject_list[$key_subject]['allow']=1;
					  		$subject_list[$key_subject]['isRegisteredPrevSem']=0;		
					  		$subject_list[$key_subject]['total_ch_taken']=0;		  		
					}
	    			
					//credit hour
    				if($subject['CourseType']==3){
				  		//research paper
    					$credithour = (isset($formData["credit_hour_registered"][$idSubject])) ? $formData["credit_hour_registered"][$idSubject]:0;
			  			$subject_list[$key_subject]['credit_hour_registered']=$credithour;
			  			
			  			if($credithour==0){
	 						$credithour = 'X';
	 					}
	 					
				  	}else{
				  		$credithour = null;
				  		$subject_list[$key_subject]['credit_hour_registered']='';
				  	}
				  	
				  	
				  	
	    			//----------- get registration item	fee info -----------
    				//get scheme selected section/group
				 	$scheme_send = 0;
				 	$idGroup = $subject['idGroup'];
				  	if(isset($idGroup) && $idGroup!=''){
				 		$selected_section_scheme = $courseGroupProgram->getGroupSchemByIdGroup($formData['IdProgram'],$idGroup);
				 		if($selected_section_scheme['program_scheme_id']!=$student['IdProgramScheme']){					 			
				 			$scheme_send = $selected_section_scheme['program_scheme_id'];
				 		}
				  	}
				  	
				  	//repeat status
    				if($subject_list[$key_subject]['isRegisteredPrevSem']==1 && $subject['CourseType']!=3){ //!RESEARCH	    					
				  		//$subject['item_register']=$item_repeat;
				  		$pass_repeat_status = 'RPT';
				  		
				  	}else if($subject_list[$key_subject]['isRegisteredPrevSem']==2 && $subject['CourseType']!=3){
				  		$pass_repeat_status = 'RPT';
				  	}else{
				  		//$subject['item_register']=$item_normal;	
				  		$pass_repeat_status = '';	
				  	}
				  	
	    			$regItemArr = $subject['regItemArr'];		 	
				 	if(isset($regItemArr) && count($regItemArr)>0){
				 		for($x=0; $x<count($regItemArr); $x++){
				 			$itemAdd = $definationDB->getData($regItemArr[$x]);					 			
				 			$fee = $icampus->calculateInvoiceAmountByCourse($IdStudentRegistration,$formData['IdSemester'],$idSubject,$itemAdd['idDefinition'],$pass_repeat_status,$credithour,$scheme_send);
				 			if(count($fee)>0){
				 				$itemAdd['fee_amount'] = $fee[0]['cur_code'].' '.$fee[0]['fsi_amount'];
				 			}else{
				 				$itemAdd['fee_amount'] = '';
				 			}
				 			$subject_list[$key_subject]['itemAdd'][$x]=$itemAdd;					 			
				 		}//end for					 		
				 	}//end if	
				 	//----------- end get registration item	fee info -----------------
				 	
				 	

				 	
		    		
	    			
	    			/* 0:Pass With Grade
		    		 * 1:Total credit hours
		    		 * 2: Co-requisite
		    		 */
	    			$requisites = $subject['requisites'];
	    			
		    		$prerequisites_status = true; //Passed
		    			
			    	if(count($requisites)>0){		    		
	 		
			    		$pre_requsite_list = array();
			    		
			    		foreach($requisites as $requisite){
			    			
				    		//get requisite type
					 		$PrerequisiteType = $requisite['PrerequisiteType'];
					 		
					 		if($PrerequisiteType==0){
					 									 			
					 			//get required subject grade				 		
					 			$required_subject = $studentRegSubDB->isRegisterPrevPass($student['IdStudentRegistration'],$requisite['IdRequiredSubject'],$semester['IdSemesterMaster']);		
				
					 			if($required_subject){
						 			if($required_subject['exam_status']=='CT' || $required_subject['exam_status']=='EX'){
							 			//lepas
						 			}else{
						 				
						 				//get grade defination					 				
						 				$grade1 = $definationDB->getDataByDefCode(22,trim($required_subject['grade_name']));	//grade yg student dapat	
										$grade2 = $definationDB->getData($requisite['PrerequisiteGrade']); // grade yg perlu dapat
										
										if(!empty($grade1) && !empty($grade2)){
											if($grade1['defOrder'] <= $grade2['defOrder']){
												//lepas										
											}else{
												//x lepas
												$prerequisites_status = false;
												array_push($pre_requsite_list,$requisite['req_subcode']);
											}
										}else{
											//anggap x lepas
											$prerequisites_status = false;
											array_push($pre_requsite_list,$requisite['req_subcode']);
										}
						 			}
					 			}else{
				 					//studnt kene amik dulu subject tu
				 					$prerequisites_status = false;
									array_push($pre_requsite_list,$requisite['req_subcode']);
					 			}
					 			
					 			
					 		}else
					 		if($PrerequisiteType==2){
					 			
					 			if($student['IdProgram']==5){ //CIFP
					 				$total_credit_hour_completed=$summary['complete_chr_cifp'];
					 			}else{
					 				$total_credit_hour_completed=$summary['complete_chr_other'];
					 			}
					 							 				
					 			if($total_credit_hour_completed >= $requisite['TotalCreditHours']){
					 				//$subject_list[$index]['requisite_status']='';
					 			}else{
					 				//$subject_list[$index]['requisite_status']='Did not meet min total credit hour';
					 				$prerequisites_status = false;
					 				array_push($pre_requsite_list,'Did not meet min total credit hour');
					 			}
					 			
					 		}else
					 		if($PrerequisiteType==3){
					 				//Co-requisite
						 			//$subject_list[$index]['requisite_req_subject']=$requisite['IdRequiredSubject'];			 		
						 			//$subject_list[$index]['requisite_status']='Subject requisite '.$requisite['req_subcode'];	
						 			//do later pending
						 			array_push($pre_requsite_list,'Subject requisite '.$requisite['req_subcode']);					 			
					 		}
					 		
			    		}//end foreach requsiite	
			    		
			    		$subject_list[$key_subject]['requisite_status']=$pre_requsite_list;
	    				$subject_list[$key_subject]['prerequisites_status']=$prerequisites_status;
			    		
				 	}//end count prerequisite
		  		    				
	    	
    			}//end foreach subject_list     			

				$student['subject_list']=$subject_list;
				array_push($student_list,$student);
    			
    		}//end for count IsStudentRegistarion
	            	
    		
    		$this->view->subject_list = $added_subject_list;
    		$this->view->student_list = $student_list;

    		$landscape_detail = $landscapeDB->getLandscapeDetails($formData['IdLandscape']);
	        $semesterType = $semester['SemesterType']==171?'Short Semester': $semester['SemesterType']==172?'Long Semester':null;
	
	        $errMsg = '';
	        #171=Short, 172=Long / 1=Course, 2=Credit hour
	        $wLanscapeDetail = $landscapeDetailDB->getDatabyLandscapeId($student['IdLandscape'],1,$semester['SemesterType']);
	        if(count($wLanscapeDetail)==0){
	            $this->view->errMsg = 'Semester rule (landscape) not found';
	        }else{
				$this->view->reqType = $wLanscapeDetail[0]['Type'];         #1=Course, 2=Credit
		        $this->view->minReq = $wLanscapeDetail[0]['MinRegCourse'];
		        $this->view->maxReq = $wLanscapeDetail[0]['MaxRegCourse'];
		    }	
		    
	    }//end if post
	  
    }
    
	public function registerAction(){
    
	    $session = Zend_Registry::get('sis');
	    	
	    if ($this->_request->isPost()){
	    	
	    		$db = Zend_Db_Table::getDefaultAdapter();    
	    		$auth = Zend_Auth::getInstance();	    		
	    		
    			$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();        
				$landscapeSubjectDb  = new GeneralSetup_Model_DbTable_Landscapesubject();
				$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
				$semesterDB = new Registration_Model_DbTable_Semester();
				$studentSemesterStatusDb =  new Registration_Model_DbTable_Studentsemesterstatus();
				$courseGroupDB = new GeneralSetup_Model_DbTable_CourseGroup();
				$courseGroupStudentDB = new GeneralSetup_Model_DbTable_CourseGroupStudent();
				$systemErrorDB = new App_Model_General_DbTable_SystemError();
				$historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
				$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
				$examRegDB = new Registration_Model_DbTable_ExamRegistration();
				$invoiceClass = new icampus_Function_Studentfinance_Invoice();
				$regSubjectItem = new Registration_Model_DbTable_StudentRegistrationItem();
				$courseGroupProgram = new GeneralSetup_Model_DbTable_CourseGroupProgram();
				$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
				$progSchemeDb =  new GeneralSetup_Model_DbTable_Programscheme();
				$intakeDB = new App_Model_Record_DbTable_Intake();
				$programDb = new Registration_Model_DbTable_Program();
							
				$previewformData = $this->_request->getPost ();   
    			
    			$formData = $session->bulk_register;
    			
    			//print_r($formData);
    			    		       			
				if(count($formData['IdStudentRegistration'])>0){

					if ($formData['IdIntake']==''){
						$formData['IdIntake'] = 0;
					}

					//intake
		    		$intake = $intakeDB->getData($formData['IdIntake']);
		    		$this->view->intake = $intake;
		    		
		    		//semester
		    		$semester = $semesterDB->getData($formData['IdSemester']);			    	    	
					$this->view->semester = $semester;
					
					//program
		    		$program = $programDb->getProgram($formData['IdProgram']);
		    		$this->view->program = $program;
		    		
		    		//program Scheme
		    		$program_scheme = $progSchemeDb->getSchemeById($formData['IdProgramScheme']);
		    		$this->view->program_scheme = $program_scheme;
					
					for($i=0; $i<count($formData['IdStudentRegistration']); $i++){
    		
    					$IdStudentRegistration = $formData['IdStudentRegistration'][$i];
    					
    					for($i=0; $i<count($formData["IdSubject"]);  $i++){
    						
    						$idSubject     = $formData["IdSubject"][$i];
    						
    						//get subject info
			 				$subject_info = $subjectDB->getData($idSubject);	
			 				
			 				if(isset($formData['IdRegItem'][$idSubject]) && count($formData['IdRegItem'][$idSubject])>0){
					 						
	 						//umpukkan
				 			$idSubjectToRegister = $idSubject;
				 			
				 			//utk cater thesis di pass ke invoice su
				 			$pass_credit_hour_registered = null;
				 			$pass_repeat_status = null;
				 			
    						
    						if(isset($previewformData['idRemoveSubject'][$IdStudentRegistration]) && in_array($idSubject,$previewformData['idRemoveSubject'][$IdStudentRegistration])){
    							//echo 'cancel';
    						}else{
    							    							
    							$db->beginTransaction();
						
									try{					
																	
										//  ---------------- start insert subject  ----------------
										 				 			
								 				    
				 						if($i==0){	
					 						//  ---------------- update studentsemesterstatus table ----------------	
											$this->updateStudentSemesterStatus($IdStudentRegistration,$formData['IdSemester'],130);
											//  ---------------- end update studentsemesterstatus table ----------------	
											
											
											//  ---------------- update studentregistration table ----------------					 					 		
									 		$data['Semesterstatus'] =  130; //Register
											$data['senior_student'] = 0; // New Student					
									 		$studentRegistrationDb->updateData($data,$IdStudentRegistration);	
									 	    //  ---------------- end update studentregistration table ----------------
				 						}//end update status	
					 					 						
										$active_status = 0; //default:Pre-register
							 			
							 			//check if duplicate entry
							 			$isRegisterSubject = $studentRegSubjectDB->isRegisteredBySemester($IdStudentRegistration,$idSubject,$formData['IdSemester']);
										
							 			if(!$isRegisterSubject){	

								 			//check if group is selected
								 			if(isset($formData['IdGroup'][$idSubject]) && $formData['IdGroup'][$idSubject]!=''){
								 				$IdCourseTaggingGroup = $formData['IdGroup'][$idSubject];
								 			}else{
								 				$IdCourseTaggingGroup=0;
								 			}						 								 				
								 												 														 			
								 			/*  Note:
								 			 *  CASE 1 : $formData['prevReg'][$idSubject] =1 register at prev sem and completed
								 			 *  CASE 2 : $formData['prevReg'][$idSubject] =2 register at prev sem and not completed, status=I
								 			 */
								 			
								 			if($subject_info['CourseType']!=3){ // Research paper dont have repeat											 			
								 				
							 					//CASE 1
							 					if($previewformData['prevReg'][$IdStudentRegistration][$idSubject]==1){
						 					
							 						$pass_repeat_status = 'RPT';
							 						$active_status = 9; // Pre-repeat									 						
							 						
								 					//check how many time this student repeat this paper
								 					$totalRepeat = $studentRegSubjectDB->getRepeatStatus($IdStudentRegistration,$formData['IdSemester']);
								 																		 						
								 					if($totalRepeat<2){	
								 						
								 						$subject["cgpa_calculation"]=1;//exclude calculation
								 						$subject2["cgpa_calculation"]=1;//exclude calculation												 						
								 					}
						 					
						 						}//end prevReg = 1
						 						
						 						
									 			//CASE 2: exam status=I
								 				//Hantar kat su REPEAT tapi simpan dalam table studentregsubject staus active=0:pre-reg (default)
							 					if($previewformData['prevReg'][$IdStudentRegistration][$idSubject]==2){									 					
							 						$pass_repeat_status = 'RPT';
							 					}//end prevReg = 2
								 			}
								 			
							 				
							 				
							 				if(isset($formData['credit_hour_registered'][$idSubject]) && $formData['credit_hour_registered'][$idSubject]!=''){
							 					
							 					$subject["credit_hour_registered"] = $formData['credit_hour_registered'][$idSubject];
							 					$subject2["credit_hour_registered"] = $formData['credit_hour_registered'][$idSubject];
							 					
							 					if($formData['credit_hour_registered'][$idSubject]==0){
							 						$pass_credit_hour_registered = 'X';
							 					}else{
							 						$pass_credit_hour_registered = $formData['credit_hour_registered'][$idSubject];
							 					}										 					
							 				}else{
												$subject["credit_hour_registered"] = null;
												$subject2["credit_hour_registered"] = null;
											}
						 				
								 			//add table studentregsubject	
								 			$subject["IdStudentRegistration"] = $IdStudentRegistration;
								 			$subject["IdSubject"] = $idSubjectToRegister;		
											$subject["IdSemesterMain"] = $formData['IdSemester'];
											$subject["SemesterLevel"]= null;
											$subject["IdLandscapeSub"]=0;
											$subject["Active"] = $active_status; 
											$subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
											$subject["UpdUser"]   = $auth->getIdentity()->iduser;
											$subject["IdCourseTaggingGroup"] = $IdCourseTaggingGroup;	
											$IdStudentRegSubjects = $studentRegSubjectDB->addData($subject);
	
											//add table studentregsubject audit trail/history
											$subject2['IdStudentRegSubjects']=0;//$IdStudentRegSubjects;
											$subject2["IdStudentRegistration"] = $IdStudentRegistration;
								 			$subject2["IdSubject"] = $idSubjectToRegister;		
											$subject2["IdSemesterMain"] = $formData['IdSemester'];
											$subject2["SemesterLevel"]= null;
											$subject["IdLandscapeSub"]=0;
											$subject2["Active"] = $active_status;  
											$subject2["UpdDate"]   = date ( 'Y-m-d H:i:s');
											$subject2["UpdUser"]   = $auth->getIdentity()->iduser;
											$subject2["IdCourseTaggingGroup"] = $IdCourseTaggingGroup;
											$subject2["message"] = 'Bulk Course Regisration';
											$subject2['createddt']=date("Y-m-d H:i:s");
											$subject2['createdby']=$auth->getIdentity()->id;
											$subject2['createdrole']='admin';
											$historyDB->addData($subject2);	
											
											
						 					if($IdStudentRegSubjects){											 		
									 			for($z=0; $z<count($formData['IdRegItem'][$idSubject]); $z++){
												
									 				$item_id = $formData['IdRegItem'][$idSubject][$z];
									 				
													//regsubjects_detail
													$dataitem = array(
																	'student_id'		=> $IdStudentRegistration,
																	'regsub_id'			=> $IdStudentRegSubjects,
																	'semester_id'		=> $formData['IdSemester'],
																	'subject_id'		=> $idSubjectToRegister,
																	'item_id'			=> $item_id,
																	'section_id'		=> $IdCourseTaggingGroup,
																	'ec_country'		=> 0,
																	'ec_city'			=> 0,
																	'created_date'		=> new Zend_Db_Expr('NOW()'),
																	'created_by'		=> $auth->getIdentity()->iduser,
																	'created_role'		=> 'admin',
																	'ses_id'			=> 0,
																	'status'			=> 0
													);
													$idRegSubjectItem = $regSubjectItem->addData($dataitem);															
													
													$dataitem['id'] = $idRegSubjectItem;
													$dataitem["message"] = 'Bulk Course Regisration: Add Item';
													$dataitem['createddt']=date("Y-m-d H:i:s");
													$dataitem['createdby']=$auth->getIdentity()->id;
													$dataitem['createdrole']='admin';
													$db->insert('tbl_studentregsubjects_detail_history',$dataitem);	
														
	
																					
													if($item_id==879){
														//---------------exam registration---------------										
														$this->saveExamRegistration($formData,$IdStudentRegistration,$idSubjectToRegister,$subject_info);													
														//--------------end exam registration---------------
													}
													
												}//end for													
									 				
											}//end insert item
	
											
					 						//get scheme selected section/group khas utk invoice
										 	$scheme_send = 0;
										  	if(isset($IdCourseTaggingGroup) && $IdCourseTaggingGroup!=''){
										 		$selected_section_scheme = $courseGroupProgram->getGroupSchemByIdGroup($formData['IdProgram'],$IdCourseTaggingGroup);
										 		if($selected_section_scheme['program_scheme_id']!=$formData['IdProgramScheme']){
										 			//su pesan: kalau x sama scheme hantar student selection section scheme
										 			$scheme_send = $selected_section_scheme['program_scheme_id'];
										 		}
										  	}											  	 
										  	$invoiceClass->generateInvoiceStudent($IdStudentRegistration,$formData['IdSemester'],$idSubjectToRegister,$pass_repeat_status,$pass_credit_hour_registered,0,$scheme_send);
											
						 				}//end if regiseter
	    								
	    								
									 	$db->commit();
																	
										//  ---------------- end insert subject  ------------------
		
										/* ================================================
										 * checking Repeat for GS student
										 * ================================================
										 */
										if($formData['IdScheme']==11){
											
											//guna function run repeat izhma								
											$modelDB = new Registration_Model_DbTable_SeekingForLandscape();
											$modelDB->repeatSubject($formData);
											$modelDB->updateRepeatCalculation($formData);
			
										}
										/* ==================================
										 * END checking Repeat for GS student
										 * ==================================
										 */
										
									}catch (Exception $event) {
															
										$db->rollBack();
											
										$error['se_txn_id']=0;
										$error['se_IdStudentRegistration']=$formData['IdStudentRegistration'];
										$error['se_IdStudentRegSubjects']=0;
										$error['se_title']='Bulk Course Registration';
										$error['se_message']=$event->getMessage();
										$error['se_createddt']=date("Y-m-d H:i:s");
										$error['se_createdby']=$auth->getIdentity()->id;
										$systemErrorDB->addData($error);
											
									}//end try cathc 								
								
    							}//end previewData  
    						
    						}//end if isset item  												
											
						}//end for subject
					
					}//end for student
					
					$this->_helper->flashMessenger->addMessage(array('success' => "Bulk Course registration completed"));
					$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'bulk-course-registration', 'action'=>'index'),'default',true));
						
					
				}//end if student_id
				
				exit;
				//$this->_helper->flashMessenger->addMessage(array('error' => "Bulk Course Registration failed"));
				
	   		}//end post
    }
    
    
 	public function updateStudentSemesterStatus($IdStudentRegistration,$IdSemesterMain,$newstatus){
    	
    	//check current status
    	$semesterStatusDB =  new Registration_Model_DbTable_Studentsemesterstatus();
    	$semester_status = $semesterStatusDB->getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain);
    	
    	if(isset($semester_status) && $semester_status['studentsemesterstatus']==$newstatus){
    		//nothing to update
    	}else{
    		//add new status & keep old status into history table
    		$cms = new Cms_Status();
    		$auth = Zend_Auth::getInstance();
    			      			        	
			$data =  array( 'IdStudentRegistration' => $IdStudentRegistration,									           
				            'idSemester' => $IdSemesterMain,
				            'IdSemesterMain' => $IdSemesterMain,								
				            'studentsemesterstatus' => $newstatus, 	//Register idDefType = 32 (student semester status)
	                        'Level'=>1,
				            'UpdDate' => date ( 'Y-m-d H:i:s'),
				            'UpdUser' => $auth->getIdentity()->iduser
	        );				
					
    		$message = 'Bulk Course Registration';
    		$cms->copyDeleteAddSemesterStatus($IdStudentRegistration,$IdSemesterMain,$data,$message,null);
    	}
    	
    }
    
	public function getStudentAction(){

		$id = $this->_getParam('id',null);

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();

		$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();   
		$student_list = $studentRegistrationDb->searchStudentName($id);

		$ajaxContext->addActionContext('view', 'html')
			->addActionContext('form', 'html')
			->addActionContext('process', 'json')
			->initContext();

		$json = Zend_Json::encode($student_list);

		echo $json;
		exit();

	}
	
	public function getExamRegistrationInfo($formData,$idSubject,$subject_info){
		
			$auth = Zend_Auth::getInstance();
    		
    		$examRegDB = new Registration_Model_DbTable_ExamRegistration();
    		$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();    		
    			
    		//if 2,3,9 should not have exam registration info
    		if( in_array($subject_info['CourseType'],array(2,3,19)) == false){
			
	    		if($formData['IdScheme']==11){ //GS
	    			//auto set
	    			$formData['country'][$idSubject] = 121; //Malaysia
	    			$formData['city'][$idSubject] = 1348; //Kuala Lumpur
	    		}
	    		
	    		if(isset($formData['country'][$idSubject]) && $formData['country'][$idSubject]!=''){
	    			    		
					if ( is_numeric($formData['city'][$idSubject]) )
					{
						$city = $formData['city'][$idSubject];
						$city_others = ''; 
					}
					else
					{
						$city = 99;
						$city_others = $formData['city'][$idSubject]; 
					}
					
					
					$dataec['er_idCountry']=$formData['country'][$idSubject];
					$dataec['er_idCity']=$city;
					
					if($city_others!=''){
						$dataec['er_idCityOthers'] = $city_others;													
					}else{
						$dataec['er_idCityOthers'] = null;
						if($city==99){
							$dataec['er_idCity']=null;
						}											
					}
						
					//assign exam center to student
					if($dataec['er_idCountry']!='' && $dataec['er_idCity']!=''){	

						//get exam center that has been assigned
						$examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
						$ec_info = $examCenterDB->getExamCenterByCountryCity($formData['IdSemester'],$dataec['er_idCountry'],$dataec['er_idCity'],$dataec['er_idCityOthers']);
						

						if(count($ec_info)==1){													
							$dataec['er_ec_id'] = $ec_info[0]['ec_id'];							
						}else{
							$dataec['er_ec_id'] = null;
						}
						
					}else{
						$dataec['er_ec_id'] = null;
					}
					
					return $dataec;
					
	    		}//end isset
	    		
    		}//end in array	
		
		
	}
	
	public function saveExamRegistration($formData,$IdStudentRegistration,$idSubject,$subject_info){

    		$auth = Zend_Auth::getInstance();    		
    		
    		//print_r($subject_info);
    		
    		//if 2,3,9 should not have exam registration info
    		if( in_array($subject_info['CourseType'],array(2,3,19)) == false){
				    			    		
	    		if(isset($formData['country'][$idSubject]) && $formData['country'][$idSubject]!=''){
	    			    	
	    			$dataec['er_idCountry']=$formData['exam_registration'][$idSubject]['er_idCountry'];
	    			$dataec['er_idCity']=$formData['exam_registration'][$idSubject]['er_idCity'];
	    			$dataec['er_idCityOthers']=$formData['exam_registration'][$idSubject]['er_idCityOthers'];
	    			$dataec['er_ec_id'] = $formData['exam_registration'][$idSubject]['er_ec_id'];
					$dataec['er_idStudentRegistration']=$IdStudentRegistration;
					$dataec['er_idSemester']=$formData['IdSemester'];
					$dataec['er_idProgram']=$formData['IdProgram'];
					$dataec['er_idSubject']=$idSubject;
					$dataec['er_status']=764; //764:Regiseterd 
					$dataec['er_createdby']=$auth->getIdentity()->iduser;
					$dataec['er_createddt']= date ( 'Y-m-d H:i:s');	
					$dataec['er_createdrole']= 'ADMIN';
				
					//print_r($dataec);
					$examRegDB = new Registration_Model_DbTable_ExamRegistration();
					$examRegDB->addData($dataec);
					
	    		}//end isset
    		}//end in array	
		
		}//end add exam center
    
}

?>