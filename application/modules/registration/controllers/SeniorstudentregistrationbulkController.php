<?php
class Registration_SeniorstudentregistrationbulkController extends Base_Base {
	private $lobjSeniorstudentregistrationModelbulk;
	private $lobjStudentregistrationModel;
	private $lobjStudentregistrationForm;
	private $lobjinitialconfig;	
	private $_gobjlogger;

	
	public function init() {
		$this->fnsetObj();
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
			
	}
	
	public function fnsetObj(){
		$this->lobjSeniorstudentregistrationModelbulk = new Registration_Model_DbTable_Seniorstudentregistrationbulk();
		$this->lobjStudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjStudentregistrationForm = new Registration_Form_Studentregistrationbulk();		
		$this->lobjplacementtestmarksmodel = new Application_Model_DbTable_Placementtestmarks();
		$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration (); 
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
		$this->lobjInvoiceModel = new Studentfinance_Model_DbTable_Invoice();
	}
	
	public function indexAction() {
		
		$idUniversity =$this->gobjsessionsis->idUniversity;	
		$this->view->config = $initialConfig = $this->lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);	
		$this->view->lobjform = $this->lobjform; 
		$this->view->results = $larrresult= array();
		$this->view->lobjStudentRegForm = $this->lobjStudentregistrationForm;
		//$studentlist = $this->lobjSeniorstudentregistrationModelbulk->fnGetApplicantNameList();
		
		$larProgramNameCombo = $this->lobjplacementtestmarksmodel->fngetProgramNameCombo();	
		$this->view->lobjform->field8->addMultiOptions($larProgramNameCombo);

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$this->view->results = $larrresult = $this->lobjSeniorstudentregistrationModelbulk->fnSearchStudentApplication( $this->lobjform->getValues ()); //searching the values for the user
			}
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
				$ldtsystemDate = date ( 'Y-m-d H:i:s' );
				$auth = Zend_Auth::getInstance();
				$count=count($larrformData['IdApplication']);
				for($i=0;$i<$count;$i++) {
					if($larrformData['IdSemestersyllabus'][$i] != "") {
/*						$exists = $this->lobjSeniorstudentregistrationModelbulk->fnCheckExistingSemester($larrformData['IdSemester'][$i],$larrformData['IdApplication'][$i]);
						if(!empty($exists)) {
							echo '<script language="javascript">alert("This Student for this particular semester is already registered")</script>';
						} elseif(@count($larrformData['subjects']) == '0') {
							echo '<script language="javascript">alert("Select the subjects")</script>';
						} else {*/
							$larrformDatas ['IdApplication'] = $larrformData['IdApplication'][$i];
							$larrformDatas ['registrationId'] = $larrformData['registrationId'][$i];
							$larrformDatas ['Psswrd'] = $larrformData['Psswrd'][$i];
							$larrformDatas ['IdLandscape'] = $larrformData['IdLandscape'][$i];
							$larrformDatas ['IdSemester'] = $larrformData['IdSemester'][$i];
							$larrformDatas ['IdSemestersyllabus'] = $larrformData['IdSemestersyllabus'][$i];
									
							$larrformDatas ['UpdDate'] = $ldtsystemDate;
							$larrformDatas ['UpdUser'] = $auth->getIdentity()->iduser;
							$larrstudentreg = $this->lobjSeniorstudentregistrationModelbulk->fnAddStudentregistration($larrformDatas);
									
							$subjects =  $larrformData['subjects'][$larrformData['IdApplication'][$i]];
							$subjectscount= count($subjects);
				
							for($j=0;$j<$subjectscount;$j++) {
								
								$larrformDatasubjects = explode("/",$larrformData['subjects'][$larrformData['IdApplication'][$i]][$j]);
								$larrformDatassub['IdSubject'] = $larrformDatasubjects[0];
								$larrformDatassub['IdStudentRegistration']=$larrstudentreg;
								$larrformDatassub['UpdDate'] =$ldtsystemDate;
								$larrformDatassub['UpdUser'] = $auth->getIdentity()->iduser;
								$larrformDatassub['Active'] = 1;
										
								$larrstudentregsubjects = $this->lobjSeniorstudentregistrationModelbulk->fnAddStudentSubjectsperSemester($larrformDatassub);
							}
							
							
						$larrresInvoice=$this->lobjInvoiceModel->fnGetAmount(8,$larrformData['IdProgram'][$i],$larrformData['IdSemester'][$i]);

						if(!empty($larrresInvoice)) {
							$lastarrInv=$this->lobjStudentregistrationModel->fnaddInvoice($larrresInvoice['Amount'],$larrformData['IdApplication'][$i],$larrformData['IdSemestersyllabus'][$i]);				
							$this->lobjStudentregistrationModel->fnaddInvDetails($larrresInvoice['Amount'],$lastarrInv);
							$this->lobjstudentapplication->fnGenerateCodes($this->gobjsessionsis->idUniversity,$lastarrInv,$larrformData['IdApplication'][$i]);
						}	
						
						/////////////////////////// end of invoice generation //////////////////////////////////
							
							
						//}
					}
					
				}
				//log file	
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."New seniorstudentregistrationbulk Add"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/registration/seniorstudentregistrationbulk/index');
		}
		
	}
}