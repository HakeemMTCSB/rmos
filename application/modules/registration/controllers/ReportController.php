<?php
class Registration_ReportController extends Base_Base { //Controller for the User Module

    private $_gobjlog;
    private $model;

    public function init() { //initialization function
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        $this->fnsetObj();	
    }

    public function fnsetObj(){
        $this->model = new Registration_Model_DbTable_Report();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Item Registration Summary');
        $searchForm = new Registration_Form_ItemRegistrationSearchForm();
        $this->view->searchForm = $searchForm;
        
        $session = Zend_Registry::get('sis');
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            if (isset($formData['search'])){
                $list = $this->model->getStudentList($formData);
                $this->view->list = $list;
                
                $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($list));				
                $paginator->setItemCountPerPage(1000);
                $paginator->setCurrentPageNumber($this->_getParam('page',1));
                $this->view->paginator = $paginator;
                $searchForm->populate($formData);
            }else{
                unset($session->result);
            }
        }else{
            if (isset($session->result)){
                $formData = $session->result;
                
                $list = $this->model->getStudentList($formData);
                $this->view->list = $list;
                
                $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($list));				
                $paginator->setItemCountPerPage(1000);
                $paginator->setCurrentPageNumber($this->_getParam('page',1));
                $this->view->paginator = $paginator;
                $searchForm->populate($formData);
            }
        }
    }
    
    public function itemRegistrationReportAction(){
        $this->view->title = $this->view->translate('Item Registration Report');
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            //dd($formData);
            $searchForm = new Registration_Form_ItemRegistrationReportSearchForm(array('programid'=>$formData['program']));
            $this->view->searchForm = $searchForm;
            $searchForm->populate($formData);
            
            $studList = $this->model->getStudentList2($formData);
            $this->view->studList = $studList;
            
            $studentArr = array(0);
            if ($studList){
                $studentArr = array();
                foreach ($studList as $studLoop){
                    array_push($studentArr, $studLoop['IdStudentRegistration']);
                }
            }
            
            $courseList = $this->model->getCourseOfferedBasedOnSem($formData['semester'], $formData, $studentArr, $formData['itemregister']);
            //dd($courseList);
            //exit;
            $this->view->courseList = $courseList;
            
            $this->view->itemId = $formData['itemregister'];
            $this->view->semesterId = $formData['semester'];
        }else{
            $searchForm = new Registration_Form_ItemRegistrationReportSearchForm();
            $this->view->searchForm = $searchForm;
        }
    }
    
    public function printReportAction(){
        $this->_helper->layout->disableLayout();
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            $list = $this->model->getStudentList($formData);
            $this->view->list = $list;
        }
        $this->view->filename = date('Ymd').'_list_of_item_registration_summary.xls';
    }
    
    public function printItemRegistrationReportAction(){
        $this->_helper->layout->disableLayout();
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            
            $studList = $this->model->getStudentList2($formData);
            $this->view->studList = $studList;
            
            $studentArr = array(0);
            if ($studList){
                $studentArr = array();
                foreach ($studList as $studLoop){
                    array_push($studentArr, $studLoop['IdStudentRegistration']);
                }
            }
            
            $courseList = $this->model->getCourseOfferedBasedOnSem($formData['semester'], $formData, $studentArr, $formData['itemregister']);
            $this->view->courseList = $courseList;
            
            $this->view->itemId = $formData['itemregister'];
            $this->view->semesterId = $formData['semester'];
        }
        $this->view->filename = date('Ymd').'_list_of_item_registration_report.xls';
    }
    
    public function getSemesterAction(){
        $id = $this->_getParam('id',null);
        
        if ($id != null){
            $this->_helper->layout->disableLayout();

            $ajaxContext = $this->_helper->getHelper('AjaxContext');
            $ajaxContext->addActionContext('view', 'html');
            $ajaxContext->initContext();

            $ajaxContext->addActionContext('view', 'html')
                ->addActionContext('form', 'html')
                ->addActionContext('process', 'json')
                ->initContext();

            $progDet = $this->model->getProgramDetail($id);
            $semList = $this->model->getSemester($progDet['IdScheme']);
        }else{
            $semList = array();
        }
        
        $json = Zend_Json::encode($semList);
		
	echo $json;
	exit();
    }
    
    static function subjectStatus($status){
        /*
         * 0:pre-register 1:Register  2: Add&Drop 3:Withdraw 4: Repeat 5:Refer background-color: #00ff00;
         */
        
        switch ($status){
            case 0:
                $statusName = '<td>Pre-Registered</td>';
                break;
            case 1:
                $statusName = '<td>Registered</td>';
                break;
            case 2:
                $statusName = '<td>Add & Drop</td>';
                break;
            case 3:
                $statusName = '<td>Withdraw</td>';
                break;
            case 4:
                $statusName = '<td>Repeat</td>';
                break;
            case 5:
                $statusName = '<td>Refer</td>';
                break;
            case 9:
                $statusName = '<td>Pre-Repeat</td>';
                break;
            case null:
                $statusName = '<td></td>';
                break;
            default:
                $statusName = '<td></td>';
        }
        
        return $statusName;
    }
}