<?php

class Registration_StudentregistrationbulkController extends Base_Base
{

    public function indexAction()
    {

        $this->view->title = $this->view->translate("New Student Registration");

        $session = Zend_Registry::get('sis');

        $registry = Zend_Registry::getInstance();
        $this->view->locale = $registry->get('Zend_Locale');

        $form = new Registration_Form_SearchApplicants();
        $this->view->form = $form;


        $applicantDB = new Registration_Model_DbTable_ApplicantTransaction();

        //clear the session
        if ($this->getRequest()->isPost() && $this->_request->getPost('Clear')) {
            unset($session->result);
        }

        if ($this->_request->isPost() && $this->_request->getPost('Search')) {

            unset($session->result);

            $formData = $this->_request->getPost();

            $formData['IdIntake'] = $this->view->escape(strip_tags($formData['IdIntake']));
            $formData['IdProgram'] = $this->view->escape(strip_tags($formData['IdProgram']));
            $formData['IdProgramScheme'] = $this->view->escape(strip_tags($formData['IdProgramScheme']));
            $formData['IdBranch'] = $this->view->escape(strip_tags($formData['IdBranch']));
            $formData['payment_status'] = $this->view->escape(strip_tags($formData['payment_status']));
            $formData['applicant_name'] = $this->view->escape(strip_tags($formData['applicant_name']));
            $formData['applicant_nomor'] = $this->view->escape(strip_tags($formData['applicant_nomor']));
            $formData['Sorting'] = $this->view->escape(strip_tags($formData['Sorting']));

            if ($form->isValid($formData)) {

                $form->populate($formData);
                $this->view->form = $form;

                $session->result = $formData;

                //get OFFER applicants

                $applicants = $applicantDB->getUnRegisteredApplicant($formData);
                $this->view->results = $applicants;

                //get student type info
                $definationDb = new App_Model_General_DbTable_Definationms();
                $this->view->student_type = $definationDb->getDataByType(142);


                //echo '<pre>';
                //print_r($applicants);
                //echo '</pre>';


                //get current and future intake list
                //$intakeDB = new App_Model_Record_DbTable_Intake();
                //$this->view->intake_list = $intakeDB->getData();

            }

        } else {

            //populate by session
            if (isset($session->result)) {
                $form->populate($session->result);
                $applicants = $applicantDB->getUnRegisteredApplicant($session->result);
                $this->view->results = $applicants;

                //get current and future intake list
                //$intakeDB = new App_Model_Record_DbTable_Intake();
                //$this->view->intake_list = $intakeDB->getData();


                //get student type info
                $definationDb = new App_Model_General_DbTable_Definationms();
                $this->view->student_type = $definationDb->getDataByType(142);

            }
        }

    }


    public function registerAction()
    {

        set_time_limit(600); //10 minutes
        ini_set('post_max_size', '2M');


        $this->view->title = $this->view->translate("New Student Registration");

        $db = Zend_Db_Table::getDefaultAdapter();

        $auth = Zend_Auth::getInstance();

        if ($this->_request->isPost()) {

            $formdata = $this->_request->getPost();

            $txnDB = new Registration_Model_DbTable_ApplicantTransaction();
            $profileDB = new Application_Model_DbTable_ApplicantProfile();
            $systemErrorDB = new App_Model_General_DbTable_SystemError();
            $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
            $invoiceDB = new Studentfinance_Model_DbTable_Invoice();
            $registrationItemDB = new Registration_Model_DbTable_RegistrationItem();

            $stack_failed = array();
            $stack_student = array();


            for ($i = 0; $i < count($formdata["txnId"]); $i++) {

                $transaction_id = $formdata["txnId"][$i];
                $appl_id = $formdata["appl_id"][$transaction_id];

                //get profile data
                $profile = $profileDB->getData($appl_id);

                $stack_student = array('txn_id' => $transaction_id, 'name' => $profile['appl_fname'] . ' ' . $profile['appl_lname']);

                //check if transaction id already exist
                $already_registered = $studentRegistrationDb->checkIfExist($transaction_id);

                if ($already_registered) {

                    throw new exception('Transaction ID already exist');
                    $stack_student['status'] = 'Fail';
                    $stack_student['function'] = 'Checking transaction exist';
                    $stack_student['error_message'] = 'Transaction ID already exist';

                } else {

                    //get transaction data
                    $transactionData = $txnDB->getTransactionProfileData($transaction_id);


                    /* --------------------------------------
                     * START GENERATE REGISTRATION ID SECTION
                     * --------------------------------------
                     * */

                    //check previous student ID
                    if ($transactionData['prev_student'] != 0 && $transactionData['prev_studentID'] != '') {

                        //check if previous student ID is valid
                        $isValid = $txnDB->fncheckUniqueRegID($transactionData['prev_studentID']);

                        if ($isValid) {

                            if ($isValid['profileStatus'] == 92) {//Active not allow to register

                                //cannot register previous student is active
                                $stack_student['status'] = 'Fail';
                                $stack_student['function'] = 'Checking Active program';
                                $stack_student['error_message'] = 'Cannot register.Student already have an Active program';

                            } else {
                                $myRegistrationId = $transactionData['prev_studentID'];
                            }


                        } else {

                            //cannot register previous student ID does not exist
                            $stack_student['status'] = 'Fail';
                            $stack_student['function'] = 'Checking valid previous student ID';
                            $stack_student['error_message'] = 'Cannot register. Previous student ID ' . $transactionData['prev_studentID'] . ' not exist';
                        }

                    } else {

                        try {

                            $myRegistrationId = $this->generateRegistrationID($formdata['student_type'], $formdata['IdIntake'][$transaction_id]);

                        } catch (Exception $eg) {

                            //throw new exception('Fail generating Student ID');
                            $stack_student['status'] = 'Fail';
                            $stack_student['function'] = 'Generate Student ID';
                            $stack_student['error_message'] = $eg->getMessage();
                        }

                    }

                    /* ------------------------------------
                     * END GENERATE REGISTRATION ID SECTION
                     * ------------------------------------
                     * */


                    if (isset($myRegistrationId)) {

                        /* ---------------------------------
                         * START SAVE DATA
                         * ---------------------------------
                         * */


                        /* Migrate Data From applicant_profile to student_profile */


                        if (!$profile) {

                            throw new exception('There is no profile data for applicant ID: ' . $appl_id);

                        } else {

                            $db->beginTransaction();

                            try {
                                $IdStudentRegistration = $this->saveData($myRegistrationId, $transaction_id, $appl_id, $transactionData, $profile, $formdata, $stack_student);

                                if ($IdStudentRegistration) {

                                    $stack_student['status'] = 'Success';
                                }

                                $db->commit();


                            } catch (Exception $e) {

                                //echo $e->getMessage();
                                $db->rollBack();

                                $stack_student['status'] = 'Fail';
                                $stack_student['function'] = 'Save Data';
                                $stack_student['error_message'] = $e->getMessage();

                                $error['se_txn_id'] = $transaction_id;
                                $error['se_IdStudentRegistration'] = 0;
                                $error['se_IdStudentRegSubjects'] = 0;
                                $error['se_title'] = 'New Student Registration';
                                $error['se_message'] = $e->getMessage();
                                $error['se_createddt'] = date("Y-m-d H:i:s");
                                $error['se_createdby'] = $auth->getIdentity()->id;
                                $systemErrorDB->addData($error);

                            }//end try catch


                            if (isset($IdStudentRegistration) && $IdStudentRegistration != '') {

                                $stack_student['status'] = 'Success';

                                /* ---------------------------------------------------------
                                 *   check if student go for initial pre-reg paper section
                                 *
                                 *   comment coding for cater migration on 28/05/2015
                                  ----------------------------------------------------------*/

//															if($transactionData['MigrateCode']!='' && $formdata['IdProgram'][$transaction_id]==2){ //MIFP ONLY

                                //get pre reg subject according to landsape
//                                $prereg_subject_list = $this->initialRegPapers($transactionData, $formdata['IdProgram'][$transaction_id]);

                                //register 1st semester subjects
                                $prereg_subject_list = $this->subjectBySemesterLandscape($transactionData, $formdata['IdProgram'][$transaction_id]);

                                if (count($prereg_subject_list) > 0) {

                                    //get item for each subject
                                    $prereg_subject_list = $registrationItemDB->getSubjectItems($formdata, $transaction_id, $prereg_subject_list);

                                    //register subject
                                    $this->register_prereg_subject($IdStudentRegistration, $transaction_id, $prereg_subject_list, $formdata);

                                    //generate invoice
                                    $this->generateInvoice($IdStudentRegistration, $transaction_id, $prereg_subject_list, $formdata);


                                }

                                //todo tagging scholarship and sponsorship if apply and accept
                                $this->scholarshipTag($transaction_id, $IdStudentRegistration);

                                /*}else{

                                    //get pre reg subject invoice based
                                     $prereg_subject_list = $invoiceDB->getPreregSubject($transaction_id);

                                    if(count($prereg_subject_list)>0){
                                        $this->register_prereg_subject($IdStudentRegistration,$transaction_id,$prereg_subject_list,$formdata);
                                     }else{
                                         //get pre reg subject proforma based where invoice status=W=>waive @ E=>exempted and fee_category=8
                                         $preregister_subject_list = $invoiceDB->getPreregisterSubject($transaction_id);

                                         if(count($preregister_subject_list)>0){
                                             $this->register_prereg_subject($IdStudentRegistration,$transaction_id,$preregister_subject_list,$formdata);
                                         }
                                     }
                                }*/

                                /* ---------------------------------------------------------
                                *   end check if student go for initial pre-reg paper section
                                 ----------------------------------------------------------*/


                            }

                        }//end else profile

                    } //end else

                }//end already registered
                array_push($stack_failed, $stack_student);

            } //end foreach

            //$this->_helper->flashMessenger->addMessage(array('success' => "Student registration completed"));

            if (count($stack_failed) > 0) {
                //echo '<pre>';
                //print_r($stack_failed);
                $this->view->stack_failed = $stack_failed;
            } else {
                $this->_redirect($this->view->url(array('module' => 'registration', 'controller' => 'studentregistrationbulk', 'action' => 'index'), 'default', true));
            }

        }//end post
    }

    public function fnCreateRandPassword($length)
    {
        $chars = "234567890abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $i = 0;
        $password = "";
        while (strlen($password) < $length) {
            @$password .= $chars{mt_rand(0, strlen($chars))};
            $i++;
        }
        return $password;

    }

    public function getProgramSchemeAction()
    {
        $idProgram = $this->_getParam('idProgram', 0);

        //if ($this->getRequest()->isXmlHttpRequest()) {
        $this->_helper->layout->disableLayout();
        //}

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $programDb = new Registration_Model_DbTable_Program();
        $result = $programDb->getProgramSchemeList($idProgram);

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }


    private function sendEmail($status, $sp_id, $pwds)
    {

        $cmsTags = new Cms_TemplateTags();

        $registry = Zend_Registry::getInstance();
        $locale = $registry->get('Zend_Locale');

        $studentDB = new Records_Model_DbTable_Studentprofile();
        $student = $studentDB->getProfile($sp_id);

        $templateDB = new App_Model_General_DbTable_EmailTemplate();
        $templateData = $templateDB->getEmailTemplate($status, $student['IdProgramScheme'], $student['appl_category']);

        if ($templateData) {

            if ($locale == 'ms_MY') {
                $templateMail = $templateData['contentMy'];
            } else {
                $templateMail = $templateData['contentEng'];
            }

            $templateMail = $cmsTags->parseTag($sp_id, $templateData['stp_tplId'], $templateMail);
            $templateMail = str_replace("[Student Password]", $pwds, $templateMail);


            $email_receipient = $student["appl_email"];
            $email_personal_receipient = $student["appl_email_personal"];
            $name_receipient = $student["appl_fname"] . ' ' . $student["appl_lname"];

            $emailDb = new App_Model_System_DbTable_Email();

            //email personal
            $data_inceif = array(
                'recepient_email'     => $email_personal_receipient,
                'subject'             => $templateData["stp_title"],
                'content'             => $templateMail,
                'attachment_path'     => null,
                'attachment_filename' => null
            );

            //add email to email que
            $email_id2 = $emailDb->addData($data_inceif);

            //get the email attachment
            //$attachment = $templateDB->getEmailAttachment($templateData['stp_Id'],$locale='en_US');

            //if attachment exist
            /*if(count($attachment)>0){
                $attachment_info = $this->generateAttachment($attachment,$student);

                if($attachment_info){
                    $auth = Zend_Auth::getInstance();
                    foreach($attachment_info as $info){

                        $emailDb = new App_Model_System_DbTable_Email();
                        $data_attachment = array(
                                        'eqa_emailque_id' => $email_id,
                                        'eqa_filename' => $info['filename'],
                                        'eqa_path' => $info['path'],
                                        'eqa_updDate' => date('Y-m-d H:i:s'),
                                        'eqa_updUser' => $auth->getIdentity()->appl_id
                        );

                        //add attachment
                        $emailDb->addAttachmentData($data_attachment);

                    }//end if foreach
                }//end if attachment info
            }*/

        }//end if email template exist
    }


    function generateAttachment($attachment_list, $student)
    {

        //print_r($applicant);exit;
        $cmsTags = new Cms_TemplateTags();

        $attachment_info = array();

        require_once '../library/dompdf/dompdf_config.inc.php';

        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        $dompdf = new DOMPDF();


        foreach ($attachment_list as $index => $attachment) {

            $template = $attachment['tpl_content'];
            $template = $cmsTags->parseContent($student['transaction_id'], $attachment['tpl_id'], $template);

            $dompdf->load_html($template);
            $dompdf->set_paper('a4', 'potrait');
            $dompdf->render();

            //output filename
            $attachment_filename = $student["id"] . '_' . $attachment['tpl_pname'] . ".pdf";

            //untuk stream ke student
            //$dompdf->stream($attachment_filename);

            //untuk save ke db
            $pdf = $dompdf->output();

            //$location_path
            $location_path = $student['sp_repository'];

            //output_directory_path
            $output_directory_path = DOCUMENT_PATH . "/" . $location_path;

            //to rename output file
            $output_file_path = $output_directory_path . "/" . $attachment_filename;

            file_put_contents($output_file_path, $pdf);

            $info['filename'] = $attachment_filename;
            $info['path'] = $location_path . $attachment_filename;
            array_push($attachment_info, $info);

        }//end foreach

        return $attachment_info;
    }


    public function generateRegistrationID($student_type, $intake)
    {

        $generateLib = new icampus_Function_General_Format();
        $myRegistrationId = $generateLib->generateStudentID($student_type, $intake);

        //check unique ID
        $txnDB = new Registration_Model_DbTable_ApplicantTransaction();
        $exist = $txnDB->fncheckUniqueRegID($myRegistrationId);

        if ($exist) {
            $this->generateRegistrationID($student_type, $intake);
        } else {
            return $myRegistrationId;
        }

    }

    public function saveData($myRegistrationId, $transaction_id, $appl_id, $transaction, $profile, $formdata, $stack_student)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $auth = Zend_Auth::getInstance();

        $passwordstudent = $this->fnCreateRandPassword(8);

        //add student profile data
        $profile['appl_username'] = $myRegistrationId;
        $profile['appl_password'] = md5($passwordstudent);
        $profile["migrate_date"] = date('Y-m-d H:i:s');
        $profile["migrate_by"] = $auth->getIdentity()->iduser;
        $profile['appl_email_personal'] = $profile['appl_email'];
        $profile['appl_email'] = $myRegistrationId . '@student.inceif.org';

        $studentDB = new Records_Model_DbTable_Studentprofile();
        $student_id = $studentDB->addData($profile);


        // --------- create repository folder (student)	---------
        $student_path = DOCUMENT_PATH . '/student/' . date('Ym');

        //create directory to locate fisle

        if (!is_dir($student_path)) {
            mkdir($student_path, 0775);
        }

        $txn_path = $student_path . "/" . $student_id;

        //create directory to locate file
        if (!is_dir($txn_path)) {
            mkdir($txn_path, 0775);
        }

        //update repository info
        $rep['sp_repository'] = '/student/' . date('Ym') . '/' . $student_id;

        $studentDB->updateData($rep, $student_id);
        /* --------- end create directory --------*/


        //-------- migrate applicant section data & documents to student ----------
        $stack_migration = array();
        $sectionDB = new App_Model_General_DbTable_ApplicationSection();
        $section_list = $sectionDB->getSectionList();

        foreach ($section_list as $index => $section) {
            //@IMPORTANT : please exclude applicant_profile & applicant_program tables
            if ($section['student_table_name'] != '') {
                try {
                    $sectionDB->migrateSectionData($student_id, $transaction_id, $section, $stack_student);
                } catch (Exception $esd) {
                    array_push($stack_migration, array('function' => 'Migrate Section Data', 'error_message' => $esd->getMessage()));
                }
            }
        }
        $stack_student['migration'] = $stack_migration;

        $sectionDB->migrateDocuments($transaction_id, $student_id, $transaction['at_repository'], $rep['sp_repository']);
        //--------end migrate---------


        //---------- insert into tbl_studentregistration	---------

        $data['IdProgram'] = $formdata['IdProgram'][$transaction_id];
        $data['IdProgramScheme'] = $formdata['IdProgramScheme'][$transaction_id];
        $data['IdLandscape'] = $formdata['IdLandscape'][$transaction_id];
        $data['IdSemestersyllabus'] = $formdata['IdSemester'][$transaction_id];
        $data['IdSemester'] = $formdata['IdSemester'][$transaction_id];
        $data['IdSemesterMain'] = $formdata['IdSemester'][$transaction_id];
        $data['IdSemesterDetails'] = $formdata['IdSemester'][$transaction_id];
        $data['IdBranch'] = $formdata['IdBranch'][$transaction_id];
        $data['IdIntake'] = $formdata['IdIntake'][$transaction_id];

        if ($transaction['at_fs_id']) {
            $fs_id = $transaction['at_fs_id'];
        } else {
            $feeStructureDb = new Studentfinance_Model_DbTable_FeeStructure();
            $feeStructureData = $feeStructureDb->getApplicantFeeStructure($formdata['IdProgram'][$transaction_id], $formdata['IdProgramScheme'][$transaction_id], $formdata['IdIntake'][$transaction_id], $transaction['appl_category']);

            $fs_id = $feeStructureData['fs_id'];
        }

        $data['fs_id'] = $fs_id;
        $data['sp_id'] = $student_id;
        $data["IdApplication"] = $appl_id;
        $data["transaction_id"] = $transaction_id;
        $data['registrationId'] = $myRegistrationId;
        $data['Semesterstatus'] = 0;
        $data['student_type'] = $formdata['student_type']; //Normal or visiting
        $data['senior_student'] = 0;

        if ($data['IdLandscape'] == "") {
            $data['IdLandscape'] = 0;
        }

        if ($data['IdSemestersyllabus'] == "") {
            $data['IdSemestersyllabus'] = 0;
        }

        if ($data['IdSemesterMain'] == "") {
            $data['IdSemesterMain'] = 0;
        }

        if ($data['IdSemesterDetails'] == "") {
            $data['IdSemesterDetails'] = 0;
        }


        $data['profileStatus'] = 92; //tbl_definationms idDefType = 20 ( Student Status )
        $data['UpdUser'] = $auth->getIdentity()->iduser;
        $data['UpdDate'] = date('Y-m-d H:i:s');


        $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
        $IdStudentRegistration = $studentRegistrationDb->addData($data);
        //---------- end insert into tbl_studentregistration	---------

        //update finance info
        $financeDB = new Studentfinance_Model_DbTable_InvoiceMain();

        //invoice_main
        $financeDB->updateTableData('invoice_main', array('IdStudentRegistration' => $IdStudentRegistration), "trans_id='" . $transaction_id . "'");

        //advance_payment
        $financeDB->updateTableData('advance_payment', array('advpy_idStudentRegistration' => $IdStudentRegistration), "advpy_trans_id='" . $transaction_id . "'");

        //receipt
        $financeDB->updateTableData('receipt', array('rcp_idStudentRegistration' => $IdStudentRegistration), "rcp_trans_id='" . $transaction_id . "'");

        //credit note
        $financeDB->updateTableData('credit_note', array('cn_IdStudentRegistration' => $IdStudentRegistration), "cn_trans_id='" . $transaction_id . "'");


        //tag sponsorship
        /*$checkScholarship = $studentRegistrationDb->checkScholarshipApplication($transaction_id);

        if ($checkScholarship){
            $scholarData = array(
                'sa_af_id'=>$checkScholarship['af_id'],
                'sa_scholarship_type'=>$checkScholarship['sct_schId'],
                'sa_trans_id'=>$transaction_id,
                'sa_cust_id'=>$IdStudentRegistration,
                'sa_cust_type'=>1,
                'sa_status'=>2,
                'sa_semester_id'=>null,
                'sa_score'=>null,
                'sa_validated_doc'=>null,
                'sa_start_date'=>$checkScholarship['sa_start_date'],
                'sa_end_date'=>$checkScholarship['sa_end_date'],
                'sa_createddt'=>date('Y-m-d H:i:s'),
                'sa_createdby'=>$auth->getIdentity()->iduser,
                'utilized_amount'=>0.00
            );
            $studentRegistrationDb->tagStudentScholarship($scholarData);
        }*/


        if ($IdStudentRegistration) {

            $data = array(
                "Users" => array(
                    array(
                        'role'      => 5,
                        'username'  => $myRegistrationId,
                        'password'  => $passwordstudent,
                        'firstname' => $profile['appl_fname'],
                        'lastname'  => $profile['appl_lname'],
                        'email'     => $myRegistrationId . '@student.inceif.org'
                    )
                )
            );

            $data1 = array(
                'username' => $myRegistrationId

            );

            $loginMoodle = '/data/htdocs/moodle/login.php';
            $moodleStatus = true;
            if (defined('MOODLE_LOGIN') && MOODLE_LOGIN == 1 && $moodleStatus == true) {
                if (defined('MOODLE_SYNC')) {

                    $response = push_to_moodle('CheckUser', $data1);

                    //dd($response); exit;

                    $Xml = new SimpleXmlElement($response);

                    if ($Xml) {
                        if ($Xml->Message == 'User does not exist!') {

                            push_to_moodle('CreateUser', $data);
                        }
                    }


                    //KMC
                    //get profile student
                    $studentprofileDB = new Records_Model_DbTable_Studentprofile();
                    $profile = $studentprofileDB->getStudentData($IdStudentRegistration);

                    $dob = explode('-', $profile['appl_dob']);

                    if (strlen($dob[0]) == 2) {
                        $mydob = $dob[2] . '-' . $dob[1] . '-' . $dob[0];
                    } else {
                        $mydob = $profile['appl_dob'];
                    }

                    $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                    $invoiceAmount = $invoiceClass->getAmountResourceFee($IdStudentRegistration);

                    if ($profile['appl_idnumber_type'] == 487) {
                        $idtype = 'mykad';
                    } else if ($profile['appl_idnumber_type'] == 487) {
                        $idtype = 'passport';
                    } else {
                        $idtype = 'others';
                    }

                    $kmc_data = array(
                        'username'                => $myRegistrationId,
                        'password'                => $passwordstudent,
                        'email'                   => $myRegistrationId . '@student.inceif.org',
                        'name'                    => $profile['appl_fname'] . ' ' . $profile['appl_lname'],
                        'status'                  => 'active',
                        'status_message'          => '',
                        'religion'                => $profile['religionName'],
                        'nationality'             => $profile['nationality'],
                        ///'identity_type'=>$profile['IdTypeName'],
                        'identity_type'           => $idtype,
                        'identity_number'         => $profile['appl_idnumber'],
                        'date_of_birth'           => $mydob,
                        'study_start'             => $profile['SemesterMainStartDate'],
                        'study_end'               => '',
                        'permanent_address'       => $profile['appl_address1'] . ' ' . $profile['appl_address2'] . ' ' . $profile['appl_address3'],
                        'permanent_city'          => $profile['PCityName'],
                        'permanent_postcode'      => $profile['appl_postcode'],
                        'permanent_state'         => $profile['PStateName'],
                        'permanent_country'       => $profile['PCountryName'],
                        'correspondence_address'  => $profile['appl_caddress1'] . ' ' . $profile['appl_caddress2'] . ' ' . $profile['appl_caddress3'],
                        'correspondence_city'     => $profile['CCityName'],
                        'correspondence_postcode' => $profile['appl_cpostcode'],
                        'correspondence_state'    => $profile['CStateName'],
                        'correspondence_country'  => $profile['CCountryName'],
                        'contact_home'            => $profile['appl_contact_home'],
                        'contact_office'          => $profile['appl_contact_office'],
                        'contact_mobile'          => $profile['appl_contact_mobile'],
                        'program_name'            => $profile['ProgramCode'],
                        'program_code'            => $profile['ProgramCode'],
                        'program_type'            => $profile['pt'],
                        'program_resource_fee'    => $invoiceAmount['amount'],
                        'program_currency'        => $invoiceAmount['currency'] != null ? $invoiceAmount['currency'] : ''
                    );

                    $kmcDB = new Registration_Model_DbTable_Kmc();
                    $kmcDB->addKmc($kmc_data);

                    $ldap_data = array(
                        'username'     => $myRegistrationId,
                        'unicodePwd'   => $passwordstudent,
                        'mail'         => $myRegistrationId . '@student.inceif.org',
                        'name'         => $profile['appl_fname'] . ' ' . $profile['appl_lname'],
                        'givenName'    => $profile['appl_fname'] . ' ' . $profile['appl_lname'],
                        'sn'           => $profile['appl_lname'],
                        'account_type' => 'STUDENT',
                        'program_code' => $profile['ProgramCode'],
                        'intake_id'    => $profile['IntakeName'],
                        'award'        => $profile['awardName']
                    );

                    $LdapDB = new Registration_Model_DbTable_Ldap();
                    $LdapDB->addLdap($ldap_data);


                    //END KMC

                }
            }

            //MOODLE_SYNC check
        }

        //------add profile status history------
        $cmsStatus = new Cms_Status();
        $cmsStatus->addProfileStatusHistory($IdStudentRegistration, 92, $type = null);
        //------end update profile status history------


        //----------Update transaction table student registration status ------------

        $updtrans = array('at_status'                => 166,
                          'at_IdStudentRegistration' => $IdStudentRegistration,
                          'at_registration_status'   => 1,
                          'at_registration_date'     => date('Y-m-d H:i:s'));

        $transDB = new App_Model_Application_DbTable_ApplicantTransaction();
        $transDB->updateData($updtrans, $transaction_id);

        $checklistVerification = new Application_Model_DbTable_ChecklistVerification();
        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();

        $dataHistory = array(
            'ash_trans_id'   => $transaction_id,
            'ash_status'     => 166,
            'ash_oldStatus'  => 158,
            'ash_changeType' => 1,
            'ash_updUser'    => $getUserIdentity->id,
            'ash_userType'   => 1,
            'ash_updDate'    => date('Y-m-d H:i:s')
        );
        $checklistVerification->storeStatusUpdateHistory($dataHistory);

        //---------------- End  --------------


        /*send email*/
        $this->sendEmail($status = 166, $student_id, $passwordstudent);

        return $IdStudentRegistration;


        /* ---------------------------------
         * END SAVE DATA
         * ---------------------------------
         * */
    }


    public function register_prereg_subject($IdStudentRegistration, $transaction_id, $prereg_subject_list, $formData)
    {

        $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
        $studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
        $studentSemesterStatusDb = new Registration_Model_DbTable_Studentsemesterstatus();
        $historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
        $registrationItemDB = new Registration_Model_DbTable_RegistrationItem();
        $systemErrorDB = new App_Model_General_DbTable_SystemError();

        $auth = Zend_Auth::getInstance();
        $db = Zend_Db_Table::getDefaultAdapter();

        $db->beginTransaction();
        try {

            // If all succeed, commit the transaction and all changes
            // are committed at once.
            $IdSemester = $formData['IdSemester'][$transaction_id];
            $IdProgram = $formData['IdProgram'][$transaction_id];

            $register = false;

            foreach ($prereg_subject_list as $list) {
                $selectCheck = $db->select()
                    ->from(array('a' => 'tbl_studentregsubjects'))
                    ->where('a.IdStudentRegistration = ?', $IdStudentRegistration)
                    ->where('a.IdSemesterMain = ?', $IdSemester)
                    ->where('a.IdSubject = ?', $list['IdSubject']);

                $check = $db->fetchAll($selectCheck);

                if (!$check) {
                    if (count($list['item']) > 0) {

                        //set status
                        $register = true;

                        $idSubject = $list['subject_id'];

                        /*
                            1. checking invoice status = exempted, no need to registered
                            2. checking invoice initial fee - if paid, Active = 1;else Active = 0
                            3. generate invoice utk subject registered
                        */


                        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                        $statusInvoice = $invoiceClass->checkingInitialFee($transaction_id, $idSubject);

                        $invoiceId = $statusInvoice['invoice_id'];


                        //add table studentregsubject
                        $subject["IdStudentRegistration"] = $IdStudentRegistration;
                        $subject['initial'] = 1;
                        $subject["IdSubject"] = $idSubject;
                        $subject["IdSemesterMain"] = $IdSemester;
                        $subject["SemesterLevel"] = 1;
                        $subject["IdLandscapeSub"] = 0;
                        $subject["Active"] = $statusInvoice['Active'];
                        $subject["UpdDate"] = date('Y-m-d H:i:s');
                        $subject["UpdUser"] = $auth->getIdentity()->iduser;
                        $subject["IdCourseTaggingGroup"] = 0;
                        $IdStudentRegSubjects = $studentRegSubjectDB->addData($subject);


                        //add table studentregsubject audit trail/history
                        $subject2['IdStudentRegSubjects'] = $IdStudentRegSubjects;
                        $subject2["IdStudentRegistration"] = $IdStudentRegistration;
                        $subject2['initial'] = 1;
                        $subject2["IdSubject"] = $idSubject;
                        $subject2["IdSemesterMain"] = $IdSemester;
                        $subject2["SemesterLevel"] = 1;
                        $subject2["Active"] = $statusInvoice['Active'];
                        $subject2["UpdDate"] = date('Y-m-d H:i:s');
                        $subject2["UpdUser"] = $auth->getIdentity()->iduser;
                        $subject2["IdCourseTaggingGroup"] = 0;
                        $subject2["message"] = 'Enrollment : Register Initial Paper';
                        $subject2["createddt"] = date('Y-m-d H:i:s');
                        $subject2["createdby"] = $auth->getIdentity()->iduser;
                        $subject2["createdrole"] = 'admin';
                        $historyDB->addData($subject2);

                        $this->saveItem($IdStudentRegSubjects, $IdStudentRegistration, $idSubject, $IdSemester, $list, $invoiceId);

                    }//end exist item
                }
            }

            if ($register == true) {


                //  ---------------- update studentregistration table ----------------
                $data['Semesterstatus'] = 130; //Register
                $data['senior_student'] = 1; //Senior:yes

                $studentRegistrationDb->updateData($data, $IdStudentRegistration);
                //  ---------------- end update studentregistration table ----------------


                // ----------------  insert into student semester status ----------------

                $lsemstatusArr = array('IdStudentRegistration' => $IdStudentRegistration,
                                       'idSemester'            => $IdSemester,
                                       'IdSemesterMain'        => $IdSemester,
                                       'studentsemesterstatus' => 130,    //Register idDefType = 32 (student semester status)
                                       'Level'                 => 1,
                                       'UpdDate'               => date('Y-m-d H:i:s'),
                                       'UpdUser'               => $auth->getIdentity()->iduser
                );
                $studentSemesterStatusDb->addData($lsemstatusArr);

                // ----------------  end insert into student semester ----------------

            }//end register true

            $db->commit();

        } catch (PDOException $e) {
            // If any of the queries failed and threw an exception,
            // we want to roll back the whole transaction, reversing
            // changes made in the transaction, even those that succeeded.
            // Thus all changes are committed together, or none are.
            $db->rollBack();

            $error['se_txn_id'] = $transaction_id;
            $error['se_IdStudentRegistration'] = 0;
            $error['se_IdStudentRegSubjects'] = 0;
            $error['se_title'] = 'Student Registration : Add Item';
            $error['se_message'] = $e->getMessage();
            $error['se_createddt'] = date("Y-m-d H:i:s");
            $error['se_createdby'] = $auth->getIdentity()->id;
            $systemErrorDB->addData($error);
        }


    }

    function generateInvoice($IdStudentRegistration, $transaction_id, $prereg_subject_list, $formData)
    {

        $systemErrorDB = new App_Model_General_DbTable_SystemError();

        $auth = Zend_Auth::getInstance();
        $db = Zend_Db_Table::getDefaultAdapter();

        $db->beginTransaction();
        try {

            // If all succeed, commit the transaction and all changes
            // are committed at once.
            $IdSemester = $formData['IdSemester'][$transaction_id];

            foreach ($prereg_subject_list as $list) {
                $selectCheck = $db->select()
                    ->from(array('a' => 'tbl_studentregsubjects'))
                    ->where('a.IdStudentRegistration = ?', $IdStudentRegistration)
                    ->where('a.IdSemesterMain = ?', $IdSemester)
                    ->where('a.IdSubject = ?', $list['IdSubject']);

                $check = $db->fetchAll($selectCheck);

                if (count($list['item']) > 0) {


                    $idSubject = $list['subject_id'];

                    /*
                        1. generate invoice utk subject registered
                    */

                    $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                    $invoiceClass->generateInvoiceStudent($IdStudentRegistration, $IdSemester, $idSubject, 0, $list['CreditHours'], 0, 0);

                }//end exist item
            }

            $db->commit();

        } catch (PDOException $e) {
            // If any of the queries failed and threw an exception,
            // we want to roll back the whole transaction, reversing
            // changes made in the transaction, even those that succeeded.
            // Thus all changes are committed together, or none are.
            $db->rollBack();

            $error['se_txn_id'] = $transaction_id;
            $error['se_IdStudentRegistration'] = 0;
            $error['se_IdStudentRegSubjects'] = 0;
            $error['se_title'] = 'Student Registration : Generate Invoice';
            $error['se_message'] = $e->getMessage();
            $error['se_createddt'] = date("Y-m-d H:i:s");
            $error['se_createdby'] = $auth->getIdentity()->id;
            $systemErrorDB->addData($error);
        }


    }

    function saveItem($IdStudentRegSubjects, $IdStudentRegistration, $idSubject, $IdSemester, $list, $invoiceId = NULL)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $auth = Zend_Auth::getInstance();

        foreach ($list['item'] as $det) {

            /*
             * comment utk cater
             * if($det['fi_id'] == 89){ //exam
                $item = 879;
            }elseif($det['fi_id'] == 90){ //paper
                $item = 890;
            }*/

            if ($det['item_id']) {
                $item = $det['item_id'];
            }

            //check for duplicate entry
            $select = $db->select()
                ->from(array('a' => 'tbl_studentregsubjects_detail'))
                ->where('a.regsub_id = ?', $IdStudentRegSubjects)
                ->where('a.item_id = ?', $item);

            $resultsa = $db->fetchRow($select);

            if (!$resultsa) {
                $dataitem = array(
                    'student_id'   => $IdStudentRegistration,
                    'regsub_id'    => $IdStudentRegSubjects,
                    'semester_id'  => $IdSemester,
                    'subject_id'   => $idSubject,
                    'item_id'      => $item,
                    'section_id'   => 0,
                    'invoice_id'   => $invoiceId,
                    'ec_country'   => 0,
                    'ec_city'      => 0,
                    'created_date' => date('Y-m-d H:i:s'),
                    'created_by'   => $auth->getIdentity()->iduser,
                    'created_role' => 'admin',
                    'ses_id'       => 0
                );


                //$db->insert('tbl_studentregsubjects_detail', $data);
                $regSubjectItem = new Registration_Model_DbTable_StudentRegistrationItem();
                $idRegSubjectItem = $regSubjectItem->addData($dataitem);

                $dataitem['id'] = $idRegSubjectItem;
                $dataitem["message"] = 'Student Course Regisration: Add Item';
                $dataitem['createddt'] = date("Y-m-d H:i:s");
                $dataitem['createdby'] = $auth->getIdentity()->id;

                $db->insert('tbl_studentregsubjects_detail_history', $dataitem);

                //exam registration
                if ($item == 879) {
                    $this->saveExamRegistration($IdSemester, $idSubject, $IdStudentRegistration);
                }
            }

        }//end foreach
    }

    function initialRegPapers($txnData)
    {

        $landscapeDB = new App_Model_General_DbTable_Landscape();
        $applicantProgramDB = new App_Model_Application_DbTable_ApplicantProgram();

        $program = $applicantProgramDB->getProgrambyTxn($txnData['at_trans_id']);

        $subject_list = $landscapeDB->getLandscapeSubject($program['ap_prog_id'], $program['ap_prog_scheme'], $txnData['at_intake']);
        $landscapeInfo = $landscapeDB->getLandscapeInfo($program['ap_prog_id'], $program['ap_prog_scheme'], $txnData['at_intake']);

        $table = '';
        $subject_list2 = array();

        if (!empty($landscapeInfo)) {
            $tagInfo = $landscapeDB->getLandscapeSubjectTag($landscapeInfo['IdLandscape']);
            $tagEncode = json_decode($tagInfo['subjectdata'], true);

            $intakeInfo = $landscapeDB->getIntakeInfo($txnData['at_intake']);
            $semInfo = $landscapeDB->getSemesterInfo($intakeInfo['sem_year'], $intakeInfo['sem_seq']);

            $i = 0;

            if (count($subject_list) > 0) {
                foreach ($subject_list as $loop) {
                    if (isset($tagEncode[$loop['IdSubject']])) {
                        //var_dump($tagEncode[$loop['IdSubject']]);
                        if (isset($tagEncode[$loop['IdSubject']][$semInfo['SemesterType']])) {
                            $loop['subject_id'] = $loop['IdSubject'];
                            $subject_list2[$i] = $loop;
                            $i++;
                        }
                    }
                }
                return $subject_list2;
            }

        }//end

    }//end function

    function subjectBySemesterLandscape($txnData, $transId = 0, $semesterNo = 1)
    {

        $landscapeDB = new App_Model_General_DbTable_Landscape();
        $applicantProgramDB = new App_Model_Application_DbTable_ApplicantProgram();

        $program = $applicantProgramDB->getProgrambyTxn($txnData['at_trans_id']);

        $subject_list = $landscapeDB->getLandscapeSubject($program['ap_prog_id'], $program['ap_prog_scheme'], $txnData['at_intake'], $semesterNo);

        return $subject_list;
        //$landscapeInfo = $landscapeDB->getLandscapeInfo($program['ap_prog_id'], $program['ap_prog_scheme'], $txnData['at_intake']);

        /*$table = '';
        $subject_list2 = array();

        if (!empty($landscapeInfo)) {
            $tagInfo = $landscapeDB->getLandscapeSubjectTag($landscapeInfo['IdLandscape']);
            $tagEncode = json_decode($tagInfo['subjectdata'], true);

            $intakeInfo = $landscapeDB->getIntakeInfo($txnData['at_intake']);
            $semInfo = $landscapeDB->getSemesterInfo($intakeInfo['sem_year'], $intakeInfo['sem_seq']);

            $i = 0;

            if (count($subject_list) > 0) {
                foreach ($subject_list as $loop) {
                    if (isset($tagEncode[$loop['IdSubject']])) {
                        //var_dump($tagEncode[$loop['IdSubject']]);
                        if (isset($tagEncode[$loop['IdSubject']][$semInfo['SemesterType']])) {
                            $loop['subject_id'] = $loop['IdSubject'];
                            $subject_list2[$i] = $loop;
                            $i++;
                        }
                    }
                }
                return $subject_list2;
            }

        }//end*/

    }//end function


    public function saveExamRegistration($IdSemester, $idSubject, $IdStudentRegistration)
    {

        $auth = Zend_Auth::getInstance();
        $db = Zend_Db_Table::getDefaultAdapter();
        $examRegDB = new Registration_Model_DbTable_ExamRegistration();

        $dataer['er_idStudentRegistration'] = $IdStudentRegistration;
        $dataer['er_idSemester'] = $IdSemester;
        $dataer['er_idProgram'] = 0;
        $dataer['er_idSubject'] = $idSubject;
        $dataer['er_idCountry'] = 0;
        $dataer['er_idCity'] = 0;
        $dataer['er_status'] = 764; //764:Regiseterd
        $dataer['er_createdby'] = $auth->getIdentity()->iduser;
        $dataer['er_createddt'] = date('Y-m-d H:i:s');

        //check if exist
        //list course
        $sql = $db->select()
            ->from(array('er' => 'exam_registration'))
            ->where('er.er_idSemester = ?', $IdSemester)
            ->where('er.er_idSubject = ?', $idSubject)
            ->where('er.er_idStudentRegistration = ?', $IdStudentRegistration);
        $examreg = $db->fetchRow($sql);

        if (!$examreg) {
            $examRegDB->addData($dataer);
        }


    }//end add exam center

    public function scholarshipTag($transid, $studentid)
    {
        //$transid = $this->_getParam('id');

        $auth = Zend_Auth::getInstance();
        $model = new Registration_Model_DbTable_ScholarshipTag();

        //check scholarship
        $scholarship = $model->getAcceptedScholarship($transid);

        if ($scholarship) {
            $studentInfo = $model->getStudentInfo($studentid);

            $semester = $model->getSemester($studentInfo['IdSemesterMain']);

            if ($semester) {
                $date = date('Y-m-d', strtotime($semester['SemesterMainStartDate']));
            } else {
                $date = null;
            }

            $data = array(
                'sa_af_id'            => $scholarship['sa_af_id'],
                'sa_scholarship_type' => $scholarship['sch_Id'],
                'sa_cust_id'          => $studentid,
                'sa_cust_type'        => 1,
                'sa_status'           => 2,
                /*'sa_start_date'=>$date,
                'sa_end_date'=>null,*/
                'sa_start_date'       => $scholarship['sa_start_date'],
                'sa_end_date'         => $scholarship['sa_end_date'],
                'sa_createddt'        => date('Y-m-d H:i:s'),
                'sa_createdby'        => $auth->getIdentity()->iduser
            );
            $tagid = $model->insertScholarshipTag($data);

            return $tagid;
        } else {
            return false;
        }
    }

    public function insertPrsNotDoneAction()
    {
        $txnDB = new Registration_Model_DbTable_ApplicantTransaction();
        $registrationItemDB = new Registration_Model_DbTable_RegistrationItem();

        $regId = array(
            '1600109',
            '1600110',
            '1600111',
            '1600112',
            '1600113',
            '1600114',
            '1600115',
            '1600123',
            '1600125',
            '1600124'
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $studentSelect = $db->select()
            ->from(array('a' => 'tbl_studentregistration'))
            ->where('a.registrationId IN (?)', $regId);

        $studentList = $db->fetchAll($studentSelect);

        if ($studentList) {
            foreach ($studentList as $studentLoop) {
                $transactionData = $txnDB->getTransactionProfileData($studentLoop['transaction_id']);

                $prereg_subject_list = $this->initialRegPapers($transactionData, $studentLoop['IdProgram']);

                $formdata = array();
                $formdata['IdSemester'][$studentLoop['transaction_id']] = $studentLoop['IdSemester'];
                $formdata['IdProgram'][$studentLoop['transaction_id']] = $studentLoop['IdProgram'];
                $formdata['IdProgramScheme'][$studentLoop['transaction_id']] = $studentLoop['IdProgramScheme'];

                if (count($prereg_subject_list) > 0) {

                    //get item for each subject
                    $prereg_subject_list = $registrationItemDB->getSubjectItems($formdata, $studentLoop['transaction_id'], $prereg_subject_list);

                    $this->register_prereg_subject($studentLoop['IdStudentRegistration'], $studentLoop['transaction_id'], $prereg_subject_list, $formdata);
                }
            }
        }
        exit;
    }
}

?>
