<?php
class Registration_RegistrationItemController extends Base_Base 
{	
	function init()
	{
		$this->regitemDB = new Registration_Model_DbTable_RegistrationItem();

	}
	public function indexAction() 
	{	
            $this->view->title = $this->view->translate("Registration Item Setup");

            if ($this->getRequest()->isPost()){
                $formData = $this->getRequest()->getPost();
                
                if (isset($formData['search'])){
                    $form = new Registration_Form_RegistrationItemSearch(array('programid'=>$formData['program']));
                    $p_data = $this->regitemDB->getPaginateData($formData);
                    $form->populate($formData);
                }else{
                    $form = new Registration_Form_RegistrationItemSearch();
                    $p_data = $this->regitemDB->getPaginateData();
                }
            }else{
                $form = new Registration_Form_RegistrationItemSearch();
                $p_data = $this->regitemDB->getPaginateData();
            }
            
            $this->view->form = $form;

            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
            $paginator->setItemCountPerPage($this->gintPageCount);
            $paginator->setCurrentPageNumber($this->_getParam('page',1));

            $this->view->paginator = $paginator;
	}

	public function addAction()
	{
		$this->view->title = $this->view->translate('Add New Registration Item');

		$form = new Registration_Form_RegistrationItem();

		$auth = Zend_Auth::getInstance();
		if ($this->_request->isPost()){
			
			$formData = $this->_request->getPost ();

			$data = array(
							'semester_id'		=> $formData['semester_id'],
							'program_id'		=> $formData['program_id'],
							'program_scheme_id'	=> $formData['program_scheme_id'],
							'item_id'			=> $formData['item_id'],
							'chargable'			=> $formData['chargable'],
							'mandatory'			=> $formData['mandatory'],
							'created_by'		=> $auth->getIdentity()->iduser,
							'created_date'		=> new Zend_Db_Expr('NOW()')
					);
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New Registration Item Added"));
			
			$this->regitemDB->addData($data);

			$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'registration-item', 'action'=>'index'),'default',true));	
		}
			
		
		//view
		$this->view->form = $form;
	}

	public function editAction()
	{
		$this->view->title = $this->view->translate('Edit Registration Item');
		
		$id = $this->_getParam('id');

		$info = $this->regitemDB->getDatabyId($id);
		$this->view->info = $info;

		$form = new Registration_Form_RegistrationItem();
		$form->populate($info);

		$auth = Zend_Auth::getInstance();
		if ($this->_request->isPost()){
			
			$formData = $this->_request->getPost ();

			$data = array(
							'semester_id'		=> $formData['semester_id'],
							'program_id'		=> $formData['program_id'],
							'program_scheme_id'	=> $formData['program_scheme_id'],
							'item_id'			=> $formData['item_id'],
							'chargable'			=> $formData['chargable'],
							'mandatory'			=> $formData['mandatory']
					);
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
			
			$this->regitemDB->updateData($data, $info['ri_id']);

			$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'registration-item', 'action'=>'edit', 'id' => $id ),'default',true));	
		}
			
		
		//view
		$this->view->form = $form;
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');

		$info = $this->regitemDB->getDatabyId($id);

		if ( empty($info) )
		{
			throw new Exception('Invalid Registration Item');
		}

		$this->regitemDB->deleteData($id);

		$this->_helper->flashMessenger->addMessage(array('success' => "Item successfully deleted"));

		$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'registration-item', 'action'=>'index' ),'default',true));	
	}
	
	public function indexActivityAction() 
	{	
		$this->view->title = $this->view->translate("Registration Item Activity Setup");
		$id = $this->_getParam('id');
		$this->view->id = $id;
		$p_data = $this->regitemDB->getPaginateDataActivity($id);
                $info = $this->regitemDB->activityInfo($id);
                $this->view->info = $info;
    	$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}
	
	public function addActivityAction()
	{
		$this->view->title = $this->view->translate('Add New Activity');
		
		$id = $this->_getParam('id');
		$this->view->id = $id;
		$form = new Registration_Form_RegistrationItemActivity();

		$auth = Zend_Auth::getInstance();
		if ($this->_request->isPost()){
			
			$formData = $this->_request->getPost ();

			$data = array(
							'ria_ri_id'		=> $id,
							'ria_activity_id'		=> $formData['ria_activity_id'],
							'ria_calculation_type'	=> $formData['ria_calculation_type'],
							'ria_amount'			=> $formData['ria_amount'],
							'created_by'		=> $auth->getIdentity()->iduser,
							'created_date'		=> new Zend_Db_Expr('NOW()')
					);
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New Registration Item Activity Added"));
			
			$this->regitemDB->addActivity($data);

			$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'registration-item', 'action'=>'index-activity','id'=>$id),'default',true));	
		}
			
		
		//view
		$this->view->form = $form;
	}
	
	public function deleteActivityAction()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');
		$ids = $this->_getParam('ids');

		$info = $this->regitemDB->getDatabyIdActivity($id);

		if ( empty($info) )
		{
			throw new Exception('Invalid Registration Item Activity');
		}

		$this->regitemDB->deleteActivity($id);

		$this->_helper->flashMessenger->addMessage(array('success' => "Activity successfully deleted"));

		$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'registration-item', 'action'=>'index-activity','id'=>$ids ),'default',true));	
	}
	
}

?>