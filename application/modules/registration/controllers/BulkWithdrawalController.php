<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 10/12/2015
 * Time: 9:15 AM
 */
class Registration_BulkWithdrawalController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Registration_Model_DbTable_BulkWithdrawal();
        $this->model2 = new GeneralSetup_Model_DbTable_ManageClassSchedule();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Bulk Withdrawal');

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $formData['scheme'] = $this->view->escape(strip_tags($formData['scheme']));
            $formData['semester'] = $this->view->escape(strip_tags($formData['semester']));
            $formData['subcode'] = $this->view->escape(strip_tags($formData['subcode']));
            $formData['coursename'] = $this->view->escape(strip_tags($formData['coursename']));

            $form = new Schedule_Form_ClassScheduleSearch(array('schemeid'=>$formData['scheme']));
            $form->populate($formData);
            $this->view->form = $form;

            $class = $this->model2->getClass($formData);
            $this->view->class = $class;
        }else{
            $form = new Schedule_Form_ClassScheduleSearch();
            $this->view->form = $form;
        }
    }

    public function clearAction(){
        $this->_redirect($this->baseUrl . '/registration/bulk-withdrawal/index');
        exit;
    }

    public function withdrawalAction(){
        $this->view->title = $this->view->translate('Bulk Withdrawal');

        $groupid = $this->_getParam('id',null);

        $auth = Zend_Auth::getInstance();
        $model = new GeneralSetup_Model_DbTable_Attendance();
        $registrationItemDB =new Registration_Model_DbTable_RegistrationItem();
        $studentRegSubDB = new Registration_Model_DbTable_Studentregsubjects();
        $studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
        $historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
        $courseGroupStudentDB = new GeneralSetup_Model_DbTable_CourseGroupStudent();
        $examRegDB = new Registration_Model_DbTable_ExamRegistration();
        $regSubjectItem = new Registration_Model_DbTable_StudentRegistrationItem();
        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
        $semesterDB = new Registration_Model_DbTable_Semester();

        $groupInfo = $model->getGoupInfo($groupid);
        $studentList = $model->getGroupStudent($groupid);

        if ($studentList){
            foreach ($studentList as $key => $value){
                $item_repeat = $registrationItemDB->getRepeatRegItems($value['IdProgram'], $groupInfo['IdSemester'], $value['IdProgramScheme'], $chargable=0);

                $item_register = $item_repeat;

                //check if Research set default item to paper
                if($groupInfo['CourseType']==3){
                    if($value['IdScheme']==1){	//PS
                        $item_register = array(array('mandatory'=>1,'item_name'=>'Paper','item_id'=>890));
                    }
                    if($value['IdScheme']==11){	//GS
                        $item_register = array(array('mandatory'=>1,'item_name'=>'Course','item_id'=>889));
                    }
                }

                foreach($item_register as $itemkey=>$i){

                    $item_register[$itemkey]['item_status'] = 0;//default

                    //get registration item status
                    $isRegisterItem = $studentRegSubDB->checkIsRegisterItem($value['IdStudentRegistration'], $groupInfo['IdSubject'],$groupInfo['IdSemester'],$i['item_id']);
                    if($isRegisterItem){

                        if($isRegisterItem['status']==3){
                            //withdraw
                            $item_register[$itemkey]['isRegisterItem']=0;
                            $item_register[$itemkey]['item_status']=3;

                        }else{
                            $item_register[$itemkey]['isRegisterItem']=1;
                            $item_register[$itemkey]['srsd_id']=$isRegisterItem['id'];
                        }
                    }else{
                        $item_register[$itemkey]['isRegisterItem']=0;
                    }
                }

                $studentList[$key]['item'] = $item_register;
            }
        }

        $this->view->groupInfo = $groupInfo;
        $this->view->studentList = $studentList;

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            //check is current semester?
            $isCurrentSemester = $semesterDB->isCurrentSemester($groupInfo['IdSemester']);

            $drop = false;
            $withdraw = false;

            if(!$isCurrentSemester){
                //can be previous or incoming mester
                //set default as withdraw subject
                $withdraw = true;
            }else{
                //get activity add & drop info
                $activity_drop = $studentRegSubjectDB->checkActivity($groupInfo['IdSemester']);

                $date_now = date('Y-m-d');
                $date_start_drop = $activity_drop['StartDate'];
                $date_last_drop = $activity_drop['EndDate'];

                if($date_now > $date_last_drop){
                    $withdraw = true;
                }else if ($date_now <= $date_last_drop) {
                    $drop = true;
                }

            }

            if (isset($formData['check']) && count($formData['check']) > 0){
                foreach ($formData['check'] as $sid => $studentLoop){
                    if (isset($formData['IdDropItem'][$sid]) && count($formData['IdDropItem'][$sid]) > 0){
                        foreach ($formData['IdDropItem'][$sid] as $itemLoop){
                            $srsd_id  = $itemLoop;
                            $refund  = $formData["refund"][$sid][$srsd_id];
                            $reg_detail = $regSubjectItem->getData($srsd_id);

                            if($reg_detail){
                                $initial_paper = $reg_detail['initial_paper'];
                                unset($reg_detail['initial_paper']);

                                //keep as history
                                if($withdraw==true){
                                    $reg_detail['status']=3;
                                    $reg_detail['refund']=$refund;
                                    $reg_detail['modifydt']=date("Y-m-d H:i:s");
                                    $reg_detail['modifyby']=$auth->getIdentity()->id;
                                    $regSubjectItem->updateData($reg_detail,$reg_detail['id']);

                                    $reg_detail['message']='Student Course Registration : Withdraw Item';
                                    $reg_detail['createddt']=date("Y-m-d H:i:s");
                                    $reg_detail['createdby']=$auth->getIdentity()->id;
                                    $reg_detail['createdrole']='admin';
                                    $regSubjectItem->addHistoryItemData($reg_detail);

                                }else{
                                    if($drop==true){
                                        $reg_detail['status']=2;
                                    }

                                    $reg_detail['refund']=$refund;
                                    $reg_detail['message']='Student Course Registration : Drop Item';
                                    $reg_detail['createddt']=date("Y-m-d H:i:s");
                                    $reg_detail['createdby']=$auth->getIdentity()->id;
                                    $reg_detail['createdrole']='admin';
                                    $regSubjectItem->addHistoryItemData($reg_detail);

                                    //drop item first
                                    $regSubjectItem->deleteData($srsd_id);
                                }

                                if($refund>0){
                                    if(isset($reg_detail["invoice_id"]) ){
                                        if($initial_paper==1){
                                            if($i==0){
                                                $invoiceClass->generateCreditNote($sid,$groupInfo['IdSubject'],$reg_detail['invoice_id'],$reg_detail['item_id'],$refund,0,$initial_paper);
                                            }
                                        }else{
                                            $invoiceClass->generateCreditNote($sid,$groupInfo['IdSubject'],$reg_detail['invoice_id'],$reg_detail['item_id'],$refund);
                                        }
                                    }
                                }
                            }//end reg_detail
                        }

                        //get total reg item
                        $total_register_item = $regSubjectItem->getTotalRegisterItem($sid,$groupInfo['IdSubject'],$groupInfo['IdSemester'],1);

                        //check if no more reg item , drop subject
                        if($total_register_item==0){
                            $subject_info = $studentRegSubjectDB->isRegisteredBySemester($sid,$groupInfo['IdSubject'],$groupInfo['IdSemester']);

                            if($subject_info){
                                if($withdraw==true){
                                    //update course status
                                    $upd_info['Active']=3;
                                    $upd_info['cgpa_calculation']=0; // set 0 in case drop repeat subject
                                    $studentRegSubjectDB->updateStatus($upd_info,$subject_info['IdStudentRegSubjects']);

                                    $subject_info['message']='Student Course Registration : Withdraw Subject';
                                    $subject_info['createddt']=date("Y-m-d H:i:s");
                                    $subject_info['createdby']=$auth->getIdentity()->id;
                                    $subject_info['createdrole']='admin';
                                    $historyDB->addData($subject_info);

                                }else{
                                    if($drop==true){
                                        $subject_info['Active']=2;
                                    }

                                    $subject_info['message']='Student Course Registration : Drop Subject';
                                    $subject_info['createddt']=date("Y-m-d H:i:s");
                                    $subject_info['createdby']=$auth->getIdentity()->id;
                                    $subject_info['createdrole']='admin';
                                    $historyDB->addData($subject_info);

                                    //delete group
                                    $courseGroupStudentDB->removeStudent($subject_info['IdCourseTaggingGroup'],$sid);

                                    //delete exam registration
                                    $examRegDB->deleteRegisterExamCenter($sid,$groupInfo['IdSemester'],$groupInfo['IdSubject']);

                                    //delete subject
                                    $studentRegSubjectDB->deleteData($subject_info['IdStudentRegSubjects']);
                                }
                            }
                        }

                        //check if there is no subject registered
                        $isRegisterSem = $studentRegSubjectDB->getTotalRegisteredBySemester($sid,$groupInfo['IdSemester']);

                        if(count($isRegisterSem)==0){
                            //update semester status NOT REGISTER

                            //  ---------------- update studentsemesterstatus table ----------------
                            $this->updateStudentSemesterStatus($sid,$groupInfo['IdSemester'],131);
                            //  ---------------- end update studentsemesterstatus table ----------------

                        }
                    }
                }
            }

            //redirect here
            $this->_helper->flashMessenger->addMessage(array('success' => 'Withdrawal successful'));
            $this->_redirect($this->baseUrl . '/registration/bulk-withdrawal/withdrawal/id/'.$groupid);
        }
    }
}