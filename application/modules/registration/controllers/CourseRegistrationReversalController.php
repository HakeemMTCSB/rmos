<?php

class Registration_CourseRegistrationReversalController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
        $this->view->title = $this->view->translate("Course Registration Reversal");
        
    	$msg = $this->_getParam('msg', null);
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Data has been saved successfully");
		}
        
        $form = new Registration_Form_SearchListStudentForm();
		$this->view->form = $form;

		 if($form->isValid($_POST)) {
			if ($this->_request->isPost()){
				
				$formData = $this->_request->getPost ();
				
					
				$form->populate($formData);
				$this->view->form = $form;
				
				//get list of registered student
				$studentDB = new Registration_Model_DbTable_Studentregistration();
				$student_list = $studentDB->getListStudent($formData);
				$this->view->student_list = $student_list;
				
				
			}
		}
    }
    
    
    
    public function reversalAction(){
		
		$auth = Zend_Auth::getInstance();
		
		$stack_failed = array();
		
					
		if ($this->_request->isPost()){
			
			    $formData =  $this->_request->getPost();
			   
			     for($i=0; $i<count($formData["IdStudentRegistration"]);  $i++){					
				 	    
			     
						echo $IdStudentRegistration = $formData["IdStudentRegistration"][$i];						
						echo '<br>';						
						
						
						/* -------------------
						 *  START REVERSAL 
						 * -------------------*/
						
				        //tbl_studentregistration						
						$data['IdLandscape'] = 0;
				 		$data['IdSemestersyllabus'] = 0;						
						$data['IdSemester'] = 0;	
						$data['SemesterType'] = 0;						
						$data['IdSemesterMain'] = 0;
						$data['IdSemesterDetails'] = 0;
						$data['student_type'] = 0; //New Student
						
						$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
						$studentRegistrationDb->updateData($data,$IdStudentRegistration);
						
				        //tbl_studentregsubjects
				        $studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
						$studentRegSubjectDB->deleteRegisterSubjects($IdStudentRegistration);
				        
				        //student_semester_status
				        $studentSemesterStatusDb =  new Registration_Model_DbTable_Studentsemesterstatus();						
						$studentSemesterStatusDb->deleteRegisterData($IdStudentRegistration);
						
				        /* -------------------
						 *  END REVERSAL 
						 * -------------------*/
						
						
			     }
			     echo '</pre>';
		}
		exit;
    }
    
      

}

?>