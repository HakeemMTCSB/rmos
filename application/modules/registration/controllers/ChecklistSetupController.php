<?php
/**
 * @author Muhamad Alif
 * @version 1.0
 */

class Registration_ChecklistSetupController extends Base_Base {

	public function init(){

	}

	public function indexAction() {
		
		$status = $this->_getParam('status', null);
		$msg = $this->_getParam('msg', null);
		
		if($status==1 && $msg){
			$this->view->noticeSuccess = $msg;
		}
		
		if($status==2 && $msg){
			$this->view->noticeError = $msg;
		}
		
		$this->view->title = $this->view->translate('Registration Checklist Setup');
		
		
		$regChecklistSetupDb = new Registration_Model_DbTable_ChecklistSetup();
		$this->view->data = $regChecklistSetupDb->getData();
	}
	
	public function addAction(){
		$this->view->title = $this->view->translate('Registration Checklist Setup : Add');
		
		$form = new Registration_Form_ChecklistSetup();
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			if($form->isValid($formData)){
				
				$regChecklistSetupDb = new Registration_Model_DbTable_ChecklistSetup();
				
				$data = array(
					'rcs_name' => $formData['rcs_name'],
					'rcs_description' => $formData['rcs_description'],
					'rcs_enable' => $formData['rcs_enable']
				);
				
				$regChecklistSetupDb->insert($data);
				
				$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'checklist-setup', 'action'=>'index', 'status'=>1, 'msg'=>'Checklist added'),'default',true));
				
			}else{
				$form->populate($formData);		
			}
		}
		
		$this->view->form = $form;
	}
	
	public function editAction(){
		
		$id = $this->_getParam('id', null);
		
		$this->view->title = $this->view->translate('Registration Checklist Setup : Edit');
		
		$form = new Registration_Form_ChecklistSetup(array('id'=>$id));
		
		if ($this->getRequest()->isPost()) {
				
			$formData = $this->getRequest()->getPost();
				
			if($form->isValid($formData)){
		
				$regChecklistSetupDb = new Registration_Model_DbTable_ChecklistSetup();
		
				$data = array(
						'rcs_name' => $formData['rcs_name'],
						'rcs_description' => $formData['rcs_description'],
						'rcs_enable' => $formData['rcs_enable']
				);
		
				$regChecklistSetupDb->update($data,'rcs_id = '.$formData['rcs_id']);
		
				$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'checklist-setup', 'action'=>'index', 'status'=>1, 'msg'=>'Checklist updated'),'default',true));
		
			}else{
				$form->populate($formData);
			}
		}else{
			$regChecklistSetupDb = new Registration_Model_DbTable_ChecklistSetup();
			$form->populate($regChecklistSetupDb->getData($id));
		}
		
		$this->view->form = $form;
	}
	
	public function deleteAction(){
		
		$id = $this->_getParam('id', null);
		
		$checklistStudentDb = new Registration_Model_DbTable_ChecklistStudent();
		
		$checklist_tagged = $checklistStudentDb->fetchAll(array('rcstd_rcs_id=?'=>$id))->toArray();
		if($checklist_tagged){
			$status = 2;
			$msg = "Cannot delete as there is data tagged to this checklist. Please disable the checklist";
		}else{
			
			$regChecklistSetupDb = new Registration_Model_DbTable_ChecklistSetup();
			
			$regChecklistSetupDb->delete('rcs_id = '.$id);
			
			$status = 1;
			$msg="Checklist deleted";
		}
		
		$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'checklist-setup', 'action'=>'index', 'status'=>$status, 'msg'=>$msg),'default',true));
		
	}
}
?>