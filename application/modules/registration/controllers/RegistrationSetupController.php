<?php

class Registration_RegistrationSetupController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }
    
 public function indexAction()
    {
        // action body
        $this->view->title = $this->view->translate("Registration Date Setup");
        
    	$msg = $this->_getParam('msg', null);
		if($msg==1){
			$this->view->noticeSuccess = $this->view->translate("Data has been saved successfully");
		}
   		if($msg==2){
			$this->view->noticeError = $this->view->translate("The information already exist");
		}
        
        $form = new Registration_Form_SearchRegSetup();
		

		 if($form->isValid($_POST)) {
			if ($this->_request->isPost()){
				
				$formData = $this->_request->getPost ();
									
				$form->populate($formData);			
				
				//get list of data
				$setupDB = new Registration_Model_DbTable_RegDateSetup();
				$date_list = $setupDB->getList($formData);
				$this->view->date_list = $date_list;
				
			}
		}
		$this->view->form = $form;
    }
    
    
    public function addAction(){
    	
    	  $this->view->title = $this->view->translate("Add Registration Date Setup");
    	  
    	  $form = new Registration_Form_AddRegDateForm();
    	  $this->view->form = $form;
    	  
    	  $intakeDB = new App_Model_Record_DbTable_Intake();
    	  $this->view->intake = $intakeDB->fngetlatestintake();
    	  
    	 if ($this->_request->isPost()){

    	 	 if($form->isValid($_POST)) {
    	 	 	
				$formData = $this->_request->getPost ();
				$auth = Zend_Auth::getInstance();

				$setupDB = new Registration_Model_DbTable_RegDateSetup();
				
				//print_r($formData);
				
				unset($formData['Save']);
				
				//check if date & intake already exist				
				$isExist = $setupDB->checkExist($formData['rds_intake'],$formData['rds_date']);
				
				if($isExist){
					$formData['rds_createddt']=date ( 'Y-m-d H:i:s');
					$formData['rds_createdby']=$auth->getIdentity()->iduser;
					$setupDB->addData($formData);
					$msg=1;
				}else{
					$msg=2;
				}
				
				
				$this->_redirect($this->view->url(array('module'=>'registration' ,'controller'=>'registration-setup', 'action'=>'index','msg'=>$msg),'default',true));
			}
    	  }
    	  
    }
    
     public function deleteAction(){
    	
     	$rds_id = $this->_getParam('id',0);
     	$setupDB = new Registration_Model_DbTable_RegDateSetup();
     	$setupDB->deleteData($rds_id);
     	$this->_redirect($this->view->url(array('module'=>'registration' ,'controller'=>'registration-setup', 'action'=>'index'),'default',true));
     }
     
     public function viewAction(){
     	
     	    $this->view->title = $this->view->translate("Registration Date Setup - Applicant List");
     	    
     	       	    
     		$rds_id = $this->_getParam('id',0);
     		
     		$setupDB = new Registration_Model_DbTable_RegDateSetup();
     		$this->view->schedule = $setupDB->getData($rds_id);
     		
     		//select from transaction
     		$transactionDB = new App_Model_Application_DbTable_ApplicantTransaction();
     		$this->view->applicant = $transactionDB->getTaggingRegistrationSchedule($rds_id);
     }
}


?>