<?php

class Registration_CourseMigrationController extends Base_Base
{

	private $db;
	private $examRegDb;	
	protected $_course_migration_ct = 'course_migration_temp_ct_missing';
	protected $_course_migration_temp_missing = 'course_migration_temp_missingmifsec';
	
	
		
    public function init()
    {
        /* Initialize action controller here */
    	$this->db =  Zend_Db_Table::getDefaultAdapter();
    	$this->examRegDb = new Registration_Model_DbTable_ExamRegistration();
    }
   
    
    public function indexAction(){
    	
    	$this->view->title = $this->view->translate("Data Migration");   
    	
    	$sql = $this->db->select()
					->from(array('cmt'=>$this->_course_migration_temp_missing),array())				
					->join(array('p'=>'tbl_program'),'p.IdProgram=cmt.IdProgram',array('IdProgram','ProgramName','ProgramCode'))
					->join(array('s'=>'tbl_scheme'),'s.IdScheme=p.IdScheme',array('IdScheme','EnglishDescription','SchemeCode'))
					->group('cmt.IdProgram');			
		$program_list = $this->db->fetchAll($sql);
    	
				
		foreach($program_list as $index=>$program){
			
			//get semester
			$sql2 = $this->db->select()
					->from(array('cmt'=>$this->_course_migration_temp_missing),array('Idsemester','semester'))					
					->join(array('p'=>'tbl_program'),'p.IdProgram=cmt.IdProgram',array())					
					->where('cmt.IdProgram = ?',$program['IdProgram'])					
					->group('cmt.Idsemester');				
			$sem_list = $this->db->fetchAll($sql2);
			
			foreach($sem_list as $key=>$sem){
								
					
						//get total course
						$sql3 = $this->db->select()
							->from(array('cmt'=>$this->_course_migration_temp_missing),array('total_raw'=>'count(course)'))			
							->where('cmt.IdProgram = ?',$program['IdProgram'])
							->where('cmt.Idsemester = ?',$sem['Idsemester']);
						$course = $this->db->fetchRow($sql3);
						$sem_list[$key]['total_raw']=$course['total_raw'];
						
						
						//get total course migrated
						$sql4 = $this->db->select()
										->from(array('cmt'=>$this->_course_migration_temp_missing),array('total_raw'=>'count(course)'))								
										->where('cmt.IdProgram = ?',$program['IdProgram'])
										->where('cmt.Idsemester = ?',$sem['Idsemester'])
										->where('cmt.migrate_status = "Migrated" ');
						$course_migrated = $this->db->fetchRow($sql4);
						$sem_list[$key]['total_migrated']=$course_migrated['total_raw'];
						
						
						//get total course update
						$sql5 = $this->db->select()
										->from(array('cmt'=>$this->_course_migration_temp_missing),array('total_raw'=>'count(course)'))								
										->where('cmt.IdProgram = ?',$program['IdProgram'])
										->where('cmt.Idsemester = ?',$sem['Idsemester'])
										->where('cmt.migrate_status = "Updated" ');
						$course_update = $this->db->fetchRow($sql5);
						$sem_list[$key]['total_updated']=$course_update['total_raw'];
					
			}
			
			$program_list[$index]['semester']=$sem_list;
		}
		
		$this->view->program_list = $program_list;
		//echo '<pre>';
		//print_r($program_list);
		
    }
    
 	public function updateInfoAction(){
    	
 		exit;
 		set_time_limit(0); //10 minutes
 		
    	$this->view->title = $this->view->translate("Data Migration");   
    	
    	$IdProgram = $this->_getParam('IdProgram',null); 
    	
    	$sql = $this->db->select()
					->from(array('cmt'=>'course_migration_temp'))					
					->join(array('p'=>'tbl_program'),'p.IdProgram=cmt.IdProgram',array('ProgramName','ProgramCode'))
					->join(array('s'=>'tbl_scheme'),'s.IdScheme=p.IdScheme',array('IdScheme','EnglishDescription','SchemeCode'))
					->where('cmt.IdProgram = ?',$IdProgram);
					
		$program_list = $this->db->fetchAll($sql);
		
		foreach($program_list as $index=>$program){
		
				//get idSemester based on scheme
				
			if($program['IdSemester']==0 || $program['IdSemester']==''){
				
			
				$sqlsem = $this->db->select()
								  ->from(array('sm'=>'tbl_semestermaster'))
								  ->where('sm.SemesterMainName = ?',trim($program['semester']))
								  ->where('sm.IdScheme=?',$program['IdScheme']);
				$semester = $this->db->fetchRow($sqlsem);
				
				if($semester){					 
					 //update
					 $bind['IdSemester']=$semester['IdSemesterMaster'];					
				}else{
					 $bind['IdSemester']=0;
				}
				
			}//update kalo belum mapping
			
			
			if($program['IdSubject']==0 || $program['IdSubject']==''){
				
			
				//get subject id
				$sqlsubject = $this->db->select()
								  ->from(array('s'=>'tbl_subjectmaster'))
								  ->where('s.SubCode = ?',trim(strtoupper ($program['course'])));
				$subject= $this->db->fetchRow($sqlsubject);
				
				if($subject){
					 //update
					 $bind['IdSubject']=$subject['IdSubject'];					
				}else{
					 $bind['IdSubject']=0;
				}
			}

			if(isset($bind)){
				$this->db->update('course_migration_temp',$bind,"id='".$program['id']."'");
			}	
			
				 
		}
		
		echo 'updated';
		exit;
		
    }
    
    
      public function courseListAction(){
    	
      		exit;
      		$this->view->title = $this->view->translate("Data Migration - Course List");   
      	
      		$IdProgram = $this->_getParam('IdProgram',null); 
      		$IdScheme= $this->_getParam('IdScheme',null); 
      		$IdSemester = $this->_getParam('IdSemester',null); 
   
      		$this->view->IdProgram = $IdProgram;
      		$this->view->IdScheme = $IdScheme;
      		$this->view->IdSemester = $IdSemester;
      		
      		//by course
      		$sql = $this->db->select()
					->from(array('cmt'=>'course_migration_temp'),array('course'))
					->join(array('sr' =>'tbl_studentregistration'),'sr.registrationId=cmt.studentId',array())		
					->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array())			
					->where('sr.IdProgram = ?',$IdProgram)
					->where('p.IdScheme= ?',$IdScheme)
					->where('cmt.Idsemester = ?',$IdSemester)
					->group('cmt.course');			
			$course_list = $this->db->fetchAll($sql);
			
			foreach($course_list as $index=>$course){
				
				
				//get total student for each course
				$sql2 = $this->db->select()
						->from(array('cmt'=>'course_migration_temp'),array('studentId'))
						->join(array('sr' =>'tbl_studentregistration'),'sr.registrationId=cmt.studentId',array())		
						->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array())			
						->where('sr.IdProgram = ?',$IdProgram)
						->where('p.IdScheme= ?',$IdScheme)
						->where('cmt.Idsemester = ?',$IdSemester)
						->where('cmt.course = ?',$course['course']);
				$student = $this->db->fetchAll($sql2);
				$course_list[$index]['total_student']=count($student);
											
			}
			
			echo '<pre>';
			print_r($course_list);
			
			$this->view->course_list = $course_list;
			
      }
      
      
 	public function studentListAction(){
    	
      		$this->view->title = $this->view->translate("Data Migration - Student List");   
      	
      		$IdProgram = $this->_getParam('IdProgram',null); 
      		//$IdScheme= $this->_getParam('IdScheme',null); 
      		$IdSemester = $this->_getParam('IdSemester',null); 
   
      		$this->view->IdProgram = $IdProgram;
      		//$this->view->IdScheme = $IdScheme;
      		$this->view->IdSemester = $IdSemester;
      					
			//by student
      		$sql3 = $this->db->select()
					->from(array('cmt'=>$this->_course_migration_temp_missing),array('total_course'=>'COUNT(IdSubject)','*'))
					->joinLeft(array('sr' =>'tbl_studentregistration'),'sr.registrationId=cmt.studentId AND cmt.IdProgram=sr.IdProgram',array('IdLandscape','IdStudentRegistration','IdProgram','IdIntake','IdProgramScheme'))		
					->where('cmt.IdProgram = ?',$IdProgram)					
					->where('cmt.Idsemester = ?',$IdSemester)
					->group('cmt.studentId')
					->order('cmt.studentId');			
			$student_list = $this->db->fetchAll($sql3);
			
			/*foreach($student_list as $key=>$student){
				
					if($student['IdStudentRegistration']!=''){
						
								//to update student landscape
								if($student['IdLandscape']==0){
									
									//tagging landscape
									//cari by intake, program, scheme FTF/ON
									
									 $sqllandscape = $this->db->select()
														   ->from(array('l'=>'tbl_landscape'),array('IdLandscape'))
														   ->where('l.IdProgram = ?',$student['IdProgram'])	
														   ->where('l.IdStartSemester = ?',$student['IdIntake'])
														   ->where('l.IdProgramScheme = ?',$student['IdProgramScheme']);	
									$landscape = $this->db->fetchRow($sqllandscape);		
											
									if($landscape){
										'update landscape';
										//update student landsacape
										$this->db->update('tbl_studentregistration',array('IdLandscape'=>$landscape['IdLandscape']),"IdStudentRegistration='".$student['IdStudentRegistration']."'");
									}
								}*/
								
								//get total course by student
								/*$sql4 = $this->db->select()
												->from(array('cmt'=>'course_migration_temp_missing'))		
												//->join(array('sr' =>'tbl_studentregistration'),'sr.registrationId=cmt.studentId',array())	
												->join(array('p'=>'tbl_program'),'p.IdProgram=cmt.IdProgram',array())
												->where('cmt.IdProgram = ?',$IdProgram)					
												->where('cmt.Idsemester = ?',$IdSemester)
												->where('cmt.studentId = ?',$student['studentId']);
								$course = $this->db->fetchAll($sql4);
								$student_list[$key]['total_course']	= count($course);
					}
				
			}*/
			
			
			$this->view->student_list = $student_list;
      }
      
      function migrateAction(){      	      	
      	
      	set_time_limit(0); 
      	
      	$this->view->title = $this->view->translate("Data Migration Status");
      	
      	$auth = Zend_Auth::getInstance();
      	
      	if ($this->_request->isPost()){
      		
      		$formData = $this->_request->getPost ();
      		
      		
      		//list course
      		 $sql = $this->db->select()
					->from(array('cmt'=>$this->_course_migration_temp_missing))
					->join(array('sr' =>'tbl_studentregistration'),'sr.registrationId=cmt.studentId AND cmt.IdProgram=sr.IdProgram',array('IdProgram','IdLandscape','IdStudentRegistration','IdIntake','IdProgramScheme'))		
					->where('cmt.IdProgram = ?',$formData['IdProgram'])					
					->where('cmt.Idsemester = ?',$formData['IdSemester'])
					->where('cmt.migrate_status IS NULL OR cmt.migrate_status =0')
					->where('cmt.IdSemester != ?',0)
					->order('cmt.studentId');
							
			$student_list = $this->db->fetchAll($sql);
			
			/*echo '<pre>';
			print_r($student_list);
			exit;*/
                        //var_dump($student_list); exit;
			$stack_student = array();
			
			foreach($student_list as $student){
				
				$stack['name']=$student['name'];
				$stack['studentId']=$student['studentId'];
												
				//if already mirated no need to add
				if($student['migrate_status']!='Migrated' || $student['migrate_status']!='Updated'){
											
												
						//step 1: to check  if student landscape  exist
						if($student['IdLandscape']!=0){
						
									//step 2: to check if subject exist in student landscape
									if($student['IdSubject']!='' && $student['IdSubject']!=0){
											
											$sqlsubject = $this->db->select()
																   ->from(array('ls'=>'tbl_landscapesubject'),array('IdLandscapeSub'))
																   ->where('ls.IdSubject = ?',$student['IdSubject'])	
																   ->where('ls.IdLandscape = ?',$student['IdLandscape']);	
											$subject_landscape = $this->db->fetchRow($sql);		
									
											if($subject_landscape){
														
														//step 4: check duplicate entry
														$sqlsubjectreg = $this->db->select()
																   ->from(array('srs'=>'tbl_studentregsubjects'))
																   ->where('srs.IdSubject = ?',$student['IdSubject'])	
																   ->where('srs.IdStudentRegistration = ?',$student['IdStudentRegistration'])
																   ->where('srs.IdSemesterMain = ?',$formData['IdSemester']);	
														$subject_regsub = $this->db->fetchRow($sqlsubjectreg);
														
														//step 5: check for multiple entry in different semester (can be repeat)
														$sqlmultiple = $this->db->select()
																   ->from(array('srs'=>'tbl_studentregsubjects'))
																   ->where('srs.IdSubject = ?',$student['IdSubject'])	
																   ->where('srs.IdStudentRegistration = ?',$student['IdStudentRegistration']);
														$subject_repeat = $this->db->fetchAll($sqlmultiple);
														
														if(count($subject_repeat)>1){
															$repeat = 1;
														}else{
															$repeat = 0;
														}
														
														if(!$subject_regsub){
															
																//1) course section tagging
		      													$idGroup = $this->createCourseSection($formData['IdSemester'],$student);
		
		      													if(trim($student['grade'])!='Not Graded' || trim($student['grade'])!=''){		      														
		      																      															
				      														try{
																				$this->db->beginTransaction();															
																				
																				$grade = $this->mappinggrade($student,$formData['IdSemester']);
																				
																				$IdStudentRegSubjects = $this->saveData($student,$formData['IdSemester'],$idGroup,$grade);	
																																		
																				$this->db->commit();
																				
																			}catch (Exception $event) {
					      			
																				$this->db->rollBack();
																				
																      			$stack['subject']  = $student['IdSubject'];
																				$stack['status']  = 'Fail';
																				$stack['message'] = $event->getMessage();																				
																				$this->db->update($this->_course_migration_temp_missing,array('migrate_message'=>$event->getMessage(),'migrate_status'=>'Fail'),"id='".$student['id']."'");
																      		}
																      		
					      													if(isset($IdStudentRegSubjects)){	
																													
																				//update table migration																			
																				$bind['migrate_date'] = date ( 'Y-m-d H:i:s');
																				$bind['migrate_by'] = 589;
																				 
																				$message = '';
																				
																				if($grade){
																					if(trim($grade["grade_name"])!=trim($student['grade'])){																		     
																				     	$message .= 'Grade unmatch. System grade is '.$grade["grade_name"];
																				     }
																				}else{
																					$message .= 'System grade not found. Please check grade setup';
																				}
																				
																				$stack['subject']  = $student['course'];
																				$stack['status']  = 'Migrated';
																				$stack['message'] = $message;
																			}
																	
		      														
		      														
			      													
													      		
		      													}else{
		      														
		      														$stack['subject']  = $student['course'];
																	$stack['status']  = 'Fail';
																	$stack['message'] = 'Grade is '.$student['grade'];
		      													}
		      													
																
																
																
																
														}else{
															
															

															try{
																$this->db->beginTransaction();															
																
																$grade = $this->mappinggrade($student,$formData['IdSemester']);
																
																$this->saveUpdateData($student,$formData['IdSemester'],null,$grade,$subject_regsub);	

																
																$stack['subject']  = $student['course'];
																$stack['status']  = 'Updated';
																$stack['message'] = '';
															
																$this->db->commit();
																
															}catch (Exception $event) {
	      			
																$this->db->rollBack();
																
												      			$stack['subject']  = $student['IdSubject'];
																$stack['status']  = 'Fail';
																$stack['message'] = $event->getMessage();																				
																$this->db->update($this->_course_migration_temp_missing,array('migrate_message'=>$event->getMessage(),'migrate_status'=>'Fail'),"id='".$student['id']."'");
												      		}							      		
															
															//update table migration																			
															$bind['migrate_date'] = date ( 'Y-m-d H:i:s');
															$bind['migrate_by'] = 589;
												      																	
														}
																											
													
												
											}else{
												
												//subject not exist in student landscape
												$stack['status']  = 'Fail';
												$stack['message'] = 'Subject not not exist in student Landscape';						
											}
									}else{
										
										//subject not exist in student landscape
										$stack['status']  = 'Fail';
										$stack['message'] = 'Subject ID = 0. Mapping Subject Fail';	
									}
									
							
						}else{
							
							//landsacpe not exist
							$stack['status']  = 'Fail';
							$stack['message'] = 'Landscape not exist';
						}
				
						
						//update table 
						$bind['migrate_status']=$stack['status'];
						$bind['migrate_message']=$stack['message'];
						$bind['repeat']=$repeat;
						$this->db->update($this->_course_migration_temp_missing,$bind,"id='".$student['id']."'");
				
				}else{
												
					$stack['subject']  = $student['course'];
					$stack['status']  = $student['migrate_status'];
					$stack['message'] = $student['migrate_status'].' on '.$student['migrate_date'];
				}
				
				array_push($stack_student,$stack);
												
			}
			
			$this->view->stack_student = $stack_student;
      		
      	}
      
      }
      
      
      public function saveData($student,$idSemester,$idGroup,$grade=null,$subject_regsub=null){
      	
      		$auth = Zend_Auth::getInstance();
      		$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
      		$historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
      	
      				
     			$approval_status = 2;
      			$exam_status = 'C';      			
      		
      			
      			if($grade){
      				
	      			if(isset($grade["Pass"])){
					     if($grade["Pass"]==1) $grade_status = 'Pass'; //Pass 
					     else if($grade["Pass"]==0) $grade_status = 'Fail'; //Fail
				     	 else $grade_status='';
				     }else{
				     	 $grade_status='';
				     }
				     				     
				     $grade_point= $grade["GradePoint"];	
				     $grade_id   = $grade['idDefinition']; //dari tbl defination
				     $grade_name = $student["grade"];	//guna data migration 			     			
				 	 $grade_desc = $grade["GradeDesc"];						
				 	
      			}else{
      				
      				$grade_point='';
      				$grade_id = '';
      				$grade_name = '';
      				$grade_desc = '';
      				$grade_status = '';
      			}
			 
			
      				
	 			$subject["IdStudentRegistration"] = $student['IdStudentRegistration'];
	 			$subject["IdSubject"] = $student['IdSubject'];		
				$subject["IdSemesterMain"] = $idSemester;
				$subject["final_course_mark"] = $student['mark'];	
				$subject["grade_point"] = $grade_point;			
				$subject["grade_id"] = $grade_id;
				$subject["grade_name"] = $grade_name;
				$subject["grade_desc"] = $grade_desc;
				$subject["grade_status"] = $grade_status;							
				$subject["mark_approval_status"] = $approval_status;				
				$subject["SemesterLevel"]= 0;
				$subject["IdLandscapeSub"]=0;
				$subject["Active"]= 1; //register
				$subject["exam_status"] = $exam_status;
				$subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject["UpdUser"]   = 589;
				$subject["migrate_date"]   = date ( 'Y-m-d H:i:s');
				$subject["migrate_by"]   = 589;
				$subject["IdCourseTaggingGroup"] = $idGroup;
				$IdStudentRegSubjects = $studentRegSubjectDB->addData($subject);	
				
				//3)add table studentregsubject audit trail/history
				$subject2['IdStudentRegSubjects']=$IdStudentRegSubjects;
				$subject2["IdStudentRegistration"] = $student['IdStudentRegistration'];
	 			$subject2["IdSubject"] = $student['IdSubject'];		
				$subject2["IdSemesterMain"] = $idSemester;
				$subject2["final_course_mark"] = $student['mark'];
				$subject2["grade_point"] = $grade_point;
				$subject2["grade_id"] = $grade_id;
				$subject2["grade_name"] = $grade_name;
				$subject2["grade_desc"] = $grade_desc;
				$subject2["grade_status"] = $grade_status;	
				$subject2["mark_approval_status"] = $approval_status;
				$subject2["SemesterLevel"]= 0;
				$subject2["IdLandscapeSub"]=0;
				$subject2["Active"]= 1;  //register
				$subject2["exam_status"] = $exam_status;
				$subject2["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject2["UpdUser"]   = 589;
				$subject2["IdCourseTaggingGroup"] = $idGroup;
				$subject2["message"] = 'Migration Course Regisration : Add Subject';
				$subject2['createddt']=date("Y-m-d H:i:s");
				$subject2['createdby']=$auth->getIdentity()->id;
				$subject2["migrate_date"]   = date ( 'Y-m-d H:i:s');
				$subject2["migrate_by"]   = 589;
				$historyDB->addData($subject2);	
				
				
				//  4) ---------------- add/update studentsemesterstatus table ----------------	
						$this->updateStudentSemesterStatus($student['IdStudentRegistration'],$idSemester,130);
				//  ---------------- end update studentsemesterstatus table ----------------	
						
					
	      		// 5) -------------------------- attendance status --------------------------
	      			
				$data['er_idStudentRegistration']=$student['IdStudentRegistration'];		
				$data['er_idSemester']=$idSemester;
				$data['er_idProgram']=$student['IdProgram'];
				$data['er_idSubject']=$student['IdSubject'];
				$data['er_status']=764; //764:Regiseterd
				$data['er_createdby']=589;
				$data['er_createddt']= date ( 'Y-m-d H:i:s');
				
	      		if(trim($student['attendance_status'])=='Present'){
	      			$attendance_status = 395;
	      		}else if(trim($student['attendance_status'])=='Absent'){
	      			$attendance_status = 398;
	      		}else{
	      			$attendance_status = 0;
	      		}
	      		
	      		if($attendance_status!=0){
      				$data['er_attendance_status']=$attendance_status; //395:Present 396:Absent with valid reason 398:Absent
					$data['er_attendance_by']=589;
					$data['er_attendance_dt']=date ( 'Y-m-d H:i:s');			
	      		}
	      				
				//check if exist
				//list course
	      		$sql = $this->db->select()
						->from(array('er'=>'exam_registration'))
						->where('er.er_idProgram = ?',$student['IdProgram'])					
						->where('er.er_idSemester = ?',$idSemester)
						->where('er.er_idSubject = ?',$student['IdSubject'])
						->where('er.er_idStudentRegistration = ?',$student['IdStudentRegistration']);			
				$examreg = $this->db->fetchRow($sql);
			
				if(!$examreg){
					$this->examRegDb->addData($data);
				}
				
				//-------------------------- end attendance status --------------------------
			
				
				return $IdStudentRegSubjects;
      			      		
      }
      
      
	  public function saveUpdateData($student,$idSemester,$idGroup,$grade=null,$subject_regsub=null){
      	
      		$auth = Zend_Auth::getInstance();
      		$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
      		$historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
      	
      				
     			
      			if($grade){
      				
	      			if(isset($grade["Pass"])){
					     if($grade["Pass"]==1) $grade_status = 'Pass'; //Pass 
					     else if($grade["Pass"]==0) $grade_status = 'Fail'; //Fail
				     	 else $grade_status='';
				     }else{
				     	 $grade_status='';
				     }
				     				     
				     $grade_point= $grade["GradePoint"];	
				     $grade_id   = $grade['idDefinition']; //dari tbl defination
				     $grade_name = $student["grade"];	//guna data migration 			     			
				 	 $grade_desc = $grade["GradeDesc"];						
				 	
      			}else{
      				
      				$grade_point='';
      				$grade_id = '';
      				$grade_name = '';
      				$grade_desc = '';
      				$grade_status = '';
      			}
			 
			     				
	 			$subject["IdSemesterMain"] = $idSemester;
				$subject["final_course_mark"] = $student['mark'];	
				$subject["grade_point"] = $grade_point;			
				$subject["grade_id"] = $grade_id;
				$subject["grade_name"] = $grade_name;
				$subject["grade_desc"] = $grade_desc;
				$subject["grade_status"] = $grade_status;							
				$subject["exam_status"] = 'C';
				$subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject["UpdUser"]   = 589;
				$subject["migrate_date"]   = date ( 'Y-m-d H:i:s');
				$subject["migrate_by"]   = 589;
				$studentRegSubjectDB->updateData($subject,$subject_regsub['IdStudentRegSubjects']);	
				
				//3)add table studentregsubject audit trail/history
				$subject2['IdStudentRegSubjects']=$subject_regsub['IdStudentRegSubjects'];
				$subject2["IdStudentRegistration"] = $student['IdStudentRegistration'];
	 			$subject2["IdSubject"] = $student['IdSubject'];		
				$subject2["IdSemesterMain"] = $idSemester;
				$subject2["final_course_mark"] = $student['mark'];
				$subject2["grade_point"] = $grade_point;
				$subject2["grade_id"] = $grade_id;
				$subject2["grade_name"] = $grade_name;
				$subject2["grade_desc"] = $grade_desc;
				$subject2["grade_status"] = $grade_status;	
				$subject2["mark_approval_status"] = 2;
				$subject2["SemesterLevel"]= 0;
				$subject2["IdLandscapeSub"]=0;
				$subject2["Active"]= 1;  //register
				$subject2["exam_status"] = 'C';
				$subject2["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject2["UpdUser"]   = 589;
				$subject2["IdCourseTaggingGroup"] = $subject_regsub['IdCourseTaggingGroup'];
				$subject2["message"] = 'Migration Course Regisration : Update Subject';
				$subject2['createddt'] = date("Y-m-d H:i:s");
				$subject2['createdby'] = $auth->getIdentity()->id;
				$subject2["migrate_date"] = date ( 'Y-m-d H:i:s');
				$subject2["migrate_by"] = 589;
				$historyDB->addData($subject2);	
				
				
				//  4) ---------------- add/update studentsemesterstatus table ----------------	
						$this->updateStudentSemesterStatus($student['IdStudentRegistration'],$idSemester,130);
				//  ---------------- end update studentsemesterstatus table ----------------	
											
				return $subject_regsub['IdStudentRegSubjects'];
      			      		
      }
      
      
 	public function saveDataCt($student,$idSemester){
      	
      		$auth = Zend_Auth::getInstance();
      		$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
      		$historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
      	      				
      		     			

     			/*if(trim(strtoupper($student['exam_status']))=='CT'){
	      			if($grade){
	      				
		      			if(isset($grade["Pass"])){
						     if($grade["Pass"]==1) $grade_status = 'Pass'; //Pass 
						     else if($grade["Pass"]==0) $grade_status = 'Fail'; //Fail
					     	 else $grade_status='';
					     }else{
					     	 $grade_status='';
					     }
					     				     
					     $grade_point= $grade["GradePoint"];	
					     $grade_id   = $grade['idDefinition']; //dari tbl defination
					     $grade_name = $student["grade"];	//guna data migration 			     			
					 	 $grade_desc = $grade["GradeDesc"];						
					 	
	      			}else{
	      				
	      				$grade_point='';
	      				$grade_id = '';
	      				$grade_name = '';
	      				$grade_desc = '';
	      				$grade_status = '';
	      			}
     			}else{
     				
     					$grade_point='';
	      				$grade_id = '';
	      				$grade_name = $student["grade"];
	      				$grade_desc = '';
	      				$grade_status = '';
     			}*/
			 
			
      		 	$exam_status = $student['exam_status']; 
     			$approval_status = 2;
      			$grade_point='';
      			$grade_id = '';
      			$grade_name = $student["grade"];
      			$grade_desc = '';
      			$grade_status = ''; 
      				
	 			$subject["IdStudentRegistration"] = $student['IdStudentRegistration'];
	 			$subject["IdSubject"] = $student['IdSubject'];		
				$subject["IdSemesterMain"] = $idSemester;
				$subject["final_course_mark"] = 0;	
				$subject["grade_point"] = $grade_point;			
				$subject["grade_id"] = $grade_id;
				$subject["grade_name"] = $grade_name;
				$subject["grade_desc"] = $grade_desc;
				$subject["grade_status"] = $grade_status;							
				$subject["mark_approval_status"] = $approval_status;				
				$subject["SemesterLevel"]= 0;
				$subject["IdLandscapeSub"]=0;
				$subject["Active"]= 1; //register
				$subject["exam_status"] = $exam_status;
				$subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject["UpdUser"]   = 589;
				$subject["migrate_date"]   = date ( 'Y-m-d H:i:s');
				$subject["migrate_by"]   = 589;
				$IdStudentRegSubjects = $studentRegSubjectDB->addData($subject);	
				
				//3)add table studentregsubject audit trail/history
				$subject2['IdStudentRegSubjects']=$IdStudentRegSubjects;
				$subject2["IdStudentRegistration"] = $student['IdStudentRegistration'];
	 			$subject2["IdSubject"] = $student['IdSubject'];		
				$subject2["IdSemesterMain"] = $idSemester;
				$subject2["final_course_mark"] = 0;
				$subject2["grade_point"] = $grade_point;
				$subject2["grade_id"] = $grade_id;
				$subject2["grade_name"] = $grade_name;
				$subject2["grade_desc"] = $grade_desc;
				$subject2["grade_status"] = $grade_status;	
				$subject2["mark_approval_status"] = $approval_status;
				$subject2["SemesterLevel"]= 0;
				$subject2["IdLandscapeSub"]=0;
				$subject2["Active"]= 1;  //register
				$subject2["exam_status"] = $exam_status;
				$subject2["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject2["UpdUser"]   = 589;
				$subject2["message"] = 'Data Migration EX & CT & U';
				$subject2['createddt']=date("Y-m-d H:i:s");
				$subject2['createdby']=$auth->getIdentity()->id;
				$subject2["migrate_date"]   = date ( 'Y-m-d H:i:s');
				$subject2["migrate_by"]   = 589;
				$historyDB->addData($subject2);	
				
				
				//  4) ---------------- add/update studentsemesterstatus table ----------------	
						$this->updateStudentSemesterStatus($student['IdStudentRegistration'],$idSemester,130);
				//  ---------------- end update studentsemesterstatus table ----------------	
						
				return $IdStudentRegSubjects;
      			      		
      }
      
      
		public function updateDataCt($student,$idSemester,$subject_regsub){
      	
      		$auth = Zend_Auth::getInstance();
      		$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
      		$historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
      	      							
      		 	$exam_status = $student['exam_status']; 
     			$approval_status = 2;
      			$grade_point='';
      			$grade_id = '';
      			$grade_name = $student["grade"];
      			$grade_desc = '';
      			$grade_status = ''; 
      				
	 			
				$subject["IdSemesterMain"] = $idSemester;
				$subject["final_course_mark"] = 0;	
				$subject["grade_name"] = $grade_name;
				$subject["mark_approval_status"] = $approval_status;				
				$subject["SemesterLevel"]= 0;
				$subject["IdLandscapeSub"]=0;
				$subject["Active"]= 1; //register
				$subject["exam_status"] = $exam_status;
				$subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject["UpdUser"]   = 589;
				$subject["migrate_date"]   = date ( 'Y-m-d H:i:s');
				$subject["migrate_by"]   = 589;
				$studentRegSubjectDB->updateData($subject,$subject_regsub['IdStudentRegSubjects']);	
				
				//3)add table studentregsubject audit trail/history
				$subject2['IdStudentRegSubjects']=$subject_regsub['IdStudentRegSubjects'];
				$subject2["IdStudentRegistration"] = $student['IdStudentRegistration'];
	 			$subject2["IdSubject"] = $student['IdSubject'];		
				$subject2["IdSemesterMain"] = $idSemester;
				$subject2["final_course_mark"] = 0;
				$subject2["grade_point"] = $grade_point;
				$subject2["grade_id"] = $grade_id;
				$subject2["grade_name"] = $grade_name;
				$subject2["grade_desc"] = $grade_desc;
				$subject2["grade_status"] = $grade_status;	
				$subject2["mark_approval_status"] = $approval_status;
				$subject2["SemesterLevel"]= 0;
				$subject2["IdLandscapeSub"]=0;
				$subject2["Active"]= 1;  //register
				$subject2["exam_status"] = $exam_status;
				$subject2["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject2["UpdUser"]   = 589;
				$subject2["message"] = 'Update Data Migration EX & CT & U';
				$subject2['createddt']=date("Y-m-d H:i:s");
				$subject2['createdby']=$auth->getIdentity()->id;
				$subject2["migrate_date"]   = date ( 'Y-m-d H:i:s');
				$subject2["migrate_by"]   = 589;
				$historyDB->addData($subject2);	
				
				
				//  4) ---------------- add/update studentsemesterstatus table ----------------	
						$this->updateStudentSemesterStatus($student['IdStudentRegistration'],$idSemester,130);
				//  ---------------- end update studentsemesterstatus table ----------------	
						
				return $subject_regsub['IdStudentRegSubjects'];
      			      		
      }
      
      
	public function updateStudentSemesterStatus($IdStudentRegistration,$IdSemesterMain,$newstatus){
    	
    	//check current status
    	$semesterStatusDB =  new Registration_Model_DbTable_Studentsemesterstatus();
    	$semester_status = $semesterStatusDB->getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain);
    	
    	if(isset($semester_status) && $semester_status['studentsemesterstatus']==$newstatus){
    		//nothing to update
    	}else{
    		//add new status & keep old status into history table
    		$cms = new Cms_Status();
    		$auth = Zend_Auth::getInstance();
    			      			        	
			$data =  array( 'IdStudentRegistration' => $IdStudentRegistration,									           
				            'idSemester' => $IdSemesterMain,
				            'IdSemesterMain' => $IdSemesterMain,								
				            'studentsemesterstatus' => $newstatus, 	//Register idDefType = 32 (student semester status)
	                        'Level'=>1,
				            'UpdDate' => date ( 'Y-m-d H:i:s'),
				            'UpdUser' => 589
	        );				
					
    		$message = 'Course Migration';
    		$cms->copyDeleteAddSemesterStatus($IdStudentRegistration,$IdSemesterMain,$data,$message,null);
    	}
    	
    }
    
    
    public function reportAction(){
    	
    		$this->view->title = $this->view->translate("Data Migration - Status Report");   
      	
      		$IdProgram = $this->_getParam('IdProgram',null);       		
      		$IdSemester = $this->_getParam('IdSemester',null); 
   
      		$this->view->IdProgram = $IdProgram;
      		$this->view->IdSemester = $IdSemester;
      					
			//by student
      		$sql = $this->db->select()
					->from(array('cmt'=>$this->_course_migration_temp_missing))
					->joinLeft(array('sr' =>'tbl_studentregistration'),'sr.registrationId=cmt.studentId AND cmt.IdProgram=sr.IdProgram',array('IdStudentRegistration'))		
					//->join(array('p'=>'tbl_program'),'p.IdProgram=cmt.IdProgram',array())			
					->where('cmt.IdProgram = ?',$IdProgram)
					->where('cmt.Idsemester = ?',$IdSemester)
					->order('cmt.studentId');
			$student_list = $this->db->fetchAll($sql);
			$this->view->student_list = $student_list;
    }
    
    
    public function createCourseSection($IdSemester,$student){
    	
    	//check if group exist
    	$sql = $this->db->select()
					->from(array('ctg'=>'tbl_course_tagging_group'))
					->where('ctg.IdSemester=?',$IdSemester)
					->where('ctg.IdSubject=?',$student['IdSubject'])
					->where('ctg.IdBranch=?',0);
		$group = $this->db->fetchRow($sql);	
		
		if(!$group){	
					
			//create group
			$data["IdSemester"] = $IdSemester;
			$data["IdSubject"]  = $student['IdSubject'];
			$data["IdBranch"] = 0;
			$data["GroupName"]  = $student['course'];
			$data["GroupCode"]  = $student['course'].'-'.$student['semester'];
			$data["IdUniversity"]  = 1;
			$data["UpdUser"]  = 589;
			$data["UpdDate"]  = date("Y-m-d H:i:s");
			$data["migrate_by"]  = 589;
			$data["migrate_date"]  = date("Y-m-d H:i:s");
			
			$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
			return $idGroup = $courseGroupDb->addData($data);
			
		}else{
			
			return $group['IdCourseTaggingGroup'];
		}
    	
    	
    }
    
    public function mappinggrade($student,$idSemester){
    	
    	 //for data migration only
    	 $mark_obtained = round($student['mark']);
         $grade_code = $student['grade'];
         //var_dump($grade_code); exit;
    	 
    	 if($student['IdProgram']==5){ //cifp only
    	 	
    	 	 $sql = $this->db->select()
					->from(array('sm'=>'tbl_semestermaster'),array('SemesterMainStartDate'))
				    ->where('sm.IdSemesterMaster = ?',$idSemester);
		 	 $sem_grade = $this->db->fetchRow($sql);
		
    	 	
    	 	 $select_grade = $this->db->select()
		 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
				 	 				  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass'))
				 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('idDefinition','grade_name'=>'DefinitionDesc','grade_code'=>'d.DefinitionCode'))
				 	 				  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
				 	 				  ->where('sm.SemesterMainStartDate <= (?)',$sem_grade['SemesterMainStartDate'])
				 	 				  ->where('gsm.BasedOn = 2 ') //program		
				 	 				  ->where('gsm.IdProgram = ?',5)
				 	 				  //->where("g.MinPoint <= ?", $mark_obtained)
				 	 			  	  //->where('g.MaxPoint >= ?', $mark_obtained)
                                                                          ->where('d.DefinitionCode = ?', $grade_code)
				 	 			  	  ->order('sm.SemesterMainStartDate desc');						 	 				
				
    	 	 $grade = $this->db->fetchRow($select_grade);
    	 	
    	 }else{    	 	
    	 		 //check setup yang base on effective semester, program
		 		 $select_grade2 = $this->db->select()
		 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
				 	 				  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass'))
				 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('idDefinition','grade_name'=>'DefinitionDesc','grade_code'=>'d.DefinitionCode'))
				 	 				  ->where('gsm.BasedOn = 2 ') //program		
				 	 				  ->where('gsm.IdIntake = ?',57)
				 	 				  ->where('gsm.IdProgram = ?',$student['IdProgram'])			 	 				
				 	 				  //->where("g.MinPoint <= ?", $mark_obtained)
				 	 				  //->where('g.MaxPoint >= ?', $mark_obtained);
                                                                          ->where('d.DefinitionCode = ?', $grade_code);
					
				 $grade = $this->db->fetchRow($select_grade2);	
    	 }
    	

		 return $grade;
    }
    
    
    public function mappinggrade2($student,$idSemester){ //for CT & EXEMPTION
    	
    	 //for data migration only
    	 $grade_obtained = $student['grade'];
    	 
    	 
    	 if($student['IdProgram']==5){ //cifp only
    	 	
    	 	 $sql = $this->db->select()
					->from(array('sm'=>'tbl_semestermaster'),array('SemesterMainStartDate'))
				    ->where('sm.IdSemesterMaster = ?',$idSemester);
		 	 $sem_grade = $this->db->fetchRow($sql);
		
    	 	
    	 	 $select_grade = $this->db->select()
		 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
				 	 				  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass'))
				 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('idDefinition','GradeName'=>'DefinitionDesc','DefinitionCode'))
				 	 				  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=gsm.IdSemester',array())
				 	 				  ->where('sm.SemesterMainStartDate <= (?)',$sem_grade['SemesterMainStartDate'])
				 	 				  ->where('gsm.BasedOn = 2 ') //program		
				 	 				  ->where('gsm.IdProgram = ?',5)
				 	 				  ->where("d.DefinitionCode = ?", $grade_obtained)
				 	 			  	  ->order('sm.SemesterMainStartDate desc');						 	 				
				
    	 	 $grade = $this->db->fetchRow($select_grade);
    	 	
    	 }else{    	 	
    	 		 //check setup yang base on effective semester, program
		 		 $select_grade2 = $this->db->select()
		 	 				  		  ->from(array('gsm'=>'tbl_gradesetup_main'))
				 	 				  ->join(array('g'=>'tbl_gradesetup'),'g.IdGradeSetUpMain=gsm.IdGradeSetUpMain',array('GradeDesc','GradePoint','Pass'))
				 	 				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=g.Grade',array('idDefinition','grade_name'=>'DefinitionDesc','DefinitionCode'))
				 	 				  ->where('gsm.BasedOn = 2 ') //program		
				 	 				  ->where('gsm.IdIntake = ?',57)
				 	 				  ->where('gsm.IdProgram = ?',$student['IdProgram'])			 	 				
				 	 				  ->where("d.DefinitionCode = ?", $grade_obtained);
					
				 $grade = $this->db->fetchRow($select_grade2);	
    	 }
    	

		 return $grade;
    }
    
    
    
    public function createUsernameAction(){
    	
    	 exit;
    	 set_time_limit(600); //10 minutes
    	
    	 $select = 'SELECT sp.id,sp.appl_fname, sr.IdStudentRegistration, sr.registrationId ,sr.IdProgram FROM `student_profile` as sp JOIN tbl_studentregistration as sr ON sr.sp_id=sp.id WHERE `appl_username` IS NULL ORDER BY sp.id';
    	 $student_list = $this->db->fetchAll($select);
    	  
    	 echo '<pre>';
    	 //print_r($student_list);
    	 
    	 
    	 foreach($student_list as $student){
    	 	
    	 	$clear_pass = $this->fnCreateRandPassword(6);
    	 	
    	 	$pass['clear_pass'] = $clear_pass;
    	 	$pass['appl_username'] = $student['registrationId'];
    	 	$pass['appl_password'] = md5($clear_pass);
    	 	print_r($pass);
    	 	//$this->db->update('student_profile',$pass,"id='".$student['id']."'");
    	 }
    	 
    	 exit;
			
    }
    
	public function fnCreateRandPassword($length)
	{
		$chars = "234567890abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$i = 0;
		$password = "";
		while ($i <= $length)
		{
			@$password .= $chars{mt_rand(0,strlen($chars))};
			$i++;
		}
		return $password;

	}
	
	public function semesterAction()
	{
			//get program list
		    $sql = $this->db->select()					
					->from(array('p'=>'tbl_program'));
			$program_list = $this->db->fetchAll($sql);
			
			foreach($program_list as $index=>$program){
				
				//get list student with no semester syllabus		
				$sql2 = $this->db->select()
						->from(array('sr'=>'tbl_studentregistration'))						
						->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ProgramCode'))			
						->where('sr.IdProgram = ?',$program['IdProgram'])
						->where('sr.IdSemestersyllabus = ?',0)		
						->order('sr.registrationId');			
				$student_list = $this->db->fetchAll($sql2);
				$program_list[$index]['total_student'] = count($student_list);
			}

			$this->view->program_list = $program_list;
			echo '<pre>';
			print_r($program_list);
		
	}
	
	public function processAction()
	{
					
		    $IdProgram = $this->_getParam('IdProgram',null); 
		    
			//get list student with no semester syllabus		
			$sql2 = $this->db->select()
					->from(array('sr'=>'tbl_studentregistration'))						
					->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ProgramCode'))			
					->where('sr.IdProgram = ?',$IdProgram)
					->where('sr.IdSemestersyllabus = ?',0)		
					->order('sr.registrationId');
					//->limit(50,0);			
			$student_list = $this->db->fetchAll($sql2);
			
			$this->view->student_list = $student_list;
			echo '<pre>';
			
			$status  =array();
			foreach($student_list as $key=>$student){
				//get semesterstatus 
				$sql3 = $this->db->select()
						->from(array('sss'=>'tbl_studentsemesterstatus'),array())	
						->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=sss.IdSemesterMain',array('IdSemesterMaster','SemesterMainName','SemesterMainStartDate'))	
						->where('sss.IdStudentRegistration=?',$student['IdStudentRegistration'])
						->where('sss.studentsemesterstatus =?',130)
						->order('sm.SemesterMainStartDate ASC');
				$semester = $this->db->fetchRow($sql3);				
			   			    
			    if($semester){
			    	    // $student_list[$key]['semester'] = $semester;
			    	    $data['IdSemestersyllabus'] = $semester['IdSemesterMaster'];						
						$data['IdSemester'] = $semester['IdSemesterMaster'];	
						$data['IdSemesterMain'] = $semester['IdSemesterMaster'];
						$data['IdSemesterDetails'] = $semester['IdSemesterMaster'];								
	      				$this->db->update('tbl_studentregistration',$data,"IdStudentRegistration='".$student['IdStudentRegistration']."'");
	      				
	      				$stack['IdStudentRegistration'] = $student['IdStudentRegistration'];
	      				$stack['message'] = 'Update =>'.$semester['IdSemesterMaster'];
			    }else{
			    		$stack['IdStudentRegistration'] = $student['IdStudentRegistration'];
	      				$stack['message'] = 'No semester status';
			    }
			    array_push($status,$stack);
			}
		
			print_r($status);
	}
	
	public function mappingCourseAction(){
		
		$sql = $this->db->select()
					->from(array('ct'=>'data_migration_ct'))					
					->group('ct.course');
					
		$course_list = $this->db->fetchAll($sql);
	
		foreach($course_list as $c){
			 
			  //get subject id
				$sqlsubject = $this->db->select()
								  ->from(array('s'=>'tbl_subjectmaster'))
								  ->where('s.SubCode = ?',trim(strtoupper($c['course'])));
				$subject= $this->db->fetchRow($sqlsubject);
				
				if($subject){
					 //update
					 $bind['IdSubject']=$subject['IdSubject'];					
				}else{
					 $bind['IdSubject']=0;
				}
				
				if(isset($bind)){
					$this->db->update('data_migration_ct',$bind,"course='".trim(strtoupper($c['course']) )."'");
				}
				
		}//course list
			
	}
	
	public function migrateCtAction(){
		
		$this->view->title = $this->view->translate("Data Migration CT/EX/U"); 
		
			//get program list
		    $sql = $this->db->select()					
					->from(array('p'=>'tbl_program'));
			$program_list = $this->db->fetchAll($sql);
			
			foreach($program_list as $index=>$program){
				
				
				$sql2 = $this->db->select()
					->from(array('ct'=>$this->_course_migration_ct))
					->where('ct.IdProgram = ?',$program['IdProgram']);
					
				$program_total = $this->db->fetchAll($sql2);
				$program_list[$index]['total']=count($program_total);
				
				
				 $sql3 = $this->db->select()
					->from(array('ct'=>$this->_course_migration_ct))
					->where('ct.IdProgram = ?',$program['IdProgram'])
					->where('ct.migrate_status = "Migrated" ');
					
				$program_migrated = $this->db->fetchAll($sql3);
				$program_list[$index]['total_migrated']=count($program_migrated);
				
				$sql4 = $this->db->select()
					->from(array('ct'=>$this->_course_migration_ct))
					->where('ct.IdProgram = ?',$program['IdProgram'])
					->where('ct.migrate_status = "Updated" ');
					
				$program_updated = $this->db->fetchAll($sql4);
				$program_list[$index]['total_updated']=count($program_updated);
				
			}
			$this->view->program_list =$program_list;
					
	}
	
	public function processctAction(){
		
		
		$IdProgram = $this->_getParam('IdProgram',null); 
			
		
		$sql2 = $this->db->select()
						 ->from(array('ct'=>$this->_course_migration_ct))
						 ->join(array('sr' =>'tbl_studentregistration'),'sr.registrationId=ct.studentId AND ct.IdProgram=sr.IdProgram',array('IdProgram','IdLandscape','IdStudentRegistration','IdIntake','IdProgramScheme'))
						 ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('IdScheme'))
						 //->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemestermaster=sr.IdSemesterMain',array('AcademicYear','SemesterMainStartDate'))
						 ->where('ct.IdProgram = ?',$IdProgram)
						 ->where('ct.IdSemester != ?',0)
						 ->where("IFNULL(migrate_status,'X') NOT IN ('Updated','Migrated')");
						// ->limit(10,10);
					
		$student_list = $this->db->fetchAll($sql2);
		
		
		
		$stack_student = array();
		
		foreach($student_list as $student){
				
				$stack['name']=$student['name'];
				$stack['studentId']=$student['studentId'];
				$stack['IdStudentRegistration']=$student['IdStudentRegistration'];
												
				//if already mirated no need to add
				if($student['migrate_status']!='Migrated' || $student['migrate_status']!='Updated'){
											
												
						//step 1: to check  if student landscape  exist
						if($student['IdLandscape']!=0){							
																				
											//step 2: check  entry
											$sqlsubjectreg = $this->db->select()
													   ->from(array('srs'=>'tbl_studentregsubjects'),array('IdStudentRegSubjects'))
													   ->where('srs.IdSubject = ?',$student['IdSubject'])	
													   ->where('srs.IdStudentRegistration = ?',$student['IdStudentRegistration']);
													   //->where('srs.IdSemesterMain = ?',$student['IdSemester']);	
											$subject_regsub = $this->db->fetchAll($sqlsubjectreg);
											
											if(count($subject_regsub)>1){
												//xboleh migrate dulu check manual												
												$stack['status']  = 'Fail';
												$stack['message'] = 'Total no of subject > 1';
											}else{
												
												if(count($subject_regsub) == 0){
												
      													try{
															$this->db->beginTransaction();															
															
															/*if($student['IdScheme']==11 && trim(strtoupper($student['exam_status']))=='CT'){
																$grade = $this->mappinggrade2($student,$student['IdSemester']);
															}*/
															
															$IdStudentRegSubjects = $this->saveDataCt($student,$student['IdSemester']);	
																													
															$this->db->commit();
															
														}catch (Exception $event) {
      			
															$this->db->rollBack();
															
											      			$stack['subject']  = $student['IdSubject'];
															$stack['status']  = 'Fail';
															$stack['message'] = $event->getMessage();																				
															$this->db->update($this->_course_migration_ct,array('migrate_message'=>$event->getMessage(),'migrate_status'=>'Fail'),"id='".$student['id']."'");
											      		}
											      		
															if(isset($IdStudentRegSubjects)){	
																									
																//update table migration																			
																$bind['migrate_date'] = date ( 'Y-m-d H:i:s');
																$bind['migrate_by'] = 589;
																 																
																$stack['subject']  = $student['course'];
																$stack['status']  = 'Migrated';
															}
      																											
													
													}else if(count($subject_regsub) == 1){
														
														
																	try{
																		$this->db->beginTransaction();															
																		
																		$this->updateDatact($student,$student['IdSemester'],$subject_regsub[0]);	
																		
																		$stack['subject'] = $student['course'];
																		$stack['status']  = 'Updated';
																		$stack['message'] = 'Update semester,grade,examstatus';
																	
																		$this->db->commit();
																		
																	}catch (Exception $event) {
			      			
																		$this->db->rollBack();
																		
														      			$stack['subject']  = $student['IdSubject'];
																		$stack['status']  = 'Fail';
																		$stack['message'] = $event->getMessage();																				
																		$this->db->update($this->_course_migration_ct,array('migrate_message'=>$event->getMessage(),'migrate_status'=>'Fail'),"id='".$student['id']."'");
														      		}					      		
																	
																	//update table migration																			
																	$bind['migrate_date'] = date ( 'Y-m-d H:i:s');
																	$bind['migrate_by'] = 589;
													}//end else
												
											}//end count subject > 1
											
											
										
													
							
						}else{
							
							//landsacpe not exist
							$stack['subject']  = $student['course'];
							$stack['status']  = 'Fail';
							$stack['message'] = 'Landscape not exist';
						}
						
						
						//update table 
						$bind['migrate_status']=$stack['status'];
						$bind['migrate_message']=$stack['message'];
						
						$this->db->update($this->_course_migration_ct,$bind,"id='".$student['id']."'");
				
				}else{
												
					$stack['subject']  = $student['course'];
					$stack['status']  =  $student['migrate_status'];
					$stack['message'] =  $student['migrate_status'].' on '.$student['migrate_date'];
				}
				
				array_push($stack_student,$stack);
								
				
			}
			
			echo '<pre>';
			print_r($stack_student);
			$this->view->stack_student =$stack_student;
		
	}
	
	
	public function createsemesterAction()
	{
		exit;
			//get program list
		    $sql = $this->db->select()					
					->from(array('sm'=>'tbl_semestermaster'))
					->order('sm.SemesterMainStartDate asc');
					//->limit(2,0);
			$semester_list = $this->db->fetchAll($sql);
			
			echo '<pre>';
			
			foreach($semester_list as $index=>$sem){	
				
				 unset($sem['IdSemesterMaster']);
				 
				 $SemesterMainCode = $sem['SemesterMainCode'];
				 
				 $year = date('Y', strtotime($sem['SemesterMainStartDate']));
				 $month = date('m', strtotime($sem['SemesterMainStartDate'])); 

				 if($sem['sem_seq']=='JAN') {
				 	$date = $year.'-'.$month.'-31';
				 }
			 	 if($sem['sem_seq']=='JUN') {
				 	$date = $year.'-'.$month.'-30';
				 }
			 	 if($sem['sem_seq']=='SEP') {
				 	$date = $year.'-'.$month.'-30';
				 }
				 
				 $sem['SemesterMainStartDate'] = $date;
				 $sem['SemesterMainEndDate'] = $date;
				 $sem['SemesterMainName']	= 'Credit Transfer';
				 $sem['SemesterMainDefaultLanguage']	= 'Credit Transfer';
				 $sem['SemesterMainCode']	= $SemesterMainCode.'(CT)';	
				 $sem['IsCountable']	= 0;
				 $sem['SemesterFunctionType']	= 1; //CT
				 $sem['ismigrate']	= 1;	
				 $sem["UpdUser"]  = 589;
				 $sem["UpdDate"]  = date("Y-m-d H:i:s");
				
				 $this->db->insert('tbl_semestermaster',$sem);

				 $sem['SemesterFunctionType']	= 2; //EXEMPTION
				 $sem['SemesterMainName']	= 'Exemption';
				 $sem['SemesterMainDefaultLanguage']	= 'Exemption';
				 $sem['SemesterMainCode']	= $SemesterMainCode.'(EX)';	
				
				 $this->db->insert('tbl_semestermaster',$sem);
				 
			}

			exit;
		
	}
	
	
	public function paperAction(){
		
		/*
		 * SELECT srs.IdStudentRegSubjects,sr.IdStudentRegistration,sr.registrationId,sm.SubCode 
		 * FROM `tbl_studentregsubjects` as srs JOIN tbl_studentregistration as sr ON sr.`IdStudentRegistration`=srs.IdStudentRegistration 
		 * Join tbl_subjectmaster as sm ON sm.IdSubject=srs.IdSubject 
		 * WHERE sr.IdProgram=2 AND srs.IdSemesterMain=6 AND IdStudentRegSubjects 
		 * NOT IN ( SELECT regsub_id FROM tbl_studentregsubjects_detail )
		 */
		
		$auth = Zend_Auth::getInstance();
		
		$registrationItemDB =new Registration_Model_DbTable_RegistrationItem();
		$courseRegItemDB = new Registration_Model_DbTable_StudentRegistrationItem();
		
		 $sql = 'SELECT srs.IdStudentRegSubjects,sr.IdStudentRegistration,sr.registrationId,srs.IdSubject,sm.SubCode,sr.IdProgramScheme,srs.IdCourseTaggingGroup FROM `tbl_studentregsubjects` as srs JOIN tbl_studentregistration as sr ON sr.`IdStudentRegistration`=srs.IdStudentRegistration Join tbl_subjectmaster as sm ON sm.IdSubject=srs.IdSubject WHERE sr.IdProgram=2 AND srs.IdSemesterMain=6 AND IdStudentRegSubjects NOT IN ( SELECT regsub_id FROM tbl_studentregsubjects_detail ) ';
		 $rows = $this->db->fetchAll($sql);

		 
		 foreach($rows as $student){
		 	$item = $registrationItemDB->getStudentItems(2,6,$student['IdProgramScheme'], $chargable=0);
		 	
		 	foreach($item as $i){
		 		
		 		if($i['mandatory']==1){
		 			
		 			//check for duplicate entry
					$select = $this->db->select()
								 ->from(array('a'=>'tbl_studentregsubjects_detail'))
								 ->where('a.regsub_id = ?',$student['IdStudentRegistration'])
								 ->where('a.item_id = ?',$i['item_id']);					
					$resultsa = $this->db->fetchRow($select); 	
					
					if(!$resultsa){						
						
					 		//regsubjects_detail
							$dataitem = array(
											'student_id'		=> $student['IdStudentRegistration'],
											'regsub_id'			=> $student['IdStudentRegSubjects'],
											'semester_id'		=> 6,
											'subject_id'		=> $student['IdSubject'],
											'item_id'			=> $i['item_id'],
											'section_id'		=> $student['IdCourseTaggingGroup'],
											'ec_country'		=> '',
											'ec_city'			=> '',
											'created_date'		=> new Zend_Db_Expr('NOW()'),
											'created_by'		=> $auth->getIdentity()->iduser,
											'created_role'		=> 'admin',
											'ses_id'			=> 0,
											'status'			=> 0
							);
			
							//print_r($dataitem);
							//$this->db->insert('tbl_studentregsubjects_detail', $dataitem);
							$idRegSubjectItem = $courseRegItemDB->addData($dataitem);			
							
							$dataitem['id'] = $idRegSubjectItem;
							$dataitem["message"] = 'Student Course Regisration: Add Item';
							$dataitem['createddt']=date("Y-m-d H:i:s");
							$dataitem['createdby']=$auth->getIdentity()->id;
							
							$this->db->insert('tbl_studentregsubjects_detail_history',$dataitem);
					}
				
		 		}//end if mandatory
		 	}
		 }
		 exit;
	}
	
	
	public function thesisAction(){
		
		
			 $sql2 = $this->db->select()
							 ->from(array('ct'=>'course_migration_temp_4'))
							 ->join(array('sr'=>'tbl_studentregistration'),'sr.registrationId=ct.studentId AND ct.IdProgram=sr.IdProgram',array('IdStudentRegistration','IdProgramScheme','IdLandscape'))
							 ->where('ct.migrate_status IS NULL');
							
							
			$rows = $this->db->fetchAll($sql2);			
			$this->view->student_list =$rows;
			
			if ($this->_request->isPost()){
				
				$formData = $this->_request->getPost ();   
				
				 $sql = $this->db->select()
							 ->from(array('ct'=>'course_migration_temp_4'))
							 ->join(array('sr'=>'tbl_studentregistration'),'sr.registrationId=ct.studentId AND ct.IdProgram=sr.IdProgram',array('IdStudentRegistration','IdProgramScheme','IdLandscape'))
							 ->where('ct.migrate_status IS NULL')
							 ->where('ct.IdSubject !=0 ')
							 ->where('ct.IdSemester = 166');
				$student_list = $this->db->fetchAll($sql);	
				
				$stack_student = array();
				foreach($student_list as $student){
				
						$stack['name']=$student['name'];
						$stack['studentId']=$student['studentId'];
						$stack['IdStudentRegistration']=$student['IdStudentRegistration'];
														
						//if already mirated no need to add
						if($student['migrate_status']!='Migrated'){
													
														
								//step 1: to check  if student landscape  exist
								if($student['IdLandscape']!=0){
									
									
													$sem['IdSemesterMaster'] = $student['IdSemester'];													
													
													if($sem){
														
															//step 4: check duplicate entry
															$sqlsubjectreg = $this->db->select()
																	   ->from(array('srs'=>'tbl_studentregsubjects'),array('IdStudentRegSubjects'))
																	   ->where('srs.IdSubject = ?',$student['IdSubject'])	
																	   ->where('srs.IdStudentRegistration = ?',$student['IdStudentRegistration'])
																	   ->where('srs.IdSemesterMain = ?',$sem['IdSemesterMaster']);	
															$subject_regsub = $this->db->fetchRow($sqlsubjectreg);
															
															if(!$subject_regsub){
																
				      													try{
																			$this->db->beginTransaction();	
																			
																			$IdStudentRegSubjects = $this->saveDataArticleship($student,$sem['IdSemesterMaster'],0,$grade=null);	
																																	
																			$this->db->commit();
																			
																		}catch (Exception $event) {
				      			
																			$this->db->rollBack();
																			
															      			$stack['subject']  = $student['IdSubject'];
																			$stack['status']  = 'Fail';
																			$stack['message'] = $event->getMessage();																				
																			$this->db->update('course_migration_temp_4',array('migrate_message'=>$event->getMessage(),'migrate_status'=>'Fail'),"id='".$student['id']."'");
															      		}
															      		
				      													if(isset($IdStudentRegSubjects)){	
																												
																			//update table migration
																			$bind['migrate_status']='Migrated';
																			$bind['migrate_date'] = date ( 'Y-m-d H:i:s');
																			$bind['migrate_by'] = 589;
																			
																			$this->db->update('course_migration_temp_4',$bind,"id='".$student['id']."'");
																			 
																			$stack['subject']  = $student['course'];
																			$stack['status']  = 'Migrated';
																			$stack['message'] = '';
																		}																
																	
															}else{
																$stack['subject']  = $student['course'];
																$stack['status']  = 'Migrated';
																$stack['message'] = 'Duplicate entry';
															}
															
													}//end if ada sem 
															
									
								}else{
									
									//landsacpe not exist
									$stack['status']  = 'Fail';
									$stack['message'] = 'Landscape not exist';
								}
						
						}else{
														
							$stack['subject']  = $student['course'];
							$stack['status']  = 'Migrated';
							$stack['message'] = 'Migrated on '.$student['migrate_date'];
						}
						
						array_push($stack_student,$stack);
						
						//update table 
						$bind['migrate_status']=$stack['status'];
						$bind['migrate_message']=$stack['message'];
						$this->db->update('course_migration_temp_4',$bind,"id='".$student['id']."'");
						
						
					}
					
					echo '<pre>';
					print_r($stack_student);
					exit;
				
			}//end if post
	
	}
	
	function saveDataArticleship($student,$idSemester,$idGroup,$grade=null){
		
		
      		$auth = Zend_Auth::getInstance();
      		$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
      		$historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
      	      				
      		    $exam_status = $student['status']; 
      		    
      		    if($idSemester==6){//Current Semester Jan2015
      		    	$approval_status = 0;
      		    	$active = $student["active"];
      		    }else{
     				$approval_status = 2;
     				$active = 1;
      		    }

     			if($student['type']==3){
     				//Thesis
     				
     				$grade_point='';
	      			$grade_id = '';
	      			$grade_name = $student["status"];
	      			$grade_desc = '';
	      			$grade_status = '';
	      			
	      			$subject["credit_hour_registered"] = $student["credit_hour_registered"];	
	      			$subject2["credit_hour_registered"] = $student["credit_hour_registered"];	
	      			
     			}else{
     				//PPP@Articleship
     				
	     			$grade_point='';
	      			$grade_id = '';
	      			$grade_name = $student["grade"];
	      			$grade_desc = '';
	      			
	      			if($student["grade"]=='P')	$grade_status = 'Pass';
	      			if($student["grade"]=='F')	$grade_status = 'Fail';	
	      			else $grade_status = '';
     			}
      				
	 			$subject["IdStudentRegistration"] = $student['IdStudentRegistration'];
	 			$subject["IdSubject"] = $student['IdSubject'];		
				$subject["IdSemesterMain"] = $idSemester;
				$subject["final_course_mark"] = 0;	
				$subject["grade_point"] = $grade_point;			
				$subject["grade_id"] = $grade_id;
				$subject["grade_name"] = $grade_name;
				$subject["grade_desc"] = $grade_desc;
				$subject["grade_status"] = $grade_status;							
				$subject["mark_approval_status"] = $approval_status;				
				$subject["SemesterLevel"]= 0;
				$subject["IdLandscapeSub"]=0;
				$subject["Active"]= $active; //1-register
				$subject["exam_status"] = $exam_status;
				$subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject["UpdUser"]   = 589;
				$subject["migrate_date"]   = date ( 'Y-m-d H:i:s');
				$subject["migrate_by"]   = 589;
				$subject["IdCourseTaggingGroup"] = $idGroup;
				$IdStudentRegSubjects = $studentRegSubjectDB->addData($subject);	
				
				//3)add table studentregsubject audit trail/history
				$subject2['IdStudentRegSubjects']=$IdStudentRegSubjects;
				$subject2["IdStudentRegistration"] = $student['IdStudentRegistration'];
	 			$subject2["IdSubject"] = $student['IdSubject'];		
				$subject2["IdSemesterMain"] = $idSemester;
				$subject2["final_course_mark"] = 0;
				$subject2["grade_point"] = $grade_point;
				$subject2["grade_id"] = $grade_id;
				$subject2["grade_name"] = $grade_name;
				$subject2["grade_desc"] = $grade_desc;
				$subject2["grade_status"] = $grade_status;	
				$subject2["mark_approval_status"] = $approval_status;
				$subject2["SemesterLevel"]= 0;
				$subject2["IdLandscapeSub"]=0;
				$subject2["Active"]= $active;  //1-register
				$subject2["exam_status"] = $exam_status;
				$subject2["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject2["UpdUser"]   = 589;
				$subject2["IdCourseTaggingGroup"] = $idGroup;
				$subject2["message"] = 'Data Migration Articleship';
				$subject2['createddt']=date("Y-m-d H:i:s");
				$subject2['createdby']=$auth->getIdentity()->id;
				$subject2["migrate_date"]   = date ( 'Y-m-d H:i:s');
				$subject2["migrate_by"]   = 589;
				$historyDB->addData($subject2);	
				
				 //2.1 add item detail
				 if($idSemester==6){
				 	
				 	//regsubjects_detail
					$dataitem = array(
									'student_id'		=> $student['IdStudentRegistration'],
									'regsub_id'			=> $IdStudentRegSubjects,
									'semester_id'		=> $idSemester,
									'subject_id'		=> $student['IdSubject'],
									'item_id'			=> 890, //PAPER
									'section_id'		=> $idGroup,
									'ec_country'		=> 0,
									'ec_city'			=> 0,
									'created_date'		=> date ( 'Y-m-d H:i:s'),
									'created_by'		=> 589,
									'created_role'		=> 'migrate',
									'ses_id'			=> 0
					);

					
					$this->db->insert('tbl_studentregsubjects_detail', $dataitem);
				 }
				 
				
				//  4) ---------------- add/update studentsemesterstatus table ----------------	
						$this->updateStudentSemesterStatus($student['IdStudentRegistration'],$idSemester,130);
				//  ---------------- end update studentsemesterstatus table ----------------	
						
				return $IdStudentRegSubjects;
      			      		
      
      
	}
	
	
	public function addArticleshipInfoAction(){
		
		echo	$sql = "SELECT *
					FROM `tbl_studentregsubjects`
					WHERE `IdSemesterMain` = '6' AND `IdSubject` = '85' AND `migrate_by` = '589'
					ORDER BY `IdSubject` DESC
					";		
			$student_list = $this->db->fetchAll($sql);	
							
			foreach($student_list as $student){
					
					$sql2 = "SELECT * FROM `thesis_articleship` WHERE `student_id` = '".$student['IdStudentRegistration']."' ";
					$row = $this->db->fetchRow($sql2);	
					
					if(!$row){
					
						$data = array(
								'student_id'				=> $student['IdStudentRegistration'],
								'semester_id'				=> 6,
								'fieldofinterest'			=> '{"1":"","2":"","3":"","4":"","5":"","6":"","7":"","8":"","9":"3","10":"2","11":"1","12":"","13":""}',
								'fieldofinterest_others'	=> '',
								'company'					=> '',
								'designation'				=> '',
								'contactperson'				=> '',
								'address'					=> '',
								'city'						=> '',
								'city_others'				=> '',
								'state'						=> '',
								'state_others'				=> '',
								'country'					=> '',
								'postcode'					=> '',
								'phoneno'					=> '',
								'faxno'						=> '',
								'email'						=> '',
								'isemployee'				=> '',
								'yearsofservice'			=> '',
								'status'					=> 2,
								'created_by'				=> 589,
								'created_date'				=> new Zend_Db_Expr('NOW()'),
								'start_date'				=> '',
								'end_date'					=> '',
								'approved_by'				=> 589,
								'approved_date'				=> new Zend_Db_Expr('NOW()')
						);
						
						echo '<hr>';
						print_r($data);
						
						$this->db->insert('thesis_articleship', $data);
						
					}
			}
					
					
			exit;	
	}
	
	
	public function migrateCeAction(){
		
	echo	$sql2 = $this->db->select()
						 ->from(array('ce'=>'course_migration_ce'))
						 ->join(array('sr' =>'tbl_studentregistration'),'sr.registrationId=ce.studentId AND ce.IdProgram=sr.IdProgram',array('IdProgram','IdLandscape','IdStudentRegistration','IdIntake','IdProgramScheme'))
						 ->where('ce.migrate_status IS NULL');
						 //->limit(1,0);					
		$student_list = $this->db->fetchAll($sql2);
		
		$stack_student = array();
		
		foreach($student_list as $student){
			
				//if already mirated no need to add
				if($student['migrate_status']!='Migrated'){
			
						//step 1: to check  if student landscape  exist
						if($student['IdLandscape']!=0){
							
										//step 2: check duplicate entry
										$sqlsubjectreg = $this->db->select()
												   ->from(array('srs'=>'tbl_studentregsubjects'),array('IdStudentRegSubjects'))
												   ->where('srs.IdSubject = ?',$student['IdSubject'])	
												   ->where('srs.IdStudentRegistration = ?',$student['IdStudentRegistration'])
												   ->where('srs.IdSemesterMain = ?',$student['IdSemester']);	
										$subject_regsub = $this->db->fetchRow($sqlsubjectreg);
										
										if(!$subject_regsub){
											
												//1) course section tagging
      											//$idGroup = $this->createCourseSection($student['IdSemester'],$student);

      											try{
													$this->db->beginTransaction();															
																										
													$IdStudentRegSubjects = $this->saveDataCe($student,$student['IdSemester'],0);	
																											
													$this->db->commit();
													
												}catch (Exception $event) {
      	
													$this->db->rollBack();
													
									      			$stack['subject']  = $student['IdSubject'];
													$stack['status']  = 'Fail';
													$stack['message'] = $event->getMessage();																				
													$this->db->update('course_migration_ce',array('migrate_message'=>$event->getMessage(),'migrate_status'=>'Fail'),"id='".$student['id']."'");
									      		}
									      		
      											if(isset($IdStudentRegSubjects)){	
																						
													//update table migration
													$bind['migrate_status']='Migrated';
													$bind['migrate_date'] = date ( 'Y-m-d H:i:s');
													$bind['migrate_by'] = 589;
													
													$this->db->update('course_migration_ce',$bind,"id='".$student['id']."'");													 
													
													$stack['subject']  = $student['IdSubject'];
													$stack['status']  = 'Migrated';
													$stack['message'] = '';
												}
													
												
										}else{
											$stack['subject']  = $student['IdSubject'];
											$stack['status']  = 'Migrated';
											$stack['message'] = 'Duplicate entry';
										}
						}else{
							//landsacpe not exist
							$stack['status']  = 'Fail';
							$stack['message'] = 'Landscape not exist';
						}
						
						//update table 
						$bind['migrate_status']=$stack['status'];
						$bind['migrate_message']=$stack['message'];
						$this->db->update('course_migration_temp',$bind,"id='".$student['id']."'");
				}
		}
		
		$this->view->stack_student = $stack_student;
		
		echo '<pre>';
		print_r($stack_student);
		exit;
	}
	
	
 	public function saveDataCe($student,$idSemester,$idGroup){
      	exit;
      		$auth = Zend_Auth::getInstance();
      		$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
      		$historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
      	
      		
			
      				
	 			$subject["IdStudentRegistration"] = $student['IdStudentRegistration'];
	 			$subject["IdSubject"] = $student['IdSubject'];		
				$subject["IdSemesterMain"] = $idSemester;
				$subject["final_course_mark"] = null;	
				$subject["grade_point"] = null;			
				$subject["grade_id"] = null;
				$subject["grade_name"] = null;
				$subject["grade_desc"] = 0;
				$subject["grade_status"] = null;							
				$subject["mark_approval_status"] = 0;				
				$subject["SemesterLevel"]= 0;
				$subject["IdLandscapeSub"]=0;
				$subject["Active"]= 1; //register
				$subject["exam_status"] = null;
				$subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject["UpdUser"]   = 589;
				$subject["migrate_date"]   = date ( 'Y-m-d H:i:s');
				$subject["migrate_by"]   = 589;
				$subject["IdCourseTaggingGroup"] = $idGroup;
				$IdStudentRegSubjects = $studentRegSubjectDB->addData($subject);	
				
				//3)add table studentregsubject audit trail/history
				$subject2['IdStudentRegSubjects']=$IdStudentRegSubjects;
				$subject2["IdStudentRegistration"] = $student['IdStudentRegistration'];
	 			$subject2["IdSubject"] = $student['IdSubject'];		
				$subject2["IdSemesterMain"] = $idSemester;
				$subject2["final_course_mark"] = null;
				$subject2["grade_point"] = null;
				$subject2["grade_id"] = null;
				$subject2["grade_name"] = null;
				$subject2["grade_desc"] = 0;
				$subject2["grade_status"] = null;	
				$subject2["mark_approval_status"] = 0;
				$subject2["SemesterLevel"]= 0;
				$subject2["IdLandscapeSub"]=0;
				$subject2["Active"]= 1;  //register
				$subject2["exam_status"] = null;
				$subject2["UpdDate"]   = date ( 'Y-m-d H:i:s');
				$subject2["UpdUser"]   = 589;
				$subject2["IdCourseTaggingGroup"] = $idGroup;
				$subject2["message"] = 'Course Migration: Add CE Subject';
				$subject2['createddt']=date("Y-m-d H:i:s");
				$subject2['createdby']=$auth->getIdentity()->id;
				$subject2["migrate_date"]   = date ( 'Y-m-d H:i:s');
				$subject2["migrate_by"]   = 589;
				$historyDB->addData($subject2);	
				
				
				//  3) ---------------- item table ----------------	
 				//check for duplicate entry
				$select = $this->db->select()
							 ->from(array('a'=>'tbl_studentregsubjects_detail'))
							 ->where('a.regsub_id = ?',$student['IdStudentRegistration'])
							 ->where('a.item_id = ?',890);		//exam only			
				$resultsa = $this->db->fetchRow($select); 	
				
				if(!$resultsa){					
					
				 		//regsubjects_detail
						$dataitem = array(
										'student_id'		=> $student['IdStudentRegistration'],
										'regsub_id'			=> $IdStudentRegSubjects,
										'semester_id'		=> 11,
										'subject_id'		=> $student['IdSubject'],
										'item_id'			=> 890,
										'section_id'		=> $idGroup,
										'ec_country'		=> '',
										'ec_city'			=> '',
										'created_date'		=> new Zend_Db_Expr('NOW()'),
										'created_by'		=> 589,
										'created_role'		=> 'admin',
										'ses_id'			=> 0,
										'status'			=> 0
						);
		
						//print_r($dataitem);
						
						$courseRegItemDB = new Registration_Model_DbTable_StudentRegistrationItem();
						$idRegSubjectItem = $courseRegItemDB->addData($dataitem);			
						
						$dataitem['id'] = $idRegSubjectItem;
						$dataitem["message"] = 'Student CE Course Migration: Add Item';
						$dataitem['createddt']=date("Y-m-d H:i:s");
						$dataitem['createdby']=589;
						
						$this->db->insert('tbl_studentregsubjects_detail_history',$dataitem);
				}
				
				
				
				//  4) ---------------- add/update studentsemesterstatus table ----------------	
						$this->updateStudentSemesterStatus($student['IdStudentRegistration'],$idSemester,130);
				//  ---------------- end update studentsemesterstatus table ----------------	
						
						
						
	      		// 5) -------------------------- attendance status --------------------------
	      			
				$dataer['er_idStudentRegistration']=$student['IdStudentRegistration'];		
				$dataer['er_idSemester']=$idSemester;
				$dataer['er_idProgram']=$student['IdProgram'];
				$dataer['er_idSubject']=$student['IdSubject'];
				$dataer['er_idCountry']=121;
				$dataer['er_idCity']=1348;
				$dataer['er_status']=764; //764:Regiseterd
				$dataer['er_createdby']=589;
				$dataer['er_createddt']= date ( 'Y-m-d H:i:s');
				$dataer['er_attendance_status']=null; //395:Present 396:Absent with valid reason 398:Absent
				$dataer['er_attendance_by']=null;
				$dataer['er_attendance_dt']=null;	
	      				
				//check if exist
				//list course
	      		$sql = $this->db->select()
						->from(array('er'=>'exam_registration'))											
						->where('er.er_idSemester = ?',$idSemester)
						->where('er.er_idSubject = ?',$student['IdSubject'])
						->where('er.er_idStudentRegistration = ?',$student['IdStudentRegistration']);			
				$examreg = $this->db->fetchRow($sql);
			
				if(!$examreg){
					$this->examRegDb->addData($dataer);
				}
				
				//-------------------------- end attendance status --------------------------
			
				return $IdStudentRegSubjects;
      			      		
      }
      
      
      public function updateExamRegistrationAction(){
      	exit;
      	$sql = "SELECT * FROM `tbl_studentregsubjects_detail`			
				WHERE `semester_id` = '11'
				AND `item_id` = '879'
				AND `status` != '3'
				AND subject_id NOT IN (85,84)				
				order by student_id
				";
      	$examreg = $this->db->fetchAll($sql);
      	
      	$add= 0;
      	foreach($examreg as $reg){
      		
      					
				$dataer['er_idStudentRegistration']=$reg['student_id'];		
				$dataer['er_idSemester']=11;
				$dataer['er_idProgram']=0;
				$dataer['er_idSubject']=$reg['subject_id'];
				$dataer['er_idCountry']=0;
				$dataer['er_idCity']=0;
				$dataer['er_status']=764; //764:Regiseterd
				$dataer['er_createdby']=589;
				$dataer['er_createddt']= date ( 'Y-m-d H:i:s');
				$dataer['er_attendance_status']=null; //395:Present 396:Absent with valid reason 398:Absent
				$dataer['er_attendance_by']=null;
				$dataer['er_attendance_dt']=null;	
	      				
				//check if exist
				//list course
	      		$sql = $this->db->select()
						->from(array('er'=>'exam_registration'))											
						->where('er.er_idSemester = ?',11)
						->where('er.er_idSubject = ?',$reg['subject_id'])
						->where('er.er_idStudentRegistration = ?',$reg['student_id']);			
				$examreg = $this->db->fetchRow($sql);
			
				if(!$examreg){
					print_r($dataer);
					echo '<hr>';
					$this->examRegDb->addData($dataer);
					echo $add++;
				}
				
				
			
      		
      	}
      	exit;
      }
      
      
      public function markDistributionAction(){
      	
      	
      
      	exit;
      	$auth = Zend_Auth::getInstance();
      	$setupDb = new Examination_Model_DbTable_MarkDistributionSetup();
      	
      	//MIF
      	$sql_mif = "SELECT DISTINCT `s`.`IdSubject`, `s`.`SubjectName`, `s`.`SubCode`, `s`.`CreditHours`, `s`.`IdSubject` AS `key`, `s`.`subjectMainDefaultLanguage` AS `name`, `ld`.`ProgramDescription` FROM `tbl_landscapesubject` AS `ls` INNER JOIN `tbl_subjectmaster` AS `s` ON s.IdSubject=ls.IdSubject INNER JOIN `tbl_landscape` AS `ld` ON ld.IdLandscape=ls.IdLandscape WHERE (ls.IdLandscape = '16') OR (ls.IdLandscape = '174') OR (ls.IdLandscape = '175') OR (ls.IdLandscape = '176') OR (ls.IdLandscape = '177') OR (ls.IdLandscape = '178') OR (ls.IdLandscape = '179') OR (ls.IdLandscape = '180') OR (ls.IdLandscape = '181') OR (ls.IdLandscape = '182') OR (ls.IdLandscape = '183') OR (ls.IdLandscape = '184') OR (ls.IdLandscape = '185') OR (ls.IdLandscape = '186') OR (ls.IdLandscape = '173') OR (ls.IdLandscape = '172') OR (ls.IdLandscape = '171') OR (ls.IdLandscape = '158') OR (ls.IdLandscape = '159') OR (ls.IdLandscape = '160') OR (ls.IdLandscape = '161') OR (ls.IdLandscape = '162') OR (ls.IdLandscape = '163') OR (ls.IdLandscape = '164') OR (ls.IdLandscape = '165') OR (ls.IdLandscape = '166') OR (ls.IdLandscape = '167') OR (ls.IdLandscape = '168') OR (ls.IdLandscape = '169') OR (ls.IdLandscape = '170') OR (ls.IdLandscape = '187') GROUP BY `ls`.`IdSubject` ORDER BY `s`.`SubCode` ASC ";
      	$res_mif = $this->db->fetchAll($sql_mif); 

      	foreach($res_mif as $mif){
      		
      				$datags['mds_user_type']=1;
					$datags['mds_semester']=11;
					$datags['mds_program']=20;	
					$datags['mds_course']=$mif['IdSubject'];			
					$datags['mds_createdt']=date ( 'Y-m-d H:i:s');
					$datags['mds_createdby']=$auth->getIdentity()->iduser;				
				
					$datags['mds_assessmenttypeid']=58;				
					$datags['mds_min_weightage']=40;
					$datags['mds_max_weightage']=60;
					print_r($datags);
					$setupDb->addData($datags);
					
					print_r($datags);
					$datags['mds_assessmenttypeid']=57;				
					$datags['mds_min_weightage']=40;
					$datags['mds_max_weightage']=60;				
					$setupDb->addData($datags);
					
      	}
      	
      	
      	
      	//PHD
      	$sql_phd = "SELECT DISTINCT `s`.`IdSubject`, `s`.`SubjectName`, `s`.`SubCode`, `s`.`CreditHours`, `s`.`IdSubject` AS `key`, `s`.`subjectMainDefaultLanguage` AS `name`, `ld`.`ProgramDescription` FROM `tbl_landscapesubject` AS `ls` INNER JOIN `tbl_subjectmaster` AS `s` ON s.IdSubject=ls.IdSubject INNER JOIN `tbl_landscape` AS `ld` ON ld.IdLandscape=ls.IdLandscape WHERE (ls.IdLandscape = '157') OR (ls.IdLandscape = '144') OR (ls.IdLandscape = '143') OR (ls.IdLandscape = '142') OR (ls.IdLandscape = '141') OR (ls.IdLandscape = '140') OR (ls.IdLandscape = '139') OR (ls.IdLandscape = '138') OR (ls.IdLandscape = '137') OR (ls.IdLandscape = '136') OR (ls.IdLandscape = '145') OR (ls.IdLandscape = '146') OR (ls.IdLandscape = '147') OR (ls.IdLandscape = '156') OR (ls.IdLandscape = '155') OR (ls.IdLandscape = '154') OR (ls.IdLandscape = '153') OR (ls.IdLandscape = '152') OR (ls.IdLandscape = '151') OR (ls.IdLandscape = '150') OR (ls.IdLandscape = '149') OR (ls.IdLandscape = '148') OR (ls.IdLandscape = '135') OR (ls.IdLandscape = '134') OR (ls.IdLandscape = '40') OR (ls.IdLandscape = '39') OR (ls.IdLandscape = '38') OR (ls.IdLandscape = '37') OR (ls.IdLandscape = '25') OR (ls.IdLandscape = '24') OR (ls.IdLandscape = '23') OR (ls.IdLandscape = '11') OR (ls.IdLandscape = '10') OR (ls.IdLandscape = '41') OR (ls.IdLandscape = '42') OR (ls.IdLandscape = '43') OR (ls.IdLandscape = '133') OR (ls.IdLandscape = '132') OR (ls.IdLandscape = '131') OR (ls.IdLandscape = '130') OR (ls.IdLandscape = '129') OR (ls.IdLandscape = '128') OR (ls.IdLandscape = '127') OR (ls.IdLandscape = '126') OR (ls.IdLandscape = '44') OR (ls.IdLandscape = '4') GROUP BY `ls`.`IdSubject` ORDER BY `s`.`SubCode` ASC ";
      	
      	$res_phd = $this->db->fetchAll($sql_phd); 

      	foreach($res_phd as $phd){
      		
      				$datags2['mds_user_type']=1;
					$datags2['mds_semester']=11;
					$datags2['mds_program']=3;	
					$datags2['mds_course']=$phd['IdSubject'];			
					$datags2['mds_createdt']=date ( 'Y-m-d H:i:s');
					$datags2['mds_createdby']=$auth->getIdentity()->iduser;				
				
					$datags2['mds_assessmenttypeid']=58;				
					$datags2['mds_min_weightage']=40;
					$datags2['mds_max_weightage']=60;
					print_r($datags);
					$setupDb->addData($datags2);
					
					print_r($datags);
					$datags2['mds_assessmenttypeid']=57;				
					$datags2['mds_min_weightage']=40;
					$datags2['mds_max_weightage']=60;				
					$setupDb->addData($datags2);
					
      	}
      	
      	
      	//msc
      	$sql_msc = "SELECT DISTINCT `s`.`IdSubject`, `s`.`SubjectName`, `s`.`SubCode`, `s`.`CreditHours`, `s`.`IdSubject` AS `key`, `s`.`subjectMainDefaultLanguage` AS `name`, `ld`.`ProgramDescription` FROM `tbl_landscapesubject` AS `ls` INNER JOIN `tbl_subjectmaster` AS `s` ON s.IdSubject=ls.IdSubject INNER JOIN `tbl_landscape` AS `ld` ON ld.IdLandscape=ls.IdLandscape WHERE (ls.IdLandscape = '12') OR (ls.IdLandscape = '85') OR (ls.IdLandscape = '84') OR (ls.IdLandscape = '83') OR (ls.IdLandscape = '82') OR (ls.IdLandscape = '81') OR (ls.IdLandscape = '80') OR (ls.IdLandscape = '79') OR (ls.IdLandscape = '13') OR (ls.IdLandscape = '86') OR (ls.IdLandscape = '36') OR (ls.IdLandscape = '35') OR (ls.IdLandscape = '34') OR (ls.IdLandscape = '33') OR (ls.IdLandscape = '22') GROUP BY `ls`.`IdSubject` ORDER BY `s`.`SubCode` ASC";
      	
        $res_msc = $this->db->fetchAll($sql_msc); 

      	foreach($res_msc as $msc){
      		
      				$datags3['mds_user_type']=1;
					$datags3['mds_semester']=11;
					$datags3['mds_program']=1;	
					$datags3['mds_course']=$msc['IdSubject'];			
					$datags3['mds_createdt']=date ( 'Y-m-d H:i:s');
					$datags3['mds_createdby']=$auth->getIdentity()->iduser;				
				
					$datags3['mds_assessmenttypeid']=58;				
					$datags3['mds_min_weightage']=40;
					$datags3['mds_max_weightage']=60;
					print_r($datags);
					$setupDb->addData($datags3);
					
					print_r($datags);
					$datags3['mds_assessmenttypeid']=57;				
					$datags3['mds_min_weightage']=40;
					$datags3['mds_max_weightage']=60;				
					$setupDb->addData($datags3);
					
      	}
      	
      	
      	
      	//mifp
      	$sql_mifp = "SELECT DISTINCT `s`.`IdSubject`, `s`.`SubjectName`, `s`.`SubCode`, `s`.`CreditHours`, `s`.`IdSubject` AS `key`, `s`.`subjectMainDefaultLanguage` AS `name`, `ld`.`ProgramDescription` FROM `tbl_landscapesubject` AS `ls` INNER JOIN `tbl_subjectmaster` AS `s` ON s.IdSubject=ls.IdSubject INNER JOIN `tbl_landscape` AS `ld` ON ld.IdLandscape=ls.IdLandscape WHERE (ls.IdLandscape = '191') OR (ls.IdLandscape = '189') OR (ls.IdLandscape = '188') OR (ls.IdLandscape = '87') OR (ls.IdLandscape = '52') OR (ls.IdLandscape = '54') OR (ls.IdLandscape = '55') OR (ls.IdLandscape = '56') OR (ls.IdLandscape = '190') OR (ls.IdLandscape = '51') OR (ls.IdLandscape = '49') OR (ls.IdLandscape = '8') OR (ls.IdLandscape = '9') OR (ls.IdLandscape = '27') OR (ls.IdLandscape = '28') OR (ls.IdLandscape = '29') OR (ls.IdLandscape = '30') OR (ls.IdLandscape = '31') OR (ls.IdLandscape = '32') OR (ls.IdLandscape = '6') GROUP BY `ls`.`IdSubject` ORDER BY `s`.`SubCode` ASC ";
      	
      	$psmifp = $this->db->fetchAll($sql_mifp);      						
			
      		foreach($psmifp as $psp){
	      			
	      			$data['mds_user_type']=1;
					$data['mds_semester']=6;
					$data['mds_program']=2;	
					$data['mds_course']=$psp['IdSubject'];			
					$data['mds_createdt']=date ( 'Y-m-d H:i:s');
					$data['mds_createdby']=$auth->getIdentity()->iduser;				
				
					$data['mds_assessmenttypeid']=51;				
					$data['mds_min_weightage']=70;
					$data['mds_max_weightage']=70;
					$setupDb->addData($data);
					print_r($data);
					$data['mds_assessmenttypeid']=56;				
					$data['mds_min_weightage']=30;
					$data['mds_max_weightage']=30;				
					$setupDb->addData($data);
					print_r($data);
					
	      		}
	      		
	      		//CIFP
	      		$sql_cifp = "SELECT DISTINCT `s`.`IdSubject`, `s`.`SubjectName`, `s`.`SubCode`, `s`.`CreditHours`, `s`.`IdSubject` AS `key`, `s`.`subjectMainDefaultLanguage` AS `name`, `ld`.`ProgramDescription` FROM `tbl_landscapesubject` AS `ls` INNER JOIN `tbl_subjectmaster` AS `s` ON s.IdSubject=ls.IdSubject INNER JOIN `tbl_landscape` AS `ld` ON ld.IdLandscape=ls.IdLandscape WHERE (ls.IdLandscape = '125') OR (ls.IdLandscape = '113') OR (ls.IdLandscape = '112') OR (ls.IdLandscape = '111') OR (ls.IdLandscape = '110') OR (ls.IdLandscape = '109') OR (ls.IdLandscape = '108') OR (ls.IdLandscape = '107') OR (ls.IdLandscape = '106') OR (ls.IdLandscape = '105') OR (ls.IdLandscape = '114') OR (ls.IdLandscape = '115') OR (ls.IdLandscape = '124') OR (ls.IdLandscape = '123') OR (ls.IdLandscape = '122') OR (ls.IdLandscape = '121') OR (ls.IdLandscape = '120') OR (ls.IdLandscape = '119') OR (ls.IdLandscape = '118') OR (ls.IdLandscape = '117') OR (ls.IdLandscape = '116') OR (ls.IdLandscape = '104') OR (ls.IdLandscape = '103') OR (ls.IdLandscape = '91') OR (ls.IdLandscape = '90') OR (ls.IdLandscape = '89') OR (ls.IdLandscape = '88') OR (ls.IdLandscape = '48') OR (ls.IdLandscape = '47') OR (ls.IdLandscape = '46') OR (ls.IdLandscape = '45') OR (ls.IdLandscape = '3') OR (ls.IdLandscape = '92') OR (ls.IdLandscape = '93') OR (ls.IdLandscape = '102') OR (ls.IdLandscape = '101') OR (ls.IdLandscape = '100') OR (ls.IdLandscape = '99') OR (ls.IdLandscape = '98') OR (ls.IdLandscape = '97') OR (ls.IdLandscape = '96') OR (ls.IdLandscape = '95') OR (ls.IdLandscape = '94') OR (ls.IdLandscape = '1') GROUP BY `ls`.`IdSubject` ORDER BY `s`.`SubCode` ASC";
	      		$pscifp = $this->db->fetchAll($sql_cifp);  
      
     			 foreach($pscifp as $cifp){
	      			
	      			$data2['mds_user_type']=1;
					$data2['mds_semester']=6;
					$data2['mds_program']=5;	
					$data2['mds_course']=$cifp['IdSubject'];			
					$data2['mds_createdt']=date ( 'Y-m-d H:i:s');
					$data2['mds_createdby']=$auth->getIdentity()->iduser;				
				
					$data2['mds_assessmenttypeid']=51;				
					$data2['mds_min_weightage']=70;
					$data2['mds_max_weightage']=80;
					$setupDb->addData($data2);
					
					$data2['mds_assessmenttypeid']=56;				
					$data2['mds_min_weightage']=20;
					$data2['mds_max_weightage']=30;				
					$setupDb->addData($data2);
					
	      		}
	      		
      		exit;
      }
      
      public function itemExamAction(){
      		exit;
      		$auth = Zend_Auth::getInstance();
      	
	      	$sql = "select srs.*,sr.*
					from tbl_studentregistration  as sr
					join tbl_studentregsubjects as srs on srs.IdStudentRegistration = sr.IdStudentRegistration
					where sr.IdProgram IN (2,5)
					and srs.IdSemesterMain = 6
					and Active != 3
					AND ifNULL(exam_status , 'Z') NOT IN ('EX','CT','U')
					AND srs.IdSubject NOT IN (84,85)
					AND sr.profileStatus = 92					
					order by sr.IdStudentRegistration";
	      	
	      	$rows = $this->db->fetchAll($sql);
	      	
	      	echo '<pre>';	      
	      	$item_total = 0;
	      	$exam = 0;
	      	foreach ($rows as $row){
	      		
	      		$sql2 = "select * from tbl_studentregsubjects_detail WHERE item_id= 879  AND regsub_id = ".$row['IdStudentRegSubjects'];
	      		$item = $this->db->fetchRow($sql2);
	      		
	      		if(!$item){
	      			//insert kalo xde exam
	      			//regsubjects_detail
						$dataitem = array(
										'student_id'		=> $row['IdStudentRegistration'],
										'regsub_id'			=> $row['IdStudentRegSubjects'],
										'semester_id'		=> 6,
										'subject_id'		=> $row['IdSubject'],
										'item_id'			=> 879,
										'section_id'		=> '',
										'ec_country'		=> '',
										'ec_city'			=> '',
										'created_date'		=> new Zend_Db_Expr('NOW()'),
										'created_by'		=> 589,
										'created_role'		=> 'admin',
										'ses_id'			=> 0,
										'status'			=> 0
						);
		
						
						print_r($dataitem);
						
						$courseRegItemDB = new Registration_Model_DbTable_StudentRegistrationItem();
						//$idRegSubjectItem = $courseRegItemDB->addData($dataitem);			
						
						$dataitem['id'] = 0;
						$dataitem["message"] = 'exam item not exist: runscript : Add Item';
						$dataitem['createddt']=date("Y-m-d H:i:s");
						$dataitem['createdby']=589;
						
						//$this->db->insert('tbl_studentregsubjects_detail_history',$dataitem);
						
				$item_total++;	
	      		}
	      		
	      		
	      		$dataer['er_idStudentRegistration']=$row['IdStudentRegistration'];		
				$dataer['er_idSemester']=6;
				$dataer['er_idProgram']=$row['IdProgram'];
				$dataer['er_idSubject']=$row['IdSubject'];
				$dataer['er_idCountry']=null;
				$dataer['er_idCity']=null;
				$dataer['er_status']=764; //764:Regiseterd
				$dataer['er_createdby']=589;
				$dataer['er_createddt']= date ( 'Y-m-d H:i:s');
				$dataer['er_attendance_status']=null; //395:Present 396:Absent with valid reason 398:Absent
				$dataer['er_attendance_by']=null;
				$dataer['er_attendance_dt']=null;	
	      				
				//check if exist
				//list course
	      		$sql = $this->db->select()
						->from(array('er'=>'exam_registration'))											
						->where('er.er_idSemester = ?',6)
						->where('er.er_idSubject = ?',$row['IdSubject'])
						->where('er.er_idStudentRegistration = ?',$row['IdStudentRegistration']);			
				$examreg = $this->db->fetchRow($sql);
			
				if(!$examreg){					
					print_r($dataer);					
					//$this->examRegDb->addData($dataer);
					$exam++;
				}
				echo '<hr>';
				
	      	}
	      	echo  'EXAM :'.$exam;
	      	echo  'ITEM :'.$item_total;
	      	exit;
      }
      
      public function checkAction(){
      	
      	/*
      	 * SELECT *
			FROM `tbl_studentregsubjects_detail`
			WHERE `status` = '3'
			group by regsub_id
			order by student_id
      	 */
      		$sql = "SELECT *
					FROM `tbl_studentregsubjects_detail`
					WHERE `status` = '3'
					group by regsub_id
					order by student_id";
	      	
	      	$rows = $this->db->fetchAll($sql);
	      	
	      	foreach($rows as $row){
	      		//check main status
	      		$sql2 = "SELECT *
						FROM `tbl_studentregsubjects`
						WHERE Active = 3
						AND `IdStudentRegSubjects` = ".$row['regsub_id'];
	      	
	      		$r = $this->db->fetchRow($sql2);
	      		
	      		if($r){
	      			//echo 'w';
	      		}else{
	      				      			
	      			$sql3 = "SELECT *
							 FROM `tbl_studentregsubjects_detail`
							 WHERE regsub_id = ".$row['regsub_id'];
			      	
	      			$item = $this->db->fetchAll($sql3);
	      			
	      			$sql4 = "SELECT *
							 FROM `tbl_studentregsubjects_detail`
							 WHERE `status` = '3'
							 AND regsub_id = ".$row['regsub_id'];
	      			$item_w = $this->db->fetchAll($sql4);
	      			
	      			if(count($item_w) == count($item)){
	      				echo '<hr>';
	      				echo 'IdStudentRegistration: '.$row['student_id'].'<br>';
	      				echo 'IdStudentRegSubjects'.$row['regsub_id'].'<br>';	      			
	      				echo 'detect';
	      			}
	      		}
	      	}
	      	
	      	exit;
      }
      
      
      public function updatePointAction(){
      		
      	$stregDB = new Examination_Model_DbTable_StudentRegistrationSubject();
      	
      		$sql = "SELECT * FROM `tbl_studentregsubjects` WHERE `exam_status` = 'CT' 
      		AND `grade_name` != 'CT' AND `grade_name` IS NOT NULL       		
      		ORDER BY `IdSemesterMain` ";
      		$student = $this->db->fetchAll($sql);
      		
      		foreach($student as $row){
      			
      			//get ID
      			$sql2 = $this->db->select()
						->from(array('d'=>'tbl_definationms'))	
						->where('d.idDefType = ?',22)
						->where('trim(d.DefinitionCode) = ?',$row['grade_name']);			
				$grade = $this->db->fetchRow($sql2);
								
				$data['grade_id'] = $grade['idDefinition'];
			
				
				if(trim($row['grade_name']) == 'A'){
						$data['grade_point'] = 4.00;
				}else 
				if(trim($row['grade_name']) == 'A-'){
					$data['grade_point'] = 3.67;
				}else 
				if(trim($row['grade_name']) == 'B+'){
					$data['grade_point'] = 3.33;
				}
	      		else 
	      		if(trim($row['grade_name']) == 'B'){
						$data['grade_point'] = 3.00;
				}
	      		else 
	      		if(trim($row['grade_name']) == 'B-'){
						$data['grade_point'] = 2.67;
				}
	      		else 
	      		if(trim($row['grade_name']) == 'C+'){
						$data['grade_point'] = 2.33;
				}
	      		else 
	      		if(trim($row['grade_name']) == 'C'){
						$data['grade_point'] = 2.00;
				}
      			else 
      			if(trim($row['grade_name']) == 'D'){
					$data['grade_point'] = 1.00;
				}else 
				if(trim($row['grade_name']) == 'F'){
					$data['grade_point'] = 0.00;
				}
				
				$stregDB->updateData($data,$row['IdStudentRegSubjects']);
			
      		}exit;
      		
      }
      
      
      public function updateWrongSemesterAction(){
      	
      	$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
      	$historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
      		$auth = Zend_Auth::getInstance();
      		
      	   //list course
      	   $sql = $this->db->select()
					->from(array('cmt'=>'course_migration_temp_missingmifsec'))
					->join(array('sr' =>'tbl_studentregistration'),'sr.registrationId=cmt.studentId AND cmt.IdProgram=sr.IdProgram',array('IdProgram','IdLandscape','IdStudentRegistration','IdIntake','IdProgramScheme'))		
					->where('cmt.migrate_status IS NULL OR cmt.migrate_status =0')
					->where('cmt.IdSemester != ?',0)
					//->where('cmt.studentId = "0800870" ')
					->order('cmt.studentId');
							
			$student_list = $this->db->fetchAll($sql);
			
			foreach($student_list as $student){
				
				$sqlsubjectreg = $this->db->select()
								   ->from(array('srs'=>'tbl_studentregsubjects'))
								   ->where('srs.IdSubject = ?',$student['IdSubject'])	
								   ->where('srs.IdStudentRegistration = ?',$student['IdStudentRegistration'])
								   ->where('srs.IdSemesterMain = ?',$student['OldIdSemester']);	
				$subject_regsub = $this->db->fetchRow($sqlsubjectreg);
			
				if($subject_regsub){
					
					//1) course section tagging
		      		$idGroup = $this->createCourseSection($student['IdSemester'],$student);
		
		      		try{
		      			
						$this->db->beginTransaction();	
						
						//update new sem
						$subject["IdSemesterMain"] = $student['IdSemester'];
						$subject["IdCourseTaggingGroup"] = $idGroup;
						$subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
						$subject["UpdUser"]   = 589;
						$studentRegSubjectDB->updateData($subject,$subject_regsub['IdStudentRegSubjects']);	
						
						//3)add table studentregsubject audit trail/history
						$subject_regsub["IdSemesterMain"] = $student['IdSemester'];
						$subject_regsub["IdCourseTaggingGroup"] = $idGroup;
						$subject_regsub["UpdDate"]   = date ( 'Y-m-d H:i:s');
						$subject_regsub["UpdUser"]   = 589;
						$subject_regsub["message"] = 'Migration Course Regisration : Update Wrong Semester';
						$subject_regsub['createddt'] = date("Y-m-d H:i:s");
						$subject_regsub['createdby'] = $auth->getIdentity()->id;
						$subject_regsub["migrate_date"] = date ( 'Y-m-d H:i:s');
						$subject_regsub["migrate_by"] = 589;
						$historyDB->addData($subject_regsub);	
						
						
						//  4) ---------------- add/update studentsemesterstatus table ----------------	
								$this->updateStudentSemesterStatus($student['IdStudentRegistration'],$student['IdSemester'],130);
						//  ---------------- end update studentsemesterstatus table ----------------	
						
								
						// 5) -------------------------- attendance status --------------------------
		      							
						//check if exist
					
			      		$sqle = $this->db->select()
								->from(array('er'=>'exam_registration'))										
								->where('er.er_idSemester = ?',$student['OldIdSemester'])
								->where('er.er_idSubject = ?',$student['IdSubject'])
								->where('er.er_idStudentRegistration = ?',$student['IdStudentRegistration']);			
						$examreg = $this->db->fetchRow($sqle);
					
						if($examreg){
							$dataer['er_idSemester']=$student['IdSemester'];
							$this->examRegDb->updateData($dataer,$examreg['er_id']);
						}
						
						//-------------------------- end attendance status --------------------------
					
						//6 : Check old Semester ada register subject lain x
						$sqlothersubjectreg = $this->db->select()
									   ->from(array('srs'=>'tbl_studentregsubjects'))
									   ->where('srs.IdStudentRegistration = ?',$student['IdStudentRegistration'])
									   ->where('srs.IdSemesterMain = ?',$student['OldIdSemester']);	
						$result_othersubjectreg = $this->db->fetchRow($sqlothersubjectreg);
						
						if(!$result_othersubjectreg){
							//update semester statsu not registeres
							$this->updateStudentSemesterStatus($student['IdStudentRegistration'],$student['OldIdSemester'],131);
						}
												
						$bind['migrate_status'] ='Updated';
						$bind['migrate_message']='Update wrong semester';	
						$bind['migrate_date'] = date ( 'Y-m-d H:i:s');
						$bind['migrate_by'] = 589;				
						$this->db->update('course_migration_temp_missingmifsec',$bind,"id='".$student['id']."'");
						
						$this->db->commit();
																				
					}catch (Exception $event) {
      
						$this->db->rollBack();
																									
						$this->db->update('course_migration_temp_missingmifsec',array('migrate_message'=>$event->getMessage(),'migrate_status'=>'Fail'),"id='".$student['id']."'");
		      		}														
					
				}
														
			}
			
			exit;
      }
      
      
      public function updateGradePointAction(){
      	
      	 $gradeSetupDB = new Examination_Model_DbTable_Grade();
      	 
      	 $sql = "SELECT srs.IdStudentRegSubjects,srs.IdSubject,srs.IdSemesterMain,srs.grade_name,srs.grade_point,sr.IdProgram FROM `tbl_studentregsubjects` as srs JOIN tbl_studentregistration as sr ON sr.IdStudentRegistration=srs.IdStudentRegistration WHERE srs.`exam_status` = 'CT' AND srs.`grade_name` != 'CT' AND srs.`grade_point` IS NULL";
      	 $subject_list = $this->db->fetchAll($sql);
      	 
      	 foreach($subject_list as $index=>$subject){
      	 	$grade = $gradeSetupDB->getGradeCTEX($subject['IdSemesterMain'],$subject['IdProgram'],$subject['IdSubject'],$subject['grade_name']);
      	 	$subject_list[$index]['grade']=$grade;
      	 	
      	 	if($grade){
      	 		//update databse  
      	 		$this->db->update('tbl_studentregsubjects',array('grade_point'=>$grade['GradePoint']),"IdStudentRegSubjects='".$subject['IdStudentRegSubjects']."'");    	 
      	 	}	
      	 }
      	 
      	 echo '<pre>';
      	 print_r($subject_list);exit;
      }
}