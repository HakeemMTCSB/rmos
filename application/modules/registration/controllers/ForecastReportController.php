<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 1/12/2015
 * Time: 10:42 AM
 */
class Registration_ForecastReportController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();

        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Registration_Model_DbTable_ForecastReport();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Forecast Report');

        $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
        $recordDb = new Records_Model_DbTable_Academicprogress();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['search'])){
                $form = new Registration_Form_ForecastReportSearch(array('programid'=>$formData['program']));
                $this->view->form = $form;
                $subjectList = $this->model->getSubjectFromLandscape($formData['program']);
                $studentList = $this->model->getStudent($formData);
                $this->view->studentList = $studentList;

                if ($subjectList){
                    if ($formData['program'] == 5){ //cifp
                        $subjectPart1 = array();
                        $subjectPart2 = array();
                        $subjectPart3 = array();

                        foreach ($subjectList as $subjectLoop){
                            switch ($subjectLoop['Level']){
                                case 1:
                                    $subjectPart1[]=$subjectLoop;
                                    break;
                                case 2:
                                    $subjectPart2[]=$subjectLoop;
                                    break;
                                case 3:
                                    $subjectPart3[]=$subjectLoop;
                                    break;
                            }
                        }

                        $this->view->subjectPart1 = $subjectPart1;
                        $this->view->subjectPart2 = $subjectPart2;
                        $this->view->subjectPart3 = $subjectPart3;
                    }else{ //other programme
                        $subjectCore = array();
                        $subjectElective = array();

                        foreach ($subjectList as $subjectLoop){
                            switch ($subjectLoop['DefinitionDesc']){
                                case 'Core':
                                    $subjectCore[] = $subjectLoop;
                                    break;
                                case 'Elective':
                                    $subjectElective[] = $subjectLoop;
                                    break;
                            }
                        }

                        $this->view->subjectCore = $subjectCore;
                        $this->view->subjectElective = $subjectElective;
                    }
                }

                $this->view->formData = $formData;
                $form->populate($formData);
            }else{
                $form = new Registration_Form_ForecastReportSearch();
                $this->view->form = $form;
            }
        }else{
            $form = new Registration_Form_ForecastReportSearch();
            $this->view->form = $form;
        }
    }

    public function printReportAction(){
        $this->_helper->layout->disableLayout();

        $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
        $recordDb = new Records_Model_DbTable_Academicprogress();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Registration_Form_ForecastReportSearch(array('programid'=>$formData['program']));
            $this->view->form = $form;
            $subjectList = $this->model->getSubjectFromLandscape($formData['program']);
            $studentList = $this->model->getStudent($formData);
            $this->view->studentList = $studentList;

            if ($subjectList){
                if ($formData['program'] == 5){ //cifp
                    $subjectPart1 = array();
                    $subjectPart2 = array();
                    $subjectPart3 = array();

                    foreach ($subjectList as $subjectLoop){
                        switch ($subjectLoop['Level']){
                            case 1:
                                $subjectPart1[]=$subjectLoop;
                                break;
                            case 2:
                                $subjectPart2[]=$subjectLoop;
                                break;
                            case 3:
                                $subjectPart3[]=$subjectLoop;
                                break;
                        }
                    }

                    $this->view->subjectPart1 = $subjectPart1;
                    $this->view->subjectPart2 = $subjectPart2;
                    $this->view->subjectPart3 = $subjectPart3;
                }else{ //other programme
                    $subjectCore = array();
                    $subjectElective = array();

                    foreach ($subjectList as $subjectLoop){
                        switch ($subjectLoop['DefinitionDesc']){
                            case 'Core':
                                $subjectCore[] = $subjectLoop;
                                break;
                            case 'Elective':
                                $subjectElective[] = $subjectLoop;
                                break;
                        }
                    }

                    $this->view->subjectCore = $subjectCore;
                    $this->view->subjectElective = $subjectElective;
                }
            }

            $this->view->formData = $formData;
            $form->populate($formData);
        }

        $this->view->filename = date('Ymd').'_forecast_report.xls';
    }

    public function clearAction(){
        $this->_redirect($this->baseUrl . '/registration/forecast-report/class-schedule');
        exit;
    }

    public function getprogramschemeAction(){
        $programId = $this->_getParam('id', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $programSchemeList = $this->model->getProgramScheme($programId);

        $json = Zend_Json::encode($programSchemeList);

        echo $json;
        exit();
    }


    public function getSemesterAction(){
        $id = $this->_getParam('id',null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $programInfo = $this->model->getProgramById($id);

        $semesterList = $this->model->getSemester($programInfo['IdScheme']);

        $json = Zend_Json::encode($semesterList);

        echo $json;
        exit();
    }
}