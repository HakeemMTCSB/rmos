<?php

class Registration_WithdrawalController extends Zend_Controller_Action
{
	/*
	 * TO REVERT WITHDRAWAL
	 */
    public function revertWithdrawalAction()
    {
    		
        // action body
        $this->view->title = $this->view->translate("Revert Withdrawal");
        
        $auth = Zend_Auth::getInstance();
        
        $idStudentRegistration = $this->_getParam('id',0);
        $IdStudentRegSubjects = $this->_getParam('IdStudentRegSubjects',0);
        $IdSem = $this->_getParam('IdSem',0);
   
        $studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
        $historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
        $regSubjectItem = new Registration_Model_DbTable_StudentRegistrationItem();
        
        
        $subject_info = $studentRegSubjectDB->getData($IdStudentRegSubjects);
        
        //keep history
        $subject_info['message']='Revert Withdrawal : Withdraw Subject';
		$subject_info['createddt']=date("Y-m-d H:i:s");
		$subject_info['createdby']=$auth->getIdentity()->id;
		
		$historyDB->addData($subject_info);
		
		
		//keep item history
		$item_info = $regSubjectItem->getItemRegisterById($IdStudentRegSubjects);
		
		foreach($item_info as $item){
			
			unset($item['item_name']);
			
			$item['message']='Revert Withdrawal : Withdraw Item';
			$item['createddt']=date("Y-m-d H:i:s");
			$item['createdby']=$auth->getIdentity()->id;
		
			$regSubjectItem->addHistoryItemData($item);
			
			//delete item by IdDetail
			$regSubjectItem->deleteData($item['id']);
		}
						
		 //delete subject
		 $studentRegSubjectDB->deleteData($IdStudentRegSubjects);
		
		//delete exam registration
		$examRegDB = new Registration_Model_DbTable_ExamRegistration();
		$examRegDB->deleteRegisterExamCenter($subject_info['IdStudentRegistration'],$subject_info['IdSemesterMain'],$subject_info['IdSubject']);
				
		 $this->gobjsessionsis->flash = array('message' => 'Withdrawal has been reverted', 'type' => 'success');
		 $this->_redirect($this->view->url(array('module'=>'registration','controller'=>'course-registration', 'action'=>'register-course','id'=>$idStudentRegistration),'default',true));
		
    }
     
}

?>