<?php
class Registration_IntegrationController extends Base_Base{

    private $_gobjlog;
    private $auth;

    public function init(){ //initialization function
        $this->_gobjlog = Zend_Registry::get('log');
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
    }

    public function indexAction(){
        $db = Zend_Db_Table::getDefaultAdapter();

        $selectStudent = $db->select()
            ->from(array('a'=>'tbl_studentregistration'))
            ->join(array('b'=>'student_profile'), 'a.sp_id = b.id')
            ->where('a.registrationId IN ("1700198","1700199","1700200","1700201","1700202","1700203","1700204","1700205","1700206","1700207","1700208","1700209","1700210")');

        $studentList = $db->fetchAll($selectStudent);

        if ($studentList){
            foreach ($studentList as $student){
                if($student) {
                    $passwordstudent = $this->fnCreateRandPassword(8);

                    $data = array(
                        "Users"=>array(
                            array(
                                'role'=> 5,
                                'username'=> $student['registrationId'],
                                'password'=>$passwordstudent,
                                'firstname'=>$student['appl_fname'],
                                'lastname'=>$student['appl_lname'],
                                'email'=>$student['registrationId'] .'@student.inceif.org'
                            )
                        )
                    );

                    $data1 = array(
                        'username'=> $student['registrationId']
                    );

                    $loginMoodle = '/data/htdocs/moodle/login.php';
                    $moodleStatus = true;
                    if ( defined('MOODLE_LOGIN') && MOODLE_LOGIN == 1 && $moodleStatus == true)
                    {
                        if ( defined('MOODLE_SYNC') )
                        {

                            $response = push_to_moodle('CheckUser',$data1);

                            ///dd($response);

                            $Xml = new SimpleXmlElement($response);

                            if($Xml)
                            {
                                if($Xml->Message == 'User does not exist!')
                                {

                                    push_to_moodle('CreateUser',$data);
                                }
                            }


                            //KMC
                            //get profile student
                            $studentprofileDB = new Records_Model_DbTable_Studentprofile();
                            $profile = $studentprofileDB->getStudentData($student['IdStudentRegistration']);

                            $dob = explode('-',$profile['appl_dob']);

                            if( strlen($dob[0])==2){
                                $mydob = $dob[2].'-'.$dob[1].'-'.$dob[0];
                            }else{
                                $mydob = $profile['appl_dob'];
                            }

                            $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                            $invoiceAmount=$invoiceClass->getAmountResourceFee($student['IdStudentRegistration']);

                            if ($profile['appl_idnumber_type']==487){
                                $idtype = 'mykad';
                            }else if ($profile['appl_idnumber_type']==487){
                                $idtype = 'passport';
                            }else{
                                $idtype = 'others';
                            }

                            $kmc_data = array(
                                'username'=>$student['registrationId'],
                                'password'=>$passwordstudent,
                                'email'=>$student['registrationId'].'@student.inceif.org',
                                'name'=>$profile['appl_fname'].' '.$profile['appl_lname'],
                                'status'=>'active',
                                'status_message'=>'',
                                'religion'=>$profile['religionName'],
                                'nationality'=>$profile['nationality'],
                                ///'identity_type'=>$profile['IdTypeName'],
                                'identity_type'=>$idtype,
                                'identity_number'=>$profile['appl_idnumber'],
                                'date_of_birth'=>$mydob,
                                'study_start'=>$profile['SemesterMainStartDate'],
                                'study_end'=>'',
                                'permanent_address'=>$profile['appl_address1'].' '.$profile['appl_address2'].' '.$profile['appl_address3'],
                                'permanent_city'=>$profile['PCityName'],
                                'permanent_postcode'=>$profile['appl_postcode'],
                                'permanent_state'=>$profile['PStateName'],
                                'permanent_country'=>$profile['PCountryName'],
                                'correspondence_address'=>$profile['appl_caddress1'].' '.$profile['appl_caddress2'].' '.$profile['appl_caddress3'],
                                'correspondence_city'=>$profile['CCityName'],
                                'correspondence_postcode'=>$profile['appl_cpostcode'],
                                'correspondence_state'=>$profile['CStateName'],
                                'correspondence_country'=>$profile['CCountryName'],
                                'contact_home'=>$profile['appl_contact_home'],
                                'contact_office'=>$profile['appl_contact_office'],
                                'contact_mobile'=>$profile['appl_contact_mobile'],
                                'program_name'=>$profile['ProgramCode'],
                                'program_code'=>$profile['ProgramCode'],
                                'program_type'=>$profile['pt'],
                                'program_resource_fee'=> $invoiceAmount['amount'],
                                'program_currency'=> $invoiceAmount['currency']!=null ? $invoiceAmount['currency']:''
                            );

                            $kmcDB = new Registration_Model_DbTable_Kmc();
                            $kmcDB->addKmc($kmc_data);

                            $ldap_data = array(
                                'username'=>$student['registrationId'],
                                'unicodePwd'=>$passwordstudent,
                                'mail'=>$student['registrationId'].'@student.inceif.org',
                                'name'=>$profile['appl_fname'].' '.$profile['appl_lname'],
                                'givenName' => $profile['appl_fname'].' '.$profile['appl_lname'],
                                'sn' => $profile['appl_lname'],
                                'account_type' => 'STUDENT',
                                'program_code' => $profile['ProgramCode'],
                                'intake_id' => $profile['IntakeName'],
                                'award' => $profile['awardName']
                            );

                            $LdapDB = new Registration_Model_DbTable_Ldap();
                            $LdapDB->addLdap($ldap_data);


                            //END KMC

                        }
                    }

                    //MOODLE_SYNC check
                }
            }
        }

        exit;
    }

    public function fnCreateRandPassword($length)
    {
        $chars = "234567890abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $i = 0;
        $password = "";
        while (strlen($password) < $length)
        {
            @$password .= $chars{mt_rand(0,strlen($chars))};
            $i++;
        }
        return $password;

    }
}