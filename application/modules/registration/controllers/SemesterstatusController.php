<?php
class Registration_SemesterstatusController extends Base_Base {
	
	private $lobjSeniorstudentregistrationModelbulk;
	private $lobjStudentregistrationModel;
	private $lobjStudentregistrationForm;
	private $lobjinitialconfig;	
	private $lobjSemesterstatusForm;
	private $lobjSemesterstatusModel;
	private $lobjsemestermaster;
	
	public function init() {
		$this->fnsetObj();			
	}
	
	public function fnsetObj(){
		$this->lobjSeniorstudentregistrationModelbulk = new Registration_Model_DbTable_Seniorstudentregistrationbulk();
		$this->lobjStudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjStudentregistrationForm = new Registration_Form_Studentregistrationbulk();
		
		$this->lobjSemesterstatusModel = new Registration_Model_DbTable_Semesterstatus();
		$this->lobjSemesterstatusForm = new Registration_Form_Semesterstatus();			
		
		$this->lobjplacementtestmarksmodel = new Application_Model_DbTable_Placementtestmarks();
		$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration (); 
		$this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
		$this->lobjsemestermaster = new GeneralSetup_Model_DbTable_Semestermaster (); 
	}
	
	public function indexAction() {
		
		$idUniversity =$this->gobjsessionsis->idUniversity;	
		$this->view->config = $initialConfig = $this->lobjinitialconfig->fnGetInitialConfigDetails($idUniversity);	
		$this->view->lobjform = $this->lobjform; 
		$this->view->results = $larrresult= array();
		$this->view->lobjSemesterstatusForm = $this->lobjSemesterstatusForm;
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				
				$ldtsystemDate = date ( 'Y-m-d H:i:s' );
				$this->view->lobjSemesterstatusForm->UpdDate->setValue( $ldtsystemDate );
				$auth = Zend_Auth::getInstance();
				$this->view->lobjSemesterstatusForm->UpdUser->setValue( $auth->getIdentity()->iduser);
			
				$this->view->results = $larrresult = $this->lobjSemesterstatusModel->fnSearchSemester( $this->lobjform->getValues ()); //searching the values for the user
				
			}
		}
		
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			
				$count=count($larrformData['IdSemester']);		
					
				for($i=0;$i<$count;$i++){					
					$semid  =  $larrformData['IdSemester'][$i];
					
					if($larrformData['IdSemesterStatus'][$i]==""){
						$larrInsertData = array('IdSemester' => $semid,
												'SemesterStatus' => $larrformData['IdSemstatus'][$semid],		
												'UpdDate' => $larrformData['UpdDate'],
												'UpdUser' => $larrformData['UpdUser'],						
											);			
					
							$this->lobjSemesterstatusModel->fnsavesemstatus($larrInsertData);
					}
					else{
						
						$larrInsertData = array('SemesterStatus' => $larrformData['IdSemstatus'][$semid],);		
								
						$this->lobjSemesterstatusModel->fnupdatesemstatus($larrInsertData,$larrformData['IdSemesterStatus'][$i]);
					}
				}		
						$this->_redirect( $this->baseUrl . '/registration/semesterstatus/index');	
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/registration/semesterstatus/index');
		}
		
	}
}