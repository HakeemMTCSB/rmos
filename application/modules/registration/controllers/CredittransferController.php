<?php
class Registration_CredittransferController extends Base_Base { //Controller for the User Module

	private $lobjCredittransferForm;
	private $lobjCredittransfer;
	private $lobjUser;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();	
		
	}
	public function fnsetObj(){
		
		$this->lobjCredittransferForm = new Registration_Form_Credittransfer();
		$this->lobjCredittransfer = new Registration_Model_DbTable_Credittransfer();
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
	}
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		
		$lobjApplicantNameList = $this->lobjCredittransfer->fnGetApplicantNameList();
		$lobjform->field5->addMultiOptions($lobjApplicantNameList);
		
		$lobjplacementtest = new Application_Model_DbTable_Placementtest(); 
		$ProgramList=$lobjplacementtest->fnGetProgramMaterList();
		$lobjform->field8->addMultiOptions($ProgramList);
		
		$larrresult = $this->lobjCredittransfer->fngetStudentprofileDtls(); //get user details
		
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->credittransferpaginatorresult);
   	    	
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->credittransferpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->credittransferpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjCredittransfer ->fnSearchStudentApplication( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->credittransferpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'studentfinance' ,'controller'=>'chargemaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/registration/credittransfer/index');
		}
	}

	public function credittransferlistAction() { //Action for the updation and view of the  details
		
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$this->view->lobjCredittransferForm = $this->lobjCredittransferForm;

		$lintIdApplication = ( int ) $this->_getParam ( 'idapplicant' );
		$this->view->IdApplication = $lintIdApplication;
		
		$this->view->lobjCredittransferForm = $this->lobjCredittransferForm;
		
		$studentdetails = $this->lobjCredittransfer->getCompleteStudentDetails($lintIdApplication);
		
		$this->view->studentdetails = $studentdetails;
	
		$programid = $studentdetails['IdProgram'];
		 
		if($programid==""){	
			
			echo "
					<script> 
					var x=window.confirm('Landscape Not Defined');
					
					if(x){
					parent.location ='".$this->view->baseUrl()."/registration/credittransfer/';	 	
					}
					else{
					parent.location ='".$this->view->baseUrl()."/registration/credittransfer/';	
					}
					</script>";			
			
		}
		else{	
		$larrlandscapelist = $this->lobjCredittransfer->getLandscapeList($programid);		
		foreach($larrlandscapelist as $larrdefmsresult) {						
			$this->lobjCredittransferForm->landscape->addMultiOption($larrdefmsresult['IdLandscape'],$larrdefmsresult['Landscape']);
		}
	   
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjCredittransferForm->UpdDate->setValue( $ldtsystemDate );
		$this->view->lobjCredittransferForm->UpdUser->setValue( $auth->getIdentity()->iduser);		
		$this->view->lobjCredittransferForm->IdApplication->setValue($lintIdApplication);		
		$this->view->lobjCredittransferForm->IdProgram->setValue($studentdetails['IdProgram']);		
		//$this->view->lobjCredittransferForm->IdSemester->setValue($stdentpresentsemester['IdSemester']);	
		$larrcheckstatussub=array();
		$larrcheckstatussta=array();
		/*$checkstatus = $this->lobjCredittransfer->fnGetCreditTransferResult($lintIdApplication,$studentdetails['IdProgram'],$stdentpresentsemester['IdSemester']);
		foreach ($checkstatus as $checkstatussub)
		{
			$larrcheckstatussub[] = $checkstatussub['IdSubject'];
			$larrcheckstatussta[$checkstatussub['IdSubject']] = $checkstatussub['Status'];
		}	*/
		$this->view->larrcheckstatussub = $larrcheckstatussub;
		$this->view->larrcheckstatussta = $larrcheckstatussta;
		
		if ($this->_request->isPost() && $this->_request->getPost('Save')) { // save opeartion	
					
			$lobjFormData = $this->_request->getPost();
			$lobjFormData['UpdDate'] = $ldtsystemDate;
			$lobjFormData['UpdUser'] =  $auth->getIdentity()->iduser;
			
			
	    $lastid=$this->lobjCredittransfer->fnAddCredittransfer($lobjFormData);
	    
	    	// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Add External Credit Transfer Id=' . $lobjFormData['IdApplication'],
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
	
		for($i=0;$i<count($lobjFormData['subjid']);$i++)
		{
			$subid  =  $lobjFormData['subjid'][$i];
			
			$larrInsertData = array('IdCreditTransfer' => $lastid,
									'IdSubject' => $subid,		
									'Marks' => $lobjFormData['marks'][$subid],
									'Comments' => $lobjFormData['comments'][$subid],						
									);
							
			$lastsubjcreditId=$this->lobjCredittransfer->fnAddCredittransferSubjects($larrInsertData,$lastid);
			//file upload
	   		$lstruploaddir = "/documents/CreditTransferSubjects/";
	   		for($lintii = 0;$lintii<count($_FILES['uploadeddocuments']['name'][$subid]);$lintii++)
			{  	
				if($_FILES['uploadeddocuments']['error'][$subid][$lintii] != UPLOAD_ERR_NO_FILE)
				{
					$lstrfilename = pathinfo(basename($_FILES['uploadeddocuments']['name'][$subid][$lintii]), PATHINFO_FILENAME);
					$lstrext = pathinfo(basename($_FILES['uploadeddocuments']['name'][$subid][$lintii]), PATHINFO_EXTENSION);
					
					$username = empty($lobjFormData['subjid'][$i])?"":$lobjFormData['applicantid']."_";
					
					$filename = $username."_".date('YmdHis').".".$lstrext;
					$filename = str_replace(' ','_',$lstrfilename)."_".$filename;	
					$file = realpath('.').$lstruploaddir . $filename;
					
					if (move_uploaded_file($_FILES['uploadeddocuments']['tmp_name'][$subid][$lintii], $file)) 
					{
						echo "success";
						$documentnames[$lintii]['uploadedfilename'] = $filename;
						$documentnames[$lintii]['filename'] = $lstrfilename;
						$documentnames[$lintii]['type'] = $_FILES['uploadeddocuments']['type'][$subid][$lintii];
						$documentnames[$lintii]['size'] = $_FILES['uploadeddocuments']['size'][$subid][$lintii];
						$documentnames[$lintii]['UpdDate'] = $lobjFormData['UpdDate'];
						$documentnames[$lintii]['UpdUser'] = $lobjFormData['UpdUser'];
						$documentnames[$lintii]['FileLocation'] = $lstruploaddir;
						$documentnames[$lintii]['Comments'] = "nil";
						//$documentnames[$lintii]['documentcategory'] = $lobjFormData['documentcategory'][$subid][$lintii];	
						$documentnames[$lintii]['documentcategory'] = 0;		
										
						if(count($documentnames) > 0)$this->lobjCredittransfer->fnaddCreditTransferDocumentDetails($documentnames,$lastsubjcreditId);
					} 
					else 
					{
			    		echo "error";
					}
				}
	  		}
		
		}
		$this->_redirect( $this->baseUrl . '/registration/credittransfer/index');
	 }
		
	}		
		
	}
	
	public function credittransfersubjectlistAction(){
		$this->_helper->layout->disableLayout();
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$this->view->lobjCredittransferForm = $this->lobjCredittransferForm;
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		$landscapeId=$this->_getParam ( 'idLandscape' );
		$lintIdApplication = ( int ) $this->_getParam ( 'appplicntid' );	
		$lintprogramid = ( int ) $this->_getParam ( 'programid' );
		
		$larrsubjectlist = $this->lobjCredittransfer->getSubjectList($landscapeId);
		$this->view->larrsubjectlist = $larrsubjectlist;
		$this->view->applicantid=$lintIdApplication;
		$this->view->programid=$lintprogramid;
		        $this->view->paginator = $lobjPaginator->fnPagination($larrsubjectlist,$lintpage,$lintpagecount);
				$this->gobjsessionsis->credittransferpaginatorresult = $larrsubjectlist;
		
	}
		   
		
	
	

	}