<?php

class Registration_StudentprofileController extends Base_Base {

    public function init() { //initialization function
        $this->fnsetObj();
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
    }

    public function fnsetObj() {
        $this->lobjform = new App_Form_Search ();
        $this->lobjschemesetupmodel = new GeneralSetup_Model_DbTable_Schemesetup();
        $this->lobjintake = new GeneralSetup_Model_DbTable_Intake();
        $this->Branchofficevenue = new GeneralSetup_Model_DbTable_Branchofficevenue();
        $this->lobjAddDropSubjectModel = new Registration_Model_DbTable_Adddropsubject();
        $this->lobjconfig = new GeneralSetup_Model_DbTable_Initialconfiguration();
        $this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
        $this->lobjStudentApplicationForm = new Application_Form_Manualapplication();
        $this->lobjUser = new GeneralSetup_Model_DbTable_User();
        $this->studentprofileForm = new Registration_Form_Studentprofile();
        $this->lobjSemesterModel = new GeneralSetup_Model_DbTable_Semester();
        $this->lobjqualificationsub = new Application_Model_DbTable_Studentqualification();
        $this->lobjStudentApplicationForm = new Application_Form_Manualapplication();
    }

    public function indexAction() {
        $this->view->lobjform = $this->lobjform;
        $schemeList = $this->lobjschemesetupmodel->fnGetSchemeDetails(); //function to display all schemesetup details in list
        foreach ($schemeList as $larrschemearr) {
            $this->view->lobjform->field23->addMultiOption($larrschemearr['IdScheme'], $larrschemearr['EnglishDescription']);
        }
        $larrintakelist = $this->lobjintake->fngetallIntake();
        $this->view->lobjform->field24->addMultiOptions($larrintakelist);
        $lobjColgBranchList = $this->Branchofficevenue->fnGetBranchList();
        foreach ($lobjColgBranchList as $larrBranchlist) {
            $this->view->lobjform->field25->addMultiOption($larrBranchlist['id'], $larrBranchlist['value']);
        }
        $lobjplacementtest = new Application_Model_DbTable_Placementtest();
        $ProgramList = $lobjplacementtest->fnGetProgramMaterList();
        $this->lobjform->field8->addMultiOptions($ProgramList);
        //$lobjsemesterlist = $this->lobjAddDropSubjectModel->fnGetSemesterList();
        $lobjsemesterlist = $this->lobjSemesterModel->getAllsemesterList();
        $this->lobjform->field1->addMultiOptions($lobjsemesterlist);

        $larrresult = $this->lobjAddDropSubjectModel->fngetRegisteredStudentDtls();

        for ($i = 0; $i < count($larrresult); $i++) {
            $studentcurrentsem = $this->lobjAddDropSubjectModel->fngetstudentcurrentsem($larrresult[$i]['IdStudentRegistration']);
            if (isset($studentcurrentsem[0]['SemesterCode']) && $studentcurrentsem[0]['SemesterCode'] != NULL) {
                $larrresult[$i]['SemesterCode'] = $studentcurrentsem[0]['SemesterCode'];
            } else if (isset($studentcurrentsem[0]['SemesterMainCode']) && $studentcurrentsem[0]['SemesterMainCode'] != NULL) {
                $larrresult[$i]['SemesterCode'] = $studentcurrentsem[0]['SemesterMainCode'];
            }
        }
        if (!$this->_getParam('search'))
            unset($this->gobjsessionsis->studentprofile);

        $lintpagecount = $this->gintPageCount;
        $lobjPaginator = new App_Model_Common(); // Definitiontype model
        $lintpage = $this->_getParam('page', 1); // Paginator instance

        if (isset($this->gobjsessionsis->studentprofile)) {
            $this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->studentprofile, $lintpage, $lintpagecount);
        } else {
            $this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
        }
        if ($this->_request->isPost() && $this->_request->getPost('Search')) {
            $larrformData = $this->_request->getPost();
            if ($this->lobjform->isValid($larrformData)) {
                $larrresult = $this->lobjAddDropSubjectModel->fnSearchRegisteredStudentApplication($this->lobjform->getValues()); //searching the values for the user
                for ($i = 0; $i < count($larrresult); $i++) {
                    $studentcurrentsem = $this->lobjAddDropSubjectModel->fngetstudentcurrentsem($larrresult[$i]['IdStudentRegistration']);
                    if (isset($studentcurrentsem[0]['SemesterCode']) && $studentcurrentsem[0]['SemesterCode'] != NULL) {
                        $larrresult[$i]['SemesterCode'] = $studentcurrentsem[0]['SemesterCode'];
                    } else if (isset($studentcurrentsem[0]['SemesterMainCode']) && $studentcurrentsem[0]['SemesterMainCode'] != NULL) {
                        $larrresult[$i]['SemesterCode'] = $studentcurrentsem[0]['SemesterMainCode'];
                    }
                }
                $this->view->paginator = $lobjPaginator->fnPagination($larrresult, $lintpage, $lintpagecount);
                $this->gobjsessionsis->studentprofile = $larrresult;
            }
        }
        if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
            $this->_redirect($this->baseUrl . '/registration/studentprofile/index');
        }
    }

    public function studentdetailAction() {
        $this->view->studentprofileForm = $this->studentprofileForm;
        $this->view->lobjStudentApplicationForm = $this->lobjStudentApplicationForm;
        $nameConfDetail = array();
        $idUniversity = $this->gobjsessionsis->idUniversity;
        $confdetail = $this->lobjconfig->fnGetInitialConfigDetails($idUniversity);
        $nameConfDetail['count'] = $confdetail['NameDtlCount'];
        $temp = array();
        for ($i = 1; $i <= $nameConfDetail['count']; $i++) {
            $field = 'NameDtl' . $i;
            $temp[$i] = $confdetail[$field];
        }
        $nameConfDetail['fields'] = $temp;
        $this->view->NameconfDetail = $nameConfDetail;
        $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
        $IdRegistration = $this->_getParam('registrationId');
        $this->view->studentdetail = $this->lobjAddDropSubjectModel->getstudentprofiledetails($IdRegistration);
        $lobjcountry = $this->lobjUser->fnGetCountryList();
        $this->studentprofileForm->PermCountry->addMultiOptions($lobjcountry);
        $this->studentprofileForm->CorrsCountry->addMultiOptions($lobjcountry);
        $this->studentprofileForm->OutcampusCountry->addMultiOptions($lobjcountry);
        $this->view->persdet = $StudentPersonalDet = $this->view->studentdetail;        
        if( !empty($this->view->persdet)){
            $this->view->persdet['IdApplicant'];            
            $applicationIdList =  $this->lobjqualificationsub->fngetapplicantId($this->view->persdet['IdApplicant']);
                       
            $qualificationDet = $this->lobjqualificationsub->fngetapplicantQualification($applicationIdList[0]['IdApplication']);
            $this->view->studentqualification = $qualificationDet;
            $this->view->lobjStudentApplicationForm->IdApplication->setValue($applicationIdList[0]['IdApplication']);
            
        }


        $this->view->studentsemdetail = $this->lobjAddDropSubjectModel->getstudentprofilesemdetails($StudentPersonalDet['IdStudentRegistration']);
        $this->view->studentcourses = $this->lobjAddDropSubjectModel->getstudentcourses($StudentPersonalDet['IdStudentRegistration'], $this->view->studentdetail['IdLandscape']);

        //  echo "<pre>";
        //  print_r( $this->view->studentcourses);
        $lobjCommonModel = new App_Model_Common();
        if (isset($StudentPersonalDet['PermCountry']) && $StudentPersonalDet['PermCountry'] != '') {
            $PermState = $lobjCommonModel->fnGetCountryStateList($StudentPersonalDet['PermCountry']);
            foreach ($PermState as $PermStateresult) {
                $this->studentprofileForm->PermState->addMultiOption($PermStateresult['key'], $PermStateresult['value']);
            }
        }

        if (isset($StudentPersonalDet['PermState']) && $StudentPersonalDet['PermState'] != '') {
            $larrStateCityList = $lobjCommonModel->fnGetCityList($StudentPersonalDet['PermState']);
            foreach ($larrStateCityList as $PermCityresult) {
                $this->studentprofileForm->PermCity->addMultiOption($PermCityresult['key'], $PermCityresult['value']);
            }
        }

        if (isset($StudentPersonalDet['CorrsCountry']) && $StudentPersonalDet['CorrsCountry'] != '') {
            $CorrsState = $lobjCommonModel->fnGetCountryStateList($StudentPersonalDet['CorrsCountry']);
            foreach ($CorrsState as $CorrsStateresult) {
                $this->studentprofileForm->CorrsState->addMultiOption($CorrsStateresult['key'], $CorrsStateresult['value']);
            }
        }

        if (isset($StudentPersonalDet['CorrsState']) && $StudentPersonalDet['CorrsState'] != '') {
            $larrStateCityList = $lobjCommonModel->fnGetCityList($StudentPersonalDet['CorrsState']);
            foreach ($larrStateCityList as $CorrsCityresult) {
                $this->studentprofileForm->CorrsCity->addMultiOption($CorrsCityresult['key'], $CorrsCityresult['value']);
            }
        }

        if (isset($StudentPersonalDet['OutcampusCountry']) && $StudentPersonalDet['OutcampusCountry'] != '') {
            $OutcampusState = $lobjCommonModel->fnGetCountryStateList($StudentPersonalDet['OutcampusCountry']);
            foreach ($OutcampusState as $OutcampusStateresult) {
                $this->studentprofileForm->OutcampusState->addMultiOption($OutcampusStateresult['key'], $OutcampusStateresult['value']);
            }
        }

        if (isset($StudentPersonalDet['OutcampusState']) && $StudentPersonalDet['OutcampusState'] != '') {
            $larrStateCityList = $lobjCommonModel->fnGetCityList($StudentPersonalDet['OutcampusState']);
            foreach ($larrStateCityList as $OutcampusCityresult) {
                $this->studentprofileForm->OutcampusCity->addMultiOption($OutcampusCityresult['key'], $OutcampusCityresult['value']);
            }
        }
        if (isset($StudentPersonalDet['HomePhone']) && $StudentPersonalDet['HomePhone'] != '') {
            $phonepices = explode('-', $StudentPersonalDet['HomePhone']);
            $StudentPersonalDet['HomePhonecountrycode'] = $phonepices[0];
            $StudentPersonalDet['HomePhonestatecode'] = $phonepices[1];
            $StudentPersonalDet['HomePhone'] = $phonepices[2];
        }

        if (isset($StudentPersonalDet['Fax']) && $StudentPersonalDet['Fax'] != '') {
            $phonepic = explode('-', $StudentPersonalDet['Fax']);
            $StudentPersonalDet['Faxcountrycode'] = $phonepic[0];
            $StudentPersonalDet['Faxstatecode'] = $phonepic[1];
            $StudentPersonalDet['Fax'] = $phonepic[2];
        }

        if (isset($StudentPersonalDet['CellPhone']) && $StudentPersonalDet['CellPhone'] != '') {
            $phonecell = explode('-', $StudentPersonalDet['CellPhone']);
            $StudentPersonalDet['CellPhonecountrycode'] = $phonecell[0];
            $StudentPersonalDet['CellPhone'] = $phonecell[1];
        }

        if (isset($StudentPersonalDet['EmergencyHomePhone']) && $StudentPersonalDet['EmergencyHomePhone'] != '') {
            $phonepices = explode('-', $StudentPersonalDet['EmergencyHomePhone']);
            $StudentPersonalDet['EmergencyHomePhonecountrycode'] = $phonepices[0];
            $StudentPersonalDet['EmergencyHomePhonestatecode'] = $phonepices[1];
            $StudentPersonalDet['EmergencyHomePhone'] = $phonepices[2];
        }

        if (isset($StudentPersonalDet['EmergencyOffPhone']) && $StudentPersonalDet['EmergencyOffPhone'] != '') {
            $phonepices = explode('-', $StudentPersonalDet['EmergencyOffPhone']);
            $StudentPersonalDet['EmergencyOffPhonecountrycode'] = $phonepices[0];
            $StudentPersonalDet['EmergencyOffPhonestatecode'] = $phonepices[1];
            $StudentPersonalDet['EmergencyOffPhone'] = $phonepices[2];
        }

        if (isset($StudentPersonalDet['EmergencyCellPhone']) && $StudentPersonalDet['EmergencyCellPhone'] != '') {
            $phonecell = explode('-', $StudentPersonalDet['EmergencyCellPhone']);
            $StudentPersonalDet['EmergencyCellPhonecountrycode'] = $phonecell[0];
            $StudentPersonalDet['EmergencyCellPhone'] = $phonecell[1];
        }
        $this->view->studentprofileForm->populate($StudentPersonalDet);
    }

    public function addstudentcontactAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $IdStudentRegistration = $this->_getParam('IdStudentRegistration');
        if ($this->_request->isPost()) { // save opeartion
            $lobjFormData = $this->_request->getPost();
            unset($lobjFormData['SameCorrespAddr']);
            unset($lobjFormData['SameOutcampusAddr']);
            unset($lobjFormData['registrationId']);
            $this->lobjAddDropSubjectModel->fnupdateStudent($lobjFormData, $IdStudentRegistration);
            echo "1";
        }
    }

}