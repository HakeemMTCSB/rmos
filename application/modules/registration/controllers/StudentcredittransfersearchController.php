<?php
class Registration_StudentcredittransfersearchController extends Base_Base { //Controller for the User Module

	private $lobjStudentcredittransfersearchForm;
	private $lobjStudentcredittransfersearch;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();	
		
	}
	public function fnsetObj(){
		
		$this->lobjStudentcredittransfersearchForm = new Registration_Form_Studentcredittransfersearch();
		$this->lobjStudentcredittransfersearch = new Registration_Model_DbTable_Studentcredittransfersearch();
	}
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$auth = Zend_Auth::getInstance();
		$lobjApplicantNameList = $this->lobjStudentcredittransfersearch->fnGetApplicantNameList();
		$lobjform->field5->addMultiOptions($lobjApplicantNameList);
		
		$lobjplacementtest = new Application_Model_DbTable_Placementtest(); 
		$ProgramList=$lobjplacementtest->fnGetProgramMaterList();
		$lobjform->field8->addMultiOptions($ProgramList);
		
		$larrresult = $this->lobjStudentcredittransfersearch->fngetStudentCrditTransferDtls(); //get user details
		
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->Studentcredittransfersearchpaginatorresult);
   	    	
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Studentcredittransfersearchpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Studentcredittransfersearchpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjStudentcredittransfersearch ->fnSearchStudentCrditTransferDtls( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Studentcredittransfersearchpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjStudentcredittransfersearch ->fnaddStudentsCreditTransfer($larrformData); //searching the values for the user
				
				
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Add External Credit Transfer Approval',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
			$this->_redirect( $this->baseUrl . '/registration/studentcredittransfersearch');	
			}
			
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'studentfinance' ,'controller'=>'chargemaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/registration/studentcredittransfersearch/index');
		}
	}

	
}