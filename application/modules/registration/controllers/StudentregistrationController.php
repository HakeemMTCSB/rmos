<?php
class Registration_StudentregistrationController extends Base_Base {
	
	
	public function indexAction() {
		
		$form = new Registration_Form_SearchListApplicants();
		$this->view->form = $form;
		
		$applicantDB = new Registration_Model_DbTable_ApplicantTransaction();
		

		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$applicant_list = $applicantDB->getUnRegisteredApplicant($formData);	
			
			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($applicant_list));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(20);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
			
			$form->populate($formData);
			
		}else{
			
			$applicant_list = $applicantDB->getUnRegisteredApplicant();

			$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($applicant_list));
			//$paginator->setItemCountPerPage($this->gintPageCount);
			$paginator->setItemCountPerPage(20);
			$paginator->setCurrentPageNumber($this->_getParam('page',1));
		}
				
		$this->view->paginator = $paginator;

		

	}





}
