<?php
class Registration_StudentprogramchangeController extends Base_Base {
	private $lobjStudentregistrationModel;
	private $lobjStudentregistrationForm;
	private $_gobjlog;
	
	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();			
	}
	
	public function fnsetObj(){
		$this->lobjStudentregistrationModel = new Registration_Model_DbTable_Studentprogramchange();
		$this->lobjStudentregistrationForm = new Registration_Form_Studentprogramchange();		
	}
	
	public function indexAction() {
		$this->view->lobjform = $this->lobjform; 
		$larrresult = $this->lobjStudentregistrationModel->fngetStudentApplicationDetails();
		$studentlist = $this->lobjStudentregistrationModel->fnGetApplicantNameList();
		$this->view->lobjform->field5->addMultiOptions($studentlist);
				
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->progstudentregistrationpaginatorresult);
		
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->progstudentregistrationpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->progstudentregistrationpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost ('Search')) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ($larrformData)) {
				$larrresult = $this->lobjStudentregistrationModel->fnSearchStudentApplication( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->progstudentregistrationpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/registration/studentprogramchange/index');
		}		
	}
	
	public function studentprogramchangeAction() { //title
		$this->view->lobjStudentRegForm = $this->lobjStudentregistrationForm;
		$ldtsystemDate = date ('Y-m-d H:i:s');
		$auth = Zend_Auth::getInstance();
	
		$lintidapplicant = $this->_getParam('idapplicant');
		$studentdetails = $this->view->studentdetails = $this->lobjStudentregistrationModel->getCompleteStudentDetails($lintidapplicant);
		$this->view->idapplicant  = $lintidapplicant;		
		$prgmlist = $this->lobjStudentregistrationModel->getProgramDropDown($studentdetails['IdProgram']);
		$this->view->lobjStudentRegForm->IdProgram->addMultiOptions($prgmlist);		
		$this->view->lobjStudentRegForm->UpdDate->setValue( $ldtsystemDate );
		$this->view->lobjStudentRegForm->UpdUser->setValue( $auth->getIdentity()->iduser);	
		$this->view->lobjStudentRegForm->IdProgram->setValue($studentdetails['IdProgram']);	
		$this->view->IdProgramOld = $studentdetails['IdProgram'];
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			if ($this->lobjStudentregistrationForm->isValid ( $larrformData )) {
				$larrstudentreg = $this->lobjStudentregistrationModel->fnAddBackupdata($larrformData,$lintidapplicant);
				
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Add Student Program Change Request Id=' . $lintidapplicant,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
				//$larrstudentreg = $this->lobjStudentregistrationModel->fnUpdateStudentApplication($larrformData['IdProgram'],$lintidapplicant);
				$this->_redirect( $this->baseUrl . '/registration/studentprogramchange/index');				
			}
		}
    }	
}