<?php
class Registration_StudentsubjectsController extends Base_Base { //Controller for the User Module

	private $lobjStudentsubjects;
	private $_gobjlog;
	
	public function init() { //initialization function
		
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->fnsetObj();	
		
	}
	public function fnsetObj(){
		
		$this->lobjStudentsubjects = new Registration_Model_DbTable_Studentsubjects();
	}
	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		
		$lobjApplicantNameList = $this->lobjStudentsubjects->fnGetApplicantNameList();
		$lobjform->field5->addMultiOptions($lobjApplicantNameList);
		
		$lobjSubjectsNameList = $this->lobjStudentsubjects->fnGetSubjectsNameList(); 
		$lobjform->field8->addMultiOptions($lobjSubjectsNameList);
		
		$lobjSubjectsCodeList = $this->lobjStudentsubjects->fnGetSubjectsCodeList(); 
		$lobjform->field1->addMultiOptions($lobjSubjectsCodeList);
		
		$larrresult = $this->lobjStudentsubjects->fngetStudentCrditTransferDtls(); //get user details
		
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->Studentsubjectssearchpaginatorresult);
   	    	
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Studentsubjectssearchpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->Studentsubjectssearchpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjStudentsubjects ->fnSearchStudentSubjects( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Studentsubjectssearchpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			//echo "<PRE>";
			//print_r($larrformData);die();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjStudentsubjects ->fnaddStudentSubjetcs($larrformData); //searching the values for the user
				
				$auth = Zend_Auth::getInstance();
				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Add Student Registered Subjects Approval',
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log
				
			$this->_redirect( $this->baseUrl . '/registration/studentsubjects');	
			}
			
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'studentfinance' ,'controller'=>'chargemaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/registration/studentsubjects/index');
		}
	}

	
}