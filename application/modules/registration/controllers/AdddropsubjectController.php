<?php
class Registration_AdddropsubjectController extends Base_Base { //Controller for the User Module

	private $lobjAddDropSubjectForm;
	private $lobjAddDropSubjectModel;
	private $lobjUser;
	private $_gobjlog;
	public function init() { //initialization function
		$this->fnsetObj();
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
			
	}
	public function fnsetObj(){

		$this->lobjAddDropSubjectForm = new Registration_Form_Adddropsubject();
		$this->lobjAddDropSubjectModel = new Registration_Model_DbTable_Adddropsubject();
		$this->lobjUser = new GeneralSetup_Model_DbTable_User();
		$this->lobjschemesetupmodel = new GeneralSetup_Model_DbTable_Schemesetup();
		$this->lobjintake = new GeneralSetup_Model_DbTable_Intake();
		$this->Branchofficevenue = new GeneralSetup_Model_DbTable_Branchofficevenue();
		$this->lobjSemesterModel = new GeneralSetup_Model_DbTable_Semester();
	}
	
	
 	public function indexAction()
    {
        // action body
        $this->view->title = $this->view->translate("Add & Drop Course");
        		
        $registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');	
		
        $form = new Registration_Form_StudentSearchAddDrop(array ('locale' => $locale));
		$this->view->form = $form;
		
			if ($this->_request->isPost()){
			
				if($form->isValid($_POST)) {
					$formData = $this->_request->getPost ();
						
					$this->view->IdProgramScheme = $formData["IdProgramScheme"];
					  
					$form->populate($formData);
					$this->view->form = $form;	
											
					//get list of registered student
					$studentDB = new Registration_Model_DbTable_Studentregistration();
					$student_list = $studentDB->getStudentList($formData);
					$this->view->student_list = $student_list;
				}
			}
    }

	public function adddropsubjectlistAction() { //Action for the updation and view of the  details
		$this->view->lobjAddDropSubjectForm = $this->lobjAddDropSubjectForm;
		$lintIdApplication = ( int ) $this->_getParam ( 'idapplicant' );
		$lintIdStudentRegistration = ( int ) $this->_getParam ( 'IdStudentRegistration' );
		$this->view->IdApplication = $lintIdApplication;

		$studentdetails = $this->view->studentdetails = $this->lobjAddDropSubjectModel->getCompleteStudentDetails($lintIdApplication);

		$stdentpresentsemester = $this->view->presentsemester = $this->lobjAddDropSubjectModel->getPresentSemester($lintIdApplication);

		$idlandscape =  $this->view->studentdetails['IdLandscape'];
		$this->view->subjectdetails = $this->lobjAddDropSubjectModel->SubjectDetails($lintIdStudentRegistration,$idlandscape);

		$this->view->compsubjectdetails = $this->lobjAddDropSubjectModel->CompSubjectDetails($lintIdStudentRegistration,$idlandscape);

		$this->view->dropsubjectdetails = $this->lobjAddDropSubjectModel->DropSubjectDetails($lintIdStudentRegistration,$idlandscape);


		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjAddDropSubjectForm->UpdDate->setValue( $ldtsystemDate );
		$this->view->lobjAddDropSubjectForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		$this->view->lobjAddDropSubjectForm->IdStudentRegistration->setValue($lintIdStudentRegistration);


		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjAddDropSubjectForm->isValid ( $larrformData )) {
				$larrdeletedRegisteredsubjects= $this->lobjAddDropSubjectModel->fnDeletedRegisteredSubject($larrformData);

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Add-Drop Subject Edit Id=' . $lintIdApplication,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/registration/adddropsubject/adddropsubjectlist/idapplicant/'.$lintIdApplication.'/IdStudentRegistration/'.$lintIdStudentRegistration);
			}
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Add' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjAddDropSubjectForm->isValid ( $larrformData )) {
				$larrAddRegisteredsubjects= $this->lobjAddDropSubjectModel->fnAddSubjects($larrformData);

				// Write Logs
				$priority=Zend_Log::INFO;
				$larrlog = array ('user_id' => $auth->getIdentity()->iduser,
								  'level' => $priority,
								  'hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
				                  'time' => date ( 'Y-m-d H:i:s' ),
				   				  'message' => 'Add-Drop Subject Edit Id=' . $lintIdApplication,
								  'Description' =>  Zend_Log::DEBUG,
								  'ip' => $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) );
				$this->_gobjlog->write ( $larrlog ); //insert to tbl_log

				$this->_redirect( $this->baseUrl . '/registration/adddropsubject/adddropsubjectlist/idapplicant/'.$lintIdApplication.'/IdStudentRegistration/'.$lintIdStudentRegistration);
			}
		}
	}

	public function dropcourseAction(){
		$lintIdStudentRegistration = $this->_getParam ( 'registrationid' );
		$studentdetail = $this->lobjAddDropSubjectModel->fngetregstudentdetail($lintIdStudentRegistration);
		$studentsemdetail = $this->lobjAddDropSubjectModel->fngetstudentsem($studentdetail['IdStudentRegistration']);
		if($studentsemdetail[0]['SemesterCode']!=''){
			$studentdetail['SemesterCode'] = $studentsemdetail[0]['SemesterCode'];
		}
		else if($studentsemdetail[0]['SemesterMainName']!=''){
			$studentdetail['SemesterCode'] = $studentsemdetail[0]['SemesterMainCode'];
		}
		$studentdetail['DefinitionDesc'] = $studentsemdetail[0]['DefinitionDesc'];
		$this->view->studentdetails = $studentdetail;

		if(isset($studentdetail['IdLandscape'])){
			$studentcurrentsem = $this->lobjAddDropSubjectModel->fngetstudentcurrentsem($studentdetail['IdStudentRegistration']);

			if(isset($studentcurrentsem[0]['idSemester'])&&($studentcurrentsem[0]['idSemester']!=0 || $studentcurrentsem[0]['idSemester']!='')){
				$studentcourse = $this->lobjAddDropSubjectModel->fngetstudentcourse($studentdetail['IdLandscape'],$studentcurrentsem[0]['idSemester'],$studentcurrentsem[0]['currentsem'],$studentdetail['IdStudentRegistration']);
			}
			else if(isset($studentcurrentsem[0]['IdSemesterMain'])&&($studentcurrentsem[0]['IdSemesterMain']!=0 || $studentcurrentsem[0]['IdSemesterMain']!='')){
				$studentcourse = $this->lobjAddDropSubjectModel->fngetstudentcourse($studentdetail['IdLandscape'],$studentcurrentsem[0]['IdSemesterMain'],$studentcurrentsem[0]['currentsem'],$studentdetail['IdStudentRegistration']);
			}
			$this->view->studentcourse = $studentcourse;
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Drop' )){
			$formdata = $this->_request->getPost();
			for($i=0;$i<count($formdata['IdStudentRegSubjects']);$i++){
				if(isset($formdata['drop']) && isset($formdata['drop'][$i]) && $formdata['drop'][$i] == "on" ){
					$this->lobjAddDropSubjectModel->fndropcourse($formdata['IdStudentRegSubjects'][$i]);
				}
			}
			$this->_redirect( $this->baseUrl . '/registration/adddropsubject/');
		}

	}

	public function addcourseAction(){
		$lintIdStudentRegistration = $this->_getParam ( 'registrationid' );
		$studentdetail = $this->lobjAddDropSubjectModel->fngetregstudentdetail($lintIdStudentRegistration);
		$studentsemdetail = $this->lobjAddDropSubjectModel->fngetstudentsem($studentdetail['IdStudentRegistration']);
		if($studentsemdetail[0]['SemesterCode']!=''){
			$studentdetail['SemesterCode'] = $studentsemdetail[0]['SemesterCode'];
		}
		else if($studentsemdetail[0]['SemesterMainName']!=''){
			$studentdetail['SemesterCode'] = $studentsemdetail[0]['SemesterMainCode'];
		}
		else{
			$studentdetail['SemesterCode'] = '';
		}
		$studentdetail['DefinitionDesc'] = $studentsemdetail[0]['DefinitionDesc'];
		$this->view->studentdetails = $studentdetail;
		//		echo "<pre>";
		//		print_r($studentdetail);
		$studentcurrentsem = $this->lobjAddDropSubjectModel->fngetcurrentsemester($studentdetail['IdStudentRegistration']);
		$studentcompletedsem = $this->lobjAddDropSubjectModel->fngetcompletedsemester($studentdetail['IdStudentRegistration']);
		//$studentfuturesem    =$this->lobjAddDropSubjectModel->fngetfuturesemester($studentdetail['IdStudentRegistration']);
		$count = 1;
		for($i=0;$i<count($studentcompletedsem);$i++){
			if($count<=count($studentcompletedsem)){
				$allstudentcourses['sem'.$count] = $this->lobjAddDropSubjectModel->fngetcourse($studentdetail['IdLandscape'],$count,$studentcompletedsem[$i]);
				$count++;
			}
		}

		$index = count($studentcompletedsem)+1;
		$allstudentcourses['sem'.$index] = $this->lobjAddDropSubjectModel->fngetcourse($studentdetail['IdLandscape'],(count($studentcompletedsem)+1),$studentcurrentsem[0]);
		//		$index = count($studentcompletedsem)+2;
		//		for($i=0;$i<count($studentfuturesem);$i++){
		//			$allstudentcourses['sem'.$index] = $this->lobjAddDropSubjectModel->fngetcourse($studentdetail['IdLandscape'],$index,$studentfuturesem[$i]);
		//			$index++;
		//		}
		$reglist = 0;
		$counttotalsem = count($allstudentcourses);
		for($i = 1;$i <= $counttotalsem;$i++){
			$counttotalsub = count($allstudentcourses['sem'.$i]);
			for($j = 0; $j<($counttotalsub-1);$j++){
				$alreadyregistered['sem'.$i][$j] = $this->lobjAddDropSubjectModel->fngetalreadyregsub($allstudentcourses['sem'.$i],$allstudentcourses['sem'.$i][$j]['IdSubject'],$studentdetail['IdStudentRegistration']);
				$reglist++;
			}
		}

		for($i=1;$i<=$counttotalsem;$i++){
			$counttotalsub = count($allstudentcourses['sem'.$i]);
			for($j = 0; $j<($counttotalsub-1);$j++){
				$k=0;
				for($l = 0; $l<count($alreadyregistered['sem'.$i][$j]); $l++){
					if(isset($alreadyregistered['sem'.$i][$j][$l]) && $alreadyregistered['sem'.$i][$j][$l]!=''){
						$allstudentcourses['sem'.$i][$j]['Register'] = "Registered" ;
						$allstudentcourses['sem'.$i][$j]['Registeredby'] = $alreadyregistered['sem'.$i][$j][$l]['loginName'];
						$allstudentcourses['sem'.$i][$j]['RegisteredDate'] = $alreadyregistered['sem'.$i][$j][$l]['UpdDate'];
						if(isset($allstudentcourses['sem'.$i]['idSemester'])){
							$allstudentcourses['sem'.$i][$j]['idSemester'] = $allstudentcourses['sem'.$i]['idSemester'];
						}
						else if(isset($allstudentcourses['sem'.$i]['IdSemesterMain'])){
							$allstudentcourses['sem'.$i][$j]['IdSemesterMain'] = $allstudentcourses['sem'.$i]['IdSemesterMain'];
						}

					}
					else{
						$allstudentcourses['sem'.$i][$j]['Register'] = "Unregistered" ;
						if(isset($allstudentcourses['sem'.$i]['idSemester'])){
							$allstudentcourses['sem'.$i][$j]['idSemester'] = $allstudentcourses['sem'.$i]['idSemester'];
						}
						else if(isset($allstudentcourses['sem'.$i]['IdSemesterMain'])){
							$allstudentcourses['sem'.$i][$j]['IdSemesterMain'] = $allstudentcourses['sem'.$i]['IdSemesterMain'];
						}
					}
				}
				$k++;
			}
			if(isset($allstudentcourses['sem'.$i]['idSemester'])){
				unset($allstudentcourses['sem'.$i]['idSemester']);
			}
			else if(isset($allstudentcourses['sem'.$i]['IdSemesterMain'])){
				unset($allstudentcourses['sem'.$i]['IdSemesterMain']);			}
					
		}
		$k = 0;
		for($i=1;$i<=$counttotalsem;$i++){
			$totalcourse[$k]=$allstudentcourses['sem'.$i];
			$k++;
		}
		$k=0;
		for($i=0;$i<count($totalcourse);$i++){
			for($j=0;$j<count($totalcourse[$i]);$j++){
				if($totalcourse[$i][$j]!=NULL){
					$totalcourses[$k] = $totalcourse[$i][$j];
					$k++;
				}
			}
		}
		$this->view->totalcourses = $totalcourses;

		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$auth = Zend_Auth::getInstance();
		$updUser =	$auth->getIdentity()->iduser;
		if ($this->_request->isPost () && $this->_request->getPost ( 'Add' )){
			$formdata = $this->_request->getPost ();
			for($i=0;$i<count($formdata['IdSubjects']);$i++){
				if(isset($formdata['add']) && isset($formdata['add'][$i]) && $formdata['add'][$i] == "on" ){
					$data['IdStudentRegistration'] = $studentdetail['IdStudentRegistration'];
					$data['IdSubject'] = $formdata['IdSubjects'][$i];
					if(isset($studentcurrentsem[0]['IdSemesterMain']) && $studentcurrentsem[0]['IdSemesterMain'] != ''){
						$data['IdSemesterMain'] = $studentcurrentsem[0]['IdSemesterMain'];
					}
					else if(isset($studentcurrentsem[0]['idSemester']) && $studentcurrentsem[0]['idSemester'] != ''){
						$data['IdSemesterDetails'] = $studentcurrentsem[0]['idSemester'];
					}
					$data['UpdUser'] = $updUser;
					$data['UpdDate'] = $ldtsystemDate;
					$this->lobjAddDropSubjectModel->fnaddcourse($data);
				}
			}
			$this->_redirect( $this->baseUrl . '/registration/adddropsubject/');
		}

	}
	
	function subjectlistAction(){
			
		$this->view->title = "Add drop subject - Subject List";
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');	
		
		$form = new GeneralSetup_Form_SearchCourse(array ('locale' => $locale));
		$this->view->form = $form;
		
		//echo '<pre>';
		
				
				
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			if ($form->isValid($formData)) {
				
				$form->populate($formData);
				
				$progDB= new GeneralSetup_Model_DbTable_Program();
				$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
				$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
				$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
				$subjectOfferDb = new GeneralSetup_Model_DbTable_Subjectsoffered();
				
				//Subject list base on Program Landscape from the selected proragm 
				if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
					$programs = $progDB->fngetProgramDetails($formData["IdProgram"]);
				}
				
				
				$i=0;
				$j=0;
				$allsemlandscape = '';
				$allblocklandscape = '';
				foreach ($programs as $key => $program){
					$activeLandscape=$landscapeDB->getAllActiveLandscape($program["IdProgram"]);
					foreach($activeLandscape as $actl){
						if($actl["LandscapeType"]==43){
							$allsemlandscape[$i] = $actl["IdLandscape"];						
							$i++;
						}elseif($actl["LandscapeType"]==44){
							$allblocklandscape[$j] = $actl["IdLandscape"];
							$j++;
						}
					}
				}
				
				//print_r($allsemlandscape);
				
				if(is_array($allsemlandscape))
					$subjectsem=$landsubDB->getMultiLandscapeCourse($allsemlandscape,$formData);
				if(is_array($allblocklandscape))
					$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape);
				
				if(is_array($allsemlandscape) && is_array($allblocklandscape)){
					$subjects=array_merge( $subjectsem , $subjectblock );
				}else{
					if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
						$subjects=$subjectsem;
					}
					elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
						$subjects=$subjectblock;
					}		
				}				
				
						
				if(isset($subjects) &&   (count($subjects) > 0)) {
					
					$i=0;
					foreach($subjects as $subject){			
							
						//get total student register this subject
						$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
						$total_student = $subjectRegDB->getTotalRegister($subject["IdSubject"],$formData["IdSemester"]);
						$subject["total_student"] = $total_student;				
						$subject["IdSemester"] = $formData["IdSemester"];
						
						/*//get total group creates
						$courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
						$total_group = $courseGroupDb->getTotalGroupByCourse($subject["IdSubject"],$formData["IdSemester"]);
						$subject["total_group"] = $total_group;
						
						
						//get total no of student has been assigned
						$total_assigned = $subjectRegDB->getTotalAssigned($subject["IdSubject"],$formData["IdSemester"]);
						$total_unassigned = $subjectRegDB->getTotalUnAssigned($subject["IdSubject"],$formData["IdSemester"]);
						$subject["total_assigned"] = $total_assigned;
						$subject["total_unassigned"] = $total_unassigned;*/
						
						//get subject offer or not
						$subject_offer = $subjectOfferDb->getSubjectsOfferBySemester($formData["IdSemester"],$subject["IdSubject"]);
						
						if(is_array($subject_offer)){
			         		$subject['status_name']=$this->view->translate("Offered");
			         		$subject['status']=1;
			         	}else{
			         		$subject['status_name']=$this->view->translate("Not Offered");
			         		$subject['status']=2;
			         	}
				         						
						$subjects[$i]=$subject;
						
					$i++;	
					}//end foreach
				}//end count subject
							
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($subjects));
				$paginator->setItemCountPerPage(1000);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));
				
				$this->view->list_subject = $paginator;
				
				
				
			}//end if
			
		}
		
	}
	
	function droplistAction(){
		
		$this->view->title = "Drop Student Register from  ".$this->_getparam("subjectcode");
		
		
					
		$progDB= new GeneralSetup_Model_DbTable_Program();
		$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
		$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
		$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
		$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
		
		$this->view->idSubject=$this->_getparam("idSubject");
		$this->view->IdCollege=$this->_getparam("IdCollege",0);
		$this->view->idSemester=$this->_getparam("idSemester");
		$this->view->subjectcode=$this->_getparam("subjectcode");
		//$this->view->student_list = $subjectRegDB->getRegStudents($this->_getparam("idSubject"),$this->_getparam("idSemester"),$this->_getparam("IdCollege"));
		//print_r($students);	
		
		$form = new Registration_Form_SearchAddDropStudent(array('idCollege'=>$this->_getparam("IdCollege")));
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
		    $form->populate($formData);
		    
			$student_list = $subjectRegDB->getRegStudents($this->_getparam("idSubject"),$this->_getparam("idSemester"),$this->_getparam("IdCollege"),$formData);
			
		}else{
			$student_list = $subjectRegDB->getRegStudents($this->_getparam("idSubject"),$this->_getparam("idSemester"),$this->_getparam("IdCollege"));
		}
		
		$this->view->student_list = $student_list;
	}
	
	function dropbysubjectAction(){
	    $msg = 1;
	    $auth = Zend_Auth::getInstance();
		
		$stack_failed = array();
		$subjectRegDB = new Registration_Model_DbTable_Studentregsubjects();
		$landscapeBlockSubjectDB = new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
			
		if ($this->_request->isPost()){
			
			    $formData =  $this->_request->getPost();
			   
			     for($i=0; $i<count($formData["IdStudentRegSubjects"]);  $i++){		
			     
						$IdStudentRegSubjects = $formData["IdStudentRegSubjects"][$i];
												
						$return_val = $subjectRegDB->dropRegisterSubjects($IdStudentRegSubjects);
						
						if(isset($return_val['ERROR'])){
						  $msg = urldecode($return_val['ERROR']);
						}
			     }
  
		$this->gobjsessionsis->flash = array('message' => 'Drop Success', 'type' => 'success');
		$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'adddropsubject', 'action'=>'droplist','idSubject'=>$formData["idSubject"],'subjectcode'=>$formData["subjectcode"],'idSemester'=>$formData["idSemester"],'IdCollege'=>$formData["IdCollege"],'msg'=>$msg),'default',true));
		}
	}
	
	
	function addStudentListAction(){
		
		$this->view->title = "Add Student to course  ".$this->_getparam("subjectcode");
		
		if($this->_getparam("msg")==1){
			$this->view->noticeMessage="Add Subject Success";
		}
		
		$this->view->idSubject=$this->_getparam("idSubject");		
		$this->view->idSemester=$this->_getparam("idSemester");
		$this->view->subjectcode=$this->_getparam("subjectcode");
		
		$form = new Registration_Form_SearchAddDropStudent();
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
		    $form->populate($formData);
		    
			//search student yg register under faculty
			$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
			$student_list = $studentRegistrationDB->getListStudent($formData);
			$this->view->student_list = $student_list;
		}
	}
	
	
	function addbysubjectAction(){
		
		
		$this->view->title = $this->_getparam("subjectcode"). " Add Student to course Status  ";
		
		$auth = Zend_Auth::getInstance();
		$stack_failed = array();
		$stack_success = array();
		
		if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
			
			$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
			$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
			$semesterStatusDB = new Registration_Model_DbTable_Studentsemesterstatus();
			$landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
		    $LandscapeSubjectDB = new GeneralSetup_Model_DbTable_Landscapesubject();	
			$LandscapeBlockSubject = new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
			$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
			
		
			
			    for($i=0; $i<count($formData["IdStudentRegistration"]);  $i++){	
			     
						$IdStudentRegistration = $formData["IdStudentRegistration"][$i];
						$NIM = $formData["registrationId"][$IdStudentRegistration];			

						
						//check jika subject ni samada dah diregister pada semester tersebut ke belum?						
						$subject_exist = $studentRegSubjectDB->getSubjectInfo($IdStudentRegistration,$formData["idSubject"],$formData["idSemester"]);
												
						if(!$subject_exist["IdStudentRegSubjects"]){
													
							//baru boleh register
							
							
							//---------------- semester section ----------------
							
							//cek jika dah add semester status
							$isRegisterSemester = $semesterStatusDB->isRegisteredSemesterStatus($IdStudentRegistration, $formData["idSemester"]);

							if(is_array($isRegisterSemester)){	
								//echo '<br>tak perlu add semester status<br>';
								 //jika semester ni dah pernah register amik level semester ni ikut	utk subject semester level column						
								 $current_level_semester_to_register = $isRegisterSemester['Level'];
							}else{
								
								 //cek level student
								$semesterStudi = $semesterStatusDB->getCountableRegisteredSemester($IdStudentRegistration);
								$total_countable_registered_semester = count($semesterStudi);	
								
								//check semester yang ingin diregister tu countable atau x?
								$semester = $semesterDB->getData($formData["idSemester"]);
						
								if(isset($semester) && $semester["IsCountable"]==1){	
									$current_level_semester_to_register =  	$total_countable_registered_semester+1;				
								}else{
									$current_level_semester_to_register = 0;
								}
								
								//echo '<br>add semester status<br>';
								//insert data semseter status
								$lsemstatusArr = array( 'IdStudentRegistration' => $IdStudentRegistration,									           
											            'idSemester' => 0,
											            'IdSemesterMain' => $formData["idSemester"],
														'IdBlock' => null,											
											            'studentsemesterstatus' => 130, //Register idDefType = 32 (student semester status)
								                        'Level'=>$current_level_semester_to_register,
											            'UpdDate' => date ( 'Y-m-d H:i:s'),
											            'UpdUser' => $auth->getIdentity()->iduser
						        );
						        												
								$semesterStatusDB->addData($lsemstatusArr);
									 
								
							}
							//---------------- complete semester section ----------------
							
							
							
							
							//cek student program utk dptkan landscape subject info
						    $student = $studentRegistrationDB->getInfo($IdStudentRegistration);	
														
							//semester based landscape
							if($student["LandscapeType"]==43){
								$landscapeSub = $LandscapeSubjectDB->getInfo($student["IdLandscape"],$formData["idSubject"]);
								if(isset($landscapeSub["IdLandscapeSub"])){
									$subject["IdLandscapeSub"]  = $landscapeSub["IdLandscapeSub"];	
								}else{
									$subject["IdLandscapeSub"]  = 0;
								}
								$subject["subjectlandscapetype"]  = 1;		
							}//end landscape semester

							
							
							//block
							if($student["LandscapeType"]==44){
								
								$block = $LandscapeBlockSubject->getBlockId($student["IdLandscape"],$formData["idSubject"]);
								
								
								if(isset($block["blockid"])){								
									$subject["IdBlock"]   		= $block["blockid"];
									$subject["BlockLevel"]		= $block["level"];
									$subject_parent["IdBlock"]   		= $block["blockid"];
									$subject_parent["BlockLevel"]		= $block["level"];
									
								}else{									
									$subject["IdBlock"]   		= 0;
									$subject["BlockLevel"]		= 0;
									$subject_parent["IdBlock"]   		= 0;
									$subject_parent["BlockLevel"]		= 0;
									
								}
								
							
								if($block['type']==3){ //child
									
									$subject["subjectlandscapetype"]  = 3;	
									
									//get parent subject id
									$IdLandscapeblocksubject = $block['parentId'];
									$parent = $LandscapeBlockSubject->getData($IdLandscapeblocksubject);
									
									//cek parent pernah register kat mana-mana semester tak sebelum ni?
									$isregistered = $studentRegSubjectDB->isRegistered($IdStudentRegistration,$parent['subjectid']);
									
									if(!$isregistered){
										
										//echo '<br>add parent<br>';
										//add data parent
										$subject_parent["IdStudentRegistration"]= $IdStudentRegistration;	
										$subject_parent["IdSubject"] 			= $parent['subjectid'];	
										$subject_parent["IdSemesterMain"] 		= $formData["idSemester"];		
										$subject_parent["SemesterLevel"]	= $current_level_semester_to_register;
										$subject_parent["Active"]  = 1;
										$subject_parent["UpdDate"] = date ( 'Y-m-d H:i:s');
										$subject_parent["UpdUser"] = $auth->getIdentity()->iduser;	
										$subject_parent["subjectlandscapetype"]  = 2;		
	
										
										$studentRegSubjectDB->addData($subject_parent);
									}
									
								}else if($block['type']==1){//single									
									$subject["subjectlandscapetype"]  = 1;	
								}
								
							}//end landscape block
										
							
							//echo '<br>add singel or child<br>';
							//add data single or child type
							$subject["IdStudentRegistration"] = $IdStudentRegistration;	
							$subject["IdSubject"] 			= $formData["idSubject"];	
							$subject["IdSemesterMain"] 		= $formData["idSemester"];		
							$subject["SemesterLevel"]	= $current_level_semester_to_register;							
							$subject["Active"]  = 1;
							$subject["UpdDate"] = date ( 'Y-m-d H:i:s');
							$subject["UpdUser"] = $auth->getIdentity()->iduser;										
							
							//print_r($subject);							
							$studentRegSubjectDB->addData($subject);	
							
							array_push($stack_success,array('nim'=>$NIM,'idStudentRegistration'=>$IdStudentRegistration,'comments'=>'Pendaftaran Berjaya'));	
							
							
						}else{
							
							//echo '<br>dah repernah register<br>';
							    array_push($stack_failed,array('nim'=>$NIM,'idStudentRegistration'=>$IdStudentRegistration,'comments'=>'Pendaftaran Tidak Berjaya. Subjek telah didaftarkan.')); 	
						}//end if exists
			    }
			
		}

		
		$this->view->stack_success = $stack_success;
		$this->view->stack_failed = $stack_failed;
		
	//exit;
		//$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'adddropsubject', 'action'=>'add-student-list','idSubject'=>$formData["idSubject"],'subjectcode'=>$formData["subjectcode"],'idSemester'=>$formData["idSemester"],'msg'=>1),'default',true));
		
	}
	
	
 	public function ajaxSemesterInfoAction(){
    	
    	$this->_helper->layout()->disableLayout();
    	
    	$semester_id = $this->_getParam('semester_id', 0);
    	
    	
    	if ($this->getRequest()->isPost()) {
		
			$ajaxContext = $this->_helper->getHelper('AjaxContext');
			$ajaxContext->addActionContext('view', 'html');
			$ajaxContext->initContext();

			$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
			$semester = $semesterDB->getSemesterCourseRegistration($semester_id);
			
			$ajaxContext->addActionContext('view', 'html')
						->addActionContext('form', 'html')
						->addActionContext('process', 'json')
						->initContext();
		
			$json = Zend_Json::encode($semester);		
			echo $json;
			exit();
    	}
    }
    
    public function courseListAction(){
    	
	    	$this->view->title = $this->view->translate("Student Registered Courses");
			
			$registry = Zend_Registry::getInstance();
			$locale = $registry->get('Zend_Locale');
			$this->view->locale = $locale;
			
			$registrationId = $this->_getParam('id');
			$this->view->registrationId = $registrationId;
				
			//get student info
			$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
			$student = $studentRegistrationDB->getData($registrationId);
			
			
	         
	         //cek landscape
	         $landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
	         $landscape = $landscapeDb->getData($student["IdLandscape"]);
	         $this->view->landscape = $landscape;
	         
	         if($landscape["LandscapeType"]==43) {//Semester Based         	
	         	
	         	//get total registered semester 
	         	$studentSemesterDB = new Registration_Model_DbTable_Studentsemesterstatus();
	         	$semester = $studentSemesterDB->getRegisteredSemester($registrationId);
	         	
	         	foreach($semester as $index=>$sem){
	         		
	         		//get drop period	         		
					$issemopen = $studentSemesterDB->checkSemesterCourseRegistration($sem["IdSemesterMain"],$student["IdProgram"]);
		         	if(is_array($issemopen)){
						$semester[$index]['drop']=1;
					}else{
						$semester[$index]['drop']=0;
					}
	         	}
	         	$this->view->semester = $semester;
	         	echo '<pre>';
	         	print_r($semester);	  		
	         }
    }
    
    
    public function dropAction(){
    	//echo 'course dropped';
    	$IdStudentRegSubjects = $this->_getParam('idCourse');
    	$registrationId = $this->_getParam('id');
    	
    	$dropDB = new Registration_Model_DbTable_Adddropsubject();
    	$info = $dropDB->fndropcourse($IdStudentRegSubjects);
    	
    	$regSubDB = new Registration_Model_DbTable_Studentregsubjects();
    	$regSubDB->getSubjectRegRaw($IdStudentRegSubjects);
    	
    	$regSubDB->saveHistory(array($info),"DROP");
    	
    	$this->_redirect( $this->url(array('module'=>'registration','controller'=>'adddropsubject', 'action'=>'course-list','id'=>$registrationId),'default',true) );
    	exit;
    }
    
    public function addAction(){
    	//echo 'add course';
    	$this->view->title = $this->view->translate("Add Course(s)");
    	
    	$auth = Zend_Auth::getInstance();
    	
    	$registrationId = $this->_getParam('id');
    	$this->view->IdStudentRegistration = $registrationId;
    	$idSemester = $this->_getParam('idSemester');
    	$this->view->idSemester = $idSemester;
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
    	
    	
	    
	    if ($this->getRequest()->isPost()) {
			
			$formData = $this->getRequest()->getPost();
		
			echo '<pre>';
			print_r($formData);
			$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
			
			for($i=0; $i<count($formData["IdLandscapeSub"]);  $i++){
				 	
				 	$IdLandscapeSub  = $formData["IdLandscapeSub"][$i];	
				 	
				 	//get details subjects
				
					if($formData["LandscapeType"]==43) {//Semester Based Landscape
						
						$landscapeSubjectDb  = new GeneralSetup_Model_DbTable_Landscapesubject();
						$subjects_info = $landscapeSubjectDb->getLandscapeCourseInfo($IdLandscapeSub);
						$subject["IdLandscapeSub"] = $subjects_info["IdLandscapeSub"];
						$subject["IdSubject"] = $subjects_info["IdSubject"];
															
					}else if($formData["LandscapeType"]==44) { //Block Based Landscape
							
						//$subjects_info = $landscapeSubjectDb->getBlockCourseInfo($IdLandscapeSub);
							
						//$subject["IdSubject"] = $subjects_info["subjectid"];
						//$subject["IdBlock"]   = $subjects_info["blockid"];
						//$subject["BlockLevel"]= $subjects_info["BlockLevel"];
					}
					
					$subject["IdStudentRegistration"] = $formData['IdStudentRegistration'];							
					$subject["IdSemesterDetails"] = $formData['idSemester'];
					$subject["IdSemesterMain"] = $formData['idSemester'];
					$subject["SemesterLevel"]= ''; //TODO
					$subject["Active"]= 1;
					$subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
					$subject["UpdUser"]   = $auth->getIdentity()->iduser;														
					print_r($subject);
					//$studentRegSubjectDB->addData($subject);
			}
	    }else{
	    	
	    	
	    		//get student info
				$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration();
				$student = $studentRegistrationDB->getData($registrationId);
				
				//get subject offered based on landscape
				$landscapeDb = new GeneralSetup_Model_DbTable_Landscape();
				$landscape = $landscapeDb->getLandscapeDetails($student['IdLandscape']);
				$this->view->landscape = $landscape;
				
				$landscapeSubjectDb  = new GeneralSetup_Model_DbTable_Landscapesubject();
							      
			    if($landscape["LandscapeType"]==43) {//Semester Based Landscape	
				     		   
					 $subjectlist = $landscapeSubjectDb->getOfferedCourseList($student['IdLandscape'],$idSemester,$student['IdProgramMajoring']);
					 
			    }else if($landscape["LandscapeType"]==44){ //Block Landscape							 
			     
			     	 //get Landscape Block by Semester, if new student semester 1 and  block 1	 
			     	 $subjectlist = array();    	 
			    }		    
			    $this->view->subject_list = $subjectlist;
			    
			    
			    
			    echo '<pre>';
			    print_r($subjectlist);
	    }
	    
	    
   
    }
}
