<?php
class Registration_ExamRegistrationController extends Zend_Controller_Action {
	
	public function indexAction() {
		
		$this->view->title = $this->view->translate("Exam Registration");
			
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
				
		$form = new Registration_Form_ExamRegistration(array ('locale' => $locale));
		$this->view->form = $form;
		
			if ($this->_request->isPost()){
			
				if($form->isValid($_POST)) {
					$formData = $this->_request->getPost ();
					
					$this->view->idSemester= $formData['IdSemester'];
					$this->view->idProgram = $formData['IdProgram'];					
					//echo '<pre>';
					//print_r($formData);
					
					$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();	
									
					$examRegDB = new Registration_Model_DbTable_ExamRegistration();
					
					$ec_list = $examCenterDB->getExamCenterList($formData);
					
					foreach($ec_list as $index=>$center){
						
						$student = $examRegDB->getStudentRegister($center['ec_id'],$center['er_idSemester'],$center['er_idSubject']);
						
						if(count($student)>0){
							$total_student = count($student);
						}else{
							$total_student= 0;
						}
						$ec_list[$index]['total_student'] = $total_student;
					}
					
					$this->view->ec_list = $ec_list;
					
					//print_r($ec_list);
				}
				
			}
				
	}
	
	public function listStudentAction() {
		
		$this->view->title = $this->view->translate("Exam Registration Details");
	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
		
		$idSemester = $this->_getParam('idSemester', 0);
		$idSubject = $this->_getParam('idSubject', 0);
		$ecid = $this->_getParam('ecid', 0);
		$fr = $this->_getParam('fr', null);
		
		$this->view->idSemester= $idSemester;
		$this->view->cur_ecid = $ecid;
		$this->view->idSubject = $idSubject;
		$this->view->fr = $fr;
		
		if($fr){
			$this->view->backUrl = $this->view->url(array('module'=>'examination','controller'=>'assign-exam-center', 'action'=>'index'),'default',true);
		}else{
			$this->view->backUrl = $this->view->url(array('module'=>'registration','controller'=>'exam-registration', 'action'=>'index'),'default',true);
		}
		//get info semester
    	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
    	$this->view->semester = $semesterDB->fngetSemestermainDetails($idSemester);

    	//get info subject
    	$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    	$this->view->subject = $subjectDB->getData($idSubject);
    
    	//exam center
    	$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
    	$this->view->ec = $examCenterDB->getDatabyId($ecid);    	
		$this->view->country = $examCenterDB->getExamCenterCountry();
    	
		//get list student register exam center
		$examRegDb = new Registration_Model_DbTable_ExamRegistration();
		$student_list = $examRegDb->getStudentRegister($ecid,$idSemester,$idSubject);
		$this->view->student_list = $student_list;
		
        
		//echo '<pre>';
		//print_r($student_list);
	}
	
	
	public function addAction() {
		
		$this->view->title = $this->view->translate("Exam Registration : Add Student");
			
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
				
		$form = new Registration_Form_ExamRegistrationSearch(array ('locale' => $locale));
		$this->view->form = $form;
		
			if ($this->_request->isPost()){
			
				if($form->isValid($_POST)) {
					$formData = $this->_request->getPost ();
					
					$this->view->idSemester= $formData['IdSemester'];
					$this->view->idProgram = $formData['IdProgram'];
					$this->view->idSubject = $formData['IdSubject'];
		
					$studentRegSubDB = new Registration_Model_DbTable_Studentregsubjects();
	    			$students = $studentRegSubDB->searchStudentNotRegisterExam($formData);
	    			$this->view->student_list = $students;
				}
				
			}
						
		$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
		$this->view->country = $examCenterDB->getExamCenterCountry();
		
	}
	
	public function registerExamAction() {
		
		$auth = Zend_Auth::getInstance();
    
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale = $locale;
				
		
		if ($this->_request->isPost()){
			
			$formData = $this->_request->getPost ();
			
			$this->view->idSubject = $formData['IdSubject'];

			$examRegDb = new Registration_Model_DbTable_ExamRegistration();
			$studentRegSubDB = new Registration_Model_DbTable_Studentregsubjects();
			
			//print_r($formData);
			$data['er_idCountry']=$formData['idCountry'];
			$data['er_idCity']=$formData['idCity'];
			$data['er_ec_id']=$formData['ec_id'];			
			$data['er_idProgram']=$formData['idProgram'];
			$data['er_idSubject']=$formData['idSubject'];
			$data['er_status']=764; //764:Regiseterd 765:Withdraw
			$data['er_createdby']=$auth->getIdentity()->iduser;
			$data['er_createddt']= date ( 'Y-m-d H:i:s');
			
			for($i=0; $i<count($formData['IdStudentRegSubjects']); $i++){
				
				$IdStudentRegSubjects=$formData['IdStudentRegSubjects'][$i];
				
				//get srs info
				$srs_info = $studentRegSubDB->getData($IdStudentRegSubjects);
				
				$data['er_idStudentRegistration']=$srs_info['IdStudentRegistration'];
				$data['er_idSemester']=$srs_info['IdSemesterMain']; //MUST FOLLOW SRS FOR CASES LIKE AUDIT CREDIT
				
				$examRegDb->addData($data);
			}
			
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			
		}
			
		$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'exam-registration', 'action'=>'index'),'default',true));
		
			
	}
	
	public function changeCenterAction(){
		
		if ($this->_request->isPost()){		
		
			$formData = $this->_request->getPost ();
			
			$data['er_idCountry']=$formData['idCountry'];
			$data['er_idCity']=$formData['idCity'];
			$data['er_ec_id']=$formData['ec_id'];
			
			$examRegDb = new Registration_Model_DbTable_ExamRegistration();
			$examRegDb->updateData($data,$formData['er_id']);
			
			$this->gobjsessionsis->flash = array('message' => 'Data has been saved', 'type' => 'success');
			$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'exam-registration', 'action'=>'list-student','idSemester'=>$formData['idSemester'],'idSubject'=>$formData['idSubject'],'ecid'=>$formData['cur_ec_id'],'fr'=>$formData['fr']),'default',true));
		
		}
			
	}
	
	public function getSubjectAction()
	{	
    	$idSemester = $this->_getParam('idSemester', 0);
    	$idProgram = $this->_getParam('idProgram', 0);
    	
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
       	$progDB= new GeneralSetup_Model_DbTable_Program();
		$landscapeDB= new GeneralSetup_Model_DbTable_Landscape();
		$landsubDB= new GeneralSetup_Model_DbTable_Landscapesubject();
		$landsubBlkDB= new GeneralSetup_Model_DbTable_LandscapeBlockSubject();
		
		
		$i=0;
		$j=0;
		$allsemlandscape = null;
		$allblocklandscape = null;
		$subjects = array();
		
	    $activeLandscape=$landscapeDB->getAllActiveLandscape($idProgram);
			
        foreach($activeLandscape as $actl){
			if($actl["LandscapeType"]==43){
				$allsemlandscape[$i] = $actl["IdLandscape"];						
				$i++;
			}elseif($actl["LandscapeType"]==44){
				$allblocklandscape[$j] = $actl["IdLandscape"];
				$j++;
			}
		}
		
		if(is_array($allsemlandscape))
			$subjectsem=$landsubDB->getAllLandscapeCourse($allsemlandscape,null,$idSemester);
		
			
		if(is_array($allblocklandscape))
			//$subjectblock=$landsubBlkDB->getMultiLandscapeCourse($allblocklandscape,$formData);
			$subjectblock = array();
			
			
		if(is_array($allsemlandscape) && is_array($allblocklandscape)){
			$subjects=array_merge( $subjectsem , $subjectblock );
		}else{
			if(is_array($allsemlandscape) && !is_array($allblocklandscape)){
				$subjects=$subjectsem;
			}
			elseif(!is_array($allsemlandscape) && is_array($allblocklandscape)){
				$subjects=$subjectblock;
			}		
		}//if else
		
		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($subjects);
		
		echo $json;
		exit();
    }
    
	public function getCityAction()
	{	
    	$idCountry = $this->_getParam('idCountry', 0);
   	    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
        $examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
        $result = $examCenterDB->getExamCenterCity($idCountry);
       		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
    
	public function getExamCenterAction()
	{	
    	$idCountry = $this->_getParam('idCountry', 0);
    	$idCity = $this->_getParam('idCity', 0);
   	    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
                
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
         
        $examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
        $result = $examCenterDB->getExamCenter($idCountry,$idCity);
       		
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($result);
		
		echo $json;
		exit();
    }
}

?>