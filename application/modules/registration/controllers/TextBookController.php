<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 23/10/2015
 * Time: 9:37 AM
 */
class Registration_TextBookController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Registration_Model_DbTable_TextBook();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Learning Material Collection');

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $formData['Program'] = $this->view->escape(strip_tags($formData['Program']));
            $formData['ProgramScheme'] = $this->view->escape(strip_tags($formData['ProgramScheme']));
            $formData['Semester'] = $this->view->escape(strip_tags($formData['Semester']));
            $formData['Method'] = $this->view->escape(strip_tags($formData['Method']));
            $formData['Name'] = $this->view->escape(strip_tags($formData['Name']));
            $formData['Studentid'] = $this->view->escape(strip_tags($formData['Studentid']));

            if (isset($formData['search'])) {
                $form = new Registration_Form_TextBookSearch(array('programid'=>$formData['Program']));
                $this->view->form = $form;

                $list = $this->model->getTextBook($formData);
                $this->view->list = $list;

                $form->populate($formData);
            }else{
                $form = new Registration_Form_TextBookSearch();
                $this->view->form = $form;
            }
        }else{
            $form = new Registration_Form_TextBookSearch();
            $this->view->form = $form;
        }
    }

    public function updateStatusAction(){
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            if (isset($formData['send']) && count($formData['send']) > 0){
                foreach ($formData['send'] as $key => $send){
                    $data = array(
                        'lmc_status'=>$send
                    );
                    $this->model->updateTextBookCourier($data, $key);
                }

                foreach ($formData['tno'] as $key2 => $tno){
                    $data = array(
                        'lmc_trackingno'=>$tno
                    );
                    $this->model->updateTextBookCourier($data, $key2);
                }

                $this->_helper->flashMessenger->addMessage(array('success' => 'Information Saved!'));
            }else{
                $this->_helper->flashMessenger->addMessage(array('error' => 'Please Select Atleast One.'));
            }
        }

        //redirect here
        $this->_redirect($this->baseUrl . '/registration/text-book/index/');
    }
}