<?php

class Registration_CourseRegistrationController extends Base_Base
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    	            
			
        // action body
        $this->view->title = $this->view->translate("Course Registration");
        		
        $session = Zend_Registry::get('sis');
        
        $registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');

		$studentDB = new Registration_Model_DbTable_Studentregistration();
		
        $form = new Registration_Form_SearchListStudentForm(array ('locale' => $locale));
		$this->view->form = $form;

		
    	//clear the session
    	if ($this->getRequest()->isPost() && $this->_request->getPost('Clear') ) {
			unset($session->result);
    	}
    	
		if ($this->_request->isPost() && $this->_request->getPost('Search') ){
		
			unset($session->result);
			
			if($form->isValid($_POST)) {
				
				$formData = $this->_request->getPost ();
					
				$session->result = $formData;
				 
				$this->view->IdProgramScheme = $formData["IdProgramScheme"];
				  
				$form->populate($formData);
				$this->view->form = $form;	
				
				//get list of registered student				
				$student_list = $studentDB->getStudentList($formData);
				
				$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
				$paginator->setItemCountPerPage(20);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));
				$paginator->getCurrentItemCount();
				$this->view->student_list = $paginator;
			
			}
		}else{
			
			//populate by session 
	    	if (isset($session->result)) {    	
	    		$form->populate($session->result);
	    		$student_list = $studentDB->getStudentList($session->result);
				
	    		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($student_list));
				$paginator->setItemCountPerPage(20);
				$paginator->setCurrentPageNumber($this->_getParam('page',1));	    					
	    		$paginator->getCurrentItemCount();
	    		$this->view->student_list = $paginator;
	    	}
		}
		
		
    }
    
    
    public function registerCourseAction(){
    	
    			
    	//register course for new student only
    	    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	      	
    	$this->view->title = $this->view->translate("Student Course Registration");       
    	
    	$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();        
		$landscapeSubjectDb  = new GeneralSetup_Model_DbTable_Landscapesubject();		
		$semesterDB = new Registration_Model_DbTable_Semester();		
		$courseGroupDB = new GeneralSetup_Model_DbTable_CourseGroup();
		$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
		$invoiceDB = new Studentfinance_Model_DbTable_Invoice();
		$studentRegSubDB = new Registration_Model_DbTable_Studentregsubjects();
		$requisiteDB = new Registration_Model_DbTable_SubjectPrerequisite();
		$definationDB = new App_Model_General_DbTable_Definationms();
		$registrationItemDB =new Registration_Model_DbTable_RegistrationItem();
		$courseScheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
		$examRegDB = new Registration_Model_DbTable_ExamRegistration();
		$countryDB = new GeneralSetup_Model_DbTable_Country();
		$landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
		$landscapeDetailDB = new GeneralSetup_Model_DbTable_LandscapeDetail();
		$subjectequivalentDB = new GeneralSetup_Model_DbTable_Subjectequivalent();
		$sssDB = new Registration_Model_DbTable_Studentsemesterstatus();
		
    	$IdStudentRegistration = $this->_getParam('id',null); 
    	$this->view->IdStudentRegistration = $IdStudentRegistration;
    	
    	$IdSem = $this->_getParam('idSem',null); 
    	$this->view->IdSem = $IdSem;
    	
    	$this->allow_register = true;
    	$flag_future_sem = false;
    	
    	//get student info
    	$student = $studentRegistrationDb->getData($IdStudentRegistration,1);
    	$this->view->student = $student;
    	
    	//echo '<pre>';
    	//print_r($student);
    	
    	//check if new student or senior student
    	//$isnewstudent = $studentRegistrationDb->isNewStudent($IdStudentRegistration,$student['IdSemesterMain']);
    		
    	//get current semester
    	/*if($isnewstudent){
    		//new student
    		$semester = $semesterDB->getData($student['IdSemesterMain']);    				
    	}else{
    		//senior student
    		$semester = $semesterDB->getApplicantCurrentSemester(array('ap_prog_scheme'=>$student['IdProgramScheme'],'branch_id'=>$student['IdBranch']));
    		//$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'course-registration', 'action'=>'register-course-senior','id'=>$IdStudentRegistration),'default',true));
    	}*/
    	
    	
    	//get remaining credit hours or course
		$icms = new icampus_Function_Application_AcademicProgress();
		$summary = $icms->getExpectedGraduateStudent($student['IdStudentRegistration'],'portal');
		$remaining = $summary['remaining'];
	
		//yati to get solid remaining paper for student
		if($student['IdProgram']==5){ //CIFP
			//check if its includes PPP/Artcleship
			$status_taken = $studentRegSubDB->isRegisteredPaper($IdStudentRegistration);
			$remaining = ($status_taken) ? ($remaining) : ($remaining-1);
		}
		$this->view->remaining  = $remaining;
		//end get summary credit hours 
			
			
    	//get first time register semester
    	$first_semester = $sssDB->getRegisteredSemester($IdStudentRegistration,'ASC');
    	
    	if(isset($IdSem) && $IdSem!=''){
    		$semester = $semesterDB->getData($IdSem); 
    		$flag_future_sem = true;   	
    	}else{    		
    		$semester = $semesterDB->getApplicantCurrentSemester(array('ap_prog_scheme'=>$student['IdScheme'],'branch_id'=>$student['IdBranch']));
    		
    		if(isset($first_semester[0]['SemesterMainStartDate'])){
		    	if($semester['SemesterMainStartDate'] < $first_semester[0]['SemesterMainStartDate']){
		    		//get semester based on semester intake    			
		    		$semester = $semesterDB->getData($first_semester[0]['IdSemesterMaster']); 		    		
    				$flag_future_sem = true;
		    	}
    		}else{
    			//if fisrt sem eq cur sem => this is new student
    			//if no registered any semester status => this is new student set first semester as current semester
    			$first_semester[0]['SemesterMainStartDate']=$semester['SemesterMainStartDate'];    			
    			$flag_future_sem = true;
    		}
    	}    	    	
    	$this->view->semester = $semester;
					
    	
    	
		//get semester to register onwards off on 8-3-2015 change to display previous semester too
		//$this->view->register_semester_onwards = $semesterDB->getApplicantRegisterSemesterOnwards(array('ap_prog_scheme'=>$student['IdScheme'],'branch_id'=>$student['IdBranch']),$semester['SemesterMainStartDate'],$flag_future_sem);
 	 	
    	$this->view->register_semester_all = $semesterDB->getSemesterByScheme($student['IdScheme'],$student['IdBranch'],$first_semester);
		
 	 	//get registartion item
		$item = $registrationItemDB->getStudentItems($student['IdProgram'],$semester['IdSemesterMaster'], $student['IdProgramScheme'], $chargable=0);
		$this->view->item = $item;
		if(count($item)==0){
			$this->allow_register = false;
		}
		
		//get item repeat
		$item_repeat = $registrationItemDB->getRepeatRegItems($student['IdProgram'],$semester['IdSemesterMaster'], $student['IdProgramScheme'], $chargable=0);
		$this->view->item_repeat = $item_repeat;
		
		
		/* REGISTER SUBJECT POLICY CHECKING
    	 * ================================
    	 * 1) Check if Add & Drop Period is open based on activity calendar setup
    	 * 2) Credit Hour or Course MAX and MIN limit based on landscape detail setup
    	 * 3) Requisite Subject - check based on landsacpe & course master
    	 * 4) Barr 
    	 */
		
		
		//get previous semester
		//$prvSemester=$semesterDB->getPreviousSemester($semester);
			
		/*Check Barring*/
		//$barringClass = new icampus_Function_Courseregistration_BarringRelease();

		//1) checking outstanding
		//$this->view->fbarred = $barringClass->checkingOutstanding($student['IdStudentRegistration'],$prvSemester["IdSemesterMaster"]);
		
		//2) checking barring list
		//$this->view->obarred = $barringClass->checkingBarring($student['IdStudentRegistration'],$semester["IdSemesterMaster"],866);
		
		/*End Barring check*/
		
		
		
		//check level landscape policy
		$policy=null;
		if($student["LandscapeType"]==42){ //for level landscape
			$policy=$landscapeSubjectDb->levelPolicy($student['IdStudentRegistration'],$student['IdLandscape']);
			$this->view->policy = $policy;			
		}
		//end policy
		
		// get subject to register list based on landscape 	 	
		if($student['IdLandscape']==0){
 	 			//tagging landscape
				//cari by intake, program, scheme FTF/ON				
				$landscape =  $landscapeSubjectDb->getLandscape($student);					
				
				if(!$landscape){
					//redirect
					$this->_helper->flashMessenger->addMessage(array('success' => "Unable to do course registration. Student Landscape not exist."));
					$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'course-registration', 'action'=>'index'),'default',true));
				}
 	 	} 	 			 
 	 	
 	 	
 	 	//$subject_list = $landscapeSubjectDb->getLandscapeCourseList($student['IdLandscape'],$semester['IdSemesterMaster'],1,0,$prereg=null);
 	    //$subject_list = $landscapeSubjectDb->getCourseOfferNotRegistered($student['IdStudentRegistration'],$student['IdLandscape'],$semester['IdSemesterMaster'],$student["LandscapeType"],$policy);
 	    $subject_list = $landscapeSubjectDb->getSubjectOfferEqOffer($student['IdStudentRegistration'],$student['IdLandscape'],$semester['IdSemesterMaster'],$student["LandscapeType"],$policy);

 	    echo '<pre>';

        $defDb = new App_Model_General_DbTable_Definationms();
        $landscapeModel = new App_Model_General_DbTable_Landscape();
        $landscapeInfo = $landscapeModel->getData($student['IdLandscape']);
        $programDb = new App_Model_General_DbTable_Program();
        $programMajor = $programDb->fngetProgramData($student['IdProgram'],1);

        if ($programMajor['total_major'] > 0 && $landscapeInfo['MajoringType'] == 2 && $student['Specialization'] == 1 && $student['IdProgramMajoring'] != 0) {
            $landscapeDb =  new App_Model_General_DbTable_Landscapesubject();
            $majoring_courses = $landscapeDb->getMajoringCourse($student['IdProgram'],$student['IdLandscape'], $student['IdProgramMajoring']);

            if ($subject_list){
                foreach( $subject_list as $subkey => $sub ){
                    if ($sub['SubjectType'] == 'Majoring'){
                        unset($subject_list[$subkey]);
                    }
                }
            }

            ///dd($subject_list[0]);
            //dd($majoring_courses[0]);
            //exit;

            if ($majoring_courses){
                foreach ($majoring_courses as $majoring_course){
                    $getCourseType = $defDb->getData($majoring_course['SubjectType']);

                    $majoring_course['IdSubjectType'] = $majoring_course['SubjectType'];
                    $majoring_course['SubjectType'] = $getCourseType['DefinitionDesc'];

                    $subject_list[] = $majoring_course;
                }
            }
        }
 	   
 	   // print_r($subject_list);
		if(count($subject_list)>0){	
			
			//$this->view->elective_subject = $landscapeSubjectDb->getOfferedElectiveSubject($student['IdStudentRegistration'],$student['IdLandscape'],$semester['IdSemesterMaster'],$student["LandscapeType"],$policy);
			$this->view->elective_subject = $landscapeSubjectDb->getElectiveTaken($student['IdStudentRegistration'],$semester['IdSemesterMaster'],$student['IdLandscape']);
			//var_dump($this->view->elective_subject);

			//country list			
			$countryList = $countryDB->fnGetCountryListDetails();
			$this->view->countryList = $countryList;

			//get exam center list
			/*$exam_center = $examCenterDB->getExamCenterBySemester($semester['IdSemesterMaster']);
		 	$sorted_values = array();
			foreach($exam_center as $key => $value) {
			    $sorted_values[$value['CountryName']][] = array(
			        'ec_id' => $value['ec_id'],
			        'ec_name' => $value['ec_name'],
			    );
			}				
			ksort($sorted_values);
			$this->view->exam_center = $sorted_values; */
			
			
			//get subject info	for each subject
    		foreach($subject_list as $index=>$subject){
    			
	    			$allow_register = true;
	    			
	    			//ubah pulop 2-3-2015 azila suh keluarkan aje semua item yg ada kat admin sebab based on section
	    			//$item_register = $item; original 
	    			$item_register=$item_repeat;
	    			
	    			
	    			//check if Research set default item to paper
	    			if($subject['CourseType']==3){	    
	    				if($student['IdScheme']==1){	//PS			
	    					$item_register = array(array('mandatory'=>1,'item_name'=>'Paper','item_id'=>890));
	    				}
	    				if($student['IdScheme']==11){	//GS			
	    					$item_register = array(array('mandatory'=>1,'item_name'=>'Course','item_id'=>889));
	    				}	    				
	    			}
	    			
	    			
	    			//check if registered at previous semester
	    			$isRegisteredPrevSem = $studentRegSubDB->isRegisteredPrevSemester($IdStudentRegistration,$subject['IdSubject'],$semester['IdSemesterMaster']);
	    			
	    			if($isRegisteredPrevSem){ 
				  		
				  		//set subject have previouly taken
				  		$subject_list[$index]['isRegisteredPrevSem']=1;
				  	
				  		
				  		//to get total research CH registered 
				  		if($subject['IdSubjectType']==271 || $subject['CourseType']==3 ){
				  			$total_research_ch_taken = $studentRegSubDB->sumCHRegisteredPrevSemester($IdStudentRegistration,$subject['IdSubject'],$semester['IdSemesterMaster']);
				  			$subject_list[$index]['total_ch_taken']=$total_research_ch_taken;
				  		}
				  		 					  		
				  		//PS only Fail subject can repeat
				  		if($student['IdScheme']==1){
				  			
					  		if( ($isRegisteredPrevSem['exam_status']=='I') || ($isRegisteredPrevSem['exam_status']=='C' && $isRegisteredPrevSem['grade_name']=='F') ){
					  			//allow register/repeat
					  			$allow_register = true;
					  			$subject_list[$index]['allow']=1;
					  		}else{
					  			//not allow	
					  			$subject_list[$index]['allow']=0;
					  			$allow_register = false;	  			
					  		}
					  		
				  		}//end PS allow repeat
				  		
				  		
				  		//to allow register for all proram ehere exam_status = I tapi status Active => ini case absent with valid reason
				  		if(($isRegisteredPrevSem['exam_status']=='I' && $isRegisteredPrevSem['Active']==1)){
				  			$allow_register = true;				  			
				  			$subject_list[$index]['isRegisteredPrevSem']=2;
				  		}
				  						  		
				  		/* KALO BUKAN RESEARCH GUNA ITEM REPEAT WHICH IS TO ALLOW USER PILIH APE2 ITEM YG ADA DLM PROGRAM
				  		 * LAGIPUN RESEACRCH MANA ADA REPEAT
				  		 */
				  		if($subject['CourseType']!=3){ 
				  			$item_register = $item_repeat;
				  		}
				  		
				  	}else{
				  		$subject_list[$index]['allow']=1;
				  		$subject_list[$index]['isRegisteredPrevSem']=0;		
				  		$subject_list[$index]['total_ch_taken']=0;		  		
				  	}
				  	
				  	
				  	if($allow_register==true){
				  		
				  			/*--------------start-----------------*/
				  		
				  		    //get course group
				  			$group = $courseGroupDB->getGroupQuota($semester['IdSemesterMaster'],$subject['IdSubject'],$student['IdProgram'],$student['IdProgramScheme'],$student['IdBranch'],null);
						 	$subject_list[$index]['group']=$group;   			   		
			    		
						 	
			    			//get exam registration info
							$exam_registration = $examRegDB->getDataExamRegistrationBySubject($IdStudentRegistration,$semester['IdSemesterMaster'],$subject['IdSubject']);
							
							if($exam_registration){
							
								$subject_list[$index]['exam_registration']=$exam_registration;
								
								//get city based on selected country
								$city_list = $examCenterDB->getcity($exam_registration['er_idCountry']);
								$subject_list[$index]['city']=$city_list;
								
							}
						 
			    			//check if subject already register
							$isRegisterSubject = $studentRegSubDB->isRegisteredBySemesterIncludeWithdraw($IdStudentRegistration,$subject['IdSubject'],$semester['IdSemesterMaster'],1);
							if($isRegisterSubject){
								if($isRegisterSubject['Active']==3){
									$subject_list[$index]['isRegisterSubject']=3;
								}else{
									$subject_list[$index]['isRegisterSubject']=1;
								}
								$subject_list[$index]['idRegisterGroup']=$isRegisterSubject['IdCourseTaggingGroup'];
								$subject_list[$index]['IdStudentRegSubjects']=$isRegisterSubject['IdStudentRegSubjects'];
								$subject_list[$index]['IdReplaceSubject']=$isRegisterSubject['replace_subject'];
								$subject_list[$index]['credit_hour_registered']=$isRegisterSubject['credit_hour_registered'];
								$subject_list[$index]['exam_status']=$isRegisterSubject['exam_status'];
								
								if($isRegisterSubject['exam_status']=='CT' || $isRegisterSubject['exam_status']=='EX' || $isRegisterSubject['exam_status']=='U' || $isRegisterSubject['Active']==3){
									$subject_list[$index]['allow_adddrop']=false;
								}else{
									$subject_list[$index]['allow_adddrop']=true;
								}
							}else{
								$subject_list[$index]['isRegisterSubject']=0;
								$subject_list[$index]['allow_adddrop']=true;
								$subject_list[$index]['exam_status']='';
							}
											
							
							foreach($item_register as $itemkey=>$i){	
			    					 
									$item_register[$itemkey]['item_status'] = 0;//default
									
						 			//get registration item status
						 			$isRegisterItem = $studentRegSubDB->checkIsRegisterItem($IdStudentRegistration,$subject['IdSubject'],$semester['IdSemesterMaster'],$i['item_id']);
									if($isRegisterItem){
										
										if($isRegisterItem['status']==3){
											//withdraw
											$item_register[$itemkey]['isRegisterItem']=0;
											$item_register[$itemkey]['item_status']=3;
											
										}else{
											$item_register[$itemkey]['isRegisterItem']=1;
											$item_register[$itemkey]['srsd_id']=$isRegisterItem['id'];
										}
									}else{
										$item_register[$itemkey]['isRegisterItem']=0;
									}
						 	}
			    			$subject_list[$index]['item'] = $item_register;
							
						 	
						 	
						 	//check requisite
						 	//$subject_list = $this->prerequsite($IdStudentRegistration,$student,$subject,$subject_list);
				    		$requisites = $requisiteDB->getPrerequisite($student['IdLandscape'],$subject['IdSubject']);
				    		
				    		/* 0:Pass With Grade
				    		 * 1:Total credit hours
				    		 * 2: Co-requisite
				    		 */
				    		$prerequisites_status = true; //Passed
				    			
					    	if(count($requisites)>0){		    		
			 		
					    		$pre_requsite_list = array();
					    		
					    		foreach($requisites as $requisite){
					    			
						    		//get requisite type
							 		$PrerequisiteType = $requisite['PrerequisiteType'];
							 		
							 		if($PrerequisiteType==0){
							 									 			
							 			//get required subject grade				 		
							 			$required_subject = $studentRegSubDB->isRegisterPrevPass($student['IdStudentRegistration'],$requisite['IdRequiredSubject'],$semester['IdSemesterMaster']);		
						
							 			if($required_subject){
								 			if($required_subject['exam_status']=='CT' || $required_subject['exam_status']=='EX'){
									 			//lepas
								 			}else{
								 				
								 				//get grade defination					 				
								 				$grade1 = $definationDB->getDataByDefCode(22,trim($required_subject['grade_name']));	//grade yg student dapat	
												$grade2 = $definationDB->getData($requisite['PrerequisiteGrade']); // grade yg perlu dapat

												$publishresult = false;

												$recordDb = new Records_Model_DbTable_Academicprogress();
												$publishInfo = $recordDb->getPublishResult($student['IdProgram'], $semester['IdSemesterMaster']);

												if ($publishInfo){
													$publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'].' '.$publishInfo['pm_time']);
													$publishInfo['pm_cdate']= strtotime(date('Y-m-d H:i:s'));

													if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']){
														$publishresult = true;
													}
												}

												if(!empty($grade1) && !empty($grade2)){
													if($grade1['defOrder'] <= $grade2['defOrder'] && $publishresult == true){
														//lepas
														$prerequisites_status = true;
														array_push($pre_requsite_list,$requisite['req_subcode']);
													}else{
														//x lepas
														$prerequisites_status = false;
														array_push($pre_requsite_list,$requisite['req_subcode']);
													}
												}else{
													//anggap x lepas
													$prerequisites_status = false;
													array_push($pre_requsite_list,$requisite['req_subcode']);
												}
								 			}
							 			}else{
						 					//studnt kene amik dulu subject tu
						 					$prerequisites_status = false;
											array_push($pre_requsite_list,$requisite['req_subcode']);
							 			}
							 			
							 			
							 		}else
							 		if($PrerequisiteType==2){
							 			
							 			if($student['IdProgram']==5){ //CIFP
							 				$total_credit_hour_completed=$summary['complete_chr_cifp'];
							 			}else{
							 				$total_credit_hour_completed=$summary['complete_chr_other'];
							 			}
							 							 				
							 			if($total_credit_hour_completed >= $requisite['TotalCreditHours']){
							 				//$subject_list[$index]['requisite_status']='';
							 			}else{
							 				//$subject_list[$index]['requisite_status']='Did not meet min total credit hour';
							 				$prerequisites_status = false;
							 				array_push($pre_requsite_list,'Did not meet min total credit hour');
							 			}
							 			
							 		}else
							 		if($PrerequisiteType==3){
							 				//Co-requisite
								 			//$subject_list[$index]['requisite_req_subject']=$requisite['IdRequiredSubject'];			 		
								 			//$subject_list[$index]['requisite_status']='Subject requisite '.$requisite['req_subcode'];	
								 			//do later pending
								 			array_push($pre_requsite_list,'Subject requisite '.$requisite['req_subcode']);					 			
							 		}
							 		
					    		}//end foreach requsiite		 			 		
						 		
					    		$subject_list[$index]['requisite_status']=$pre_requsite_list;
					    		$subject_list[$index]['prerequisites_status']=$prerequisites_status;
					    		
						 	}//end count prerequisite
				  		
				  			/*--------------end----------------*/
				  		
				  	}else{
				  		
				  		unset($subject_list[$index]);
				  		
				  	}//end allow register
	    			
			 			
			 }//end if subject loop
			 
		}//end count subject	   
    	
		//echo '<pre>';
		//print_r($subject_list);
    	$this->view->subject_list = $subject_list; 
		    	    	
    	    	
    	//1) chcek if add & drop PERIOD is open
    	$ssemDB = new Registration_Model_DbTable_Studentsemesterstatus();
		$isopen = $ssemDB->checkSemesterCourseRegistration($semester['IdSemesterMaster'],$student["IdProgram"]);
		
		if($isopen){
			$this->view->drop_open = true;
		}else{
			$this->view->drop_open = false;
		}
		
		$iswithdrawal = $ssemDB->checkWithdrawalPeriod($semester['IdSemesterMaster'],$student["IdProgram"]);
    	if($iswithdrawal){
			$this->view->withdrawal_open = true;
		}else{
			$this->view->withdrawal_open = false;
		}
		
				
		//2)LIMIT REGISTER  
        //$landscape_detail = $landscapeDB->getActiveLandscape($student['IdProgram'],$student['IdIntake'],$student['IdProgramScheme']);
        $landscape_detail = $landscapeDB->getLandscapeDetails($student['IdLandscape']);
        $semesterType = $semester['SemesterType']==171?'Short Semester': $semester['SemesterType']==172?'Long Semester':null;

        $errMsg = '';
        #171=Short, 172=Long / 1=Course, 2=Credit hour
        //$wLanscapeDetail = $landscapeDetailDB->getDatabyLandscapeId($landscape_detail[0]['IdLandscape'],1,$semester['SemesterType']);
        $wLanscapeDetail = $landscapeDetailDB->getDatabyLandscapeId($student['IdLandscape'],1,$semester['SemesterType']);
        if(count($wLanscapeDetail)==0){
            $errMsg = 'Semester rule (landscape) not found';
        }else{
			$this->view->reqType = $wLanscapeDetail[0]['Type'];         #1=Course, 2=Credit
	        $this->view->minReq = $wLanscapeDetail[0]['MinRegCourse'];
	        $this->view->maxReq = $wLanscapeDetail[0]['MaxRegCourse'];
	    }	
		
    }
    
    
    public function previewAction(){
    	
    	$this->view->title = 'Preview course registration';
	    if ($this->_request->isPost()){
	  
	    	$session = Zend_Registry::get('sis');
	    	
	    	$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();        
			$landscapeSubjectDb  = new GeneralSetup_Model_DbTable_Landscapesubject();		
			$semesterDB = new Registration_Model_DbTable_Semester();		
			$courseGroupDB = new GeneralSetup_Model_DbTable_CourseGroup();
			$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
			$invoiceDB = new Studentfinance_Model_DbTable_Invoice();
			$studentRegSubDB = new Registration_Model_DbTable_Studentregsubjects();
			$requisiteDB = new Registration_Model_DbTable_SubjectPrerequisite();
			$definationDB = new App_Model_General_DbTable_Definationms();
			$registrationItemDB =new Registration_Model_DbTable_RegistrationItem();
			$courseScheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
			$examRegDB = new Registration_Model_DbTable_ExamRegistration();
			$countryDB = new GeneralSetup_Model_DbTable_Country();
			$landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
			$landscapeDetailDB = new GeneralSetup_Model_DbTable_LandscapeDetail();
			$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
	    	$cityDB =  new GeneralSetup_Model_DbTable_City();
	    	$icampus = new icampus_Function_Studentfinance_Invoice();
		    $regSubjectItem = new Registration_Model_DbTable_StudentRegistrationItem();   
		    $courseGroupProgram = new GeneralSetup_Model_DbTable_CourseGroupProgram();
			$wlmodel = new Registration_Model_DbTable_WaitingList();
	    		    		    	
	    	$formData = $this->_request->getPost ();
			//var_dump($formData); exit;
    		$session->register = $formData;	    		  	    
    		
    		$IdStudentRegistration = $formData['IdStudentRegistration'];
    		
    		//get student info
    		$student = $studentRegistrationDb->getData($formData['IdStudentRegistration'],1);
    		
    		
    		//$semester = $semesterDB->getApplicantCurrentSemester(array('ap_prog_scheme'=>$student['IdScheme'],'branch_id'=>$student['IdBranch']));
    		$semester = $semesterDB->getData($formData['IdSemester']);			    	    	
			$this->view->semester = $semester;
						
			//get registartion item
			$item_normal = $registrationItemDB->getStudentItems($student['IdProgram'],$semester['IdSemesterMaster'], $student['IdProgramScheme'], $chargable=0);
			$this->view->item = $item_normal;			
			
			$item_repeat = $registrationItemDB->getRepeatRegItems($student['IdProgram'],$semester['IdSemesterMaster'], $student['IdProgramScheme'], $chargable=0);
			$this->view->item_repeat = $item_repeat;			
		
		
	    	$stack_subject = array();
			$notavailableArr = array();
			$notavailablereserveArr = array();
			$examcheck = array();
			$schedulecheck = array();
			$examconflict = false;
			$scconflict = false;
			$errMsg = 'Possible exam conflict for\n';
			$errMsg2 = '';
			$errMsg3 = '';
			$errMsgCount = 0;
    		if(isset($formData['IdSubject']) && count($formData['IdSubject'])>0){	
    			
    			for($i=0; $i<count($formData["IdSubject"]);  $i++){
					 	
					 	$idSubject     = $formData["IdSubject"][$i];
					 	$prevReg	   = $formData["prevReg"][$idSubject];
					 	$exam_status	   = $formData["exam_status"][$idSubject];
					 	$idGroup       = isset($formData["IdGroup"][$idSubject]) ? $formData["IdGroup"][$idSubject]:null;	
					 	$idCountry     = isset($formData["country"][$idSubject]) ? $formData["country"][$idSubject]:null;
					 	$idCity        = isset($formData["city"][$idSubject]) ? $formData["city"][$idSubject]:null;	
					 	$regItemArr    = (isset($formData["IdRegItem"][$idSubject]) && count($formData["IdRegItem"][$idSubject])>0) ? $formData["IdRegItem"][$idSubject]:null;	
					 	$dropItemArr   = (isset($formData["IdDropItem"][$idSubject]) && count($formData["IdDropItem"][$idSubject])>0) ? $formData["IdDropItem"][$idSubject]:null;
					 	$IdSubjectType = (isset($formData["IdSubjectType"][$idSubject])) ? $formData["IdSubjectType"][$idSubject]:null;	
					 	$IdCourseType = (isset($formData["IdSubjectType"][$idSubject])) ? $formData["IdCourseType"][$idSubject]:null;		
					 						 	
					 	//get subject info
					 	$subject = $subjectDB->getData($idSubject);
					 	
					 	$subject['IdSubjectType']=$IdSubjectType;
					 	$subject['IdCourseType']=$IdCourseType;
					 	$subject['exam_status']=$exam_status;

					 	//get item that already registered
					 	$item = $regSubjectItem->getItemRegister($IdStudentRegistration,$subject['IdSubject'],$semester['IdSemesterMaster'],1);
						$subject['item_registered'] = $item;
					 					 	
	    				if($prevReg==1 && $IdCourseType!=3){ //!RESEARCH	    					
					  		$subject['item_register']=$item_repeat;
					  		$pass_repeat_status = 'RPT';
					  		
					  	}else if($prevReg==2 && $IdCourseType!=3){
					  		$pass_repeat_status = 'RPT';
					  	}else{
					  		$subject['item_register']=$item_normal;	
					  		$pass_repeat_status = '';	
					  	}
					  
					  	
					  	//check if any reg course left
					  	$total_registered = count($item);
					 	$total_add = count($regItemArr);
					 	$total_drop = count($dropItemArr);
					 					 	
					 	if($total_registered==0){
					 		if($exam_status=='CT' || $exam_status=='EX'){
					 			$subject['new']=false;
					 		}else{
					 			$subject['new']=true;
					 		}
					 	}else{
					 		$subject['new']=false;
					 	}
					 	
					 	$total_reg_item = abs($total_registered)-abs($total_drop)+abs($total_add);
					 	if($total_reg_item==0){
					 		if($exam_status=='CT' || $exam_status=='EX'){
					 			$subject['drop']=false;
					 		}else{
					 			$subject['drop']=true;
					 		}
					 	}else{
					 		$subject['drop']=false;
					 	}
					  	
					  	if($IdCourseType==3){
					  		//research paper
					  		if($subject['drop']==false){
					  			$credithour = (isset($formData["credit_hour_registered"][$idSubject])) ? $formData["credit_hour_registered"][$idSubject]:0;
					  			$subject['credit_hour_registered']=$credithour;
					  			
					  			if($credithour==0){
			 						$credithour = 'X';
			 					}
			 							
					  		}else{
					  			$credithour=0;
					  			$subject['credit_hour_registered']=0;
					  		}
					  	}else{
					  		$credithour = null;
					  		$subject['credit_hour_registered']='';
					  	}
					  			 			
					 	//get scheme selected section/group
					 	$scheme_send = 0;
					  	if(isset($idGroup) && $idGroup!=''){
					 		$selected_section_scheme = $courseGroupProgram->getGroupSchemByIdGroup($student['IdProgram'],$idGroup);

					 		if($selected_section_scheme['program_scheme_id']!=$student['IdProgramScheme']){					 			
					 			$scheme_send = $selected_section_scheme['program_scheme_id'];
					 		}
					  	}
					  	
					 	//get reg item if any
					 	
					 	if(isset($regItemArr) && count($regItemArr)>0){
					 		for($x=0; $x<count($regItemArr); $x++){
					 			$itemAdd = $definationDB->getData($regItemArr[$x]);
								//var_dump($itemAdd);
					 			$fee = $icampus->calculateInvoiceAmountByCourse($IdStudentRegistration,$semester['IdSemesterMaster'],$idSubject,$itemAdd['idDefinition'],$pass_repeat_status,$credithour,$scheme_send);

					 			if(count($fee)>0){
					 				$itemAdd['fee_amount'] = $fee[0]['cur_code'].' '.$fee[0]['fsi_amount'];
					 			}else{
					 				$itemAdd['fee_amount'] = '';
					 			}
					 			$subject['itemAdd'][$x]=$itemAdd;

								if ($itemAdd['DefinitionCode']=='EXAM'){
									$examschedule = $examRegDB->getScheduleBySubject($semester['IdSemesterMaster'], $idSubject);

									//check
									if (isset($examcheck[$examschedule['es_date']])){
										foreach ( $examcheck[$examschedule['es_date']] as $echk ){
											if ( $echk['subject'] != $subject ){
												if ( $examschedule['es_start_time'] <= $echk['end'] && $examschedule['es_end_time'] >= $echk['start']){
													$errMsgCount++;
													$subjectExam = $subjectDB->getData($echk['subject']);
													$errMsg .= '      '.$errMsgCount.') '.$subject['SubCode'].'-'.$subject['SubjectName'].' and '.$subjectExam['SubCode'].'-'.$subjectExam['SubjectName'].'\n';
													$examconflict = true;
												}
											}
										}
									}

									if ($examschedule){
										$examcheck[$examschedule['es_date']][] = array(
											'semester_id' => $semester['IdSemesterMaster'],
											'subject' => $idSubject,
											'start' => $examschedule['es_start_time'],
											'end' => $examschedule['es_end_time']
										);
									}
								}

								if(isset($idGroup) && $idGroup!='') {
									$sectioninfo = $courseGroupDB->getInfo($idGroup);
									$schedule = $courseGroupDB->getSchedule($sectioninfo['IdCourseTaggingGroup'], $itemAdd['idDefinition']);
									//var_dump($schedule);
									if ($schedule) {
										foreach ($schedule as $sc) {
											$method = $sc['sc_date'] == '0000-00-00' || empty($sc['sc_date']) ? 'sc_day' : 'sc_date';

											if (isset($schedulecheck[$sc[$method]])) {
												foreach ($schedulecheck[$sc[$method]] as $schk) {
													if ($schk['subject'] != $subject) {
														if (date('H:i:s', strtotime("+1 minute", strtotime($sc['sc_start_time']))) <= $schk['end'] && $sc['sc_end_time'] >= date('H:i:s', strtotime("+1 minute", strtotime($schk['start'])))) {

															$subjectSchedule = $subjectDB->getData($schk['subject']);
															$errMsg2 .= 'Possible schedule conflict for '.$subject['SubCode'].'-'.$subject['SubjectName'].' and '.$subjectSchedule['SubCode'].'-'.$subjectSchedule['SubjectName'].'\n';
															$scconflict = true;
															/*$errMsg .= '<div style="overflow:hidden"><div style="width:320px; float:left; margin-right:10px;"><table class="table"><tr><th>Date</th><th>Day</th><th>Start Time</th><th>End Time</th></tr>';
															$sch1 = $courseGroupScheduleDb->getSchedule($sectioninfo['IdCourseTaggingGroup'], $item['item']);
															foreach ($sch1 as $sc1) {
																$errMsg .= '<tr><td>' . format_date($sc1['sc_date']) . '</td><td>' . $sc1['sc_day'] . '</td><td>' . $sc1['sc_start_time'] . '</td><td>' . $sc1['sc_end_time'] . '</td></tr>';
															}
															$errMsg .= '</table></div>';

															$errMsg .= '<div style="width:320px; float:left;"><table class="table"><tr><th>Date</th><th>Day</th><th>Start Time</th><th>End Time</th></tr>';
															$sch2 = $courseGroupScheduleDb->getSchedule($schk['group'], $schk['item']);
															foreach ($sch2 as $sc2) {
																$errMsg .= '<tr><td>' . format_date($sc2['sc_date']) . '</td><td>' . $sc2['sc_day'] . '</td><td>' . $sc2['sc_start_time'] . '</td><td>' . $sc2['sc_end_time'] . '</td></tr>';
															}

															$errMsg .= '</table></div>';
															$errMsg .= '</div>';*/
															break;
														}
													}
												}
											}

											$schedulecheck[$sc[$method]][] = array(
												'group' => $sectioninfo['IdCourseTaggingGroup'],
												'subject' => $idSubject,
												'item' => $itemAdd['idDefinition'],
												'start' => $sc['sc_start_time'],
												'end' => $sc['sc_end_time']
											);
										}
									}
								}
					 		}					 		
					 	}

    					//get reg drop if any
					 	if(isset($dropItemArr) && count($dropItemArr)>0){
					 		for($z=0; $z<count($dropItemArr); $z++){
					 			$itemDrop = $registrationItemDB->getItemInfo($dropItemArr[$z]);	
					 			$itemDrop['refund'] = $formData["refund"][$idSubject][$dropItemArr[$z]];
					 			$subject['itemDrop'][$z]=$itemDrop;
					 		}					 		
					 	}
					 	
					 						
					 	
					 	//get selected group info
					 	if(isset($idGroup) && $idGroup!=''){
					 		$group = $courseGroupDB->getInfo($idGroup);
							$studentList = $wlmodel->getGroupStudentList($idGroup);
							$studentListReserve = $wlmodel->getGroupStudentListReserve($idGroup);

							$groupQuota = $group['maxstud'];
							$groupReservedQuota = $group['reservation'];
							$noOfStudent = count($studentList);
							$noOfStudentReserve = count($studentListReserve);

							$available = ($groupQuota - $groupReservedQuota - $noOfStudent);
							$availableReserve = ($groupReservedQuota - $noOfStudentReserve);

							if ($available <= 0){
								$group['groupavailable']=$available;
								$group['groupavailablereserve']=$availableReserve;

								$notavailableArr[$idSubject]=$group;
							}

							if ($availableReserve <= 0){
								$group['groupavailable']=$available;
								$group['groupavailablereserve']=$availableReserve;

								$notavailablereserveArr[$idSubject]=$group;
							}

							$subject['group']=$group;
							$subject['groupavailable']=$available;
							$subject['groupavailablereserve']=$availableReserve;
					 	}
					 	
					 	//get selected exam center country/city
					 	$CountryReg = $countryDB->getDatabyId($idCountry);
					 	$CityReg = $cityDB->getDatabyId($idCity);
					 	$subject['country']=$CountryReg;
					 	$subject['city']=$CityReg;	
					 							
					 	$student['subject'][$i]=$subject;
    			}
    		}//end if count

//    		echo '<pre>';
//    		print_r($student);
			//var_dump($notavailablereserveArr);
			$this->view->notavailableArr = $notavailableArr;
			$this->view->notavailablereserveArr = $notavailablereserveArr;
    		$this->view->student = $student;
			$this->view->examconflict = $examconflict;
			$this->view->errMsg = $errMsg;
			$this->view->scconflict = $scconflict;
			$this->view->errMsg2 = $errMsg2;
    		
    		
    		$landscape_detail = $landscapeDB->getActiveLandscape($student['IdProgram'],$student['IdIntake'],$student['IdProgramScheme']);
	        $semesterType = $semester['SemesterType']==171?'Short Semester': $semester['SemesterType']==172?'Long Semester':null;
	
	        #171=Short, 172=Long / 1=Course, 2=Credit hour
	        $wLanscapeDetail = $landscapeDetailDB->getDatabyLandscapeId($landscape_detail[0]['IdLandscape'],1,$semester['SemesterType']);
	        if(count($wLanscapeDetail)==0){
	            $this->view->errMsg3 = 'Semester rule (landscape) not found';
	        }else{
				$this->view->reqType = $wLanscapeDetail[0]['Type'];         #1=Course, 2=Credit
		        $this->view->minReq = $wLanscapeDetail[0]['MinRegCourse'];
		        $this->view->maxReq = $wLanscapeDetail[0]['MaxRegCourse'];
	        }
	        
	        $this->view->remaining = $formData['remaining'];
	        
	    }//end if post
	  
    }
    
    
    public function registerAction(){
    	
    	$session = Zend_Registry::get('sis');
    	
    	//echo '<pre>';
    	//print_r($session->register);

    	if ($this->_request->isPost()){
    		
    			$db = Zend_Db_Table::getDefaultAdapter();    
    			$auth = Zend_Auth::getInstance();
    			
    			
    
    			$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();        
				$landscapeSubjectDb  = new GeneralSetup_Model_DbTable_Landscapesubject();
				$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
				$semesterDB = new Registration_Model_DbTable_Semester();
				$studentSemesterStatusDb =  new Registration_Model_DbTable_Studentsemesterstatus();
				$courseGroupDB = new GeneralSetup_Model_DbTable_CourseGroup();
				$courseGroupStudentDB = new GeneralSetup_Model_DbTable_CourseGroupStudent();
				$systemErrorDB = new App_Model_General_DbTable_SystemError();
				$historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
				$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
				$examRegDB = new Registration_Model_DbTable_ExamRegistration();
				$invoiceClass = new icampus_Function_Studentfinance_Invoice();
				$regSubjectItem = new Registration_Model_DbTable_StudentRegistrationItem();
				$courseGroupProgram = new GeneralSetup_Model_DbTable_CourseGroupProgram();
				
				
    			$previewformData = $this->_request->getPost ();   

    			$formData = $session->register;
    			
    			//var_dump($previewformData); exit;
    		       			
				if(count($formData['IdSubject'])>0){	
					
					 	//NOTICE : there are subjects to be registered set semester status ACTIVE = register
					 																	  	
					  	//if register for previous semester (< current semester no need to generate invoice)
					  	$isSelPrevSem = $semester = $semesterDB->isPreviousSemester($formData['IdSemester']);
						$db->beginTransaction();
					
						try{
													
								 								 		
						 	for($i=0; $i<count($formData["IdSubject"]);  $i++){
						 		
						 								 			
							 		if(isset($previewformData['idRemoveSubject']) && in_array($formData["IdSubject"][$i],$previewformData['idRemoveSubject'])){
										//echo 'break'.$formData["IdSubject"][$i];							 			
									}else{
									$idSubject     = $formData["IdSubject"][$i];	
						 								 			
						 			
						 			/* -------------------------
						 			 * Register Section
						 			 * -------------------------
						 			 */
						 			
						 			if(isset($formData['IdRegItem'][$idSubject]) && count($formData['IdRegItem'][$idSubject])>0){
												//var_dump($formData['IdReplaceSubject'][$idSubject]); exit;
						 				
						 						//echo 'reg'.$formData["IdSubject"][$i];
						 						
						 						//umpukkan
									 			$idSubjectToRegister = $idSubject;
									 			
									 			//utk cater thesis di pass ke invoice su
									 			$pass_credit_hour_registered = null;
									 			$pass_repeat_status = null;
									 			
									 				    
						 						if($i==0){	
							 						//  ---------------- update studentsemesterstatus table ----------------	
													$this->updateStudentSemesterStatus($formData['IdStudentRegistration'],$formData['IdSemester'],130);
													//  ---------------- end update studentsemesterstatus table ----------------	
													
													
													//  ---------------- update studentregistration table ----------------					 					 		
											 		$data['Semesterstatus'] =  130; //Register
													$data['senior_student'] = 0; // New Student					
											 		$studentRegistrationDb->updateData($data,$formData['IdStudentRegistration']);	
											 	    //  ---------------- end update studentregistration table ----------------
						 						}//end update status	
							 
						 						
										 		
						 						
												$active_status = 0; //default:Pre-register
									 			
									 			//check if duplicate entry
									 			$isRegisterSubject = $studentRegSubjectDB->isRegisteredBySemester($formData['IdStudentRegistration'],$idSubject,$formData['IdSemester']);
												//var_dump($isRegisterSubject); exit;
									 			if(!$isRegisterSubject){	

									 					//echo ' add item';
									 					
											 			//check if group is selected
											 			if(isset($formData['IdGroup'][$idSubject]) && $formData['IdGroup'][$idSubject]!=''){
											 				$IdCourseTaggingGroup = $formData['IdGroup'][$idSubject];
											 			}else{
											 				$IdCourseTaggingGroup=0;
											 			}
									 			
									 													 				
										 				//to cater REPEAT/REPLACE cases										 				
											 			
											 														 			
											 			/*  Note:
											 			 *  CASE 1 : $formData['prevReg'][$idSubject] =1 register at prev sem and completed
											 			 *  CASE 2 : $formData['prevReg'][$idSubject] =2 register at prev sem and not completed, status=I
											 			 */
											 			
											 			if($formData['IdCourseType'][$idSubject]!=3){ // Research paper dont have repeat
											 					//check exam attendance
																$checkExamAtt = $examCenterDB->checkExamAttendance($formData['IdStudentRegistration'], $idSubjectToRegister);
																$checkExamAtt2 = $examCenterDB->checkExamAttendance2($formData['IdStudentRegistration'], $idSubjectToRegister);

																if ($checkExamAtt['er_attendance_status'] != 396 || count($checkExamAtt2) > 0) {

																	//CASE 1
																	if ($formData['prevReg'][$idSubject] == 1) {

																		$pass_repeat_status = 'RPT';


																		//check how many time this student repeat this paper
																		//$totalRegisteredPrevSemester = $studentRegSubjectDB->totalRegisteredPrevSemester($formData['IdStudentRegistration'],$idSubject,$formData['IdSemester']);
																		$totalRepeat = $studentRegSubjectDB->getRepeatStatus($formData['IdStudentRegistration'], $formData['IdSemester']);

																		/*
                                                                         * If repeat lebih dari 3 subject tu di kelaskan sebagai register else set as Pre-Repeat
                                                                         * BUT
                                                                         * If repeat replace (GS sahaja) set kan as Pre-replace
                                                                         */


																		if (isset($formData['IdReplaceSubject'][$idSubject]) && $formData['IdReplaceSubject'][$idSubject] != '') {
																			$active_status = 10; // Pre-replace
																		} else {
																			$active_status = 9; // Pre-repeat
																		}

																		if ($totalRepeat < 2) {

																			$subject["cgpa_calculation"] = 1;//exclude calculation
																			$subject2["cgpa_calculation"] = 1;//exclude calculation
																		}

																		if (isset($formData['IdReplaceSubject'][$idSubject]) && $formData['IdReplaceSubject'][$idSubject] != '') {
																			$idSubjectToRegister = $idSubject;
																			$replaceSub = $studentRegSubjectDB->getReplaceSubject($formData['IdStudentRegistration'], $formData['IdReplaceSubject'][$idSubject]);

																			//$group = $courseGroupDB->getGroupQuota($semester['IdSemesterMaster'],$subject['IdSubject'],$student['IdProgram'],$student['IdProgramScheme'],$student['IdBranch'],null);
																			$IdCourseTaggingGroup = 0;

																			//Repeat Replace
																			$subject["replace_subject"] = $formData['IdReplaceSubject'][$idSubject];
																			//$subject["replace_subject"] = $replaceSub['IdStudentRegSubjects'];
																			$subject2["replace_subject"] = $formData['IdReplaceSubject'][$idSubject];
																			//$subject2["replace_subject"] = $replaceSub['IdStudentRegSubjects'];
																		}

																	} else {
																		if (isset($formData['IdReplaceSubject'][$idSubject]) && $formData['IdReplaceSubject'][$idSubject] != '' && $formData['IdReplaceSubject'][$idSubject] != 'null') {
																			$pass_repeat_status = 'RPT';


																			//check how many time this student repeat this paper
																			//$totalRegisteredPrevSemester = $studentRegSubjectDB->totalRegisteredPrevSemester($formData['IdStudentRegistration'],$idSubject,$formData['IdSemester']);
																			$totalRepeat = $studentRegSubjectDB->getRepeatStatus($formData['IdStudentRegistration'], $formData['IdSemester']);

																			/*
                                                                             * If repeat lebih dari 3 subject tu di kelaskan sebagai register else set as Pre-Repeat
                                                                             * BUT
                                                                             * If repeat replace (GS sahaja) set kan as Pre-replace
                                                                             */
																			$active_status = 10; // Pre-replace

																			if ($totalRepeat < 2) {
																				$subject["cgpa_calculation"] = 1;//exclude calculation
																				$subject2["cgpa_calculation"] = 1;//exclude calculation
																			}

																			$idSubjectToRegister = $idSubject;
																			$replaceSub = $studentRegSubjectDB->getReplaceSubject($formData['IdStudentRegistration'], $formData['IdReplaceSubject'][$idSubject]);

																			//$group = $courseGroupDB->getGroupQuota($semester['IdSemesterMaster'],$subject['IdSubject'],$student['IdProgram'],$student['IdProgramScheme'],$student['IdBranch'],null);
																			$IdCourseTaggingGroup = 0;

																			//Repeat Replace
																			//$subject["replace_subject"] = $formData['IdReplaceSubject'][$idSubject];
																			$subject["replace_subject"] = $replaceSub['IdStudentRegSubjects'];
																			//$subject2["replace_subject"] = $formData['IdReplaceSubject'][$idSubject];
																			$subject2["replace_subject"] = $replaceSub['IdStudentRegSubjects'];
																		}
																	}//end prevReg = 1
																}

										 						
													 			//CASE 2: exam status=I
												 				//Hantar kat su REPEAT tapi simpan dalam table studentregsubject staus active=0:pre-reg (default)
											 					if($formData['prevReg'][$idSubject]==2){									 					
											 						$pass_repeat_status = 'RPT';
											 					}//end prevReg = 2
											 			}
											 			
										 				
										 				
										 				if(isset($formData['credit_hour_registered'][$idSubject]) && $formData['credit_hour_registered'][$idSubject]!=''){
										 					
										 					$subject["credit_hour_registered"] = $formData['credit_hour_registered'][$idSubject];
										 					$subject2["credit_hour_registered"] = $formData['credit_hour_registered'][$idSubject];
										 					
										 					if($formData['credit_hour_registered'][$idSubject]==0){
										 						$pass_credit_hour_registered = 'X';
										 					}else{
										 						$pass_credit_hour_registered = $formData['credit_hour_registered'][$idSubject];
										 					}										 					
										 				}else{
															$subject["credit_hour_registered"] = null;
															$subject2["credit_hour_registered"] = null;
														}
									 				
											 			//add table studentregsubject	
											 			$subject["IdStudentRegistration"] = $formData['IdStudentRegistration'];
											 			$subject["IdSubject"] = $idSubjectToRegister;		
														$subject["IdSemesterMain"] = $formData['IdSemester'];
														$subject["SemesterLevel"]= 1;
														$subject["IdLandscapeSub"]=0;
														$subject["Active"] = $active_status; 
														$subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
														$subject["UpdUser"]   = $auth->getIdentity()->iduser;
														$subject["IdCourseTaggingGroup"] = $IdCourseTaggingGroup;
														$subject["not_include_calculation"] = isset($previewformData['includecalculation'][$idSubjectToRegister]) ? 1:0;
														$IdStudentRegSubjects = $studentRegSubjectDB->addData($subject);

														//add table studentregsubject audit trail/history
														$subject2['IdStudentRegSubjects']=$IdStudentRegSubjects;
														$subject2["IdStudentRegistration"] = $formData['IdStudentRegistration'];
											 			$subject2["IdSubject"] = $idSubjectToRegister;		
														$subject2["IdSemesterMain"] = $formData['IdSemester'];
														$subject2["SemesterLevel"]= 1;
														$subject["IdLandscapeSub"]=0;
														$subject2["Active"] = $active_status;  
														$subject2["UpdDate"]   = date ( 'Y-m-d H:i:s');
														$subject2["UpdUser"]   = $auth->getIdentity()->iduser;
														$subject2["IdCourseTaggingGroup"] = $IdCourseTaggingGroup;
														$subject2["not_include_calculation"] = isset($previewformData['includecalculation'][$idSubjectToRegister]) ? 1:0;
														$subject2["message"] = 'Student Course Regisration: Register Subject';
														$subject2['createddt']=date("Y-m-d H:i:s");
														$subject2['createdby']=$auth->getIdentity()->id;
														$subject2['createdrole']='admin';
														$historyDB->addData($subject2);	
													
																										
													
								 				}else{
								 					$IdStudentRegSubjects = $isRegisterSubject['IdStudentRegSubjects'];
								 					$IdCourseTaggingGroup = $isRegisterSubject['IdCourseTaggingGroup'];

													if ($isRegisterSubject['Active']==4 || $isRegisterSubject['Active']==9 || $isRegisterSubject['Active']==6 || $isRegisterSubject['Active']==10){
														$pass_repeat_status = 'RPT';
													}
								 				}//end if registered
							 			
								 		
								 				
												if($IdStudentRegSubjects){
											 		if(isset($formData['IdRegItem'][$idSubject]) && count($formData['IdRegItem'][$idSubject])>0){
											 			for($z=0; $z<count($formData['IdRegItem'][$idSubject]); $z++){
														
											 				$item_id = $formData['IdRegItem'][$idSubject][$z];
											 				
															//regsubjects_detail
															$dataitem = array(
																			'student_id'		=> $formData['IdStudentRegistration'],
																			'regsub_id'			=> $IdStudentRegSubjects,
																			'semester_id'		=> $formData['IdSemester'],
																			'subject_id'		=> $idSubjectToRegister,
																			'item_id'			=> $item_id,
																			'section_id'		=> $IdCourseTaggingGroup,
																			'ec_country'		=> 0,
																			'ec_city'			=> 0,
																			'created_date'		=> new Zend_Db_Expr('NOW()'),
																			'created_by'		=> $auth->getIdentity()->iduser,
																			'created_role'		=> 'admin',
																			'ses_id'			=> 0,
																			'status'			=> 0
															);
															$idRegSubjectItem = $regSubjectItem->addData($dataitem);															
															
															$dataitem['id'] = $idRegSubjectItem;
															$dataitem["message"] = 'Student Course Regisration: Add Item';
															$dataitem['createddt']=date("Y-m-d H:i:s");
															$dataitem['createdby']=$auth->getIdentity()->id;
															$dataitem['createdrole']=$auth->getIdentity()->role;
															$db->insert('tbl_studentregsubjects_detail_history',$dataitem);	
																

																							
															if($item_id==879){
																//---------------exam registration---------------													
																$this->saveExamRegistration($formData,$idSubjectToRegister,$IdStudentRegSubjects);													
																//--------------end exam registration---------------
															}
															
														}//end for													
										 			}//end item		
												}//end insert item

												
						 						//get scheme selected section/group khas utk invoice
											 	$scheme_send = 0;
											  	if(isset($IdCourseTaggingGroup) && $IdCourseTaggingGroup!=''){
											 		$selected_section_scheme = $courseGroupProgram->getGroupSchemByIdGroup($formData['IdProgram'],$IdCourseTaggingGroup);
											 		if($selected_section_scheme['program_scheme_id']!=$formData['IdProgramScheme']){
											 			//su pesan: kalau x sama scheme hantar student selection section scheme
											 			$scheme_send = $selected_section_scheme['program_scheme_id'];
											 		}
											  	}
											  	 
											  	if($isSelPrevSem==false){
									 				$invoiceClass->generateInvoiceStudent($formData['IdStudentRegistration'],$formData['IdSemester'],$idSubjectToRegister,$pass_repeat_status,$pass_credit_hour_registered,0,$scheme_send);
											  	}
						 				
						 			}else{
						 				
						 				
						 					if(isset($formData['IdStudentRegSubjects'][$idSubject]) && $formData['IdStudentRegSubjects'][$idSubject]!=''){			 				
								 				
						 						//nak cater jika tak add just nak update group or exam center
								 				$Id = $formData['IdStudentRegSubjects'][$idSubject];
								 			
								 				//check if group is selected
									 			if(isset($formData['IdGroup'][$idSubject]) && $formData['IdGroup'][$idSubject]!=''){
									 				$datagroup['IdCourseTaggingGroup'] = $formData['IdGroup'][$idSubject];
									 			}else{
									 				$datagroup['IdCourseTaggingGroup'] = 0;
									 			}
																					 			
												$studentRegSubjectDB->updateGroupData($datagroup,$Id);
								 				
												
												
												//---------------exam registration---------------													
												$this->saveExamRegistration($formData,$idSubject,$Id);													
												//--------------end exam registration---------------
						 					}
						 				
						 				
						 			}
						 			//end of add item
						 			
						 			
						 		
								 	//if drop subject exist	
									if(isset($formData['IdDropItem'][$idSubject]) && count($formData['IdDropItem'][$idSubject])>0){
										//drop	
										$this->dropItemSubject($idSubject,$formData);
										$drop_status = true;
									}
									
								}//end preview
						 								
							} //end for
							
							
						
							$db->commit();
							$add_status = true;
							
							//  ---------------- end insert subject  ----------------

							/* ================================================
							 * Regenerate cgpa & checking Repeat for GS student
							 * ================================================
							 */
							if($formData['IdScheme']==11){
								
								//guna function run repeat izhma								
								$modelDB = new Registration_Model_DbTable_SeekingForLandscape();
								$modelDB->repeatSubject($formData);
								$modelDB->updateRepeatCalculation($formData);

								//check is current semester?
    							$isPreviousSemester = $semesterDB->isPreviousSemester($formData['IdSemester']);
    
								if($isPreviousSemester){
									//regenerate cgpa for previous semester only, current and incoming semester strictly prohibited!!
									$grade_calculation = new Cms_ExamCalculation();
									$grade_calculation->generateGrade($formData['IdStudentRegistration']);
								}
							}
							/* ==================================
							 * END Regenerate cgpa for GS student
							 * ==================================
							 */

							$this->_helper->flashMessenger->addMessage(array('success' => "Course registration completed"));
							$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'course-registration', 'action'=>'register','id'=>$formData['IdStudentRegistration'],'idSem'=>$formData['IdSemester']),'default',true));
						
						
						}catch (Exception $event) {
												
							$db->rollBack();
								
							$error['se_txn_id']=0;
							$error['se_IdStudentRegistration']=$formData['IdStudentRegistration'];
							$error['se_IdStudentRegSubjects']=0;
							$error['se_title']='Student Course Registration';
							$error['se_message']=$event->getMessage();
							$error['se_createddt']=date("Y-m-d H:i:s");
							$error['se_createdby']=$auth->getIdentity()->id;
							$systemErrorDB->addData($error);
									
							$add_status = false;
							
							$this->_helper->flashMessenger->addMessage(array('error' => "New Student Course Registration : Add & Drop course failed"));
							$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'course-registration', 'action'=>'register-course','id'=>$formData['IdStudentRegistration'],'idSem'=>$formData['IdSemester']),'default',true));
										
						}
				
			 		
				}//end count

			$sflModel = new Registration_Model_DbTable_SeekingForLandscape();

			//$sflModel->updateRepeatCalculation($formData);

    	  }else{
    	  	
    	 
	    	  	$registry = Zend_Registry::getInstance();
				$locale = $registry->get('Zend_Locale');
				$this->view->locale  = $locale;
			      	
		    	$this->view->title = $this->view->translate("Course Registration List");       
		    	
		    	$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();        
				$semesterDB = new Registration_Model_DbTable_Semester();		
				$courseGroupDB = new GeneralSetup_Model_DbTable_CourseGroup();
				$examRegistrationDB =  new Registration_Model_DbTable_ExamRegistration();
				$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
				$regSubjectItem = new Registration_Model_DbTable_StudentRegistrationItem();
				
		    	$IdStudentRegistration = $this->_getParam('id',null); 
		    	
		    	$IdSem = $this->_getParam('idSem',null); 
    			
		    	//get student info
		    	$student = $studentRegistrationDb->getData($IdStudentRegistration,1);
		    	$this->view->student = $student;
		    	
		    	//get current semester
				//$semester = $semesterDB->getApplicantCurrentSemester(array('ap_prog_scheme'=>$student['IdScheme'],'branch_id'=>$student['IdBranch']));
				$semester = $semesterDB->getData($IdSem);	
				$this->view->semester = $semester;
	    	  	
				$subject_list = $studentRegSubjectDB->getRegSubjectGroup($IdStudentRegistration,$semester['IdSemesterMaster']);
				
				foreach($subject_list as $index=>$subject){
					
					//get exam center
					$exam_center = $examRegistrationDB->getDataExamRegistrationBySubject($IdStudentRegistration,$semester['IdSemesterMaster'],$subject['IdSubject']);
					$subject_list[$index]['exam_center'] = $exam_center;
					
					//$item = $regSubjectItem->getItemRegister($IdStudentRegistration,$subject['IdSubject'],$semester['IdSemesterMaster'],1);
					$item = $regSubjectItem->getItemRegisterById($subject['IdStudentRegSubjects']);
					$subject_list[$index]['item'] = $item;
				}
				
				$this->view->subject_list = $subject_list;
    	  }    	
    	 
    }
    
    
    public function prerequsite($IdStudentRegistration,$student,$subject,$subject_list,$index){
    	
    	$requisiteDB = new Registration_Model_DbTable_SubjectPrerequisite();
		$definationDB = new App_Model_General_DbTable_Definationms();
		$studentRegSubDB = new Registration_Model_DbTable_Studentregsubjects();
		
    }
    
    public function updateStudentSemesterStatus($IdStudentRegistration,$IdSemesterMain,$newstatus){
    	
    	//check current status
    	$semesterStatusDB =  new Registration_Model_DbTable_Studentsemesterstatus();
    	$semester_status = $semesterStatusDB->getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain);
    	
    	if(isset($semester_status) && $semester_status['studentsemesterstatus']==$newstatus){
    		//nothing to update
    	}else{
    		//add new status & keep old status into history table
    		$cms = new Cms_Status();
    		$auth = Zend_Auth::getInstance();
    			      			        	
			$data =  array( 'IdStudentRegistration' => $IdStudentRegistration,									           
				            'idSemester' => $IdSemesterMain,
				            'IdSemesterMain' => $IdSemesterMain,								
				            'studentsemesterstatus' => $newstatus, 	//Register idDefType = 32 (student semester status)
	                        'Level'=>1,
				            'UpdDate' => date ( 'Y-m-d H:i:s'),
				            'UpdUser' => $auth->getIdentity()->iduser
	        );				
					
    		$message = 'Course Registration : Add & Drop';
    		$cms->copyDeleteAddSemesterStatus($IdStudentRegistration,$IdSemesterMain,$data,$message,null);
    	}
    	
    }
    
    public function dropItemSubject($idSubject,$formData){
    	
    	$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
    	$historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
    	$courseGroupStudentDB = new GeneralSetup_Model_DbTable_CourseGroupStudent();
    	$examRegDB = new Registration_Model_DbTable_ExamRegistration();
    	$regSubjectItem = new Registration_Model_DbTable_StudentRegistrationItem();
    	$invoiceClass = new icampus_Function_Studentfinance_Invoice();
    	$semesterDB = new Registration_Model_DbTable_Semester();
    		
    	$auth = Zend_Auth::getInstance();
    	
    	//check add & drop 
    	
		$drop = false;
		$withdraw = false;
				
				
    	//check is current semester?
    	$isCurrentSemester = $semesterDB->isCurrentSemester($formData['IdSemester']);
    	//var_dump($isCurrentSemester); exit;
    	if(!$isCurrentSemester){
    		
    			//can be previous or incoming mester
    			//set default as withdraw subject
    			$withdraw = true;
    			
    	}else{
    		
	    		//current semester
	    		
		    	//get activity add & drop info
		    	$activity_drop = $studentRegSubjectDB->checkActivity($formData['IdSemester']);
					
				$date_now = date('Y-m-d');
				$date_start_drop = $activity_drop['StartDate'];
				$date_last_drop = $activity_drop['EndDate'];
												
				if($date_now > $date_last_drop){	
											
					//echo 'withdraw';				
					$withdraw = true;
					
				}else if ($date_now <= $date_last_drop) {
					
					//echo 'add & drop';
					$drop = true;	
				}
    		
    	}

		//var_dump($withdraw); exit;

    	for($i=0; $i<count($formData["IdDropItem"][$idSubject]);  $i++){
						 	
				$srsd_id  = $formData["IdDropItem"][$idSubject][$i];
				
				$refund  = $formData["refund"][$idSubject][$srsd_id];
				
				$reg_detail = $regSubjectItem->getData($srsd_id);

				if($reg_detail){
					
					$initial_paper = $reg_detail['initial_paper'];
					unset($reg_detail['initial_paper']);
					
					//keep as history	
					if($withdraw==true){
						
												
						$reg_detail['status']=3;
						$reg_detail['refund']=$refund;					
						$reg_detail['modifydt']=date("Y-m-d H:i:s");
						$reg_detail['modifyby']=$auth->getIdentity()->id;						
						$regSubjectItem->updateData($reg_detail,$reg_detail['id']);
						
						
						$reg_detail['message']='Student Course Registration : Withdraw Item';
						$reg_detail['createddt']=date("Y-m-d H:i:s");
						$reg_detail['createdby']=$auth->getIdentity()->id;
						$reg_detail['createdrole']='admin';	
						$regSubjectItem->addHistoryItemData($reg_detail);
						
					}else{
						
						if($drop==true){
							$reg_detail['status']=2;
						}
						
						$reg_detail['refund']=$refund;	
						$reg_detail['message']='Student Course Registration : Drop Item';
						$reg_detail['createddt']=date("Y-m-d H:i:s");
						$reg_detail['createdby']=$auth->getIdentity()->id;
						$reg_detail['createdrole']='admin';						
						$regSubjectItem->addHistoryItemData($reg_detail);
						
						//drop item first
						$regSubjectItem->deleteData($srsd_id);						
					}
					
					if($refund>0){
						if(isset($reg_detail["invoice_id"]) ){
														
							if($initial_paper==1){
								if($i==0){					
									//kene on -> tunggu request by su 19-6-2015	
									//su on kan 19-06-2015			
									$invoiceClass->generateCreditNote($formData['IdStudentRegistration'],$idSubject,$reg_detail['invoice_id'],$reg_detail['item_id'],$refund,0,$initial_paper);	
								}else{
									$invoiceClass->generateCreditNote($formData['IdStudentRegistration'],$idSubject,$reg_detail['invoice_id'],$reg_detail['item_id'],$refund);
								}
							}else{
									$invoiceClass->generateCreditNote($formData['IdStudentRegistration'],$idSubject,$reg_detail['invoice_id'],$reg_detail['item_id'],$refund);
							}							
						}
					}
				
				}//end reg_detail
								
		}
	
		//get total reg item 
		$total_register_item = $regSubjectItem->getTotalRegisterItem($formData['IdStudentRegistration'],$idSubject,$formData['IdSemester'],1);

		
		//check if no more reg item , drop subject
		if($total_register_item==0){			
							
	    	$subject_info = $studentRegSubjectDB->isRegisteredBySemester($formData['IdStudentRegistration'],$idSubject,$formData['IdSemester']);

			if($subject_info){	
				
				if($withdraw==true){
					//dont delete just update status
					
					//update course status
					$upd_info['Active']=3;
					$upd_info['cgpa_calculation']=0; // set 0 in case drop repeat subject 
					//$upd_info['UpdDate']=date("Y-m-d H:i:s");
					//$upd_info['UpdUser']=$auth->getIdentity()->id;
					//$upd_info['UpdRole']='admin';
					//var_dump($upd_info);
					//var_dump($subject_info['IdStudentRegSubjects']); //exit;
					$studentRegSubjectDB->updateStatus($upd_info,$subject_info['IdStudentRegSubjects']);
				
					$subject_info['message']='Student Course Registration : Withdraw Subject';
					$subject_info['createddt']=date("Y-m-d H:i:s");
					$subject_info['createdby']=$auth->getIdentity()->id;
					$subject_info['createdrole']='admin';
					$historyDB->addData($subject_info);
					
				}else{
					
					//copy into audit trail						

					if($drop==true){
						$subject_info['Active']=2;
					}
					
					$subject_info['message']='Student Course Registration : Drop Subject';
					$subject_info['createddt']=date("Y-m-d H:i:s");
					$subject_info['createdby']=$auth->getIdentity()->id;
					$subject_info['createdrole']='admin';
					$historyDB->addData($subject_info);
						
					//delete group
					$courseGroupStudentDB->removeStudent($subject_info['IdCourseTaggingGroup'],$formData['IdStudentRegistration']);
					
					//delete exam registration
					$examRegDB->deleteRegisterExamCenter($formData['IdStudentRegistration'],$formData['IdSemester'],$idSubject);
					
					//delete subject
					$studentRegSubjectDB->deleteData($subject_info['IdStudentRegSubjects']);
				}
				
			}
			
		}
		
		
		//check if there is no subject registered
		$isRegisterSem = $studentRegSubjectDB->getTotalRegisteredBySemester($formData['IdStudentRegistration'],$formData['IdSemester']);
				
		if(count($isRegisterSem)==0){
			//update semester status NOT REGISTER
			
			//  ---------------- update studentsemesterstatus table ----------------	
			$this->updateStudentSemesterStatus($formData['IdStudentRegistration'],$formData['IdSemester'],131);
			//  ---------------- end update studentsemesterstatus table ----------------	
												
		}
    }
    
    
 	public function dropSubject($formData){
    	
    	$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
    	$historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
    	$courseGroupStudentDB = new GeneralSetup_Model_DbTable_CourseGroupStudent();
    	$examRegDB = new Registration_Model_DbTable_ExamRegistration();
    	$auth = Zend_Auth::getInstance();
    	
    	for($i=0; $i<count($formData["IdSubjectDrop"]);  $i++){
						 	
				$idSubject  = $formData["IdSubjectDrop"][$i];
						 			
				$subject_info = $studentRegSubjectDB->isRegisteredBySemester($formData['IdStudentRegistration'],$idSubject,$formData['IdSemester']);
				
				if($subject_info){
					
					//copy into audit trail
					$subject_info['message']='New Student Course Registration : Drop Subject';
					$subject_info['createddt']=date("Y-m-d H:i:s");
					$subject_info['createdby']=$auth->getIdentity()->id;
					$historyDB->addData($subject_info);
						
					//delete group
					$courseGroupStudentDB->removeStudent($subject_info['IdCourseTaggingGroup'],$formData['IdStudentRegistration']);
					
					//delete exam center
					
					//delete subject
					$studentRegSubjectDB->deleteData($subject_info['IdStudentRegSubjects']);
					
				}
				
    	}
    }
    
     public function registerCourseSeniorAction(){
    	
    	//register course for senior student only
    	
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	      	
    	$this->view->title = $this->view->translate("Senior Student Course Registration");       
    	
    	
     }
     
     public function editDetailAction(){
     	     		
    	$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->view->locale  = $locale;
	      	
    	$this->view->title = $this->view->translate("Edit Detail Registration Info");       
    	
    	$studentRegistrationDb = new Registration_Model_DbTable_Studentregistration(); 
		$semesterDB = new Registration_Model_DbTable_Semester();		
		$courseGroupDB = new GeneralSetup_Model_DbTable_CourseGroup();
		$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();		
		$studentRegSubDB = new Registration_Model_DbTable_Studentregsubjects();		
		$examRegistrationDB = new Registration_Model_DbTable_ExamRegistration();
		
    	$IdStudentRegistration = $this->_getParam('id',null); 
    	
    	//get student info
    	$student = $studentRegistrationDb->getData($IdStudentRegistration,1);
    	$this->view->student = $student;
    	
    	//check if new student or senior student
    	$isnewstudent = $studentRegistrationDb->isNewStudent($IdStudentRegistration,$student['IdSemesterMain']);
    		
    	//get current semester
    	if($isnewstudent){
    		//new student
    		$semester = $semesterDB->getData($student['IdSemesterMain']);    				
    	}else{
    		//senior student
    		$semester = $semesterDB->getApplicantCurrentSemester(array('ap_prog_scheme'=>$student['IdProgramScheme'],'branch_id'=>$student['IdBranch']));
    		$this->_helper->flashMessenger->addMessage(array('notice' => "She/He is senior student.Senior student course registration screen is under development"));
    		$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'course-registration', 'action'=>'index'),'default',true));
    	}		    	    	
		$this->view->semester = $semester;
		
		//get registered courses only
		
		$subject_list = $studentRegSubDB->getRegisteredSubject($IdStudentRegistration,$semester['IdSemesterMaster']);
		
		if(count($subject_list)>0){	

			//get exam center list
			$exam_center = $examCenterDB->getExamCenterBySemester($semester['IdSemesterMaster']);
		 	$sorted_values = array();
			foreach($exam_center as $key => $value) {
			    $sorted_values[$value['CountryName']][] = array(
			        'ec_id' => $value['ec_id'],
			        'ec_name' => $value['ec_name'],
			    );
			}				
			ksort($sorted_values);
			$this->view->exam_center = $sorted_values; 
		
			foreach($subject_list as $index=>$subject){
				
				//get course group
			 	$group = $courseGroupDB->getGroupQuota($semester['IdSemesterMaster'],$subject['IdSubject'],$student['IdProgram'],$student['IdProgramScheme'],$student['IdBranch']);
			 	$subject_list[$index]['group']=$group;
			 
				
				//get exam reg
				$exam = $examRegistrationDB->getExamRegistrationBySubject($IdStudentRegistration,$semester['IdSemesterMaster'],$subject['IdSubject']);
				$subject_list[$index]['exam']=$exam;
			}
		}
		$this->view->subject_list = $subject_list;
		
     }
     
     
     public function saveDetailAction(){
     	
     	if ($this->_request->isPost()){
    		
    			$db = Zend_Db_Table::getDefaultAdapter();    
    			$auth = Zend_Auth::getInstance();
    			
    			$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
    			$courseGroupStudentDB = new GeneralSetup_Model_DbTable_CourseGroupStudent();
    			$examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
    			$examRegDB = new Registration_Model_DbTable_ExamRegistration();
    			
    			$formData = $this->_request->getPost (); 
    			//print_r($formData);
    			
    			
    			for($i=0; $i<count($formData["IdStudentRegSubjects"]);  $i++){
					 	
			 			$IdStudentRegSubjects     = $formData["IdStudentRegSubjects"][$i];	
			 			
			 			//update course group
			 			if(isset($formData['IdGroup'][$IdStudentRegSubjects]) && $formData['IdGroup'][$IdStudentRegSubjects]!=''){
			 				$IdCourseTaggingGroup = $formData['IdGroup'][$IdStudentRegSubjects];
			 			}else{
			 				$IdCourseTaggingGroup=0;
			 			}			 			
			 			$studentRegSubjectDB->updateData(array('IdCourseTaggingGroup'=>$IdCourseTaggingGroup),$IdStudentRegSubjects);
			 								
						
			 			
						//add,update exam center location    					
						if(isset($formData['ec_id'][$IdStudentRegSubjects]) && $formData['ec_id'][$IdStudentRegSubjects]!=''){
							
							$ec = $examCenterDB->getExamCenterById($formData['ec_id'][$IdStudentRegSubjects]);
							
							$dataec['er_idStudentRegistration']=$formData['IdStudentRegistration'];
							$dataec['er_idCountry']=$ec['add_country'];
							$dataec['er_idCity']=$ec['add_city'];
							$dataec['er_ec_id']=$formData['ec_id'][$IdStudentRegSubjects];
							$dataec['er_idSemester']=$formData['IdSemester'];
							$dataec['er_idProgram']=$formData['IdProgram'];
							$dataec['er_idSubject']=$formData['IdSubject'][$IdStudentRegSubjects];
							$dataec['er_status']=764; //764:Regiseterd 765:Withdraw
							$dataec['er_createdby']=$auth->getIdentity()->iduser;
							$dataec['er_createddt']= date ( 'Y-m-d H:i:s');
							
							if($formData['er_id'][$IdStudentRegSubjects]!=''){
								$examRegDB->updateData($dataec,$formData['er_id'][$IdStudentRegSubjects]);
							}else{
								$examRegDB->addData($dataec);
							}
							
						}else{
							
							if($formData['er_id'][$IdStudentRegSubjects]!=''){
								//remove
								$examRegDB->deleteData($formData['er_id'][$IdStudentRegSubjects]);
							}
							
						}
					 		    	
    			}
    			$this->_helper->flashMessenger->addMessage(array('success' => "Data has been saved"));
    			$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'course-registration', 'action'=>'edit-detail','id'=>$formData['IdStudentRegistration']),'default',true));
     	}
     }
     
public function getcityAction($country_id=0){
    	$country_id = $this->_getParam('country_id', 0);
    	
     	//if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        //}
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
            
	  	$db = Zend_Db_Table::getDefaultAdapter();
	  	$select = $db->select()
	                 ->from(array('ec'=>'tbl_exam_center'),array('ec_id','ec_name'))
					 ->join(array('add'=>'tbl_address'),'add.add_org_id=ec.ec_id',array('add.add_city','add.add_city_others'))
					 ->joinLeft(array('c'=>'tbl_countries'),'add.add_country=c.idCountry',array('CountryName'))
	                 ->joinLeft(array('s'=>'tbl_state'),'add.add_state=s.idState',array('StateName'))
	                 ->joinLeft(array('ct'=>'tbl_city'),'add.add_city=ct.idCity',array('CityName','idCity'))
	                 ->where('add.add_country = ?', $country_id)
					 ->where('add.add_org_name = ?', 'tbl_exam_center')
					 ->group('add.add_city');
	  
        $stmt = $db->query($select);
        $rows = $stmt->fetchAll();

		$cities = array();

		foreach ( $rows as $row )
		{
			$cities[] = array('city' => $row['CityName'], 'key' => $row['idCity']);
		}
		
		//others
	  	$select2 = $db->select()
	                 ->from(array('ec'=>'tbl_exam_center'),array('ec_id','ec_name'))
					 ->join(array('add'=>'tbl_address'),'add.add_org_id=ec.ec_id',array('add.add_city','add.add_city_others as CityName'))
					 ->joinLeft(array('c'=>'tbl_countries'),'add.add_country=c.idCountry',array('CountryName'))
	                 ->joinLeft(array('s'=>'tbl_state'),'add.add_state=s.idState',array('StateName'))
	                 ->where('add.add_country = ?', $country_id)
					 ->where('add.add_city = ?',99)
					 ->where('add.add_org_name = ?', 'tbl_exam_center')
					 ->group('add.add_city_others');
	  
        $stmt2 = $db->query($select2);
        $rows2 = $stmt2->fetchAll();
		
		foreach ( $rows2 as $row )
		{
			$cities[] = array('city' => $row['CityName'], 'key' => $row['CityName']);
		}
		

		//real others
		$others = array(array('city' => 'Others','key' => 'Others'));

		$cities = array_merge($cities, $others);
	  	
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($cities);
		
		echo $json;
		exit();
    }
    
    public function saveExamRegistration($formData,$idSubject,$IdStudentRegSubjects=null){

    		$auth = Zend_Auth::getInstance();
    		
    		$examRegDB = new Registration_Model_DbTable_ExamRegistration();
    		$subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
    		
    		$subject_info = $subjectDB->getData($idSubject);
    		
    		//if 2,3,9 should not have exam registration info
    		if( in_array($subject_info['CourseType'],array(2,3,19)) == false){
			
	    		if($formData['IdScheme']==11){ //GS
	    			//auto set
	    			$formData['country'][$idSubject] = 121; //Malaysia
	    			$formData['city'][$idSubject] = 1348; //Kuala Lumpur
	    		}
	    		
	    		if(isset($formData['country'][$idSubject]) && $formData['country'][$idSubject]!=''){
	    			    		
					if ( is_numeric($formData['city'][$idSubject]) )
					{
						$city = $formData['city'][$idSubject];
						$city_others = ''; 
					}
					else
					{
						$city = 99;
						$city_others = $formData['city'][$idSubject]; 
					}
					
					
					$dataec['er_idCountry']=$formData['country'][$idSubject];
					$dataec['er_idCity']=$city;
					
					if($city_others!=''){
						$dataec['er_idCityOthers'] = $city_others;													
					}else{
						$dataec['er_idCityOthers'] = null;
						if($city==99){
							$dataec['er_idCity']=null;
						}											
					}
						
					//assign exam center to student
					if($dataec['er_idCountry']!='' && $dataec['er_idCity']!=''){	

						//get exam center that has been assigned
						$examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
						$ec_info = $examCenterDB->getExamCenterByCountryCity($formData['IdSemester'],$dataec['er_idCountry'],$dataec['er_idCity'],$dataec['er_idCityOthers']);
						

						if(count($ec_info)>0){
							$dataec['er_ec_id'] = $ec_info[0]['ec_id'];							
						}else{
							$dataec['er_ec_id'] = null;
						}
						
					}else{
						$dataec['er_ec_id'] = null;
					}
					
					//check if exist update saja
					if(isset($formData['er_id'][$idSubject]) && $formData['er_id'][$idSubject]!=''){
						if($formData['IdScheme']!=11){	
							//JIKA PS UPDATE; SEBAB MAYBE USER TUKAR EXAM CENTER TAPI KALO GS MMG DAH DEFAULT KL SO X PERLU UPDATE												
							$examRegDB->updateData($dataec,$formData['er_id'][$idSubject]);
						}					
					}else{
						
						$dataec['er_idStudentRegistration']=$formData['IdStudentRegistration'];
						$dataec['er_idSemester']=$formData['IdSemester'];
						$dataec['er_idProgram']=$formData['IdProgram'];
						$dataec['er_idSubject']=$idSubject;
						$dataec['er_status']=764; //764:Regiseterd 
						$dataec['er_createdby']=$auth->getIdentity()->iduser;
						$dataec['er_createddt']= date ( 'Y-m-d H:i:s');	
						$dataec['er_createdrole']= 'ADMIN';
					
						$examRegDB->addData($dataec);
					}
					
	    		}//end isset
    		}//end in array	
		
		}//end add exam center
    
    
}

?>