<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 1/3/2016
 * Time: 3:30 PM
 */
class Registration_GradebookByBatchController extends Base_Base {

    private $_gobjlog;
    private $model;
    private $auth;

    public function init() { //initialization function
        $this->_gobjlog = Zend_Registry::get ( 'log' );
        $this->fnsetObj();

        $registry = Zend_Registry::getInstance();
        $locale = $registry->get('Zend_Locale');
        $this->view->locale  = $locale;
    }

    public function fnsetObj(){
        $this->model = new Registration_Model_DbTable_Migrate();
        $this->auth = Zend_Auth::getInstance();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Gradebook By Batch');

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Registration_Form_GradebookByBatch(array('programid'=>$formData["IdProgram"], 'formData'=>$formData));

            $this->view->idProgram = $formData["IdProgram"];
            $this->view->idSemester = $formData["IdSemester"];


            $form->populate($formData);

            //get group
            $courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
            $groups = $courseGroupDb->getCourseGroupListByProgram($formData);

            $courseGroupStudent = new GeneralSetup_Model_DbTable_CourseGroupStudent();

            foreach($groups as $i=>$group){

                $total_student = $courseGroupStudent->getTotalStudent($group["IdCourseTaggingGroup"], false);
                $total_submit = $courseGroupStudent->getTotalStudentMarkSubmitApproval($group["IdCourseTaggingGroup"], false);
                $total_approve = $courseGroupStudent->getTotalStudentMarkApproved($group["IdCourseTaggingGroup"], false);

                $groups[$i]["total_student"] = $total_student;
                $groups[$i]["total_submit"] = $total_submit;
                $groups[$i]["total_approve"] = $total_approve;

            }

            $this->view->groups = $groups;
        }else{
            $form = new Registration_Form_GradebookByBatch();
        }

        $this->view->form = $form;
    }

    public function exportAction(){
        $this->_helper->layout->disableLayout();

        $auth = Zend_Auth::getInstance();
        $this->view->role = $auth->getIdentity()->IdRole;

        $model = new Registration_Model_DbTable_GradebookByBatch();

        $groupList = array();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (isset($formData['check']) && count($formData['check']) > 0){
                foreach ($formData['check'] as $key => $loop){
                    $groupInfo = $model->getGroupDetails($key);

                    //get info semester
                    $semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
                    $semester = $semesterDB->fngetSemestermainDetails($groupInfo['IdSemester']);
                    $groupList[$key]['semester'] = $semester;

                    //get info program
                    $programDB = new GeneralSetup_Model_DbTable_Program();
                    $groupList[$key]['program'] = $programDB->fngetProgramData($formData['idProgram']);

                    //get info subject
                    $subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();
                    $groupList[$key]['subject'] = $subjectDB->getData($groupInfo['IdSubject']);

                    //get info group
                    $courseGroupDb = new GeneralSetup_Model_DbTable_CourseGroup();
                    $groupList[$key]['group'] = $courseGroupDb->getInfo($key);

                    //get component
                    $markDistributionDB =  new Examination_Model_DbTable_Marksdistributionmaster();
                    $list_component = $markDistributionDB->getListMainComponent($groupInfo['IdSemester'], $formData['idProgram'], $groupInfo['IdSubject'], $key);

                    $oCompitem = new Examination_Model_DbTable_Marksdistributiondetails();
                    foreach($list_component as $index=>$component){

                        $component_item = $oCompitem->getListComponentItem($component["IdMarksDistributionMaster"]);
                        $list_component[$index]['component_item'] = $component_item;
                    }
                    $groupList[$key]['component'] = $list_component;

                    //get list student yg register  semester,program,subject di atas
                    //keluarkan aje dulu list student filter by group or lecturer later

                    $studentDB = new Examination_Model_DbTable_StudentRegistration();
                    $students = $studentDB->getStudentEntrybyGroup($groupInfo['IdSemester'], $formData['idProgram'], $groupInfo['IdSubject'], $key, null, false);

                    $db = Zend_Db_Table::getDefaultAdapter();

                    foreach($students as $key2=>$student){

                        if($student['exam_status']=='U' && $student['audit_program']==''){
                            unset($students[$key2]);
                        }else{
                            foreach($list_component as $c=>$component){

                                $select_main_mark = $db->select()
                                    ->from(array('sme'=>'tbl_student_marks_entry'))
                                    ->where('sme.IdStudentRegistration = ?',$student["IdStudentRegistration"])
                                    ->where('sme.IdStudentRegSubjects = ?',$student["IdStudentRegSubjects"]);
                                // ->where('sme.IdMarksDistributionMaster = ?',$component['IdMarksDistributionMaster']);
                                $entry_main = $db->fetchAll($select_main_mark);

                                foreach($entry_main as $m=>$main){
                                    $students[$key2]['IdStudentMarksEntry'.$main['IdMarksDistributionMaster']]=$main['IdStudentMarksEntry'];
                                    $students[$key2]['totaltic'.$main['IdMarksDistributionMaster']]=$main['TotalMarkObtained'];
                                    $students[$key2]['tic'.$main['IdMarksDistributionMaster']]=$main['FinalTotalMarkObtained'];

                                    //get item
                                    $select_item_mark = $db->select()
                                        ->from(array('smed'=>'tbl_student_detail_marks_entry'))
                                        ->where('smed.IdStudentMarksEntry = ?',$main["IdStudentMarksEntry"]);
                                    $entry_item = $db->fetchAll($select_item_mark);

                                    foreach($entry_item as $i=>$item){
                                        $students[$key2]['IdStudentMarksEntryDetail'.$item['ComponentDetail']]=$item['IdStudentMarksEntryDetail'];
                                        $students[$key2]['r'.$item['ComponentDetail']]=$item['MarksObtained'];
                                        $students[$key2]['w'.$item['ComponentDetail']]=$item['FinalMarksObtained'];
                                    }
                                }
                            }
                        }
                    }
                    array_values($students);

                    $groupList[$key]['student'] = $students;
                }
            }
        }

        $this->view->groupList = $groupList;
        $this->view->filename = date('Ymd').'_gradebook.xls';
    }

    public function testAction(){
        echo 'testing page';

        $jsondata = '{"141":{"172":"1","171":"1"},"115":{"172":"1","171":"1"},"106":{"172":"1"}}';

        $subprereg = json_decode($jsondata, true);
        var_dump($subprereg);

        $result = array();
        foreach ( $subprereg as $sub_id => $subdata )
        {
            foreach ( $subdata as $semtype => $val )
            {
                if ( $val == 1 )
                {
                    $result[ $semtype ] = !isset($result[$semtype]) ? array() : $result[$semtype];
                    if ( !isset( $result[ $semtype ]['count']) )
                    {
                        $result[ $semtype ]['count'] = 0;
                    }

                    $result[ $semtype ]['count'] ++;
                    $result[ $semtype ]['subject'][] = array('IdSubject' => $sub_id, 'amount' => 0);
                }
            }
        }
        var_dump($result['172']);
        exit;
    }
}
