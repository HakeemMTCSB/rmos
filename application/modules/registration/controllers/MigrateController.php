<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 30/10/2015
 * Time: 3:08 PM
 */
class Registration_MigrateController extends Base_Base {

    private $_gobjlog;
    private $model;
    private $auth;

    public function init() { //initialization function
        $this->_gobjlog = Zend_Registry::get ( 'log' );
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->model = new Registration_Model_DbTable_Migrate();
        $this->auth = Zend_Auth::getInstance();
    }

    public function migrateAuditPaperAction(){
        $apmodel = new Records_Model_DbTable_Auditpaper();

        $list = $this->model->getMigrateAuditPaper();
        //var_dump($list);
        //exit;

        if ($list){
            foreach ($list as $loop){

                //month
                switch ($loop['month']){
                    case 'January':
                        $month = 'JAN';
                        break;
                    case 'June':
                        $month = 'JUN';
                        break;
                    case 'September':
                        $month = 'SEP';
                        break;
                }

                $sem = $this->model->getSemester($loop['year'], $month, $loop['IdScheme']);

                $appData = array(
                    'apa_studregid'=>$loop['studentid'],
                    'apa_appid'=>$this->generateApplicationID(),
                    'apa_programId'=>$loop['programid'],
                    'apa_semesterid'=>$sem['IdSemesterMaster'],
                    'apa_status'=>825,
                    'apa_approveddate'=>date('Y-m-d'),
                    'apa_applieddate'=>date('Y-m-d'),
                    'apa_appliedby'=>1,
                    'apa_remarks'=>'migrate',
                    'apa_source'=>'Admin',
                    'apa_type'=>857,
                    'apa_upddate'=>date('Y-m-d'),
                    'apa_upduser'=>1
                );
                //var_dump($appData);
                $appId = $apmodel->insertApApp($appData);

                $courseData = array(
                    'apc_apaid'=>$appId,
                    'apc_courseid'=>$loop['IdSubject'],
                    'apc_credithours'=>$loop['CreditHours'],
                    'apc_gred'=>'U',
                    'apc_remarks'=>'migrate',
                    'apc_program'=>$loop['IdProgram'],
                    'apc_programscheme'=>$loop['IdProgramScheme'],
                    'apc_coursesection'=>0,
                    'apc_country'=>0,
                    'apc_city'=>0,
                    'apc_cityothers'=>'',
                    'apc_itemreg'=>889,
                    'apc_upddate'=>date('Y-m-d'),
                    'apc_upduser'=>1
                );
                //var_dump($courseData);
                $idCourse = $apmodel->insertApAppCourse($courseData);

                $itemregData = array(
                    'api_apa_id'=>$appId,
                    'api_apc_id'=>$idCourse,
                    'api_item'=>889,
                    'api_invid_0'=>0,
                    'api_invid_1'=>$loop['invoiceid'],
                    'api_paid_0'=>0,
                    'api_paid_1'=>0,
                    'api_upddate'=>date('Y-m-d H:i:s'),
                    'api_upduser'=>1
                );
                //var_dump($itemregData);
                $apiId = $apmodel->insertCourseItem($itemregData);

                $approve = $this->approveAudit($appId);
                var_dump($approve);
            }
        }
        exit;
    }

    public function approveAudit($apaId){

        $cms =  new Cms_Status();
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        $apmodel = new Records_Model_DbTable_Auditpaper();
        $appApList = $apmodel->getAuditPaperApp($apaId);
        $getCourse = $apmodel->getAuditAppCourse($apaId);

        $checkSubReg = $apmodel->checkSubReg($appApList['IdStudentRegistration'], $getCourse['apc_courseid'], $appApList['apa_semesterid']);
        //var_dump($checkSubReg);
        if ($checkSubReg){
            echo 'lalala';
            exit;
            $return = false;
        }else{
            $this->updateStudentSemesterStatus($appApList['apa_studregid'],$appApList['apa_semesterid'],130);

            $getCourse2 = $apmodel->getAuditAppCourse2($apaId);
            //var_dump($getCourse2); exit;
            if ($getCourse2){
                $subregId = array();
                $ecArr = array();
                foreach ($getCourse2 as $getCourseLoop){
                    $auditSub = array(
                        'IdStudentRegistration'=>$appApList['apa_studregid'],
                        'IdSubject'=>$getCourseLoop['apc_courseid'],
                        'IdSemesterMain'=>$appApList['apa_semesterid'],
                        'SemesterLevel'=>count($apmodel->getSemStatusBasedRegId($appApList['apa_studregid']))+1,
                        'exam_status'=>$getCourseLoop['apc_gred'],
                        'credit_hour_registered'=>$getCourseLoop['apc_credithours'],
                        'grade_name'=>$appApList['apa_type']==858 ? null:$getCourseLoop['apc_gred'],
                        'audit_program'=>$appApList['apa_type']==858 ? $getCourseLoop['apc_program']:null,
                        'audit_programscheme'=>$appApList['apa_type']==858 ? $getCourseLoop['apc_programscheme']:null,
                        'mark_approval_status'=>2,
                        'UpdUser'=>$userId,
                        'UpdDate'=>date('Y-m-d'),
                        'Active'=>0,
                        'IdCourseTaggingGroup'=>$getCourseLoop['apc_coursesection']
                    );
                    $studRegSubId = $apmodel->registerAuditPaper($auditSub);

                    $auditSubHis = array(
                        'IdStudentRegSubjects'=>$studRegSubId,
                        'IdStudentRegistration'=>$appApList['apa_studregid'],
                        'IdSubject'=>$getCourseLoop['apc_courseid'],
                        'IdSemesterMain'=>$appApList['apa_semesterid'],
                        'SemesterLevel'=>count($apmodel->getSemStatusBasedRegId($appApList['apa_studregid']))+1,
                        'exam_status'=>$getCourseLoop['apc_gred'],
                        'credit_hour_registered'=>$getCourseLoop['apc_credithours'],
                        //'grade_name'=>$getCourseLoop['apc_gred'],
                        'grade_name'=>$appApList['apa_type']==858 ? null:$getCourseLoop['apc_gred'],
                        'audit_program'=>$appApList['apa_type']==858 ? $getCourseLoop['apc_program']:null,
                        'audit_programscheme'=>$appApList['apa_type']==858 ? $getCourseLoop['apc_programscheme']:null,
                        'mark_approval_status'=>$appApList['apa_type']==858 ? 0:2,
                        'UpdUser'=>$userId,
                        'UpdDate'=>date('Y-m-d'),
                        'Active'=>0,
                        'IdCourseTaggingGroup'=>$getCourseLoop['apc_coursesection']
                    );
                    $apmodel->historyRegisterAuditPaper($auditSubHis);

                    array_push($subregId, $studRegSubId);

                    //item registration part
                    $itemreg = explode('|', $getCourseLoop['apc_itemreg']);
                    if ($itemreg[0] != ''){
                        foreach ($itemreg as $itemregLoop){
                            $itemRegData = array(
                                'regsub_id'=>$studRegSubId,
                                'student_id'=>$appApList['apa_studregid'],
                                'semester_id'=>$appApList['apa_semesterid'],
                                'subject_id'=>$getCourseLoop['apc_courseid'],
                                'item_id'=>$itemregLoop,
                                'section_id'=>$getCourseLoop['apc_coursesection'],
                                'ec_country'=>$getCourseLoop['apc_country'],
                                'ec_city'=>($getCourseLoop['apc_city']==99 || $getCourseLoop['apc_city']==0) ? $getCourseLoop['apc_cityothers']:$getCourseLoop['City'],
                                'invoice_id'=>NULL,
                                'created_date'=>date('Y-m-d h:i:s'),
                                'created_by'=>$userId,
                                'created_role'=>'admin',
                                'ses_id'=>0,
                                'status'=>0
                            );
                            $regItemId = $apmodel->insertItemReg($itemRegData);

                            $itemRegHisData = array(
                                'id'=>$regItemId,
                                'regsub_id'=>$studRegSubId,
                                'student_id'=>$appApList['apa_studregid'],
                                'semester_id'=>$appApList['apa_semesterid'],
                                'subject_id'=>$getCourseLoop['apc_courseid'],
                                'item_id'=>$itemregLoop,
                                'section_id'=>$getCourseLoop['apc_coursesection'],
                                'ec_country'=>$getCourseLoop['apc_country'],
                                'ec_city'=>($getCourseLoop['apc_city']==99 || $getCourseLoop['apc_city']==0) ? $getCourseLoop['apc_cityothers']:$getCourseLoop['City'],
                                'invoice_id'=>NULL,
                                'created_date'=>date('Y-m-d h:i:s'),
                                'created_by'=>$userId,
                                'created_role'=>'admin',
                                'ses_id'=>0,
                                'status'=>0
                            );
                            $apmodel->insertItemRegHis($itemRegHisData);

                            if ($itemregLoop == 879){
                                if($getCourseLoop['apc_country']==0 || $getCourseLoop['apc_city']==0){
                                    //auto set
                                    $getCourseLoop['apc_country'] = 121; //Malaysia
                                    $getCourseLoop['apc_city'] = 1348; //Kuala Lumpur
                                }

                                //assign exam center to student
                                if($getCourseLoop['apc_country']!='' && $getCourseLoop['apc_city']!=''){

                                    //get exam center that has been assigned
                                    $examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
                                    $ec_info = $examCenterDB->getExamCenterByCountryCity($appApList['apa_semesterid'], $getCourseLoop['apc_country'], $getCourseLoop['apc_city'], $getCourseLoop['apc_cityothers']);
                                    //var_dump($ec_info); exit;
                                    if(count($ec_info) > 0){
                                        $er_ec_id = $ec_info[0]['ec_id'];
                                    }else{
                                        $er_ec_id = null;
                                    }

                                }else{
                                    $er_ec_id = null;
                                }

                                //insert exam center
                                $examCenterData = array(
                                    'er_idCountry'=>$getCourseLoop['apc_country'],
                                    'er_idCity'=>$getCourseLoop['apc_city'],
                                    'er_idCityOthers'=>$getCourseLoop['apc_cityothers'],
                                    'er_idSemester'=>$appApList['apa_semesterid'],
                                    'er_idProgram'=>$getCourseLoop['apc_program'],
                                    'er_idStudentRegistration'=>$appApList['apa_studregid'],
                                    'er_idSubject'=>$getCourseLoop['apc_courseid'],
                                    'er_ec_id'=>$er_ec_id,
                                    'er_status'=>764,
                                    'er_createdby'=>$userId,
                                    'er_createddt'=>date('Y-m-d h:i:s')
                                );
                                $ecId = $apmodel->insertExamCenter($examCenterData);

                                array_push($ecArr, $ecId);
                            }
                        }
                    }
                }

                $implodeSubRegId = implode('|',$subregId);
                $implodeEcId = implode('|',$ecArr);

                $imlplodeData = array(
                    'apa_subjectreg_id'=>$implodeSubRegId,
                    'apa_ec_id'=>$implodeEcId
                );
                $apmodel->updateAppStatus($imlplodeData, $apaId);
            }

            $return = true;
        }

        return $return;
    }

    public function updateStudentSemesterStatus($IdStudentRegistration,$IdSemesterMain,$newstatus){

        //check current status
        $semesterStatusDB =  new Registration_Model_DbTable_Studentsemesterstatus();
        $semester_status = $semesterStatusDB->getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain);

        if(isset($semester_status) && $semester_status['studentsemesterstatus']==$newstatus){
            //nothing to update
        }else{
            //add new status & keep old status into history table
            $cms = new Cms_Status();
            $auth = Zend_Auth::getInstance();

            $data =  array( 'IdStudentRegistration' => $IdStudentRegistration,
                'idSemester' => $IdSemesterMain,
                'IdSemesterMain' => $IdSemesterMain,
                'studentsemesterstatus' => $newstatus, 	//Register idDefType = 32 (student semester status)
                'Level'=>0,
                'UpdDate' => date ( 'Y-m-d H:i:s'),
                'UpdUser' => $auth->getIdentity()->iduser
            );

            $message = 'Audit Paper : Approval';
            $cms->copyDeleteAddSemesterStatus($IdStudentRegistration,$IdSemesterMain,$data,$message,3);
        }
    }

    public function migrateExemptionAction(){
        $srModel = new Registration_Model_DbTable_Studentregistration();

        $list = $this->model->getMigrateExemption();
        //var_dump($list); exit;
        if ($list) {
            foreach ($list as $loop) {
                //month
                switch ($loop['month']){
                    case 'January':
                        $month = 'JAN';
                        break;
                    case 'June':
                        $month = 'JUN';
                        break;
                    case 'September':
                        $month = 'SEP';
                        break;
                }

                //defined status
                //var_dump($loop['mstatus']);
                switch ($loop['mstatus']){
                    case 'Approved pending for upon approval payment':
                        $status=786;
                        $creditStatus=1;
                        $charge=1;
                        $charge2=1;
                        $invoiceid_0=0;
                        if ($loop['invoiceid']!=null){
                            $invoiceid_1=$loop['invoiceid'];
                        }else{
                            $invoiceid_1=0;
                        }
                        $paid_0=1;
                        $paid_1=0;
                        break;
                    case 'Exemption processing payment':
                        $status=784;
                        $creditStatus=null;
                        $charge=1;
                        $charge2=1;
                        $invoiceid_1=0;
                        if ($loop['invoiceid']!=null){
                            $invoiceid_0=$loop['invoiceid'];
                        }else{
                            $invoiceid_0=0;
                        }
                        $paid_0=0;
                        $paid_1=0;
                        break;
                    case 'Waiting for exemption confirmation':
                        $status=784;
                        $creditStatus=null;
                        $charge=1;
                        $charge2=1;
                        $invoiceid_1=0;
                        if ($loop['invoiceid']!=null){
                            $invoiceid_0=$loop['invoiceid'];
                        }else{
                            $invoiceid_0=0;
                        }
                        $paid_0=1;
                        $paid_1=0;
                        break;
                }

                $sem = $this->model->getSemester($loop['year'], $month, $loop['IdScheme']);

                $ctAppData = array(
                    'IdStudentRegistration'=>$loop['studentid'],
                    'IdCTApplication'=>$this->generateApplicationID2(781),
                    'IdApplication'=>0,
                    'IdProgram'=>$loop['programid'],
                    'Status'=>null,
                    'ApplicationStatus'=>$status,
                    'DateApplied'=>date('Y-m-d'),
                    'AppliedBy'=>1,
                    'DateApproved'=>null,
                    'UpdDate'=>date('Y-m-d'),
                    'UpdUser'=>1,
                    'Comments'=>'migrate',
                    'InstitutionType'=>783,
                    'semesterId'=>$sem['IdSemesterMaster'],
                    'applicationType'=>781,
                    'source'=>'Admin'
                );
                $appid = $srModel->insertCtApp($ctAppData);

                $ctSubData = array(
                    'IdCreditTransfer'=>$appid,
                    'IdSubject'=>null,
                    'IdSemesterMain'=>null,
                    'creditStatus'=>$creditStatus,
                    'ApplicationDate'=>date('Y-m-d'),
                    'AppliedBy'=>1,
                    'ApprovedDate'=>null,
                    'Approvedby'=>null,
                    'ApplicationStatus'=>$status,
                    'IdInstitution'=>null,
                    'IdQualification'=>null,
                    'IdSpecialization'=>null,
                    'EquivalentCourse'=>$loop['IdSubject'],
                    'EquivalentGrade'=>'EX',
                    'EquivalentCreditHours'=>$loop['CreditHours'],
                    'Marks'=>null,
                    'Comments'=>'migrate',
                    'disapprovalComment'=>null,
                    'UpdUser'=>1,
                    'UpdDate'=>date('Y-m-d'),
                    'charge'=>$charge,
                    'charge2'=>$charge2,
                    'invoiceid_0'=>$invoiceid_0,
                    'invoiceid_1'=>$invoiceid_1,
                    'paid_0'=>$paid_0,
                    'paid_1'=>$paid_1,
                    'institutionName'=>'N/A'
                );
                //var_dump($ctSubData); exit;
                $IdCreditTransferSubjects = $srModel->insertCtSub($ctSubData);

                $ctSubMoreData = array();

                $ctSubMoreData['csm_creditTransferId']=$appid;
                $ctSubMoreData['csm_creditTransferSubId']=$IdCreditTransferSubjects;
                $ctSubMoreData['csm_type']=783;

                $ctSubMoreData['csm_subjectexternal']='N/A';


                $ctSubMoreData['csm_upduser']=1;
                $ctSubMoreData['csm_upddate']=date('Y-m-d');

                $srModel->insertCtSubMore($ctSubMoreData);

                if ($status == 786){
                    $this->approveExemption($appid);
                }
            }
        }
        exit;
    }

    public function approveExemption($ctId){

        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;
        $srModel = new Registration_Model_DbTable_Studentregistration();
        $getCtInfo = $srModel->getCtInfo($ctId);
        $ctSubject2 = $srModel->getCtSub2($ctId);

        $gradeModel = new Examination_Model_DbTable_Grade();

        //check subject register
        if ($ctSubject2){
            $courseArr = array();
            foreach ($ctSubject2 as $CourseLoop){
                array_push($courseArr, $CourseLoop['EquivalentCourse']);
            }
        }

        $checkSubReg = $srModel->checkSubReg($getCtInfo['IdStudentRegistration'], $courseArr, $getCtInfo['semesterId']);
        //var_dump($checkSubReg); exit;
        if ($checkSubReg){
            $return = false;
        }else{

            $this->updateStudentSemesterStatus($getCtInfo['IdStudentRegistration'],$getCtInfo['semesterId'],130);

            if ($ctSubject2){
                $subregId = array();

                if ($getCtInfo['applicationType']==780){
                    $examstatus = 'CT';
                }else{
                    $examstatus = 'EX';
                }

                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                $checkInvoiceSetup = $invoiceClass->getActivityOtherData($getCtInfo['IdProgram'], 1);

                if ($checkInvoiceSetup){
                    $active = 0;
                }else{
                    $active = 0;
                }

                foreach ($ctSubject2 as $getCourseLoop){
                    $checkSubReg2 = $srModel->checkSubReg2($getCtInfo['IdStudentRegistration'], $getCourseLoop['EquivalentCourse'], $getCtInfo['semesterId']);

                    if ($checkSubReg2){
                        $srModel->deleteWithdrawSubject($checkSubReg2['IdStudentRegSubjects']);
                    }

                    $auditSub = array(
                        'IdStudentRegistration'=>$getCtInfo['IdStudentRegistration'],
                        'IdSubject'=>$getCourseLoop['EquivalentCourse'],
                        'IdSemesterMain'=>$getCtInfo['semesterId'],
                        'SemesterLevel'=>count($srModel->getSemStatusBasedRegId($getCtInfo['IdStudentRegistration']))+1,
                        //'grade_name'=>$getCourseLoop['EquivalentGrade'],
                        'credit_hour_registered'=>$getCourseLoop['EquivalentCreditHours'],
                        'exam_status'=>$examstatus,
                        'grade_name'=>$getCourseLoop['EquivalentGrade'],
                        'mark_approval_status'=>2,
                        'UpdUser'=>$userId,
                        'UpdDate'=>date('Y-m-d'),
                        'Active'=>$active
                    );

                    if ($getCourseLoop['EquivalentGrade']!='CT' && $getCourseLoop['EquivalentGrade']!='EX'){
                        $gradeInfo = $gradeModel->getGradeCTEX($getCtInfo['semesterId'], $getCtInfo['IdProgram'], $getCourseLoop['EquivalentCourse'], $getCourseLoop['EquivalentGrade']);

                        $auditSub['grade_point']=$gradeInfo['GradePoint'];
                    }

                    $studRegSubId = $srModel->registerAuditPaper($auditSub);

                    $auditSubHis = array(
                        'IdStudentRegSubjects'=>$studRegSubId,
                        'IdStudentRegistration'=>$getCtInfo['IdStudentRegistration'],
                        'IdSubject'=>$getCourseLoop['EquivalentCourse'],
                        'IdSemesterMain'=>$getCtInfo['semesterId'],
                        'SemesterLevel'=>count($srModel->getSemStatusBasedRegId($getCtInfo['IdStudentRegistration']))+1,
                        //'grade_name'=>$getCourseLoop['EquivalentGrade'],
                        'credit_hour_registered'=>$getCourseLoop['EquivalentCreditHours'],
                        'exam_status'=>$examstatus,
                        'grade_name'=>$getCourseLoop['EquivalentGrade'],
                        'mark_approval_status'=>2,
                        'UpdUser'=>$userId,
                        'UpdDate'=>date('Y-m-d'),
                        'Active'=>$active
                    );
                    if ($getCourseLoop['EquivalentGrade']!='CT' && $getCourseLoop['EquivalentGrade']!='EX'){
                        $gradeInfo = $gradeModel->getGradeCTEX($getCtInfo['semesterId'], $getCtInfo['IdProgram'], $getCourseLoop['EquivalentCourse'], $getCourseLoop['EquivalentGrade']);

                        $auditSubHis['grade_point']=$gradeInfo['GradePoint'];
                    }
                    $srModel->historyRegisterAuditPaper($auditSubHis);

                    array_push($subregId, $studRegSubId);
                }

                $implodeSubRegId = implode('|',$subregId);

                $imlplodeData = array(
                    'subjectreg_id'=>$implodeSubRegId
                );
                $srModel->updateCtAppStatus($imlplodeData, $ctId);
            }

            $return = true;
        }

        return $return;
    }

    function generateApplicationID(){

        $apmodel = new Records_Model_DbTable_Auditpaper();

        //user
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        //bermulanya sebuah penciptaan id

        //check sequence
        $checkSequence = $apmodel->getSequence(3, date('Y'));

        if (!$checkSequence){
            $dataSeq = array(
                'tsi_type'=>3,
                'tsi_seq_no'=>1,
                'tsi_seq_year'=>date('Y'),
                'tsi_upd_date'=>date('Y-m-d'),
                'tsi_upd_user'=>$userId
            );
            $apmodel->insertSequence($dataSeq);
        }

        //generate applicant ID
        $applicationid_format = array();

        //get sequence
        $sequence = $apmodel->getSequence(3, date('Y'));

        $config = $apmodel->getConfig(1);

        //format
        $format_config = explode('|',$config['auditPaperIdFormat']);
        //var_dump($format_config);
        for($i=0; $i<count($format_config); $i++){
            $format = $format_config[$i];
            if($format=='AU'){ //prefix
                $result = $config['auditPaperPrefix'];
            }elseif($format=='YYYY'){
                $result = date('Y');
            }elseif($format=='YY'){
                $result = date('y');
            }elseif($format=='seqno'){
                $result = sprintf("%05d", $sequence['tsi_seq_no']);
            }
            array_push($applicationid_format,$result);
        }

        $applicationID = implode("",$applicationid_format);

        //update sequence
        $apmodel->updateSequence(array('tsi_seq_no'=>$sequence['tsi_seq_no']+1),$sequence['tsi_id']);

        return $applicationID;
    }

    function generateApplicationID2($type){
        $srModel = new Registration_Model_DbTable_Studentregistration();

        //user
        $auth = Zend_Auth::getInstance();
        $userId = $auth->getIdentity()->iduser;

        //bermulanya sebuah penciptaan id

        //check sequence
        if ($type == 780){
            $checkSequence = $srModel->getSequence(1, date('Y'));

            if (!$checkSequence){
                $dataSeq = array(
                    'tsi_type'=>1,
                    'tsi_seq_no'=>1,
                    'tsi_seq_year'=>date('Y'),
                    'tsi_upd_date'=>date('Y-m-d'),
                    'tsi_upd_user'=>$userId
                );
                $srModel->insertSequence($dataSeq);
            }

            //generate applicant ID
            $applicationid_format = array();

            //get sequence
            $sequence = $srModel->getSequence(1, date('Y'));

            $config = $srModel->getConfig(1);

            //format
            $format_config = explode('|',$config['creditTransferIdFormat']);
            //var_dump($format_config);
            for($i=0; $i<count($format_config); $i++){
                $format = $format_config[$i];
                if($format=='CT'){ //prefix
                    $result = $config['creditTransferPrefix'];
                }elseif($format=='YYYY'){
                    $result = date('Y');
                }elseif($format=='YY'){
                    $result = date('y');
                }elseif($format=='seqno'){
                    $result = sprintf("%05d", $sequence['tsi_seq_no']);
                }
                array_push($applicationid_format,$result);
            }

            $applicationID = implode("",$applicationid_format);

            //update sequence
            $srModel->updateSequence(array('tsi_seq_no'=>$sequence['tsi_seq_no']+1),$sequence['tsi_id']);

        }else{
            $checkSequence = $srModel->getSequence(2, date('Y'));

            if (!$checkSequence){
                $dataSeq = array(
                    'tsi_type'=>2,
                    'tsi_seq_no'=>1,
                    'tsi_seq_year'=>date('Y'),
                    'tsi_upd_date'=>date('Y-m-d'),
                    'tsi_upd_user'=>$userId
                );
                $srModel->insertSequence($dataSeq);
            }

            //generate applicant ID
            $applicationid_format = array();

            //get sequence
            $sequence = $srModel->getSequence(2, date('Y'));

            $config = $srModel->getConfig(1);

            //format
            $format_config = explode('|',$config['exemptionIdFormat']);
            //var_dump($format_config);
            for($i=0; $i<count($format_config); $i++){
                $format = $format_config[$i];
                if($format=='EX'){ //prefix
                    $result = $config['exemptionPrefix'];
                }elseif($format=='YYYY'){
                    $result = date('Y');
                }elseif($format=='YY'){
                    $result = date('y');
                }elseif($format=='seqno'){
                    $result = sprintf("%05d", $sequence['tsi_seq_no']);
                }
                array_push($applicationid_format,$result);
            }

            $applicationID = implode("",$applicationid_format);

            //update sequence
            $srModel->updateSequence(array('tsi_seq_no'=>$sequence['tsi_seq_no']+1),$sequence['tsi_id']);
        }

        return $applicationID;
    }
}