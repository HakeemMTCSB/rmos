<?php

class Registration_SeniorstudentregistrationController extends Base_Base {

	  private $lobjSeniorstudentregistrationModel;
	  private $lobjSeniorstudentregistrationForm;
	  private $lobjinitialconfig;
	  private $_gobjlogger;
	  private $lobjschemeModel;
	  private $lobjprogramModel;
	  private $lobjintakeModel;
	  private $lobjbranchModel;
	  private $lobjsemesterModel;
	  private $lobjstudentsubjectModel;
	  private $lobjReleaseModel;
	  private $lobjstudentHistoryModel;

	  public function init() {
	    $this->fnsetObj();
	    $this->_gobjlogger = Zend_Registry::get('logger'); //instantiate log object
	  }

	  public function fnsetObj() {
	    $this->lobjSeniorstudentregistrationModel = new Registration_Model_DbTable_Seniorstudentregistration();
	    $this->lobjSeniorstudentregistrationForm = new Registration_Form_Seniorstudentregistration();
	    $this->lobjplacementtestmarksmodel = new Application_Model_DbTable_Placementtestmarks();
	    $this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
	    $this->lobjstudentapplication = new Application_Model_DbTable_Studentapplication();
	    $this->lobjInvoiceModel = new Studentfinance_Model_DbTable_Invoice();
	    $this->lobjStudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
	    $this->lobjschemeModel = new GeneralSetup_Model_DbTable_Schemesetup();
	    $this->lobjprogramModel = new GeneralSetup_Model_DbTable_Program();
	    $this->lobjintakeModel = new GeneralSetup_Model_DbTable_Intake();
	    $this->lobjbranchModel = new GeneralSetup_Model_DbTable_Branchofficevenue();
	    $this->lobjsemesterModel = new GeneralSetup_Model_DbTable_Semester();
	    $this->lobjstudentsubjectModel = new Registration_Model_DbTable_Studentsubjects();
	    $this->lobjReleaseModel = new Studentfinance_Model_DbTable_Studentrelease();
	    $this->lobjstudentHistoryModel = new Registration_Model_DbTable_Studenthistory();
	  }

	  public function indexAction() {
	  	
	  	$this->view->title = $this->view->translate("Senior Student Registration");
	  	
	  	$msg = $this->_getParam('msg', null);
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Student Registration Completed");
		}
			
			
	    $this->view->lobjform = $this->lobjform;
	    
	    // Creating scheme dropdown
	    $schemeList = $this->lobjschemeModel->fngetSchemes();
	    $this->view->lobjform->field5->addMultiOptions($schemeList);
	
	    // Creating dropdown for Program
	    $programList = $this->lobjprogramModel->fnGetProgramList();
	    $this->view->lobjform->field1->addMultiOptions($programList);
	
	    // Creating dropdown for intake
	    $intakeList = $this->lobjintakeModel->fngetallIntake();
	    $this->view->lobjform->field8->addMultiOptions($intakeList);
	
	    // Creating dropdown for branch
	    $branchList = $this->lobjbranchModel->fnGetAllBranchList();
	    $this->view->lobjform->field12->addMultiOptions($branchList);
	
	    // Creating dropdown for semester
	    //$semesterList = $this->lobjsemesterModel->fngetSemesterDetail();
	    $semesterList = $this->lobjsemesterModel->getAllsemesterList();
	    $this->view->lobjform->field27->addMultiOptions($semesterList);    
	
	    
	    $larrresult = $this->lobjSeniorstudentregistrationModel->searchSeniorStudent();
	
	    if (!$this->_getParam('search'))
		      unset($this->gobjsessionsis->progseniorstudentregistrationpaginatorresult);
		      $lintpagecount = $this->gintPageCount;
		      $lintpage = $this->_getParam('page', 1); // Paginator instance
		      
	      
	      if (isset($this->gobjsessionsis->progseniorstudentregistrationpaginatorresult)) {
	        	$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->progseniorstudentregistrationpaginatorresult, $lintpage, $lintpagecount);
	      } else {
	        	$this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
	      }
	
	      
	      if ($this->_request->isPost() && $this->_request->getPost('Search')) {
	      	
		        $larrformData = $this->_request->getPost();
		        
		        if ($this->lobjform->isValid($larrformData)) {
			         // $larrresult = $this->lobjSeniorstudentregistrationModel->fnSearchStudentApplication($larrformData); //searching the values for the user
			          $larrresult = $this->lobjSeniorstudentregistrationModel->searchSeniorStudentPaginate($larrformData); //searching the values for the user
			          $this->view->paginator = $this->lobjCommon->fnPagination($larrresult, $lintpage, $lintpagecount);
			          $this->gobjsessionsis->progseniorstudentregistrationpaginatorresult = $larrresult;
		        }
	      }
	
	      if ($this->_request->isPost() && $this->_request->getPost('Clear')) {
	        	$this->_redirect($this->baseUrl . '/registration/seniorstudentregistration/index');
	      }
	  }
  
  
	   public function studentRegistrationAction(){
	   	
		   	$IdRegistartion = $this->_getParam('Idregistartion');    
		    
		   	 //ini nak dptkan landscape student
		   	$registrationDb = new Registration_Model_DbTable_Seniorstudentregistration();
		    $info = $registrationDb->getData($IdRegistartion);
		    
		    if($info["LandscapeType"]=="43"){	    	
		    	
		    	  $this->_redirect($this->view->url(array('module'=>'registration','controller'=>'seniorstudentregistration','action'=>'seniorstudentregistrationdetails','Idregistartion'=>$IdRegistartion),'default',true));
		    	  
		    }else if($info["LandscapeType"]=="44"){  //Block Landscape
		    	
		    	  $this->_redirect($this->view->url(array('module'=>'registration','controller'=>'seniorstudentregistration','action'=>'block-student-registration','Idregistartion'=>$IdRegistartion),'default',true));
		    }
	    
	   }
	  
	   
	   
	public function blockStudentRegistrationAction() { //title
	  	
		$this->view->title = $this->view->translate ( "Senior Student Registration" );
		
		$auth = Zend_Auth::getInstance ();
		
		$lintIdregistartion = $this->_getParam ( 'Idregistartion' );
		$this->view->Idregistartion = $lintIdregistartion;
		
		//ini nak dptkan info registration student
		$registrationDet = $this->lobjSeniorstudentregistrationModel->fngetSeniorStudentRegDet ( $lintIdregistartion );
		$this->view->registrationDet = $registrationDet;
		
		$IdLandscape = $registrationDet [0] ['IdLandscape'];
		$IdStudentRegistration = $registrationDet [0] ['IdStudentRegistration'];
		$IdProgram = $registrationDet [0] ['IdProgram'];
		
		

	   /* ---------------
        * Finance Section
        * Desc : Dah bayar previous semester ke belum? Amount?
         ----------------- */
		
		
		
		
		
	   /* -----------------
	    * Prerequisite section     
	    * Desc : Check prerequisite where all courses at previous block status=pass.
	    * ----------------- */
		
		// 1st: Get jumlah block yg sudah register
		$BlockCount = $this->lobjstudentsubjectModel->getSemesterNo ( $IdStudentRegistration );
		$last_block_level = count ( $BlockCount );
		$new_block_level  = count ( $BlockCount ) + 1;
		$this->view->BlockLevel = $new_block_level;
		
	

		// 2nd : read courses from last block level
		$studentRegistrationDB = new Registration_Model_DbTable_Studentregistration ();
		list ( $status_fail, $total_fail ) = $studentRegistrationDB->getCourseRegisteredBySemesterLevel ( $IdStudentRegistration, $last_block_level );
		
		/* NOTE : kene confirmkan pak agung berapa jumlah subject yg gagal tak boleh proceed register next block
			     * buat masa ni anggap 3 gagal terus x boleh */
		
		$status_fail == false;
		$total_fail = 0;
		if (($status_fail == true) && ($total_fail >= 3)) {
			
			//tak boleh register
			$this->view->message_fail = $this->view->translate ( "Unable to register next block. This student have failed $total_fail subjects from previous block." );
			$this->view->status_fail = $status_fail;
		
		} else {
			
			// boleh register
			$semesterList = $this->lobjStudentregistrationModel->fetchSemMaster ( $registrationDet [0] ['IdScheme'], date ( "Y-m-d" ), $registrationDet [0] ['IdProgram'] );
		    $this->view->semester = $semesterList;			
		
		}//end if	
		   	
			    		    
			
			    
   		if ($this->_request->isPost()){				
			
   			$formData =  $this->_request->getPost();
   			
   			$this->view->idSemester = $formData["IdSemester"];
				   			
   			//Array ( [Idregistartion] => 1 [IdSemester] => 86_main ) 	/
   			$arraySemester = split('_',$formData["IdSemester"]);
   			$IdSemester = $arraySemester[0];
   			$semesterType = $arraySemester[1];
   			
   			if($semesterType=='details'){
   				$this->view->IdSemesterDetails = $IdSemester;
   			}
   			
   			if($semesterType=='main'){
   				$this->view->IdSemesterMain = $IdSemester;
   			}  				

   			// Dapatkan list courses to register
			$blockSubjectDb = new GeneralSetup_Model_DbTable_LandscapeBlockSubject ();
			$blockcourses = $blockSubjectDb->getCoursebyBlock ( $IdLandscape, $new_block_level, $IdSemester );
			$this->view->courseList = $blockcourses;
			   
   		}//end post
		    
			    
 	}//end function
  
 	
 	
  
	public function registerBlockStudentAction(){
		  		
		
		 $auth = Zend_Auth::getInstance();
		
		
		  		if ($this->_request->isPost()){
					
					$formData =  $this->_request->getPost();
										
					//nak cari semester level
					$landscapeBlockDB = new GeneralSetup_Model_DbTable_Landscapeblock();
					$blockSemester = $landscapeBlockDB->getData($formData["blockid"]);
					
					
					if($formData["IdSemesterDetails"]!=''){
						$semesterID = $formData["IdSemesterDetails"];
					}else{
						$semesterID = $formData["IdSemesterMain"];
					}
							

					//-------- Change Previous Block Status------
					$this->lobjStudentregistrationModel->changePreviousBlockStatus($formData['IdStudentRegistration'],$semesterID);
		            //-------- End Change Previous Block Status------
		            
		            
		            
		            //------------Add New Semester Block Status-> to indicate that student has registered this block -----------
		            $lsemstatusArr = array( 'IdStudentRegistration' => $formData["IdStudentRegistration"],								            
								            'idSemester' => $formData["IdSemesterDetails"],
								            'IdSemesterMain' => $formData["IdSemesterMain"],
											'idBlock' => $formData["blockid"],
								            'studentsemesterstatus' => 130, //Register idDefType = 32 (student semester status)
								            'UpdDate' => date ( 'Y-m-d H:i:s'),
								            'UpdUser' => $auth->getIdentity()->iduser
		            );
		            
		            echo '<pre>';
					print_r($lsemstatusArr);
					echo '</pre>';
		            
		            $studentSemesterStatusDb =  new Registration_Model_DbTable_Studentsemesterstatus();
					$studentSemesterStatusDb->addData($lsemstatusArr);
					//---------------------end add new semester status -----------
					        
				            
					//--------------- Register Subject--------------
					 for($i=0; $i<count($formData["subjectId"]);  $i++){
											 	    
							$subject_id = $formData["subjectId"][$i];
						
							$subject["IdSubject"] = $subject_id;
							$subject["IdBlock"]   = $formData["blockid"];
							$subject["BlockLevel"]= $formData["BlockLevel"];
							$subject["IdStudentRegistration"] = $formData["IdStudentRegistration"];							
							$subject["IdSemesterDetails"] = $formData["IdSemesterDetails"];
							$subject["IdSemesterMain"] = $formData["IdSemesterMain"];
							$subject["SemesterLevel"]=  $blockSemester["semester"];
							$subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
							$subject["UpdUser"]   = $auth->getIdentity()->iduser;
							
							echo '<pre>';
							print_r($subject);
							echo '</pre>';
							
							$studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
							$studentRegSubjectDB->addData($subject);
							
														
					 }//end foreach
					//--------------- End Register Subject-------------- 
					 
					 
				 	// -------------- Insert student status in history table ---------------
				        $ret = $this->lobjstudentHistoryModel->fetchStudentHistory($formData['IdStudentRegistration']);  
				            
				        $statusArray['profileStatus'] = 92;
				        $statusArray['IdStudentRegistration'] = $formData['IdStudentRegistration'];
				        $statusArray['IpAddress'] = $this->getRequest()->getServer('REMOTE_ADDR');
				        $statusArray['UpdUser'] = date ( 'Y-m-d H:i:s');
				        $statusArray['UpdDate'] = $auth->getIdentity()->iduser;
				        $len = '';
				        $len = count($ret);
				        echo '<pre>';
				        print_r($statusArray);
				        echo '<pre>';
				        
				        if($ret[$len]['profileStatus']!= 92){
				            $this->lobjstudentHistoryModel->addStudentProfileHistory($statusArray);
				        }
				    // -------------- End Insert student status in history table ---------------
					        
					 
				}//end if
				
				
				$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'seniorstudentregistration', 'action'=>'index','msg'=>1),'default',true));
	}
	
	

	public function seniorstudentregistrationdetailsAction() { //title
		
    $lintIdregistartion = $this->_getParam('Idregistartion');
    $this->view->Idregistartion = $lintIdregistartion;
    $registrationDet = $this->lobjSeniorstudentregistrationModel->fngetSeniorStudentRegDet($lintIdregistartion);

    $this->view->releasestatus = '';   
    if($registrationDet[0]['IdStudentRegistration'] != '' && $registrationDet[0]['IdIntake'] != ''){
        $releasearray = $this->lobjReleaseModel->fetchRelease($registrationDet[0]['IdIntake'], $registrationDet[0]['IdStudentRegistration']);  
        if(count($releasearray)>0) { 
        $this->view->releasestatus = $releasearray[0]['DefinitionDesc']; }
    }
    
    $this->view->registrationDet = $registrationDet;
    $this->view->lobjSeniorstudentregistrationForm = $this->lobjSeniorstudentregistrationForm;
    $semesterList = $this->lobjStudentregistrationModel->fetchSemMaster($registrationDet[0]['IdScheme'],date("Y-m-d H:i:s"),$registrationDet[0]['IdProgram']);
//    echo "<pre>";
//    print_r($registrationDet);
//    echo "<pre>";
//    print_r($semesterList);
    $newSemesterList = array();
    foreach($semesterList as $semester){
        if($registrationDet[0]['idSemester'] != ''){
          if(date('Y-m-d',  strtotime($semester['SemesterStartDate'])) >= date('Y-m-d',strtotime($registrationDet[0]['SemesterStartDate']))){
            $newSemesterList[] = $semester;
          }
        }

        if($registrationDet[0]['IdSemesterMain'] != ''){
          if(date('Y-m-d',  strtotime($semester['SemesterStartDate'])) >= date('Y-m-d',strtotime($registrationDet[0]['SemesterMainStartDate']))){
            $newSemesterList[] = $semester;
          }
        }
     }
    
    $this->view->lobjSeniorstudentregistrationForm->IdSemester->addMultioptions($newSemesterList);    


    $this->view->lobjSeniorstudentregistrationForm->populate($registrationDet[0]);
    $auth = Zend_Auth::getInstance();
    $this->view->lobjSeniorstudentregistrationForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

    $ldtsystemDate = date ( 'Y-m-d H:i:s' );
    $this->view->lobjSeniorstudentregistrationForm->UpdDate->setValue ( $ldtsystemDate );

    //$IdSemester = $registrationDet[0]['IdSemester'];
    $IdLandscape = $registrationDet[0]['IdLandscape'];
    $IdStudentRegistration = $registrationDet[0]['IdStudentRegistration'];
    $IdProgram = $registrationDet[0]['IdProgram'];

    $SemesterNo = $this->lobjstudentsubjectModel->getSemesterNo($IdStudentRegistration);
    if(!empty($newSemesterList)){
        $Pices = explode('_',$newSemesterList[0]['key']);
        $IdSemester =  $Pices[0];
    }
    $registrationDet[0]['IdStudentRegistration'];
    if(isset($IdSemester)){
        $retArray = $this->lobjStudentregistrationModel->checkRegisterSemester($registrationDet[0]['IdStudentRegistration'],$IdSemester);
    }
    $this->view->semstatus = '';
    if(!empty($SemesterNo)){
      if(empty($retArray)){
          $this->view->semstatus = 'Unregistered';
          $this->view->registered = 0;
          $SemesterCount = count($SemesterNo) +1;
          $courseList = $this->lobjStudentregistrationModel->fnGetSeniorStudentCourses($IdLandscape,$IdProgram,$SemesterCount);
      }else{
        $this->view->semstatus = 'Registerd';
        $SemesterCount = count($SemesterNo);
        $this->view->registered = 1;
        // If the semester is registered
        $courseList = $this->lobjStudentregistrationModel->fnGetSeniorStudentCourses($IdLandscape,$IdProgram,$SemesterCount);
      }
    }

    $i = 0;
    $flag = 0;
    foreach($courseList as $course){        
        $ret = $this->lobjstudentsubjectModel->checksubjectexist($IdStudentRegistration,$course['IdSubject']);
        if(!empty($ret)){
            //unset($courseList[$i]);
            $courseList[$i]['alreg'] = "1";
        }
    	else {
        	$courseList[$i]['UpdDate'] = "";
        	$courseList[$i]['loginName'] = "";
                $courseList[$i]['alreg'] = "0";
                $flag = 1;
        }
        $i++;
    }
    $this->view->regbuttonflg = $flag;
//    echo "<pre>";
//    print_r($courseList);
   




    $this->view->courseList = $courseList;
    if ($this->_request->isPost()){
        $lobjFormData = $this->_request->getPost();        
        $data = array();
        $i = 0;
        if($lobjFormData['isregister'] == 0){
            if(trim($registrationDet[0]['idSemester']) != "") {
                $this->lobjStudentregistrationModel->changePrevioussemstatus($lobjFormData['IdStudentRegistration'],$registrationDet[0]['idSemester']);
            } else if(trim($registrationDet[0]['IdSemesterMain']) != "") {
	  	$this->lobjStudentregistrationModel->changePrevioussemstatus($lobjFormData['IdStudentRegistration'],$registrationDet[0]['IdSemesterMain']);
            }
            $dataForstudentSemester = array();
            $dataForstudentSemester['IdApplication'] = $registrationDet[0]['IdApplication'];
            $dataForstudentSemester['IdStudentRegistration'] = $lobjFormData['IdStudentRegistration'];
            $dataForstudentSemester['studentsemesterstatus'] = 130;
            $dataForstudentSemester['UpdUser'] = $lobjFormData['UpdUser'];
            $dataForstudentSemester['UpdDate'] = $lobjFormData['UpdDate'];
            $pieces = explode('_',$lobjFormData['IdSemester']);
            if($pieces[1] == 'detail'){
                $dataForstudentSemester['idSemester'] = $pieces[0];
            }else{
                $dataForstudentSemester['IdSemesterMain'] = $pieces[0];
            }
            $retId = $this->lobjStudentregistrationModel->AddNewSemForStudent($dataForstudentSemester);
        }
        
        if(isset($lobjFormData['coursechk']) && $lobjFormData['coursechk'] != ''){
            foreach($lobjFormData['coursechk'] AS $subject){
                $data['IdStudentRegistration'] =  $lobjFormData['IdStudentRegistration'];
                //$data['IdProgram'] =  $lobjFormData['IdProgram'];
                $data['UpdUser'] =  $lobjFormData['UpdUser'];
                $data['UpdDate'] =  $lobjFormData['UpdDate'];
                $data['IdSubject'] =  $subject;
                $data['Active'] =  1;
                $pieces = explode('_',$lobjFormData['IdSemester']);
                if($pieces[1] == 'detail'){
                    $data['IdSemesterDetails'] = $pieces[0];
                }else{
                    $data['IdSemesterMain'] = $pieces[0];
                }
                $this->lobjstudentsubjectModel->fnaddStudentSubjetcs($data);
            }
        }

        // Now iinsert student status in history table
        $ret = $this->lobjstudentHistoryModel->fetchStudentHistory($lobjFormData['IdStudentRegistration']);      
        $statusArray['profileStatus'] = 92;
        $statusArray['IdStudentRegistration'] = $lobjFormData['IdStudentRegistration'];
        $statusArray['IpAddress'] = $this->getRequest()->getServer('REMOTE_ADDR');
        $statusArray['UpdUser'] = $lobjFormData['UpdUser'];
        $statusArray['UpdDate'] = $lobjFormData['UpdDate'];
        $len = '';
        $len = count($ret);
        if($ret[$len]['profileStatus']!= 92){
            $this->lobjstudentHistoryModel->addStudentProfileHistory($statusArray);
        }
        $this->_redirect( $this->baseUrl . '/registration/seniorstudentregistration');
    }

  }

  public function checksemesterregisterAction(){
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    $lintidsemsster = $this->_getParam('IdSemester');
    $Pices = explode('_',$lintidsemsster);
    $IdSemester =  $Pices[0];
    $IdLandscape = $this->_getParam('IdLandscape');
    $IdStudentRegistration = $this->_getParam('IdStudentRegistration');
    $IdProgram = $this->_getParam('IdProgram');
    $semesterList = $this->lobjStudentregistrationModel->checkRegisterSemester($IdStudentRegistration,$IdSemester);
    //print_r($semesterList);
    if(empty($semesterList)){
        echo "0";
    }else{
        echo "1";
    }
    exit;
  }


  public function seniorstudentcourseAction(){
    $SemesterCount = 0;
    $this->_helper->layout->disableLayout();    
    $this->view->lobjSeniorstudentregistrationForm = $this->lobjSeniorstudentregistrationForm;
    $IdSemester = $this->_getParam('IdSemester');
    $IdLandscape = $this->_getParam('IdLandscape');
    $IdStudentRegistration = $this->_getParam('IdStudentRegistration');
    $IdProgram = $this->_getParam('IdProgram');

    $SemesterNo = $this->lobjstudentsubjectModel->getSemesterNo($IdStudentRegistration);
    if(!empty($SemesterNo)){
      $SemesterCount = count($SemesterNo);
    }    
    $courseList = $this->lobjStudentregistrationModel->fnGetSeniorStudentCourses($IdLandscape,$IdProgram,$SemesterCount);
   
    $i = 0;
    foreach($courseList as $course){
      $ret = $this->lobjstudentsubjectModel->checksubjectexist($IdStudentRegistration,$course['IdSubject']);
     
      if(!empty($ret)){
        unset($courseList[$i]);
      }
      $i++;
    }
    $this->view->courseList = $courseList;
  }

  public function getprerequistvalidationAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    $lintidsubject = $this->_getParam('idsubject');
    $lintidapplicant = $this->_getParam('idapplicant');
    $lintidprogram = $this->_getParam('idprogram');
    $lintidsemsyllabus = $this->_getParam('idsemsyllabus');
    $subjectvalidation = $this->lobjSeniorstudentregistrationModel->fnGetSubjectprerequisitsvalidation($lintidsubject, $lintidapplicant, $this->view->TakeMarks);

    foreach ($subjectvalidation as $subjectvalidations) {
      $subjectmarksvalidation[] = $this->lobjSeniorstudentregistrationModel->fnGetSubjectMarksvalidation($subjectvalidations['IdRequiredSubject'], $lintidprogram, number_format($subjectvalidations['fullcalculatedmarks'], 2), $lintidsemsyllabus);
    }
    echo '{"subjectsmarks":' . Zend_Json_Encoder::encode($subjectmarksvalidation) . '}';
  }

  public function getallcheckboxsubjectdetailsAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    $lintidlandscape = $this->_getParam('idlandscape');
    $lintidsemsster = $this->_getParam('idsemsster');
    $lintidprogram = $this->_getParam('idprogram');
    $lintidsemsstersyllabus = $this->_getParam('idsemsstersyllabus');
    $lintidapplicant = $this->_getParam('idapplicant');
    $intlandscapetype = $this->lobjSeniorstudentregistrationModel->fnGetLandscapeType($lintidlandscape);
    $landscapetype = $intlandscapetype['LandscapeType'];
    if ($landscapetype == "43") {
      $larrsubjects = $this->lobjSeniorstudentregistrationModel->fnGetAllSemesterbasedSujectDetails($lintidlandscape, $lintidsemsster, $lintidsemsstersyllabus, $lintidapplicant, $this->view->Failedsubjects, $lintidprogram, $this->view->TakeMarks);    // semester based
    } elseif ($landscapetype == "44") {
      $larrsubjects = $this->lobjSeniorstudentregistrationModel->fnGetAllBlockbasedSujectDetails($lintidlandscape, $lintidsemsster, $lintidsemsstersyllabus, $lintidapplicant, $this->view->Failedsubjects, $lintidprogram, $this->view->TakeMarks);   // block based
    }
    echo '{"subjects":' . Zend_Json_Encoder::encode($larrsubjects) . '}';
  }

  public function getchargeamountAction() {
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();

    //Get Po details Id
    $lintidcharge = $this->_getParam('idcharge');
    $larramountdata = $this->lobjSeniorstudentregistrationModel->fnGetChargeAmount($lintidcharge);
    echo $larramountdata['Rate'];
  }

  /* 	public function getsemesterlistAction(){
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender();
    $lintidlandscape = $this->_getParam('idlandscape');
    $semesterlist = $this->lobjSeniorstudentregistrationModel->fnGetSemesterList($lintidlandscape);

    for($i=1;$i<=$semesterlist[0]['SemsterCount'];$i++){
    $larrsemesterlist[$i]['key'] = $i;
    $larrsemesterlist[$i]['value'] = $i;Falert


    }
    $larrsemesterlist= $this->lobjCommon->fnResetArrayFromValuesToNames($larrsemesterlist);
    echo Zend_Json_Encoder::encode($larrsemesterlist);
    } */
}