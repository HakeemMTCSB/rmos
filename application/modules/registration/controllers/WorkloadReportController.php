<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 12/12/2015
 * Time: 12:45 PM
 */
class Registration_WorkloadReportController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Registration_Model_DbTable_WorkloadReport();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Workload Report');

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Registration_Form_WorkloadReportForm();
            $this->view->form = $form;
            $form->populate($formData);

            $lecturerList = $this->model->getLecturerList();
            $semGroupList = $this->model->getSemesterGroup($formData['year']);

            $this->view->lecturerList = $lecturerList;
            $this->view->semGroupList = $semGroupList;
            $this->view->formData = $formData;
        }else{
            $form = new Registration_Form_WorkloadReportForm();
            $this->view->form = $form;
        }
    }

    public function clearAction(){
        $this->_redirect($this->baseUrl . '/registration/workload-report/index');
        exit;
    }

    public function printAction(){
        $this->_helper->layout->disableLayout();

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Registration_Form_WorkloadReportForm();
            $this->view->form = $form;
            $form->populate($formData);

            $lecturerList = $this->model->getLecturerList();
            $semGroupList = $this->model->getSemesterGroup($formData['year']);

            $this->view->lecturerList = $lecturerList;
            $this->view->semGroupList = $semGroupList;
            $this->view->formData = $formData;
        }

        $this->view->filename = date('Ymd').'_workload_report.xls';
    }
}