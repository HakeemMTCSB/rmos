<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 11/12/2015
 * Time: 10:25 PM
 */
class Registration_WaitingListController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate = Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get('log'); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Registration_Model_DbTable_WaitingList();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Waiting List');

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $formData['scheme'] = $this->view->escape(strip_tags($formData['scheme']));
            $formData['semester'] = $this->view->escape(strip_tags($formData['semester']));
            $formData['subcode'] = $this->view->escape(strip_tags($formData['subcode']));
            $formData['coursename'] = $this->view->escape(strip_tags($formData['coursename']));

            $form = new Schedule_Form_ClassScheduleSearch(array('schemeid'=>$formData['scheme']));
            $form->populate($formData);
            $this->view->form = $form;

            $waitingList = $this->model->getWaitingList($formData);
            $this->view->waitingList = $waitingList;
        }else{
            $form = new Schedule_Form_ClassScheduleSearch();
            $this->view->form = $form;
        }
    }

    public function clearAction(){
        $this->_redirect($this->baseUrl . '/registration/waiting-list/index');
        exit;
    }

    public function waitingListRegistrationAction(){
        $this->view->title = $this->view->translate('Waiting List');

        $auth = Zend_Auth::getInstance();
        $model = new GeneralSetup_Model_DbTable_Attendance();
        $registrationItemDB =new Registration_Model_DbTable_RegistrationItem();
        $studentRegSubDB = new Registration_Model_DbTable_Studentregsubjects();
        $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
        $recordDb = new Records_Model_DbTable_Academicprogress();
        $studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
        $historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
        $courseGroupStudentDB = new GeneralSetup_Model_DbTable_CourseGroupStudent();
        $examRegDB = new Registration_Model_DbTable_ExamRegistration();
        $regSubjectItem = new Registration_Model_DbTable_StudentRegistrationItem();
        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
        $semesterDB = new Registration_Model_DbTable_Semester();

        $id = $this->_getParam('id',0);

        $waitingListInfo = $this->model->getWaitingListById($id);

        if ($waitingListInfo){
            $item_repeat = $registrationItemDB->getRepeatRegItems2($waitingListInfo['IdProgram'], $waitingListInfo['twl_semester'], $waitingListInfo['IdProgramScheme'], $chargable=0);

            $item_register = $item_repeat;

            //check if Research set default item to paper
            if($waitingListInfo['CourseType']==3){
                if($waitingListInfo['IdScheme']==1){	//PS
                    $item_register = array(array('mandatory'=>1,'item_name'=>'Paper','item_id'=>890));
                }
                if($waitingListInfo['IdScheme']==11){	//GS
                    $item_register = array(array('mandatory'=>1,'item_name'=>'Course','item_id'=>889));
                }
            }

            foreach($item_register as $itemkey=>$i){

                $item_register[$itemkey]['item_status'] = 0;//default

                //get registration item status
                $isRegisterItem = $studentRegSubDB->checkIsRegisterItem($waitingListInfo['IdStudentRegistration'], $waitingListInfo['twl_subjectid'],$waitingListInfo['twl_semester'],$i['item_id']);
                if($isRegisterItem){

                    if($isRegisterItem['status']==3){
                        //withdraw
                        $item_register[$itemkey]['isRegisterItem']=0;
                        $item_register[$itemkey]['item_status']=3;

                    }else{
                        $item_register[$itemkey]['isRegisterItem']=1;
                        $item_register[$itemkey]['srsd_id']=$isRegisterItem['id'];
                    }
                }else{
                    $item_register[$itemkey]['isRegisterItem']=0;
                }
            }

            $waitingListInfo['item'] = $item_register;

            $equivid = false;
            $course_status = $landscapeSubjectDb->getCommonSubjectStatus($waitingListInfo['IdProgram'],$waitingListInfo['IdLandscape'],$waitingListInfo['twl_subjectid']);
            if(!$course_status){
                $equivid=$recordDb->checkEquivStatus($waitingListInfo['IdLandscape'],$waitingListInfo['twl_subjectid']);

                if($equivid){
                    $course_status = $landscapeSubjectDb->getCommonSubjectStatus($waitingListInfo['IdProgram'],$waitingListInfo['IdLandscape'],$equivid);
                }
            }

            $waitingListInfo['coursetypename'] = $course_status['DefinitionDesc'];
        }

        $this->view->waitingListInfo = $waitingListInfo;


        $groupInfo = $this->model->getGourpInfo($waitingListInfo['twl_groupid']);
        $studentList = $this->model->getGroupStudentList($waitingListInfo['twl_groupid']);
        $studentListReserve = $this->model->getGroupStudentListReserve($waitingListInfo['twl_groupid']);

        $groupQuota = $groupInfo['maxstud'];
        $groupReservedQuota = $groupInfo['reservation'];
        $noOfStudent = count($studentList);
        $noOfStudentResereve = count($studentListReserve);

        $available = ($groupQuota - $groupReservedQuota - $noOfStudent);
        $availableReserve = ($groupReservedQuota - $noOfStudentResereve);
        //var_dump($available);
        //var_dump($availableReserve);
        $this->view->available = $available;
        $this->view->availableReserve = $availableReserve;

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //var_dump($formData); exit;
            if (isset($formData['register'])){ //register
                $this->register($formData, $waitingListInfo);

                $registerData = array(
                    'twl_status'=>1,
                    'twl_remark'=>$formData['remark'],
                    'twl_approveby'=>$auth->getIdentity()->id,
                    'twl_approvedate'=>date('Y-m-d H:i:s')
                );
                $this->model->updateWaitingList($registerData, $waitingListInfo['twl_id']);

                //redirect here
                $this->_helper->flashMessenger->addMessage(array('success' => 'Waiting List Registered'));
                $this->_redirect($this->baseUrl . '/registration/waiting-list/waiting-list-registration/id/'.$waitingListInfo['twl_id']);
            }else if ($formData['reject']){ //reject
                $rejectData = array(
                    'twl_status'=>2,
                    'twl_remark'=>$formData['remark'],
                    'twl_approveby'=>$auth->getIdentity()->id,
                    'twl_approvedate'=>date('Y-m-d H:i:s')
                );
                $this->model->updateWaitingList($rejectData, $waitingListInfo['twl_id']);

                //redirect here
                $this->_helper->flashMessenger->addMessage(array('success' => 'Waiting List Rejected'));
                $this->_redirect($this->baseUrl . '/registration/waiting-list/waiting-list-registration/id/'.$waitingListInfo['twl_id']);
            }
        }
    }

    public function register($formData, $waitingListInfo){
        $db = Zend_Db_Table::getDefaultAdapter();
        $auth = Zend_Auth::getInstance();



        $studentRegistrationDb = new Registration_Model_DbTable_Studentregistration();
        $landscapeSubjectDb  = new GeneralSetup_Model_DbTable_Landscapesubject();
        $studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
        $semesterDB = new Registration_Model_DbTable_Semester();
        $studentSemesterStatusDb =  new Registration_Model_DbTable_Studentsemesterstatus();
        $courseGroupDB = new GeneralSetup_Model_DbTable_CourseGroup();
        $courseGroupStudentDB = new GeneralSetup_Model_DbTable_CourseGroupStudent();
        $systemErrorDB = new App_Model_General_DbTable_SystemError();
        $historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
        $examCenterDB =  new GeneralSetup_Model_DbTable_ExamCenter();
        $examRegDB = new Registration_Model_DbTable_ExamRegistration();
        $invoiceClass = new icampus_Function_Studentfinance_Invoice();
        $regSubjectItem = new Registration_Model_DbTable_StudentRegistrationItem();
        $courseGroupProgram = new GeneralSetup_Model_DbTable_CourseGroupProgram();
        $studentRegSubDB = new Registration_Model_DbTable_Studentregsubjects();

        if(isset($formData['IdItem'][$waitingListInfo['twl_studentid']]) && count($formData['IdItem'][$waitingListInfo['twl_studentid']])>0){
            //umpukkan
            $idSubjectToRegister = $waitingListInfo['twl_subjectid'];

            //utk cater thesis di pass ke invoice su
            $pass_credit_hour_registered = null;
            $pass_repeat_status = null;

            //---------------- update studentsemesterstatus table ----------------
            $this->updateStudentSemesterStatus($waitingListInfo['twl_studentid'],$waitingListInfo['twl_semester'],130);
            //  ---------------- end update studentsemesterstatus table ----------------

            //  ---------------- update studentregistration table ----------------
            $data['Semesterstatus'] =  130; //Register
            $data['senior_student'] = 0; // New Student
            $studentRegistrationDb->updateData($data,$waitingListInfo['twl_studentid']);
            //  ---------------- end update studentregistration table ----------------

            $active_status = 0; //default:Pre-register

            //check if duplicate entry
            $isRegisterSubject = $studentRegSubjectDB->isRegisteredBySemester($waitingListInfo['twl_studentid'],$waitingListInfo['twl_subjectid'],$waitingListInfo['twl_semester']);

            if(!$isRegisterSubject){
                $IdCourseTaggingGroup=$waitingListInfo['twl_groupid'];

                //to cater REPEAT/REPLACE cases

                /*  Note:
                 *  CASE 1 : $formData['prevReg'][$idSubject] =1 register at prev sem and completed
                 *  CASE 2 : $formData['prevReg'][$idSubject] =2 register at prev sem and not completed, status=I
                 */
                if($waitingListInfo['CourseType']!=3) { // Research paper dont have repeat
                    if ($waitingListInfo['twl_repeat']==1 || $waitingListInfo['twl_replace']!=0) {
                        //check if registered at previous semester
                        $isRegisteredPrevSem = $studentRegSubDB->isRegisteredPrevSemester($waitingListInfo['twl_studentid'], $waitingListInfo['twl_subjectid'], $waitingListInfo['twl_semester']);

                        $pass_repeat_status = 'RPT';

                        //check how many time this student repeat this paper
                        $totalRepeat = $studentRegSubjectDB->getRepeatStatus($waitingListInfo['twl_studentid'], $waitingListInfo['twl_semester']);

                        /*
                         * If repeat lebih dari 3 subject tu di kelaskan sebagai register else set as Pre-Repeat
                         * BUT
                         * If repeat replace (GS sahaja) set kan as Pre-replace
                         */


                        if ($waitingListInfo['twl_replace']!=0) {
                            $active_status = 10; // Pre-replace
                        } else {
                            $active_status = 9; // Pre-repeat
                        }

                        if ($totalRepeat < 2) {

                            $subject["cgpa_calculation"] = 1;//exclude calculation
                            $subject2["cgpa_calculation"] = 1;//exclude calculation
                        }

                        if ($waitingListInfo['twl_replace']!=0) {
                            $idSubjectToRegister = $waitingListInfo['twl_subjectid'];
                            $replaceSub = $studentRegSubjectDB->getReplaceSubject($waitingListInfo['twl_studentid'], $waitingListInfo['twl_replace']);

                            //Repeat Replace
                            $subject["replace_subject"] = $replaceSub['IdStudentRegSubjects'];
                            $subject2["replace_subject"] = $replaceSub['IdStudentRegSubjects'];
                        }
                    }
                }



                $subject["credit_hour_registered"] = null;
                $subject2["credit_hour_registered"] = null;

                //add table studentregsubject
                $subject["IdStudentRegistration"] = $waitingListInfo['twl_studentid'];
                $subject["IdSubject"] = $idSubjectToRegister;
                $subject["IdSemesterMain"] = $waitingListInfo['twl_semester'];
                $subject["SemesterLevel"]= 1;
                $subject["IdLandscapeSub"]=0;
                $subject["Active"] = $active_status;
                $subject["UpdDate"]   = date ( 'Y-m-d H:i:s');
                $subject["UpdUser"]   = $auth->getIdentity()->iduser;
                $subject["IdCourseTaggingGroup"] = $IdCourseTaggingGroup;
                $subject["not_include_calculation"] = isset($formData['includecalculation']) ? 1:0;
                $IdStudentRegSubjects = $studentRegSubjectDB->addData($subject);

                //add table studentregsubject audit trail/history
                $subject2['IdStudentRegSubjects']=$IdStudentRegSubjects;
                $subject2["IdStudentRegistration"] = $waitingListInfo['twl_studentid'];
                $subject2["IdSubject"] = $idSubjectToRegister;
                $subject2["IdSemesterMain"] = $waitingListInfo['twl_semester'];
                $subject2["SemesterLevel"]= 1;
                $subject["IdLandscapeSub"]=0;
                $subject2["Active"] = $active_status;
                $subject2["UpdDate"]   = date ( 'Y-m-d H:i:s');
                $subject2["UpdUser"]   = $auth->getIdentity()->iduser;
                $subject2["IdCourseTaggingGroup"] = $IdCourseTaggingGroup;
                $subject2["not_include_calculation"] = isset($formData['includecalculation']) ? 1:0;
                $subject2["message"] = 'Student Course Regisration: Register Subject';
                $subject2['createddt']=date("Y-m-d H:i:s");
                $subject2['createdby']=$auth->getIdentity()->id;
                $subject2['createdrole']='admin';
                $historyDB->addData($subject2);



            }else{
                $IdStudentRegSubjects = $isRegisterSubject['IdStudentRegSubjects'];
                $IdCourseTaggingGroup = $isRegisterSubject['IdCourseTaggingGroup'];
            }//end if registered




            if($IdStudentRegSubjects){
                if(isset($formData['IdItem'][$waitingListInfo['twl_studentid']]) && count($formData['IdItem'][$waitingListInfo['twl_studentid']])>0){
                    foreach ($formData['IdItem'][$waitingListInfo['twl_studentid']] as $itemkey => $value){

                        $item_id = $itemkey;

                        //regsubjects_detail
                        $dataitem = array(
                            'student_id'		=> $waitingListInfo['twl_studentid'],
                            'regsub_id'			=> $IdStudentRegSubjects,
                            'semester_id'		=> $waitingListInfo['twl_semester'],
                            'subject_id'		=> $idSubjectToRegister,
                            'item_id'			=> $item_id,
                            'section_id'		=> $IdCourseTaggingGroup,
                            'ec_country'		=> 0,
                            'ec_city'			=> 0,
                            'created_date'		=> new Zend_Db_Expr('NOW()'),
                            'created_by'		=> $auth->getIdentity()->iduser,
                            'created_role'		=> 'admin',
                            'ses_id'			=> 0,
                            'status'			=> 0
                        );
                        $idRegSubjectItem = $regSubjectItem->addData($dataitem);

                        $dataitem['id'] = $idRegSubjectItem;
                        $dataitem["message"] = 'Student Course Regisration: Add Item';
                        $dataitem['createddt']=date("Y-m-d H:i:s");
                        $dataitem['createdby']=$auth->getIdentity()->id;
                        $dataitem['createdrole']='admin';
                        $db->insert('tbl_studentregsubjects_detail_history',$dataitem);



                        if($item_id==879){
                            //---------------exam registration---------------
                            $this->saveExamRegistration($waitingListInfo,$idSubjectToRegister,$IdStudentRegSubjects);
                            //--------------end exam registration---------------
                        }

                    }//end for
                }//end item
            }//end insert item


            //get scheme selected section/group khas utk invoice
            $scheme_send = 0;
            if(isset($IdCourseTaggingGroup) && $IdCourseTaggingGroup!=''){
                $selected_section_scheme = $courseGroupProgram->getGroupSchemByIdGroup($waitingListInfo['IdProgram'],$IdCourseTaggingGroup);
                if($selected_section_scheme['program_scheme_id']!=$waitingListInfo['IdProgramScheme']){
                    //su pesan: kalau x sama scheme hantar student selection section scheme
                    $scheme_send = $selected_section_scheme['program_scheme_id'];
                }
            }

            $isSelPrevSem = $semester = $semesterDB->isPreviousSemester($waitingListInfo['twl_semester']);

            if($isSelPrevSem==false){
                $invoiceClass->generateInvoiceStudent($waitingListInfo['twl_studentid'],$waitingListInfo['twl_semester'],$idSubjectToRegister,$pass_repeat_status,$pass_credit_hour_registered,0,$scheme_send);
            }

        }
    }

    public function updateStudentSemesterStatus($IdStudentRegistration,$IdSemesterMain,$newstatus){
        //check current status
        $semesterStatusDB =  new Registration_Model_DbTable_Studentsemesterstatus();
        $semester_status = $semesterStatusDB->getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain);

        if(isset($semester_status) && $semester_status['studentsemesterstatus']==$newstatus){
            //nothing to update
        }else{
            //add new status & keep old status into history table
            $cms = new Cms_Status();
            $auth = Zend_Auth::getInstance();

            $data =  array( 'IdStudentRegistration' => $IdStudentRegistration,
                'idSemester' => $IdSemesterMain,
                'IdSemesterMain' => $IdSemesterMain,
                'studentsemesterstatus' => $newstatus, 	//Register idDefType = 32 (student semester status)
                'Level'=>1,
                'UpdDate' => date ( 'Y-m-d H:i:s'),
                'UpdUser' => $auth->getIdentity()->iduser
            );

            $message = 'Course Registration : Add & Drop';
            $cms->copyDeleteAddSemesterStatus($IdStudentRegistration,$IdSemesterMain,$data,$message,null);
        }
    }

    public function saveExamRegistration($formData,$idSubject,$IdStudentRegSubjects=null){

        $auth = Zend_Auth::getInstance();

        $examRegDB = new Registration_Model_DbTable_ExamRegistration();
        $subjectDB = new GeneralSetup_Model_DbTable_Subjectmaster();

        $subject_info = $subjectDB->getData($idSubject);

        //if 2,3,9 should not have exam registration info
        if( in_array($subject_info['CourseType'],array(2,3,19)) == false){

            if($formData['IdScheme']==11){ //GS
                //auto set
                $formData['country'][$idSubject] = 121; //Malaysia
                $formData['city'][$idSubject] = 1348; //Kuala Lumpur
            }

            if(isset($formData['country'][$idSubject]) && $formData['country'][$idSubject]!=''){

                if ( is_numeric($formData['city'][$idSubject]) )
                {
                    $city = $formData['city'][$idSubject];
                    $city_others = '';
                }
                else
                {
                    $city = 99;
                    $city_others = $formData['city'][$idSubject];
                }


                $dataec['er_idCountry']=$formData['country'][$idSubject];
                $dataec['er_idCity']=$city;

                if($city_others!=''){
                    $dataec['er_idCityOthers'] = $city_others;
                }else{
                    $dataec['er_idCityOthers'] = null;
                    if($city==99){
                        $dataec['er_idCity']=null;
                    }
                }

                //assign exam center to student
                if($dataec['er_idCountry']!='' && $dataec['er_idCity']!=''){

                    //get exam center that has been assigned
                    $examCenterDB = new GeneralSetup_Model_DbTable_ExamCenter();
                    $ec_info = $examCenterDB->getExamCenterByCountryCity($formData['twl_semester'],$dataec['er_idCountry'],$dataec['er_idCity'],$dataec['er_idCityOthers']);

                    if(count($ec_info)>0){
                        $dataec['er_ec_id'] = $ec_info[0]['ec_id'];
                    }else{
                        $dataec['er_ec_id'] = null;
                    }

                }else{
                    $dataec['er_ec_id'] = null;
                }

                //check if exist update saja
                if(isset($formData['er_id'][$idSubject]) && $formData['er_id'][$idSubject]!=''){
                    if($formData['IdScheme']!=11){
                        //JIKA PS UPDATE; SEBAB MAYBE USER TUKAR EXAM CENTER TAPI KALO GS MMG DAH DEFAULT KL SO X PERLU UPDATE
                        $examRegDB->updateData($dataec,$formData['er_id'][$idSubject]);
                    }
                }else{

                    $dataec['er_idStudentRegistration']=$formData['twl_studentid'];
                    $dataec['er_idSemester']=$formData['twl_semester'];
                    $dataec['er_idProgram']=$formData['IdProgram'];
                    $dataec['er_idSubject']=$idSubject;
                    $dataec['er_status']=764; //764:Regiseterd
                    $dataec['er_createdby']=$auth->getIdentity()->iduser;
                    $dataec['er_createddt']= date ( 'Y-m-d H:i:s');
                    $dataec['er_createdrole']= 'ADMIN';

                    $examRegDB->addData($dataec);
                }

            }//end isset
        }//end in array

    }//end add exam center
}