<?php

class Registration_AdvisorTaggingController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
        $this->view->title = $this->view->translate("Advisor Tagging");
        
    	$msg = $this->_getParam('msg', null);
		if($msg){
			$this->view->noticeSuccess = $this->view->translate("Data has been saved successfully");
		}
        
        $form = new Registration_Form_SearchStudentForm();
		$this->view->form = $form;

		 if($form->isValid($_POST)) {
			if ($this->_request->isPost()){
				
				$formData = $this->_request->getPost ();
				
				//var_dump($formData); exit;	
				$form->populate($formData);
				$this->view->form = $form;
				
				//get list of registered student
				$studentDB = new Registration_Model_DbTable_Studentregistration();
				$student_list = $studentDB->getListStudent($formData);
				$this->view->student_list = $student_list;
				//var_dump($student_list); exit;
				
				//get program info
				$programDB = new Registration_Model_DbTable_Program();
				$program = $programDB->getProgram($formData['IdProgram']);
				
				//staff list
				$staffDB = new GeneralSetup_Model_DbTable_Staffmaster();
				//$staff = $staffDB->getListAcademicStaff($program["IdCollege"]);
				$staff = $staffDB->getListAcademicStaff();
				$this->view->staff_list = $staff;
			}
		}
    }
    
      public function tagStudentAction(){
    	
    	$auth = Zend_Auth::getInstance();
    	
    	if ($this->_request->isPost()){
    		
    		$formData = $this->_request->getPost ();    		
    		
    		//loop to tag student
    		
    		for($i=0; $i<count($formData["IdStudentRegistration"]);  $i++){					
				 	
				$id     = $formData["IdStudentRegistration"][$i];
				
	    		$studentDB = new Registration_Model_DbTable_Studentregistration();    		
	    		$studentDB->updateData(array('AcademicAdvisor'=>$formData["IdStaff"]),$id);
                
                $auth = Zend_Auth::getInstance();			
                $created = $auth->getIdentity()->iduser;
                
                if(($id != '') || ($id != null))
                {
                    $history_data = array(
                        'saa_IdStudentRegistration' => $id,
                        'saa_staff_id'  => $formData["IdStaff"],
                        'saa_status'    => 'Assign',
                        'created_date'  => date('Y-m-d H:i:s'),
                        'created_by'    => $created
                    );
                    
                    $StudentAdvisor = new Registration_Model_DbTable_StudentAcademicAdvisorHistory();
                    $StudentAdvisor->addStudentAdvisorHistory($history_data);
                }
    		}
    		
    		
    		//loop to remove tagging from student
    		for($i=0; $i<count($formData["IdStudentRegistration2"]);  $i++){					
				 	
				$id2   = $formData["IdStudentRegistration2"][$i];
				
	    		$studentDB = new Registration_Model_DbTable_Studentregistration(); 
	    		$studentDB->updateData(array('AcademicAdvisor'=>0),$id2);  
                
                $auth = Zend_Auth::getInstance();			
                $created = $auth->getIdentity()->iduser;
                
                if(($id2 != '') || ($id2 != null))
                {
                    $history_data = array(
                        'saa_IdStudentRegistration' => $id2,
                        'saa_staff_id'  => 0,
                        'saa_status'    => 'Unassigned',
                        'created_date'  => date('Y-m-d H:i:s'),
                        'created_by'    => $created
                    );
                    
                    $StudentAdvisor = new Registration_Model_DbTable_StudentAcademicAdvisorHistory();
                    $StudentAdvisor->addStudentAdvisorHistory($history_data);
                }
    		}
    		
    		
    	}
    	
    	$this->_redirect($this->view->url(array('module'=>'registration','controller'=>'advisor-tagging', 'action'=>'index','msg'=>1),'default',true));
    	
    	
      }

}

?>