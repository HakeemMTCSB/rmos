<?php
class Registration_RegistrationreportController extends Base_Base {
	private $lobjStudentregistrationModel;
	private $lobjStudentregistrationForm;
	private $lobjinitialconfig;
	private $lobjSendoffer;
	private $_gobjlog;
	private $_gobjlogger;

	

	public function init() {
		$this->fnsetObj();
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object

	}

	public function fnsetObj(){
		$this->lobjStudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
		$this->lobjStudentregistrationForm = new Registration_Form_Studentregistration();
		$this->lobjplacementtestmarksmodel = new Application_Model_DbTable_Placementtestmarks();
		//$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration ();
        $this->lobjconfig = new GeneralSetup_Model_DbTable_Initialconfiguration();
		$this->lobjschemesetupmodel = new GeneralSetup_Model_DbTable_Schemesetup();  //local object for Schemesetup Model		
        $this->studentprofileForm = new Registration_Form_Studentprofile();
		$this->lobjAddDropSubjectModel = new Registration_Model_DbTable_Adddropsubject();
        $this->lobjUser = new GeneralSetup_Model_DbTable_User();
        $this->lobjintake = new GeneralSetup_Model_DbTable_Intake();
        $this->lobjRegistrationreportForm = new Registration_Form_Registrationreport();

	}
	
	
	public function indexAction() {
		$this->view->lobjform = $this->lobjform;
		$this->view->lobjRegistrationreportForm = $this->lobjRegistrationreportForm;

		// Set Scheme Form value
		$schemeList = $this->lobjschemesetupmodel->fnGetSchemeDetails(); //function to display all schemesetup details in list
		foreach($schemeList as $larrschemearr) {
			$this->view->lobjRegistrationreportForm->Scheme->addMultiOption($larrschemearr['IdScheme'],$larrschemearr['EnglishDescription']);
		}
		$larrintakelist = $this->lobjintake->fngetallIntake();
		$this->view->lobjform->field26->addMultiOptions($larrintakelist);
		$larProgramNameCombo = $this->lobjplacementtestmarksmodel->fngetProgramNameCombo();
		$this->view->lobjform->field5->addMultiOptions($larProgramNameCombo);
		$this->view->larrresult =$larrresult= $this->lobjStudentregistrationModel->fnSearchregStudentsstat($post = array());
		if(isset($larrresult)){
				foreach($larrresult as $result){
					$this->view->totalcount = $this->view->totalcount + $result['count'];
				}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Generate' )) {
			$larrformData = $this->_request->getPost ();
			$pieces = explode("_", $larrformData['field23']);
			$larrformData['field23'] = $pieces[0];
			if(isset($pieces[1])){
				$larrformData['sem'] = "Detail";
			}
			else{
				$larrformData['sem'] = "Main";
			}
			
			$this->view->larrresult =$larrresult= $this->lobjStudentregistrationModel->fnSearchregStudentsstat($larrformData); //searching the values for the user
			if(count($larrresult)==0) { $this->view->blankMsg = '1';   } 
			$this->view->totalcount = 0;
			if(isset($larrresult)){
				foreach($larrresult as $result){
					$this->view->totalcount = $this->view->totalcount + $result['count'];
				}
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/registration/registrationreport/index');
		}
		
	}
	
	public function regstudentlistAction(){
		$scheme = $this->_getParam('scheme');
		$scheme = explode("_",$scheme);
		$this->view->Idscheme = $scheme[0];
		$semester= $this->_getParam('semester');
		$semester = explode("_",$semester);
		$this->view->semester = $semester[0];
		$intake = $this->_getParam('intake');
		$intake = explode("_",$intake);
		$this->view->intake = $intake[0];
		$program = $this->_getParam('program');
		$program = explode("_",$program);
		$this->view->program = $program[0];
                $lintdd = $this->_getParam("dd");
		$post = array();
		if(isset($program[1])){
			$post['program'] = $program[1];
			$querystring = "/program/".$post['program']."/programname/".$this->view->program;
		}
		else{
			$querystring = "/program/".""."/programname/".$this->view->program;
		}
		if(isset($semester[1])){
			$post['Idsem'] = $semester[1];
			$post['sem'] = $semester[2];
			$querystring1 = "/Idsem/".$post['Idsem']."/sem/".$post['sem']."/semestername/".$this->view->semester;
		}
		else{
			$querystring1 = "/Idsem/".""."/sem/".""."/semestername/".$this->view->semester;
		}
		if(isset($scheme[1])){
			$post['Scheme'] = $scheme[1];
			$querystring2 = "/Scheme/".$post['Scheme']."/schemename/".$this->view->Idscheme;
		}
		else{
			$querystring2 = "/Scheme/".""."/schemename/".$this->view->Idscheme;
		}
		if(isset($intake[1])){
			$post['Intake'] = $intake[1];
			$querystring3 = "/Intake/".$post['Intake']."/intakename/".$this->view->intake;
		}
		else{
			$querystring3 = "/Intake/".""."/intakename/".$this->view->intake;
		}
                if(isset($lintdd)) {
                    $this->view->dd = date("d-m-Y",$lintdd);
                    $post['dd'] = date("Y-m-d",$lintdd);
                    $querystring4 = "/dd/".$post['dd']."/date/".$this->view->dd;
                }
                else {
                    $this->view->dd = "";
                    $querystring4 = "/dd/".""."/date/".$this->view->dd;
                }
        $this->view->querystring = $querystring.$querystring1.$querystring2.$querystring3.$querystring4;
		$this->view->studentlist = $this->lobjStudentregistrationModel->fngetstudents($post);
	}
	
	/**
	 * Function to download Registration Report
	 */
	
	public function downloadreportAction(){
		$formdata = array();
		$formdata['program'] =$this->_getParam('program',''); 
		$formdata['Idsem'] =$this->_getParam('Idsem','');
    	$formdata['sem'] =$this->_getParam('sem','');
 		$formdata['Scheme'] =$this->_getParam('Scheme','');
 		$formdata['Intake'] =$this->_getParam('Intake','');
 		$formdata['dd'] =$this->_getParam('dd','');
 		
 		$this->view->Idscheme = $this->_getParam('schemename','');
 		$this->view->semester = $this->_getParam('semestername','');
 		$this->view->intake   = $this->_getParam('intakename','');
 		$this->view->program  = $this->_getParam('programname','');
 		$this->view->dd       = $this->_getParam('date','');
 		
        $this->view->studentlist = $this->lobjStudentregistrationModel->fngetstudents($formdata);
		$this->view->setScriptPath($this->view->getScriptPaths());
        $html = $this->view->render('registrationreport/downloadexcel.phtml');
        $this->getResponse()->setRawHeader( "Content-Type: application/vnd.ms-excel; charset=UTF-8" )
            ->setRawHeader( "Content-Disposition: attachment; filename=Registration_Report.xls" )
           ->setRawHeader( "Content-Transfer-Encoding: binary" )
            ->setRawHeader( "Expires: 0" )
            ->setRawHeader( "Cache-Control: must-revalidate, post-check=0, pre-check=0" )
            ->setRawHeader( "Pragma: public" )
            ->sendResponse();
        echo $html; 
        exit();
	}

        /**
         * Function to Display the unregistered students
         */
	
	
        public function unregstudentsAction() {		
             $this->view->lobjform = $this->lobjform;
             
             $larProgramNameCombo = $this->lobjplacementtestmarksmodel->fngetProgramNameCombo();
             $this->view->lobjform->field8->addMultiOptions($larProgramNameCombo);
             
             // Set Scheme Form value
             $schemeList = $this->lobjschemesetupmodel->fnGetSchemeDetails(); //function to display all schemesetup details in list
             foreach($schemeList as $larrschemearr) {
                    $this->view->lobjform->field23->addMultiOption($larrschemearr['IdScheme'],$larrschemearr['EnglishDescription']);
             }

             $larrresult = $this->lobjStudentregistrationModel->fnSearchUnregStudents($post = NULL); 
             $i = '0';
             $larrresultFinal = array();
             foreach ($larrresult as $values) {
                      $regID = $values['IdStudentRegistration'];
                      $idProgram = $values['IdProgram'];
                      $idScheme = $values['IdScheme'];
                      $returnSem = $this->lobjStudentregistrationModel->fnfetchAllCurentSemester($regID,$idProgram,$idScheme);
                      if($returnSem!='') {
                          $larrresultFinal[$i]= $values;
                          $larrresultFinal[$i]['returnSem']= $returnSem; 
                          $i++;
                      } 
             }
             //asd($larrresultFinal);
             if(!$this->_getParam('search')) { 
   	    	unset($this->gobjsessionsis->progstudentregistrationpaginatorresult);
             }

		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->progstudentregistrationpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->progstudentregistrationpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresultFinal,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			//asd($larrformData);
			//if ($this->lobjform->isValid ( $larrformData )) {
				$larrresultFinals = array();
				$larrresult = $this->lobjStudentregistrationModel->fnSearchUnregStudents( $larrformData ); //searching the values for the user
                                foreach ($larrresult as $values) {
                                    $regID = $values['IdStudentRegistration'];
                                    $idProgram = $values['IdProgram'];
                                    $idScheme = $values['IdScheme'];
                                    $returnSem = $this->lobjStudentregistrationModel->fnfetchAllCurentSemester($regID,$idProgram,$idScheme);
                                    if($returnSem!='') {
                                        $larrresultFinals[$i]= $values;
                                        $larrresultFinals[$i]['returnSem']= $returnSem; 
                                        $i++;
                                    } 
                                }
                                
                                $this->view->paginator = $this->lobjCommon->fnPagination($larrresultFinals,$lintpage,$lintpagecount);
				$this->gobjsessionsis->progstudentregistrationpaginatorresult = $larrresultFinals;
			//}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/registration/registrationreport/unregstudents');
		}

        }
        
        
        /**
         * Function to show the student Details
         * @Author Vipul
         */
        public function studentdetailAction(){
                $this->view->studentprofileForm = $this->studentprofileForm;
                $nameConfDetail = array();
                $idUniversity = $this->gobjsessionsis->idUniversity;
                $confdetail = $this->lobjconfig->fnGetInitialConfigDetails($idUniversity);
                $nameConfDetail['count'] = $confdetail['NameDtlCount'];
                $temp = array();
                for ($i = 1; $i <= $nameConfDetail['count']; $i++) {
                  $field = 'NameDtl' . $i;
                  $temp[$i] = $confdetail[$field];
                }
                $nameConfDetail['fields'] = $temp;
                $this->view->NameconfDetail = $nameConfDetail;
                $this->view->defaultlanguage = $this->gobjsessionsis->UniversityLanguage;
                $IdRegistration = $this->_getParam('regID');
                $this->view->studentdetail = $this->lobjAddDropSubjectModel->getstudentprofiledetailstwo($IdRegistration);
        //	echo "<pre>";
        //	print_r($this->view->studentdetail['IdLandscape']);
                //asd($this->view->studentdetail);
                $lobjcountry = $this->lobjUser->fnGetCountryList();
                $this->studentprofileForm->PermCountry->addMultiOptions($lobjcountry)->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->CorrsCountry->addMultiOptions($lobjcountry)->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->OutcampusCountry->addMultiOptions($lobjcountry)->setAttribs(array('readonly' => 'true'));
                $this->view->persdet = $StudentPersonalDet = $this->view->studentdetail;
               
                $this->view->studentsemdetail = $this->lobjAddDropSubjectModel->getstudentprofilesemdetails($StudentPersonalDet['IdStudentRegistration']);
                $this->view->studentcourses = $this->lobjAddDropSubjectModel->getstudentcourses($StudentPersonalDet['IdStudentRegistration'],$this->view->studentdetail['IdLandscape']);

        //    echo "<pre>";
        //    print_r( $this->view->studentcourses);
                $lobjCommonModel = new App_Model_Common();
                if (isset($StudentPersonalDet['PermCountry']) && $StudentPersonalDet['PermCountry'] != ''){
                $PermState = $lobjCommonModel->fnGetCountryStateList($StudentPersonalDet['PermCountry']);
                foreach ($PermState as $PermStateresult) {
                  $this->studentprofileForm->PermState->addMultiOption($PermStateresult['key'], $PermStateresult['value'])->setAttribs(array('readonly' => 'true'));
                }
                }

                if (isset($StudentPersonalDet['PermState']) && $StudentPersonalDet['PermState'] != ''){
                $larrStateCityList = $lobjCommonModel->fnGetCityList($StudentPersonalDet['PermState']);
                foreach ($larrStateCityList as $PermCityresult) {
                  $this->studentprofileForm->PermCity->addMultiOption($PermCityresult['key'], $PermCityresult['value'])->setAttribs(array('readonly' => 'true'));
                }
                }

                if (isset($StudentPersonalDet['CorrsCountry']) && $StudentPersonalDet['CorrsCountry'] != ''){
                $CorrsState = $lobjCommonModel->fnGetCountryStateList($StudentPersonalDet['CorrsCountry']);
                foreach ($CorrsState as $CorrsStateresult) {
                  $this->studentprofileForm->CorrsState->addMultiOption($CorrsStateresult['key'], $CorrsStateresult['value'])->setAttribs(array('readonly' => 'true'));
                }
                }

                if (isset($StudentPersonalDet['CorrsState']) && $StudentPersonalDet['CorrsState'] != ''){
                $larrStateCityList = $lobjCommonModel->fnGetCityList($StudentPersonalDet['CorrsState']);
                foreach ($larrStateCityList as $CorrsCityresult) {
                  $this->studentprofileForm->CorrsCity->addMultiOption($CorrsCityresult['key'], $CorrsCityresult['value'])->setAttribs(array('readonly' => 'true'));
                }
                }

                if (isset($StudentPersonalDet['OutcampusCountry']) && $StudentPersonalDet['OutcampusCountry'] != ''){
                $OutcampusState = $lobjCommonModel->fnGetCountryStateList($StudentPersonalDet['OutcampusCountry']);
                foreach ($OutcampusState as $OutcampusStateresult) {
                  $this->studentprofileForm->OutcampusState->addMultiOption($OutcampusStateresult['key'], $OutcampusStateresult['value'])->setAttribs(array('readonly' => 'true'));
                }
                }

                if (isset($StudentPersonalDet['OutcampusState']) && $StudentPersonalDet['OutcampusState'] != ''){
                $larrStateCityList = $lobjCommonModel->fnGetCityList($StudentPersonalDet['OutcampusState']);
                foreach ($larrStateCityList as $OutcampusCityresult) {
                  $this->studentprofileForm->OutcampusCity->addMultiOption($OutcampusCityresult['key'], $OutcampusCityresult['value'])->setAttribs(array('readonly' => 'true'));
                }
                }
                if (isset($StudentPersonalDet['HomePhone']) && $StudentPersonalDet['HomePhone'] != '') {
                $phonepices = explode('-', $StudentPersonalDet['HomePhone']);
                $StudentPersonalDet['HomePhonecountrycode'] = $phonepices[0];
                $StudentPersonalDet['HomePhonestatecode'] = $phonepices[1];
                $StudentPersonalDet['HomePhone'] = $phonepices[2];
                }

                if (isset($StudentPersonalDet['Fax']) && $StudentPersonalDet['Fax'] != '') {
                $phonepic = explode('-', $StudentPersonalDet['Fax']);
                $StudentPersonalDet['Faxcountrycode'] = $phonepic[0];
                $StudentPersonalDet['Faxstatecode'] = $phonepic[1];
                $StudentPersonalDet['Fax'] = $phonepic[2];
                }

                if (isset($StudentPersonalDet['CellPhone']) && $StudentPersonalDet['CellPhone'] != '') {
                $phonecell = explode('-', $StudentPersonalDet['CellPhone']);
                $StudentPersonalDet['CellPhonecountrycode'] = $phonecell[0];
                $StudentPersonalDet['CellPhone'] = $phonecell[1];
                }

                if (isset($StudentPersonalDet['EmergencyHomePhone']) && $StudentPersonalDet['EmergencyHomePhone'] != '') {
                $phonepices = explode('-', $StudentPersonalDet['EmergencyHomePhone']);
                $StudentPersonalDet['EmergencyHomePhonecountrycode'] = $phonepices[0];
                $StudentPersonalDet['EmergencyHomePhonestatecode'] = $phonepices[1];
                $StudentPersonalDet['EmergencyHomePhone'] = $phonepices[2];
                }

                if (isset($StudentPersonalDet['EmergencyOffPhone']) && $StudentPersonalDet['EmergencyOffPhone'] != '') {
                $phonepices = explode('-', $StudentPersonalDet['EmergencyOffPhone']);
                $StudentPersonalDet['EmergencyOffPhonecountrycode'] = $phonepices[0];
                $StudentPersonalDet['EmergencyOffPhonestatecode'] = $phonepices[1];
                $StudentPersonalDet['EmergencyOffPhone'] = $phonepices[2];
                }

                if (isset($StudentPersonalDet['EmergencyCellPhone']) && $StudentPersonalDet['EmergencyCellPhone'] != '') {
                $phonecell = explode('-', $StudentPersonalDet['EmergencyCellPhone']);
                $StudentPersonalDet['EmergencyCellPhonecountrycode'] = $phonecell[0];
                $StudentPersonalDet['EmergencyCellPhone'] = $phonecell[1];
                }
                
                
                $this->studentprofileForm->PermAddressDetails->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->PermZip->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->SameCorrespAddr->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->CorrsAddressDetails->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->CorrsZip->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->SameOutcampusAddr->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->OutcampusAddressDetails->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->OutcampusZip->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->CellPhonecountrycode->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->CellPhone->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->HomePhonecountrycode->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->HomePhonestatecode->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->HomePhone->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->Faxcountrycode->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->Faxstatecode->setAttribs(array('readonly' => 'true'));
                $this->studentprofileForm->Fax->setAttribs(array('readonly' => 'true'));
                
                
                $this->view->studentprofileForm->populate($StudentPersonalDet);
		
	}
	
public function getschemesemesterAction(){
	$this->_helper->layout->disableLayout();
	$this->_helper->viewRenderer->setNoRender();
	$Idscheme= $this->_getParam('Idscheme');
	$semestersmain = $this->lobjAddDropSubjectModel->fngetsemestersmain($Idscheme);
	$semestersdetail = $this->lobjAddDropSubjectModel->fngetsemestersdetail($Idscheme);
	$j=count($semestersmain);
	for($i=0;$i<count($semestersdetail);$i++){
		$semestersmain[$j]['key'] = $semestersdetail[$i]['key']."_detail";
		$semestersmain[$j]['name'] = $semestersdetail[$i]['name'];
		$j++;
	}
	echo Zend_Json_Encoder::encode($semestersmain);
}
        
        
        
        






}