<?php
class Registration_SeekingForLandscapeController extends Base_Base {

    private $_gobjlog;
    private $model;
    private $auth;

    public function init() { //initialization function
        $this->_gobjlog = Zend_Registry::get ( 'log' );
        $this->fnsetObj();	
    }

    public function fnsetObj(){
        $this->model = new Registration_Model_DbTable_SeekingForLandscape();
        $this->scholarshipModel = new Studentfinance_Model_DbTable_Scholarship();
        $this->auth = Zend_Auth::getInstance();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Seeking For Landscape');
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        //var_dump(md5('ridhwan123'));
        if ($this->getRequest()->isPost()){
            $studentList = $this->model->getStudent();
            $i = 0;
            if ($studentList){
                foreach ($studentList as $studentLoop){
                    $landscapeId = $this->getStudentLandscapeId($studentLoop['IdStudentRegistration']);
                    
                    if ($landscapeId != 0){
                        $i++;
                    }
                    
                    $data = array(
                        'IdLandscape'=>$landscapeId
                    );
                    
                    //update landscape id
                    $this->model->updateStudent($data, $studentLoop['IdStudentRegistration']);
                }
            }
            $this->_helper->flashMessenger->addMessage(array('success' => $i.' student got landscape ID'));
            $this->view->backURL = $this->view->url(array('module'=>'registration','controller'=>'seeking-for-landscape', 'action'=>'index'),'default',true);
            $this->_redirect($this->view->backURL);
        }
    }
    
    public function getStudentLandscapeId($txnId){
		
        $db = Zend_Db_Table::getDefaultAdapter();

        //get landscape from txn 
        $txnDb = $this->model;
        $txnData = $txnDb->getTheStudentRegistrationDetail($txnId);

        $program_id = $txnData['IdProgram'];
        if($program_id){
            $program_id = $program_id;
        }else{
            throw new Exception('No student program data');
        }

        //assign landscape if null
        $landscapeDb = new App_Model_General_DbTable_Landscape();
        if($txnData['IdLandscape'] == null || $txnData['IdLandscape'] == 0){

            //get landscape based on program, scheme and active
            $where = array(
                'IdProgram = ?' => $program_id,
                'IdStartSemester = ?' => $txnData['IdIntake'],
                'IdProgramScheme = ?' => $txnData['IdProgramScheme']
            );

            $row_landscape = $landscapeDb->fetchRow($where);

            if($row_landscape == null){
                throw new Exception('No Active landscape');
            }else{
                //update tbl transaction
                //$data_update = array('IdLandscape' =>$row_landscape['IdLandscape']);
                //$db->update('tbl_studentregistration',$data_update, 'IdStudentRegistration = '.$txnId);
                $landscape_id = $row_landscape['IdLandscape'];
            }

        }else{
            $landscape_id = $txnData['IdLandscape'];
        }

        return $landscape_id;
    }
    
    public function updateRepeatSubjectAction(){
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
      
       
        
        if ($this->getRequest()->isPost()){
        	
            $formData = $this->getRequest()->getPost();
            
            $modelDB = new Registration_Model_DbTable_SeekingForLandscape();
            $studentList = $this->model->getStudentByProgramScheme($formData['scheme']);

            $i=0;
            $j=0;
            
            if ($studentList){
                foreach ($studentList as $studentLoop){
                    $modelDB->repeatSubject($studentLoop);
                }
            }

            $this->_helper->flashMessenger->addMessage(array('info' => $i.' student reflect and '.$j.' subject reflect'));
            $this->view->backURL = $this->view->url(array('module'=>'registration','controller'=>'seeking-for-landscape', 'action'=>'update-repeat-subject'),'default',true);
            $this->_redirect($this->view->backURL);
        }
    }
    
    
    
    
    
    public function updateCgpaCalculationAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            $studentList = $this->model->getStudentByProgramScheme($formData['scheme']);
            
            $i=0;
            $j=0;
            
            if ($studentList){
                foreach ($studentList as $studentLoop){
                    $this->model->updateRepeatCalculation($studentLoop);
                    $i++;
                }
            }
            $this->_helper->flashMessenger->addMessage(array('info' => $i.' student reflect and '.$j.' subject reflect'));
            $this->view->backURL = $this->view->url(array('module'=>'registration','controller'=>'seeking-for-landscape', 'action'=>'update-cgpa-calculation'),'default',true);
            $this->_redirect($this->view->backURL);
        }
    }
    
    public function updateFeeStructureAction(){
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        if ($this->getRequest()->isPost()){
        	$formData = $this->getRequest()->getPost();
			$type = $formData['type'];
			
			if($type == 1){
            	$studentList = $this->model->getApplicantDontHaveFsID();
			}else{
            	$studentList = $this->model->getStudentDontHaveFsID();
			}
            $i=0;
            $j=0;
            if ($studentList){
                foreach ($studentList as $studentLoop){
                	$IdProgram = $studentLoop['IdProgram'];
                	$scheme = $studentLoop['IdProgramScheme'];
                	$intake = $studentLoop['IdIntake'];
                	$category =  $studentLoop['appl_category'];
                	if($IdProgram && $scheme && $intake){
	                    $feeStructureData = $this->model->getApplicantFeeStructure($IdProgram,$scheme , $intake,$category, 0);
	                    if ($feeStructureData != false){
	                        
	                        if($type == 1){
	                        	$data = array (
		                            'at_fs_id'=>$feeStructureData['fs_id']
		                        );
	                        	$this->model->updateApplicant($data, $studentLoop['at_trans_id']);
	                        }else{
	                        	$data = array (
		                            'fs_id'=>$feeStructureData['fs_id']
		                        );
	                        	$this->model->updateStudent($data, $studentLoop['IdStudentRegistration']);
	                        }
	                        
	                        $i++;
	                    }else{
	                        $j++;
	                    }
                	}
                }
            }
            $this->_helper->flashMessenger->addMessage(array('success' => 'total student '.count($studentList).', '.$i.' student got fee structure id and '.$j.' student not fount fee structure id'));
            $this->view->backURL = $this->view->url(array('module'=>'registration','controller'=>'seeking-for-landscape', 'action'=>'update-fee-structure'),'default',true);
            $this->_redirect($this->view->backURL);
        }
    }
    
    public function barringMigrationAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        if ($this->getRequest()->isPost()){
            $listStud = $this->model->selectStudentBarring();
            //var_dump($listStud); exit;
            if ($listStud){
                $i = 0;
                foreach ($listStud as $loopStud){
                    //echo '---------------------------------------------------------------';
                    $studInfo = $this->model->getStudentInfo($loopStud['Student_ID']);
                    $currentSemester = $this->model->getCurrentSem($studInfo['schemeid']);
                    //var_dump($studInfo);
                    //var_dump($currentSemester);

                    $data = array(
                        'tbr_category'=>2, 
                        'tbr_appstud_id'=>$studInfo['IdStudentRegistration'], 
                        'tbr_type'=>866, 
                        'tbr_intake'=>$currentSemester['IdSemesterMaster'], 
                        'tbr_reason'=>'AAERC', 
                        'tbr_status'=>0, 
                        'tbr_createby'=>1, 
                        'tbr_createdate'=>date('Y-m-d')
                    );
                    $this->model->insertBarring($data);

                    $data2 = array(
                        'tbr_category'=>2, 
                        'tbr_appstud_id'=>$studInfo['IdStudentRegistration'], 
                        'tbr_type'=>867, 
                        'tbr_intake'=>$currentSemester['IdSemesterMaster'], 
                        'tbr_reason'=>'AAERC', 
                        'tbr_status'=>0, 
                        'tbr_createby'=>1, 
                        'tbr_createdate'=>date('Y-m-d')
                    );
                    $this->model->insertBarring($data2);

                    $i++;
                }
                echo $i;
            }
        }
    }
    
    public function defineSemesterStatusAction(){
        
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $userId = $this->auth->getIdentity()->iduser;

        if ($this->getRequest()->isPost()){
            $i = 0;
            $j = 0;
            $studentList = $this->model->getActiveStudent();
            
            if ($studentList){
                foreach ($studentList as $studentLoop){
                    $currentSem = $this->model->getCurrentSem($studentLoop['schemeid']);
                    
                    if ($currentSem){
                        $checkSemStatus = $this->model->checkSemesterStatus($studentLoop['IdStudentRegistration'], $currentSem['IdSemesterMaster']);
                        
                        if ($checkSemStatus){
                            $checkSubReg = $this->model->checkRegisterSubject($studentLoop['IdStudentRegistration'], $currentSem['IdSemesterMaster']);
                            
                            if ($checkSubReg){
                                $data = array(
                                    'studentsemesterstatus'=>130
                                );
                            }else{
                                $data = array(
                                    'studentsemesterstatus'=>131
                                );
                            }
                            $i++;
                            $this->model->updateSemesterStatus($data, $checkSemStatus['idstudentsemsterstatus']);
                        }else{
                            $checkSubReg = $this->model->checkRegisterSubject($studentLoop['IdStudentRegistration'], $currentSem['IdSemesterMaster']);
                            
                            if ($checkSubReg){
                                $data = array(
                                    'IdStudentRegistration'=>$studentLoop['IdStudentRegistration'], 
                                    'idSemester'=>$currentSem['IdSemesterMaster'], 
                                    'IdSemesterMain'=>$currentSem['IdSemesterMaster'], 
                                    'IdBlock'=>null, 
                                    'studentsemesterstatus'=>130, 
                                    'Level'=>1, 
                                    'Reason'=>'', 
                                    'UpdDate'=>date('Y-m-d h:i:s'), 
                                    'UpdUser'=>$userId, 
                                    'UpdRole'=>'admin'
                                );
                            }else{
                                $data = array(
                                    'IdStudentRegistration'=>$studentLoop['IdStudentRegistration'], 
                                    'idSemester'=>$currentSem['IdSemesterMaster'], 
                                    'IdSemesterMain'=>$currentSem['IdSemesterMaster'], 
                                    'IdBlock'=>null, 
                                    'studentsemesterstatus'=>131, 
                                    'Level'=>1, 
                                    'Reason'=>'', 
                                    'UpdDate'=>date('Y-m-d h:i:s'), 
                                    'UpdUser'=>$userId, 
                                    'UpdRole'=>'admin'
                                );
                            }
                            $j++;
                            $this->model->insertSemesterStatus($data);
                        }
                    }
                }
            }
            
            $this->_helper->flashMessenger->addMessage(array('success' => 'Process Finish, '.$i.' Student Semester Update & '.$j.' student semester insert'));
            $this->view->backURL = $this->view->url(array('module'=>'registration','controller'=>'seeking-for-landscape', 'action'=>'define-semester-status'),'default',true);
            $this->_redirect($this->view->backURL);
        }
    }
    
    public function differAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $userId = $this->auth->getIdentity()->iduser;
        
        $studentList = $this->model->getDifferStudent();
        
        if ($studentList){
            foreach ($studentList as $studentLoop){
                $lastSemStatus = $this->model->getLastSemester($studentLoop['IdStudentRegistration']);
                var_dump($studentLoop['registrationId']);
                echo '--------------------------------sem status ------------------<br/>';
                var_dump($lastSemStatus);
                
                if ($lastSemStatus){
                    $data = array(
                        'studentsemesterstatus'=>250, 
                    );
                    $this->model->updateSemesterStatus($data, $lastSemStatus['idstudentsemsterstatus']);
                }else{
                    $currentSem = $this->model->getCurrentSem($studentLoop['IdScheme']);
                    
                    $data = array(
                        'IdStudentRegistration'=>$studentLoop['IdStudentRegistration'], 
                        'idSemester'=>$currentSem['IdSemesterMaster'], 
                        'IdSemesterMain'=>$currentSem['IdSemesterMaster'], 
                        'IdBlock'=>null, 
                        'studentsemesterstatus'=>250, 
                        'Level'=>1, 
                        'Reason'=>'', 
                        'UpdDate'=>date('Y-m-d h:i:s'), 
                        'UpdUser'=>$userId, 
                        'UpdRole'=>'admin'
                    );
                    $this->model->insertSemesterStatus($data);
                }
            }
        }
        exit;
    }
    
    public function failedexamAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $userId = $this->auth->getIdentity()->iduser;
        
        $studentList = $this->model->getDifferStudent(94, 'testingizham2');
        //var_dump($studentList); exit;
        if ($studentList){
            foreach ($studentList as $studentLoop){
                $lastSemStatus = $this->model->getLastSemester($studentLoop['IdStudentRegistration']);
                var_dump($studentLoop['registrationId']);
                echo '--------------------------------sem status ------------------<br/>';
                var_dump($lastSemStatus);
                
                $datalalala = array(
                    'profileStatus'=>94
                );
                
                $this->model->updateStudentReg($datalalala, $studentLoop['IdStudentRegistration']);
                
                if ($lastSemStatus){
                    $data = array(
                        'studentsemesterstatus'=>132, 
                    );
                    $this->model->updateSemesterStatus($data, $lastSemStatus['idstudentsemsterstatus']);
                }else{
                    $currentSem = $this->model->getCurrentSem($studentLoop['IdScheme']);
                    
                    $data = array(
                        'IdStudentRegistration'=>$studentLoop['IdStudentRegistration'], 
                        'idSemester'=>$currentSem['IdSemesterMaster'], 
                        'IdSemesterMain'=>$currentSem['IdSemesterMaster'], 
                        'IdBlock'=>null, 
                        'studentsemesterstatus'=>132, 
                        'Level'=>1, 
                        'Reason'=>'', 
                        'UpdDate'=>date('Y-m-d h:i:s'), 
                        'UpdUser'=>$userId, 
                        'UpdRole'=>'admin'
                    );
                    $this->model->insertSemesterStatus($data);
                }
            }
        }
        exit;
    }
    
    public function disciplinaryAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $userId = $this->auth->getIdentity()->iduser;
        
        $studentList = $this->model->getDifferStudent(94, 'testingizham2');
        //var_dump($studentList); exit;
        if ($studentList){
            foreach ($studentList as $studentLoop){
                $lastSemStatus = $this->model->getLastSemester($studentLoop['IdStudentRegistration']);
                var_dump($studentLoop['registrationId']);
                echo '--------------------------------sem status ------------------<br/>';
                var_dump($lastSemStatus);
                
                if ($lastSemStatus){
                    $data = array(
                        'studentsemesterstatus'=>845, 
                    );
                    $this->model->updateSemesterStatus($data, $lastSemStatus['idstudentsemsterstatus']);
                }else{
                    $currentSem = $this->model->getCurrentSem($studentLoop['IdScheme']);
                    
                    $data = array(
                        'IdStudentRegistration'=>$studentLoop['IdStudentRegistration'], 
                        'idSemester'=>$currentSem['IdSemesterMaster'], 
                        'IdSemesterMain'=>$currentSem['IdSemesterMaster'], 
                        'IdBlock'=>null, 
                        'studentsemesterstatus'=>845, 
                        'Level'=>1, 
                        'Reason'=>'', 
                        'UpdDate'=>date('Y-m-d h:i:s'), 
                        'UpdUser'=>$userId, 
                        'UpdRole'=>'admin'
                    );
                    $this->model->insertSemesterStatus($data);
                }
            }
        }
        exit;
    }
    
    public function updateCtGredAction(){
        echo 'update ct gred here';
        
        $list = $this->model->getStudentToUpdateCtGred();
        //var_dump(count($list));
        //var_dump($list);
        
        if ($list){
            foreach ($list as $listLoop){
                if ($listLoop['grade'] == ''){
                    $data = array(
                        'grade_name'=>$listLoop['status'],
                    );
                }else{
                    $data = array(
                        'grade_name'=>$listLoop['grade'],
                    );
                }
                
                //var_dump($data);
                
                $this->model->updateGred($data, $listLoop['IdStudentRegSubjects']);
            }
        }
        
        exit;
    }
    
    public function updateIdLandscapeChangeModeAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $list = $this->model->getApplicantChangeMode();
        ///var_dump($list);
        ///exit;
        $i = 0;
        if ($list){
            foreach ($list as $loop){
                $landscape = $this->model->getNewLandscape($loop['IdProgram'], $loop['IdProgramScheme'], $loop['IdIntake']);
            
                $data = array(
                    'IdLandscape'=>$landscape['IdLandscape'],
                );
                $this->model->updateStudent($data, $loop['cma_studregid']);
                
                $i++;
            }
        }
        
        exit;
    }
    
    public function updateInitialCourseAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $auth = Zend_Auth::getInstance();
        
        $list = $this->model->getStudentInitialCourse();
        $txnDB = new Registration_Model_DbTable_ApplicantTransaction();
        $studentRegSubjectDB = new Registration_Model_DbTable_Studentregsubjects();
        $historyDB = new Registration_Model_DbTable_StudentregsubjectHistory();
        $registrationItemDB =new Registration_Model_DbTable_RegistrationItem();	
        $studentSemesterStatusDb =  new Registration_Model_DbTable_Studentsemesterstatus();
       
        if ($list){
            foreach ($list as $loop){
                echo '--------------------------------------------------------------------<br/>';
                var_dump($loop['registrationId']);
                echo '--------------------------------------------------------------------<br/>';
                $transactionData = $txnDB->getTransactionProfileData($loop['transaction_id']);
                
                //semester 
                if ($loop['IdScheme']==1){
                    $IdSemester = 184;
                }else{
                    $IdSemester = 185;
                }
                
                $checksemstat = $this->model->checkSemesterStatus($loop['IdStudentRegistration'], $IdSemester);
                
                if (!$checksemstat){
                    $lsemstatusArr = array( 
                        'IdStudentRegistration' => $loop['IdStudentRegistration'],									           
                        'idSemester' => $IdSemester,
                        'IdSemesterMain' => $IdSemester,								
                        'studentsemesterstatus' => 130, 	//Register idDefType = 32 (student semester status)
                        'Level'=>1,
                        'UpdDate' => date ('Y-m-d H:i:s'),
                        'UpdUser' => $auth->getIdentity()->iduser
                    );											
                    $studentSemesterStatusDb->addData($lsemstatusArr);
                }
                
                $prereg_subject_list=$this->initialRegPapers($transactionData);
                var_dump($prereg_subject_list);
                
                if ($prereg_subject_list){
                    $formdata['IdSemester'][$loop['transaction_id']]=$IdSemester;
                    $formdata['IdProgram'][$loop['transaction_id']]=$loop['IdProgram'];
                    $formdata['IdProgramScheme'][$loop['transaction_id']]=$loop['IdProgramScheme'];
                    
                    $prereg_subject_list=$registrationItemDB->getSubjectItems($formdata, $loop['transaction_id'], $prereg_subject_list);
                    
                    foreach ($prereg_subject_list as $prereg_subject_loop){
                        $check = $this->model->checkInitialCourse($loop['IdStudentRegistration'], $prereg_subject_loop['IdSubject']);
                        
                        echo '--------------------------------------------------------------------<br/>';
                        if ($check){
                            echo 'true <br/>';
                        }else{
                            echo 'false <br/>';
                            $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                            $statusInvoice = $invoiceClass->checkingInitialFee($loop['transaction_id'], $prereg_subject_loop['IdSubject']);
                            
                            $invoiceId = $statusInvoice['invoice_id'];
                            
                            if ($statusInvoice){
                                $active = $statusInvoice['Active'];
                            }else{
                                $active = 0;
                            }
	
                            //add table studentregsubject	
                            $subject["IdStudentRegistration"] = $loop['IdStudentRegistration'];
                            $subject['initial'] = 1;
                            $subject["IdSubject"] = $prereg_subject_loop['IdSubject'];		
                            $subject["IdSemesterMain"] = $IdSemester;
                            $subject["SemesterLevel"]= 1;
                            $subject["IdLandscapeSub"] = 0;
                            $subject["Active"]= $statusInvoice['Active'];
                            $subject["UpdDate"]   = date('Y-m-d H:i:s');
                            $subject["UpdUser"]   = $auth->getIdentity()->iduser;
                            $subject["IdCourseTaggingGroup"] = 0;
                            $IdStudentRegSubjects = $studentRegSubjectDB->addData($subject);	


                            //add table studentregsubject audit trail/history
                            $subject2['IdStudentRegSubjects']=$IdStudentRegSubjects;
                            $subject2["IdStudentRegistration"] = $loop['IdStudentRegistration'];
                            $subject2['initial'] = 1;
                            $subject2["IdSubject"] = $prereg_subject_loop['IdSubject'];		
                            $subject2["IdSemesterMain"] = $IdSemester;
                            $subject2["SemesterLevel"]= 1;
                            $subject2["Active"]= $statusInvoice['Active'];
                            $subject2["UpdDate"]   = date('Y-m-d H:i:s');
                            $subject2["UpdUser"]   = $auth->getIdentity()->iduser;
                            $subject2["IdCourseTaggingGroup"] = 0;
                            $historyDB->addData($subject2);		

                            $this->saveItem($IdStudentRegSubjects,$loop['IdStudentRegistration'],$prereg_subject_loop['IdSubject'],$IdSemester,$prereg_subject_loop,$invoiceId);
                        }
                    }
                }
            }
        }
        
        exit;
    }
    
    function initialRegPapers($txnData){
        $landscapeDB = new App_Model_General_DbTable_Landscape();
        $applicantProgramDB = new App_Model_Application_DbTable_ApplicantProgram();

        $program=$applicantProgramDB->getProgrambyTxn($txnData['at_trans_id']);

        $subject_list = $landscapeDB->getLandscapeSubject($program['ap_prog_id'],$program['ap_prog_scheme'],$txnData['at_intake']);
        $landscapeInfo = $landscapeDB->getLandscapeInfo($program['ap_prog_id'],$program['ap_prog_scheme'],$txnData['at_intake']);

        $table = '';
        $subject_list2 = false;
        if (!empty($landscapeInfo)){
            $tagInfo = $landscapeDB->getLandscapeSubjectTag($landscapeInfo['IdLandscape']);
            $tagEncode = json_decode($tagInfo['subjectdata'],true);

            $intakeInfo = $landscapeDB->getIntakeInfo($txnData['at_intake']);
            $semInfo = $landscapeDB->getSemesterInfo($intakeInfo['sem_year'], $intakeInfo['sem_seq']);

            $i = 0;

            if(count($subject_list)>0){
                foreach ($subject_list as $loop){
                    if (isset($tagEncode[$loop['IdSubject']])){
                        //var_dump($tagEncode[$loop['IdSubject']]);	
                        if (isset($tagEncode[$loop['IdSubject']][$semInfo['SemesterType']])){	
                            $loop['subject_id'] = $loop['IdSubject'];							
                            $subject_list2[$i] = $loop;
                            $i++;
                        }
                    }
                }
            }
        }//end  
        return $subject_list2;
    }//end function
    
    function saveItem($IdStudentRegSubjects,$IdStudentRegistration,$idSubject,$IdSemester,$list,$invoiceId=NULL){
        $db = Zend_Db_Table::getDefaultAdapter();
        $auth = Zend_Auth::getInstance();  

        foreach($list['item'] as $det){

            /*
            * comment utk cater 
            * if($det['fi_id'] == 89){ //exam
            $item = 879;
            }elseif($det['fi_id'] == 90){ //paper
            $item = 890;
            }*/

            if($det['item_id']){ 
                $item = $det['item_id'];
            }

            //check for duplicate entry
            $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects_detail'))
            ->where('a.regsub_id = ?',$IdStudentRegSubjects)
            ->where('a.item_id = ?',$item);		

            $resultsa = $db->fetchRow($select); 	

            if(!$resultsa){
                $dataitem = array(
                    'student_id'		=> $IdStudentRegistration,
                    'regsub_id'			=> $IdStudentRegSubjects,
                    'semester_id'		=> $IdSemester,
                    'subject_id'		=> $idSubject,
                    'item_id'			=> $item,
                    'section_id'		=> 0,
                    'invoice_id'		=> $invoiceId,
                    'ec_country'		=> 0,
                    'ec_city'			=> 0,
                    'created_date'		=> date ( 'Y-m-d H:i:s'),
                    'created_by'		=> $auth->getIdentity()->iduser,
                    'created_role'		=> 'admin',
                    'ses_id'			=> 0
                );


                //$db->insert('tbl_studentregsubjects_detail', $data);
                $regSubjectItem = new Registration_Model_DbTable_StudentRegistrationItem();
                $idRegSubjectItem = $regSubjectItem->addData($dataitem);

                $dataitem['id'] = $idRegSubjectItem;
                $dataitem["message"] = 'Student Course Regisration: Add Item';
                $dataitem['createddt']=date("Y-m-d H:i:s");
                $dataitem['createdby']=$auth->getIdentity()->id;

                $db->insert('tbl_studentregsubjects_detail_history',$dataitem);

                //exam registration
                if($item==879){							
                    $this->saveExamRegistration($IdSemester,$idSubject,$IdStudentRegistration);
                }
            }

        }//end foreach
    }
    
    public function saveExamRegistration($IdSemester,$idSubject,$IdStudentRegistration){
        $auth = Zend_Auth::getInstance();
        $db =  Zend_Db_Table::getDefaultAdapter();
        $examRegDB = new Registration_Model_DbTable_ExamRegistration();

        $dataer['er_idStudentRegistration']=$IdStudentRegistration;		
        $dataer['er_idSemester']=$IdSemester;
        $dataer['er_idProgram']=0;
        $dataer['er_idSubject']=$idSubject;
        $dataer['er_idCountry']=0;
        $dataer['er_idCity']=0;
        $dataer['er_status']=764; //764:Regiseterd
        $dataer['er_createdby']=$auth->getIdentity()->iduser;
        $dataer['er_createddt']= date ( 'Y-m-d H:i:s');	

        //check if exist
        //list course
        $sql = $db->select()
        ->from(array('er'=>'exam_registration'))											
        ->where('er.er_idSemester = ?',$IdSemester)
        ->where('er.er_idSubject = ?',$idSubject)
        ->where('er.er_idStudentRegistration = ?',$IdStudentRegistration);			
        $examreg = $db->fetchRow($sql);

        if(!$examreg){					
            $examRegDB->addData($dataer);					
        }
    }//end add exam center
    
    public function completeStudentAction(){
        $list = $this->model->getCompleteList();
        
        if ($list){
            foreach ($list as $loop){
                $lastSemStatus = $this->model->getLastSemester($loop['IdStudentRegistration']);
                var_dump($lastSemStatus);
                
                $data2 = array(
                    'profileStatus'=>248, 
                );
                $this->model->updateStudentReg($data2, $loop['IdStudentRegistration']);
                
                if ($lastSemStatus){
                    $data = array(
                        'studentsemesterstatus'=>229, 
                    );
                    $this->model->updateSemesterStatus($data, $lastSemStatus['idstudentsemsterstatus']);
                }else{
                    $currentSem = $this->model->getCurrentSem($loop['IdScheme']);
                    
                    $data = array(
                        'IdStudentRegistration'=>$loop['IdStudentRegistration'], 
                        'idSemester'=>$currentSem['IdSemesterMaster'], 
                        'IdSemesterMain'=>$currentSem['IdSemesterMaster'], 
                        'IdBlock'=>null, 
                        'studentsemesterstatus'=>229, 
                        'Level'=>1, 
                        'Reason'=>'', 
                        'UpdDate'=>date('Y-m-d h:i:s'), 
                        'UpdUser'=>1, 
                        'UpdRole'=>'admin'
                    );
                    $this->model->insertSemesterStatus($data);
                }
            }
        }
        
        exit;
    }
    
    public function hostelMigrationAction(){
        $list = $this->model->getHosMigDat();
        
        if ($list){
            foreach($list as $loop){
                $data = array(
                    'IdStudentRegistration'=>$loop['IdStudentRegistration'],
                    'IdHostelRoom'=>$loop['idroom'],
                    'ActivityDate'=>date('Y-m-d h:i:s'),
                    'Activity'=>'Check In',
                    'checkinDate'=>$loop['datein'],
                    'checkinDate2'=>null,
                    'OldValue'=>'',
                    'NewValue'=>'Check In',
                    'Remarks'=>'migrate',
                    'UpdUser'=>1,
                    'UpdDate'=>date('Y-m-d h:i:s'),
                );
                $this->model->insertHostelMigrate($data);
            }
        }
        exit;
    }
    
    public function updatemissingcnAction(){
        $list = $this->model->getInvoiveToCn();
        
        if ($list){
            foreach ($list as $loop){
                $check = $this->model->checkCreditNote($loop['invoiceid_0']);
                
                if (!$check){
                    $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                    $invoiceClass->generateCreditNote($loop['IdStudentRegistration'], $loop['EquivalentCourse'], $loop['invoiceid_0'], 0);
                }
            }
        }
        exit;
    }
    
    public function updateregsubidappAction(){
        $list = $this->model->getAppCourse();
        
        if ($list){
            foreach ($list as $loop){
                $resubinfo = $this->model->getRegSubject($loop['sa_cust_id'], $loop['sac_subjectid'], $loop['sa_semester_id']);
                
                if ($resubinfo){
                    $bind = array(
                        'sac_regsubid'=>$resubinfo['IdStudentRegSubjects']
                    );

                    $this->model->updateRegSubId($bind, $loop['sac_id']);
                }
            }
        }
        exit;
    }
    
    public function updateregsubidreappAction(){
        $list = $this->model->getReAppCourse();
        //var_dump($list); exit;
        if ($list){
            foreach ($list as $loop){
                $resubinfo = $this->model->getRegSubject($loop['sre_cust_id'], $loop['src_subjectid'], $loop['sre_semester_id']);
                
                if ($resubinfo){
                    $bind = array(
                        'src_regsubid'=>$resubinfo['IdStudentRegSubjects']
                    );

                    $this->model->updateRegSubId2($bind, $loop['src_id']);
                }
            }
        }
        exit;
    }
    
    public function terNotCompleteAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->view->title = 'Ter Not Complete';
        
        $this->model->truncateTerNotComplete();
        
        $termodel = new App_Model_Question_DbTable_StudentAnswer();
        
        $schemeList = $this->model->getSchemeList();
        //var_dump($schemeList);
        $this->view->schemeList = $schemeList;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $studentList = $this->model->getStudentTerNotComplete($formData['semester']);
            
            if ($studentList){
                foreach ($studentList as $studentLoop){
                    
                    $studentData = array(
                        'IdBranch'=> $studentLoop['IdBranch'],
                        'IdProgram'=> $studentLoop['exam_status']=='U' ? $studentLoop['audit_program']:$studentLoop['IdProgram'],
                        'IdProgramScheme'=> $studentLoop['exam_status']=='U' ? $studentLoop['audit_programscheme']:$studentLoop['IdProgramScheme'],
                        'IdStudentRegistration'=> $studentLoop['IdStudentRegistration'],
                    );
                    
                    $checkCompleteTer = $termodel->getTerCompleteStatus($studentData,$formData['semester'],$studentLoop['IdSubject'],$studentLoop['IdCourseTaggingGroup']);
                
                    if($checkCompleteTer == 1){
                        //insert into report table
                        $data = array(
                            'tnc_regid'=>$studentLoop['IdStudentRegistration'],
                            'tnc_subregid'=>$studentLoop['IdStudentRegSubjects']
                        );
                        $this->model->insertTerNotComplete($data);
                    }
                }
            }
            
            //redirect here
            $this->_redirect($this->baseUrl . '/registration/seeking-for-landscape/view-ternotcomplete/');
        }
    }
    
    public function viewTernotcompleteAction(){
        $this->view->title = 'Ter Not Complete';
        $list = $this->model->getTerNotComplete();
        $this->view->list = $list;
    }
    
    public function exportTernotcompleteAction(){
        $this->_helper->layout->disableLayout();
        
        $list = $this->model->getTerNotComplete();
        $this->view->list = $list;
        
        $this->view->filename = date('Ymd').'_ternotcomplete.xls';
    }
    
    public function getSemesterAjaxAction(){
        $id = $this->_getParam('id',0);
        
        $this->_helper->layout->disableLayout();
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        $semList = $this->model->getSemesterByScheme($id);
        
        $json = Zend_Json::encode($semList);
		
	echo $json;
	exit();
    }
    
    public function updateStaffIdAction(){
        $list = $this->model->getStaffList();
        //var_dump($list);
        
        if ($list){
            foreach ($list as $loop){
                if (is_numeric($loop['StaffId'])){
                    $oldstaffid = $loop['StaffId'];
                    $length = strlen($loop['StaffId']);
                    
                    switch($length){
                        case 1:
                            $newstaffid = '00000'.$loop['StaffId'];
                            break;
                        case 2:
                            $newstaffid = '0000'.$loop['StaffId'];
                            break;
                        case 3:
                            $newstaffid = '000'.$loop['StaffId'];
                            break;
                        case 4:
                            $newstaffid = '00'.$loop['StaffId'];
                            break;
                        case 5:
                            $newstaffid = '0'.$loop['StaffId'];
                            break;
                        case 6:
                            $newstaffid = $loop['StaffId'];
                            break;
                    }
                    
                    echo '<fieldset>'
                        .'Old Staff Id : '.$oldstaffid.'<br/>'
                        .'Old Staff Id Length : '.$length.'<br/>'
                        .'New Staff Id : '.$newstaffid.'<br/>'
                        .'New Staff Id Length : '.strlen($newstaffid).'<br/>'
                        .'</fieldset>';
                    
                    $data = array(
                        'StaffId'=>$newstaffid
                    );
                    $this->model->updateStaff($data, $loop['IdStaff']);
                }
            }
        }
        exit;
    }

    public function exportScheduleIntoQpeventAction(){

        $scheduleDB = new GeneralSetup_Model_DbTable_CourseGroupSchedule();
        $groupList = $this->model->getGroup();
        //var_dump($groupList); //exit;
        if ($groupList){
            foreach ($groupList as $groupLoop){
                $groupProgram = $this->model->getGroupProgram($groupLoop['IdCourseTaggingGroup']);
                echo '------------------------------------------------------------<br/>';
                echo 'Group Name : '.$groupLoop['GroupName'].'<br/>';
                echo 'Group Code : '.$groupLoop['GroupCode'].'<br/>';
                echo '------------------------------------------------------------<br/>';

                $scheduleList = $this->model->getGroupSchedule($groupLoop['IdCourseTaggingGroup']);

                if ($scheduleList){
                    foreach ($scheduleList as $scheduleLoop){
                        //var_dump($scheduleLoop); //exit;
                        //var_dump($groupProgram); exit;
                        if ($groupProgram){
                            foreach ($groupProgram as $groupProgramLoop){
                                $classstarttime = $scheduleLoop["sc_start_time"];
                                $classendtime = $scheduleLoop["sc_end_time"];

                                $class_start_time = ($scheduleLoop["sc_start_time"]*60);
                                $class_end_time = ($scheduleLoop["sc_end_time"]*60);

                                if ($scheduleLoop["sc_date"]=='0000-00-00' || $scheduleLoop["sc_date"]==null){
                                    $start_day = 0;
                                    $end_day = 0;
                                    $program_mode = 2;
                                }else{
                                    $start_day = strtotime($scheduleLoop["sc_date"].' '.$classstarttime);
                                    $end_day = strtotime($scheduleLoop["sc_date"].' '.$classendtime);
                                    $program_mode = 1;
                                }

                                $start_time = strtoupper(date('h:i a', strtotime($classstarttime)));
                                $end_time = strtoupper(date('h:i a', strtotime($classendtime)));

                                $venue = $scheduleDB->getDefination($scheduleLoop["sc_venue"]);

                                $classtype = $scheduleDB->getDefination($scheduleLoop['idClassType']);

                                $lecturer = $scheduleDB->getLecturer($scheduleLoop["IdLecturer"]);

                                $semesterInfo = $scheduleDB->getSemesterById($groupLoop['IdSemester']);

                                $qpData = array(
                                    'IdCourseTaggingGroup'=>$groupLoop['IdCourseTaggingGroup'],
                                    'sc_id'=>$scheduleLoop['sc_id'],
                                    'title'=>$groupLoop['GroupName'],
                                    'description'=>'',
                                    'venue'=>isset($venue['DefinitionDesc']) ? $venue['DefinitionDesc']:'',
                                    'all_day'=>0,
                                    'start_day'=>$start_day,
                                    'end_day'=>$end_day,
                                    'start_time'=>$start_time,
                                    'end_time'=>$end_time,
                                    'background_color'=>'',
                                    'border_color'=>'',
                                    'text_color'=>'',
                                    'date_created'=>strtotime(date('Y-m-d H:i:s')),
                                    'last_updated'=>strtotime(date('Y-m-d H:i:s')),
                                    'access'=>0,
                                    'category'=>1,
                                    'status'=>0,
                                    'repeat'=>0,
                                    'program_mode'=>$program_mode,
                                    'course_code'=>$groupLoop['subject_code'],
                                    'course_name'=>$groupLoop['subject_name'],
                                    'merge_group'=>null,//to be declare
                                    'section'=>null,//to be declare
                                    'class_size'=>$groupLoop['maxstud'],
                                    'class_duration'=>($class_start_time-$class_end_time),
                                    'lecturer_code'=>$lecturer['IdStaff'],
                                    'lecturer_name'=>$lecturer['FullName'],
                                    'lecturer_email'=>$lecturer['Email'],
                                    'class_id'=>$groupLoop['GroupName'],
                                    'class_type'=>$classtype['DefinitionDesc'],
                                    'class_number'=>0,//to be declare
                                    'class_date'=>$start_day,
                                    'class_day'=>$scheduleLoop["sc_day"],
                                    'class_start_time'=>$class_start_time,
                                    'class_end_time'=>$class_end_time,
                                    'in_workload'=>0,
                                    'ordering'=>0,
                                    'tag1_title'=>'Programme',
                                    'tag1_value'=>$groupProgramLoop['ProgramName'],
                                    'tag2_title'=>'Programme Mode',
                                    'tag2_value'=>$groupProgramLoop['mop'],
                                    'tag3_title'=>'Programme Status',
                                    'tag3_value'=>$groupProgramLoop['mos'],
                                    'tag4_title'=>'Branch',
                                    'tag4_value'=>isset($groupProgramLoop['BranchName']) ? $groupProgramLoop['BranchName']:'',
                                    'tag5_title'=>'Study Mode',
                                    'tag5_value'=>$groupProgramLoop['pt'],
                                    'tag6_title'=>'Department',
                                    'tag6_value'=>$groupProgramLoop['EnglishDescription'],
                                    'tag7_title'=>'Collaborative Partner',
                                    'tag7_value'=>$groupLoop['BranchName'],
                                    'export_date'=>strtotime(date('Y-m-d H:i:s')),
                                    'schedule_id'=>0,
                                    'term'=>$semesterInfo['SemesterMainName'].' Semester',
                                    'session'=>0,
                                    'version'=>0,
                                    'field_change1'=>0,
                                    'field_change2'=>0,
                                    'field_change3'=>0,
                                    'field_change4'=>0,
                                    'field_change5'=>0,
                                    'field_change6'=>0,
                                    'field_change7'=>0
                                );
                                $scheduleDB->insertQpEvent($qpData);
                            }
                        }
                    }
                }
            }
        }
        exit;
    }

    public function regenerateInvoiveExAction(){
        $lobjstudentregistrationModel = new Registration_Model_DbTable_Studentregistration();
        $list = $this->model->getExApp(3400);

        if ($list){
            foreach ($list as $loop){
                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                $invoiceid = $invoiceClass->generateOtherInvoiceStudent($loop['IdStudentRegistration'],$loop['semesterId'],'EX',$loop['EquivalentCourse'],0);

                $invData = array(
                    'invoiceid_0'=>$invoiceid
                );
                $lobjstudentregistrationModel->updateCtSub($invData, $loop['IdCreditTransferSubjects']);
            }
        }
        exit;
    }

    public function preGraduationListAction(){
        $graduationDB = new Graduation_Model_DbTable_ConvocationChecklist();
        $db = Zend_Db_Table::getDefaultAdapter();

        $list = $this->model->getStudentComplete();

        if ($list){
            foreach ($list as $loop){
                $data = array(
                    'idStudentRegistration'=>$loop['IdStudentRegistration'],
                    'status'=>1,
                    'c_id'=>10, //10 id hardcode convocation
                    'shortlisted_date'=>date('Y-m-d H:i:s'),
                    'shortlisted_by'=>1,
                    'approve_date'=>date('Y-m-d H:i:s'),
                    'approve_by'=>1,
                    /*'graduate_date',
                    'graduate_by',
                    'publish',
                    'publish_createdt',
                    'publish_createby',
                    'confirm_attend',
                    'confirm_by',
                    'confirm_date',
                    'confirm_role'*/
                );
                $gid = $this->model->insertPreGraduationList($data);

                $checklist = $graduationDB->getChecklistByProgram(10, $loop['IdProgram']); //10 id hardcode convocation

                if ($checklist) {
                    foreach ($checklist as $check) {

                        $checkData['pregraduate_list_id'] = $gid;
                        $checkData['IdStudentRegistration'] = $loop['IdStudentRegistration'];
                        $checkData['idChecklist'] = $check['idChecklist'];
                        $checkData['psc_createddt'] = date('Y-m-d H:i:s');
                        $checkData['psc_createdby'] = 1;

                        $db->insert('pregraduate_student_checklist', $checkData);
                    }
                }
            }
        }
        exit;
    }

    public function cancelAppCourseReceiptAction(){
        $list = $this->model->getAppCourseNoReceiptCancel();
        //var_dump($list);

        if ($list){
            foreach ($list as $loop){
                $this->cancelSponsorInvoice($loop['sac_receiptid']);

                $receiptData = array(
                    'sac_receiptid'=>0
                );
                $this->scholarshipModel->updateApplicationCourse($receiptData, $loop['sac_id']);
            }
        }
        exit;
    }

    public function cancelReappCourseReceiptAction(){
        $list = $this->model->getReAppCourseNoReceiptCancel();
        //var_dump($list);

        if ($list){
            foreach ($list as $loop){
                $this->cancelSponsorInvoice($loop['src_receiptid']);

                $receiptData = array(
                    'src_receiptid'=>0
                );
                $this->scholarshipModel->updateReApplicationCourse($receiptData, $loop['src_id']);
            }
        }
        exit;
    }

    public function cancelSponsorInvoice($receiptid){
        $sponsorinvreceipt = $this->model->getSponsorInvReceipt($receiptid);

        $sponsorinvreceiptinv = $this->model->sponsorInvReceiptInv($receiptid);

        if ($sponsorinvreceiptinv){
            foreach ($sponsorinvreceiptinv as $loop2){
                $sponsorInvDetail = $this->model->sponsorInvDetail($loop2['rcp_inv_sponsor_dtl_id']);

                $invoiceDetail = $this->model->invoiceDetail($sponsorInvDetail['sp_invoice_det_id']);

                $paidInvDetails = $invoiceDetail['paid']-$sponsorInvDetail['sp_paid'];
                $balanceInvDetails = $invoiceDetail['balance']+$sponsorInvDetail['sp_paid'];

                $invoiceDetailData = array(
                    'paid'=> $paidInvDetails,
                    'balance'=> $balanceInvDetails
                );
                $this->model->updateInvoiceDetail($invoiceDetailData, $invoiceDetail['id']);

                $invoiceMain = $this->model->invoiceMain($sponsorInvDetail['sp_invoice_id']);

                $paidInvMain = $invoiceMain['bill_paid']-$sponsorInvDetail['sp_paid'];
                $balanceInvMain = $invoiceMain['bill_balance']+$sponsorInvDetail['sp_paid'];

                $invoiceMainData = array(
                    'bill_paid'=> $paidInvMain,
                    'bill_balance'=> $balanceInvMain
                );
                $this->model->updateInvoiceMain($invoiceMainData, $invoiceMain['id']);

                $cancelSponsorData = array(
                    'sp_status'=>'X',
                    'sp_cancel_by'=>1,
                    'sp_cancel_date'=>date('Y-m-d H:i:s')
                );
                $this->model->updateSponsorMain($cancelSponsorData, $sponsorInvDetail['sp_id']);
            }
        }

        $cancelReceiptData = array(
            'rcp_status'=>'CANCEL',
            'cancel_by'=>1,
            'cancel_date'=>date('Y-m-d H:i:s')
        );
        $this->model->updateReceiptMain($cancelReceiptData, $sponsorinvreceipt['rcp_id']);
    }

    public function updateFinancialAidAppCourseAction(){

        $db = getDB();
        $list = $this->model->getAppCourseNoReceipt();

        if ($list){
            foreach ($list as $loop){
                $arrinvdet = array();
                $totalamount = 0;
                $itemList = $this->scholarshipModel->getSubjectItem($loop['sac_regsubid']);
                $info = $this->model->viewApp($loop['sac_said']);

                if ($itemList) {
                    foreach ($itemList as $itemLoop) {
                        if ($itemLoop['invoice_id'] != null) {
                            $invoiceList = $this->scholarshipModel->getInvoiceDetails($itemLoop['invoice_id']);
                            $invmaininfo = $this->scholarshipModel->getInvoiceMain($itemLoop['invoice_id']);

                            if ($invoiceList) {
                                foreach ($invoiceList as $invoiceLoop) {

                                    $financialaidcoverage = $this->scholarshipModel->financialAidCoverage($invoiceLoop['fi_id']);

                                    //todo generate bayaran
                                    if ($financialaidcoverage) {
                                        if ($financialaidcoverage['calculation_mode'] == 1) { //amount
                                            if ($financialaidcoverage['amount'] > $invoiceLoop['amount']) {
                                                $amount = $invoiceLoop['amount'];
                                            } else {
                                                $amount = $financialaidcoverage['amount'];
                                            }
                                        } else { //percentage
                                            $amount = (($financialaidcoverage['amount'] / 100) * $invoiceLoop['amount']);
                                        }

                                        $totalamount = ($totalamount + $amount);

                                        $desc = $loop['SubCode'] . ' - ' . $loop['SubjectName'] . ', ' . $invoiceLoop['fee_item_description'];

                                        $cnDetailData = array(
                                            //'subregdetid' =>  $itemLoop['id'],
                                            'sp_invoice_id' => $invoiceLoop['invoice_main_id'],
                                            'sp_invoice_det_id' => $invoiceLoop['id'],
                                            'sp_amount' => $amount,
                                            'sp_balance' => $amount,
                                            'sp_receipt' => $amount,
                                            'sp_description' => $desc
                                        );
                                        $arrinvdet[] = $cnDetailData; //array push
                                    }
                                }
                            }
                        }

                        if (count($arrinvdet) > 0){
                            $invoiceclass = new icampus_Function_Studentfinance_Invoice();
                            $bill_no =  $invoiceclass->getBillSeq(11, date('Y'));

                            $data_dn = array(
                                'sp_fomulir_id' => $bill_no,
                                'sp_txn_id' => $info['transaction_id'],
                                'sp_IdStudentRegistration' => $info['IdStudentRegistration'],
                                //'sp_batch_id' => $btch_id,
                                'sp_amount' => $totalamount,
                                'sp_balance' => $totalamount,
                                'sp_type' => 3,
                                'sp_type_id' => 1,
                                'sp_invoice_date' => date('Y-m-d'),
                                'sp_description' => 'Financial Assistance',
                                'sp_currency_id' => $invmaininfo['currency_id'],
                                'sp_exchange_rate' => $invmaininfo['exchange_rate'],
                                'sp_creator'=>1,
                                'sp_create_date'=>date('Y-m-d H:i:s'),
                                'sp_status' => 'E',//entry
                            );
                            $spinvmainid = $this->scholarshipModel->saveSponsorInvMain($data_dn);

                            //todo generate receipt
                            $receipt_no = $this->getReceiptNoSeq();

                            $receiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();

                            $receipt_data = array(
                                'rcp_no' => $receipt_no,
                                'rcp_account_code' => 96,
                                'rcp_date' => date('Y-m-d H:i:s'),
                                'rcp_receive_date' => date('Y-m-d'),
                                'rcp_batch_id' => 0,
                                'rcp_type' => 3,
                                'rcp_type_id' => 1,
                                'rcp_description' => 'Fisabilillah - '.$info['SemesterMainName'].' - '.$loop['SubCode'] . ' - ' . $loop['SubjectName'],
                                'rcp_amount' => $totalamount,
                                'rcp_adv_amount' => 0,
                                'rcp_gainloss' => 0,
                                'rcp_gainloss_amt' => 0,
                                'rcp_cur_id' => $invmaininfo['currency_id'],
                                'rcp_exchange_rate' => $invmaininfo['exchange_rate'],
                                'p_payment_mode_id' => 20,
                                'p_cheque_no' => null,
                                'p_doc_bank' => null,
                                //'p_doc_branch' => isset($formData['payment_doc_branch'])?$formData['payment_doc_branch']:null,
                                'p_terminal_id' => null,
                                'p_card_no' => null,
                                'rcp_create_by'=>1
                            );
                            $receipt_id = $receiptDb->insert($receipt_data);

                            //todo update receipt id
                            $receiptData = array(
                                'sac_receiptid'=>$receipt_id
                            );
                            $this->scholarshipModel->updateApplicationCourse($receiptData, $loop['sac_id']);

                            $data_rcp_stud = array(
                                'rcp_inv_rcp_id' => $receipt_id,
                                'IdStudentRegistration' => $info['IdStudentRegistration'],
                                'rcp_inv_amount' => $totalamount,
                                'rcp_adv_amount' => 0,
                                'rcp_inv_cur_id' => $invmaininfo['currency_id'],
                                'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                                'rcp_inv_create_by'=>1
                            );
                            $db->insert('sponsor_invoice_receipt_student',$data_rcp_stud);
                            $idStd = $db->lastInsertId();

                            $receiptInvoiceDb = new Studentfinance_Model_DbTable_SponsorReceiptInvoice();

                            foreach ($arrinvdet as $arrinvdetLoop){
                                $arrinvdetLoop['sp_id']=$spinvmainid;

                                $spinvdetailid = $this->scholarshipModel->saveSponsorInvDetail($arrinvdetLoop);

                                $data_rcp_inv = array(
                                    'rcp_inv_rcp_id' => $receipt_id,
                                    'rcp_inv_sp_id' => $spinvmainid,
                                    'rcp_inv_student' => $idStd,
                                    'rcp_inv_sponsor_dtl_id' => $spinvdetailid,
                                    'rcp_inv_amount' => $arrinvdetLoop['sp_amount'],
                                    'rcp_inv_amount_default' => $arrinvdetLoop['sp_amount'],
                                    'rcp_inv_cur_id' => $invmaininfo['currency_id'],
                                    'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                                    'rcp_inv_create_by'=>1
                                );
                                $receiptInvoiceDb->insert($data_rcp_inv);
                            }

                            //approve invoice
                            $this->approveSponsorInvoive($receipt_id);
                        }
                    }
                }
            }
        }
        exit;
    }

    public function updateFinancialAidReappCourseAction(){
        $db = getDB();
        $list = $this->model->getReAppCourseNoReceipt();

        if ($list) {
            foreach ($list as $loop) {
                $arrinvdet = array();
                $totalamount = 0;
                $itemList = $this->scholarshipModel->getSubjectItem($loop['src_regsubid']);
                $info = $this->model->getReApplication($loop['src_sreid']);

                if ($itemList) {
                    foreach ($itemList as $itemLoop) {
                        if ($itemLoop['invoice_id'] != null) {
                            $invoiceList = $this->scholarshipModel->getInvoiceDetails($itemLoop['invoice_id']);
                            $invmaininfo = $this->scholarshipModel->getInvoiceMain($itemLoop['invoice_id']);

                            if ($invoiceList) {
                                foreach ($invoiceList as $invoiceLoop) {

                                    $financialaidcoverage = $this->scholarshipModel->financialAidCoverage($invoiceLoop['fi_id']);

                                    //todo generate bayaran
                                    if ($financialaidcoverage) {
                                        if ($financialaidcoverage['calculation_mode'] == 1) { //amount
                                            if ($financialaidcoverage['amount'] > $invoiceLoop['amount']) {
                                                $amount = $invoiceLoop['amount'];
                                            } else {
                                                $amount = $financialaidcoverage['amount'];
                                            }
                                        } else { //percentage
                                            $amount = (($financialaidcoverage['amount'] / 100) * $invoiceLoop['amount']);
                                        }

                                        $totalamount = ($totalamount + $amount);

                                        $desc = $loop['SubCode'] . ' - ' . $loop['SubjectName'] . ', ' . $invoiceLoop['fee_item_description'];

                                        $cnDetailData = array(
                                            //'subregdetid' =>  $itemLoop['id'],
                                            'sp_invoice_id' => $invoiceLoop['invoice_main_id'],
                                            'sp_invoice_det_id' => $invoiceLoop['id'],
                                            'sp_amount' => $amount,
                                            'sp_balance' => $amount,
                                            'sp_receipt' => $amount,
                                            'sp_description' => $desc
                                        );
                                        $arrinvdet[] = $cnDetailData; //array push
                                    }
                                }
                            }
                        }
                    }
                }

                if (count($arrinvdet) > 0){
                    $invoiceclass = new icampus_Function_Studentfinance_Invoice();
                    $bill_no =  $invoiceclass->getBillSeq(11, date('Y'));

                    $data_dn = array(
                        'sp_fomulir_id' => $bill_no,
                        'sp_txn_id' => $info['transaction_id'],
                        'sp_IdStudentRegistration' => $info['IdStudentRegistration'],
                        //'sp_batch_id' => $btch_id,
                        'sp_amount' => $totalamount,
                        'sp_balance' => $totalamount,
                        'sp_type' => 3,
                        'sp_type_id' => 1,
                        'sp_invoice_date' => date('Y-m-d'),
                        'sp_description' => 'Financial Assistance',
                        'sp_currency_id' => $invmaininfo['currency_id'],
                        'sp_exchange_rate' => $invmaininfo['exchange_rate'],
                        'sp_creator'=>1,
                        'sp_create_date'=>date('Y-m-d H:i:s'),
                        'sp_status' => 'E',//entry
                    );
                    $spinvmainid = $this->scholarshipModel->saveSponsorInvMain($data_dn);

                    //todo generate receipt
                    $receipt_no = $this->getReceiptNoSeq();

                    $receiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();

                    $receipt_data = array(
                        'rcp_no' => $receipt_no,
                        'rcp_account_code' => 96,
                        'rcp_date' => date('Y-m-d H:i:s'),
                        'rcp_receive_date' => date('Y-m-d'),
                        'rcp_batch_id' => 0,
                        'rcp_type' => 3,
                        'rcp_type_id' => 1,
                        'rcp_description' => 'Fisabilillah - '.$info['SemesterMainName'].' - '.$loop['SubCode'] . ' - ' . $loop['SubjectName'],
                        'rcp_amount' => $totalamount,
                        'rcp_adv_amount' => 0,
                        'rcp_gainloss' => 0,
                        'rcp_gainloss_amt' => 0,
                        'rcp_cur_id' => $invmaininfo['currency_id'],
                        'rcp_exchange_rate' => $invmaininfo['exchange_rate'],
                        'p_payment_mode_id' => 20,
                        'p_cheque_no' => null,
                        'p_doc_bank' => null,
                        //'p_doc_branch' => isset($formData['payment_doc_branch'])?$formData['payment_doc_branch']:null,
                        'p_terminal_id' => null,
                        'p_card_no' => null,
                        'rcp_create_by'=>1
                    );
                    $receipt_id = $receiptDb->insert($receipt_data);

                    //todo update receipt id
                    $receiptData = array(
                        'src_receiptid'=>$receipt_id
                    );
                    $this->scholarshipModel->updateReApplicationCourse($receiptData, $loop['src_id']);

                    $data_rcp_stud = array(
                        'rcp_inv_rcp_id' => $receipt_id,
                        'IdStudentRegistration' => $info['IdStudentRegistration'],
                        'rcp_inv_amount' => $totalamount,
                        'rcp_adv_amount' => 0,
                        'rcp_inv_cur_id' => $invmaininfo['currency_id'],
                        'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                        'rcp_inv_create_by'=>1
                    );
                    $db->insert('sponsor_invoice_receipt_student',$data_rcp_stud);
                    $idStd = $db->lastInsertId();

                    $receiptInvoiceDb = new Studentfinance_Model_DbTable_SponsorReceiptInvoice();

                    foreach ($arrinvdet as $arrinvdetLoop){
                        $arrinvdetLoop['sp_id']=$spinvmainid;

                        $spinvdetailid = $this->scholarshipModel->saveSponsorInvDetail($arrinvdetLoop);

                        $data_rcp_inv = array(
                            'rcp_inv_rcp_id' => $receipt_id,
                            'rcp_inv_sp_id' => $spinvmainid,
                            'rcp_inv_student' => $idStd,
                            'rcp_inv_sponsor_dtl_id' => $spinvdetailid,
                            'rcp_inv_amount' => $arrinvdetLoop['sp_amount'],
                            'rcp_inv_amount_default' => $arrinvdetLoop['sp_amount'],
                            'rcp_inv_cur_id' => $invmaininfo['currency_id'],
                            'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                            'rcp_inv_create_by'=>1
                        );
                        $receiptInvoiceDb->insert($data_rcp_inv);
                    }

                    //approve invoice
                    $this->approveSponsorInvoive($receipt_id);
                }
            }
        }
        exit;
    }

    public function getReceiptNoSeq(){

        $seq_data = array(
            12,
            date('Y'),
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS receipt_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['receipt_no'];
    }

    public function approveSponsorInvoive($id){
        //receipt
        $sponsorreceiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();
        $receipt = $sponsorreceiptDb->getData($id);

        $receipt = $receipt[0];

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        /*
         * knockoff invoice
         */

        if ($receipt['rcp_status'] == 'ENTRY') {
            $sponsorinvoiceMainDb = new Studentfinance_Model_DbTable_SponsorInvoice();
            $sponsorinvoiceDetailDb = new Studentfinance_Model_DbTable_SponsorInvoiceDetail();
            $invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();


            foreach ($receipt['receipt_student'] as $rcpStudent) {

                $totalSponsorAmount = $rcpStudent['rcp_inv_amount'];

                $appTrxId = $rcpStudent['IdStudentRegistration'];
                $txnStudentDb = new Registration_Model_DbTable_Studentregistration();
                $txnProfile = $txnStudentDb->getData($appTrxId);

                //get current semester
                $semesterDb = new Records_Model_DbTable_SemesterMaster();
                $semesterInfo = $semesterDb->getStudentCurrentSemester($txnProfile);

                $totalAmount = 0.00;
                $total_payment_amount = $receipt['rcp_amount'];
                $total_paid_receipt = 0;
                $paidReceiptInvoice = 0;
                $paidReceipt = 0;

                if ($rcpStudent['receipt_invoice']) {
                    //get receipt invoice
                    $balanceNew1 = 0;
                    $paidNew1 = 0;
                    $balanceNew = 0;
                    $paidNew = 0;
                    $balance = 0;
                    $amountDN = 0;
                    foreach ($rcpStudent['receipt_invoice'] as $inv) {
                        $invID = $inv['rcp_inv_sp_id'];//sponsor_invoice_main
                        $invDetID = $inv['rcp_inv_sponsor_dtl_id'];//sponsor_invoice_detail
                        $rcpCurrency = $receipt['rcp_cur_id'];

                        $invDetData = $sponsorinvoiceDetailDb->getData($invDetID);

                        $invCurrency = $invDetData['currency_id'];

                        $currencyDb = new Studentfinance_Model_DbTable_Currency();
                        $currency = $currencyDb->fetchRow('cur_id = ' . $invCurrency)->toArray();

                        $amountDefault = $invDetData['sp_balance_det'];

                        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                        $paidReceipt = $inv['rcp_inv_amount'];

                        if ($invCurrency == 2) {
                            //var_dump($inv); exit;
                            //$dataCur = $curRateDB->getData($inv['rcp_exchange_rate']);
                            //$paidReceipt = round($inv['rcp_inv_amount'] * $dataCur['cr_exchange_rate'], 2);
                            $paidReceipt = round($inv['rcp_inv_amount'],2);
                        }

                        $balance = $amountDefault - $paidReceipt;
                        $totalSponsorAmount -= $paidReceipt;

                        //update invoice details
                        $dataInvDetail = array(
                            'sp_paid' => $paidReceipt + $invDetData['sp_paid_det'],
                            'sp_balance' => $balance,
                        );

                        $sponsorinvoiceDetailDb->update($dataInvDetail, array('id =?' => $invDetID));

                        $totalAmount += $paidReceiptInvoice;

                        //update invoice main
                        $invMainData = $sponsorinvoiceMainDb->getData($invID);

                        $balanceMain = ($invMainData['sp_balance']) - ($paidReceipt);
                        $paidMain = ($invMainData['sp_paid']) + ($paidReceipt);

                        //update invoice main
                        $dataInvMain = array(
                            'sp_paid' => $paidMain,
                            'sp_balance' => $balanceMain,
                            'sp_approve_by' => $getUserIdentity->id,
                            'sp_approve_date' => date('Y-m-d H:i:s')
                        );

                        $sponsorinvoiceMainDb->update($dataInvMain, array('sp_id =?' => $invID));
                        $invDetMainID = $inv['idDet'];
                        $invMainId = $inv['idMain'];

                        //update invoice_main & invoice_detail
                        $invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
                        $invDet = $invoiceDetailDB->getData($invDetMainID);

                        $amountDN = $inv['rcp_inv_amount'];
                        if ($invDet['cur_id'] == 2) {
                            //$dataCur = $curRateDB->getData($inv['rcp_exchange_rate']);
                            //$amountDN = round($inv['rcp_inv_amount'] * $dataCur['cr_exchange_rate'], 2);
                            $amountDN = round($inv['rcp_inv_amount'],2);
                        }

                        $balanceNew1 = $invDet['balance'] - $amountDN;
                        $paidNew1 = $invDet['paid'] + $amountDN;
                        $dataUpdDetail = array('balance' => $balanceNew1, 'sp_id' => $invDetID, 'paid' => $paidNew1);
                        $db->update('invoice_detail', $dataUpdDetail, $db->quoteInto("id = ?", $invDetMainID));

                        //update amount sponsor at invoice_main

                        $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                        $invMain = $invoiceMainDB->getData($invMainId);

                        $balanceNew = $invMain['bill_balance'] - $amountDN;
                        $paidNew = $invMain['bill_paid'] + $amountDN;

                        $dataUpdMain = array('bill_balance' => $balanceNew, 'bill_paid' => $paidNew, 'upd_by' => $creator, 'upd_date' => date('Y-m-d H:i:s'));
                        $db->update('invoice_main', $dataUpdMain, $db->quoteInto("id = ?", $invMainId));

                    }
                }

                if ($totalSponsorAmount > 0 || $rcpStudent['rcp_adv_amount'] != 0) {

                    //generetae advance payment
                    if ($rcpStudent['rcp_adv_amount'] > 0) {
                        $total_advance_payment_amount = $rcpStudent['rcp_adv_amount'];
                    } else {
                        $total_advance_payment_amount = $totalSponsorAmount;
                    }

                    $advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
                    $advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();

                    $bill_no = $this->getBillSeq(8, date('Y'));

                    $advance_payment_data = array(
                        'advpy_fomulir' => $bill_no,
                        'advpy_appl_id' => 0,
                        'advpy_trans_id' => 0,
                        'advpy_idStudentRegistration' => $rcpStudent['IdStudentRegistration'],
                        'advpy_sem_id' => $semesterInfo['IdSemesterMaster'],
                        'advpy_rcp_id' => $receipt['rcp_id'],
                        'advpy_description' => 'Advance payment from receipt no:' . $receipt['rcp_no'],
                        'advpy_cur_id' => $receipt['rcp_cur_id'],
                        'advpy_amount' => $total_advance_payment_amount,
                        'advpy_total_paid' => 0.00,
                        'advpy_total_balance' => $total_advance_payment_amount,
                        'advpy_status' => 'A',
                        'advpy_date' => date('Y-m-d'),

                    );


                    $advPayID = $advancePaymentDb->insert($advance_payment_data);

                    $advance_payment_det_data = array(
                        'advpydet_advpy_id' => $advPayID,
                        'advpydet_total_paid' => 0.00
                    );

                    $advancePaymentDetailDb->insert($advance_payment_det_data);

                    $dataAdv = array('rcp_adv_amount' => $total_advance_payment_amount);
                    $db->update('sponsor_invoice_receipt_student', $dataAdv, $db->quoteInto("rcp_inv_id = ?", $rcpStudent['rcp_inv_id']));

                }
            }

        }

        /*knockoff end*/


        //change receipt status to APPROVE
        if ($receipt['rcp_status'] == 'ENTRY') {

            foreach ($receipt['receipt_student'] as $rcpStudent) {
                if ($rcpStudent['receipt_invoice']) {
                    //get receipt invoice
                    foreach ($rcpStudent['receipt_invoice'] as $inv) {
                        $invID = $inv['idMain'];
                        $invDetMainID = $inv['idDet'];

                        if ($invID) {
                            if ($balanceNew == 0) {
                                $paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
                                $paymentClass->updateCourseStatus($invID);

                            }
                        }
                    }
                }
            }

            $sponsorreceiptDb->update(
                array(
                    'rcp_status' => 'APPROVE',
                    'UpdDate' => date('Y-m-d H:i:s'),
                    'UpdUser' => $getUserIdentity->id
                ), 'rcp_id = ' . $receipt['rcp_id']);

            return array('type' => 'success', 'message' => 'Receipt Approved');
        } else {
            return array('type' => 'error', 'message' => 'Receipt Already Approved');
        }
    }

    private function getBillSeq($type, $year)
    {

        $seq_data = array(
            $type,
            $year,
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['proforma_invoice_no'];
    }

    public function updateApplicantLandscapeAction(){
        $list = $this->model->getApplicant();

        if ($list){
            $i = 0;
            foreach ($list as $loop){
                $landscape = $this->model->getAppLandscapeInfo($loop['ap_prog_id'], $loop['ap_prog_scheme'], $loop['at_intake']);

                if ($landscape){
                    $landscapeid = $landscape['IdLandscape'];

                    $data = array(
                        'IdLandscape'=>$landscapeid
                    );
                    $this->model->updateTrans($loop['at_trans_id'], $data);
                }
            }
        }
        var_dump($i);
        exit;
    }

    /*
     *
     */
    public function updateRegItemPaymentStatusAction(){
        //set unlimited
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        echo 'lulululu';
        $list = $this->model->getSubjectItem();
        //var_dump($list); exit;
        if ($list){
            foreach ($list as $loop) {
                if (isset($loop['invoice_id']) && $loop['invoice_id'] != null){
                    $invmain = $this->model->getInvoiceMain($loop['invoice_id']);
                    /*}else{
                        $otherregitem = $this->model->getSubjectItem2($loop['regsub_id']);

                        if ($otherregitem && isset($otherregitem['invoice_id']) && $otherregitem['invoice_id']!=null){
                            $invmain = $this->model->getInvoiceMain($otherregitem['invoice_id']);
                            $loop['item_id']==$otherregitem['item_id'];
                        }else{
                            $invmain = false;
                        }
                    }*/

                    $subjectInfo = $this->model->getSubjectMain($loop['regsub_id']);
                    $studentInfo = $this->model->getStudentInfo2($loop['student_id']);

                    if ($subjectInfo['Active'] != 3) { //skip for widthdreaw subject
                        if ($invmain) { //normal invoice
                            $invdtl = $this->model->getInvoiceDetails($invmain['id'], $invmain['fs_id'], $loop['item_id']);

                            if ($invdtl) {
                                if ($invdtl['balance'] <= 0.00) {
                                    if ($subjectInfo['Active'] == 9 || $subjectInfo['Active'] == 4) {
                                        $data = array(
                                            'status' => 4
                                        );
                                        $this->model->updateItem($data, $loop['id']);
                                    } else if ($subjectInfo['Active'] == 10 || $subjectInfo['Active'] == 6) {
                                        $data = array(
                                            'status' => 6
                                        );
                                        $this->model->updateItem($data, $loop['id']);
                                    } else if ($subjectInfo['Active'] == 0 || $subjectInfo['Active'] == 1) {
                                        $data = array(
                                            'status' => 1
                                        );
                                        $this->model->updateItem($data, $loop['id']);
                                    }
                                } else {
                                    if ($subjectInfo['Active'] == 9 || $subjectInfo['Active'] == 4) {
                                        $data = array(
                                            'status' => 9
                                        );
                                        $this->model->updateItem($data, $loop['id']);
                                    } else if ($subjectInfo['Active'] == 10 || $subjectInfo['Active'] == 6) {
                                        $data = array(
                                            'status' => 10
                                        );
                                        $this->model->updateItem($data, $loop['id']);
                                    } else if ($subjectInfo['Active'] == 0 || $subjectInfo['Active'] == 1) {
                                        $data = array(
                                            'status' => 0
                                        );
                                        $this->model->updateItem($data, $loop['id']);
                                    }
                                }
                            } else { //initial invoice
                                $invoiceClass = new icampus_Function_Studentfinance_Invoice();

                                if ($studentInfo['transaction_id'] != null && $studentInfo['transaction_id'] != 0 && $subjectInfo['IdSubject'] != null && $subjectInfo['IdSubject'] != 0) {
                                    $statusInvoice = $invoiceClass->checkingInitialFee($studentInfo['transaction_id'], $subjectInfo['IdSubject']);

                                    if ($statusInvoice['Active'] == 0) {
                                        $data = array(
                                            'status' => 0
                                        );
                                        $this->model->updateItem($data, $loop['id']);
                                    } else if ($statusInvoice['Active'] == 1) {
                                        $data = array(
                                            'status' => 1
                                        );
                                        $this->model->updateItem($data, $loop['id']);
                                    }
                                }
                            }
                        }
                    }
                }else{
                    $data = array(
                        'status' => 0
                    );
                    $this->model->updateItem($data, $loop['id']);
                }
            }
        }
        exit;
    }

    public function migrateCeAction(){

        $list = $this->model->getCeMigrate();

        if ($list){
            foreach ($list as $loop){
                $year = date('Y', strtotime($loop['semester']));
                $month = date('M', strtotime($loop['semester']));

                $sem = $this->model->getCeSemester($year, $month, $loop['IdScheme']);
                
                $data = array(
                    'IdStudentRegistration'=>$loop['IdStudentRegistration'],
                    'initial'=>0,
                    'IdSubject'=>126,
                    'IdSemesterMain'=>$sem['IdSemesterMaster'],
                    'UpdUser'=>1,
                    'UpdDate'=>date('Y-m-d H:i:s'),
                    'UpdRole'=>'admin',
                    'Active'=>1,
                    'exam_status'=>null,
                    'grade_name'=>trim($loop['grade']),
                    'grade_desc'=>trim($loop['grade'])=='P' ? 'Pass':'Fail',
                    'grade_status'=>trim($loop['grade'])=='P' ? 'Pass':'Fail',
                    'mark_approval_status'=>2,
                    'mark_approveby'=>1,
                    'mark_approvedt'=>date('Y-m-d H:i:s'),
                    'migrate_date'=>date('Y-m-d H:i:s'),
                    'migrate_by'=>589
                );
                $this->model->insertCe($data);
            }
        }
        exit;
    }

    public function testAction(){


        $model = new icampus_Function_Studentfinance_Invoice();
        $landscapeNewDB = new App_Model_General_DbTable_Landscape();

        $semester = $landscapeNewDB->getSemesterInfo(2016, 'JUN');

        $landscapeSubjectDb = new GeneralSetup_Model_DbTable_Landscapesubject();
        $preregdata = $landscapeSubjectDb->getLandscapeSubjectTag(230);
        $subprereg = json_decode($preregdata['subjectdata'], true);
        $preregdatabytype = $model->getPreregData($subprereg);

        $semester_type = $semester['SemesterType'];

        $total_subject = $preregdatabytype[$semester_type]['count'];
        $fsitem['subject'] = isset($preregdatabytype[$semester_type] ['subject']) ? $preregdatabytype[$semester_type] ['subject'] : array();

        var_dump($preregdata);
        var_dump($subprereg);
        var_dump($preregdatabytype);
        var_dump($fsitem['subject']);

        exit;
    }

    public function updateLibraryApiAction(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectApi = $db->select()
            ->from(array('a'=>'library_api'), array('value'=>'*', 'api_id'=>'a.id'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.username = b.registrationId')
            ->join(array('c'=>'tbl_program'), 'b.IdProgram = c.IdProgram AND a.program_code = c.ProgramCode')
            ->where('a.action_status = ?', 'NEW')
            ->where('a.action = ?', 'ADD')
            ->where('a.action_type = ?', 'USER')
            ->order('a.username ASC');

        $apiList = $db->fetchAll($selectApi);

        if ($apiList){
            foreach ($apiList as $apiLoop){
                $invoiceClass = new icampus_Function_Studentfinance_Invoice();
                $invoiceAmount=$invoiceClass->getAmountResourceFee($apiLoop['IdStudentRegistration']);

                $data = array(
                    'program_resource_fee'=> $invoiceAmount['amount'],
                    'program_currency'=> $invoiceAmount['currency']!=null ? $invoiceAmount['currency']:''
                );
                $db->update('library_api', $data, 'id = '.$apiLoop['api_id']);
            }
        }

        exit;
    }

    public function insertExamItemAction(){
        $list = $this->model->getListToInsertExamItem();

        //var_dump($list); exit;

        if ($list){
            foreach ($list as $key => $loop){
                $detail = $this->model->getItemDetails($loop['IdStudentRegSubjects'], true);

                if (!$detail){
                    $data = array(
                        'regsub_id' => $loop['IdStudentRegSubjects'],
                        'student_id' => $loop['IdStudentRegistration'],
                        'semester_id' => $loop['IdSemesterMain'],
                        'subject_id' => $loop['IdSubject'],
                        'item_id' => 879,
                        'section_id' => $loop['IdCourseTaggingGroup'],
                        'ec_country' => 121,
                        'ec_city' => 1348,
                        'invoice_id' => null,
                        'created_date' => date('Y-m-d'),
                        'created_by' => 1,
                        'created_role' => 'admin',
                        'ses_id' => 0,
                        'status' => 0,
                        'refund' => null,
                        'modifyby' => null,
                        'modifydt' => null,
                    );
                    $this->model->insertItemExam($data);

                    $examCheck = $this->model->checkExamCenter($loop['IdStudentRegistration'], $loop['IdSubject']);

                    if (!$examCheck) {
                        $dataExam = array(
                            'er_idCountry' => 121,
                            'er_idCity' => 1348,
                            'er_idCityOthers' => '',
                            'er_ec_id' => 26,
                            'er_idSemester' => $loop['IdSemesterMain'],
                            'er_idStudentRegistration' => $loop['IdStudentRegistration'],
                            'er_idSubject' => $loop['IdSubject'],
                            'er_status' => 764,
                            'er_createdby' => 1,
                            'er_createddt' => date("Y-m-d H:i:s"),
                            'er_createdrole' => 'ADMIN'
                        );
                        $this->model->insertExamCenter($dataExam);
                    }
                }
            }
        }

        exit;
    }

    public function updateCourseRegistrationStatusAction(){
        $model = new Registration_Model_DbTable_SeekingForLandscape();
        $paModel = new Studentfinance_Model_DbTable_PaymentAmount();

        $list = $model->getCourseNotUpdate();

        if ($list){
            foreach ($list as $loop){
                $items = $model->getSubjectItem3($loop['IdStudentRegSubjects']);

                if (!empty($items)){
                    $totalBalance = 0.00;
                    $status = false;
                    foreach ($items as $item){
                        if (($item['invoice_id']==null || $item['invoice_id']==0) && $item['initial']==1){
                            $item2 = $model->getSubjectReg($item['student_id']);
                            $item['invoice_id'] = isset($item2['invoice_id']) ? $item2['invoice_id']:null;
                        }

                        if ($item['invoice_id']!=null && $item['invoice_id']!=0) {
                            $balance = $paModel->getBalanceMain($item['invoice_id']);
                            $totalBalance = $totalBalance + $balance['bill_balance'];
                            $status = true;
                        }
                    }

                    if ($totalBalance <= 0 && $status == true){
                        if ($loop['Active'] == 9) {
                            $status = 4;
                        } else if ($loop['Active'] == 10) {
                            $status = 6;
                        } else if ($loop['Active'] == 0) {
                            $status = 1;
                        }

                        $data = array(
                            'Active'=>$status
                        );

                        $model->updateCourseRegistration($data, $loop['IdStudentRegSubjects']);
                    }
                }
            }
        }
        exit;
    }
}