<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 1/12/2015
 * Time: 10:45 AM
 */
class Registration_Model_DbTable_ForecastReport extends Zend_Db_Table {

    public function getProgram(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->order('a.seq_no');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getProgramById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->where('a.IdProgram = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getProgramScheme($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('pt'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSemester($idscheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IsCountable = ?', 1)
            ->where('a.IdScheme = ?', $idscheme);

        $select = $select.' ORDER BY a.AcademicYear DESC, CASE a.sem_seq '
            . 'WHEN "JAN" THEN 1 '
            . 'WHEN "FEB" THEN 2 '
            . 'WHEN "MAR" THEN 3 '
            . 'WHEN "APR" THEN 4 '
            . 'WHEN "MAY" THEN 5 '
            . 'WHEN "JUN" THEN 6 '
            . 'WHEN "JUL" THEN 7 '
            . 'WHEN "AUG" THEN 8 '
            . 'WHEN "SEP" THEN 9 '
            . 'WHEN "OCT" THEN 10 '
            . 'WHEN "NOV" THEN 11 '
            . 'WHEN "DEC" THEN 12 '
            . 'ELSE 13 '
            . 'END DESC';

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSubjectFromLandscape($program_id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_landscapesubject'), array('value'=>'*'))
            ->joinLeft(array("b"=>"tbl_subjectmaster"),'b.IdSubject=a.IdSubject',array('BahasaIndonesia','b.SubCode','SubjectName'=>'b.SubjectName'))
            ->joinLeft(array("c"=>"tbl_definationms"),'c.idDefinition=a.SubjectType',array('c.DefinitionDesc'))
            ->joinLeft(array("d"=>"tbl_programrequirement"),'d.IdProgramReq=a.IdProgramReq',array())
            ->joinLeft(array('e'=>'tbl_landscapeblock'), 'e.idblock=a.IdLevel',array('e.block as Level'))
            ->where("a.IdProgram = ?", $program_id)
            ->where("a.IDProgramMajoring = 0")
            ->order("a.IdSemester")
            ->order("d.IdProgramReq")
            ->order("a.IdLandscapeSub")
            ->group('a.IdSubject');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getStudent($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'student_profile'), 'a.sp_id = b.id')
            ->joinLeft(array('c'=>'tbl_program'), 'a.IdProgram = c.IdProgram')
            ->joinLeft(array('d'=>'tbl_program_scheme'), 'a.IdProgramScheme = d.IdProgramScheme')
            ->joinLeft(array('e'=>'tbl_definationms'), 'd.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc'))
            ->joinLeft(array('f'=>'tbl_definationms'), 'd.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'd.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc'))
            ->joinLeft(array('h'=>'tbl_intake'), 'a.IdIntake = h.IdIntake')
            ->joinLeft(array('i'=>'tbl_countries'), 'b.appl_nationality = i.idCountry', array('i.CountryName'));

        if ($search != false){
            if (isset($search['program']) && $search['program'] != ''){
                $select->where('a.IdProgram = ?', $search['program']);
            }
            if (isset($search['programscheme']) && $search['programscheme'] != ''){
                $select->where('a.IdProgramScheme in (?)', $search['programscheme']);
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getStudentById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'student_profile'), 'a.sp_id = b.id')
            ->joinLeft(array('c'=>'tbl_program'), 'a.IdProgram = c.IdProgram')
            ->joinLeft(array('d'=>'tbl_program_scheme'), 'a.IdProgramScheme = d.IdProgramScheme')
            ->joinLeft(array('e'=>'tbl_definationms'), 'd.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc'))
            ->joinLeft(array('f'=>'tbl_definationms'), 'd.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'd.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc'))
            ->joinLeft(array('h'=>'tbl_intake'), 'a.IdIntake = h.IdIntake')
            ->joinLeft(array('i'=>'tbl_countries'), 'b.appl_nationality = i.idCountry', array('i.CountryName'))
            ->where('a.IdStudentRegistration = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    static function forecastSubject($studentid, $subjectid){
        $db = Zend_Db_Table::getDefaultAdapter();

        $selectStudent = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'student_profile'), 'a.sp_id = b.id')
            ->joinLeft(array('c'=>'tbl_program'), 'a.IdProgram = c.IdProgram')
            ->joinLeft(array('d'=>'tbl_program_scheme'), 'a.IdProgramScheme = d.IdProgramScheme')
            ->joinLeft(array('e'=>'tbl_definationms'), 'd.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc'))
            ->joinLeft(array('f'=>'tbl_definationms'), 'd.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'd.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc'))
            ->joinLeft(array('h'=>'tbl_intake'), 'a.IdIntake = h.IdIntake')
            ->joinLeft(array('i'=>'tbl_countries'), 'b.appl_nationality = i.idCountry', array('i.CountryName'))
            ->where('a.IdStudentRegistration = ?', $studentid);

        $studentInfo = $db->fetchRow($selectStudent);

        $requisiteDB = new Registration_Model_DbTable_SubjectPrerequisite();
        $requisites = $requisiteDB->getPrerequisite($studentInfo['IdLandscape'],$subjectid);

        $selectSubject = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->order('a.IdStudentRegSubjects DESC')
            ->where('a.Active != ?', 3)
            ->where('a.grade_name NOT IN (?)', '("U")')
            ->where('a.exam_status NOT IN (?)', '("U")')
            ->where('a.IdStudentRegistration = ?', $studentid)
            ->where('a.IdSubject = ?', $subjectid);

        $subject = $db->fetchRow($selectSubject);

        $result = array();
        if ($subject){ // dah ambil
            if ($subject['grade_name']=='F'){ //repeat
                $result = 1;
            }else{ //lulus
                $result = 2;
            }
        }else{ //belum ambil
            //kene cek pre-requisite
            if ($requisites){
                /*foreach ($requisites as $requisite){
                    $selectRequisite = $db->select()
                        ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
                        ->joinLeft(array('b'=>'tbl_semestermaster'), '');
                }*/
                $result = 0;
            }else{
                $result = 0;
            }
        }

        return $result;
    }

    public function getCourseRegister($semid, $studentid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->where('a.Active != ?', 3)
            ->where('a.exam_status NOT IN (?)', '("U")')
            ->where('a.IdSemesterMain = ?', $semid)
            ->where('a.IdStudentRegistration = ?', $studentid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getCoursePass($studentid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->where('a.Active != ?', 3)
            ->where('a.grade_name NOT IN (?)', '("F", "U")')
            ->where('a.exam_status NOT IN (?)', '("U")')
            ->where('a.IdStudentRegistration = ?', $studentid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getCurrentSemester($idScheme){
        $date = date('Y-m-d');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.SemesterMainStartDate <= ?', $date)
            ->where('a.SemesterMainEndDate >= ?', $date)
            ->where('a.IdScheme = ?', $idScheme);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function CurrentSemesterCourseRegistration($studentid, $programid){
        $date = date('Y-m-d');
        $db = Zend_Db_Table::getDefaultAdapter();

        $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
        $recordDb = new Records_Model_DbTable_Academicprogress();

        $studentLoop = $this->getStudentById($studentid);

        $result = array();
        if ($programid == 5) { //cifp
            $subjectRegPart1 = 0;
            $subjectRegPart2 = 0;
            $subjectRegPart3 = 0;

            $currentSem = $this->getCurrentSemester($studentLoop['IdScheme']);
            $currentSubRegs = $this->getCourseRegister($currentSem['IdSemesterMaster'], $studentid);

            if ($currentSubRegs){
                foreach ($currentSubRegs as $currentSubReg){
                    $equivid = false;

                    $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentLoop['IdProgram'], $studentLoop['IdLandscape'], $currentSubReg['IdSubject']);
                    if(!$course_status){
                        $equivid=$recordDb->checkEquivStatus($studentLoop['IdLandscape'], $currentSubReg["IdSubject"]);

                        if($equivid){
                            $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentLoop['IdProgram'], $studentLoop['IdLandscape'], $equivid);
                        }
                    }

                    switch ($course_status['block']){
                        case 1:
                            $subjectRegPart1++;
                            break;
                        case 2:
                            $subjectRegPart2++;
                            break;
                        case 3:
                            $subjectRegPart3++;
                            break;
                    }
                }
            }

            $result = array(
                'subjectRegPart1' => $subjectRegPart1,
                'subjectRegPart2' => $subjectRegPart2,
                'subjectRegPart3' => $subjectRegPart3,
            );
        }else{
            $subjectRegCore = 0;
            $subjectRegElective = 0;

            $currentSem = $this->getCurrentSemester($studentLoop['IdScheme']);
            $currentSubRegs = $this->getCourseRegister($currentSem['IdSemesterMaster'], $studentid);

            if ($currentSubRegs){
                foreach ($currentSubRegs as $currentSubReg){
                    $equivid = false;

                    $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentLoop['IdProgram'], $studentLoop['IdLandscape'], $currentSubReg['IdSubject']);
                    if(!$course_status){
                        $equivid=$recordDb->checkEquivStatus($studentLoop['IdLandscape'], $currentSubReg["IdSubject"]);

                        if($equivid){
                            $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentLoop['IdProgram'], $studentLoop['IdLandscape'], $equivid);
                        }
                    }

                    switch ($course_status['DefinitionDesc']){
                        case 'Core':
                            $subjectRegCore++;
                            break;
                        case 'Elective':
                            $subjectRegElective++;
                            break;
                    }
                }
            }

            $result = array(
                'subjectRegCore' => $subjectRegCore,
                'subjectRegElective' => $subjectRegElective,
            );
        }

        return $result;
    }

    public function totalPassCurrent($studentid, $programid){
        $date = date('Y-m-d');
        $db = Zend_Db_Table::getDefaultAdapter();

        $landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
        $recordDb = new Records_Model_DbTable_Academicprogress();

        $studentLoop = $this->getStudentById($studentid);

        $result = array();
        if ($programid == 5) { //cifp
            $subjectPassPart1 = 0;
            $subjectPassPart2 = 0;
            $subjectPassPart3 = 0;

            $currentSubPass = $this->getCoursePass($studentid);

            if ($currentSubPass){
                foreach ($currentSubPass as $currentSubPassLoop){
                    $equivid = false;

                    $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentLoop['IdProgram'], $studentLoop['IdLandscape'], $currentSubPassLoop['IdSubject']);
                    if(!$course_status){
                        $equivid=$recordDb->checkEquivStatus($studentLoop['IdLandscape'], $currentSubPassLoop["IdSubject"]);

                        if($equivid){
                            $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentLoop['IdProgram'], $studentLoop['IdLandscape'], $equivid);
                        }
                    }

                    switch ($course_status['block']){
                        case 1:
                            $subjectPassPart1++;
                            break;
                        case 2:
                            $subjectPassPart2++;
                            break;
                        case 3:
                            $subjectPassPart3++;
                            break;
                    }
                }
            }

            $result = array(
                'subjectPassPart1' => $subjectPassPart1,
                'subjectPassPart2' => $subjectPassPart2,
                'subjectPassPart3' => $subjectPassPart3,
            );
        }else{
            $subjectPassCore = 0;
            $subjectPassElective = 0;

            $currentSubPass = $this->getCoursePass($studentid);

            if ($currentSubPass){
                foreach ($currentSubPass as $currentSubPassLoop){
                    $equivid = false;

                    $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentLoop['IdProgram'], $studentLoop['IdLandscape'], $currentSubPassLoop['IdSubject']);
                    if(!$course_status){
                        $equivid=$recordDb->checkEquivStatus($studentLoop['IdLandscape'], $currentSubPassLoop["IdSubject"]);

                        if($equivid){
                            $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentLoop['IdProgram'], $studentLoop['IdLandscape'], $equivid);
                        }
                    }

                    switch ($course_status['DefinitionDesc']){
                        case 'Core':
                            $subjectPassCore++;
                            break;
                        case 'Elective':
                            $subjectPassElective++;
                            break;
                    }
                }
            }

            $result = array(
                'subjectPassCore' => $subjectPassCore,
                'subjectPassElective' => $subjectPassElective,
            );
        }

        return $result;
    }
}