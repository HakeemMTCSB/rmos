<?php 

class Registration_Model_DbTable_StudentRegistrationItem extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_studentregsubjects_detail';
	protected $_primary = "id";

			
	public function addData($data){		
	
	   $this->insert($data);
	   $db = Zend_Db_Table::getDefaultAdapter();
	   $lastId = $db->lastInsertId();
	   return $lastId;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getTotalRegisterItem($IdStudentRegistration,$idSubject,$idSemester,$withdraw_exception=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array())	 
	 				 ->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id=srs.IdStudentRegSubjects')				
	 				 ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester);
	 				 
	 				 if($withdraw_exception==1){
	 				 	$select->where('srsd.status != ?',3);
	 				 }
	 				
	 	$row = $db->fetchAll($select);
	 	
	 	if($row){
	 		return count($row);
	 	}else{
	 		return 0;
	 	}
				 
	}
	
	public function getItemRegister($IdStudentRegistration,$idSubject,$idSemester,$withdraw_exception=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		 $select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array())	 
	 				 ->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id=srs.IdStudentRegSubjects')
	 				 ->join(array('d'=>'tbl_definationms'),'d.idDefinition=srsd.item_id',array('item_name'=>'DefinitionDesc'))				
	 				 ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srsd.semester_id = ?',$idSemester);
	 				
					 if($withdraw_exception==1){
	 				 	$select->where('srsd.status != ?',3);
	 				 }
	 				 
	 	$row = $db->fetchAll($select);
	 	
	 	return $row;
				 
	}
	
	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srsd'=>'tbl_studentregsubjects_detail'))	 
	 				 ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegSubjects = srsd.regsub_id',array('initial_paper'=>'initial'))
	 				 ->where('srsd.id = ?',$id);
	 				
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
				 
	}
	
	public function addHistoryItemData($data){		
	   $db = Zend_Db_Table::getDefaultAdapter();
	   $db->insert('tbl_studentregsubjects_detail_history',$data);	   
	}
	
	public function getItemRegisterById($IdStudentRegSubjects){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()
	 				 ->from(array('srsd'=>'tbl_studentregsubjects_detail'))
	 				 ->join(array('d'=>'tbl_definationms'),'d.idDefinition=srsd.item_id',array('item_name'=>'DefinitionDesc'))				
	 				 ->where('srsd.regsub_id = ?',$IdStudentRegSubjects);
	 				 
	 	$row = $db->fetchAll($select);
	 	
	 	return $row;
				 
	}
	
	
	public function getItemWithdrawHistory($IdStudentRegSubjects){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()
	 				 ->from(array('srsd'=>'tbl_studentregsubjects_detail'))
	 				 ->join(array('srsh'=>'tbl_studentregsubjects_history'),'srsh.IdStudentRegistration=srsd.student_id AND srsh.IdSubject=srsd.subject_id AND srsd.semester_id=srsh.IdSemesterMain',array('history_createdby'=>'UpdRole'))
	 				 ->join(array('d'=>'tbl_definationms'),'d.idDefinition=srsd.item_id',array('item_name'=>'DefinitionDesc'))				
	 				 ->where('srsd.regsub_id = ?',$IdStudentRegSubjects)
	 				 ->group('srsd.id');
	 				 
	 	$row = $db->fetchAll($select);
	 	
	 	return $row;
				 
	}
	
	public function  displayRefundDetails($item_list){
		
		//echo '<pre>';
		//print_r($item_list);
		
			if(isset($item_list) && count($item_list)>0){								
				
				$creditNoteDB = new Studentfinance_Model_DbTable_CreditNote();
				
				echo '<fieldset><legend>Refund Details</legend><ul>';
				foreach($item_list as $item){
				
					if(isset($item['history_createdby']) && $item['history_createdby']=='student'){
						//22-6-2015
						//check invoice...buat ni bukan ape sebab bila subject di drop dari student portal xde record refund disimpan
						//so just check jika CN tu wujud maka just display ada refund						
						if($item['invoice_id']){
							
							if($item['refund']!=''){
								$refund = 	$item['refund'].'%';
							}else{
								$cn_status = $creditNoteDB->getDataByInvoice($item['invoice_id'],null,0);											
								if($cn_status){
									if($cn_status['type_amount']!=''){
										$refund = 	$cn_status['type_amount'].'%';
									}else{
										$refund = 'With Refund';
									}
								}else{
									$refund = 'No Refund';
								}
							}
							
						}else{
							$refund = 'N/A';
						}											
						
					}else{
						if($item['refund']){
							$refund = 	$item['refund'].'%';
						}else{
							$refund = 'No Refund';
						}
					}
					
					
					echo '<li>'.$item['item_name'].' ( '.$refund.' )</li>';
				}
				echo '</ul></fieldset>';
			}
	}
}

?>