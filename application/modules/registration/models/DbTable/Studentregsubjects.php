<?php 

class Registration_Model_DbTable_Studentregsubjects extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_studentregsubjects';
	protected $_primary = "IdStudentRegSubjects";

	public function getData($IdStudentRegSubjects){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
		 $select =$db->select()
	 				 ->from(array('srs'=>$this->_name))
	 				 ->where('srs.IdStudentRegSubjects = ?',$IdStudentRegSubjects);
	 				 $row = $db->fetchRow($select);
	 				 return $row;
	}	
		
	public function addData($data){		
	
	   $this->insert($data);
	   $db = Zend_Db_Table::getDefaultAdapter();
	   $lastId = $db->lastInsertId();
	   return $lastId;
	}
	
	public function updateData($data,$id){		
		 $this->update($data, $this->_primary .' =' . (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .'=' . (int)$id);
	}
	
	public function updateGroupData($data,$id){
		 $this->update($data, 'IdStudentRegSubjects = '. (int)$id);
	}
	
	public function getTotalRegister($idSubject=null,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('srs'=>$this->_name))
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain IN (?)',$idSemester)
	 				 ->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")')
	 				 ->where('srs.Active != 3');
	 	$row = $db->fetchAll($select);
	 	if($row){
	 		return count($row);	
	 	}else{
	 		return 0;
	 	}
				 
	}
	
	public function getStudents($idSubject=null,$idSemester,$student_per_group){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		 $select =$db->select()
	 				 ->from(array('sr'=>'tbl_studentregistration'))
	 				 ->joinLeft(array('srs'=>$this->_name),'srs.IdStudentRegistration=sr.IdStudentRegistration')
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->where('srs.IdCourseTaggingGroup = 0')
	 				 ->order("registrationId")
	 				 ->limit($student_per_group,0);
	 	$row = $db->fetchAll($select);
	 	
	 	
	 	return $row;
				 
	}
	
	public function getUnTagGroupStudents($idSubject,$idSemester,$semArr,$limit=""){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('sr'=>'tbl_studentregistration'))
	 				 ->joinLeft(array('srs'=>$this->_name),'srs.IdStudentRegistration=sr.IdStudentRegistration')
	 				 ->joinLeft(array('ap'=>'student_profile'),'ap.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
	 				 ->joinLeft(array('d'=>'tbl_program'), 'sr.IdProgram = d.IdProgram')
		            ->joinLeft(array('e'=>'tbl_program_scheme'), 'sr.IdProgramScheme = e.IdProgramScheme')
		            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
		            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
		            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
		            ->where('sr.profileStatus = ?',92)
                            ->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")')
                            ->where('srs.Active <> ?', 3)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain IN (?)',$semArr)
	 				 ->where('srs.IdCourseTaggingGroup = 0')//0:No Group
	 				 ->order("registrationId");
	 	if(isset($limit) && $limit!=""){
	 		$select->limit($limit);
	 	}
	 
	 	$row = $db->fetchAll($select);
	 	return $row;
				 
	}
	
	public function getTaggedGroupStudents($idGroup){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('sr'=>'tbl_studentregistration'))
	 				 ->joinLeft(array('srs'=>$this->_name),'srs.IdStudentRegistration=sr.IdStudentRegistration')
	 				 ->joinLeft(array('ap'=>'student_profile'),'ap.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))	 				 
	 				 ->joinLeft(array('d'=>'tbl_program'), 'sr.IdProgram = d.IdProgram')
		            ->joinLeft(array('e'=>'tbl_program_scheme'), 'sr.IdProgramScheme = e.IdProgramScheme')
		            ->joinLeft(array('f'=>'tbl_definationms'), 'e.mode_of_program = f.idDefinition', array('mop'=>'f.DefinitionDesc'))
		            ->joinLeft(array('g'=>'tbl_definationms'), 'e.mode_of_study = g.idDefinition', array('mos'=>'g.DefinitionDesc'))
		            ->joinLeft(array('h'=>'tbl_definationms'), 'e.program_type = h.idDefinition', array('pt'=>'h.DefinitionDesc'))
                            ->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")')
	 				 ->where('srs.IdCourseTaggingGroup = ?',$idGroup)//0:No Group
					->where('srs.Active <> ?', 3)
	 				 ->order("registrationId");
	 				
	 	$row = $db->fetchAll($select);
	 	return $row;
				 
	}
	
	
	public function getTotalAssigned($idSubject=null,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('srs'=>$this->_name))
	 				 ->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")')
	 				 ->where('srs.Active <> ?', 3)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain IN (?)',$idSemester)
	 				 ->where('IdCourseTaggingGroup != 0');
	 	$row = $db->fetchAll($select);
	 	
	 	if($row){
	 		return count($row);	
	 	}else{
	 		return null;
	 	}
				 
	}
	
	public function getTotalUnAssigned($idSubject=null,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('srs'=>$this->_name))
	 				 ->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")')
	 				 ->where('srs.Active <> ?', 3)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain IN (?)',$idSemester)
	 				 ->where('IdCourseTaggingGroup = 0');
	 	$row = $db->fetchAll($select);
	 	
	 	if($row){
	 		return count($row);	
	 	}else{
	 		return null;
	 	}
				 
	}
	
	
	public function updateRegistrationData($data,$id){
		 $this->update($data, 'IdStudentRegistration = '. $id);
	}
	
	public function deleteRegisterSubjects($id){		
	  $this->delete('IdStudentRegistration =' . (int)$id);
	}
	
	public function getRegStudents($idSubject=null,$idSemester,$idCollege="",$post=null,$active=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		 $select =$db->select()
		 			->distinct()
	 				 ->from(array('sr'=>'tbl_studentregistration'),array("registrationId"))	 				 
	 				 ->join(array('tp'=>'tbl_program'),'sr.IdProgram=tp.IdProgram',array())
	 			//	 ->join(array('tc'=>'tbl_collegemaster'),'tp.idCollege=tc.idCollege',array("collegename","idCollege"))
	 				 ->join(array('sp'=>"student_profile"),"sp.id=sr.sp_id",array("appl_fname","appl_mname","appl_lname",'appl_username'))
	 				 ->join(array('srs'=>$this->_name),'srs.IdStudentRegistration=sr.IdStudentRegistration')
	 			//	 ->joinLeft(array('at'=>'applicant_transaction'),'at.at_trans_id=sr.transaction_id',array('at_pes_id'))
	 			//	 ->joinLeft(array('lk'=>'sis_setup_detl'),"lk.ssd_id=sp.appl_religion",array("religion"=>"ssd_name"))
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->order("registrationId");
	 			
	 	if($active=="yes"){
	 		$select->where("srs.Active != ?",3);
	 	}			 
	
	    if (isset($post['applicant_name']) && !empty($post['applicant_name'])) {
         
            $select->where("(sp.appl_fname LIKE '%". $post['applicant_name']."%'");
            $select->orwhere("sp.appl_mname LIKE '%". $post['applicant_name']."%'");
            $select->orwhere("sp.appl_lname LIKE '%". $post['applicant_name']."%')");
        }
        
        if (isset($post['s_student_id']) && !empty($post['s_student_id'])) { 
        	if (isset($post['e_student_id']) && !empty($post['e_student_id'])) {           
            	$select->where("sr.registrationId >= '". $post['s_student_id']."' AND sr.registrationId <= '". $post['e_student_id']."'");
        	}else{
        		$select->where("sr.registrationId >= '". $post['s_student_id']."' AND sr.registrationId <= '". $post['s_student_id']."'");
        	}
        }
	
	    if (isset($post['IdProgram']) && !empty($post['IdProgram'])) {           
            $select->where("sr.IdProgram = ?", $post['IdProgram']);
        }    
		
        $row = $db->fetchAll($select);
	 	
	 	
	 	return $row;
				 
	}
	
	public function dropRegisterSubjects($id){	
		
		$auth = Zend_Auth::getInstance();
		
		$reginfo = $this->getSubjectRegRaw($id);
		$studentregDB = new Registration_Model_DbTable_Studentregistration();
		$studInfo = $studentregDB->getInfo($reginfo["IdStudentRegistration"]);
		
		$authorized=true;
		if($auth->getIdentity()->IdRole=="445" || $auth->getIdentity()->IdRole=="3" ){
			if($studInfo["AcademicAdvisor"]!=$auth->getIdentity()->IdStaff){
				$authorized = false;
			}
		}
		$ssemDB = new Registration_Model_DbTable_Studentsemesterstatus();
		$issemopen = $ssemDB->checkSemesterCourseRegistration($reginfo["IdSemesterMain"],$studInfo["IdProgram"]);
		
		if($authorized){
			if(is_array($issemopen)){
				$this->saveHistory($reginfo,"DROP");
				$this->delete('IdStudentRegSubjects =' . (int)$id);
				//echo "masuk";
				$returnVal["ERROR"]= null;
			}else{
				$returnVal["ERROR"]="POLICY ERROR : Subject can be only drop during Add&Drop Period";
				echo $returnVal["ERROR"]; 
			}
		}else{			
			$returnVal["ERROR"]= "AUTHORIZATION ERROR"; 
		}
	  	
		return $returnVal;
	}	
	
	public function saveHistory($data,$activity){
		$auth = Zend_Auth::getInstance();
		$db = Zend_Db_Table::getDefaultAdapter();
		$data["hdate"]=date("Y-m-d H:i:s");
		$data["huser"]=$auth->getIdentity()->id;
		$data["hactivity"]=$activity;		
		$db->insert("studentregsubjects_history",$data);
	}
	
	public function getSubjectInfo($idStudentRegistration,$idSubject,$idSemester=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester);
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}
	
	public function getSubjectRegistrationInfo($idStudentRegistration,$idSemester=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->group("srs.IdStudentRegistration");
	 				 
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}
	
	public function getRegisteredSubject($idStudentRegistration,$semester_id){
	
	  $db = Zend_Db_Table::getDefaultAdapter();
	  	
	  $select =$db->select()
	  ->from(array('srs'=>'tbl_studentregsubjects'))
	  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = srs.IdSubject')
	  ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	  ->where('srs.IdSemesterMain = ?',$semester_id)
	  ->where('srs.Active != 3');
	  	
	  $row = $db->fetchAll($select);
	   
	  return $row;
	
	}
	
	
	public function isRegistered($idStudentRegistration,$idSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.Active != 3')
	 				 ->order('IdStudentRegSubjects DESC');
	 				
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}
	
	public function getSubjectRegInfo($IdStudentRegSubjects){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	
	 				 ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration') 				
	 				 ->where('srs.IdStudentRegSubjects = ?',$IdStudentRegSubjects);
	 				
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}
	
	public function deleteParentSubjects($idStudentRegistration,$idSubject){	
	
	  $this->delete('IdStudentRegistration ='.$idStudentRegistration.' AND IdSubject ='.$idSubject);
	}
	
	public function getSubjectRegRaw($IdStudentRegSubjects){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	
	 				 ->where('srs.IdStudentRegSubjects = ?',$IdStudentRegSubjects);
	 				
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}	
	
	
	public function getSumCreditHour($idStudentRegistration,$semester_id){
	
	  $db = Zend_Db_Table::getDefaultAdapter();
	  	
	  $select =$db->select()
	  			  ->from(array('srs'=>'tbl_studentregsubjects'),array("TotalCH"=>"SUM(CreditHours)"))
				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = srs.IdSubject',array())
				  ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
				  ->where('srs.IdSemesterMain = ?',$semester_id)
				  ->where('srs.subjectlandscapetype != 2')
				  ->where('(srs.Active = 1 OR srs.Active = 4 OR srs.Active = 5 )');
				  	
	  $row = $db->fetchRow($select);
	   
	  return $row;
	
	}
	
	
	public function searchRegisterStudent($post=null){
		
		    $db = Zend_Db_Table::getDefaultAdapter();
			
		    $select =$db->select()
		  			->distinct()
	 				 ->from(array('sr'=>'tbl_studentregistration'),array("registrationId"))	 	
	 				 ->join(array('srs'=>$this->_name),'srs.IdStudentRegistration=sr.IdStudentRegistration')			 
	 				 ->join(array('tp'=>'tbl_program'),'sr.IdProgram=tp.IdProgram',array())	 			
	 				 ->join(array('sp'=>"student_profile"),"sp.id=sr.sp_id",array("appl_fname","appl_mname","appl_lname"))	
	 				 ->order("registrationId");
	 			

		    if (isset($post['IdSemester']) && !empty($post['IdSemester'])) {         
	             $select->where("srs.IdSemesterMain = ?", $post['IdSemester']);
	        }
	        
			if (isset($post['IdProgram']) && !empty($post['IdProgram'])) {         
	             $select->where("sr.IdProgram = ?", $post['IdProgram']);
	        } 
		  
			if (isset($post['IdSubject']) && !empty($post['IdSubject'])) {         
	             $select->where("srs.IdSubject = ?", $post['IdSubject']);
	        } 
	        
		    if (isset($post['student_name']) && !empty($post['student_name'])) {
	         
	            $select->where("(sp.appl_fname LIKE '%". $post['applicant_name']."%'");
	            $select->orwhere("sp.appl_mname LIKE '%". $post['applicant_name']."%'");
	            $select->orwhere("sp.appl_lname LIKE '%". $post['applicant_name']."%')");
	        }
        
	        if (isset($post['registrationId']) && !empty($post['registrationId'])) { 
	        	  $select->where("(sr.registrationId LIKE '%". $post['registrationId']."%'");	        	
	        }	      
		
	        $row = $db->fetchAll($select);
		 	
		 	
		 	return $row;
				 
	}
	
	
	public function searchStudentNotRegisterExam($post=null){
		
		    $db = Zend_Db_Table::getDefaultAdapter();

		    $semesterDB = new Registration_Model_DbTable_Semester();
			$semester_arr = $semesterDB->getEqSemesterOpen($post['IdSemester']);
		
		    $select_er =$db->select()
		    				->from(array('er'=>'exam_registration'),array('er_idStudentRegistration'))
		    				->join(array('srs'=>$this->_name),'srs.IdStudentRegistration=er.er_idStudentRegistration',array())	
		    				->where('er_idSubject = ?',$post['IdSubject'])
		    				->where('er_idSemester IN (?)',$semester_arr)
		    				->where('srs.Active != ?',3)
		    				->where('er.er_idCountry IS NULL')
		    				->where('er.er_idCity IS NULL');		   
			$row_er = $db->fetchAll($select_er);
			 
			
		    $select = $db->select()
		  			 	->distinct()
	 				 	->from(array('sr'=>'tbl_studentregistration'),array("registrationId"))	 	
	 					->join(array('srs'=>$this->_name),'srs.IdStudentRegistration=sr.IdStudentRegistration')			 
	 				 	->join(array('tp'=>'tbl_program'),'sr.IdProgram=tp.IdProgram',array())	 			
	 				 	->join(array('sp'=>"student_profile"),"sp.id=sr.sp_id",array("appl_fname","appl_mname","appl_lname"))	
	 				 	->where('srs.IdSemesterMain IN (?)',$semester_arr) 					 				
	 				 	->order("registrationId");
	 				

 			 if(count($row_er)>0){
 			 	 $select->where('sr.IdStudentRegistration NOT IN (?)',$row_er);
 			 }
 			 
		    if (isset($post['IdSemester']) && !empty($post['IdSemester'])) {         
	           // $select->where("srs.IdSemesterMain = ?", $post['IdSemester']);
	        }
	        
			if (isset($post['IdProgram']) && !empty($post['IdProgram'])) {         
	             //$select->where("sr.IdProgram = ?", $post['IdProgram']);
	        } 
		  
			if (isset($post['IdSubject']) && !empty($post['IdSubject'])) {         
	             $select->where("srs.IdSubject = ?", $post['IdSubject']);
	        } 
	        
		    if (isset($post['student_name']) && !empty($post['student_name'])) {
	         
	            $select->where("(sp.appl_fname LIKE '%". $post['applicant_name']."%'");
	            $select->orwhere("sp.appl_lname LIKE '%". $post['applicant_name']."%')");
	        }
        
	        if (isset($post['registrationId']) && !empty($post['registrationId'])) { 
	        	  $select->where("sr.registrationId LIKE '%". $post['registrationId']."%'");	        	
	        }	      
		
	       
	        $row = $db->fetchAll($select);
		 	
		 	
		 	return $row;
				 
	}
	
	public function getRegSubjectGroup($idStudentRegistration,$semester_id){
	
	  $db = Zend_Db_Table::getDefaultAdapter();
	  	
	  $select =$db->select()
				  ->from(array('srs'=>'tbl_studentregsubjects'))
				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = srs.IdSubject',array('IdSubject','SubjectName','SubCode','subjectMainDefaultLanguage','CreditHours','CourseType'))
				  ->joinLeft(array('g'=>'tbl_course_tagging_group'),'g.IdCourseTaggingGroup=srs.IdCourseTaggingGroup',array('GroupName','GroupCode'))
				  ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
				  ->where('srs.IdSemesterMain = ?',$semester_id);
	  	
	  $row = $db->fetchAll($select);
	   
	  return $row;
	
	}
	
	
	public function isRegisteredBySemester($idStudentRegistration,$idSubject,$idSemester,$withdraw_exception=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->where('srs.Active != ?',3);
	 					 				 
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}
	
	
	public function getCompletedCreditHour($idStudentRegistration){
	
	  $db = Zend_Db_Table::getDefaultAdapter();
	  	
	  $select =$db->select()
	  			  ->from(array('srs'=>'tbl_studentregsubjects'),array("TotalCH"=>"SUM(CreditHours)"))
				  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject = srs.IdSubject',array())
				  ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
				  ->where('srs.Active != 3')
				  ->where('srs.exam_status = "C"')
				  ->where('srs.grade_name != "F"')
				  ->group('srs.IdSubject');
				  	
	  $row = $db->fetchRow($select);
	   
	  return $row['TotalCH'];
	
	}
	
	public function getTotalRegisteredBySemester($idStudentRegistration,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select = $db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				// ->where('(srs.Active = 1 OR srs.Active = 4 OR srs.Active = 5 )')
	 				->where('srs.Active != 3');
	 				
	 	$row = $db->fetchAll($select);
	 	
	 	return $row;
		
	}
	
	
	public function checkIsRegisterItem($IdStudentRegistration,$idSubject,$idSemester,$idItem,$withdraw_exception=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array())	 
	 				 ->join(array('srsd'=>'tbl_studentregsubjects_detail'),'srsd.regsub_id=srs.IdStudentRegSubjects')				
	 				 ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester)
	 				 ->where('srsd.item_id = ?',$idItem);
	 				
					 if($withdraw_exception==1){
	 				 	$select->where('srsd.status != ?',3);
	 				 }
	 				 
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
							
	}
	
	
	public function isRegisteredPrevSemester($idStudentRegistration,$idSubject,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 	
	 				 ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster = srs.IdSemesterMain',array())			
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain != ?',$idSemester)
	 				 ->where('srs.Active != 3')
	 				 ->order('sm.SemesterMainStartDate DESC');
	 				 	 				
	 	$row = $db->fetchRow($select);
	 		 	
	 	if(!$row){
	 		
	 		$isRegisteredEqSubject = $this->checkSubjectEquivalent($idStudentRegistration,$idSubject,$idSemester);
	 			 		
	 		if($isRegisteredEqSubject){
	 			return $isRegisteredEqSubject;
	 		}
	 		
	 	}	 	
	 	return $row;
		
	}
	
	public function checkSubjectEquivalent($idStudentRegistration,$idSubject,$idSemester){
		
			$db = Zend_Db_Table::getDefaultAdapter();
		
			//check if equavalent paper
	 		$lstrSelect = $db->select()
		 				 ->from(array("se"=>"tbl_subjectequivalent"),array('idsubject'))	
		 				 ->where('se.Active=1')	 				
		 				 ->where('se.idsubjectequivalent = ?',$idSubject);	
			$eq_result = $db->fetchAll($lstrSelect);
		
			 	
	    	if(count($eq_result)>0){
	    		
	    		//check all this return subject already registerd or not
	    		foreach($eq_result as $res){
	    			
		    		$select = $db->select()		 			
				 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 	
				 				 ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster = srs.IdSemesterMain',array())			
				 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
				 				 ->where('srs.IdSubject = ?',$res['idsubject'])
				 				 ->where('srs.IdSemesterMain != ?',$idSemester)
				 				 ->where('srs.Active != 3')
				 				 ->order('sm.SemesterMainStartDate DESC');
		 				 	 				
		 			$row = $db->fetchRow($select);
		 			
		 			if($row){		 				
		 				return $row;
		 			}
	    		}	    		
	    	}
	    		
	    	return false; 
	}
	
	
	public function totalRegisteredPrevSemester($idStudentRegistration,$idSubject,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain != ?',$idSemester)
	 				 ->where('srs.Active != 3');
	 				
	 	$row = $db->fetchAll($select);
	 	
	 	return count($row);
		
	}
	
	public function sumCHRegisteredPrevSemester($idStudentRegistration,$idSubject,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'),array('jumlah'=>'SUM(credit_hour_registered)'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain != ?',$idSemester)
	 				 ->where('srs.Active != 3');
	 				
	 	$row = $db->fetchRow($select);
	 	
	 	if($row)
	 		return $row['jumlah'];
	 	else
	 		return 0;
		
	}
	
	public function checkActivity($IdSemester){
				
 		 $db = Zend_Db_Table::getDefaultAdapter();
        
 		 $curdate = date('Y-m-d');
       
         $select = $db->select()
                             ->from(array('ac'=>'tbl_activity_calender'))                            
                             ->where('ac.idActivity = ?',18) // Add & Drop 100% Refund
                             ->where('ac.IdSemesterMain = ?',$IdSemester);
        
        $row = $db->fetchRow($select);       
        return $row;		
	}	
	
	public function updateStatus($data,$id){	
		 $db = Zend_Db_Table::getDefaultAdapter();			
		 $where = 'IdStudentRegSubjects = "'.$id.'"';
		 $db->update('tbl_studentregsubjects',$data,$where);
	}
	
	public function checkCourseStatus($idstudent, $idSubject,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =$db->select()
	 				 ->from(array('srs'=>$this->_name))
	 				 ->where('srs.IdStudentRegistration = ?',$idstudent)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester);
	 	$row = $db->fetchRow($select);
	 	if($row){
	 		return $row;	
	 	}else{
	 		$select2 =$db->select()
	 				 ->from(array('srs'=>'tbl_studentregsubjects_history'))
	 				 ->where('srs.IdStudentRegistration = ?',$idstudent)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester);
	 		$row2 = $db->fetchRow($select2);
	 		if($row2){
	 			return $row2;
	 		}else{
	 			return null;
	 		}
	 	}
				 
	}
	
	public function getRepeatStatus($idStudentRegistration,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSemesterMain != ?',$idSemester)
	 				 ->where('srs.Active != 3')
	 				 ->where('srs.cgpa_calculation = ?',1);
	 				
	 	$row = $db->fetchAll($select);
	 	
	 	return count($row);
		
	}
	
	public function isRegisteredBySemesterIncludeWithdraw($idStudentRegistration,$idSubject,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select =$db->select()		 			
	 				 ->from(array('srs'=>'tbl_studentregsubjects'))	 				
	 				 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	 				 ->where('srs.IdSubject = ?',$idSubject)
	 				 ->where('srs.IdSemesterMain = ?',$idSemester);
	 					 				 
	 	$row = $db->fetchRow($select);
	 	
	 	return $row;
		
	}
	
	
	public function getStudentData($IdStudentRegSubjects){
		$db = Zend_Db_Table::getDefaultAdapter();
	    $select = $db->select()
	 	 				  ->from(array('srs'=>'tbl_studentregsubjects'),array('srs.exam_status'))
	 	 				  ->join(array('sr' => 'tbl_studentregistration'),'sr.IdStudentRegistration=srs.IdStudentRegistration',array('IdStudentRegistration','registrationId'))  
	 	 				  ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array("student_name"=>"CONCAT_WS(' ',appl_fname,appl_lname)"))
	 	 				  ->where('srs.IdStudentRegSubjects= ?',$IdStudentRegSubjects);	 	 							  
	 	return $row = $db->fetchRow($select);	 				 
	}
	
	
	//For Course Audit Report = CIFP
	//get selectte smester + current
	public function getTotalPaperRegistered($idStudentRegistration,$semester_id){
	
		
	  $db = Zend_Db_Table::getDefaultAdapter();
	  
	  $select =$db->select()
	  ->from(array('srs'=>'tbl_studentregsubjects'),array('IdSubject','mark_approval_status'))
	  ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
	  ->where('srs.IdSemesterMain = ?',$semester_id)
	  ->where('srs.Active != 3')
	  ->where('IFNULL(srs.exam_status, "X") != "U"');
	  //->where('IFNULL(srs.exam_status,"X") NOT IN ("EX","CT","U")');
	  	//echo $select;
	  $row = $db->fetchAll($select);
	   
	  return $row;
	
	}
	
	//For Course Audit Report = GS
	public function getTotalCreditHourRegisteredByType($idStudentRegistration,$my_regsub_arr,$subType){
	
	  $db = Zend_Db_Table::getDefaultAdapter();
	  	
	  /*if($subType==271){//Research
	  	$select =$db->select()
					  ->from(array('srs'=>'tbl_studentregsubjects'),array('IdSubject','mark_approval_status','total_ch_registered'=>'SUM(srs.credit_hour_registered)'))
					  ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
					  ->where('srs.IdSubject IN (?)',$my_regsub_arr)
					  ->where('srs.Active != 3')
					  ->where('srs.exam_status != "U"');
	  }else{
	  echo '<hr>'.	$select =$db->select()
					  ->from(array('srs'=>'tbl_studentregsubjects'),array('IdSubject','mark_approval_status','total_ch_registered'=>'SUM(sm.CreditHours)'))
					  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array())
					  ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
					  ->where('srs.IdSubject IN (?)',$my_regsub_arr)
					  ->where('srs.Active != 3')
					  ->where('srs.exam_status != "U"');	 
	  }*/
	  
	   $select =$db->select()
					  ->from(array('srs'=>'tbl_studentregsubjects'),array('IdSubject','mark_approval_status','credit_hour_registered'))
					  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours'))
					  ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
					  ->where('srs.IdSubject IN (?)',$my_regsub_arr)
					  ->where('srs.Active != 3')
					  ->where('srs.exam_status != "U"')
					  ->where('srs.exam_status != "I"');
	   	
	  $rows = $db->fetchAll($select);
	   
	   $total_credit_hour = 0;
 	   foreach($rows as $row){
 	   		if($row['credit_hour_registered']!='' || $row['credit_hour_registered']!=0){
 	   			$registered_ch = $row['credit_hour_registered'];
 	   		}else{
 	   			$registered_ch =  $row['CreditHours'];
 	   		}
 	   		$total_credit_hour=$total_credit_hour+$registered_ch;
 	   }
 	   
 	   return $total_credit_hour;
	  
	
	}
	
        public function getSemesterInfo($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
                ->where('a.IdSemesterMaster = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getSemesterRelated($year, $month){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
                ->where('a.AcademicYear = ?', $year)
                ->where('a.sem_seq = ?', $month)
                ->where('a.IsCountable = 1');
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        
/*
	 * To check if student have meet pre-requisite subject
	 */
	public function isRegisterPrevPass($idStudentRegistration,$idSubject,$idSemester){
		 $db = Zend_Db_Table::getDefaultAdapter();
		 $select =$db->select()		 					
					 ->from(array('srs'=>'tbl_studentregsubjects'))	 			
					 ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster = srs.IdSemesterMain',array())					
					 ->where('srs.IdStudentRegistration = ?',$idStudentRegistration)		
					 ->where('srs.IdSubject = ?',$idSubject)		
					 ->where('srs.IdSemesterMain != ?',$idSemester)		
					 ->where('srs.Active != 3')		
					 ->order('sm.SemesterMainStartDate DESC');		
												
		 $row = $db->fetchRow($select);	
		 
		 return $row;
	}
	
	
/* 
    * Function : For CIFP to check if student already registered PPP or Articleship
    */
	public function isRegisteredPaper($idStudentRegistration){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		                                      
		$select =$db->select()	
					->from(array('srs'=>'tbl_studentregsubjects'))	
					->where('srs.IdStudentRegistration = ?',$idStudentRegistration)
					->where('srs.IdSubject IN (84,85)')
					->where('srs.Active != ?',3)
					->where('srs.exam_status = "C"')
					->where('srs.grade_name = "P"')
					->where('srs.mark_approval_status = ?',2)
					->order('srs.IdStudentRegSubjects DESC'); //amik yg latest
		
		$row = $db->fetchRow($select);			
		
		$publishresult = false;
		
		if($row){	
		
		       $publishInfo = $this->getPublishResult(5, $row['IdSemesterMain']);
		
		         if ($publishInfo){
		         	$publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'].' '.$publishInfo['pm_time']);
		         	$publishInfo['pm_cdate']= strtotime(date('Y-m-d H:i:s'));
		
		           	if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']){
		           	$publishresult = true;
		           	}
		          }
		}
		
		if($row && $publishresult==true){
		return $row;
		}
	
	}
	
	
	public function getPublishResult($programId, $semesterId, $category = 0){
           $db = Zend_Db_Table::getDefaultAdapter();
           $select = $db->select()
               ->from(array('a'=>'tbl_publish_mark'), array('value'=>'*'))
               ->where('a.pm_idProgram = ?', $programId)
               ->where('a.pm_idSemester = ?', $semesterId)
               ->where('a.pm_category = ?', $category);

           $result = $db->fetchRow($select);
           return $result;
       }

	public function getReplaceSubject($studentid, $subjectid){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
			->where('a.IdStudentRegistration = ?', $studentid)
			->where('a.IdSubject = ?', $subjectid)
			->order('a.IdStudentRegSubjects DESC');

		$result = $db->fetchRow($select);
		return $result;
	}
}

?>