<?php

class Registration_Model_DbTable_StudentAcademicAdvisorHistory extends Zend_Db_Table_Abstract {

	protected $_name = 'tbl_student_academic_advisor_history';
	private $lobjDbAdpt;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function addStudentAdvisorHistory(array $data) {
        $this->insert($data);
    
    }
    
    public function getData($id) {
		$sql = $this->lobjDbAdpt->select()
		->from(array('a' => $this->_name))		
		->joinLeft(array('b' => 'tbl_studentregistration'), 'a.saa_IdStudentRegistration = b.IdStudentRegistration')
		->joinLeft(array('c' => 'tbl_staffmaster'), 'a.saa_staff_id = c.IdStaff')
		->joinLeft(array('d' => 'tbl_user'), 'a.created_by = d.iduser')
		->where("b.IdStudentRegistration =?", $id)
        ->order("a.created_date ASC");
		$result = $this->lobjDbAdpt->fetchAll($sql);
		return $result;
	}

	
	
}