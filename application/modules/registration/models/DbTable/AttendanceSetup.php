<?php 
class Registration_Model_DbTable_AttendanceSetup extends Zend_Db_Table_Abstract
{
	
	/**
	 * The default table name 
	 */
	
	protected $_name = 'attendance_setup';
	protected $_primary = "att_id";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function checkDuplicate($att_effective_date,$idProgram,$att_program_scheme,$att_reg_item, $att_type)
    {
       	$db = Zend_Db_Table::getDefaultAdapter();
    
        $sql = $db->select()
                  ->from(array('att' => $this->_name))
                  ->where('DATE(att.att_effective_date) = ?',date('Y-m-d',strtotime($att_effective_date) ))        
                  ->where('att.att_program_id = ?',$idProgram)
                  ->where('att.att_program_scheme = ?',$att_program_scheme)
                  ->where('att.att_reg_item = ?',$att_reg_item)
                  ->where('att.att_type = ?',$att_type);
		$result = $db->fetchRow($sql);
        
        return $result;
    	
    }
    
	public function getList($formData=null)
    {
       	$db = Zend_Db_Table::getDefaultAdapter();
    
        $sql = $db->select()
                  ->from(array('att' => $this->_name)) 
                  ->join(array('p'=>'tbl_program'), 'att.att_program_id = p.IdProgram',array('ProgramName','ProgramCode'))
                  ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme=att.att_program_scheme',array())	
				  ->join(array('def'=>'tbl_definationms'),'def.idDefinition=ps.mode_of_study',array('studymode'=>'DefinitionDesc'))
				  ->join(array('d'=>'tbl_definationms'),'d.idDefinition=ps.mode_of_program',array('programmode'=>'DefinitionDesc'))
				  ->join(array('dt'=>'tbl_definationms'),'dt.idDefinition=ps.program_type',array('programtype'=>'DefinitionDesc'))
				  ->join(array('di'=>'tbl_definationms'),'di.idDefinition=att.att_reg_item',array('reg_item'=>'DefinitionDesc'))
				  ->order('att.att_effective_date DESC');

				  if(isset($formData)){
				  	
				  	if(isset($formData['att_program_id']) && $formData['att_program_id']){
				  		$sql->where('att.att_program_id = ?',$formData['att_program_id']);
				  	}
				  	
				  	if(isset($formData['att_program_scheme']) && $formData['att_program_scheme']){
				  		$sql->where('att.att_program_scheme = ?',$formData['att_program_scheme']);
				  	}
				  	
				 	if(isset($formData['att_reg_item']) && $formData['att_reg_item']){
				  		$sql->where('att.att_reg_item = ?',$formData['att_reg_item']);
				  	}

					  if(isset($formData['att_type']) && $formData['att_type']){
						  $sql->where('att.att_type = ?',$formData['att_type']);
					  }
				  	
				  	if(isset($formData['att_date_from']) && $formData['att_date_from']){
				  		$sql->where('DATE(att.att_effective_date) >= ?',date('Y-m-d',strtotime($formData['att_date_from'])));
				  	}
				  }
					
		$result = $db->fetchAll($sql); 
        
        return $result;
    	
    }
    
	public function getData()
    {
       	$db = Zend_Db_Table::getDefaultAdapter();
    
        $sql = $db->select()
                  ->from(array('att' => $this->_name))
                  ->where('DATE(att.att_effective_date) = ?',date('Y-m-d',strtotime($att_effective_date) ))        
                  ->where('att.att_program_id = ?',$idProgram)
                  ->where('att.att_program_scheme = ?',$att_program_scheme)
                  ->where('att.att_reg_item = ?',$att_reg_item);
		$result = $db->fetchRow($sql); 
        
        return $result;
    	
    }
    
	public function getLatestPercentage($idProgram,$att_program_scheme,$att_reg_item, $att_type)
    {
       	$db = Zend_Db_Table::getDefaultAdapter();
    
        $sql = $db->select()
                  ->from(array('att' => $this->_name))
                  ->where('DATE(att.att_effective_date) <= ?',date('Y-m-d'))        
                  ->where('att.att_program_id = ?',$idProgram)
                  ->where('att.att_program_scheme = ?',$att_program_scheme)
                  ->where('att.att_reg_item = ?',$att_reg_item)
                  ->where('att.att_type = ?',$att_type)
                  ->order('att.att_effective_date desc'); //amik date yg paling latest sekali
		$result = $db->fetchRow($sql); 
        
		return $result;
    	
    }
}

?>