<?php

class Registration_Model_DbTable_Studenthistory extends Zend_Db_Table_Abstract {

    protected $_name = 'tbl_student_status_history';
    private $lobjDbAdpt;

    public function init() {
        $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    }

    public function fetchStudentHistory($IdStudent){
         $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_student_status_history"), array("a.*"))
                    ->where("a.IdStudentRegistration =?",$IdStudent);
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function addStudentProfileHistory($data){
        $this->insert($data);
    }
    
    public function deleteData($id){		
	  $this->delete('IdStudentRegistration = '. (int)$id);
	}
	
 	public function getChangeStatusLatestProfileStatus($IdStudent){
         $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array("a" => "tbl_student_status_history"))
                    ->where("a.IdStudentRegistration =?",$IdStudent)
                    ->where('a.Type=?',1)
                    ->order('a.IdStudentHistory DESC');
        $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }

}

?>
