<?php 
class Registration_Model_DbTable_Ldap extends Zend_Db_Table_Abstract {


	/**
	 * The default table name 
	 */
	
	protected $_name = 'ldap_api';
	protected $_primary = "ldap_id";
	
	/**
	 *  insert record
	 */
	 
	public function addLdap(array $data) {
	
		$data['action_type'] = 'NEW';
		$data['status_type'] = 'NEW';
		
		if ($this->insert($data))
		{
			return 'Ok';
		}
		else
		{
			return 'Failed';
		}
		
	}
	
	public function changePasswordLdap(array $data) {
		
		$data['action_type'] = 'PASSWORD';
		$data['status_type'] = 'NEW';
		
		
		if ($this->insert($data))
		{
			return 'Ok';
		}
		else
		{
			return 'Failed';
		}
		
	}
	
	public function updateLdap(array $data) {
	
		$data['action_status'] = 'EDIT';
		$data['status_type'] = 'NEW';
		
		if ($this->insert($data))
		{
			return 'Ok';
		}
		else
		{
			return 'Failed';
		}
		
	}
	
}
