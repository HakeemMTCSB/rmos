<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 1/3/2016
 * Time: 3:33 PM
 */
class Registration_Model_DbTable_GradebookByBatch extends Zend_Db_Table_Abstract{

    public function getProgram(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('value'=>'*'))
            ->order('a.seq_no');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getGroupDetails($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->where('a.IdCourseTaggingGroup = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }
}