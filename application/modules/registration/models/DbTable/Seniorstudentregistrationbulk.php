<?php 
class Registration_Model_DbTable_Seniorstudentregistrationbulk extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_studentregistration';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	
	public function fnSearchStudentApplication($post = array()) { //Function to get the user details
		
		//$consistantresult = 'SELECT i.IdApplication  from tbl_studentregistration i where i.IdApplication=sa.IdApplication';
		$consistantresult = 'SELECT MAX(i.IdStudentRegistration)  from tbl_studentregistration i where i.IdApplication = a.IdApplication';
		
		
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_studentapplication'),array('a.IdApplication','a.StudentId','a.FName','a.MName','a.LName'))
                			->join(array('b'=>'tbl_collegemaster'),'a.idCollege  = b.IdCollege',array('b.IdCollege','b.CollegeName','b.ShortName','b.CollegeCode'))
                			->join(array('c'=>'tbl_program'),'a.IDCourse = c.IdProgram')
                			->join(array('d'=>'tbl_studentregistration'),'a.IdApplication = d.IdApplication')
                			->join(array('e'=>'tbl_verifypayments'),'e.IdApplication = d.IdApplication AND e.IdSemester = d.IdSemestersyllabus AND e.VerifyPayment = 1',array('e.IdVerifyPayments'))
                			->where("d.IdStudentRegistration = ?",new Zend_Db_Expr('('.$consistantresult.')'))
                			->where('a.ICNumber like "%" ? "%"',$post['field2'])
       						->where('a.StudentId like "%" ? "%"',$post['field3'])
       						->where('a.FName like "%" ? "%"',$post['field4'])
       						->where('d.registrationId like "%" ? "%"',$post['field6']);
			
			if(isset($post['field8']) && !empty($post['field8']) ){
				$lstrSelect = $lstrSelect->where("a.IDCourse = ?",$post['field8']);
			}

       	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	
	 
	 
	   public function getCompleteStudentDetails($lintidapplicant) { 
	     	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a' => 'tbl_studentapplication'),array('a.*'))
                			->join(array('b'=>'tbl_collegemaster'),'a.idCollege  = b.IdCollege')
                			->join(array('c'=>'tbl_program'),'a.IDCourse = c.IdProgram')
                			->where('a.IdApplication = ?',$lintidapplicant);
			$result = $db->fetchRow($sql);
			return $result;
		}
	
	public function getLandscapeDropDown($idprogram){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_landscape"),array("key"=>"a.IdLandscape","value"=>"CONCAT_WS('-',IFNULL(b.DefinitionDesc,'-'),IFNULL(a.IdLandscape,'-'))"))
		 				 ->join(array('b'=>'tbl_definationms'),'a.LandscapeType = b.idDefinition')
		 				 ->where('a.IdProgram = ?',$idprogram)
		 				 ->where("a.Active = 123");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
		
	
	
    public function fnAddStudentregistration($formData) { //Function for adding the Program Branch details to the table
		unset ( $formData ['IdProgram'] );
		unset ( $formData ['Save']);
		unset ( $formData ['subjects']);
		unset ( $formData ['selectall']);
		unset ( $formData ['studentname']);
	    return $this->insert($formData);
	}
	
	public function fnAddStudentSubjectsperSemester($larrformData) {  // function to insert po details
		unset ( $larrformData ['IdProgram'] );
		unset ( $larrformData ['Save']);
		unset ( $larrformData ['selectall']);
		
		unset ( $larrformData ['IdApplication']);
		unset ( $larrformData ['registrationId']);
		unset ( $larrformData ['IdLandscape']);
		unset ( $larrformData ['studentname']);
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_studentregsubjects";
		$db->insert($table,$larrformData);	
	}
	
	
	public function fnUpdateStudentApplication($lintidapplicant) {  // function to update po details
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_studentapplication";
			$larrapplicant = array('Registered'=>'1');
			$where = "IdApplication = '".$lintidapplicant."'";
			$db->update($table,$larrapplicant,$where);	
	}
	
	function fnGenerateCode($idUniversity,$collageId,$page,$IdInserted){	
			$db 	= 	Zend_Db_Table::getDefaultAdapter();			
		$select =   $db->select()
				->  from('tbl_config')
				->	where('idUniversity  = ?',$idUniversity);				 
		$result = 	$db->fetchRow($select);		
		$sepr	=	$result[$page.'Separator'];
		$str	=	$page."CodeField";
		$CodeText	=	$page."CodeText";
		for($i=1;$i<=4;$i++){
			$check = $result[$str.$i];
			$TextCode = $result[$CodeText.$i];
			switch ($check){
				case 'Year':
					  $code	= date('Y');
					  break;
				case 'Uniqueid':
					  $code	= $IdInserted;
					  break;
				case 'College':
					  $select =  $db->select()
					 		 -> from('tbl_collegemaster')
					 		 ->	where('IdCollege  = ?',$collageId);  				 
					  $resultCollage = $db->fetchRow($select);		
					  $code		   = $resultCollage['ShortName'];
				      break;
				case 'University':
					  $select =  $db->select()
					 		 -> from('tbl_universitymaster')
					 		 ->	where('IdUniversity  = ?',$idUniversity);  				 
					  $resultCollage = $db->fetchRow($select);		
					  $code	= $resultCollage['ShortName'];
				      break;
				 case 'Text':					 		
					  $code		   = $TextCode;
				      break;
				default:
				      break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}	 	
		return $accCode;			
	}
	
	 public function fnupdateRegistrationtCode($larrstudentreg,$registrationCode) { 
	 	$larrformData['registrationId']   	 = $registrationCode;
		$where = 'IdStudentRegistration = '.$larrstudentreg;
		$this->update($larrformData,$where);
	 }
	 
	   public function fnCheckExistingSemester($IdSemester,$lintidapplicant) { 
	     	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a' => 'tbl_studentregistration'),array('a.*'))
							->where('a.IdApplication  = ?',$lintidapplicant)
							->where('a.IdSemester = ?',$IdSemester);
			$result = $db->fetchRow($sql);
			return $result;
		}

}
?>