<?php

class Registration_Model_DbTable_Seniorstudentregistration extends Zend_Db_Table_Abstract {

  protected $_name = 'tbl_studentregistration';
  private $lobjDbAdpt;

  public function init() {
    $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  }

  public function fngetStudentRegistrationDetails() {
 	$select = $this->lobjDbAdpt->select()
                ->from(array('a'=>'tbl_studentregistration'),array('a.registrationId','a.IdStudentRegistration'))
                ->joinLeft(array("p"=>"applicant_profile"),'p.appl_id=a.IdApplication',array('appl_fname','appl_mname','appl_lname'))
                ->joinLeft(array('i' => 'tbl_intake'),'a.IdIntake = i.IdIntake',array("i.IntakeDesc"))
                ->joinLeft(array('pr'=>'tbl_program'),'pr.IdProgram = a.IdProgram',array("ArabicName","ProgramCode"))
                ->joinLeft(array('b'=>'tbl_studentsemesterstatus'),'a.IdStudentRegistration  = b.IdStudentRegistration',array("b.idSemester","b.IdSemesterMain"))
                ->joinLeft(array('c'=>'tbl_semester'),'b.idSemester = c.IdSemester',array("c.SemesterCode AS semestername"))
                ->joinLeft(array('d'=>'tbl_semestermaster'),'b.IdSemesterMain = d.IdSemesterMaster',array("d.SemesterMainCode AS mainsemestername"))
                ->where("b.studentsemesterstatus = 130")
                ->order("p.appl_fname");
       $result = $this->lobjDbAdpt->fetchAll($select);
       return $result;
  }

  public function fngetSeniorStudentRegDet($id){
    $select = $this->lobjDbAdpt->select()
                   ->from(array('a'=>'tbl_studentregistration'),array('a.IdStudentRegistration','a.IdProgram','a.IdIntake','a.registrationId','a.IdBranch','a.status','a.ApplicationDate','a.IdLandscape','a.IdApplication'))
 				   ->joinLeft(array("p"=>"applicant_profile"),'p.appl_id=a.IdApplication',array('appl_fname','appl_mname','appl_lname'))
                   ->joinLeft(array('b' =>'tbl_program'),'a.IdProgram = b.IdProgram',array("b.IdScheme","b.ProgramName","b.ArabicName"))
                   ->joinLeft(array('c' =>'tbl_scheme'),'b.IdScheme = c.IdScheme',array("c.ArabicDescription AS schemeName"))
                   ->joinLeft(array('d' => 'tbl_intake'),'a.IdIntake = d.IdIntake',array("d.IntakeDesc"))
                   ->joinLeft(array('e' => 'tbl_branchofficevenue'),'a.IdBranch = e.IdBranch',array("e.BranchName"))
                   ->joinLeft(array('f' => 'tbl_definationms'),'a.status = f.idDefinition',array("f.DefinitionDesc","f.BahasaIndonesia"))
                   ->joinLeft(array('g' => 'tbl_studentsemesterstatus'),'a.IdStudentRegistration = g.IdStudentRegistration',array('g.idSemester','g.IdSemesterMain'))
                   ->joinLeft(array('h'=>'tbl_semester'),'g.idSemester = h.IdSemester',array("h.SemesterCode AS semestername","h.SemesterStartDate"))
                   ->joinLeft(array('i'=>'tbl_semestermaster'),'g.IdSemesterMain = i.IdSemesterMaster',array("i.SemesterMainCode AS semestername","i.SemesterMainStartDate"))
                   ->where("a.IdStudentRegistration =?",$id)
                   ->where("g.studentsemesterstatus =?",130);

   return $this->lobjDbAdpt->fetchAll($select);
  }

  /* public function fngetStudentRegistrationDetails() { //Function to get the Program Branch details
    $consistantresult = 'SELECT MAX(i.IdStudentRegistration)  from tbl_studentregistration i where i.IdApplication = a.IdApplication';
    $select = $this->select()
    ->setIntegrityCheck(false)
    ->join(array('a' => 'tbl_studentapplication'),array('IdApplication'))
    ->join(array('b'=>'tbl_studentregistration'),'a.IdApplication  = b.IdApplication')
    ->where("b.IdStudentRegistration = ?",new Zend_Db_Expr('('.$consistantresult.')'))
    ->where("a.Active = 1")
    ->where("a.Offered = 1")
    ->where("a.Accepted = 1")
    ->where("a.Termination = 0")
    ->order("a.FName");
    $result = $this->fetchAll($select);
    return $result->toArray();
    } */

  public function fnGetApplicantNameList() {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("sa" => "tbl_studentapplication"), array("key" => "sa.IdApplication", "value" => "CONCAT_WS(' ',IFNULL(sa.FName,''),IFNULL(sa.MName,''),IFNULL(sa.LName,''))"))
                    ->where("sa.Active = 1")
                    ->where("sa.Offered = 1")
                    ->where("sa.Accepted = 1")
                    ->where("sa.Termination = 0")
                    ->order("sa.FName");
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);


    return $larrResult;
  }

  public function fnGetSemesterList($lintidlandscape) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("sa" => "tbl_landscape"), array("sa.SemsterCount"))
                    ->where("sa.Active = 123")
                    ->where("sa.IdLandscape= ?", $lintidlandscape);
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);

    return $larrResult;
  }

  public function fnSearchStudentApplication($post = array()) { 

    $lstrSelect = $this->lobjDbAdpt->select()
                ->from(array('a'=>'tbl_studentregistration'),array('a.FName','a.LName','a.MName','a.registrationId','a.IdStudentRegistration'))
 				->joinLeft(array("p"=>"applicant_profile"),'p.appl_id=a.IdApplication',array('appl_fname','appl_mname','appl_lname'))
 				->joinLeft(array('pr'=>'tbl_program'),'pr.IdProgram = a.IdProgram',array("ArabicName","ProgramCode"))
                ->joinLeft(array('b'=>'tbl_studentsemesterstatus'),'a.IdStudentRegistration  = b.IdStudentRegistration',array("b.idSemester"))
                ->joinLeft(array('c'=>'tbl_semester'),'b.idSemester = c.IdSemester',array("c.SemesterCode AS semestername"))
                ->joinLeft(array('d'=>'tbl_semestermaster'),'b.IdSemesterMain = d.IdSemesterMaster',array("d.SemesterMainCode AS mainsemestername"));
               

    if (isset($post['field1']) && !empty($post['field1'])) {
      $lstrSelect = $lstrSelect->where("a.IdProgram = ?", $post['field1']);
    }

    if (isset($post['field2']) && !empty($post['field2'])) {
      $lstrSelect = $lstrSelect->where('a.registrationId like "%" ? "%"', $post['field2']);
    }

    if (isset($post['field6']) && !empty($post['field6'])) {
      //$wh_condition = "a.FName like '%".$post['field6']."%' OR  a.MName like '%".$post['field6']."%' OR  a.LName like '%".$post['field6']."%' ";
      // $lstrSelect = $lstrSelect->where($wh_condition);
        $lstrSelect->where("(p.appl_fname LIKE '%". $post['field6']."%'");
        $lstrSelect->orwhere("p.appl_mname LIKE '%". $post['field6']."%'");
        $lstrSelect->orwhere("p.appl_lname LIKE '%". $post['field6']."%')");
    }

    if (isset($post['field12']) && !empty($post['field12'])) {
      $lstrSelect = $lstrSelect->where('a.IdBranch like "%" ? "%"', $post['field12']);
    }

    if (isset($post['field8']) && !empty($post['field8'])) {
      $lstrSelect = $lstrSelect->where('a.IdIntake like "%" ? "%"', $post['field8']);
    }

    if (isset($post['field27']) && !empty($post['field27'])) {
      $where = "b.idSemester = '".$post['field27']."' OR b.IdSemesterMain = '".$post['field27']."'";
      $lstrSelect = $lstrSelect->where($where);
    }

    if (isset($post['field5']) && !empty($post['field5'])) {
      $lstrSelect->joinLeft(array('e'=>'tbl_program'),'a.IdProgram = e.IdProgram',array("e.IdScheme"));
      $lstrSelect = $lstrSelect->where("e.IdScheme = ?", $post['field5']);
    }

    $lstrSelect->where("b.studentsemesterstatus = 130");
    $lstrSelect->order("p.appl_fname");
        
    
    $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function getCompleteStudentRegistrationDetails($lintidapplicant) {

    $consistantresult = 'SELECT MAX(i.IdStudentRegistration)  from tbl_studentregistration i where i.IdApplication = ' . $lintidapplicant;

    $db = Zend_Db_Table::getDefaultAdapter();
    $sql = $db->select()
                    ->from(array('a' => 'tbl_studentapplication'), array('a.*'))
                    ->join(array('b' => 'tbl_collegemaster'), 'a.idCollege  = b.IdCollege')
                    ->join(array('c' => 'tbl_program'), 'a.IDCourse = c.IdProgram')
                    ->join(array('d' => 'tbl_studentregistration'), 'a.IdApplication = d.IdApplication')
                    ->where("d.IdStudentRegistration = ?", new Zend_Db_Expr('(' . $consistantresult . ')'))
                    ->where('a.IdApplication = ?', $lintidapplicant);
    $result = $db->fetchRow($sql);
    return $result;
  }

  public function getSemesterDropDown() {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("sa" => "tbl_semestermaster"), array("key" => "b.IdSemester", "value" => "CONCAT_WS(' ',IFNULL(sa.SemesterMasterName,''),IFNULL(b.year,''))"))
                    ->join(array('b' => 'tbl_semester'), 'sa.IdSemesterMaster = b.Semester')
                    ->where("b.IdSemester != 1")
                    ->where("sa.Active = 1");
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function getLandscapeDropDown($idprogram) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_landscape"), array("key" => "a.IdLandscape", "value" => "CONCAT_WS('-',IFNULL(b.DefinitionDesc,'-'),IFNULL(a.IdLandscape,'-'))"))
                    ->join(array('b' => 'tbl_definationms'), 'a.LandscapeType = b.idDefinition')
                    ->where('a.IdProgram = ?', $idprogram)
                    ->where("a.Active = 123");
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function fnGetLandscapeType($lintidlandscape) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $sql = $db->select()
                    ->from(array('a' => 'tbl_landscape'), array('a.LandscapeType'))
                    ->where('a.IdLandscape = ?', $lintidlandscape);
    $result = $db->fetchRow($sql);
    return $result;
  }

  public function fnGetAllSemesterbasedSujectDetails($lintidlandscape, $lintidsemsster, $lintidsemsstersyllabus, $lintidapplicant, $failedsubjects, $lintidprogram, $takemarks) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

    $consistantresult = "SELECT i.IdSubject  from tbl_subjectsoffered i
							 where i.IdSemester='$lintidsemsstersyllabus'";
    $selectA = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_landscapesubject"), array("a.*"))
                    ->join(array('b' => 'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
                    ->where("a.IdSubject IN ?", new Zend_Db_Expr('(' . $consistantresult . ')'))
                    ->where('a.IdSemester = ?', $lintidsemsster)
                    ->where('a.IdLandscape = ?', $lintidlandscape);

//////////////////////////////////////////////////////////////////////////
    $consistantsubjects = "SELECT i.IdSubject  from tbl_studentregsubjects i,tbl_studentregistration j
							 WHERE j.IdApplication ='$lintidapplicant'  AND j.IdStudentRegistration= i.IdStudentRegistration";

    $consistantresult1 = "SELECT DISTINCT(i.IdSubject)  from tbl_subjectsoffered i,tbl_studentregistration j
							 where j.IdApplication ='$lintidapplicant' AND j.IdSemestersyllabus = i.IdSemester";

    $selectB = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_landscapesubject"), array("a.*"))
                    ->join(array('b' => 'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
                    ->join(array('c' => 'tbl_studentregistration'), 'a.IdSemester = c.IdSemester', array())
                    ->where("a.IdSubject IN ?", new Zend_Db_Expr('(' . $consistantresult1 . ')'))
                    ->where("a.IdSubject NOT IN ?", new Zend_Db_Expr('(' . $consistantsubjects . ')'))
                    ->where('c.IdApplication = ?', $lintidapplicant)
                    ->where('a.IdLandscape = ?', $lintidlandscape);

/////////////////////////////////////////////////////////////////////		 				 

    if ($takemarks == '0') {
      $selectdb = 'SELECT w.*,SUM(w.totalsubmarks) as fullcalculatedmarks FROM
						(SELECT `b`.`IdSubject`,`c`.`IdSemestersyllabus`, AVG(e.verifiresubjectmarks) AS `totalsubmarks` 
						FROM  `tbl_studentregsubjects` AS `b`
						INNER JOIN `tbl_studentregistration` AS `c` ON b.IdStudentRegistration = c.IdStudentRegistration
						INNER JOIN `tbl_subjectmarksentry` AS `d` ON c.IdStudentRegistration = d.IdStudentRegistration AND b.IdSubject= d.idSubject
						INNER JOIN `tbl_verifiermarks` AS `e` ON d.idSubjectMarksEntry = e.idSubjectMarksEntry WHERE (c.IdApplication    =  "' . $lintidapplicant . '")
						GROUP BY `e`.`idSubjectMarksEntry`) w 
						GROUP BY w.IdSubject';
    } elseif ($takemarks == '1') {
      $selectdb = $lobjDbAdpt->select()
                      ->from(array('b' => 'tbl_studentregsubjects'), array('b.IdSubject'))
                      ->join(array('c' => 'tbl_studentregistration'), 'b.IdStudentRegistration = c.IdStudentRegistration', array('c.IdSemestersyllabus'))
                      ->join(array('d' => 'tbl_subjectmarksentry'), 'c.IdStudentRegistration = d.IdStudentRegistration AND b.IdSubject= d.idSubject', array())
                      ->join(array('e' => 'tbl_verifiermarks'), 'd.idSubjectMarksEntry = e.idSubjectMarksEntry', array('SUM(e.verifiresubjectmarks) as fullcalculatedmarks'))
                      ->where("e.Rank = 1 OR e.Rank = 0")
                      ->where('c.IdApplication = ?', $lintidapplicant)
                      ->group('d.idSubject');
    }

    $larrResultDb = $lobjDbAdpt->fetchAll($selectdb);

    $subjectssubjects = 0;
    $values = 0;
    foreach ($larrResultDb as $larrResultDbs) {
      $lstrSelect = $lobjDbAdpt->select()
                      ->from(array('a' => 'tbl_gradesetup'), array('a.Pass', 'a.IdSubject'))
                      ->where('a.MinPoint <= ?', $larrResultDbs['fullcalculatedmarks'])
                      ->where('a.MaxPoint >= ?', $larrResultDbs['fullcalculatedmarks'])
                      ->where('a.IdSubject = ?', $larrResultDbs['IdSubject'])
                      ->where('a.IdProgram = ?', $lintidprogram)
                      ->where('a.IdSemester = ?', $larrResultDbs['IdSemestersyllabus']);
      $larrResultmarks = $lobjDbAdpt->fetchRow($lstrSelect);

      if ($larrResultmarks['Pass'] == '0') {
        $value = $larrResultmarks['IdSubject'];
        $values = $values . ',' . $value;
        $subjectssubjects = $values;
      }
    }

    $selectC = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_landscapesubject"), array("a.*"))
                    ->join(array('b' => 'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
                    ->join(array('c' => 'tbl_studentregistration'), 'a.IdSemester = c.IdSemester', array())
                    ->where("a.IdSubject in ($subjectssubjects)")
                    ->where('c.IdApplication = ?', $lintidapplicant)
                    ->where('a.IdLandscape = ?', $lintidlandscape);
/////////////////////////////////////////////////////////////////////
    $select = $lobjDbAdpt->select()
                    ->union(array($selectA, $selectB, $selectC));

    if ($failedsubjects == 0) {
      $larrResult = $lobjDbAdpt->fetchAll($select);
    } elseif ($failedsubjects == 1) {
      $larrResult = $lobjDbAdpt->fetchAll($selectA);
    }
    return $larrResult;
  }

  public function fnGetAllBlockbasedSujectDetails($lintidlandscape, $lintidsemsster, $lintidsemsstersyllabus, $lintidapplicant, $failedsubjects, $lintidprogram, $takemarks) {
    $consistantresult = "SELECT i.IdSubject  from tbl_subjectsoffered i
							 where i.IdSemester='$lintidsemsstersyllabus'";

    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $selectA = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_landscapeblocksemester"), array("a.*"))
                    ->join(array('b' => 'tbl_landscapeblocksubject'), 'a.blockid = b.blockid')
                    ->join(array('c' => 'tbl_subjectmaster'), 'b.subjectid = c.IdSubject')
                    ->where("b.subjectid IN ?", new Zend_Db_Expr('(' . $consistantresult . ')'))
                    ->where('a.IdLandscape = ?', $lintidlandscape)
                    ->where('b.IdLandscape = ?', $lintidlandscape)
                    ->where('a.semesterid = ?', $lintidsemsster);

////////////////////////////////////////////////////////////////////////

    $consistantsubjects = "SELECT i.IdSubject  from tbl_studentregsubjects i,tbl_studentregistration j
							 WHERE j.IdApplication ='$lintidapplicant'  AND j.IdStudentRegistration= i.IdStudentRegistration";


    $consistantresult1 = "SELECT DISTINCT(i.IdSubject)  from tbl_subjectsoffered i,tbl_studentregistration j
							 where j.IdApplication ='$lintidapplicant' AND j.IdSemestersyllabus = i.IdSemester";

    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $selectB = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_landscapeblocksemester"), array("a.*"))
                    ->join(array('b' => 'tbl_landscapeblocksubject'), 'a.blockid = b.blockid')
                    ->join(array('c' => 'tbl_subjectmaster'), 'b.subjectid = c.IdSubject')
                    ->join(array('d' => 'tbl_studentregistration'), 'a.semesterid = d.IdSemester', array())
                    ->where("b.subjectid IN ?", new Zend_Db_Expr('(' . $consistantresult1 . ')'))
                    ->where("b.subjectid NOT IN ?", new Zend_Db_Expr('(' . $consistantsubjects . ')'))
                    ->where('a.IdLandscape = ?', $lintidlandscape)
                    ->where('b.IdLandscape = ?', $lintidlandscape)
                    ->where('d.IdApplication = ?', $lintidapplicant);
/////////////////////////////////////////////////////////////////////////////////////////

    if ($takemarks == '0') {
      $selectdb = 'SELECT w.*,SUM(w.totalsubmarks) as fullcalculatedmarks FROM
						(SELECT `b`.`IdSubject`,`c`.`IdSemestersyllabus`, AVG(e.verifiresubjectmarks) AS `totalsubmarks` 
						FROM  `tbl_studentregsubjects` AS `b`
						INNER JOIN `tbl_studentregistration` AS `c` ON b.IdStudentRegistration = c.IdStudentRegistration
						INNER JOIN `tbl_subjectmarksentry` AS `d` ON c.IdStudentRegistration = d.IdStudentRegistration AND b.IdSubject= d.idSubject
						INNER JOIN `tbl_verifiermarks` AS `e` ON d.idSubjectMarksEntry = e.idSubjectMarksEntry WHERE (c.IdApplication    =  "' . $lintidapplicant . '")
						GROUP BY `e`.`idSubjectMarksEntry`) w 
						GROUP BY w.IdSubject';
    } elseif ($takemarks == '1') {
      $selectdb = $lobjDbAdpt->select()
                      ->from(array('b' => 'tbl_studentregsubjects'), array('b.IdSubject'))
                      ->join(array('c' => 'tbl_studentregistration'), 'b.IdStudentRegistration = c.IdStudentRegistration', array('c.IdSemestersyllabus'))
                      ->join(array('d' => 'tbl_subjectmarksentry'), 'c.IdStudentRegistration = d.IdStudentRegistration AND b.IdSubject= d.idSubject', array())
                      ->join(array('e' => 'tbl_verifiermarks'), 'd.idSubjectMarksEntry = e.idSubjectMarksEntry', array('SUM(e.verifiresubjectmarks) as fullcalculatedmarks'))
                      ->where("e.Rank = 1 OR e.Rank = 0")
                      ->where('c.IdApplication = ?', $lintidapplicant)
                      ->group('d.idSubject');
    }

    $larrResultDb = $lobjDbAdpt->fetchAll($selectdb);

    $subjectssubjects = 0;
    $values = 0;
    foreach ($larrResultDb as $larrResultDbs) {
      $lstrSelect = $lobjDbAdpt->select()
                      ->from(array('a' => 'tbl_gradesetup'), array('a.Pass', 'a.IdSubject'))
                      ->where('a.MinPoint <= ?', $larrResultDbs['fullcalculatedmarks'])
                      ->where('a.MaxPoint >= ?', $larrResultDbs['fullcalculatedmarks'])
                      ->where('a.IdSubject = ?', $larrResultDbs['IdSubject'])
                      ->where('a.IdProgram = ?', $lintidprogram)
                      ->where('a.IdSemester = ?', $larrResultDbs['IdSemestersyllabus']);
      $larrResultmarks = $lobjDbAdpt->fetchRow($lstrSelect);

      if ($larrResultmarks['Pass'] == '0') {
        $value = $larrResultmarks['IdSubject'];
        $values = $values . ',' . $value;
        $subjectssubjects = $values;
      }
    }

    $selectC = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_landscapeblocksemester"), array("a.*"))
                    ->join(array('b' => 'tbl_landscapeblocksubject'), 'a.blockid = b.blockid')
                    ->join(array('c' => 'tbl_subjectmaster'), 'b.subjectid = c.IdSubject')
                    ->join(array('d' => 'tbl_studentregistration'), 'a.semesterid = d.IdSemester', array())
                    ->where("b.subjectid in ($subjectssubjects)")
                    ->where('a.IdLandscape = ?', $lintidlandscape)
                    ->where('b.IdLandscape = ?', $lintidlandscape)
                    ->where('d.IdApplication = ?', $lintidapplicant);

//////////////////////////////////////////////////////////////////////////////////////////


    $select = $lobjDbAdpt->select()
                    ->union(array($selectA, $selectB, $selectC));

    if ($failedsubjects == 0) {
      $larrResult = $lobjDbAdpt->fetchAll($select);
    } elseif ($failedsubjects == 1) {
      $larrResult = $lobjDbAdpt->fetchAll($selectA);
    }
    return $larrResult;
  }

  public function fnAddStudentregistration($formData, $lintidapplicant) { //Function for adding the Program Branch details to the table
    unset($formData ['IdProgram']);
    unset($formData ['Save']);
    unset($formData ['subjects']);
    unset($formData ['selectall']);
    $formData ['IdApplication'] = $lintidapplicant;
    return $this->insert($formData);
  }

  public function fnUpdateStudentApplication($lintidapplicant) {  // function to update po details
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_studentapplication";
    $larrapplicant = array('Registered' => '1');
    $where = "IdApplication = '" . $lintidapplicant . "'";
    $db->update($table, $larrapplicant, $where);
  }

  public function fnAddStudentSubjectsperSemester($larrformData, $larrstudentreg) {  // function to insert po details
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_studentregsubjects";
    $countofsubjects = count($larrformData['subjects']);
    for ($i = 0; $i < $countofsubjects; $i++) {
      $larrformDatasubjects = explode("/", $larrformData['subjects'][$i]);
      $larrformData['subjects'][$i] = $larrformDatasubjects[0];
      $larrSubjects = array('IdStudentRegistration' => $larrstudentreg,
          'IdSubject' => $larrformData['subjects'][$i],
          'UpdDate' => $larrformData['UpdDate'],
          'UpdUser' => $larrformData['UpdUser'],
          'Active' => 1
      );

      $db->insert($table, $larrSubjects);
    }
  }

  function fnGenerateCode($idUniversity, $collageId, $page, $IdInserted) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $select = $db->select()
                    ->from('tbl_config')
                    ->where('idUniversity  = ?', $idUniversity);
    $result = $db->fetchRow($select);
    $sepr = $result[$page . 'Separator'];
    $str = $page . "CodeField";
    $CodeText = $page . "CodeText";
    for ($i = 1; $i <= 4; $i++) {
      $check = $result[$str . $i];
      $TextCode = $result[$CodeText . $i];
      switch ($check) {
        case 'Year':
          $code = date('Y');
          break;
        case 'Uniqueid':
          $code = $IdInserted;
          break;
        case 'College':
          $select = $db->select()
                          ->from('tbl_collegemaster')
                          ->where('IdCollege  = ?', $collageId);
          $resultCollage = $db->fetchRow($select);
          $code = $resultCollage['ShortName'];
          break;
        case 'University':
          $select = $db->select()
                          ->from('tbl_universitymaster')
                          ->where('IdUniversity  = ?', $idUniversity);
          $resultCollage = $db->fetchRow($select);
          $code = $resultCollage['ShortName'];
          break;
        case 'Text':
          $code = $TextCode;
          break;
        default:
          break;
      }
      if ($i == 1)
        $accCode = $code;
      else
        $accCode .= $sepr . $code;
    }
    return $accCode;
  }

  public function fnupdateRegistrationtCode($larrstudentreg, $registrationCode) {
    $larrformData['registrationId'] = $registrationCode;
    $where = 'IdStudentRegistration = ' . $larrstudentreg;
    $this->update($larrformData, $where);
  }

  public function fnGetSemesterNameList($idlandscape) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    /* 		$lstrSelect = $lobjDbAdpt->select()
      ->from(array('c' => 'tbl_oldlandsacpesemester'),array())
      ->join(array('a'=>'tbl_semester'),'a.IdSemester = c.semesterid',array("key"=>"a.IdSemester","value"=>"CONCAT_WS(' ',IFNULL(b.SemesterMasterName,''),IFNULL(a.year,''))"))
      ->join(array('b' => 'tbl_semestermaster'),'a.Semester = b.IdSemesterMaster',array())
      ->where('c.IdLandscape  = ?',$idlandscape)
      ->where('a.Active = 1')
      ->where('b.Active = 1')
      ->order("a.year"); */

    $lstrSelect = $lobjDbAdpt->select()
                    // ->from(array('c' => 'tbl_oldlandsacpesemester'),array())
                    ->from(array('a' => 'tbl_semester'), array("key" => "a.IdSemester", "value" => "CONCAT_WS(' ',IFNULL(b.SemesterMasterName,''),IFNULL(a.year,''))"))
                    ->join(array('b' => 'tbl_semestermaster'), 'a.Semester = b.IdSemesterMaster', array())
                    //->where('c.IdLandscape  = ?',$idlandscape)
                    ->where('a.Active = 1')
                    ->where('b.Active = 1')
                    ->order("a.year");
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function fnCheckExistingDisplinary($lintidapplicant) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $sql = $db->select()
                    ->from(array('a' => 'tbl_disciplinaryaction'), array('a.*'))
                    ->where('a.IdApplication  = ?', $lintidapplicant)
                    ->where('a.Approve = 1');
    $result = $db->fetchRow($sql);
    if ($result['IdDisciplinaryAction'] == "") {
      return 1;
    } else {
      $sqlquery = $db->select()
                      ->from(array('a' => 'tbl_disciplinaryactiondetails'), array('a.*'))
                      ->where('a.IdDisciplinaryAction  = ?', $result['IdDisciplinaryAction']);
      $results = $db->fetchRow($sqlquery);
      if ($results['iddisciplinayactiondetails'] == "") {
        return 0;
      } else {
        return 1;
      }
    }
  }

  public function fnCheckStatus($lintidapplicant, $semester) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $sql = $db->select()
                    ->from(array('a' => 'tbl_studentsemesterstatus'), array('a.*'))
                    ->join(array('b' => 'tbl_definationms'), 'a.studentsemesterstatus=b.idDefinition')
                    ->where('a.IdApplication = ?', $lintidapplicant)
                    ->where('a.idSemester = ?', $semester)
                    ->where('a.studentsemesterstatus = 168');
    $result = $db->fetchRow($sql);
    return $result;
  }

  public function fnCheckPayments($lintidapplicant, $semester) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $sql = $db->select()
                    ->from(array('a' => 'tbl_verifypayments'), array('a.*'))
                    ->where('a.IdApplication = ?', $lintidapplicant)
                    ->where('a.IdSemester = ?', $semester)
                    ->where('a.VerifyPayment = 1');
    $result = $db->fetchRow($sql);
    return $result;
  }

  public function getChargeNameDropDown() {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("sa" => "tbl_charges"), array("key" => "sa.IdCharges", "value" => "sa.ChargeName"));
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function fnGetChargeAmount($lintidcharge) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $sql = $db->select()
                    ->from(array('a' => 'tbl_charges'), array('a.*'))
                    ->where('a.IdCharges = ?', $lintidcharge);
    $result = $db->fetchRow($sql);
    return $result;
  }

  public function fnAddStudentCharges($larrformData, $larrstudentreg) {  // function to insert po details
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_studentregcharges";

    $countofsubjects = count($larrformData['IdCharge']);
    for ($i = 0; $i < $countofsubjects; $i++) {
      $larrcharges = array('IdStudentRegistration' => $larrstudentreg,
          'IdCharge' => $larrformData['IdCharge'][$i],
          'Charge' => $larrformData['chargepaid'][$i],
          'UpdDate' => $larrformData['UpdDate'],
          'UpdUser' => $larrformData['UpdUser']
      );
      $db->insert($table, $larrcharges);
    }
  }

  public function fnCheckExistingSemester($IdSemester, $lintidapplicant) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $sql = $db->select()
                    ->from(array('a' => 'tbl_studentregistration'), array('a.*'))
                    ->where('a.IdApplication  = ?', $lintidapplicant)
                    ->where('a.IdSemester = ?', $IdSemester);
    $result = $db->fetchRow($sql);
    return $result;
  }

  public function getProgramcharges($lintidprogram) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $currentdate = date('Y-m-d');
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array('a' => 'tbl_programcharges'), array('a.*'))
                    ->join(array('b' => 'tbl_charges'), 'a.IdCharges = b.IdCharges')
                    ->where('b.effectiveDate <= ?', $currentdate)
                    ->where('a.IdProgram = ?', $lintidprogram)
                    ->where('b.Payment = 1');
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
    return $larrResult;
  }

  public function fnGetSubjectprerequisitsvalidation($lintidsubject, $lintidapplicant, $takemarks) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    if ($takemarks == 0) {
      $select = 'SELECT w.*,SUM(w.totalsubmarks) as fullcalculatedmarks FROM
					(SELECT `a`.*, `e`.`idVerifierMarks`, `e`.`idverifier` , AVG(e.verifiresubjectmarks) AS `totalsubmarks` 
					FROM `tbl_subjectprerequisites` AS `a`
					INNER JOIN `tbl_studentregsubjects` AS `b` ON a.IdRequiredSubject = b.IdSubject
					INNER JOIN `tbl_studentregistration` AS `c` ON b.IdStudentRegistration = c.IdStudentRegistration
					INNER JOIN `tbl_subjectmarksentry` AS `d` ON c.IdStudentRegistration = d.IdStudentRegistration AND a.IdRequiredSubject= d.idSubject
					INNER JOIN `tbl_verifiermarks` AS `e` ON d.idSubjectMarksEntry = e.idSubjectMarksEntry WHERE (a.IdSubject = "' . $lintidsubject . '") AND (c.IdApplication    =  "' . $lintidapplicant . '")
					GROUP BY `e`.`idSubjectMarksEntry`) w 
					GROUP BY w.IdRequiredSubject';
    } else if ($takemarks == 1) {
      $select = $lobjDbAdpt->select()
                      ->from(array("a" => "tbl_subjectprerequisites"), array("a.*"))
                      ->join(array('b' => 'tbl_studentregsubjects'), 'a.IdRequiredSubject = b.IdSubject', array())
                      ->join(array('c' => 'tbl_studentregistration'), 'b.IdStudentRegistration = c.IdStudentRegistration', array())
                      ->join(array('d' => 'tbl_subjectmarksentry'), 'c.IdStudentRegistration = d.IdStudentRegistration AND a.IdRequiredSubject= d.idSubject', array())
                      ->join(array('e' => 'tbl_verifiermarks'), 'd.idSubjectMarksEntry = e.idSubjectMarksEntry', array('SUM(e.verifiresubjectmarks) as fullcalculatedmarks', 'e.idVerifierMarks', 'e.idverifier'))
                      ->where("e.Rank = 1 OR e.Rank = 0")
                      ->where('a.IdSubject = ?', $lintidsubject)
                      ->where('c.IdApplication = ?', $lintidapplicant)
                      ->group('d.idSubject');
    }
    $larrResult = $lobjDbAdpt->fetchAll($select);
    return $larrResult;
  }

  public function fnGetSubjectMarksvalidation($idsubject, $lintidprogram, $submarks, $lintidsemsyllabus) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array('a' => 'tbl_gradesetup'), array('a.Pass'))
                    ->where('a.MinPoint <= ?', $submarks)
                    ->where('a.MaxPoint >= ?', $submarks)
                    ->where('a.IdSubject = ?', $idsubject)
                    ->where('a.IdProgram = ?', $lintidprogram)
                    ->where('a.IdSemester = ?', $lintidsemsyllabus);
    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
    return $larrResult;
  }

  public function fnCheckgpacalculation($lintidapplicant, $IdProgram, $idvirtualsemester) {
    $consistantresult = 'SELECT MAX(i.IdStudentRegistration)  from tbl_studentregistration i where i.IdApplication = "' . $lintidapplicant . '"';



    $db = Zend_Db_Table::getDefaultAdapter();

    $sql1 = $db->select()
                    ->from(array('a' => 'tbl_gpacalculation'), array('MAX(a.Idgpacalculation) as Idgpacalculation'))
                    ->where("a.IdStudentRegistration = ?", new Zend_Db_Expr('(' . $consistantresult . ')'));
    $result1 = $db->fetchRow($sql1);

    if ($result1['Idgpacalculation'] == "" || $result1['Idgpacalculation'] == NULL) {
      $Idgpacalculation = 0;
    } else {
      $Idgpacalculation = $result1['Idgpacalculation'];
    }

    $sql = $db->select()
                    ->from(array('a' => 'tbl_gpacalculation'), array('a.Gpa'))
                    ->where("a.Idgpacalculation = ?", $Idgpacalculation)
                    ->where("a.IdStudentRegistration = ?", new Zend_Db_Expr('(' . $consistantresult . ')'));
    $result = $db->fetchRow($sql);

    if ($result['Gpa'] == "") {
      return $result;
    } else {
      $sql = $db->select()
                      ->from(array('a' => 'tbl_subjectregistration'), array('a.TerminateStatus'))
                      ->where('a.Min <= ?', $result['Gpa'])
                      ->where('a.Max >= ?', $result['Gpa'])
                      ->where('a.AcademicStatus = 0')
                      ->where('a.VirtualSemester= ?', $idvirtualsemester)
                      ->where('a.IdProgram = ?', $IdProgram);
      $result = $db->fetchRow($sql);
      return $result;
    }
  }

  public function fnGetSubjectMinCRHours($lintidsubject) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array('a' => 'tbl_subjectmaster'), array('a.MinCreditHours'))
                    ->where('a.IdSubject = ?', $lintidsubject)
                    ->where('a.Active = 1');
    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
    return $larrResult;
  }

  public function fnGetRegisteredSubjectCRHours($lintidapplicant, $lintidprogram, $takemarks) {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    /////////////////////////////////////////////////////////////////////////////////////////
    if ($takemarks == '0') {
      $selectdb = 'SELECT w.*,SUM(w.totalsubmarks) as fullcalculatedmarks FROM
						(SELECT `b`.`IdSubject`,`c`.`IdSemestersyllabus`, AVG(e.verifiresubjectmarks) AS `totalsubmarks` 
						FROM  `tbl_studentregsubjects` AS `b`
						INNER JOIN `tbl_studentregistration` AS `c` ON b.IdStudentRegistration = c.IdStudentRegistration
						INNER JOIN `tbl_subjectmarksentry` AS `d` ON c.IdStudentRegistration = d.IdStudentRegistration AND b.IdSubject= d.idSubject
						INNER JOIN `tbl_verifiermarks` AS `e` ON d.idSubjectMarksEntry = e.idSubjectMarksEntry WHERE (c.IdApplication    =  "' . $lintidapplicant . '")
						GROUP BY `e`.`idSubjectMarksEntry`) w 
						GROUP BY w.IdSubject';
    } elseif ($takemarks == '1') {
      $selectdb = $lobjDbAdpt->select()
                      ->from(array('b' => 'tbl_studentregsubjects'), array('b.IdSubject'))
                      ->join(array('c' => 'tbl_studentregistration'), 'b.IdStudentRegistration = c.IdStudentRegistration', array('c.IdSemestersyllabus'))
                      ->join(array('d' => 'tbl_subjectmarksentry'), 'c.IdStudentRegistration = d.IdStudentRegistration AND b.IdSubject= d.idSubject', array())
                      ->join(array('e' => 'tbl_verifiermarks'), 'd.idSubjectMarksEntry = e.idSubjectMarksEntry', array('SUM(e.verifiresubjectmarks) as fullcalculatedmarks'))
                      ->where("e.Rank = 1 OR e.Rank = 0")
                      ->where('c.IdApplication = ?', $lintidapplicant)
                      ->group('d.idSubject');
    }

    $larrResultDb = $lobjDbAdpt->fetchAll($selectdb);

    $subjectssubjects = 0;
    $values = 0;
    foreach ($larrResultDb as $larrResultDbs) {
      $lstrSelect = $lobjDbAdpt->select()
                      ->from(array('a' => 'tbl_gradesetup'), array('a.Pass', 'a.IdSubject'))
                      ->where('a.MinPoint <= ?', $larrResultDbs['fullcalculatedmarks'])
                      ->where('a.MaxPoint >= ?', $larrResultDbs['fullcalculatedmarks'])
                      ->where('a.IdSubject = ?', $larrResultDbs['IdSubject'])
                      ->where('a.IdProgram = ?', $lintidprogram)
                      ->where('a.IdSemester = ?', $larrResultDbs['IdSemestersyllabus']);
      $larrResultmarks = $lobjDbAdpt->fetchRow($lstrSelect);

      if ($larrResultmarks['Pass'] == '1') {
        $value = $larrResultmarks['IdSubject'];
        $values = $values . ',' . $value;
        $subjectssubjects = $values;
      }
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    $lstrSelect = $lobjDbAdpt->select()
                    ->from(array('a' => 'tbl_studentregistration'), array(''))
                    ->join(array('b' => 'tbl_studentregsubjects'), 'a.IdStudentRegistration = b.IdStudentRegistration', array(''))
                    ->join(array('c' => 'tbl_subjectmaster'), 'b.IdSubject = c.IdSubject', array('SUM(CreditHours) as Totalcredithours'))
                    ->where("b.IdSubject in ($subjectssubjects)")
                    ->where('a.IdApplication = ?', $lintidapplicant);
    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
    return $larrResult;
  }

  
  	/*yatie*/
	public function getData($id) {
 	   $select = $this->lobjDbAdpt->select()
                ->from(array('sr'=>'tbl_studentregistration')) 
                ->join(array('l'=>'tbl_landscape'),'l.IdLandscape=sr.IdLandscape')
                ->where("sr.IdStudentRegistration = ?",$id);        
       $result = $this->lobjDbAdpt->fetchRow($select);
       return $result;
  }
  
  
  	public function searchSeniorStudent() {
  	
	 	  $select = $this->lobjDbAdpt->select()
	                ->from(array('a'=>'tbl_studentregistration'),array('a.registrationId','a.IdStudentRegistration'))
	                ->joinLeft(array("p"=>"applicant_profile"),'p.appl_id=a.IdApplication',array('appl_fname','appl_mname','appl_lname'))
	                ->joinLeft(array('i' => 'tbl_intake'),'a.IdIntake = i.IdIntake',array("i.IntakeDesc"))
	                ->joinLeft(array('pr'=>'tbl_program'),'pr.IdProgram = a.IdProgram',array("ArabicName","ProgramCode"))
	                ->joinLeft(array('c'=>'tbl_semester'),'c.IdSemester = a.IdSemestersyllabus',array("c.SemesterCode AS semestername"))
                	->joinLeft(array('d'=>'tbl_semestermaster'),'d.IdSemesterMaster=a.IdSemestersyllabus',array("d.SemesterMainCode AS mainsemestername"))
	                ->where("profileStatus = 92")
	                ->order("p.appl_fname");
	       $result = $this->lobjDbAdpt->fetchAll($select);
	       return $result;
  	}
  
	 public function searchSeniorStudentPaginate($post = array()) { 
	
		    $lstrSelect = $this->lobjDbAdpt->select()
		                ->from(array('a'=>'tbl_studentregistration'),array('a.FName','a.LName','a.MName','a.registrationId','a.IdStudentRegistration'))
		 				->joinLeft(array("p"=>"applicant_profile"),'p.appl_id=a.IdApplication',array('appl_fname','appl_mname','appl_lname'))
						->joinLeft(array('i' => 'tbl_intake'),'a.IdIntake = i.IdIntake',array("i.IntakeDesc"))
		 				->joinLeft(array('pr'=>'tbl_program'),'pr.IdProgram = a.IdProgram',array("ArabicName","ProgramCode"))
		 				->joinLeft(array('c'=>'tbl_semester'),'c.IdSemester = a.IdSemestersyllabus',array("c.SemesterCode AS semestername"))
                		->joinLeft(array('d'=>'tbl_semestermaster'),'d.IdSemesterMaster=a.IdSemestersyllabus',array("d.SemesterMainCode AS mainsemestername"))
                		->where("profileStatus = 92")
		                ->order("p.appl_fname");
		               
		
		    if (isset($post['field1']) && !empty($post['field1'])) {
		      $lstrSelect = $lstrSelect->where("a.IdProgram = ?", $post['field1']);
		    }
		
		    if (isset($post['field2']) && !empty($post['field2'])) {
		      $lstrSelect = $lstrSelect->where('a.registrationId like "%" ? "%"', $post['field2']);
		    }
		
		    if (isset($post['field6']) && !empty($post['field6'])) {
		        $lstrSelect->where("(p.appl_fname LIKE '%". $post['field6']."%'");
		        $lstrSelect->orwhere("p.appl_mname LIKE '%". $post['field6']."%'");
		        $lstrSelect->orwhere("p.appl_lname LIKE '%". $post['field6']."%')");
		    }
		
		    if (isset($post['field12']) && !empty($post['field12'])) {
		      $lstrSelect = $lstrSelect->where('a.IdBranch like "%" ? "%"', $post['field12']);
		    }
		
		    if (isset($post['field8']) && !empty($post['field8'])) {
		      $lstrSelect = $lstrSelect->where('a.IdIntake like "%" ? "%"', $post['field8']);
		    }
		
		    if (isset($post['field27']) && !empty($post['field27'])) {
		      // $where = "b.idSemester = '".$post['field27']."' OR b.IdSemesterMain = '".$post['field27']."'";
		      $lstrSelect = $lstrSelect->where("a.idSemester = ?",$post['field27']);
		    }
		
		    if (isset($post['field5']) && !empty($post['field5'])) {		     
		      $lstrSelect = $lstrSelect->where("pr.IdScheme = ?", $post['field5']);
		    }
		    
		    //echo $lstrSelect;
		
		    $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;
	  }
  

}

?>