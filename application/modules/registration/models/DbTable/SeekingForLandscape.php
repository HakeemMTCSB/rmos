<?php 
class Registration_Model_DbTable_SeekingForLandscape extends Zend_Db_Table_Abstract{
    
    protected $_name = 'tbl_studentregistration';
    
    public function getStudent(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->where('a.IdLandscape = 0 OR a.IdLandscape is null');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function updateStudent($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregistration', $bind, 'IdStudentRegistration = '.$id);
        return $update;
    }
    
	public function updateApplicant($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('applicant_transaction', $bind, 'at_trans_id = '.$id);
        return $update;
    }
    
    public function getTheStudentRegistrationDetail($id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array('a' => 'tbl_studentregistration'), array('a.registrationId', 'a.IdProgram','at_pes_id'=>'a.registrationId','transaction_id','IdApplication','*'))
            ->joinLeft(array('StudentProfile' => 'student_profile'), "a.sp_id = StudentProfile.id", array("CONCAT_WS(' ',IFNULL(StudentProfile.appl_fname,''),IFNULL(StudentProfile.appl_mname,''),IFNULL(StudentProfile.appl_lname,'')) as name",'StudentProfile.*' ))
            ->joinLeft(array('b' => 'tbl_program'), "a.IdProgram = b.IdProgram", array("b.ProgramName", "b.IdScheme"))
            ->joinLeft(array('c' => 'tbl_intake'), "a.IdIntake = c.IdIntake", array("c.IntakeDesc"))
            ->joinLeft(array('d' => 'tbl_definationms'), "a.profileStatus = d.idDefinition", array("d.DefinitionDesc"))
            ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=a.IdProgramScheme', array())
            ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc','ProgramModeMy'=>'BahasaIndonesia'))
            ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc','StudyModeMy'=>'BahasaIndonesia'))
            ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc','ProgramTypeMy'=>'BahasaIndonesia'))
            ->joinLeft(array("fs" => "fee_structure"),'fs.fs_id=a.fs_id', array('fs_name'))
            ->joinLeft(array('o'=>'student_financial'), 'a.IdStudentRegistration = o.sp_id')
            ->joinLeft(array('u'=>'tbl_definationms'), 'o.af_method = u.idDefinition', array('afMethodName'=>'u.DefinitionDesc'))
            ->joinLeft(array('v'=>'tbl_definationms'), 'o.af_type_scholarship = v.idDefinition', array('typeScholarshipName'=>'v.DefinitionDesc'))
            ->joinLeft(array('w'=>'tbl_scholarship_sch'), 'o.af_scholarship_apply = w.sch_Id', array('applScholarshipName'=>'w.sch_name'))
            ->joinLeft(array("dc" => "tbl_definationms"),'dc.idDefinition=StudentProfile.appl_category', array('StudentCategory'=>'DefinitionDesc'))
            ->joinLeft(array("st" => "tbl_definationms"),'st.idDefinition=a.profileStatus', array('StatusName'=>'DefinitionDesc'))
            ->where("a.IdStudentRegistration =?", $id);
        
        $result = $db->fetchRow($sql);
        return $result;
    }
    
    public function getStudentByProgramScheme($scheme=11){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.Idprogram = b.IdProgram')
            ->joinLeft(array('c'=>'student_profile'), 'a.sp_id = c.id')
            ->where('a.IdStudentRegistration IN (SELECT b.IdStudentRegistration FROM tbl_studentregsubjects as b WHERE b.migrate_by=589 GROUP BY b.IdStudentRegistration)')
            ->where('b.IdScheme = ?', $scheme);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSubjectRegMoreThenTwo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('a.IdSubject'))
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('a.exam_status <> "CT" AND a.exam_status <> "EX" AND a.exam_status <> "U"')
            ->group('a.IdSubject')
            ->having('count(a.IdSubject) > 1');
        ///echo $select; exit;
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function detectRepeatSubject($studId, $subId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_semestermaster'), 'a.IdSemesterMain = b.IdSemesterMaster')
            ->joinLeft(array('c'=>'tbl_subjectmaster'), 'a.IdSubject = c.IdSubject')
            ->where('a.IdStudentRegistration = ?', $studId)
            ->where('a.IdSubject = ?', $subId)
            ->order('b.SemesterMainStartDate ASC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function updateStatusSubject($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregsubjects', $bind, 'IdStudentRegSubjects = '.$id);
        return $update;
    }
    
    public function getStudentDontHaveFsID(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'student_profile'), 'a.sp_id = b.id')
            ->where('a.fs_id = 0 OR a.fs_id IS NULL');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
	public function getApplicantDontHaveFsID(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_transaction'), array('value'=>'*','IdIntake'=>'a.at_intake'))
            ->joinLeft(array('ba'=>'applicant_profile'), 'ba.appl_id = a.at_appl_id')
            ->joinLeft(array('b'=>'applicant_program'), 'a.at_trans_id = b.ap_at_trans_id',array('IdProgram'=>'b.ap_prog_id','IdProgramScheme'=>'b.ap_prog_scheme'))
            ->where('a.at_fs_id = 0 OR a.at_fs_id IS NULL')
            ->where('a.at_status != 591');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getApplicantFeeStructure( $program_id, $scheme, $intake_id, $student_category=579,$throw=1){
		
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('fs'=>'fee_structure'))
            ->join( array('fsp'=>'fee_structure_program'), 'fsp.fsp_fs_id = fs.fs_id and fsp.fsp_program_id = '.$program_id.' and fsp.fsp_idProgramScheme = '.$scheme)
            ->where("fs.fs_intake_start = '".$intake_id."'")
            ->where("fs.fs_student_category = '".$student_category."'");
        //echo $selectData; exit;
        $row = $db->fetchRow($selectData);

        if(!$row){

            $ex_msg = "No Fee Structure setup for program(".$program_id."), scheme(".$scheme."), intake(".$intake_id."), category(".$student_category.")";

            if ($throw==1){
                throw new Exception($ex_msg);
            }else{
                return false;
            }

        }else{
            return $row;
        }
    }
    
    public function selectStudentBarring(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'barringmigrate'), array('value'=>'*'));
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getStudentInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.IdProgram = b.IdProgram', array('schemeid'=>'b.IdScheme'))
            ->where('a.registrationId = ?', $id)
            ->where('a.profileStatus = ?', 92);
        
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getStudentInfo2($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.IdProgram = b.IdProgram', array('schemeid'=>'b.IdScheme'))
            ->where('a.IdStudentRegistration = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getCurrentSem($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $id)
            ->where('a.SemesterMainStartDate <= ?', date('Y-m-d'))
            ->where('a.SemesterMainEndDate >= ?', date('Y-m-d'));
        //echo $select; exit;
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function insertBarring($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_barringrelease', $bind);
        $id = $db->lastInsertId('tbl_barringrelease', 'tbr_id');
        return $id;
    }
    
    public function getActiveStudent(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.IdProgram = b.IdProgram', array('schemeid'=>'b.IdScheme'))
            ->where('a.profileStatus = ?', 92);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function checkRegisterSubject($studId, $semId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->where('a.IdStudentRegistration = ?', $studId)
            ->where('a.IdSemesterMain = ?', $semId)
            ->where('a.Active != 3');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function checkSemesterStatus($studId, $semId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'), array('value'=>'*'))
            ->where('a.IdStudentRegistration = ?', $studId)
            ->where('a.IdSemesterMain = ?', $semId);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function insertSemesterStatus($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_studentsemesterstatus', $bind);
        $id = $db->lastInsertId('tbl_studentsemesterstatus', 'idstudentsemsterstatus');
        return $id;
    }
    
    public function updateSemesterStatus($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentsemesterstatus', $bind, 'idstudentsemsterstatus = '.$id);
        return $update;
    }
    
    public function updateStudentReg($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregistration', $bind, 'IdStudentRegistration = '.$id);
        return $update;
    }
    
    public function getDifferStudent($status=92, $table='testingizham'){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'testingizham3'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.idstudent = b.registrationId AND a.program = b.IdProgram')
            ->joinLeft(array('c'=>'tbl_program'), 'b.IdProgram=c.IdProgram')
            //->where('a.profileStatus = ?', $status)
            ->order('b.IdStudentRegistration');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getLastSemester($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentsemesterstatus'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_semestermaster'), 'a.idSemester = b.IdSemesterMaster', array('b.SemesterMainName'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.studentsemesterstatus = c.idDefinition', array('c.DefinitionDesc'))
            ->where('a.IdStudentRegistration = ?', $id)
            ->order('b.SemesterMainStartDate DESC');
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getRepeatSubject($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_semestermaster'), 'a.IdSemesterMain = b.IdSemesterMaster')
            ->joinLeft(array('c'=>'exam_registration'), 'a.IdSemesterMain = c.er_idSemester AND a.IdStudentRegistration = c.er_idStudentRegistration AND a.IdSubject = c.er_idSubject')
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('a.Active IN (4, 9, 6, 10)')
            ->where('c.er_attendance_status != ?', 396)
            ->order('b.SemesterMainStartDate ASC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getStudentToUpdateCtGred(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_migration_ct'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.studentId = b.registrationId AND a.IdProgram = b.IdProgram')
            ->join(array('c'=>'tbl_studentregsubjects'), 'b.IdStudentRegistration=c.IdStudentRegistration AND a.IdSubject = c.IdSubject');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function updateGred($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregsubjects', $bind, 'IdStudentRegSubjects = '.$id);
        return $update;
    }
    
    public function getApplicantChangeMode(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_changemodeapplication'), array('*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.cma_studregid=b.IdStudentRegistration')
            ->where('a.cma_status = ?', 841);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getNewLandscape($programId, $progSchemId, $idIntake){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_landscape'))
            ->where('a.IdProgram = ?', $programId)
            ->where('a.IdProgramScheme = ?', $progSchemId)
            ->where('a.IdStartSemester = ?', $idIntake);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getStudentInitialCourse(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.IdProgram=b.IdProgram')
            ->where('a.IdStudentRegistration = ?', 5001);
            //->where('a.IdIntake = ?', 70)
            //->where('a.IdProgram = ?', 2);
            //->where('a.IdProgramScheme = ?', 5);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function checkInitialCourse($studentid, $subjectid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->where('a.IdStudentRegistration = ?', $studentid)
            ->where('a.IdSubject = ?', $subjectid);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getCompleteList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'complete_student'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.studentid = b.registrationId AND a.programid = b.IdProgram')
            ->joinLeft(array('c'=>'tbl_program'), 'a.programid=c.IdProgram');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getHosMigDat(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'hostelmigration'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.studentid=b.registrationId AND a.programid=b.IdProgram');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function insertHostelMigrate($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_hostel_history', $bind);
        $id = $db->lastInsertId('tbl_hostel_history', 'IdHistory');
        return $id;
    }
    
    public function getInvoiveToCn(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_credittransfer'), array('value'=>'*'))
            ->join(array('b'=>'tbl_credittransfersubjects'), 'a.IdCreditTransfer = b.IdCreditTransfer')
            ->where('a.ApplicationStatus = ?', 785)
            ->where('b.invoiceid_0 <> ?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function checkCreditNote($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'credit_note'), array('value'=>'*'))
            ->where('a.cn_invoice_id = ?', $id)
            ->where('a.cn_status = ?', 'A');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getAppCourse(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_application_course'), array('value'=>'*'))
            ->join(array('b'=>'sponsor_application'), 'a.sac_said=b.sa_id')
            ->where('a.sac_regsubid = ?', 0);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getRegSubject($id, $subid, $semid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('a.IdSubject = ?', $subid)
            ->where('a.IdSemesterMain = ?', $semid);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function updateRegSubId($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('sponsor_application_course', $bind, 'sac_id = '.$id);
    }
    
    public function getReAppCourse(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_reapplication_course'), array('value'=>'*'))
            ->join(array('b'=>'sponsor_reapplication'), 'a.src_sreid=b.sre_id')
            ->where('a.src_regsubid = ?', 0);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function updateRegSubId2($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('sponsor_reapplication_course', $bind, 'src_id = '.$id);
    }
    
    public function getSchemeList(){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>"tbl_scheme"),array("a.*"));
        
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }
    
    public function getSemesterByScheme($scheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $scheme)
            ->where('a.display = ?', 1);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getStudentTerNotComplete($semester){
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $selectData1 = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'))
            ->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.IdStudentRegistration',array('registrationId','IdProgram','IdStudentRegistration','IdProgramScheme','IdBranch'))
            ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = srs.IdProgramScheme',array('mode_of_program'))
            ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
            ->where('a.Active NOT IN (3)')
            ->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
            ->where("IFNULL(a.exam_status,'x') NOT IN ('CT','EX','U')")
            ->where('a.IdSemesterMain = ?', $semester)
            ->where('a.IdCourseTaggingGroup <> ?', 0)
            ->where('a.IdCourseTaggingGroup IS NOT NULL');

        $selectData2 = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'))
            ->joinLeft(array('srs'=>'tbl_studentregistration'),'srs.IdStudentRegistration = a.IdStudentRegistration',array('registrationId','IdProgram','IdStudentRegistration','IdProgramScheme','IdBranch'))
            ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = srs.IdProgramScheme',array('mode_of_program'))
            ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))  
            ->where('a.audit_program IS NOT NULL')
            ->where('a.Active NOT IN (3)')
            ->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
            ->where("IFNULL(a.exam_status,'x') IN ('U')")
            ->where('a.IdSemesterMain = ?', $semester)
            ->where('a.IdCourseTaggingGroup <> ?', 0)
            ->where('a.IdCourseTaggingGroup IS NOT NULL');

        $select = $db->select()
            ->union(array($selectData1, $selectData2));

        $result = $db->fetchAll($select);	
        return $result;
    }
    
    public function insertTerNotComplete($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_ternotcomplete', $bind);
        $id = $db->lastInsertId('tbl_ternotcomplete', 'tnc_id');
        return $id;
    }
    
    public function truncateTerNotComplete(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $truncate = $db->query('TRUNCATE TABLE tbl_ternotcomplete');
        return $truncate;
    }
    
    public function getTerNotComplete(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_ternotcomplete'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.tnc_regid = b.IdStudentRegistration')
            ->joinLeft(array('c'=>'tbl_studentregsubjects'), 'a.tnc_subregid = c.IdStudentRegSubjects')
            ->joinLeft(array('d'=>'student_profile'), 'b.sp_id = d.id')
            ->joinLeft(array('e'=>'tbl_program'), 'b.IdProgram = e.IdProgram')
            ->joinLeft(array('f'=>'tbl_program_scheme'), 'b.IdProgramScheme = f.IdProgramScheme')
            ->joinLeft(array('g'=>'tbl_definationms'), 'f.mode_of_program = g.idDefinition', array('mop'=>'g.DefinitionDesc'))
            ->joinLeft(array('h'=>'tbl_definationms'), 'f.mode_of_study = h.idDefinition', array('mos'=>'h.DefinitionDesc'))
            ->joinLeft(array('i'=>'tbl_definationms'), 'f.program_type = i.idDefinition', array('pt'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_subjectmaster'), 'c.IdSubject = j.IdSubject')
            ->joinLeft(array('k'=>'tbl_course_tagging_group'), 'c.IdCourseTaggingGroup = k.IdCourseTaggingGroup');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    
	public function repeatSubject($studentLoop){
    	  	$landscapeSubjectDb =  new GeneralSetup_Model_DbTable_Landscapesubject();
    	  	$recordDb = new Records_Model_DbTable_Academicprogress(); 
    	  
    				$sublist = $this->getSubjectRegMoreThenTwo($studentLoop['IdStudentRegistration']);

                    if ($sublist){
                        foreach ($sublist as $subloop){
                            $equivid = false;
                            $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentLoop['IdProgram'],$studentLoop['IdLandscape'],$subloop['IdSubject']);
                            if(!$course_status){
                                $equivid=$recordDb->checkEquivStatus($studentLoop['IdProgram'],$subloop['IdSubject']);

                                if($equivid){
                                    $course_status = $landscapeSubjectDb->getCommonSubjectStatus($studentLoop['IdProgram'],$studentLoop['IdLandscape'],$equivid);
                                }
                            }
                            
                            if ($course_status['DefinitionDesc'] != "Research"){
                                $subject = $this->detectRepeatSubject($studentLoop['IdStudentRegistration'], $subloop['IdSubject']);
                                if (count($subject)>1){
                                    $k=1;
                                    foreach ($subject as $subjectLoop){
                                        
                                        if ($k != 1){
                                            $data = array(
                                                'Active'=>4,
                                                'cgpa_calculation'=>1
                                            );
                                            
                                           
                                           $this->updateStatusSubject($data, $subjectLoop['IdStudentRegSubjects']);
                                        }else{
                                            $data = array(
                                                'Active'=>1,
                                                'cgpa_calculation'=>1
                                            );
                                            
                                           $this->updateStatusSubject($data, $subjectLoop['IdStudentRegSubjects']);
                                        }
                                        $k++;
                                    }
                                }
                                
                            }
                        }
                    }
    }
    
    
    public function updateRepeatCalculation($studentLoop){
    	
    				$reSubList = $this->getRepeatSubject($studentLoop['IdStudentRegistration']);

                    if ($reSubList){
                        
                        if (count($reSubList) > 0){
                            $k=0;
                            foreach ($reSubList as $reSubLoop){
                                if ($k >= 2){
                                    $data = array(
                                        'cgpa_calculation'=>0
                                    );
                                   
                                  $this->updateStatusSubject($data, $reSubLoop['IdStudentRegSubjects']);
                                }else{
                                    $data = array(
                                        'cgpa_calculation'=>1
                                    );
                                   
                                  $this->updateStatusSubject($data, $reSubLoop['IdStudentRegSubjects']);
                                }
                                $k++;
                               
                            }
                        }
                    }
    }
    
    public function getStaffList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_staffmaster'), array('value'=>'*'));
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function updateStaff($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_staffmaster', $bind, 'IdStaff = '.$id);
        return $update;
    }
    
    public function getGroup(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=a.IdSubject',array('subject_code'=>'SubCode','subject_name'=>'SubjectName','faculty_id'=>'IdFaculty','subjectMainDefaultLanguage'))
            ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=a.IdSemester',array('semester_name'=>'SemesterMainName','AcademicYear'))
            ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=a.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
            ->joinLeft(array('branch'=>'tbl_branchofficevenue'), 'a.IdBranch=branch.IdBranch', array('branch.BranchName'))
            ->where('IdSemester = 8 or IdSemester = 58');
            //->where('IdCourseTaggingGroup = ?', 1961);
        //echo $select; exit;
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getGroupProgram($groupid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_program'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_program'), 'a.program_id=b.IdProgram')
            ->joinLeft(array('c'=>'tbl_program_scheme'), 'a.program_scheme_id = c.IdProgramScheme')
            ->joinLeft(array('d'=>'tbl_definationms'), 'c.mode_of_program=d.idDefinition', array('mop'=>'d.DefinitionDesc'))
            ->joinLeft(array('e'=>'tbl_definationms'), 'c.mode_of_study=e.idDefinition', array('mos'=>'e.DefinitionDesc'))
            ->joinLeft(array('f'=>'tbl_definationms'), 'c.program_type=f.idDefinition', array('pt'=>'f.DefinitionDesc'))
            ->joinLeft(array('g'=>'tbl_scheme'), 'b.IdScheme=g.IdScheme')
            ->where('a.group_id = ?', $groupid);
        //echo $select; exit;
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getGroupSchedule($groupid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_schedule'), array('value'=>'*'))
            ->where('a.idGroup = ?', $groupid);
        //echo $select.';<br/>'; //exit;
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getExApp($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_credittransfer'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_credittransfersubjects'), 'a.IdCreditTransfer = b.IdCreditTransfer')
            ->where('a.IdStudentRegistration = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getStudentComplete(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->where('a.profileStatus = ?', 248);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function insertPreGraduationList($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('pregraduate_list', $data);
        $id = $db->lastInsertId('pregraduate_list', 'id');
        return $id;
    }

    public function getAppCourseNoReceipt(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_application_course'), array('value'=>'*'))
            ->joinLeft(array('b'=>'sponsor_application'), 'a.sac_said = b.sa_id')
            ->joinLeft(array('c'=>'tbl_subjectmaster'), 'a.sac_subjectid = c.IdSubject')
            ->where('b.sa_semester_id = 8 or b.sa_semester_id = 58')
            ->where('a.sac_receiptid = ?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getReAppCourseNoReceipt(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_reapplication_course'), array('value'=>'*'))
            ->joinLeft(array('b'=>'sponsor_reapplication'), 'a.src_sreid = b.sre_id')
            ->joinLeft(array('c'=>'tbl_subjectmaster'), 'a.src_subjectid = c.IdSubject')
            ->where('b.sre_semester_id = 8 or b.sre_semester_id = 58')
            ->where('a.src_receiptid = ?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAppCourseNoReceiptCancel(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_application_course'), array('value'=>'*'))
            ->joinLeft(array('b'=>'sponsor_application'), 'a.sac_said = b.sa_id')
            ->joinLeft(array('c'=>'tbl_subjectmaster'), 'a.sac_subjectid = c.IdSubject')
            ->where('b.sa_semester_id != ?', 8)
            ->where('b.sa_semester_id != ?', 58)
            ->where('a.sac_receiptid != ?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getReAppCourseNoReceiptCancel(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_reapplication_course'), array('value'=>'*'))
            ->joinLeft(array('b'=>'sponsor_reapplication'), 'a.src_sreid = b.sre_id')
            ->joinLeft(array('c'=>'tbl_subjectmaster'), 'a.src_subjectid = c.IdSubject')
            ->where('b.sre_semester_id != ?', 8)
            ->where('b.sre_semester_id != ?', 58)
            ->where('a.src_receiptid != ?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function viewApp($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectData = $db->select()
            ->from(array('pm'=>'sponsor_application'))
            ->joinLeft(array('b'=>'sponsor_application_details'),'b.application_id=pm.sa_id')
            ->joinLeft(array('c'=>'tbl_studentregistration'),'pm.sa_cust_id=c.IdStudentRegistration')
            ->joinLeft(array('d'=>'student_profile'),'c.sp_id=d.id')
            ->joinLeft(array('e'=>'tbl_program'),'c.IdProgram=e.IdProgram')
            ->joinLeft(array('f'=>'tbl_qualificationmaster'), 'b.edulevel = f.IdQualification', array('f.QualificationLevel'))
            ->joinLeft(array('g'=>'tbl_semestermaster'), 'pm.sa_semester_id = g.IdSemesterMaster')
            ->where("pm.sa_id = ?", (int)$id);


        $row = $db->fetchRow($selectData);
        return $row;
    }

    public function getReApplication($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_reapplication'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.sre_cust_id=b.IdStudentRegistration')
            ->joinLeft(array('c'=>'student_profile'), 'b.sp_id=c.id')
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram=d.IdProgram')
            ->joinLeft(array('e'=>'tbl_semestermaster'), 'a.sre_semester_id = e.IdSemesterMaster')
            ->where('a.sre_id = ?', $id)
            ->where('a.sre_cust_type = 1');

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getSponsorInvReceipt($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_invoice_receipt'))
            ->where('a.rcp_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function sponsorInvReceiptInv($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_invoice_receipt_invoice'))
            ->where('a.rcp_inv_rcp_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function sponsorInvDetail($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_invoice_detail'))
            ->where('a.id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function invoiceDetail($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_detail'))
            ->where('a.id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function invoiceMain($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main'))
            ->where('a.id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateInvoiceDetail($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('invoice_detail', $data, 'id = '.$id);
        return $update;
    }

    public function updateInvoiceMain($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('invoice_main', $data, 'id = '.$id);
        return $update;
    }

    public function updateSponsorMain($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('sponsor_invoice_main', $data, 'sp_id = '.$id);
        return $update;
    }

    public function updateReceiptMain($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('sponsor_invoice_receipt', $data, 'rcp_id = '.$id);
        return $update;
    }

    public function updateAdvancePayment($data, $rcpid){
        $db = Zend_Db_Table::getDefaultAdapter();

        $update = $db->update('advance_payment', $data, 'advpy_idStudentRegistration IN (SELECT IdStudentRegistration FROM sponsor_invoice_receipt_student WHERE rcp_inv_rcp_id = '.$rcpid.') AND advpy_rcp_id = '.$rcpid);
        return $update;
    }

    public function getApplicant(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'applicant_transaction'), array('value'=>'*'))
            ->join(array('b'=>'applicant_program'), 'a.at_trans_id = b.ap_at_trans_id')
            ->where('a.IdLandscape IS NULL');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAppLandscapeInfo($program_id='',$program_scheme_id='',$intake=''){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_landscape'), array('value'=>'*'))
            ->where('a.IdProgram = ?',$program_id)
            ->where('a.IdProgramScheme = ?',$program_scheme_id)
            ->where('a.IdStartSemester = ?',$intake);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateTrans($id, $data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('applicant_transaction', $data, 'at_trans_id = '.$id);
        return $update;
    }

    public function getSubjectItem(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects_detail'), array('value'=>'*'))
            ->order('regsub_id');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSubjectItem2($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects_detail'), array('value'=>'*'))
            ->where('a.invoice_id IS NOT NULL')
            ->where('a.regsub_id = ?', $id)
            ->order('regsub_id');

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getSubjectMain($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->where('a.IdStudentRegSubjects = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getInvoiceMain($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main'), array('value'=>'*'))
            ->where('a.id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getInvoiceDetails($invid, $fsid, $item){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_detail'), array('value'=>'*'))
            ->join(array('b'=>'fee_structure_item'), 'a.fi_id = b.fsi_item_id')
            ->where('a.invoice_main_id = ?', $invid)
            ->where('b.fsi_structure_id = ?', $fsid)
            ->where('b.fsi_registration_item_id = ?', $item);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateItem($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregsubjects_detail', $data, 'id = '.$id);
        return $update;
    }

    public function getCeMigrate(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'migrate_ce'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.studentid = b.registrationId')
            ->join(array('c'=>'tbl_program'), 'b.IdProgram = c.IdProgram')
            ->where('b.IdProgram = ?', 3);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getCeSemester($year, $month, $scheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.AcademicYear = ?', $year)
            ->where('a.sem_seq = ?', $month)
            ->where('a.IdScheme = ?', $scheme);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function insertCe($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_studentregsubjects', $data);
        $id = $db->lastInsertId('tbl_studentregsubjects', 'IdStudentRegSubjects');
        return $id;
    }

    public function getListToInsertExamItem(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration = b.IdStudentRegistration', array())
            ->join(array('c'=>'student_profile'), 'b.sp_id = c.id', array())
            ->joinLeft(array('d'=>'tbl_subjectmaster'), 'a.IdSubject = d.IdSubject', array())
            ->where('b.IdProgram = ?', 1)
            ->where('a.IdSemesterMain = ?', 176)
            ->where('d.CourseType != ?', 2)
            ->where('d.CourseType != ?', 3)
            ->where('d.CourseType != ?', 19)
            ->where('a.Active != ?', 3);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getItemDetails($id, $value=false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects_detail'))
            ->where('a.regsub_id = ?', $id);

        if ($value != false){
            $select->where('a.item_id = ?', 879);
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function checkExamCenter($id, $idSubject){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'exam_registration'))
            ->where('a.er_idSemester = ?', 176)
            ->where('a.er_idStudentRegistration = ?', $id)
            ->where('a.er_idSubject = ?', $idSubject);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function insertItemExam($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_studentregsubjects_detail', $data);
        $id = $db->lastInsertId('tbl_studentregsubjects_detail', 'id');
        return $id;
    }

    public function insertExamCenter($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('exam_registration', $data);
        $id = $db->lastInsertId('exam_registration', 'er_id');
        return $id;
    }

    public function getCourseNotUpdate(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'))
            ->where('a.IdSemesterMain IN (180, 181, 190)')
            ->where('a.Active IN (0, 9, 10)');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSubjectItem3($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects_detail'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregsubjects'), 'a.regsub_id = b.IdStudentRegSubjects', array('b.initial'))
            //->where('a.invoice_id IS NOT NULL')
            ->where('a.regsub_id = ?', $id)
            ->order('a.regsub_id')
            ->group('a.invoice_id');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateCourseRegistration($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_studentregsubjects', $data, 'IdStudentRegSubjects = '.$id);
        return $update;
    }

    public function getSubjectReg($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects_detail'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregsubjects'), 'a.regsub_id = b.IdStudentRegSubjects', array('b.initial'))
            ->where('a.invoice_id IS NOT NULL')
            ->where('b.IdStudentRegistration = ?', $id)
            ->where('b.initial = ?', 1)
            ->order('a.regsub_id');

        $result = $db->fetchRow($select);
        return $result;
    }
}
?>