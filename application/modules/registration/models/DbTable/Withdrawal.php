<?php 

class Registration_Model_DbTable_Withdrawal extends Zend_Db_Table_Abstract {
	
	protected $_name = 'tbl_withdrawal';
	protected $_primary = "w_id";

	public function getDatabyId($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from(array('w'=>$this->_name))					 
					  ->where("w_id = ?",$id);
		
		//echo $select;
		$row = $db->fetchRow($select);
		
		return $row;
		
	}
	public function getListApplication($formdata=null){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$session = new Zend_Session_Namespace('sis');
		
		$select = $db ->select()
					  ->from(array('w'=>$this->_name))
					  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegSubjects=w.IdStudentRegSubjects',array())
					  ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=w.IdStudentRegistration',array('IdStudentRegistration','registrationId'))
					  ->join(array('sp'=>'student_profile'),'sp.appl_id=sr.idApplication',array('appl_fname','appl_mname','appl_lname'))
					  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=srs.IdSemesterMain',array('IdSemesterMaster','SemesterMainName'))
					  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject=srs.IdSubject',array('SubjectCode'=>'SubCode','SubjectName'=>'subjectMainDefaultLanguage'));
					  
		$rows = $db->fetchAll($select);
		
		if(isset($formdata)){
			
			if(isset($formdata["idIntake"]) && $formdata["idIntake"]!=''){
				$select->where("sr.IdIntake = ?",$formdata["idIntake"]);
			}
			
			if(isset($formdata["IdProgram"]) && $formdata["IdProgram"]!=''){
				$select->where("sr.IdProgram = ?",$formdata["IdProgram"]);
			}
			
			if(isset($formdata["IdSemester"]) && $formdata["IdSemester"]!=''){
				$select->where("srs.IdSemesterMain = ?",$formdata["IdSemester"]);
			}
						
			if(isset($formdata["status"]) && $formdata["status"]!=''){
				$select->where("w.w_status = ?",$formdata["status"]);
			}
			
			if(isset($formdata["Student"]) && $formdata["Student"]!=''){
				$select->where("((sp.appl_fname  LIKE '%".$formdata["Student"]."%'");
		 		$select->orwhere("sp.appl_mname  LIKE '%".$formdata["Student"]."%'");
		 		$select->orwhere("sp.appl_lname  LIKE '%".$formdata["Student"]."%')");
		 		$select->orwhere("sr.registrationId  LIKE '%".$formdata["Student"]."%')");
			}
			
			
			if($session->IdRole == 311 || $session->IdRole == 298){ //FACULTY DEAN atau FACULTY ADMIN nampak faculty dia sahaja
				$select->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array());
				$select->where("p.IdCollege='".$session->idCollege."'");		
	    	} 
		}
		
		echo $select;
		$rows = $db->fetchAll($select);
		
		return $rows;
	}
	
	
	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					  ->from(array('w'=>$this->_name))					 
					  ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegSubjects=w.IdStudentRegSubjects',array())
					  ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=srs.IdSemesterMain',array('IdSemesterMaster','SemesterMainName'))
					  ->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject=srs.IdSubject',array('SubjectCode'=>'SubCode','SubjectName'=>'subjectMainDefaultLanguage'))
					  ->where("w_id = ?",$id);
		
		//echo $select;
		$row = $db->fetchRow($select);
		
		return $row;
		
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	
	
	
}

?>