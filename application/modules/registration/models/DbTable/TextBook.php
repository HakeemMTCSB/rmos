<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 23/10/2015
 * Time: 9:39 AM
 */
class Registration_Model_DbTable_TextBook extends Zend_Db_Table_Abstract {

    public function getTextBook($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_learning_material_courier'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_learning_material'), 'a.lmc_tlmid = b.tlm_id')
            ->joinLeft(array('c'=>'tbl_subjectmaster'), 'b.tlm_subjectId = c.IdSubject')
            ->joinLeft(array('d'=>'tbl_studentregistration'), 'a.lmc_studentid = d.IdStudentRegistration')
            ->joinLeft(array('e'=>'student_profile'), 'd.sp_id = e.id')
            ->joinLeft(array('f'=>'tbl_program'), 'd.IdProgram = f.IdProgram')
            ->joinLeft(array('g'=>'tbl_program_scheme'), 'd.IdProgramScheme = g.IdProgramScheme')
            ->joinLeft(array('h'=>'tbl_definationms'), 'g.mode_of_program = h.idDefinition', array('mop'=>'h.DefinitionDesc'))
            ->joinLeft(array('i'=>'tbl_definationms'), 'g.mode_of_study = i.idDefinition', array('mos'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'g.program_type = j.idDefinition', array('pt'=>'j.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_semestermaster'), 'b.tlm_semester = k.IdSemesterMaster')
            ->joinLeft(array('l'=>'tbl_countries'), 'a.lmc_country = l.idCountry', array('countryname'=>'l.CountryName'))
            ->joinLeft(array('m'=>'tbl_state'), 'a.lmc_state = m.idState', array('statename'=>'m.StateName'))
            ->joinLeft(array('n'=>'tbl_city'), 'a.lmc_city = n.idCity', array('cityname'=>'n.CityName'));

        if ($search != false){
            if (isset($search['Program']) && $search['Program'] != ''){
                $select->where($db->quoteInto('d.IdProgram = ?', $search['Program']));
            }
            if (isset($search['Programscheme']) && $search['ProgramScheme'] != ''){
                $select->where($db->quoteInto('d.IdProgramScheme = ?', $search['ProgramScheme']));
            }
            if (isset($search['Semester']) && $search['Semester'] != ''){
                $select->where($db->quoteInto('b.tlm_semester = ?', $search['Semester']));
            }
            if (isset($search['Method']) && $search['Method'] != ''){
                $select->where($db->quoteInto('a.lmc_method = ?', $search['Method']));
            }
            if (isset($search['Name']) && $search['Name'] != ''){
                $select->where($db->quoteInto('CONCAT_WS(" ", e.appl_fname, e.appl_lname) = ?', '%'.$search['Name'].'%'));
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateTextBookCourier($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_learning_material_courier', $data, 'lmc_id = '.$id);
        return $update;
    }
}