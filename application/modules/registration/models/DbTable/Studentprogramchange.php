<?php 
class Registration_Model_DbTable_Studentprogramchange extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_programchangebackup';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	//Update this......
	public function getProgramDropDown($lintIdProgram){
		
		
		
		$consistantresult = 'SELECT i.IdAllowanceLevel   from tbl_awardlevel i  JOIN tbl_program p ON (p.Award=i.IdLevel) where p.IdProgram='.$lintIdProgram;
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("p"=>"tbl_program"),array("key"=>"IdProgram","value"=>"ProgramName"))	
		 				 ->where('p.Award IN (?)',new Zend_Db_Expr('('.$consistantresult.')'))		 				
		 				 ->where("p.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnAddBackupdata($formData,$lintidapplicant) { //Function for adding the Program Branch details to the table		
			$formData ['IdApplication'] = $lintidapplicant;
			unset($formData ['Save']);
    		return $this->insert($formData);
	}
	public function fnUpdateStudentApplication($IDCourse,$lintidapplicant) {  // function to update po details
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_studentapplication";
			$larrapplicant = array('IDCourse'=>$IDCourse);
			$where = "IdApplication = '".$lintidapplicant."'";
			return $db->update($table,$larrapplicant,$where);	
	}
     public function fngetStudentApplicationDetails() { //Function to get the Program Branch details
 		$select = $this->select()
			                ->setIntegrityCheck(false)  
			                ->join(array('a' => 'tbl_studentapplication'),array('IdApplication'))
			                ->join(array('b'=>'tbl_sendoffer'),'a.IdApplication  = b.IdApplication')
			                ->join(array('c'=>'tbl_collegemaster'),'a.idCollege  = c.IdCollege',array("c.CollegeName"))
                			->join(array('d'=>'tbl_program'),'a.IDCourse = d.IdProgram',array("d.ProgramName"))
					 		->where("b.Approved = 1")
			                ->where("a.Active = 1")               
			                ->where("a.Offered = 1")
			                ->where("a.Accepted = 1")
			                ->where("a.Termination = 0")
			                ->group("a.IdApplication")
			                ->order("a.FName");
       $result = $this->fetchAll($select);
       return $result->toArray();
     }
    public function fnSearchStudentApplication($post = array()) { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
       								->from(array("sa"=>"tbl_studentapplication"),array("sa.*"))
       								->join(array('b'=>'tbl_sendoffer'),'sa.IdApplication  = b.IdApplication')
       								->join(array('c'=>'tbl_collegemaster'),'sa.idCollege  = c.IdCollege',array("c.CollegeName"))
                					->join(array('d'=>'tbl_program'),'sa.IDCourse = d.IdProgram',array("d.ProgramName"));
					 			
			if(isset($post['field5']) && !empty($post['field5']) ){
				$lstrSelect = $lstrSelect->where("sa.IdApplication = ?",$post['field5']);										
			}
			
       		$lstrSelect		->where('sa.ICNumber like "%" ? "%"',$post['field2'])
		       				->where("b.Approved = 1")
		       				->where("sa.Offered = 1")
		       				->where("sa.Accepted = 1")
		       				->where("sa.Active = 1")             
		                	->where("sa.Termination = 0")
		                	->group("sa.IdApplication")
		                	->order("sa.FName");
       				
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	
     
	public function fnGetApplicantNameList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
					 			->from(array("sa"=>"tbl_studentapplication"),array("key"=>"sa.IdApplication","value"=>"CONCAT_WS(' ',IFNULL(sa.FName,''),IFNULL(sa.MName,''),IFNULL(sa.LName,''))"))
					 			->join(array('b'=>'tbl_sendoffer'),'sa.IdApplication  = b.IdApplication')	
					 			->where("b.Approved = 1")
			       				->where("sa.Offered = 1")
			       				->where("sa.Accepted = 1")
			       				->where("sa.Active = 1")             
			                	->where("sa.Termination = 0")
			                	->group("sa.IdApplication")
			                	->order("sa.FName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);		
		return $larrResult;
	}
	
	

	 
	 
	   public function getCompleteStudentDetails($lintidapplicant) { 
	     	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a' => 'tbl_studentapplication'),array('a.*'))
                			->join(array('b'=>'tbl_collegemaster'),'a.idCollege  = b.IdCollege')
                			->join(array('c'=>'tbl_program'),'a.IDCourse = c.IdProgram')
                			->where('a.IdApplication = ?',$lintidapplicant);
			$result = $db->fetchRow($sql);
			return $result;
		}


}
?>