<?php 
class Registration_Model_DbTable_Report extends Zend_Db_Table_Abstract{
    
    
    public function getStudentList($formData = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'student_profile'), 'a.sp_id=b.id')
            ->joinLeft(array('c'=>'tbl_program'), 'a.IdProgram = c.IdProgram', array('programName'=>'concat(c.ProgramCode, " - ",c.ProgramName)'))
            ->joinLeft(array('d'=>'tbl_program_scheme'), 'a.IdProgramScheme = d.IdProgramScheme')
            ->joinLeft(array('e'=>'tbl_definationms'), 'd.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc'))
            ->joinLeft(array('f'=>'tbl_definationms'), 'd.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'd.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc'))
            ->joinLeft(array('h'=>'tbl_intake'), 'a.IdIntake = h.IdIntake', array('intakeName'=>'h.IntakeDesc'))
            ->joinLeft(array('i'=>'tbl_semestermaster'), 'a.IdSemester = i.IdSemesterMaster', array('semesterName'=>'concat(i.SemesterMainName, " - ", i.SemesterMainCode)'));
        
        if ($formData != false){
            if (isset($formData['program']) && $formData['program']!=''){
                $select->where('a.IdProgram = ?', $formData['program']);
            }
            if (isset($formData['status']) && $formData['status']!=''){
                $select->where('a.profileStatus = ?', $formData['status']);
            }
            if (isset($formData['intake']) && $formData['intake']!=''){
                $select->where('a.IdIntake = ?', $formData['intake']);
            }
            if (isset($formData['programscheme']) && $formData['programscheme']!=''){
                $select->where('a.IdProgramScheme = ?', $formData['programscheme']);
            }
            if (isset($formData['name']) && $formData['name']!=''){
                $select->where('concat(b.appl_fname, " ", b.appl_lname) like "%'.$formData['name'].'%"');
            }
            if (isset($formData['id']) && $formData['id']!=''){
                $select->where('a.registrationId like "%'.$formData['id'].'%"');
            }
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSubjectList($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.IdSemesterMain = IdSemesterMaster', array('subSemName'=>'concat(c.SemesterMainName, " - ", c.SemesterMainCode)'))
            ->where('a.IdStudentRegistration = ?', $id);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getItemRegistration($semId, $progId, $progSchemeId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_registration_item'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.item_id = b.idDefinition', array('itemRegName'=>'b.DefinitionDesc'))
            ->where('a.semester_id = ?', $semId)
            ->where('a.program_id = ?', $progId)
            ->where('a.program_scheme_id = ?', $progSchemeId);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'))
            ->order('a.seq_no')
            ->order('a.ProgramName');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getCourseOfferedBasedOnSem($semId, $formData = false, $stdArr, $item){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_subjectsoffered'), array('value'=>'*'))
            ->joinRight(array('b'=>'tbl_subjectmaster'), 'a.IdSubject=b.IdSubject')
            ->joinLeft(array('e'=>'tbl_landscapesubject'), 'a.IdSubject=e.IdSubject')
            ->where('a.IdSemester =?', $semId)
            ->where('a.IdSubject IS NOT NULL')
            ->where('a.IdSubject IN (SELECT c.IdSubject FROM tbl_studentregsubjects as c LEFT JOIN tbl_studentregsubjects_detail as d ON c.IdStudentRegSubjects=d.regsub_id WHERE d.item_id = '.$item.' AND c.IdStudentRegistration IN (?))', $stdArr);
            //->where('a.IdSubject IN (SELECT c.IdSubject FROM tbl_studentregsubjects as c INNER JOIN tbl_studentregsubjects_detail as d ON c.IdStudentRegSubjects=d.regsub_id WHERE d.item_id = '.$item.' AND c.Active <> 3 AND d.status <> 3)')
            //->where('b.CourseType = ?', 1);
        
        if ($formData != false){
            if (isset($formData['subjectbranch']) && $formData['subjectbranch']!=''){
                $select->where('a.Branch = ?', $formData['subjectbranch']);
            }
            if (isset($formData['program']) && $formData['program']!=''){
                $select->where('e.IdProgram = ?', $formData['program']);
            }
        }
        
        $select->group('a.IdSubject');
        //echo $select; //exit;
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getItemRegisterList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', 160)
            ->order('a.defOrder');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getProgramDetail($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program'), array('a.*'))
            ->where('a.IdProgram = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSemester($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.display = ?', 1)
            ->where('a.IdScheme = ?', $id)
            ->order('a.SemesterMainStartDate DESC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getBranchList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_branchofficevenue'), array('value'=>'*'))
            ->order('a.BranchName');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getStudentList2($formData = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->joinLeft(array('b'=>'student_profile'), 'a.sp_id=b.id')
            ->joinLeft(array('c'=>'tbl_program'), 'a.IdProgram = c.IdProgram', array('programName'=>'concat(c.ProgramCode, " - ",c.ProgramName)'))
            ->joinLeft(array('d'=>'tbl_program_scheme'), 'a.IdProgramScheme = d.IdProgramScheme')
            ->joinLeft(array('e'=>'tbl_definationms'), 'd.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc'))
            ->joinLeft(array('f'=>'tbl_definationms'), 'd.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc'))
            ->joinLeft(array('g'=>'tbl_definationms'), 'd.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_definationms'), 'a.profileStatus = k.idDefinition', array('statusName'=>'k.DefinitionDesc'))
            ->joinLeft(array('h'=>'tbl_intake'), 'a.IdIntake = h.IdIntake', array('intakeName'=>'h.IntakeDesc'))
            ->joinLeft(array('i'=>'tbl_semestermaster'), 'a.IdSemester = i.IdSemesterMaster', array('semesterName'=>'concat(i.SemesterMainName, " - ", i.SemesterMainCode)'))
            ->joinLeft(array('j'=>'tbl_branchofficevenue'), 'a.IdBranch = j.IdBranch', array('branchName'=>'j.BranchName'))
            ->where('a.IdStudentRegistration IN (SELECT k.IdStudentRegistration FROM tbl_studentregsubjects AS k LEFT JOIN tbl_studentregsubjects_detail as l ON k.IdStudentRegSubjects=l.regsub_id WHERE l.item_id = '.$formData['itemregister'].' AND k.IdSemesterMain = '.$formData['semester'].' AND k.Active <> 3 AND l.status <> 3 GROUP BY k.IdStudentRegistration)');
            //$select->where('a.profileStatus = ?', 92);
        
        if ($formData != false){
            if (isset($formData['program']) && $formData['program']!=''){
                $select->where('a.IdProgram = ?', $formData['program']);
            }
            if (isset($formData['programscheme']) && $formData['programscheme']!=''){
                $select->where('a.IdProgramScheme IN (?)', $formData['programscheme']);
            }
            if (isset($formData['intake']) && $formData['intake']!=''){
                $select->where('a.IdIntake IN (?)', $formData['intake']);
            }
            /*if (isset($formData['semester']) && $formData['semester']!=''){
                
            }*/
            if (isset($formData['studentbranch']) && $formData['studentbranch']!=''){
                $select->where('a.IdBranch = ?', $formData['studentbranch']);
            }
            /*if (isset($formData['status']) && $formData['status']!=''){
                $select->where('a.profileStatus = ?', $formData['status']);
            }*/
        }
        
        $select->where('a.IdStudentRegistration IN (SELECT student_id FROM tbl_studentregsubjects_detail)');
        $select->group('a.registrationId');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getRegisteredCourse($itemId, $studentId, $subjectId, $semesterId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregsubjects_detail'), 'a.IdStudentRegSubjects = b.regsub_id')
            ->where('a.Active <> ?', 3)
            ->where('b.status <> ?', 3)
            //->where('a.exam_status lik "CT" OR a.exam_status = "EX"')
            //->where('a.exam_status <> ?', 'EX')
            ->where('b.item_id = ?', $itemId)
            ->where('a.IdStudentRegistration = ?', $studentId)
            ->where('a.IdSubject = ?', $subjectId)
            ->where('a.IdSemesterMain = ?', $semesterId);
        //echo $select; exit;
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getProgScheme($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_program_scheme'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.mode_of_program = b.idDefinition', array('mop'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.mode_of_study = c.idDefinition', array('mos'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_definationms'), 'a.program_type = d.idDefinition', array('pt'=>'d.DefinitionDesc'))
            ->where('a.IdProgram = ?', $id)
            ->order("a.IdProgramScheme");
        
        $result = $db->fetchAll($select);
        return $result;
    }
}
?>