<?php
class Registration_Model_DbTable_Credittransfer extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	
	
	public function fngetStudentprofileDtls() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       
        $consistantresult = 'SELECT i.IdApplication  from tbl_sendoffer i';
        
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("sa"=>"tbl_studentapplication"),array("sa.*"))
       								->join(array("p"=>"tbl_program"),'p.IdProgram=sa.IDCourse',array("p.*"))
       								->where("sa.Registered = 0")
       								->where("sa.Offered = 1")
       								->where("sa.Active = 1")
       								->where("sa.Termination = 0")
       								->where("sa.Accepted = 1")
       								->where('sa.IdApplication IN (?)',new Zend_Db_Expr('('.$consistantresult.')'))
       								->order("sa.FName");
       					
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     }
     
	public function fnGetApplicantNameList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
		$consistantresult = 'SELECT i.IdApplication  from tbl_sendoffer i';		
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_studentapplication"),array("key"=>"sa.IdApplication","value"=>"CONCAT_WS(' ',IFNULL(sa.FName,''),IFNULL(sa.MName,''),IFNULL(sa.LName,''))"))
		 				// ->join(array("a"=>"tbl_studentregistration"),'sa.IdApplication=a.IdApplication')
		 				 ->where("sa.Active = 1")
		 				 ->where('sa.IdApplication IN (?)',new Zend_Db_Expr('('.$consistantresult.')'))
		 				 ->group("sa.IdApplication");
		 				
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
     
	
	
    public function getLandscapeList($idprogram){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();						
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_studentapplication"))
		 				 ->join(array('c'=>'tbl_program'),'sa.IDCourse = c.IdProgram')
		 				 ->join(array('d'=>'tbl_landscape'),'d.IdProgram = c.IdProgram',array('d.IdLandscape as IdLandscape'))
		 				 ->join(array('e'=>'tbl_definationms'),'d.LandscapeType = e.idDefinition',array('e.DefinitionDesc as Landscape'))
		 				 ->where("sa.Active = 1")
		 				 ->where("sa.IDCourse = ?",$idprogram);	
		 				
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function getSubjectList($landscapeId){
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();			
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_landscapeblocksubject"))
		 				 ->join(array('c'=>'tbl_subjectmaster'),'sa.subjectid = c.IdSubject',array('c.IdSubject as SubjId','c.SubjectName as SubjName','c.SubCode as SubCode'))
		 			     ->where("sa.IdLandscape = ?",$landscapeId);
		 			
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
		
		
	}
	public function fnSearchStudentApplication($post = array()) { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
           
	   $lstrSelect = $lobjDbAdpt->select()
       								->from(array("sa"=>"tbl_studentapplication"),array("sa.*"))
       								->join(array("p"=>"tbl_program"),'p.IdProgram=sa.IDCourse',array("p.*"));
       								       								
			if(isset($post['field5']) && !empty($post['field5']) ){
				$lstrSelect = $lstrSelect->where("sa.IdApplication = ?",$post['field5']);										
			}
			if(isset($post['field8']) && !empty($post['field8']) ){
				$lstrSelect = $lstrSelect->where("sa.IDCourse = ?",$post['field8']);
										
			}				
       			$lstrSelect	->where('sa.ICNumber like "%" ? "%"',$post['field2'])
       				->where("sa.Registered = 0")       	
       				->order("sa.FName");
        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
  
	   public function getCompleteStudentDetails($lintidapplicant) { 
	     	$db = Zend_Db_Table::getDefaultAdapter();
	    
			$sql = $db->select()	
							->from(array('a' =>'tbl_studentapplication'),array('a.*'))
                			->join(array('b'=>'tbl_collegemaster'),'a.idCollege  = b.IdCollege')
                			->join(array('c'=>'tbl_program'),'a.IDCourse = c.IdProgram')
                			->join(array('d'=>'tbl_landscape'),'d.IdProgram = c.IdProgram')
                			->join(array('e'=>'tbl_definationms'),'d.LandscapeType = e.idDefinition')
                			->join(array('g'=>'tbl_semester'),'d.IdStartSemester = g.IdSemester')
                			->join(array('f'=>'tbl_semestermaster'),'g.Semester = f.IdSemesterMaster')
                			->where('a.IdApplication = ?',$lintidapplicant)
							->where("d.Active  = 123");
						
			$result = $db->fetchRow($sql);
			return $result;
		}
		
		
	public function getCompleteStudentSubjectDetails($lintidapplicant) { //Function to get the user details
		
		$consistantresult = 'SELECT max(i.IdStudentRegistration) from tbl_studentregistration i where i.IdApplication = a.IdApplication';
      	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_studentapplication'),array('a.*'))
                			->join(array('b'=>'tbl_program'),'a.IDCourse = b.IdProgram')                			
                			->join(array('c'=>'tbl_landscape'),'c.IdProgram = b.IdProgram')                			
                			->join(array('d'=>'tbl_studentregistration'),'a.IdApplication = d.IdApplication')                			
                			->join(array('e'=>'tbl_studentregsubjects'),'d.IdStudentRegistration = e.IdStudentRegistration',array('e.IdSubject as subjectId'))                			
                			->join(array('f'=>'tbl_definationms'),'c.LandscapeType = f.idDefinition',array('f.DefinitionDesc as landscapetype'))                			
                			->join(array('g'=>'tbl_semester'),'d.IdSemester = g.IdSemester')                			
                			->join(array('h'=>'tbl_semestermaster'),'g.Semester = h.IdSemesterMaster')                			
                			->join(array('j'=>'tbl_subjectmaster'),'e.IdSubject = j.IdSubject',array('j.CreditHours as CreditHoursforsubjects','j.SubjectName as SubjectName','j.IdSubject as IdSubject','j.SubCode as SubCode','j.AmtPerHour as AmtPerHour'))                			
                			->join(array('k'=>'tbl_landscapesubject'),'e.IdSubject = k.IdSubject')                			
                			->join(array('l'=>'tbl_definationms'),'l.idDefinition = k.SubjectType',array('l.DefinitionDesc as SubjectType'))
                			->where('a.IdApplication = ?',$lintidapplicant)
							->where("c.Active  = 123")
							->where("k.IdLandscape = c.IdLandscape")
							->where("e.IdStudentRegistration = ?",new Zend_Db_Expr('('.$consistantresult.')'));							
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     }
		

	public function fnAddCredittransfer($larrformData) {  // function to insert po details
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_credittransfer";		
		$larrInsertData = array('IdApplication' => $larrformData["applicantid"],
								'IdProgram' => $larrformData["programid"],
								'UpdDate' => $larrformData["UpdDate"],
								'UpdUser' => $larrformData["UpdUser"]
							);
			$db->insert($table,$larrInsertData);
	
		return $db->lastInsertId();		
	}
	
	public function fnaddCreditTransferDocumentDetails($larrfilenameslist,$lastsubjcreditId)
	{
		$str = "";
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
		
		
		foreach($larrfilenameslist as $larrfilenames) {
			$str .= "(".$lastsubjcreditId.",'".$larrfilenames['FileLocation']."','".$larrfilenames['filename']."','".$larrfilenames['uploadedfilename']."','".$larrfilenames['size']."','".$larrfilenames['type']."','".$larrfilenames['UpdDate']."',".$larrfilenames['UpdUser'].",'".$larrfilenames['Comments']."','".$larrfilenames['documentcategory']."'),";
		}
			
		$lstrselectsql = "INSERT INTO tbl_credittransferuploads (IdCreditTransferSubjects,FileLocation,FileName,UploadedFilename,FileSize,MIMEType,UpdDate,UpdUser,Comments,documentcategory) VALUES ".substr($str,0,-1); 
	
		$lobjDbAdpt->query($lstrselectsql);
	}
	
    public function fnAddCredittransferSubjects($larrInsertData,$lastid) {  // function to insert po details
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_credittransfersubjects";		
		$db->insert($table,$larrInsertData);	
		return $db->lastInsertId();		
	}
	
	
	 	 
   public function getPresentSemester($lintidapplicant) { 
   	
   	$consistantresult = 'SELECT max(j.IdStudentRegistration) from tbl_studentregistration j where j.IdApplication = "'.$lintidapplicant.'"';

   	    $db = Zend_Db_Table::getDefaultAdapter();
		$sql = $db->select()	
							->from(array('a' => 'tbl_studentregistration'),array('a.*'))
                			->join(array('g'=>'tbl_semester'),'a.IdSemester = g.IdSemester')
                			->join(array('f'=>'tbl_semestermaster'),'g.Semester = f.IdSemesterMaster')
                			->where('a.IdApplication = ?',$lintidapplicant)
							->where("a.IdStudentRegistration = ?",new Zend_Db_Expr('('.$consistantresult.')'));
		$result = $db->fetchRow($sql);
		return $result;
	}
	 
	 
	public function fnGetCreditTransferResult($lintidapplicant,$idprogram,$idsemester) { //Function to get the user details
      	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_credittransfer'),array('a.*'))
                			->where('a.IdApplication = ?',$lintidapplicant)
                			->where('a.IdProgram = ?',$idprogram)
                			->where('a.IdSemester = ?',$idsemester);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     }
	 

	public function fnGetCreditTransferAppliedResult($lintidapplicant,$idprogram,$idsemester) { //Function to get the user details
      	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_credittransfer'),array('a.IdCreditTransfer as IdCreditTransfer','a.Status as Transferstatus','a.DateApplied as DateApplied','a.DateApproved as DateApproved','a.PaymentFees as PaymentFees','a.PaymentStatus as PaymentStatus','a.PaymentDate as PaymentDate'))
							->join(array('b'=>'tbl_subjectmaster'),'a.IdSubject = b.IdSubject',array('b.CreditHours as CreditHoursforsubjects','b.SubjectName as SubjectName','b.IdSubject as IdSubject','b.SubCode as SubCode','b.AmtPerHour as AmtPerHour'))
							->join(array('c'=>'tbl_landscape'),'c.IdProgram = a.IdProgram') 
							->join(array('d'=>'tbl_landscapesubject'),'c.IdLandscape = d.IdLandscape')
							->join(array('e'=>'tbl_definationms'),'e.idDefinition = d.SubjectType')
                			->where('a.IdApplication = ?',$lintidapplicant)
                			->where('a.IdProgram = ?',$idprogram)
                			->where('a.IdSubject = d.IdSubject')
                			->where("c.Active  = 123")
                			->where('a.IdSemester = ?',$idsemester);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     }
	     
	public function fnUpdateCredittransfer($larrformData) {  // function to update po details
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_credittransfer";
		
			$countofstatus = count($larrformData['idcredittransfer']);
			for($i=0;$i<$countofstatus;$i++) {
				$ldtsystemDate = date ('Y-m-d H:i:s');
				$larrCredit = array('Status'=>$larrformData['status'][$i],
									'DateApproved'=>$ldtsystemDate);
				$where = "IdCreditTransfer = '".$larrformData['idcredittransfer'][$i]."'";
				$db->update($table,$larrCredit,$where);	
			}
			
	}
	

}