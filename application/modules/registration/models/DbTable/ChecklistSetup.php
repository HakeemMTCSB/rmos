<?php 
class Registration_Model_DbTable_ChecklistSetup extends Zend_Db_Table_Abstract
{
	
	/**
	 * The default table name 
	 */
	
	protected $_name = 'registration_checklist_setup';
	protected $_primary = "rcs_id";
	
	public function getData($id=null){
	
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select = $db ->select()
					  ->from(array('rc'=>$this->_name))
					  ->joinLeft(array('tu'=>'tbl_user'), 'tu.iduser = rc.rcs_create_by', array())
					  ->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = tu.IdStaff', array('rcs_create_by_name'=>'Fullname'))
					  ->joinLeft(array('tu2'=>'tbl_user'), 'tu2.iduser = rc.rcs_last_update_by', array())
					  ->joinLeft(array('ts2'=>'tbl_staffmaster'), 'ts2.IdStaff = tu2.IdStaff', array('rcs_last_update_by_name'=>'Fullname'))
		              ->order("rc.rcs_name");
	
		if($id){
			$select->where('rc.rcs_id = ?', $id);
			
			$row = $db->fetchRow($select);
		}else{
			$row = $db->fetchAll($select);
		}
		
		
		return $row;
	}
	
	public function insert(array $data){
	
		$auth = Zend_Auth::getInstance();
	
		$data['rcs_create_by'] = $auth->getIdentity()->iduser;
		$data['rcs_create_date'] = date('Y-m-d H:i:s');
			
		return parent::insert($data);
	}
	
	
	public function update(array $data,$where){
	
		$auth = Zend_Auth::getInstance();
		
		$data['rcs_last_update_by'] = $auth->getIdentity()->iduser;
		$data['rcs_last_update_date'] = date('Y-m-d H:i:s');
	
		return parent::update($data, $where);
	}
}