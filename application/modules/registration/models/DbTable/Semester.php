<?php 
class Registration_Model_DbTable_Semester extends Zend_Db_Table_Abstract
{
	
	/**
	 * The default table name 
	 */
	
	protected $_name = 'tbl_semestermaster';
	protected $_primary = "";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getProgramSemester($IdProgram,$IdIntake,$IdScheme=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$semList = array();
		 
/*		$select1 = $db ->select()
					   ->from(array('s'=>$this->_name))					
		 			   ->where("s.Program = ?",$IdProgram)
		 			   ->where("s.StudentIntake = ?",$IdIntake)
		 			   ->where("s.Active = ?",1);
		 	
		$row1 = $db->fetchAll($select1);	*/	

		
		
	    $select2 = $db->select()
	   				  ->from(array('sm'=>'tbl_semestermaster'))	
		 			  ->where("sm.Scheme = ?",$IdScheme)
		 			  ->where("sm.IsCountable =?",1)
                      ->where("sm.DummyStatus IS NULL")
                      ->where('sm.SemesterMainCode <> ?',' ');
		 			                                                         
		$row2 = $db->fetchAll($select2);
			
	 	/*foreach($row1 as $arr){
                    $tem['key'] = $arr['IdSemester'].'_detail';
                    $tem['value'] = $arr['SemesterCode'];
                    $semList[] = $tem;
                }*/
                
        foreach($row2 as $arr){
                    $tem['key'] = $arr['IdSemesterMaster'].'_main';
                    $tem['value'] = $arr['SemesterMainCode'];
                     $tem['name'] = $arr['SemesterMainName'];
                    $semList[] = $tem;
        }
                                
		return $semList; 
	}
	
	
	
public function getApplicantCurrentSemester($data)
    {
       	$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                  ->from(array('sm' => $this->_name))                 
                  ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
                  ->where('sm.IdScheme = ?',(int)$data['ap_prog_scheme'])
                  ->where('sm.special_semester = ?',0);

		if ( isset($data['branch_id']) )
		{
			$sql->where('sm.Branch = ?',(int)$data['branch_id']);   
		}

        $result = $db->fetchRow($sql); 
        
        if(!$result){
        	 $sql = $db->select()
                  ->from(array('sm' => $this->_name))                 
                  ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
                  ->where('sm.IdScheme = ?',(int)$data['ap_prog_scheme'])
                  ->where('sm.special_semester = ?',0);
        	$result = $db->fetchRow($sql); 
        }
        
        return $result;
    	
    }

    public function getApplicantCurrentSemester2($data)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()
            ->from(array('sm' => $this->_name))
            ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
            ->where('sm.IdScheme = ?',(int)$data['ap_prog_scheme']);

        if ( isset($data['branch_id']) )
        {
            $sql->where('sm.Branch = ?',(int)$data['branch_id']);
        }

        $result = $db->fetchRow($sql);

        if(!$result){
            $sql = $db->select()
                ->from(array('sm' => $this->_name))
                ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
                ->where('sm.IdScheme = ?',(int)$data['ap_prog_scheme']) ;
            $result = $db->fetchRow($sql);
        }

        if ($result){
            $sql2 = $db->select()
                ->from(array('sm' => $this->_name))
                ->where('sm.AcademicYear = ?',$result['AcademicYear'])
                ->where('sm.sem_seq = ?',$result['sem_seq']);

            $result2 = $db->fetchAll($sql2);

            $semarr = array();
            if ($result2){
                foreach ($result2 as $result2Loop){
                    $semarr[]=$result2Loop['IdSemesterMaster'];
                }
            }
        }

        return $semarr;
    }
    
    
	public function getApplicantRegisterSemesterOnwards($data,$semester_date,$flag)
    {
       	$db = Zend_Db_Table::getDefaultAdapter();	
        
   		 $sql = $db->select()
                  ->from(array('sm' => $this->_name)) 
                  ->where('sm.SemesterMainName NOT IN ("Credit Transfer","Audit","Exemption","Part 3")') 
                  ->where('sm.SemesterMainEndDate >= CURDATE()')                  
                  ->where('sm.IdScheme = ?',(int)$data['ap_prog_scheme'])
                  ->where('sm.Branch = ?',(int)$data['branch_id'])
                  ->order('sm.SemesterMainStartDate ASC')
                  ->order('sm.SemesterMainEndDate ASC')
                  ->limit(3,0);
                  
                  if(isset($semester_date) && $semester_date!=''){
                  		$sql->where('sm.SemesterMainStartDate >= ?',$semester_date);
                  }
                  		
        $result = $db->fetchAll($sql); 
        
        
        $sql2 = $db->select()
                  ->from(array('sm' => $this->_name)) 
                  ->where('sm.SemesterMainName NOT IN ("Credit Transfer","Audit","Exemption","Part 3")') 
                  ->where('sm.SemesterMainEndDate >= CURDATE()')
                  ->where('sm.IdScheme = ?',(int)$data['ap_prog_scheme'])
                  ->order('sm.SemesterMainStartDate ASC')
                  ->order('sm.SemesterMainEndDate ASC')
                   ->limit(3,0);
                  
     			  if(isset($semester_date) && $semester_date!=''){
                  		$sql2->where('sm.SemesterMainStartDate >= ?',$semester_date);
                  }
                  
                  
        $result2 = $db->fetchAll($sql2); 
       
        $result_all = array_unique(array_merge($result, $result2),SORT_REGULAR);  
       
        return $result_all;
    	
    }
    
    
    /*
     * Function : To get semester register for new student registration
     * Param : Based on applicant semester intake and display semester onwards
     */
	public function getApplicantCurrentSemesterOnwards($data)
    {
       	$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                  ->from(array('sm' => $this->_name))                 
                  //->where('( ( sm.SemesterMainStartDate <= CURDATE() AND sm.SemesterMainEndDate >  CURDATE()) OR (sm.SemesterMainStartDate > CURDATE() ) )')
                  ->where('((sm.IdScheme = ?',(int)$data['IdScheme']) 
                  ->where('sm.Branch = ?)',(int)$data['branch_id'])
                  ->orwhere('sm.Branch = 0)')
                  ->where('sm.AcademicYear >= ?',$data['sem_year'])   
                  ->where('sm.sem_seq >= ?',$data['sem_seq'])   
                  ->where('sm.SemesterMainName NOT IN ("Credit Transfer","Audit","Exemption")')             
                  ->order('sm.sem_seq ASC')
                  ->order('sm.SemesterMainStartDate')
                  ->limit(4,0);
        $result = $db->fetchAll($sql); 
                
        return $result;
    	
    }
    
    
    
		
    
	public function getData($idSemester)
    {
       	$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                  ->from(array('sm' => $this->_name))                 
                 ->where('sm.IdSemesterMaster =?',$idSemester);
        $result = $db->fetchRow($sql); 
        return $result;
    	
    }
    
    
	public function getPreviousSemester($semester){
		$db = Zend_Db_Table::getDefaultAdapter(); 
		
		
		if($semester){
			$startDate = $semester["SemesterMainStartDate"];
			$scheme = $semester["IdScheme"];
		}else{
			$startDate =  'CURDATE()';
			$scheme = '0';
		}
		 $sql = $db->select()
			->from(array('sm'=>'tbl_semestermaster'))
			->where("SemesterMainStartDate < ?",$startDate)
			->where("IsCountable = 1");
			
			if($semester){
				$sql->where("IdScheme = ?",$scheme);
			}
			
			$sql->order('SemesterMainStartDate desc')
			->limit(1,0);
		$result = $db->fetchRow($sql);
		
		return $result;				
	}
    
	
	public function getAllCurrentSemester()
    {
       	 $db = Zend_Db_Table::getDefaultAdapter();	
        
         $sql = $db->select()
                  ->from(array('sm' => $this->_name))                 
                  ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
                  ->where('sm.IdScheme IN (1,11)');
         $result = $db->fetchAll($sql); 
        
         return $result;
    	
    }
    
    
	public function getListSemesterBackwards($semester){
		
		$db = Zend_Db_Table::getDefaultAdapter(); 
		
		$sql = $db->select()
                  ->from(array('sm' => $this->_name))         
                  ->where('sm.SemesterMainName NOT IN ("Credit Transfer","Audit","Exemption","Part 3")')           
                  //->where('( sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
                  //->orwhere('sm.SemesterMainEndDate <= CURDATE() )')
                  ->where('sm.SemesterMainEndDate <= ?',$semester['SemesterMainEndDate'])
                  ->where('sm.IdScheme =?',$semester['IdScheme'])
                  ->order('sm.SemesterMainStartDate DESC')
                  ->limit(3);
         $result = $db->fetchAll($sql); 		
		
		return $result;				
	}
    
	
	public function getEqSemesterOpen($idSemester)
    {
       	$db = Zend_Db_Table::getDefaultAdapter();

       	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
        $semester = $semesterDB->getData($idSemester);       
        
        $sql = $db->select()
                  ->from(array('sm' => 'tbl_semestermaster'),array('IdSemesterMaster')) 
                  ->where('sm.IdScheme = ?',12)                
                  ->where('sm.AcademicYear = ?',$semester['AcademicYear'])
                  ->where('sm.sem_seq = ?',$semester['sem_seq']);
		$result = $db->fetchAll($sql); 
        
        return $result;
    	
    }
    
    
	public function getSemesterByScheme($idScheme,$idBranch,$first_semester)
    {
       	$db = Zend_Db_Table::getDefaultAdapter();
       
   
        $sql = $db->select()
                  ->from(array('sm' => $this->_name))  
                  ->where('sm.SemesterMainName NOT IN ("Credit Transfer","Audit","Exemption","Part 3")') 
                  ->where('sm.SemesterMainStartDate >= ? ',$first_semester[0]['SemesterMainStartDate'])              
                  ->where('sm.IdScheme =?',(int)$idScheme)
                  ->where('sm.Branch = ?',(int)$idBranch)
                  ->order('sm.SemesterMainStartDate')
                  ->order('sm.SemesterMainEndDate');
                  
        $result = $db->fetchAll($sql); 
        
        $sql2 = $db->select()
                  ->from(array('sm' => $this->_name)) 
                  ->where('sm.SemesterMainName NOT IN ("Credit Transfer","Audit","Exemption","Part 3")')             
                  ->where('sm.SemesterMainStartDate >= ? ',$first_semester[0]['SemesterMainStartDate'])          
                  ->where('sm.IdScheme = ?',(int)$idScheme)
                  ->order('sm.SemesterMainStartDate ASC')
                  ->order('sm.SemesterMainEndDate ASC');
                  
        $result2 = $db->fetchAll($sql2); 
       
        $result_all = array_unique(array_merge($result, $result2),SORT_REGULAR);  
       
        return $result_all;
    	
    }
	
    public function isPreviousSemester($idSemester){
    	
    		$db = Zend_Db_Table::getDefaultAdapter();
    		
    		 $sql = $db->select()
                  	   ->from(array('sm' => $this->_name))                 
                       ->where('sm.IdSemesterMaster =?',$idSemester);
	        $result = $db->fetchRow($sql); 
	        
	        $selected_date = date('Y-m-d', strtotime($result['SemesterMainEndDate']));
			$today= date('Y-m-d');

	        if( $today > $selected_date){
	        	//selected semester is previous semester
	        	return true;
	        }else{
	        	return false;
	        }
	       
       
    }
    
 	 public function isCurrentSemester($idSemester){
    	
    		$db = Zend_Db_Table::getDefaultAdapter();
    		
    		 $sql = $db->select()
                  	   ->from(array('sm' => $this->_name))                 
                       ->where('sm.IdSemesterMaster =?',$idSemester)
                       ->where('( sm.SemesterMainStartDate <= CURDATE() AND sm.SemesterMainEndDate >= CURDATE() ) ');
	        $result = $db->fetchRow($sql); 

	        return $result;
       
    }
}
?>