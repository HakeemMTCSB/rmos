<?php
class Registration_Model_DbTable_Studentcredittransfersearch extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_credittransfer';
	
	
	public function fngetStudentCrditTransferDtls() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("ct"=>"tbl_credittransfer"),array("ct.*"))
       								->join(array("cts"=>"tbl_credittransfersubjects"),'cts.IdCreditTransfer=ct.IdCreditTransfer')
       								->join(array("ctu"=>"tbl_credittransferuploads"),'ctu.IdCreditTransferSubjects=cts.IdCreditTransferSubjects')
       								->join(array("sa"=>"tbl_studentapplication"),'sa.IdApplication =ct.IdApplication',array("sa.*"))
       								->join(array("p"=>"tbl_program"),'p.IdProgram=ct.IdProgram ',array("p.*"))
       								
       								//->where("sa.Registered = 1")
       								//->where("sa.Offered = 1")
       								//->where("sa.Active = 1")
       								//->where("sa.Termination = 0")
       								//->where("sa.Accepted = 1")
       								->order("sa.FName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
     }
     
	public function fnGetApplicantNameList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_studentapplication"),array("key"=>"sa.IdApplication","value"=>"CONCAT_WS(' ',IFNULL(sa.FName,''),IFNULL(sa.MName,''),IFNULL(sa.LName,''))"))
		 				 //->join(array("a"=>"tbl_studentregistration"),'sa.IdApplication=a.IdApplication')
		 				 ->where("sa.Active = 1")
		 				 ->group("sa.IdApplication")
		 				 ->order("sa.FName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
     
	public function fnSearchStudentCrditTransferDtls($post = array()) { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
       								->from(array("ct"=>"tbl_credittransfer"),array("ct.*"))
       								->join(array("cts"=>"tbl_credittransfersubjects"),'cts.IdCreditTransfer=ct.IdCreditTransfer')
       								->join(array("ctu"=>"tbl_credittransferuploads"),'ctu.IdCreditTransferSubjects=cts.IdCreditTransferSubjects')
       								->join(array("sa"=>"tbl_studentapplication"),'sa.IdApplication =ct.IdApplication',array("sa.*"))
       								->join(array("p"=>"tbl_program"),'p.IdProgram=ct.IdProgram ',array("p.*"));
       								
			if(isset($post['field5']) && !empty($post['field5']) ){
				$lstrSelect = $lstrSelect->where("ct.IdApplication = ?",$post['field5']);
										
			}
			if(isset($post['field8']) && !empty($post['field8']) ){
				$lstrSelect = $lstrSelect->where("ct.IdProgram = ?",$post['field8']);
										
			}				
       			$lstrSelect	->where('sa.ICNumber like "%" ? "%"',$post['field2'])
       				//->where("sa.Registered = 1")
       				->order("sa.FName");
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
  	
	 
	public function fnaddStudentsCreditTransfer($larrformData) { 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		//echo "<PRE>";
		//echo count($larrformData['applicantid']);die();
		//print_r($larrformData);die();
		unset($larrformData['Save']);
		for($i = 0 ;$i<count($larrformData['applicantid']); $i++) {
				if(@$larrformData['DocVerified'][$i]) {
					$documentsverfied = 1;
					} else {
				   $documentsverfied = 0;
				}
			if(@$larrformData['DocApproved'][$i]) {
					$DocApproved = 1;
					} else {
				   $DocApproved = 0;
				}
		$IdCreditTransferSubjectsDocs=$larrformData["IdCreditTransferSubjectsDocs"][$i];
		$where = "IdCreditTransferSubjectsDocs = '$IdCreditTransferSubjectsDocs'";
		$lstrTable = "tbl_credittransferuploads";
		$larrInsertData = array('DocumentsVerified' => $documentsverfied,
								'DocumentsApproved' => $DocApproved,
								'CreditTransferComments' => $larrformData["Comments"][$i]
							);
		$lobjDbAdpt->update($lstrTable,$larrInsertData,$where);
		}
	 
	}
	
	public function fnUpdateMainSubjectprerequisites($transferedSubjectprereqdetails,$IdSubjectPrerequisites) {
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_subjectprerequisites";
			$where = "IdSubjectPrerequisites = '$IdSubjectPrerequisites'";
			$db->update($table,$transferedSubjectprereqdetails,$where);	
		}	
	
	   public function getCompleteStudentDetails($lintidapplicant) { 
	     	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a' => 'tbl_studentapplication'),array('a.*'))
                			->join(array('b'=>'tbl_collegemaster'),'a.idCollege  = b.IdCollege')
                			->join(array('c'=>'tbl_program'),'a.IDCourse = c.IdProgram')
                			->join(array('d'=>'tbl_landscape'),'d.IdProgram = c.IdProgram')
                			->join(array('e'=>'tbl_definationms'),'d.LandscapeType = e.idDefinition')
                			->join(array('g'=>'tbl_semester'),'d.IdStartSemester = g.IdSemester')
                			->join(array('f'=>'tbl_semestermaster'),'g.Semester = f.IdSemesterMaster')
                			->where('a.IdApplication = ?',$lintidapplicant)
							->where("d.Active  = 1");
			$result = $db->fetchRow($sql);
			return $result;
		}
		
		
	public function getCompleteStudentSubjectDetails($lintidapplicant) { //Function to get the user details
		$consistantresult = 'SELECT max(i.IdStudentRegistration) from tbl_studentregistration i where i.IdApplication = a.IdApplication';
      	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_studentapplication'),array('a.*'))
                			->join(array('b'=>'tbl_program'),'a.IDCourse = b.IdProgram')                			
                			->join(array('c'=>'tbl_landscape'),'c.IdProgram = b.IdProgram')                			
                			->join(array('d'=>'tbl_studentregistration'),'a.IdApplication = d.IdApplication')                			
                			->join(array('e'=>'tbl_studentregsubjects'),'d.IdStudentRegistration = e.IdStudentRegistration',array('e.IdSubject as subjectId'))                			
                			->join(array('f'=>'tbl_definationms'),'c.LandscapeType = f.idDefinition',array('f.DefinitionDesc as landscapetype'))                			
                			->join(array('g'=>'tbl_semester'),'d.IdSemester = g.IdSemester')                			
                			->join(array('h'=>'tbl_semestermaster'),'g.Semester = h.IdSemesterMaster')                			
                			->join(array('j'=>'tbl_subjectmaster'),'e.IdSubject = j.IdSubject',array('j.CreditHours as CreditHoursforsubjects','j.SubjectName as SubjectName','j.IdSubject as IdSubject','j.SubCode as SubCode','j.AmtPerHour as AmtPerHour'))                			
                			->join(array('k'=>'tbl_landscapesubject'),'e.IdSubject = k.IdSubject')                			
                			->join(array('l'=>'tbl_definationms'),'l.idDefinition = k.SubjectType',array('l.DefinitionDesc as SubjectType'))
                			->where('a.IdApplication = ?',$lintidapplicant)
							->where("c.Active  = 1")
							->where("k.IdLandscape = c.IdLandscape")
							->where("e.IdStudentRegistration = ?",new Zend_Db_Expr('('.$consistantresult.')'));
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     }
		

	public function fnAddCredittransfer($larrformData) {  // function to insert po details
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_credittransfer";
		$countofsubjects = count($larrformData['subjects']);
		for($i=0;$i<$countofsubjects;$i++) {
			$larrCredittransfer = array('IdApplication'=>$larrformData['IdApplication'],
			                         'IdProgram'=>$larrformData['IdProgram'],
									 'IdSemester'=>$larrformData['IdSemester'],
									 'IdSubject'=>$larrformData['subjects'][$i],
									 'DateApplied'=>$larrformData['UpdDate'],
									 'UpdDate'=>$larrformData['UpdDate'],
									 'UpdUser'=>$larrformData['UpdUser']
			);
			$db->insert($table,$larrCredittransfer);	
		}
	}
	 	 
   public function getPresentSemester($lintidapplicant) { 
   	$consistantresult = 'SELECT max(j.IdStudentRegistration) from tbl_studentregistration j where j.IdApplication = "'.$lintidapplicant.'"';
     	$db = Zend_Db_Table::getDefaultAdapter();
		$sql = $db->select()	
							->from(array('a' => 'tbl_studentregistration'),array('a.*'))
                			->join(array('g'=>'tbl_semester'),'a.IdSemester = g.IdSemester')
                			->join(array('f'=>'tbl_semestermaster'),'g.Semester = f.IdSemesterMaster')
                			->where('a.IdApplication = ?',$lintidapplicant)
							->where("a.IdStudentRegistration = ?",new Zend_Db_Expr('('.$consistantresult.')'));
		$result = $db->fetchRow($sql);
		return $result;
	}
	 
	 
	public function fnGetCreditTransferResult($lintidapplicant,$idprogram,$idsemester) { //Function to get the user details
      	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_credittransfer'),array('a.*'))
                			->where('a.IdApplication = ?',$lintidapplicant)
                			->where('a.IdProgram = ?',$idprogram)
                			->where('a.IdSemester = ?',$idsemester);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     }
	 

	public function fnGetCreditTransferAppliedResult($lintidapplicant,$idprogram,$idsemester) { //Function to get the user details
      	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_credittransfer'),array('a.IdCreditTransfer as IdCreditTransfer','a.Status as Transferstatus','a.DateApplied as DateApplied','a.DateApproved as DateApproved','a.PaymentFees as PaymentFees','a.PaymentStatus as PaymentStatus','a.PaymentDate as PaymentDate'))
							->join(array('b'=>'tbl_subjectmaster'),'a.IdSubject = b.IdSubject',array('b.CreditHours as CreditHoursforsubjects','b.SubjectName as SubjectName','b.IdSubject as IdSubject','b.SubCode as SubCode','b.AmtPerHour as AmtPerHour'))
							->join(array('c'=>'tbl_landscape'),'c.IdProgram = a.IdProgram') 
							->join(array('d'=>'tbl_landscapesubject'),'c.IdLandscape = d.IdLandscape')
							->join(array('e'=>'tbl_definationms'),'e.idDefinition = d.SubjectType')
                			->where('a.IdApplication = ?',$lintidapplicant)
                			->where('a.IdProgram = ?',$idprogram)
                			->where('a.IdSubject = d.IdSubject')
                			->where("c.Active  = 1")
                			->where('a.IdSemester = ?',$idsemester);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     }
	
	
     
	public function fnUpdateCredittransfer($larrformData) {  // function to update po details
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_credittransfer";
		
			$countofstatus = count($larrformData['idcredittransfer']);
			for($i=0;$i<$countofstatus;$i++) {
				$ldtsystemDate = date ('Y-m-d H:i:s');
				$larrCredit = array('Status'=>$larrformData['status'][$i],
									'DateApproved'=>$ldtsystemDate);
				$where = "IdCreditTransfer = '".$larrformData['idcredittransfer'][$i]."'";
				$db->update($table,$larrCredit,$where);	
			}
			
	}
	

}