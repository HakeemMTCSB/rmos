<?php 
class Registration_Model_DbTable_StudentRegistrationSeqno extends Zend_Db_Table_Abstract
{
	
	/**
	 * The default table name 
	 */
	
	protected $_name = 'student_registrationid_seqno';
	protected $_primary = "srs_id";
	
	
	public function addData($data){		
	   $this->insert($data);
	   $db = Zend_Db_Table::getDefaultAdapter();
	   return $id = $db->lastInsertId($this->_name);
	}
	
	public function updateData($data,$id){
		 //$this->update($data, $this->_primary .' = '. (int)$id);
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->update('student_registrationid_seqno', $data, 'srs_id = '.(int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
		
	public function getData($year){
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
					 ->from(array('srs'=>$this->_name))
				     ->where("srs_year = ?",$year);
		$row = $db->fetchRow($select);
		
		if(!$row){
			$data['srs_year']=$year;
			$data['srs_seqno']=1;
			$data['srs_updateby']=$auth->getIdentity()->id;
			$data['srs_updatedt']=date("Y-m-d H:i:s");
			$this->addData($data);

			$row = $this->getData($year);
		}
		return $row;		
	}

	public function getIntake($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_intake'), array('value'=>'*'))
			->where('a.IdIntake = ?', $id);

		$result = $db->fetchRow($select);
		return $result;
	}
}