<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 30/10/2015
 * Time: 3:10 PM
 */
class Registration_Model_DbTable_Migrate extends Zend_Db_Table_Abstract {

    public function getMigrateAuditPaper(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'migrate_audit_paper'), array('value'=>'*', 'invoiceid'=>'d.id', 'studentid'=>'b.IdStudentRegistration'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.studentid = b.registrationId AND a.programid = b.IdProgram')
            ->joinLeft(array('c'=>'tbl_subjectmaster'), 'a.coursecode = c.SubCode')
            ->joinLeft(array('d'=>'invoice_main'), 'a.invoiceno = d.bill_number')
            ->joinLeft(array('e'=>'tbl_program'), 'a.programid = e.IdProgram')
            ->where('a.id = ?', 7);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getMigrateExemption(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'migrate_exemption'), array('value'=>'*', 'invoiceid'=>'d.id', 'mstatus'=>'a.status', 'studentid'=>'b.IdStudentRegistration'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.studentid = b.registrationId AND a.programid = b.IdProgram')
            ->joinLeft(array('c'=>'tbl_subjectmaster'), 'a.coursecode = c.SubCode')
            ->joinLeft(array('d'=>'invoice_main'), 'a.invoice = d.bill_number')
            ->joinLeft(array('e'=>'tbl_program'), 'a.programid = e.IdProgram')
            ->order('a.status ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSemester($year, $month, $scheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.AcademicYear = ?', $year)
            ->where('a.sem_seq = ?', $month)
            ->where('a.IdScheme = ?', $scheme);

        $result = $db->fetchRow($select);
        return $result;
    }
}