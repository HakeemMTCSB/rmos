<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 11/12/2015
 * Time: 10:27 PM
 */
class Registration_Model_DbTable_WaitingList extends Zend_Db_Table_Abstract {

    public function getWaitingList($search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_waiting_list'), array('value'=>'*'))
            ->joinLeft(array('h'=>'tbl_course_tagging_group'), 'h.IdCourseTaggingGroup = a.twl_groupid')
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.twl_subjectid = b.IdSubject')
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.twl_semester = c.IdSemesterMaster')
            ->joinLeft(array('d'=>'tbl_subjectmaster'), 'a.twl_replace = d.IdSubject', array('replacecode'=>'d.SubCode'))
            ->join(array('e'=>'tbl_studentregistration'), 'a.twl_studentid = e.IdStudentRegistration')
            ->join(array('f'=>'student_profile'), 'e.sp_id = f.id')
            ->join(array('g'=>'tbl_program'), 'e.IdProgram = g.IdProgram')
            ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme=e.IdProgramScheme',array())
            ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc'))
            ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc'))
            ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc'));

        if ($search != false){
            if (isset($search['semester']) && $search['semester']!='') {
                $select->where($db->quoteInto('a.twl_semester = ?', $search['semester']));
            }
            if (isset($search['subcode']) && $search['subcode']!='') {
                $select->where($db->quoteInto('b.SubCode like ?', '%'.$search['subcode'].'%'));
            }
            if (isset($search['coursename']) && $search['coursename']!='') {
                $select->where($db->quoteInto('b.SubjectName like ?', '%'.$search['coursename'].'%'));
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getWaitingListById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_waiting_list'), array('value'=>'*'))
            ->joinLeft(array('h'=>'tbl_course_tagging_group'), 'h.IdCourseTaggingGroup = a.twl_groupid')
            ->joinLeft(array('k'=>'tbl_user'), 'a.twl_approveby = k.iduser', array('k.loginName'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.twl_subjectid = b.IdSubject')
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.twl_semester = c.IdSemesterMaster')
            ->joinLeft(array('d'=>'tbl_subjectmaster'), 'a.twl_replace = d.IdSubject', array('replacecode'=>'d.SubCode'))
            ->join(array('e'=>'tbl_studentregistration'), 'a.twl_studentid = e.IdStudentRegistration')
            ->joinLeft(array('i'=>'tbl_intake'), 'i.IdIntake = e.IdIntake')
            ->joinLeft(array('j'=>'tbl_definationms'), 'j.idDefinition = e.profileStatus', array('studentstatus'=>'j.DefinitionDesc'))
            ->join(array('f'=>'student_profile'), 'e.sp_id = f.id')
            ->join(array('g'=>'tbl_program'), 'e.IdProgram = g.IdProgram')
            ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme=e.IdProgramScheme',array())
            ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc'))
            ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc'))
            ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc'))
            ->where('a.twl_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateWaitingList($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_waiting_list', $data, 'twl_id = '.$id);
        return $update;
    }

    public function getGourpInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->where('a.IdCourseTaggingGroup = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getGroupStudentList($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->where('a.Active != ?', 3)
            ->where('a.not_include_calculation = ?', 0)
            ->where('a.IdCourseTaggingGroup = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getGroupStudentListReserve($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->where('a.Active != ?', 3)
            ->where('a.not_include_calculation = ?', 1)
            ->where('a.IdCourseTaggingGroup = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }
}