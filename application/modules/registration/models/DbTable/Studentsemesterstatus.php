<?php 

class Registration_Model_DbTable_Studentsemesterstatus extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_studentsemesterstatus';
	protected $_primary = "idstudentsemsterstatus";

			
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	

	public function getRegisteredBlock($registrationId){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))   
                   ->joinleft(array('b'=>'tbl_landscapeblock'),'b.idblock=s.IdBlock',array('blockname','semester_level'=>'semester'))      
                   ->joinleft(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=s.IdSemesterMain',array('SemesterMainName','SemesterMainCode'))       
                   ->where('s.IdStudentRegistration = ?',$registrationId) 
                   ->order("s.idstudentsemsterstatus")
                   ->group('s.IdBlock'); 
                  
        $result = $db->fetchAll($sql);
        
        //print_r($result);
        return $result;
	}
	
	
	public function getRegisteredSemester($registrationId,$sorts='Desc'){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))  
                   ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=s.IdSemesterMain',array('SemesterMainName','SemesterMainCode','SemesterMainStartDate','IdSemesterMaster'))       
                   ->where('s.IdStudentRegistration = ?',$registrationId)
                   ->order("sm.SemesterMainStartDate $sorts")
                   ->group('s.IdSemesterMain'); 
                  
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	public function updateRegistrationData($data,$id){
		 $this->update($data, 'IdStudentRegistration = '. $id);
	}
	
	public function deleteRegisterData($id){		
	  $this->delete('IdStudentRegistration =' . (int)$id);
	}
	
		
	public function getSemesterInfo($idstudentsemsterstatus=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))  
                   ->joinleft(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=s.IdSemesterMain',array('SemesterMainName','SemesterMainCode'))  
                   ->where('s.idstudentsemsterstatus = ?',$idstudentsemsterstatus);                   
                  
        $result = $db->fetchRow($sql);
        return $result;
	}
	
	public function getRegisteredSemesterBlock($registrationId,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))   
                   ->joinleft(array('b'=>'tbl_landscapeblock'),'b.idblock=s.IdBlock',array('blockname','semester_level'=>'semester'))      
                   ->joinleft(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=s.IdSemesterMain',array('SemesterMainName','SemesterMainCode'))       
                   ->where('s.IdStudentRegistration = ?',$registrationId) 
                   ->where('s.IdSemesterMain = ?',$idSemester) 
                   ->order("s.idstudentsemsterstatus");
                  
                  
        $result = $db->fetchAll($sql);
        
        //print_r($result);
        return $result;
	}
	
	public function getCountableRegisteredSemester($registrationId){
		
		$date = date("Y-m-d");
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))  
                   ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=s.IdSemesterMain',array('SemesterMainName','SemesterMainCode','SemesterFunctionType','IsCountable'))                    
                   ->where('s.IdStudentRegistration = ?',$registrationId)
                   ->where('sm.IsCountable = 1')
                   ->where('s.studentsemesterstatus = ?',130) 
                   ->where('sm.SemesterMainStartDate < ?',$date)
                   ->order("sm.SemesterMainStartDate");                  
                 
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	public function isRegisteredSemesterStatus($IdStudentRegistration,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))   
                   ->where('s.IdStudentRegistration = ?',$IdStudentRegistration) 
                   ->where('s.IdSemesterMain = ?',$idSemester);
                                     
        $result = $db->fetchRow($sql);
        
        return $result;
	}
	
	public function checkSemesterCourseRegistration($idsemester,$progid){
				
 		 $db = Zend_Db_Table::getDefaultAdapter();
        
 		 $curdate = date('Y-m-d');
       
         $select = $db->select()
                             ->from(array('ac'=>'tbl_activity_calender'))
                             ->where('? BETWEEN ac.StartDate AND ac.EndDate',$curdate)
                             ->where('ac.idActivity IN (18)')
                             ->where('ac.IdSemesterMain = ?',$idsemester);
     
        
        $row = $db->fetchRow($select);       
        return $row;		
	}	
	
	public function getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain){
		
			$db = Zend_Db_Table::getDefaultAdapter();      
			
			$sql = $db->select()	
					->from(array('sss' => 'tbl_studentsemesterstatus'))
					->where('IdStudentRegistration  = ?',$IdStudentRegistration)
					->where('IdSemesterMain = ?',$IdSemesterMain);
			$result = $db->fetchRow($sql);
			return $result;		
	}
	
	
	public function getStudentPreviousSemester($IdStudentRegistration){
			
		$db = Zend_Db_Table::getDefaultAdapter();      
			
		$curdate = date('Y-m-d');
		
		$sql = $db->select()	
				->from(array('sss' => 'tbl_studentsemesterstatus'))
				->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster =  sss.IdSemesterMain')
				->where('sss.IdStudentRegistration  = ?',$IdStudentRegistration)
				->where('sm.SemesterMainEndDate < ?',$curdate)
				->order('sm.SemesterMainStartDate DESC');
		$result = $db->fetchRow($sql);
		return $result;		
	}
	
	public function checkWithdrawalPeriod($idsemester,$progid){
				
 		$db = Zend_Db_Table::getDefaultAdapter();
        
 		 $curdate = date('Y-m-d');
       
         $select = $db->select()
                             ->from(array('ac'=>'tbl_activity_calender'))
                             ->where('? BETWEEN ac.StartDate AND ac.EndDate',$curdate)
                             ->where('ac.idActivity IN (40)')
                             ->where('ac.IdSemesterMain = ?',$idsemester);
     
        
        $row = $db->fetchRow($select);       
        return $row;		
	}	
	
 	public function getListSemesterRegistered($IdStudentRegistration){
        	
         	  $auth = Zend_Auth::getInstance(); 
        	  $db = Zend_Db_Table::getDefaultAdapter();
        	  
        	  $date = date('Y-m-d');
        	   
        	  $sql = $db->select()
        	  			->from(array('srs'=>'tbl_studentregsubjects'),array('IdSemesterMain'))
        	  			->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=srs.IdSemesterMain',array('SemesterMainName','SemesterMainCode','SemesterFunctionType','IsCountable'))
        	  			->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
        	  			->where('srs.Active != ?',3)
                        ->where('sm.IsCountable = ?', 1)
        	  			//->where("srs.exam_status NOT IN ()")
        	  		    //->where('sm.SemesterMainEndDate < ?',$date)
        	  		    ->where('sm.SemesterMainStartDate < ?',$date)
        	  			->order('sm.SemesterMainStartDate')
        	  			->group('srs.IdSemesterMain');
        	                   
        	$result = $db->fetchAll($sql);

        	//var_dump($result);
        	//below code was created to cater if there are subjct registered in tbl_studentregsubject but record in tbl_studentsemesterstatus 14-2-1014 yatie
	        foreach($result as $index=>$semester){
	        	
	        	//check if studentsemesterstatus exist
	        	$isExist = $this->getSemesterStatusBySemester($IdStudentRegistration,$semester['IdSemesterMain']);
	        	
	        	if(!$isExist){
	        		//create
	        		$data = array(  'IdStudentRegistration' => $IdStudentRegistration,									           
						            'idSemester' => $semester['IdSemesterMain'],
						            'IdSemesterMain' => $semester['IdSemesterMain'],								
						            'studentsemesterstatus' => 130, 	//Register idDefType = 32 (student semester status)
			                        'Level'=>1,
						            'UpdDate' => date ( 'Y-m-d H:i:s'),
						            'UpdUser' => $auth->getIdentity()->iduser
					       );											
					$idstudentsemsterstatus  = $this->addData($data);
					$result[$index]['idstudentsemsterstatus']=$idstudentsemsterstatus;
	        	}else{
	        		$result[$index]['idstudentsemsterstatus']=$isExist['idstudentsemsterstatus'];
	        	}
	        }
	        return $result;
        }
        
	public function getSemesterTranscript($registrationId){
		
		$date = date("Y-m-d");
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                   ->from(array('s' => $this->_name))  
                   ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=s.IdSemesterMain',array('SemesterMainName','SemesterMainCode','SemesterFunctionType','IsCountable'))                    
                   ->where('s.IdStudentRegistration = ?',$registrationId)
                   ->where('sm.IsCountable = 1')
                   ->where('s.studentsemesterstatus = ?',130) 
                   ->where('sm.SemesterMainStartDate < ?',$date)
                   ->order("sm.SemesterMainStartDate");                  
                 
        $result = $db->fetchAll($sql);
        return $result;
	}
	
}

?>