<?php 
class Registration_Model_DbTable_RegistrationItem extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_registration_item';
	protected $_primary = "ri_id";
	
	public function getDatabyId($id=0){
		$id = (int)$id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name) ) 
	                ->where($this->_primary.' = ' .$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function getDatabyIdActivity($id=0){
		$id = (int)$id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>'tbl_registration_item_activity') ) 
	                ->where('er.ria_id =?',$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	

	public function getData(){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name));		                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}

	public function getItems($fs_id, $semester_id )
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name))
					->where('er.semester_id=?',$semester_id)
					->where('er.program_id IN ( SELECT fsp_program_id FROM fee_structure_program WHERE fsp_fs_id = \''.$fs_id.'\')')
					->joinLeft(array('i'=>'tbl_definationms'), 'er.item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc'));		              

        $row = $db->fetchAll($select);
		return $row;
		
	}


	public function getPaginateData($search=false){
				
            $db = Zend_Db_Table::getDefaultAdapter();

            $select = $db->select()
                ->from(array('er'=>$this->_name))
                ->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster=er.semester_id', array('s.SemesterMainCode', 's.SemesterMainName'))
                ->joinLeft(array('p'=>'tbl_program'),'er.program_id=p.IdProgram', array('p.ProgramName as program_name'))
                ->joinLeft(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=er.program_scheme_id')
                ->joinLeft(array('e'=>'tbl_definationms'), 'ps.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc','mop_my'=>'BahasaIndonesia'))
                ->joinLeft(array('f'=>'tbl_definationms'), 'ps.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc','mos_my'=>'BahasaIndonesia'))
                ->joinLeft(array('g'=>'tbl_definationms'), 'ps.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc','pt_my'=>'BahasaIndonesia'))
                ->joinLeft(array('i'=>'tbl_definationms'), 'er.item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc'));	
                
            if ($search != false){
                if (isset($search['program']) && $search['program']!=''){
                    $select->where('er.program_id = ?', $search['program']);
                }
                if (isset($search['programscheme']) && $search['programscheme']!=''){
                    $select->where('er.program_scheme_id = ?', $search['programscheme']);
                }
                if (isset($search['semester']) && $search['semester']!=''){
                    $select->where('er.semester_id = ?', $search['semester']);
                }
                if (isset($search['item']) && $search['item']!=''){
                    $select->where('er.item_id = ?', $search['item']);
                }
            }    
                
            return $select;
	}
	
	public function getPaginateDataActivity($id){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name),array())
	                ->joinLeft(array('era'=>'tbl_registration_item_activity'), 'era.ria_ri_id = er.ri_id', array('*'))
	                ->joinLeft(array('tc'=>'tbl_activity_calender'), 'tc.IdActivity = era.ria_activity_id and er.semester_id = tc.IdSemesterMain', array('*'))
	                ->joinLeft(array('tca'=>'tbl_activity'), 'tca.idActivity = tc.IdActivity', array('*'))
                        ->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster=er.semester_id', array('s.SemesterMainCode', 's.SemesterMainName'))
                        ->joinLeft(array('p'=>'tbl_program'),'er.program_id=p.IdProgram', array('p.ProgramName as program_name'))
                        ->joinLeft(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=er.program_scheme_id')
                        ->joinLeft(array('e'=>'tbl_definationms'), 'ps.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc','mop_my'=>'BahasaIndonesia'))
                        ->joinLeft(array('f'=>'tbl_definationms'), 'ps.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc','mos_my'=>'BahasaIndonesia'))
                        ->joinLeft(array('g'=>'tbl_definationms'), 'ps.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc','pt_my'=>'BahasaIndonesia'))
                        ->joinLeft(array('i'=>'tbl_definationms'), 'er.item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc'))
					->where('era.ria_ri_id =?',$id)
					->group('era.ria_id');		                     
        return $select;
	}
	
        public function activityInfo($id){
            $db = Zend_Db_Table::getDefaultAdapter();

            $select = $db->select()
                ->from(array('er'=>$this->_name))
                ->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster=er.semester_id', array('s.SemesterMainCode', 's.SemesterMainName'))
                ->joinLeft(array('p'=>'tbl_program'),'er.program_id=p.IdProgram', array('p.ProgramName as program_name'))
                ->joinLeft(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=er.program_scheme_id')
                ->joinLeft(array('e'=>'tbl_definationms'), 'ps.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc','mop_my'=>'BahasaIndonesia'))
                ->joinLeft(array('f'=>'tbl_definationms'), 'ps.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc','mos_my'=>'BahasaIndonesia'))
                ->joinLeft(array('g'=>'tbl_definationms'), 'ps.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc','pt_my'=>'BahasaIndonesia'))
                ->joinLeft(array('i'=>'tbl_definationms'), 'er.item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc'))
                ->where('er.ri_id = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
	
	public function addData($data){
		$this->insert($data);
		$db = Zend_Db_Table::getDefaultAdapter();
		return $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		$this->update($data,"ri_id ='".$id."'");
	}
	
	public function deleteData($add_id){
		$this->delete(array("ri_id = ?" =>$add_id));
	}
	
	public function getStudentItems($program_id, $semester_id, $scheme_id, $chargable=0 )
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name))
					->where('er.semester_id=?',$semester_id)
					->where('er.program_id=?',$program_id)
					->where('er.program_scheme_id=?',$scheme_id)
					->joinLeft(array('i'=>'tbl_definationms'), 'er.item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc','item_code'=>'i.DefinitionCode'));	
		if ( $chargable ) 
		{
			$select->where('er.chargable = 1');
		}

		$select->order('i.defOrder ASC');

        $row = $db->fetchAll($select);
		return $row;
	}	
	
	
	public function getRepeatRegItems($program_id, $semester_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name))
					->where('er.semester_id=?',$semester_id)
					->where('er.program_id=?',$program_id)
					->joinLeft(array('i'=>'tbl_definationms'), 'er.item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc','item_code'=>'i.DefinitionCode'))
					->group('er.item_id')
					->order('i.defOrder ASC');

        $row = $db->fetchAll($select);
		return $row;
	}

	public function getRepeatRegItems2($program_id, $semester_id, $scheme)
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select()
			->from(array('er'=>$this->_name))
			->where('er.semester_id=?',$semester_id)
			->where('er.program_id=?',$program_id)
			->where('er.program_scheme_id=?',$scheme)
			->joinLeft(array('i'=>'tbl_definationms'), 'er.item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc','item_code'=>'i.DefinitionCode'))
			->group('er.item_id')
			->order('i.defOrder ASC');

		$row = $db->fetchAll($select);
		return $row;
	}

	public function getItemInfo($rdid=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from( array('d'=>'tbl_definationms') ) 
	                ->join( array('rd'=>'tbl_studentregsubjects_detail'),'rd.item_id=d.idDefinition',array())
	                ->where('rd.id = ?',$rdid);		                    
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function addActivity($data){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->insert('tbl_registration_item_activity',$data);
		return $db->lastInsertId();
	}
	
	public function deleteActivity($add_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->delete('tbl_registration_item_activity',array("ria_id = ?" =>$add_id));
	}
	
	public function getSubjectItems($formdata,$transaction_id,$prereg_subject_list )
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$IdSemester = $formdata['IdSemester'][$transaction_id];
		$IdProgram = $formdata['IdProgram'][$transaction_id];
		$IdProgramScheme = $formdata['IdProgramScheme'][$transaction_id];

		foreach($prereg_subject_list as $index=>$subject){
			
		
			$select = $db->select()
		                ->from(array('er'=>$this->_name),array('item_id'=>'er.item_id'))
						->where('er.semester_id=?',$IdSemester)
						->where('er.program_id=?',$IdProgram)
						->where('er.program_scheme_id=?',$IdProgramScheme)
						->where('er.mandatory = 1');
	
	        $rows = $db->fetchAll($select);
	        $prereg_subject_list[$index]['item'] = $rows;
		}
		
		return $prereg_subject_list;
	}	
	
	public function getItemDetail($program_id, $semester_id, $scheme_id, $item_id )
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name))
					->where('er.semester_id=?',$semester_id)
					->where('er.program_id=?',$program_id)
					->where('er.program_scheme_id=?',$scheme_id)
					->where('er.item_id=?',$item_id);

        $row = $db->fetchRow($select);
		return $row;
	}	
	
}
?>