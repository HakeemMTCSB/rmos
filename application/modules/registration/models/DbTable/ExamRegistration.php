<?php 
class Registration_Model_DbTable_ExamRegistration extends Zend_Db_Table_Abstract
{
    protected $_name = 'exam_registration';
	protected $_primary = "er_id";
	
	public function getDatabyId($id=0){
		$id = (int)$id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name) ) 
	                ->where($this->_primary.' = ' .$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function getData(){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name));		                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function addData($data){
		$this->insert($data);
		$db = Zend_Db_Table::getDefaultAdapter();
		return $db->lastInsertId();
	}
	
	public function updateData($data,$id){
	
		$this->update($data,"er_id ='".$id."'");
	}
	
	public function deleteData($add_id){
		$this->delete("er_id ='".$add_id."'");
	}
	
	
	public function getStudentRegister($ecid,$idSemester,$idSubject){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$semester_all = $this->getSemester($idSemester);
		
		$select1 = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=er.er_idStudentRegistration',array('registrationId'))
	                ->join(array('srs'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration=srs.IdStudentRegistration AND srs.IdSubject=er.er_idSubject AND srs.IdSemesterMain=er.er_idSemester',array())
	                ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
	                ->where('er.er_ec_id = ?',$ecid)
	                ->where('srs.Active != ?',3)
	                ->where('sr.profileStatus = ?',92)
	                //->where('er.er_idSemester = ?',$idSemester)
	                ->where('er.er_idSemester IN (?)',$semester_all)
	                ->where('er.er_idSubject = ?',$idSubject);
	               
	                		                     
      $select2 = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=er.er_idStudentRegistration',array('registrationId'))
	                ->join(array('srs'=>'tbl_studentregsubjects'),'sr.IdStudentRegistration=srs.IdStudentRegistration AND srs.IdSubject=er.er_idSubject AND srs.IdSemesterMain=er.er_idSemester',array())
	                ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))
	                ->where('er.er_ec_id = ?',$ecid)
	                ->where('srs.Active != ?',3)
	                ->where('sr.profileStatus = ?',92)
	                //->where('er.er_idSemester = ?',$idSemester)
	                ->where('er.er_idSemester IN (?)',$semester_all)
	                ->where('er.er_idSubject = ?',$idSubject)
	                ->where('srs.audit_program IS NOT NULL') ;
	                
        $select = $db->select()->union(array($select1, $select2)) 
        						->order('appl_fname')
	                			->order('appl_lname');;
	    $row = $db->fetchAll($select);
		return $row;
		
	}
	
	
	public function getStudentListAttendance($ecid,$idSemester,$idSubject){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select1 = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=er.er_idStudentRegistration',array('registrationId','IdProgram'))
	                ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=er.er_idStudentRegistration AND srs.IdSubject=er.er_idSubject AND srs.IdSemesterMain=er.er_idSemester',array('IdStudentRegSubjects','exam_status','audit_program'))
	                ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))	                
	                ->where('er.er_ec_id = ?',$ecid)
	                ->where('srs.IdSemesterMain = ?',$idSemester)
	                ->where('srs.IdSubject = ?',$idSubject)
	                ->where('sr.profileStatus = ?',92)
                    ->where('srs.Active != ?',3)
				    ->where('sr.registrationId != ?',"1409999")   
				    ->where('IFNULL("srs.exam_status","Z") NOT IN ("EX","CT","U")') ;   
                
        $semesterArr = $this->getSemester($idSemester);
        
        $select2 = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=er.er_idStudentRegistration',array('registrationId','IdProgram'))
	                ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration=er.er_idStudentRegistration AND srs.IdSubject=er.er_idSubject AND srs.IdSemesterMain=er.er_idSemester',array('IdStudentRegSubjects','exam_status','audit_program'))
	                ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_mname','appl_lname'))	    
	                ->where('srs.audit_program IS NOT NULL')               
	                ->where('er.er_ec_id = ?',$ecid)
	                ->where('srs.IdSemesterMain IN (?)',$semesterArr)
	                ->where('srs.IdSubject = ?',$idSubject)
	                ->where('sr.profileStatus = ?',92)
                    ->where('srs.Active != ?',3);
        
	    $select = $db ->select()
	    		      ->union(array($select1, $select2))
	    		      ->order(new Zend_Db_Expr("CASE WHEN IdProgram=3 THEN 0 ELSE 1 END")) 
	                  ->order('IdProgram')     
	                  ->order('appl_fname')
	                  ->order('appl_lname')
	                  ->order('registrationId');
	                  
        $rows = $db->fetchAll($select);
        
       /* if(count($rows)>0){
	        foreach($rows as $key=>$row){
	        	if($row['exam_status']=='U' && $row['audit_program']==''){
	        		unset($rows[$key]);
	        	}
	        }
        }*/
		return $rows;
		
	}
	
	public function getSemester($idSemester)
    {
       	$db = Zend_Db_Table::getDefaultAdapter();

       	$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
        $semester = $semesterDB->getData($idSemester);
       
        
        $sql = $db->select()
                  ->from(array('sm' => 'tbl_semestermaster'),array('IdSemesterMaster'))                 
                  ->where('sm.AcademicYear = ?',$semester['AcademicYear'])
                  ->where('sm.sem_seq = ?',$semester['sem_seq']);
		$result = $db->fetchAll($sql); 
        
        return $result;
    	
    }
	
	public function getExamRegistrationBySubject($idStudentRegistration,$idSemester,$idSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('ec'=>'tbl_exam_center'),'ec.ec_id=er.er_ec_id',array('ec_name'))
	                ->where('er_idStudentRegistration = ?',$idStudentRegistration)
	                ->where('er_idSemester = ?',$idSemester)
	                ->where('er_idSubject = ?',$idSubject);	                                   
        
        $row = $db->fetchRow($select);
		return $row;
				
	}
	
	public function deleteRegisterExamCenter($idStudentRegistration,$idSemester,$idSubject){		
	  $this->delete('er_idSemester  =' . (int)$idSemester .' AND er_idStudentRegistration = '. (int)$idStudentRegistration .' AND 	er_idSubject = '. (int)$idSubject);
	}
	
	
	public function getDataExamRegistrationBySubject($idStudentRegistration,$idSemester,$idSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('er'=>$this->_name))	               
	                ->joinLeft(array('c'=>'tbl_countries'),'c.idCountry=er.er_idCountry',array('CountryName'))
	                ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=er.er_idCity',array('CityName'))
	                ->where('er_idStudentRegistration = ?',$idStudentRegistration)
	                ->where('er_idSemester = ?',$idSemester)
	                ->where('er_idSubject = ?',$idSubject);	                                   
        
        $row = $db->fetchRow($select);
		return $row;
				
	}
	
	
	public function getAbsenteesByProgram($idSemester,$idProgram,$attendance_status=null){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$examScheduleDB  = new Reports_Model_DbTable_ExamSchedule();
		
	    $select = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=er.er_idStudentRegistration',array('registrationId','IdProgram'))
	                ->join(array('sp'=>'student_profile'),'sp.id=sr.sp_id',array('appl_fname','appl_lname'))
	                ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=er.er_idSubject',array('SubCode','SubjectName'))
		            ->joinLeft(array('ec'=>'tbl_exam_center'),'ec.ec_id=er.er_ec_id',array('ec_code'))
	                ->where('er.er_idSemester = ?',$idSemester)
	                ->where('sr.IdProgram = ?',$idProgram)
	                ->order('sp.appl_fname')
	                ->order('sp.appl_lname');
        
	                if(isset($attendance_status) && $attendance_status!=''){
	                	$select->where('er.er_attendance_status = ?',$attendance_status);
	                }else{
	                	$select->where('IFNULL(er.er_attendance_status,"0") IN (396,398)');
	                }
	                
        $rows = $db->fetchAll($select);
        
        $rownum = 0;
        $prevregid = 0;
        foreach($rows as $index=>$row){
        	
        		if($row['er_idStudentRegistration']==$prevregid){
        			$rownum = $rownum+1;
        		}else{
        			$rownum = 1;
        		}
        		
        		$prevregid = $row['er_idStudentRegistration'];
        		
        	 	//get subject
	        	$select_subject = $db->select()
				                ->from(array('er'=>$this->_name),array('total_subject'=>'COUNT(er.er_id)'))
				                ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=er.er_idStudentRegistration',array('registrationId','IdProgram'))
				                ->where('er.er_idSemester = ?',$idSemester)
			               	    ->where('sr.IdProgram = ?',$idProgram)
			               	    ->where('er.er_idStudentRegistration = ?',$row['er_idStudentRegistration']);
			               	    
			        			if(isset($attendance_status) && $attendance_status!=''){
				                	$select_subject->where('er.er_attendance_status = ?',$attendance_status);
				                }else{
				                	$select_subject->where('IFNULL(er.er_attendance_status,"0") IN (396,398)');
				                }
	                
		        $row_subject = $db->fetchRow($select_subject);
		        $rows[$index]['total_subject']=$row_subject['total_subject'];
		        $rows[$index]['row_num']=$rownum;
		        
		        
		        
		        //get exam date
		        $schedule =  $examScheduleDB->getExamDateByCourse($idSemester,$row['er_idSubject'],$row['er_ec_id']);
		        $rows[$index]['exam_date']=$schedule['es_date'];
        }
                
		return $rows;
		
	}

	public function getScheduleBySubject($idSemester,$idSubject,$ec_id=0){
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select()
			->from(array('es'=>'exam_schedule'))
			->where('es.es_semester = ?',$idSemester)
			->where('es.es_course = ?',$idSubject);

		if(isset($ec_id) && $ec_id!=0){
			$select->where('es.es_exam_center = ?',$ec_id);
		}

		$row = $db->fetchRow($select);

		if(!$row){

			$select2 = $db->select()
				->from(array('es'=>'exam_schedule'))
				->where('es.es_semester = ?',$idSemester)
				->where('es.es_course = ?',$idSubject);

			$row2 = $db->fetchRow($select2);
			$row = $row2;
		}
		return $row;
	}
}
?>