<?php
class Registration_Model_DbTable_Adddropsubject extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	
	
	
	public function SubjectDetails($lintIdStudentRegistration,$idlandscape){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 		 ->from(array("sr" =>"tbl_studentregsubjects"),array('sr.IdStudentRegSubjects'))
		 			 	// ->join(array("ls"=>"tbl_landscapesubject"),'sr.IdSubject=ls.IdSubject')
		 				 ->join(array("sm"=>"tbl_subjectmaster"),'sr.IdSubject=sm.IdSubject')
		 				 ->where("sr.IdStudentRegistration = ?",$lintIdStudentRegistration)
		 				// ->where("ls.IdLandscape = ?",$idlandscape)
		 				 ->where("sr.Active = 1");
		 				// ->where("ls.Compulsory = 0");
		 				 //echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function CompSubjectDetails($lintIdStudentRegistration,$idlandscape){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 		 ->from(array("sr" =>"tbl_studentregsubjects"),array('sr.IdStudentRegSubjects'))
		 			 	 ->join(array("ls"=>"tbl_landscapesubject"),'sr.IdSubject=ls.IdSubject')
		 				 ->join(array("sm"=>"tbl_subjectmaster"),'sr.IdSubject=sm.IdSubject')
		 				 ->where("sr.IdStudentRegistration = ?",$lintIdStudentRegistration)
		 				 ->where("ls.IdLandscape = ?",$idlandscape)
		 				 ->where("sr.Active = 1")
		 				 ->where("ls.Compulsory = 1");
		 				 //echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function DropSubjectDetails($lintIdStudentRegistration,$idlandscape){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 		 ->from(array("sr" =>"tbl_studentregsubjects"),array('sr.IdStudentRegSubjects'))
		 			 	// ->join(array("ls"=>"tbl_landscapesubject"),'sr.IdSubject=ls.IdSubject')
		 				 ->join(array("sm"=>"tbl_subjectmaster"),'sr.IdSubject=sm.IdSubject')
		 				 ->where("sr.IdStudentRegistration = ?",$lintIdStudentRegistration)
		 				// ->where("ls.IdLandscape = ?",$idlandscape)
		 				 ->where("sr.Active = 0");
		 				// ->where("ls.Compulsory = 0");
		 				 //echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetApplicantNameList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("sa"=>"tbl_studentapplication"),array("key"=>"sa.IdApplication","value"=>"CONCAT_WS(' ',IFNULL(sa.FName,''),IFNULL(sa.MName,''),IFNULL(sa.LName,''))"))
		 				 ->join(array("a"=>"tbl_studentregistration"),'sa.registrationId=a.registrationId')
		 				 ->group("sa.registrationId")
		 				 ->order("sa.FName");
		 
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetSemesterList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_semester"),array("key"=>"IdSemester","value"=>"SemesterCode"))
		 				 ->where("a.Active = 1")
		 				 ->where('a.SemesterCode <> ?',' ')
		 				 ->order("a.SemesterCode");
		 
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fngetRegisteredStudentDtls() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();			
	  		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("sr"=>"tbl_studentregistration"),array('sr.registrationId AS StudentId','sr.registrationId','sr.IdStudentRegistration','sr.FName','sr.MName','sr.LName'))
       								->join(array("sp"=>"student_profile"),'sp.appl_id = sr.IdApplication')
       								->joinLeft(array("l"=>"tbl_landscape"),'sr.IdLandscape = l.IdLandscape AND (l.AddDrop = 1)',array())
       								->joinLeft(array("p"=>"tbl_program"),'sr.IdProgram = p.IdProgram',array("p.ProgramName"))
       								->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = sr.profileStatus',array('d.DefinitionDesc'));
                                                              
																//->where('sr.profileStatus = ?',92);
                                                                //->where("sr.profileStatus != '249' ");
									//->order('b.IdStudentRegistration DESC')
									//->group('b.IdStudentRegistration');
									
       		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
       		return $larrResult;
     }
     
	public function fnSearchRegisteredStudentApplication($post = array()) { //Function to get the user details
	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();			
	  		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("sr"=>"tbl_studentregistration"),array('sr.registrationId AS StudentId','sr.registrationId','sr.IdStudentRegistration','sr.FName','sr.MName','sr.LName'))
       								->join(array("l"=>"tbl_landscape"),'sr.IdLandscape = l.IdLandscape AND (l.AddDrop = 1)',array())
       								->joinLeft(array("a"=>"tbl_studentapplication"),'sr.registrationId=a.registrationId',array())
       								->joinLeft(array("p"=>"tbl_program"),'sr.IdProgram = p.IdProgram',array("p.ProgramName"));
       								//->where('sr.profileStatus = ?',92);
       														
			if(isset($post['field23']) && !empty($post['field23']) ){
				$lstrSelect = $lstrSelect->join(array('prg'=>'tbl_program'),'prg.IdProgram=sr.IdProgram');
				$lstrSelect = $lstrSelect->join(array('schm'=>'tbl_scheme'),'prg.IdScheme=schm.IdScheme');
				$lstrSelect = $lstrSelect->where("schm.IdScheme = ?",$post['field23']);
			}						
			if(isset($post['field24']) && !empty($post['field24']) ){
				$lstrSelect = $lstrSelect->where("sr.IdIntake = ?",$post['field24']);
			}
			if(isset($post['field8']) && !empty($post['field8']) ){
				$lstrSelect = $lstrSelect->where("sr.IdProgram = ?",$post['field8']);
										
			}				
       		if(isset($post['field2']) && !empty($post['field2'])){
				$lstrSelect	->where('sr.registrationId like "%" ? "%"',$post['field2']);   
			}    				
			if(isset($post['field25']) && !empty($post['field25'])){
				$lstrSelect = $lstrSelect->where("sr.IdBranch = ?",$post['field25']);
			}
			if(isset($post['field3']) && !empty($post['field3'])){
				$lstrSelect	->where('sr.FName like "%" ? "%"',$post['field3'])   
							->orwhere('sr.MName like "%" ? "%"',$post['field3'])
							->orwhere('sr.LName like "%" ? "%"',$post['field3']);
			}
			if(isset($post['field5']) && !empty($post['field5'])){
                            
                                $semIDs = explode('_',$post['field5']);
                                if($semIDs['1']=='main') { $idSem = $semIDs['0'] ;  }
                                if($semIDs['1']=='detail') { $idSem = $semIDs['0'] ;  }
                            
                            
				$lstrSelect = $lstrSelect->joinLeft(array("s"=>"tbl_studentsemesterstatus"),'sr.IdStudentRegistration = s.IdStudentRegistration',array())
										 -> where("s.idSemester = ?",$idSem)
										 ->orwhere('s.IdSemesterMain = ?',$idSem)
                                                                                 //-> where($where)
										 ->order('s.UpdDate DESC');
										 //->limit('1');
			}
                        //echo $lstrSelect;
       	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	 
	   public function getCompleteStudentDetails($lintidapplicant) { 
	     	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a' => 'tbl_studentapplication'),array('a.*'))
							->join(array('e'=>'tbl_studentregistration'),'a.IdApplication = e.IdApplication')
                			->join(array('b'=>'tbl_collegemaster'),'a.idCollege  = b.IdCollege')
                			->join(array('c'=>'tbl_program'),'a.IDCourse = c.IdProgram')
                			->join(array('d'=>'tbl_landscape'),'e.IdLandscape = d.IdLandscape')
                			->join(array('f'=>'tbl_definationms'),'d.LandscapeType = f.idDefinition')
                			->where('a.IdApplication = ?',$lintidapplicant);
      
			$result = $db->fetchRow($sql);
			return $result;
		}
		
		public function getstudentprofiledetails($IdRegistration){
			$db = Zend_Db_Table::getDefaultAdapter();
			 $sql = $db->select()
				      ->from(array("a"=>"tbl_studentregistration"),array("a.*"))
				      ->joinLeft(array("b"=>"tbl_definationms"),"a.MaritalStatus = b.idDefinition",array("b.DefinitionDesc AS Marital"))
				      ->joinLeft(array("c"=>"tbl_countries"),"a.Nationality = c.idCountry",array("c.CountryName"))
				      ->joinLeft(array("d"=>"tbl_definationms"),"a.Religion = d.idDefinition",array("d.DefinitionDesc AS Relig"))
				      ->joinLeft(array("e"=>"tbl_definationms"),"a.Race = e.idDefinition",array("e.DefinitionDesc AS Rac"))				      
				      ->joinLeft(array("f"=>"tbl_definationms"),"a.RelationshipType = f.idDefinition",array("f.DefinitionDesc AS Relation"))				      
				      ->joinLeft(array("g"=>"tbl_countries"),"a.EmergencyCountry = g.idCountry",array("g.CountryName AS Emergencycountry"))
      				  ->joinLeft(array("h"=>"tbl_state"),"a.EmergencyState = h.idState",array("h.StateName"))
      				  ->joinLeft(array("i"=>"tbl_city"),"a.EmergencyCity = i.idCity",array("i.CityName"))
      				  ->joinLeft(array("j"=>"tbl_program"),"a.IdProgram = j.IdProgram",array("j.ProgramName"))
      				  ->joinLeft(array("k"=>"tbl_scheme"),"j.IdScheme = k.IdScheme",array("k.EnglishDescription AS scheme"))
      				  ->joinLeft(array("l"=>"tbl_definationms"),"a.SpecialTreatmentType = l.idDefinition",array("l.DefinitionDesc AS SpecialTreatType"))				      
      				  ->joinLeft(array("m"=>"tbl_definationms"),"a.SpecialTreatment = m.idDefinition",array("m.DefinitionDesc AS SpecialTreat"))				      				      
				      ->where("a.IdStudentRegistration = ?",$IdRegistration);
		    $result = $db->fetchRow($sql);
		    if(isset($result['OldIdStudentRegistration'])){
		    $sql = $db->select()
				      ->from(array("a"=>"tbl_studentregistration"),array("a.registrationId AS oldregistrationId"))
				      ->where('a.IdStudentRegistration = ?',$result['OldIdStudentRegistration']);
			$result1 = $db->fetchRow($sql);
		    
			$result['oldregistrationId'] = $result1['oldregistrationId'];
		    }
			return $result;
		}
                
                
                public function getstudentprofiledetailstwo($IdRegistration) { 
                    $db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()
				      ->from(array("a"=>"tbl_studentregistration"),array("a.*"))
				      ->joinLeft(array("b"=>"tbl_definationms"),"a.MaritalStatus = b.idDefinition",array("b.DefinitionDesc AS Marital"))
				      ->joinLeft(array("c"=>"tbl_countries"),"a.Nationality = c.idCountry",array("c.CountryName"))
				      ->joinLeft(array("d"=>"tbl_definationms"),"a.Religion = d.idDefinition",array("d.DefinitionDesc AS Relig"))
				      ->joinLeft(array("e"=>"tbl_definationms"),"a.Race = e.idDefinition",array("e.DefinitionDesc AS Rac"))				      
				      ->joinLeft(array("f"=>"tbl_definationms"),"a.RelationshipType = f.idDefinition",array("f.DefinitionDesc AS Relation"))				      
				      ->joinLeft(array("g"=>"tbl_countries"),"a.EmergencyCountry = g.idCountry",array("g.CountryName AS Emergencycountry"))
      				  ->joinLeft(array("h"=>"tbl_state"),"a.EmergencyState = h.idState",array("h.StateName"))
      				  ->joinLeft(array("i"=>"tbl_city"),"a.EmergencyCity = i.idCity",array("i.CityName"))
      				  ->joinLeft(array("j"=>"tbl_program"),"a.IdProgram = j.IdProgram",array("j.ProgramName"))
      				  ->joinLeft(array("k"=>"tbl_scheme"),"j.IdScheme = k.IdScheme",array("k.EnglishDescription AS scheme"))
      				  ->joinLeft(array("l"=>"tbl_definationms"),"a.SpecialTreatmentType = l.idDefinition",array("l.DefinitionDesc AS SpecialTreatType"))				      
      				  ->joinLeft(array("m"=>"tbl_definationms"),"a.SpecialTreatment = m.idDefinition",array("m.DefinitionDesc AS SpecialTreat"))				      
      				  				      
      				  
				      				      
				      ->where("a.registrationId = ?",$IdRegistration);
		    $result = $db->fetchRow($sql);
			return $result;
                    
                    
                }
                
                
		
		public function getstudentprofilesemdetails($IdRegistration){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()
					  ->from(array('a'=>'tbl_studentsemesterstatus'),array(''))
					  ->joinLeft(array('b'=>'tbl_semester'),'a.idSemester = b.IdSemester',array('b.SemesterCode AS semcode'))
					  ->joinLeft(array('c'=>'tbl_semestermaster'),'a.IdSemesterMain = c.IdSemesterMaster',array('c.SemesterMainCode AS semcodemain'))
					  ->joinLeft(array('d'=>'tbl_definationms'),'a.studentsemesterstatus = d.idDefinition',array('d.DefinitionDesc AS Status'))
					  ->where('a.IdStudentRegistration = ?',$IdRegistration);
				  
			$result = $db->fetchAll($sql);
			return $result;	  
		}
		
		public function getstudentcourses($IdStudentRegistration,$Idlandscape){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()
					  ->from(array('a'=>'tbl_studentregsubjects'),array())
					  ->joinLeft(array('b'=>'tbl_subjectmaster'),'a.IdSubject = b.IdSubject',array('b.SubjectName','b.SubCode AS SubjectCode'))
					  ->joinLeft(array('c'=>'tbl_semestermaster'),'a.IdSemesterMain = c.IdSemesterMaster',array('c.SemesterMainCode'))
					  ->joinLeft(array('d'=>'tbl_semester'),'a.IdSemesterDetails = d.IdSemester',array('d.SemesterCode'))
					  ->joinLeft(array('e'=>'tbl_landscapesubject'),'a.IdSubject = e.IdSubject',array('e.CreditHours'))
					  ->where("e.IdLandscape = ?",$Idlandscape)
					  ->where('a.IdStudentRegistration = ?',$IdStudentRegistration)
					  ->order("c.UpdDate DESC")
					  ->order("c.SemesterMainCode DESC")
					  ->order("d.SemesterCode DESC")
					  ->order("b.SubCode")
					  ->group('a.IdSubject');
					  
			$result = $db->fetchAll($sql);
			return $result;
		}
		
		public function fnupdateStudent($lobjFormData, $registrationId){
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_studentregistration";
			$where = 'IdStudentRegistration = ' . $registrationId;
			print_r($lobjFormData);
		    if($lobjFormData['HomePhonecountrycode'] != ''){
				$lobjFormData['HomePhone'] = $lobjFormData['HomePhonecountrycode'] . "-" . $lobjFormData['HomePhonestatecode'] . "-" . $lobjFormData['HomePhone'];
			    unset($lobjFormData['HomePhonecountrycode']);
			    unset($lobjFormData['HomePhonestatecode']);
		    }
		    else{
				unset($lobjFormData['HomePhonecountrycode']);
			    unset($lobjFormData['HomePhonestatecode']);
		    }
		    
		    if($lobjFormData['CellPhone'] != ''){
			    $lobjFormData['CellPhone'] = $lobjFormData['CellPhonecountrycode'] . "-" . $lobjFormData['CellPhone'];
			    unset($lobjFormData['CellPhonecountrycode']);
		    }
		    else{
			    unset($lobjFormData['CellPhonecountrycode']);
		    }
			
		    if($lobjFormData['Fax'] != ''){
			    $lobjFormData['Fax'] = $lobjFormData['Faxcountrycode'] . "-" . $lobjFormData['Faxstatecode'] . "-" . $lobjFormData['Fax'];
			    unset($lobjFormData['Faxcountrycode']);
			    unset($lobjFormData['Faxstatecode']);
			}
			else{
				unset($lobjFormData['Faxcountrycode']);
			    unset($lobjFormData['Faxstatecode']);
			}
			if(isset($lobjFormData['EmergencyHomePhonecountrycode']) && $lobjFormData['EmergencyHomePhonecountrycode'] != ''){
				$lobjFormData['EmergencyHomePhone'] = $lobjFormData['EmergencyHomePhonecountrycode'] . "-" . $lobjFormData['EmergencyHomePhonestatecode'] . "-" . $lobjFormData['EmergencyHomePhone'];
				unset($lobjFormData['EmergencyHomePhonecountrycode']);
    			unset($lobjFormData['EmergencyHomePhonestatecode']);
			}
			if(isset($lobjFormData['EmergencyHomePhonecountrycode'])){
				unset($lobjFormData['EmergencyHomePhonecountrycode']);
    			unset($lobjFormData['EmergencyHomePhonestatecode']);
			}
			if(isset($lobjFormData['EmergencyOffPhonecountrycode']) && $lobjFormData['EmergencyOffPhonecountrycode'] != ''){
				$lobjFormData['EmergencyOffPhone'] = $lobjFormData['EmergencyOffPhonecountrycode'] . "-" . $lobjFormData['EmergencyOffPhonestatecode'] . "-" . $lobjFormData['EmergencyOffPhone'];
				unset($lobjFormData['EmergencyOffPhonecountrycode']);
    			unset($lobjFormData['EmergencyOffPhonestatecode']);
			}
			if(isset($lobjFormData['EmergencyOffPhonecountrycode'])){
				unset($lobjFormData['EmergencyOffPhonecountrycode']);
    			unset($lobjFormData['EmergencyOffPhonestatecode']);
			}
			if(isset($lobjFormData['EmergencyCellPhonecountrycode']) && $lobjFormData['EmergencyCellPhonecountrycode'] != ''){
				$lobjFormData['EmergencyCellPhone'] = $lobjFormData['EmergencyCellPhonecountrycode'] . "-" . $lobjFormData['EmergencyCellPhone'];
				unset($lobjFormData['EmergencyCellPhonecountrycode']);
			}
			if(isset($lobjFormData['EmergencyCellPhonecountrycode'])){
				unset($lobjFormData['EmergencyCellPhonecountrycode']);
			}
			print_r($lobjFormData);
			$db->update($table,$lobjFormData,$where);
		}
		
		public function fngetstudentdetail($registrationid){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()
				      ->from(array("a"=>"tbl_studentregistration"),array("a.*"))
				      ->joinLeft(array("b"=>"tbl_definationms"),"a.MaritalStatus = b.idDefinition",array("b.DefinitionDesc AS Marital"))
				      ->joinLeft(array("c"=>"tbl_countries"),"a.Nationality = c.idCountry",array("c.CountryName"))
				      ->joinLeft(array("d"=>"tbl_definationms"),"a.Religion = d.idDefinition",array("d.DefinitionDesc AS Relig"))
				      ->joinLeft(array("e"=>"tbl_definationms"),"a.Race = e.idDefinition",array("e.DefinitionDesc AS Rac"))				      
				      ->joinLeft(array("f"=>"tbl_definationms"),"a.RelationshipType = f.idDefinition",array("f.DefinitionDesc AS Relation"))				      
				      ->joinLeft(array("g"=>"tbl_countries"),"a.EmergencyCountry = g.idCountry",array("g.CountryName AS Emergencycountry"))
      				  ->joinLeft(array("h"=>"tbl_state"),"a.EmergencyState = h.idState",array("h.StateName"))
      				  ->joinLeft(array("i"=>"tbl_city"),"a.EmergencyCity = i.idCity",array("i.CityName"))
      				  ->joinLeft(array("j"=>"tbl_program"),"a.IdProgram = j.IdProgram",array("j.ProgramName"))
      				  ->joinLeft(array("k"=>"tbl_scheme"),"j.IdScheme = k.IdScheme",array("k.EnglishDescription AS scheme"))
      				  ->joinLeft(array("l"=>"tbl_definationms"),"a.SpecialTreatmentType = l.idDefinition",array("l.DefinitionDesc AS SpecialTreatType"))				      
      				  ->joinLeft(array("m"=>"tbl_definationms"),"a.SpecialTreatment = m.idDefinition",array("m.DefinitionDesc AS SpecialTreat"))				      
      				  				      
      				  
				      				      
				      ->where("a.registrationId = ?",$registrationid);
		    $result = $db->fetchRow($sql);
			return $result;
		}
		
//		public function getstudentprofilesemdetails($IdRegistration){
//			$db = Zend_Db_Table::getDefaultAdapter();
//			$sql = $db->select()
//					  ->from(array('a'=>'tbl_studentsemesterstatus'),array(''))
//					  ->joinLeft(array('b'=>'tbl_semester'),'a.idSemester = b.IdSemester',array('b.SemesterCode AS semcode'))
//					  ->joinLeft(array('c'=>'tbl_semestermaster'),'a.IdSemesterMain = c.IdSemesterMaster',array('c.SemesterMainCode AS semcodemain'))
//					  ->joinLeft(array('d'=>'tbl_definationms'),'a.studentsemesterstatus = d.idDefinition',array('d.DefinitionDesc AS Status'))
//					  ->where('a.IdStudentRegistration = ?',$IdRegistration);
//			$result = $db->fetchAll($sql);
//			return $result;	  
//		}
//		
//		public function getstudentcourses($IdStudentRegistration){
//			$db = Zend_Db_Table::getDefaultAdapter();
//			$sql = $db->select()
//					  ->from(array('a'=>'tbl_studentregsubjects'),array())
//					  ->joinLeft(array('b'=>'tbl_subject'),'a.IdSubject = b.IdSubject',array('b.SubjectName','b.SubjectCode'))
//					  ->where('a.IdStudentRegistration = ?',$IdStudentRegistration);
//					  
//			$result = $db->fetchAll($sql);
//			return $result;
//		}
		
//		public function fnupdateStudent($lobjFormData, $registrationId){
//			$db = Zend_Db_Table::getDefaultAdapter();
//			$table = "tbl_studentregistration";
//			echo $where = 'IdStudentRegistration = ' . $registrationId;
//			print_r($lobjFormData);
//		    if($lobjFormData['HomePhonecountrycode'] != ''){
//				$lobjFormData['HomePhone'] = $lobjFormData['HomePhonecountrycode'] . "-" . $lobjFormData['HomePhonestatecode'] . "-" . $lobjFormData['HomePhone'];
//			    unset($lobjFormData['HomePhonecountrycode']);
//			    unset($lobjFormData['HomePhonestatecode']);
//		    }
//		    else{
//				unset($lobjFormData['HomePhonecountrycode']);
//			    unset($lobjFormData['HomePhonestatecode']);
//		    }
//		    
//		    if($lobjFormData['CellPhone'] != ''){
//			    $lobjFormData['CellPhone'] = $lobjFormData['CellPhonecountrycode'] . "-" . $lobjFormData['CellPhone'];
//			    unset($lobjFormData['CellPhonecountrycode']);
//		    }
//		    else{
//			    unset($lobjFormData['CellPhonecountrycode']);
//		    }
//			
//		    if($lobjFormData['Fax'] != ''){
//			    $lobjFormData['Fax'] = $lobjFormData['Faxcountrycode'] . "-" . $lobjFormData['Faxstatecode'] . "-" . $lobjFormData['Fax'];
//			    unset($lobjFormData['Faxcountrycode']);
//			    unset($lobjFormData['Faxstatecode']);
//			}
//			else{
//				unset($lobjFormData['Faxcountrycode']);
//			    unset($lobjFormData['Faxstatecode']);
//			}
//		    $db->update($table,$lobjFormData,$where);
//		}
//		
		public function fngetregstudentdetail($registrationid){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a' => 'tbl_studentregistration'),array('a.IdLandscape','a.IdStudentRegistration','a.registrationId AS StudentId','a.FName','a.MName','a.LName'))
							->joinLeft(array('b'=>	'tbl_studentapplication'),'a.registrationId=b.registrationId',array())
							->joinLeft(array('c'=> 'tbl_program'),'a.IdProgram = c.IdProgram',array("c.ProgramName"))
							->joinLeft(array('d'=> 'tbl_scheme'),'c.IdScheme = d.IdScheme',array("d.EnglishDescription"))
							->joinLeft(array('e'=> 'tbl_intake'),'a.IdIntake = e.IdIntake',array("e.IntakeId"))
							->joinLeft(array('g'=> 'tbl_branchofficevenue'),'a.IdBranch = g.IdBranch',array("g.BranchName"))
							->joinLeft(array("srs"=>"tbl_student_release_status"),'srs.IdCategory = 1 AND a.IdStudentRegistration = srs.IdStudent',array())
							->joinLeft(array("srss"=>"tbl_student_release_status"),'srss.IdCategory = 1 AND b.IdApplication',array())
							->joinLeft(array("f"=>"tbl_definationms"),'srs.IdReleaseType = f.idDefinition',array('f.DefinitionDesc AS Desc1'))
							->joinLeft(array("h"=>"tbl_definationms"),'srss.IdReleaseType = h.idDefinition',array('h.DefinitionDesc AS Desc2'))	
      						->where('a.IdStudentRegistration = ?',$registrationid);
      						
			$result = $db->fetchRow($sql);
			return $result;
		}
//		
		public function fngetstudentsem($IdStudentRegistration){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a'=>'tbl_studentsemesterstatus'),array('a.*'))
							->order('a.UpdDate DESC')
							//->limit('1')
							->where('a.IdStudentRegistration = ?',$IdStudentRegistration);
							//->where('a.studentsemesterstatus = ?', 130);
			$result = $db->fetchAll($sql);
			
			$select = $db->select()
							->from(array('h'=> 'tbl_studentsemesterstatus'),array())
							->joinLeft(array('f'=> 'tbl_semester'),"f.IdSemester = h.idSemester",array("f.IdSemester","f.SemesterCode"))
      						->joinLeft(array('g'=> 'tbl_semestermaster'),"g.IdSemesterMaster = h.IdSemesterMain",array("g.SemesterMainName","g.SemesterMainCode"))
							->joinLeft(array('i'=> 'tbl_definationms'),'h.studentsemesterstatus = i.idDefinition',array('i.DefinitionDesc'))
							->where('h.IdStudentRegistration = ?',$IdStudentRegistration);
							if($result[0]['idSemester']!=''){
								$select = $select->where('h.idSemester=?',$result[0]['idSemester']);
							}
							if($result[0]['IdSemesterMain']!=''){
								$select = $select->where('h.IdSemesterMain=?',$result[0]['IdSemesterMain']);
							}
			$result1 = $db->fetchAll($select);			
			return $result1;
		}
		
		public function fngetcurrentsemester($IdStudentRegistration){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()
					  ->from(array('a'=>'tbl_studentsemesterstatus'),array('a.UpdDate','a.IdSemesterMain','a.idSemester'))
					  ->order('a.UpdDate DESC')
					  ->where('a.IdStudentRegistration = ?',$IdStudentRegistration);
			//$sql = $sql->where('a.studentsemesterstatus = 130');	
			$result = $db->fetchAll($sql);	
			return $result;
		}
		
		public function fngetcompletedsemester($IdStudentRegistration){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()
					  ->from(array('a'=>'tbl_studentsemesterstatus'),array('a.UpdDate','a.IdSemesterMain','a.idSemester'))
					  ->where('a.IdStudentRegistration = ?',$IdStudentRegistration);
			$sql = $sql->where('a.studentsemesterstatus = 229');	
			$result = $db->fetchAll($sql);	
			return $result;
		}
		
		public function fngetfuturesemester($IdStudentRegistration){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()
					  ->from(array('a'=>'tbl_studentsemesterstatus'),array('a.UpdDate','a.IdSemesterMain','a.idSemester'))
					  ->order('a.UpdDate')
					  ->limit('2')
					  ->where('a.IdStudentRegistration = ?',$IdStudentRegistration)
					  ->where('a.studentsemesterstatus = 131');
			$result = $db->fetchAll($sql);
			return $result;
		}
		
		public function fngetcourse($IdLandscape,$semester,$idsemester){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()
					  ->from(array('a'=>'tbl_landscapesubject'),array('a.CreditHours'))
					  ->join(array('b'=>'tbl_subjectmaster'),'a.IdSubject = b.IdSubject',array('b.IdSubject','b.SubjectName','b.SubCode AS SubjectCode'))
					  ->where('a.IdLandscape = ?',$IdLandscape)
					  ->where('a.IdSemester = ?',$semester);
			$result = $db->fetchAll($sql);
			
			for($i=0;$i<=count($result);$i++){
				if(isset($idsemester['IdSemesterMain']) && ($idsemester['IdSemesterMain']!=NULL||$idsemester['IdSemesterMain']!=0)){
					$result['IdSemesterMain'] = $idsemester['IdSemesterMain'];
				}
				else if(isset($idsemester['idSemester']) && ($idsemester['idSemester']!=NULL||$idsemester['idSemester']!=0)){
					$result['idSemester'] = $idsemester['idSemester'];
				}
			}
			return $result;
					  
		}
		
		public function fngetalreadyregsub($allstudentcourses,$IdSubject,$idreg){
			if(isset($allstudentcourses['idSemester']) && $allstudentcourses['idSemester']!=''){
				$db = Zend_Db_Table::getDefaultAdapter();
				$sql = $db->select()
						  ->from(array('a'=>'tbl_studentregsubjects'),array('a.IdStudentRegSubjects','a.UpdUser','a.UpdDate'))
						  ->join(array('b' => 'tbl_user'),'b.iduser = a.UpdUser',array('b.loginName'))
						  //->where('a.IdSemesterDetails = ?',$allstudentcourses['idSemester'])
						  ->where('a.IdSubject = ?',$IdSubject)
						  ->where('a.IdStudentRegistration = ?',$idreg);
				$result = $db->fetchAll($sql);
				return $result;
			}
			else if(isset($allstudentcourses['IdSemesterMain']) && $allstudentcourses['IdSemesterMain']!=''){
				$db = Zend_Db_Table::getDefaultAdapter();
				$sql = $db->select()
						  ->from(array('a'=>'tbl_studentregsubjects'),array('a.IdStudentRegSubjects','a.UpdUser','a.UpdDate'))
						  ->join(array('b' => 'tbl_user'),'b.iduser = a.UpdUser',array('b.loginName'))
						  ->where('a.IdSemesterMain = ?',$allstudentcourses['IdSemesterMain'])
						  ->where('a.IdSubject = ?',$IdSubject)
						  ->where('a.IdStudentRegistration = ?',$idreg);
				$result = $db->fetchAll($sql);
				return $result;
			}
		}
		
		public function fnaddcourse($data){
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_studentregsubjects";
			$db->insert($table,$data);
		}
		
		public function fngetstudentcurrentsem($registrationid){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a'=>'tbl_studentsemesterstatus'),array('a.*'))
							->joinLeft(array('b'=>'tbl_semester'),'a.idSemester = b.IdSemester',array('b.SemesterCode'))
							->joinLeft(array('c'=>'tbl_semestermaster'),'a.IdSemesterMain = c.IdSemesterMaster',array('c.SemesterMainCode'))
							->joinLeft(array('d'=>'tbl_definationms'),'a.studentsemesterstatus = d.idDefinition',array('d.DefinitionDesc'))
							->order('a.UpdDate DESC')
							->where('a.IdStudentRegistration = ?',$registrationid);
							//$sql = $sql->where('a.studentsemesterstatus = 229 OR a.studentsemesterstatus = 130');
			
			$result = $db->fetchAll($sql);
			$result[0]['currentsem'] = count($result);
			return $result;
			
		}
		
		public function fngetstudentcourse($IdLandscape,$idsemester,$currentsem,$idreg){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()
//							->from(array('a' => 'tbl_studentregistration'),array())
//							->join(array('d' => 'tbl_studentsemesterstatus'),"d.idSemester =".$idsemester."  OR  d.IdSemesterMain = ".$idsemester."",array())
//							->join(array('b' => 'tbl_studentregsubjects'),"b.IdSemesterDetails =".$idsemester."  OR d.IdSemesterMain = ".$idsemester."",array('b.IdStudentRegSubjects','b.UpdDate'))
//							->join(array('c' => 'tbl_subject'),'b.IdSubject = c.IdSubject',array('c.SubjectName','c.SubjectCode'))
//							->join(array('e' => 'tbl_landscapesubject'),'a.IdLandscape = e.IdLandscape AND b.IdSubject = e.IdSubject',array('e.CreditHours'))
//							->join(array('f' => 'tbl_user'),'f.iduser = b.UpdUser',array('f.loginName'))
//							->where('a.IdLandscape = ?',$IdLandscape)
//							->where('b.IdStudentRegistration = ?',$idreg)
//							->where('d.idSemester  = ?',$idsemester)
//							->orwhere('d.IdSemesterMain = ?',$idsemester)
//							->where('e.IdSemester = ?',$currentsem)
//							->where('e.IdLandscape = ?',$IdLandscape);
							->from(array('a' => 'tbl_studentregsubjects'),array('a.IdStudentRegSubjects','a.UpdDate'))
							->join(array('b' => 'tbl_subjectmaster'),'a.IdSubject = b.IdSubject',array('b.SubjectName','b.SubCode AS SubjectCode'))
							->join(array('c' => 'tbl_landscapesubject'),'a.IdSubject = c.IdSubject',array('c.CreditHours'))
							->join(array('d' => 'tbl_user'),'d.iduser = a.UpdUser',array('d.loginName'))
							->where('a.IdStudentRegistration = ?',$idreg)
							->where('a.IdSemesterDetails = ? OR a.IdSemesterMain = ?',$idsemester)
							->where('c.IdLandscape = ?',$IdLandscape)
							->where('c.IdSemester = ?',$currentsem);
							
			$result = $db->fetchAll($sql);
			return $result;
		}
		
		
		public function fndropcourse($IdStudentRegSubjects){
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = 'tbl_studentregsubjects';
    		$where = $db->quoteInto('IdStudentRegSubjects = ?', $IdStudentRegSubjects);
    		$db->delete($table,$where);
		}
		
		
   public function getPresentSemester($lintidapplicant) { 
   	$consistantresult = 'SELECT max(j.IdStudentRegistration) from tbl_studentregistration j where j.IdApplication = "'.$lintidapplicant.'"';
     	$db = Zend_Db_Table::getDefaultAdapter();
		$sql = $db->select()	
							->from(array('a' => 'tbl_studentregistration'),array('a.*'))
                			->join(array('g'=>'tbl_semester'),'a.IdSemester = g.IdSemester')
                			->join(array('f'=>'tbl_semestermaster'),'g.Semester = f.IdSemesterMaster')
                			->where('a.IdApplication = ?',$lintidapplicant)
							->where("a.IdStudentRegistration = ?",new Zend_Db_Expr('('.$consistantresult.')'));
		$result = $db->fetchRow($sql);
		return $result;
	}
	 
	public function getCompleteRegisteredStudentSubjectDetails($lintIdStudentRegistration) { //Function to get the user details
      	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
	              			->from(array('d'=>'tbl_studentregistration'),array("d.*"))                			
                			->join(array('e'=>'tbl_studentregsubjects'),'d.IdStudentRegistration = e.IdStudentRegistration')                			
                			->join(array('j'=>'tbl_subjectmaster'),'e.IdSubject = j.IdSubject')
							->where("e.IdStudentRegistration = ?",$lintIdStudentRegistration);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     }
	 
	 
	public function getCompleteSubjectForSmesterDetails($lintIdSemester,$lstrsubjects) { //Function to get the user details
      	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
	              			->from(array('a'=>'tbl_subjectsoffered'),array("a.*"))                			
                			->join(array('b'=>'tbl_subjectmaster'),'a.IdSubject = b.IdSubject') 
							->where("a.IdSemester = ?",$lintIdSemester)
							->where("b.IdSubject NOT IN (".$lstrsubjects.")");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     }
	 
	public function fnAddSubjects($larrformData) {  // function to insert po details
		 		$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_studentregsubjects";
			$gridcount = count($larrformData['idregsubjects']);
			for($i=0;$i<$gridcount;$i++){
			$larridpo = array('Active'=>1);
			$where = "IdStudentRegSubjects = ".$larrformData['idregsubjects'][$i];
			$db->update($table,$larridpo,$where);
			}
	}

	 public function fnDeletedRegisteredSubject($larrformData) { //Function for Delete Purchase order terms
	 		$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_studentregsubjects";
			$gridcount = count($larrformData['idregsubjects']);
			for($i=0;$i<$gridcount;$i++){
			$larridpo = array('Active'=>0);
			$where = "IdStudentRegSubjects = ".$larrformData['idregsubjects'][$i];
			$db->update($table,$larridpo,$where);
			}
	 }
	 
	 public function fngetsemestersmain($Idscheme){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
	              			->from(array('a'=>'tbl_semestermaster'),array('key'=>'IdSemesterMaster','name'=>'SemesterMainCode'))    
               			    ->where('a.Scheme = "'.$Idscheme.'" AND (a.DummyStatus IS NULL OR a.DummyStatus = 0)');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	 
	 public function fngetsemestersdetail($Idscheme){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                	->from(array('a'=>'tbl_semester'),array('key'=>'IdSemester','name'=>'SemesterCode')) 
                			->joinLeft(array('b'=>'tbl_program'),'a.Program  = b.IdProgram',array()) 
							->where('b.IdScheme = ?',$Idscheme);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
     
}