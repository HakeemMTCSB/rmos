<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Registration_Model_DbTable_ScholarshipTag extends Zend_Db_Table_Abstract{
    
    public function getAcceptedScholarship($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_application'), array('value'=>'*'))
            ->join(array('b'=>'applicant_financial'), 'a.sa_af_id=b.af_id')
            ->join(array('c'=>'tbl_scholarshiptagging_sct'), 'b.af_scholarship_apply=c.sct_Id')
            ->join(array('d'=>'tbl_scholarship_sch'), 'c.sct_schId=d.sch_Id')
            ->where('a.sa_cust_id = ?', $id)
            ->where('a.sa_cust_type = ?', 2)
            ->where('a.sa_status = ?', 9);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function insertScholarshipTag($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_scholarship_studenttag', $bind);
        $id = $db->lastInsertId('tbl_scholarship_studenttag', 'sa_id');
        return $id;
    }
    
    public function getStudentInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'), array('value'=>'*'))
            ->where('a.IdStudentRegistration = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSemester($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdSemesterMaster = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
}