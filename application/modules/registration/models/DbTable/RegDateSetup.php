<?php 

class Registration_Model_DbTable_RegDateSetup extends Zend_Db_Table_Abstract {
	
	protected $_name = 'reg_date_setup';
	protected $_primary = "rds_id";
	
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getList($data){
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('rds'=>$this->_name))
					  ->join(array('i'=>'tbl_intake'),'i.IdIntake=rds.rds_intake',array('intake'=>'IntakeDefaultLanguage'))	;

					  if(isset($data['registration_date']) && $data['registration_date']!=''){
					  		$select->where('rds_date = ?',$data['registration_date']);
					  }
					  
	 				  if(isset($data['IdIntake']) && $data['IdIntake']!=''){
					  		$select->where('rds_intake = ?',$data['IdIntake']);
					  }
		
		// echo $select;
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	public function checkExist($intake,$rds_date){
		
		$db = Zend_Db_Table::getDefaultAdapter();	
		$select = $db ->select()
					  ->from(array('rds'=>$this->_name))
					  ->where('rds_intake = ?',$intake)
					  ->where('rds_date = ?',$rds_date);
		$row = $db->fetchRow($select);
		 	
		if(isset($row))
		 	return $row;
		else
		 	return null;
		 	
	}
	
	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();	
		$select = $db ->select()
					  ->from(array('rds'=>$this->_name))
					   ->join(array('i'=>'tbl_intake'),'i.IdIntake=rds.rds_intake',array('intake'=>'IntakeDefaultLanguage'))	
					  ->where('rds_id = ?',$id);
		$row = $db->fetchRow($select);
		 	
		if(isset($row))
		 	return $row;
		else
		 	return null;
		 	
	}	
	
}
?>