<?php
class Registration_Model_DbTable_Verifypayments extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_paymentvarification';
	
	public function init(){
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}	
	public function fnSearchEducationlist($post) { //Function to get the Program Branch details
     	$field7 = "a.Active = ".$post["field7"];
		$select = $this->select()
			  	 ->setIntegrityCheck(false) 			  	
			    ->from(array('a' => 'tbl_studentapplication'),array('a.*'))
                ->join(array('b'=>'tbl_program'),'a.IDCourse  = b.IdProgram')
                ->join(array('c'=>'tbl_definationms'),'b.Award = c.idDefinition')
                ->join(array('d'=>'tbl_collegemaster'),'a.idCollege = d.IdCollege')
                ->joinLeft(array('e'=>'tbl_paymentvarification'),'a.IdApplication= e.IdApplication',array('e.PaymentRecived','e.FullPayment','e.OtherDocumentVarified','e.Comments'))
                ->joinLeft(array('f'=>'tbl_city'),'a.PermCity = f.idCity',array('f.CityName'))
                ->where('a.FName  like "%" ? "%"',$post['field3'])
                ->where('a.Rejected = 0')
                ->where('a.Registered = 0')
			   	->where('a.ICNumber like  "%" ? "%"',$post['field2'])
			   	->where($field7);;
			   if(isset($post['field5']) && !empty($post['field5']) ){
				  $select = $select->where("b.IdProgram  = ?",$post['field5']);
			 }		
			 if(isset($post['field8']) && !empty($post['field8']) ){
				  $select = $select->where("a.IdCollege  = ?",$post['field8']);
			 }	
		$select	->order("a.FName");		
		$result = $this->fetchAll($select);
		return $result->toArray();
     }		
	public function fninsertNewMarks($larrformdata){		
		$lstrinsert = "";
		$thjdbAdpt = Zend_Db_Table::getDefaultAdapter();
		for($icount = 0;$icount < count($larrformdata['IdApplication']);$icount++ )
			$lstrinsert .= "(".$larrformdata['IdApplication'][$icount].",".$larrformdata['PaymentRecived'][$icount].",".$larrformdata['FullPayment'][$icount].",
			".$larrformdata['OtherDocumentVarified'][$icount].",".$larrformdata['Comments'][$icount].",".$larrformdata['UpdUser'].",'".$larrformdata['UpdDate']."'),";

		$thjdbAdpt->query("INSERT INTO tbl_paymentvarification(IdApplication,PaymentRecived,FullPayment,OtherDocumentVarified,Comments,UpdUser,UpdDate) 
		VALUES ".substr($lstrinsert,0,-1));	
	}	
	public function fndeleteOldMarks($IdApplication){
		$IdApplications  = "";
		for($ms=0;$ms<count($IdApplication);$ms++)$IdApplications .= $IdApplication[$ms].",";
		$thjdbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrwhere = "IdApplication IN( ".substr($IdApplications,0,-1).")";		
		$thjdbAdpt->delete('tbl_paymentvarification',$lstrwhere);
	}	
}