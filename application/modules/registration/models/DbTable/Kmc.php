<?php 
class Registration_Model_DbTable_Kmc extends Zend_Db_Table_Abstract {


	/**
	 * The default table name 
	 */
	
	protected $_name = 'library_api';
	protected $_primary = "id";
	
	/**
	 *  insert record
	 */
	 
	public function addKmc(array $data) {
	
	/**	
		`username` varchar(30) NOT NULL COMMENT 'matric number',
		`password` text COMMENT 'plain password',
		`email` text COMMENT 'student email',
		`name` text COMMENT 'Full Name',
		`user_type` varchar(10) DEFAULT 'Student',
		`role` varchar(10) DEFAULT 'user',
		`status` set('active','blocked','suspended') DEFAULT 'active',
		`status_message` text COMMENT 'id status = blocked or suspended',
		`religion` varchar(50) DEFAULT NULL,
		`nationality` varchar(200) DEFAULT NULL,
		`identity_type` set('mykad','passport') DEFAULT NULL,
		`identity_number` varchar(30) DEFAULT NULL,
		`date_of_birth` date DEFAULT NULL,
		`study_start` date DEFAULT NULL,
		`study_end` date DEFAULT NULL,
		`permanent_address` text,
		`permanent_city` varchar(100) DEFAULT NULL,
		`permanent_postcode` varchar(10) DEFAULT NULL,
		`permanent_state` text,
		`permanent_country` text,
		`correspondence_address` text,
		`correspondence_city` varchar(100) DEFAULT NULL,
		`correspondence_postcode` varchar(10) DEFAULT NULL,
		`correspondence_state` text,
		`correspondence_country` text,
		`contact_home` varchar(30) DEFAULT NULL,
		`contact_office` varchar(30) DEFAULT NULL,
		`contact_mobile` varchar(30) DEFAULT NULL,
		`program_name` text,
		`program_type` text,
		`program_resource_fee` decimal(10,2) DEFAULT NULL,
		`response` text,
		`action_status` set('NEW','DONE') NOT NULL DEFAULT 'NEW',
		`action_type` set('user','status','password') NOT NULL DEFAULT 'user',
		`action` set('ADD','EDIT') NOT NULL DEFAULT 'ADD',
		
		*/
		$data['role'] = 'user';
		$data['user_type'] = 'Student';
		$data['action_status'] = 'NEW';
		$data['action_type'] = 'user';
		$data['action'] = 'ADD';
		
		if ($this->insert($data))
		{
			return 'Ok';
		}
		else
		{
			return 'Failed';
		}
		
	}
	
	public function changePasswordKmc(array $data) {
		/**	
		`username` varchar(30) NOT NULL COMMENT 'matric number',
		`password` text COMMENT 'plain password',
		`email` text COMMENT 'student email',
		`name` text COMMENT 'Full Name'
		*/
		
		$data['role'] = 'user';
		$data['user_type'] = 'Student';
		$data['action_status'] = 'NEW';
		$data['action_type'] = 'password';
		$data['action'] = 'EDIT';
		
		if ($this->insert($data))
		{
			return 'Ok';
		}
		else
		{
			return 'Failed';
		}
		
	}
	
	public function updateKmc(array $data) {
	
	/**	
		`username` varchar(30) NOT NULL COMMENT 'matric number',
		`password` text COMMENT 'plain password',
		`email` text COMMENT 'student email',
		`name` text COMMENT 'Full Name',
		`user_type` varchar(10) DEFAULT 'Student',
		`role` varchar(10) DEFAULT 'user',
		`status` set('active','blocked','suspended') DEFAULT 'active',
		`status_message` text COMMENT 'id status = blocked or suspended',
		`religion` varchar(50) DEFAULT NULL,
		`nationality` varchar(200) DEFAULT NULL,
		`identity_type` set('mykad','passport') DEFAULT NULL,
		`identity_number` varchar(30) DEFAULT NULL,
		`date_of_birth` date DEFAULT NULL,
		`study_start` date DEFAULT NULL,
		`study_end` date DEFAULT NULL,
		`permanent_address` text,
		`permanent_city` varchar(100) DEFAULT NULL,
		`permanent_postcode` varchar(10) DEFAULT NULL,
		`permanent_state` text,
		`permanent_country` text,
		`correspondence_address` text,
		`correspondence_city` varchar(100) DEFAULT NULL,
		`correspondence_postcode` varchar(10) DEFAULT NULL,
		`correspondence_state` text,
		`correspondence_country` text,
		`contact_home` varchar(30) DEFAULT NULL,
		`contact_office` varchar(30) DEFAULT NULL,
		`contact_mobile` varchar(30) DEFAULT NULL,
		`program_name` text,
		`program_type` text,
		`program_resource_fee` decimal(10,2) DEFAULT NULL,
		`response` text,
		
		*/
		$data['role'] = 'user';
		$data['user_type'] = 'Student';
		$data['action_status'] = 'NEW';
		$data['action_type'] = 'user';
		$data['action'] = 'EDIT';
		
		if ($this->insert($data))
		{
			return 'Ok';
		}
		else
		{
			return 'Failed';
		}
		//$this->validate($data);
	}
	
	/*public function updateKmc(array $data) {
	
	/**	
		`username` varchar(30) NOT NULL COMMENT 'matric number',
		`password` text COMMENT 'plain password',
		`email` text COMMENT 'student email',
		`name` text COMMENT 'Full Name',
		
		`status` set('active','blocked','suspended') DEFAULT 'active',
		`status_message` text COMMENT 'id status = blocked or suspended',
		
		
		$data['role'] = 'user';
		$data['user_type'] = 'Student';
		$data['action_status'] = 'NEW';
		$data['action_type'] = 'status';
		$data['action'] = 'EDIT';
		
		if ($this->insert($data))
		{
			return 'Ok';
		}
		else
		{
			return 'Failed';
		}
		//$this->validate($data);
	}*/
	/**
	 *  validate data
	 *

	private function validate($data) {
		
	}*/
}
