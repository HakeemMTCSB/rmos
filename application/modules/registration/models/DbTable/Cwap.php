<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 28/7/2016
 * Time: 11:00 AM
 */
class Registration_Model_DbTable_Cwap extends Zend_Db_Table_Abstract{

    public function getAdvancePayment(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'advance_payment'))
            ->join(array('b'=>'invoice_main'), 'a.advpy_invoice_id = b.id')
            ->join(array('c'=>'tbl_studentregistration'), 'b.IdStudentRegistration = c.IdStudentRegistration')
            ->where('a.advpy_status = ?', 'A')
            ->where('advpy_invoice_id IS NOT NULL');

        $result = $db->fetchAll($select);
        return $result;
    }
}