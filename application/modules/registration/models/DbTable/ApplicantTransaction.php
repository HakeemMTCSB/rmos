<?php

class Registration_Model_DbTable_ApplicantTransaction extends Zend_Db_Table_Abstract
{


    /**
     * The default table name
     */

    protected $_name = 'applicant_transaction';
    protected $_primary = "at_trans_id";

    public function addData($data)
    {
        $id = $this->insert($data);
        return $id;
    }

    public function updateData($data, $id)
    {
        $this->update($data, $this->_primary . ' = ' . (int)$id);
    }

    public function deleteData($id)
    {
        $this->delete($this->_primary . ' =' . (int)$id);
    }

    public function getTransactionData($txn_id)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('p' => 'applicant_transaction'))
            ->where("p.at_trans_id = ? ", $txn_id);


        $row2 = $db->fetchRow($select);

        return $row2;
    }


    public function getOfferPaidApplicant($data = null)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('at' => $this->_name))
            ->joinleft(array('ap' => 'applicant_profile'), 'ap.appl_id=at.at_appl_id', array('appl_fname', 'appl_mname', 'appl_lname'))
            ->joinleft(array('apr' => 'applicant_program'), 'apr.ap_at_trans_id=at.at_trans_id')
            ->where("at.at_status='OFFER'");

        if ($data != null) {

            if (isset($data["academic_year"]) && $data["academic_year"] != '') {
                $select->where("at.at_academic_year  = '" . $data["academic_year"] . "'");
            }

            if (isset($data["IdIntake"]) && $data["IdIntake"] != '') {
                $select->where("at.at_intake  = '" . $data["IdIntake"] . "'");
            }

            if (isset($data["admission_type"]) && $data["admission_type"] != '') {
                $select->where("at.at_appl_type ='" . $data["admission_type"] . "'");
            }

            if (isset($data["IdProgram"]) && $data["IdProgram"] != '') {
                //0650
                // $select->where("apr.ap_prog_code ='".$data["IdProgram"]."'");
                $select->where("apr.ap_prog_code ='0650'");
            }
        }


        $row = $db->fetchAll($select);

        //print_r($row);

        foreach ($row as $key => $applicant) {

            if ($applicant['at_appl_type'] == 1) {//USM
                if ($applicant["ap_usm_status"] == 2 || $applicant["ap_usm_status"] == 0) {
                    unset($row[$key]);
                }
            }
        }


        //print_r($row);

        /*foreach ($row as $key=>$applicant){
            $select = $db ->select()
                    ->from(array('pm'=>'payment_main'))
                    ->join(array('pi'=>'applicant_proforma_invoice'),'pm.billing_no = pi.billing_no')
                      ->where("pi.payee_id ='".$applicant['at_pes_id']."'");

            $row2 = $db->fetchRow($select);

            if(!$row2){
                unset($row[$key]);
            }
        }*/


        return $row;
    }


    public function getProfile($appl_id)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('p' => 'applicant_profile'))
            ->where("p.appl_id = ? ", $appl_id);


        $row2 = $db->fetchRow($select);

        return $row2;

    }

    public function fncheckUniqueRegID($registrationId)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()
            ->from(array('sr' => 'tbl_studentregistration', array('sr.IdStudentRegistration', 'sr.registrationId')))
            ->where('sr.registrationId = ?', $registrationId);
        $result = $db->fetchRow($sql);

        return $result;
    }


    public function getUnRegisteredApplicant($data = null)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $landscapeDB = new GeneralSetup_Model_DbTable_Landscape();
        $semesterDB = new Registration_Model_DbTable_Semester();
        $invoice_helper = new icampus_Function_Studentfinance_Invoice();

        $select = $db->select()
            ->from(array('at' => $this->_name))
            ->join(array('ap' => 'applicant_profile'), 'ap.appl_id=at.at_appl_id', array('appl_id', 'appl_fname', 'appl_mname', 'appl_lname'))
            ->join(array('apr' => 'applicant_program'), 'apr.ap_at_trans_id=at.at_trans_id')
            ->join(array('prg' => 'tbl_program'), 'prg.IdProgram=apr.ap_prog_id', array('ArabicName', 'ProgramName', 'ProgramCode', 'IdScheme'))
            ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=apr.ap_prog_scheme', array())
            ->joinLeft(array("dp" => "registry_values"), 'dp.id=ps.mode_of_program', array('ProgramMode' => 'name', 'ProgramModeMy' => 'name_malay'))
            ->joinLeft(array("ds" => "registry_values"), 'ds.id=ps.mode_of_study', array('StudyMode' => 'name', 'StudyModeMy' => 'name_malay'))
            ->joinLeft(array("dt" => "registry_values"), 'dt.id=ps.program_type', array('ProgramType' => 'name', 'ProgramTypeMy' => 'name_malay'))
            ->join(array('i' => 'tbl_intake'), 'i.IdIntake=at.at_intake', array('IntakeDesc', 'IntakeDefaultLanguage' => 'IntakeDesc', 'sem_year', 'sem_seq'))
            ->joinLeft(array('dc' => 'registry_values'), 'dc.id=ap.appl_category', array('student_category' => 'name', 'student_category_my' => 'name_malay'))
            ->where("at.at_IdStudentRegistration = 0")
            ->where("at.at_status='158' || at.at_status='164'"); //offer or accept

        if ($data != null) {

            if (isset($data["IdProgram"]) && $data["IdProgram"] != '') {
                $select->where($db->quoteInto("apr.ap_prog_id = ?", $data["IdProgram"]));
            }

            if (isset($data["IdProgramScheme"]) && $data["IdProgramScheme"] != '') {
                $select->where($db->quoteInto("apr.ap_prog_scheme = ?", $data["IdProgramScheme"]));
            }

            if (isset($data["IdIntake"]) && $data["IdIntake"] != '') {
                $select->where($db->quoteInto("at.at_intake  = ?", $data["IdIntake"]));
            }

            if (isset($data["applicant_name"]) && $data["applicant_name"] != '') {
                $select->where($db->quoteInto("CONCAT_WS(' ', ap.appl_fname, ap.appl_lname) LIKE ?", "%" . $data["applicant_name"] . "%"));
            }

            if (isset($data["applicant_nomor"]) && $data["applicant_nomor"] != '') {
                $select->where($db->quoteInto("at.at_pes_id = ?", $data["applicant_nomor"]));
            }

            if (isset($data["Sorting"]) && $data["Sorting"] != '') {

                if ($data["Sorting"] == 1) {
                    $select->order("ap.appl_fname");
                } else
                    if ($data["Sorting"] == 2) {
                        $select->order("at.at_pes_id");
                    } else
                        if ($data["Sorting"] == 3) {
                            $select->order("prg.ProgramName");
                        }

            } else {
                $select->order("at.at_submit_date");
            }

        }

        $rows = $db->fetchAll($select);

        foreach ($rows as $index => $row) {

            //get landsacpe
            $landscape = $landscapeDB->getActiveLandscape($row['ap_prog_id'], $row['at_intake'], $row['ap_prog_scheme']);
            $rows[$index]['landscape'] = $landscape;

            //get register semester based on intake
            $semester = $semesterDB->getApplicantCurrentSemesterOnwards($row);
            $rows[$index]['semester'] = $semester;

            //get payment status
            /*
             *  1=no outstanding
                2=outstanding
                3=no invoice
             */
            $status = $invoice_helper->checkOutstandingBalance($row['at_trans_id'], 1); //1 applicant 2 student

            $rows[$index]['payment_status'] = $status;

            /*//filter by payment status
             if(isset($data["at_payment_status"]) && $data["at_payment_status"]!=''){
                     if($data["at_payment_status"]!=$status){
                         unset($rows[$index]);
                     }else{
                         $rows[$index]['payment_status'] = $status;
                     }
            }else{
                    $rows[$index]['payment_status'] = $status;
            }*/


        }

        return $rows;
    }

    public function getUnRegisteredApplicantOld($data = null)
    {


        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('at' => $this->_name))
            ->join(array('ap' => 'applicant_profile'), 'ap.appl_id=at.at_appl_id', array('appl_id', 'appl_fname', 'appl_mname', 'appl_lname'))
            ->join(array('apr' => 'applicant_program'), 'apr.ap_at_trans_id=at.at_trans_id')
            ->join(array('prg' => 'tbl_program'), 'prg.IdProgram=apr.ap_prog_id', array('ArabicName', 'ProgramName'))
            ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=apr.ap_prog_scheme', array())
            ->joinLeft(array("dp" => "tbl_definationms"), 'dp.idDefinition=ps.mode_of_program', array('ProgramMode' => 'DefinitionDesc', 'ProgramModeMy' => 'BahasaIndonesia'))
            ->joinLeft(array("ds" => "tbl_definationms"), 'ds.idDefinition=ps.mode_of_study', array('StudyMode' => 'DefinitionDesc', 'StudyModeMy' => 'BahasaIndonesia'))
            ->joinLeft(array("dt" => "tbl_definationms"), 'dt.idDefinition=ps.program_type', array('ProgramType' => 'DefinitionDesc', 'ProgramTypeMy' => 'BahasaIndonesia'))
            ->join(array('i' => 'tbl_intake'), 'i.IdIntake=at.at_intake', array('IntakeDesc', 'IntakeDefaultLanguage'))
            ->joinLeft(array('dc' => 'tbl_definationms'), 'dc.idDefinition=ap.appl_category', array('student_category' => 'DefinitionDesc', 'student_category_my' => 'BahasaIndonesia'))
            ->where("at.at_IdStudentRegistration = 0")
            ->where("at.at_status='593' || at.at_status='599'")//offer or accept
            ->order("at.at_submit_date");

        if ($data != null) {

            if (isset($data["IdProgram"]) && $data["IdProgram"] != '') {
                $select->where("apr.ap_prog_id ='" . $data["IdProgram"] . "'");
            }

            if (isset($data["IdProgramScheme"]) && $data["IdProgramScheme"] != '') {
                $select->where("apr.ap_prog_scheme='" . $data["IdProgramScheme"] . "'");
            }

            if (isset($data["IdIntake"]) && $data["IdIntake"] != '') {
                $select->where("at.at_intake  = '" . $data["IdIntake"] . "'");
            }

            if (isset($data["applicant_name"]) && $data["applicant_name"] != '') {
                $select->where("(ap.appl_fname LIKE '%" . $data["applicant_name"] . "%'");
                $select->orwhere("ap.appl_mname LIKE '%" . $data["applicant_name"] . "%'");
                $select->orwhere("ap.appl_lname LIKE '%" . $data["applicant_name"] . "%')");
            }

            if (isset($data["applicant_nomor"]) && $data["applicant_nomor"] != '') {
                $select->where("at.at_pes_id ='" . $data["applicant_nomor"] . "'");
            }
        }
        //echo '<pre>'.$select;
        $row = $db->fetchAll($select);

        return $row;
    }


    public function getUnRegisteredApplicants($data = null)
    {


        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('at' => $this->_name))
            ->joinleft(array('ap' => 'applicant_profile'), 'ap.appl_id=at.at_appl_id', array('appl_id', 'appl_fname', 'appl_mname', 'appl_lname'))
            ->joinleft(array('apr' => 'applicant_program'), 'apr.ap_at_trans_id=at.at_trans_id')
            ->where("at.at_status='OFFER'")
            ->where("at.at_IdStudentRegistration = 0");

        if ($data["IdCollege"] == 2) {//FTI
            $select->order("apr.ap_usm_mark DESC");
        } else {
            $select->order("ap.appl_fname");
        }


        if ($data != null) {

            if (isset($data["program_code"]) && $data["program_code"] != '') {
                $select->where("apr.ap_prog_code ='" . $data["program_code"] . "'");
            }

            if (isset($data["IdIntake"]) && $data["IdIntake"] != '') {
                $select->where("at.at_intake  = '" . $data["IdIntake"] . "'");
            }

            if (isset($data["applicant_name"]) && $data["applicant_name"] != '') {
                $select->where("(ap.appl_fname LIKE '%" . $data["applicant_name"] . "%'");
                $select->orwhere("ap.appl_mname LIKE '%" . $data["applicant_name"] . "%'");
                $select->orwhere("ap.appl_lname LIKE '%" . $data["applicant_name"] . "%')");
            }

            if (isset($data["applicant_nomor"]) && $data["applicant_nomor"] != '') {
                $select->where("at.at_pes_id ='" . $data["applicant_nomor"] . "'");
            }
        }

        $row = $db->fetchAll($select);

        //echo $select;

        //print_r($row);

        foreach ($row as $key => $applicant) {
            if ($applicant['at_appl_type'] == 1) {//USM
                if ($applicant["ap_usm_status"] == 2 || $applicant["ap_usm_status"] == 0) {
                    unset($row[$key]);
                }
            }
        }


        foreach ($row as $key => $applicant) {

            if ($applicant["at_payment_status"] != 1 && $applicant["at_payment_status"] != 2) {
                $select = $db->select()
                    ->from(array('pm' => 'invoice_main'))
                    ->join(array('pi' => 'applicant_proforma_invoice'), 'pm.bill_number = pi.billing_no')
                    ->where('im.bill_paid>0.00')
                    ->where("pi.payee_id ='" . $applicant['at_pes_id'] . "'");

                $row2 = $db->fetchRow($select);

                if (!$row2) {
                    unset($row[$key]);
                }
            }
        }

        return $row;
    }

    public function getApplicantRegistrationDetail($at_trans_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = $db->select()
            ->from(array('ApplicantTransaction' => 'applicant_transaction'), array('ApplicantTransaction.*', 'IdBranch' => 'ApplicantTransaction.branch_id'))
            ->joinLeft(array('ApplicantProfile' => 'applicant_profile'), 'ApplicantProfile.appl_id = ApplicantTransaction.at_appl_id',
                array("CONCAT_WS(' ',IFNULL(ApplicantProfile.appl_fname,''),IFNULL(ApplicantProfile.appl_mname,''),IFNULL(ApplicantProfile.appl_lname,'')) as name", 'ApplicantProfile.*'))
            ->join(array('apr' => 'applicant_program'), 'apr.ap_at_trans_id=ApplicantTransaction.at_trans_id')
            ->join(array('prg' => 'tbl_program'), 'prg.IdProgram=apr.ap_prog_id', array('ArabicName', 'ProgramName', 'ProgramCode'))
            ->join(array('bch' => 'tbl_branchofficevenue'), 'bch.IdBranch = ApplicantTransaction.branch_id', array('BranchName', 'chargeable' => 'invoice'))
            ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=apr.ap_prog_scheme', array())
            ->joinLeft(array("dp" => "tbl_definationms"), 'dp.idDefinition=ps.mode_of_program', array('ProgramMode' => 'DefinitionDesc', 'ProgramModeMy' => 'BahasaIndonesia'))
            ->joinLeft(array("ds" => "tbl_definationms"), 'ds.idDefinition=ps.mode_of_study', array('StudyMode' => 'DefinitionDesc', 'StudyModeMy' => 'BahasaIndonesia'))
            ->joinLeft(array("dt" => "tbl_definationms"), 'dt.idDefinition=ps.program_type', array('ProgramType' => 'DefinitionDesc', 'ProgramTypeMy' => 'BahasaIndonesia'))
            ->join(array('i' => 'tbl_intake'), 'i.IdIntake=ApplicantTransaction.at_intake', array('IntakeDesc', 'IntakeDefaultLanguage'))
            ->joinLeft(array("dc" => "tbl_definationms"), 'dc.idDefinition=ApplicantProfile.appl_category', array('StudentCategory' => 'DefinitionDesc'))
            ->joinLeft(array('nt' => 'tbl_countries'), 'nt.idCountry=ApplicantProfile.appl_nationality', array('nt.CountryName as Nationality'))
            ->joinLeft(array("st" => "tbl_definationms"), 'st.idDefinition=ApplicantTransaction.at_status', array('StatusName' => 'DefinitionDesc'))
            ->joinLeft(array("fs" => "fee_structure"), 'fs.fs_id=ApplicantTransaction.at_fs_id', array('fs_name'))
            ->joinLeft(array('o' => 'student_financial'), 'ApplicantProfile.appl_id = o.sp_id')
            ->joinLeft(array('u' => 'tbl_definationms'), 'o.af_method = u.idDefinition', array('afMethodName' => 'u.DefinitionDesc'))
            ->joinLeft(array('v' => 'tbl_definationms'), 'o.af_type_scholarship = v.idDefinition', array('typeScholarshipName' => 'v.DefinitionDesc'))
            ->joinLeft(array('w' => 'tbl_scholarship_sch'), 'o.af_scholarship_apply = w.sch_Id', array('applScholarshipName' => 'w.sch_name'))
            ->joinLeft(array('city' => 'tbl_city'), 'city.idCity=ApplicantProfile.appl_city', array('CityName'))
            ->joinLeft(array('state' => 'tbl_state'), 'state.IdState=ApplicantProfile.appl_state', array('StateName'))
            ->joinLeft(array('ctrs' => 'tbl_countries'), 'ctrs.idCountry=ApplicantProfile.appl_country', array('CountryName'))
            ->joinLeft(array('city2' => 'tbl_city'), 'city2.idCity=ApplicantProfile.appl_ccity', array('CCityName' => 'CityName'))
            ->joinLeft(array('state2' => 'tbl_state'), 'state2.IdState=ApplicantProfile.appl_cstate', array('CStateName' => 'StateName'))
            ->joinLeft(array('ctrs2' => 'tbl_countries'), 'ctrs2.idCountry=ApplicantProfile.appl_ccountry', array('CCountryName' => 'CountryName'))
            ->where('ApplicantTransaction.at_trans_id = ?', $at_trans_id);

        $applicant = $db->fetchRow($where);
        return ($applicant);
    }

    public function getTransactionProfileData($txn_id)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('at' => 'applicant_transaction'))
            ->join(array('ap' => 'applicant_profile'), 'ap.appl_id=at.at_appl_id', array('MigrateCode', 'prev_studentID', 'prev_student', 'appl_category'))
            ->where("at.at_trans_id = ? ", $txn_id);

        $row2 = $db->fetchRow($select);

        return $row2;
    }

}
