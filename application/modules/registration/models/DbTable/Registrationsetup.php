<?php
class Registration_Model_DbTable_Registrationsetup extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_registration_configuration';
    private $lobjDbAdpt;

	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}


     /**
     * Function to SAVE Configure Registration setup for senior and new student
     * @auhor: vipul
     */

    public function fnAddregistrationsetup($formData) { //Function for adding the Program Branch details to the table
			unset ( $formData ['Save']);
			if($formData ['registrationType']=='2') {  // NEW STUDENT
				$formData ['academicLandscapeReqt']='0';
				$formData ['minCgpa']='0';
				$formData ['studentDisciplinary']='0';
				$formData ['minFinanceBalance']= $formData ['minFinanceBalanceNew'];
			}
			if($formData ['registrationType']=='1') {  // Senior STUDENT
				$formData ['minFinanceBalance'] = $formData ['minFinanceBalanceSenior'];
			}

			$data = array(
			'registrationType'   => $formData ['registrationType'],
            'minFinanceBalance'   => $formData ['minFinanceBalance'],
            'academicLandscapeReqt'   => $formData ['academicLandscapeReqt'],
            'minCgpa'   => $formData ['minCgpa'],
            'studentDisciplinary'   => $formData ['studentDisciplinary'],
           );
    		return $this->insert($data);
	}


     /**
     * Function to DELETE Configure Registration setup based on registrationType
     * @auhor: vipul
     */

    public function fnDeleteConfiguration($regType) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $table = "tbl_registration_configuration";
    $where = $db->quoteInto('registrationType = ?', $regType);
    $db->delete($table, $where);
    }

 	/**
     * Function to GET Configure Registration setup based on registrationType
     * @auhor: vipul
     */
    public function fnGetConfiguration($regType) {
    $db = Zend_Db_Table::getDefaultAdapter();
    $select = $db->select()
                    ->from(array('c' => 'tbl_registration_configuration'), array('c.*'))
                    ->where("c.registrationType  = '$regType'");
    $result = $db->fetchAll($select);
    return $result;
    }



}
?>