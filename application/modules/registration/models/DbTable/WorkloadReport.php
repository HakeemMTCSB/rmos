<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 12/12/2015
 * Time: 12:47 PM
 */
class Registration_Model_DbTable_WorkloadReport extends Zend_Db_Table_Abstract {

    public function getLecturerList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_staffmaster'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.IdLevel = b.idDefinition', array('levelname'=>'b.DefinitionDesc'))
            ->where('a.StaffAcademic = ?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSemesterGroup($year){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.AcademicYear = ?', $year)
            ->group('a.sem_seq');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSemester($year, $month, $scheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.AcademicYear = ?', $year)
            ->where('a.sem_seq = ?', $month)
            ->where('a.IdScheme = ?', $scheme)
            ->where('a.IsCountable = ?', 1);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getWorkload($lecturer, $sem, $mop = 0){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->join(array('b'=>'course_group_program'), 'a.IdCourseTaggingGroup = b.group_id')
            ->join(array('c'=>'tbl_program_scheme'), 'b.program_scheme_id = c.IdProgramScheme')
            ->where('a.IdLecturer = ?', $lecturer)
            ->where('a.IdSemester = ?', $sem)
            //->where('c.mode_of_program = ?', $mop)
            ->group('a.IdCourseTaggingGroup');

        if ($mop != 0){
            $select->where('c.mode_of_program = ?', $mop);
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function checkTutorial($groupid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'course_group_schedule'), array('value'=>'*'))
            ->where('a.idGroup = ?', $groupid)
            ->where('a.idClassType = ?', 830);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getCurrentSemester($scheme){
        $date = date('Y-m-d');
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.SemesterStartDate <= ?', $date)
            ->where('a.SemesterEndDate >= ?', $date)
            ->where('a.IdScheme = ?', $scheme)
            ->where('a.IsCountable = ?', 1);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getPPPComplete($lecturer){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'thesis_exemption'), array('value'=>'*'))
            ->join(array('b'=>'thesis_article_supervisor'), 'a.e_id = b.ps_pid')
            ->join(array('c'=>'tbl_studentregsubjects'), 'a.student_id = c.IdStudentRegistration')
            ->join(array('d'=>'tbl_user'), 'b.ps_supervisor_id = d.iduser')
            ->where('a.status = ?', 2)
            ->where('b.ps_type = ?', 'exemption')
            ->where('d.IdStaff = ?', $lecturer)
            ->where('c.IdSubject = ?', 84)
            ->where('c.exam_status = ?', 'C');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getPPPCurrent($lecturer){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'thesis_exemption'), array('value'=>'*'))
            ->join(array('b'=>'thesis_article_supervisor'), 'a.e_id = b.ps_pid')
            ->join(array('c'=>'tbl_studentregsubjects'), 'a.student_id = c.IdStudentRegistration')
            ->join(array('d'=>'tbl_user'), 'b.ps_supervisor_id = d.iduser')
            ->where('a.status = ?', 2)
            ->where('b.ps_type = ?', 'exemption')
            ->where('d.IdStaff = ?', $lecturer)
            ->where('c.IdSubject = ?', 84)
            ->where('c.exam_status != ?', 'C');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getArticleshipComplete($lecturer){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'thesis_articleship'), array('value'=>'*'))
            ->join(array('b'=>'thesis_article_supervisor'), 'a.a_id = b.ps_pid')
            ->join(array('c'=>'tbl_studentregsubjects'), 'a.student_id = c.IdStudentRegistration')
            ->join(array('d'=>'tbl_user'), 'b.ps_supervisor_id = d.iduser')
            ->where('a.status = ?', 2)
            ->where('b.ps_type = ?', 'articleship')
            ->where('d.IdStaff = ?', $lecturer)
            ->where('c.IdSubject = ?', 85)
            ->where('c.exam_status = ?', 'C');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getArticleshipCurrent($lecturer){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'thesis_articleship'), array('value'=>'*'))
            ->join(array('b'=>'thesis_article_supervisor'), 'a.a_id = b.ps_pid')
            ->join(array('c'=>'tbl_studentregsubjects'), 'a.student_id = c.IdStudentRegistration')
            ->join(array('d'=>'tbl_user'), 'b.ps_supervisor_id = d.iduser')
            ->where('a.status = ?', 2)
            ->where('b.ps_type = ?', 'articleship')
            ->where('d.IdStaff = ?', $lecturer)
            ->where('c.IdSubject = ?', 85)
            ->where('c.exam_status != ?', 'C');

        $result = $db->fetchAll($select);
        return $result;
    }
}