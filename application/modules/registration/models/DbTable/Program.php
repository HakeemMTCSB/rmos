<?php 
class Registration_Model_DbTable_Program extends Zend_Db_Table_Abstract
{
	
	/**
	 * The default table name 
	 */
	
	protected $_name = 'tbl_program';
	protected $_primary = "";
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getProgram($programId){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select = $db ->select()
					  ->from(array('p'=>$this->_name))
					  ->where("p.IdProgram = ?", $programId);
				  
		$row = $db->fetchRow($select);			 
		return $row; 
	}
	
	public function getData(){
		
		$session = new Zend_Session_Namespace('sis');
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select = $db ->select()
					  ->from(array('p'=>$this->_name))
					  ->where("Active =1")
					  ->order("p.ProgramCode");					 	 			 
		 			  
		//if($session->IdRole == 311 || $session->IdRole == 298){ //FACULTY DEAN atau FACULTY ADMIN nampak faculty dia sahaja
				//$select->where("p.IdCollege='".$session->idCollege."'");		
	   // } 
		$row = $db->fetchAll($select);			 
		return $row; 
	}
	
	public function getProgramByFaculty($faculty_id=null,$program_active=true){
	
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select = $db ->select()
					->from(array('fac'=>'tbl_collegemaster'))
					->order("fac.ArabicName");
	
		$row_fac = $db->fetchAll($select);
		
		foreach ($row_fac as $index=>$fac){
			$select2 = $db ->select()
						->from(array('p'=>$this->_name))
						->where('p.IdCollege = ?', $fac['IdCollege'])
						->order("p.ProgramCode");
			
			if($program_active){
				$select2->where('p.Active = 1');
			}
			
			$row_program = $db->fetchAll($select2);
			
			if($row_program){
				$row_fac[$index]['programs'] = $row_program;
			}
		}
		
		if($row_fac){
			return $row_fac;
		}else{
			return null;
		}
		
	}
	
	public function getProgramSchemeList($idProgram){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()		
					  ->from(array('ps'=>'tbl_program_scheme'),array('IdProgramScheme'))	
					  ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc'))
				      ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc'))
					  ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc'))					 
					  ->where('IdProgram = ?',$idProgram);
		$row = $db->fetchAll($select);
		return $row;
	}
	
	public function getProgramArray($programId){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select = $db ->select()
					  ->from(array('p'=>$this->_name));
					  
		if($programId != 'ALL'){
			$select->where("p.IdProgram = ?", $programId);
		}
		
		$row = $db->fetchAll($select);		
				  
		return $row; 
	}
	
	public function getProgramInfoArray($programId){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select = $db ->select()
					  ->from(array('p'=>$this->_name),array('ProgramName'=>'group_concat(distinct ProgramName)','ProgramCode'=>'group_concat(distinct ProgramCode)'));
					  
		if($programId){
			$select->where("p.IdProgram IN ($programId)" );
		}
		
		$row = $db->fetchRow($select);		
				  
		return $row; 
	}
	
}
?>