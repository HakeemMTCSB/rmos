<?php 
class Registration_Model_DbTable_ChecklistStudent extends Zend_Db_Table_Abstract
{
	
	/**
	 * The default table name 
	 */
	
	protected $_name = 'registration_checklist_student';
	protected $_primary = "rcstd_id";
	
	public function getStudentData($studentRegistrationId=null){
	
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select = $db ->select()
					  ->from(array('rcs'=>$this->_name))
					  ->joinLeft(array('tu'=>'tbl_user'), 'tu.iduser = rcs.rcstd_create_by', array())
					  ->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = tu.IdStaff', array('rcstd_create_by_name'=>'Fullname'))
					  ->joinLeft(array('tu2'=>'tbl_user'), 'tu2.iduser = rcs.rcstd_update_by', array())
					  ->joinLeft(array('ts2'=>'tbl_staffmaster'), 'ts2.IdStaff = tu2.IdStaff', array('rcstd_update_by_name'=>'Fullname'))
					  -join(array('rc'=>'registration_checklist_setup'), 'rc.rcs_id = rcs.rcstd_rcs_id', array('rcs_name'))
					  ->where('rcs.rcstd_idStudentRegistration = ?',$studentRegistrationId)
		              ->order("rc.rcs_name");
	
		
		$row = $db->fetchAll($select);
		
		return $row;
	}
	
	public function getChecklistStudentData($studentRegistrationId=null){
	
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$select = $db ->select()
		->from(array('rcs'=>'registration_checklist_setup'))
		->joinLeft(array('rcstd'=>'registration_checklist_student'),'rcstd.rcstd_rcs_id = rcs.rcs_id AND rcstd.rcstd_idStudentRegistration ='.$studentRegistrationId )
		->joinLeft(array('tu'=>'tbl_user'), 'tu.iduser = rcstd.rcstd_create_by', array())
		->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = tu.IdStaff', array('rcstd_create_by_name'=>'Fullname'))
		->joinLeft(array('tu2'=>'tbl_user'), 'tu2.iduser = rcstd.rcstd_update_by', array())
		->joinLeft(array('ts2'=>'tbl_staffmaster'), 'ts2.IdStaff = tu2.IdStaff', array('rcstd_update_by_name'=>'Fullname'))
		->order("rcs.rcs_name");
	
	
		$row = $db->fetchAll($select);
	
		return $row;
	}
	
	public function insert(array $data){
	
		$auth = Zend_Auth::getInstance();
	
		$data['rcstd_create_by'] = $auth->getIdentity()->iduser;
		$data['rcstd_create_date'] = date('Y-m-d H:i:s');
			
		return parent::insert($data);
	}
	
	
	public function update(array $data,$where){
	
		$auth = Zend_Auth::getInstance();
		
		$data['rcstd_update_by'] = $auth->getIdentity()->iduser;
		$data['rcstd_update_date'] = date('Y-m-d H:i:s');
	
		return parent::update($data, $where);
	}
}