<?php 
class Registration_Model_DbTable_Semesterstatus extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_semesterstatus';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
	
	public function fnSearchSemester($post = array()) { //Function to get the user details
		
			
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
							->from(array('a' => 'tbl_semestermaster'),array('a.*'))
                			->join(array('b'=>'tbl_semester'),'a.IdSemesterMaster = b.Semester')    
                			->joinLeft(array('c'=>'tbl_semesterstatus'),'b.IdSemester = c.IdSemester',array("c.IdSemester as semsts","c.SemesterStatus","c.IdSemesterStatus"))                		
                			->where('a.	Active =1');
       						
       						
		if(isset($post['field2']) && !empty($post['field2']) ){
				$lstrSelect = $lstrSelect->where("a.SemesterMasterName like ?",'%'.$post['field2'].'%');
			}
		if(isset($post['field4']) && !empty($post['field4']) ){
				$lstrSelect = $lstrSelect->where("a.ArabicName = ?",'%'.$post['field4'].'%');
			}
       								

       	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	
	 
	 
	   public function getCompleteStudentDetails($lintidapplicant) { 
	     	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a' => 'tbl_studentapplication'),array('a.*'))
                			->join(array('b'=>'tbl_collegemaster'),'a.idCollege  = b.IdCollege')
                			->join(array('c'=>'tbl_program'),'a.IDCourse = c.IdProgram')
                			->where('a.IdApplication = ?',$lintidapplicant);
			$result = $db->fetchRow($sql);
			return $result;
		}
	
	public function getLandscapeDropDown($idprogram){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_landscape"),array("key"=>"a.IdLandscape","value"=>"CONCAT_WS('-',IFNULL(b.DefinitionDesc,'-'),IFNULL(a.IdLandscape,'-'))"))
		 				 ->join(array('b'=>'tbl_definationms'),'a.LandscapeType = b.idDefinition')
		 				 ->where('a.IdProgram = ?',$idprogram)
		 				 ->where("a.Active = 123");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
		
	
	
    public function fnsavesemstatus($formData) { //Function for adding the Program Branch details to the table
		
		unset ( $formData ['Save']);
		
	    return $this->insert($formData);
	}
	
	
	public function fnupdatesemstatus($larrInsertData,$IdSemstatus){
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_semesterstatus";
			$where = "IdSemesterStatus = '".$IdSemstatus."'";
			$db->update($table,$larrInsertData,$where);		
	}
	
	public function fnAddStudentSubjectsperSemester($larrformData) {  // function to insert po details
		unset ( $larrformData ['IdProgram'] );
		unset ( $larrformData ['Save']);
		unset ( $larrformData ['selectall']);
		
		unset ( $larrformData ['IdApplication']);
		unset ( $larrformData ['registrationId']);
		unset ( $larrformData ['IdLandscape']);
		unset ( $larrformData ['studentname']);
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_studentregsubjects";
		$db->insert($table,$larrformData);	
	}
	
	
	public function fnUpdateStudentApplication($lintidapplicant) {  // function to update po details
			$db = Zend_Db_Table::getDefaultAdapter();
			$table = "tbl_studentapplication";
			$larrapplicant = array('Registered'=>'1');
			$where = "IdApplication = '".$lintidapplicant."'";
			$db->update($table,$larrapplicant,$where);	
	}
	
	function fnGenerateCode($idUniversity,$collageId,$page,$IdInserted){	
			$db 	= 	Zend_Db_Table::getDefaultAdapter();			
		$select =   $db->select()
				->  from('tbl_config')
				->	where('idUniversity  = ?',$idUniversity);				 
		$result = 	$db->fetchRow($select);		
		$sepr	=	$result[$page.'Separator'];
		$str	=	$page."CodeField";
		$CodeText	=	$page."CodeText";
		for($i=1;$i<=4;$i++){
			$check = $result[$str.$i];
			$TextCode = $result[$CodeText.$i];
			switch ($check){
				case 'Year':
					  $code	= date('Y');
					  break;
				case 'Uniqueid':
					  $code	= $IdInserted;
					  break;
				case 'College':
					  $select =  $db->select()
					 		 -> from('tbl_collegemaster')
					 		 ->	where('IdCollege  = ?',$collageId);  				 
					  $resultCollage = $db->fetchRow($select);		
					  $code		   = $resultCollage['ShortName'];
				      break;
				case 'University':
					  $select =  $db->select()
					 		 -> from('tbl_universitymaster')
					 		 ->	where('IdUniversity  = ?',$idUniversity);  				 
					  $resultCollage = $db->fetchRow($select);		
					  $code	= $resultCollage['ShortName'];
				      break;
				 case 'Text':					 		
					  $code		   = $TextCode;
				      break;
				default:
				      break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}	 	
		return $accCode;			
	}
	
	 public function fnupdateRegistrationtCode($larrstudentreg,$registrationCode) { 
	 	$larrformData['registrationId']   	 = $registrationCode;
		$where = 'IdStudentRegistration = '.$larrstudentreg;
		$this->update($larrformData,$where);
	 }
	 
	   public function fnCheckExistingSemester($IdSemester,$lintidapplicant) { 
	     	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a' => 'tbl_studentregistration'),array('a.*'))
							->where('a.IdApplication  = ?',$lintidapplicant)
							->where('a.IdSemester = ?',$IdSemester);
			$result = $db->fetchRow($sql);
			return $result;
		}


                

   	public function addSemesterStatus($data){                    
                    $db = Zend_Db_Table::getDefaultAdapter();
                    $tbl = 'tbl_studentsemesterstatus';
                    $db->insert($tbl,$data);
  	}

    public function checkeffectivesemester($value,$formdata){
                    echo "<pre>";
                    print_r($value);
                    print_r($formdata);
                    die;
                    
    }

    
 


}
?>