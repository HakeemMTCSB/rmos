<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 4/11/2015
 * Time: 9:54 AM
 */
class Schedule_Model_DbTable_Schedule extends Zend_Db_Table_Abstract {

    public function getScheme(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_scheme'), array('value'=>'*'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSemester($scheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IsCountable = ?', 1)
            ->where('a.IdScheme = ?', $scheme);

        $select = $select.' ORDER BY a.AcademicYear DESC, CASE a.sem_seq '
            . 'WHEN "JAN" THEN 1 '
            . 'WHEN "FEB" THEN 2 '
            . 'WHEN "MAR" THEN 3 '
            . 'WHEN "APR" THEN 4 '
            . 'WHEN "MAY" THEN 5 '
            . 'WHEN "JUN" THEN 6 '
            . 'WHEN "JUL" THEN 7 '
            . 'WHEN "AUG" THEN 8 '
            . 'WHEN "SEP" THEN 9 '
            . 'WHEN "OCT" THEN 10 '
            . 'WHEN "NOV" THEN 11 '
            . 'WHEN "DEC" THEN 12 '
            . 'ELSE 13 '
            . 'END DESC';

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getClass($id, $search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.IdSemester = c.IdSemesterMaster')
            ->where('a.IdLecturer = ?', $id);

        if ($search != false){
            if (isset($search['semester']) && $search['semester']!='') {
                $select->where('a.IdSemester = ?', $search['semester']);
            }
            if (isset($search['subcode']) && $search['subcode']!='') {
                $select->where('b.SubCode like "%'.$search['subcode'].'%"');
            }
            if (isset($search['coursename']) && $search['coursename']!='') {
                $select->where('b.SubjectName like "%'.$search['coursename'].'%"');
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getExam($id, $search = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_course_tagging_group'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.IdSemester = c.IdSemesterMaster')
            ->where('a.IdLecturer = ?', $id)
            ->group('b.IdSubject');

        if ($search != false){
            if (isset($search['semester']) && $search['semester']!='') {
                $select->where('a.IdSemester = ?', $search['semester']);
            }
            if (isset($search['subcode']) && $search['subcode']!='') {
                $select->where('b.SubCode like "%'.$search['subcode'].'%"');
            }
            if (isset($search['coursename']) && $search['coursename']!='') {
                $select->where('b.SubjectName like "%'.$search['coursename'].'%"');
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getGroupStudent($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.IdStudentRegistration=b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id=c.id', array('studentName'=>'concat(c.appl_fname, " ", c.appl_lname)', 'appl_fname','appl_mname','appl_lname', 'appl_email'))
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram=d.IdProgram', array('d.ProgramCode', 'd.*'))
            ->joinLeft(array('h'=>'tbl_program_scheme'), 'b.IdProgramScheme = h.IdProgramScheme')
            ->joinLeft(array('i'=>'tbl_definationms'), 'h.mode_of_program = i.idDefinition', array('mop'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'h.mode_of_study = j.idDefinition', array('mos'=>'j.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_definationms'), 'h.program_type = k.idDefinition', array('pt'=>'k.DefinitionDesc'))
            ->where('a.IdCourseTaggingGroup = ?', $id)
            ->where('IFNULL(a.exam_status,"Z") NOT IN ("EX","CT")')
            //->where('a.Active <> ?', 3)
            ->order('c.appl_fname ASC')
            ->group('a.IdStudentRegistration');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getExamSchedule($courseid, $semid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'exam_schedule'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.es_course = b.IdSubject')
            ->joinLeft(array('c'=>'tbl_semestermaster'), 'a.es_semester = c.IdSemesterMaster')
            ->where('a.es_course = ?', $courseid)
            ->where('a.es_semester = ?', $semid);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getExamStudent($courseid, $semid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'exam_registration'), array('value'=>'*'))
            ->joinLeft(array('l'=>'tbl_exam_center'), 'a.er_ec_id = l.ec_id')
            ->joinLeft(array('m'=>'tbl_definationms'), 'a.er_status = m.idDefinition', array('statusname'=>'m.DefinitionDesc'))
            ->join(array('b'=>'tbl_studentregistration'), 'a.er_idStudentRegistration=b.IdStudentRegistration')
            ->join(array('c'=>'student_profile'), 'b.sp_id=c.id', array('studentName'=>'concat(c.appl_fname, " ", c.appl_lname)', 'appl_fname','appl_mname','appl_lname', 'appl_email'))
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram=d.IdProgram', array('d.ProgramCode', 'd.*'))
            ->joinLeft(array('h'=>'tbl_program_scheme'), 'b.IdProgramScheme = h.IdProgramScheme')
            ->joinLeft(array('i'=>'tbl_definationms'), 'h.mode_of_program = i.idDefinition', array('mop'=>'i.DefinitionDesc'))
            ->joinLeft(array('j'=>'tbl_definationms'), 'h.mode_of_study = j.idDefinition', array('mos'=>'j.DefinitionDesc'))
            ->joinLeft(array('k'=>'tbl_definationms'), 'h.program_type = k.idDefinition', array('pt'=>'k.DefinitionDesc'))
            ->where('a.er_idSubject = ?', $courseid)
            ->where('a.er_idSemester = ?', $semid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getTextbook($subjectid, $semesterid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_learning_material'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.tlm_materialType = b.idDefinition', array('typename'=>'b.DefinitionDesc'))
            ->where('a.tlm_subjectId = ?', $subjectid)
            ->where('a.tlm_semester = ?', $semesterid);

        $result = $db->fetchAll($select);
        return $result;
    }
}