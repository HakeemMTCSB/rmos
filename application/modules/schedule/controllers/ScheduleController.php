<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 4/11/2015
 * Time: 9:53 AM
 */
class Schedule_ScheduleController extends Base_Base {

    private $_gobjlog;
    private $locale;
    private $model;

    public function init(){
        $this->view->translate =Zend_Registry::get('Zend_Translate');
        $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
        Zend_Form::setDefaultTranslator($this->view->translate);
        $this->fnsetObj();
    }

    public function fnsetObj(){
        $this->auth = Zend_Auth::getInstance();
        $this->registry = Zend_Registry::getInstance();
        $this->view->locale = $this->locale = $this->registry->get('Zend_Locale');
        $this->model = new Schedule_Model_DbTable_Schedule();
    }

    public function indexAction(){
        $this->view->title = $this->view->translate('Schedule');
    }

    public function classScheduleAction(){
        $this->view->title = $this->view->translate('Class Schedule');

        $auth = Zend_Auth::getInstance();

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Schedule_Form_ClassScheduleSearch(array('schemeid'=>$formData['scheme']));
            $this->view->form = $form;

            $list = $this->model->getClass($auth->getIdentity()->IdStaff, $formData);
            $this->view->list = $list;

            $form->populate($formData);
        }else{
            $form = new Schedule_Form_ClassScheduleSearch();
            $this->view->form = $form;
        }
    }

    public function clearClassScheduleAction(){
        $this->_redirect($this->baseUrl . '/schedule/schedule/class-schedule');
        exit;
    }

    public function viewClassScheduleAction(){
        $this->view->title = $this->view->translate('View Class Schedule');

        $id = $this->_getParam('id',null);

        $atmodel = new GeneralSetup_Model_DbTable_Attendance();

        $groupInfo = $atmodel->getGoupInfo($id);
        $classList = $this->getSchedule($id);

        $this->view->groupInfo = $groupInfo;
        $this->view->classList = $classList;
    }

    public function viewClassStudentAction(){
        $this->view->title = $this->view->translate('Class Student');

        $id = $this->_getParam('id',null);

        $atmodel = new GeneralSetup_Model_DbTable_Attendance();

        $studentList = $this->model->getGroupStudent($id);
        $groupInfo = $atmodel->getGoupInfo($id);

        if ($studentList){
            $lobjStudentprofile = new Records_Model_DbTable_Studentprofile();

            foreach ($studentList as $key => $studentLoop){
                $photo = $lobjStudentprofile->getStudentPhoto($studentLoop['sp_id']);
                $app_photo = $app_photo_full = '';
                if (!empty($photo))
                {
                    $app_photo_full = DOCUMENT_URL.$photo['ad_filepath'];
                    $app_photo = $this->view->baseUrl().'/thumb.php?s=1&w=100&f='.DOCUMENT_PATH.urlencode($photo['ad_filepath']);
                }

                $studentList[$key]['app_photo'] = $app_photo;
                $studentList[$key]['app_photo_full'] = $app_photo_full;
            }
        }

        $this->view->studentList = $studentList;
        $this->view->groupInfo = $groupInfo;
    }

    public function getSemesterAction(){
        $id = $this->_getParam('id',null);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $semesterList = $this->model->getSemester($id);

        $json = Zend_Json::encode($semesterList);

        echo $json;
        exit();
    }

    public function getSchedule($groupId){
        $model = new GeneralSetup_Model_DbTable_Attendance();

        $groupInfo = $model->getGoupInfo($groupId);
        //var_dump($groupInfo); exit;
        $this->view->groupInfo = $groupInfo;

        $studentList = $model->getGroupStudent($groupId);

        $this->view->studentList = $studentList;

        $groupSchedule = $model->getGroupSchedule($groupId);

        $sdate = $model->getAttendanceDate($groupId);
        //var_dump($sdate);
        $status = $model->getDefinition();
        $this->view->status = $status;

        if ($groupSchedule){
            $classList = $groupSchedule;
        }else{
            throw new Exception('Group Schedule Not Found');
        }

        $weekArr = array();
        $monthArr = array();

        if ($classList){
            $i = 0;
            foreach ($classList as $key => $dateLoop){

                if (isset($dateLoop['sc_day'])){
                    switch($dateLoop['sc_day']){
                        case 'Monday':
                            $d[$key] = 1;
                            break;
                        case 'Tuesday':
                            $d[$key] = 2;
                            break;
                        case 'Wednesday':
                            $d[$key] = 3;
                            break;
                        case 'Thursday':
                            $d[$key] = 4;
                            break;
                        case 'Friday':
                            $d[$key] = 5;
                            break;
                        case 'Saturday':
                            $d[$key] = 6;
                            break;
                        case 'Sunday':
                            $d[$key] = 7;
                            break;
                    }
                }

                if ($dateLoop['sc_date']==null || $dateLoop['sc_date']==''){
                    if ($sdate){
                        $i = 0;
                        foreach ($sdate as $sdateLoop){
                            //$classList[$i]['sc_id'] = $dateLoop['sc_id'];
                            $classList[$i]['sc_date'] = $sdateLoop['ctd_scdate'];
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];

                            $detectday1 = date('l', strtotime($sdateLoop['ctd_scdate']));
                            $detectday2 = $dateLoop['sc_day'];

                            if (!isset($classList[$i]['sc_id'])) {
                                $classList[$i]['sc_id'] = $dateLoop['sc_id'];
                                $classList[$i]['idGroup'] = $dateLoop['idGroup'];
                                $classList[$i]['IdLecturer'] = $dateLoop['IdLecturer'];
                                $classList[$i]['idCollege'] = $dateLoop['idCollege'];
                                $classList[$i]['idBranch'] = $dateLoop['idBranch'];
                                $classList[$i]['idClassType'] = $dateLoop['idClassType'];
                                $classList[$i]['sc_day'] = $dateLoop['sc_day'];
                                $classList[$i]['sc_start_time'] = $dateLoop['sc_start_time'];
                                $classList[$i]['sc_end_time'] = $dateLoop['sc_end_time'];
                                $classList[$i]['sc_venue'] = $dateLoop['sc_venue'];
                                $classList[$i]['sc_class'] = $dateLoop['sc_class'];
                                $classList[$i]['sc_remark'] = $dateLoop['sc_remark'];
                                $classList[$i]['sc_createdby'] = $dateLoop['sc_createdby'];
                                $classList[$i]['sc_createddt'] = $dateLoop['sc_createddt'];
                                $classList[$i]['classtype']=$dateLoop['classtype'];
                                $classList[$i]['lecturername']=$dateLoop['lecturername'];
                                $classList[$i]['veneuname']=$dateLoop['veneuname'];
                            }

                            $i++;

                            $month = date('F', strtotime($sdateLoop['ctd_scdate']));
                            $week = date('W', strtotime($sdateLoop['ctd_scdate']));

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }else{
                        if ($key > 0){
                            $dkey = $d[$key]-$d[($key-1)];
                            $plusdate = $dkey.' day';
                            $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                        }else{
                            $start_day = date('l', strtotime($groupInfo['class_start_date']));

                            switch($start_day){
                                case 'Monday':
                                    $start_day_no = 1;
                                    break;
                                case 'Tuesday':
                                    $start_day_no = 2;
                                    break;
                                case 'Wednesday':
                                    $start_day_no = 3;
                                    break;
                                case 'Thursday':
                                    $start_day_no = 4;
                                    break;
                                case 'Friday':
                                    $start_day_no = 5;
                                    break;
                                case 'Saturday':
                                    $start_day_no = 6;
                                    break;
                                case 'Sunday':
                                    $start_day_no = 7;
                                    break;
                            }
                            $dkey = $d[$key]-$start_day_no;

                            $plusdate = $dkey.' day';
                            $groupInfo['class_start_date'] = $date = date('Y-m-d', strtotime($plusdate, strtotime($groupInfo['class_start_date'])));
                        }

                        while ($date <= $groupInfo['class_end_date']){
                            $classList[$i]['sc_date'] = $date;
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];

                            $classList[$i]['sc_id'] = $dateLoop['sc_id'];
                            $classList[$i]['idGroup'] = $dateLoop['idGroup'];
                            $classList[$i]['IdLecturer'] = $dateLoop['IdLecturer'];
                            $classList[$i]['idCollege'] = $dateLoop['idCollege'];
                            $classList[$i]['idBranch'] = $dateLoop['idBranch'];
                            $classList[$i]['idClassType'] = $dateLoop['idClassType'];
                            $classList[$i]['sc_day'] = $dateLoop['sc_day'];
                            $classList[$i]['sc_start_time'] = $dateLoop['sc_start_time'];
                            $classList[$i]['sc_end_time'] = $dateLoop['sc_end_time'];
                            $classList[$i]['sc_venue'] = $dateLoop['sc_venue'];
                            $classList[$i]['sc_class'] = $dateLoop['sc_class'];
                            $classList[$i]['sc_remark'] = $dateLoop['sc_remark'];
                            $classList[$i]['sc_createdby'] = $dateLoop['sc_createdby'];
                            $classList[$i]['sc_createddt'] = $dateLoop['sc_createddt'];
                            $classList[$i]['classtype']=$dateLoop['classtype'];
                            $classList[$i]['lecturername']=$dateLoop['lecturername'];
                            $classList[$i]['veneuname']=$dateLoop['veneuname'];

                            $month = date('F', strtotime($date));
                            $week = date('W', strtotime($date));

                            $date = date('Y-m-d', strtotime($date. ' + 7 days'));
                            $i++;

                            if (!in_array($month, $monthArr)){
                                array_push($monthArr, $month);
                            }

                            if (!in_array($week, $weekArr)){
                                array_push($weekArr, $week);
                            }
                        }
                    }
                }

                $month = date('F', strtotime($dateLoop['sc_date']));
                $week = date('W', strtotime($dateLoop['sc_date']));

                if (!in_array($month, $monthArr)){
                    array_push($monthArr, $month);
                }

                if (!in_array($week, $weekArr)){
                    array_push($weekArr, $week);
                }
            }
        }

        $this->aasort($classList, 'sc_date');

        return $classList;
    }

    public function examScheduleAction(){
        $this->view->title = $this->view->translate('Exam Schedule');

        $auth = Zend_Auth::getInstance();

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Schedule_Form_ClassScheduleSearch(array('schemeid'=>$formData['scheme']));
            $this->view->form = $form;

            $list = $this->model->getExam($auth->getIdentity()->IdStaff, $formData);
            $this->view->list = $list;

            $form->populate($formData);
        }else{
            $form = new Schedule_Form_ClassScheduleSearch();
            $this->view->form = $form;
        }
    }

    public function viewExamScheduleAction(){
        $this->view->title = $this->view->translate('View Exam Schedule');

        $id = $this->_getParam('id', 0);
        $semid = $this->_getParam('semid', 0);

        $examSchedule = $this->model->getExamSchedule($id, $semid);
        $examStudent = $this->model->getExamStudent($id, $semid);

        if ($examStudent){
            $lobjStudentprofile = new Records_Model_DbTable_Studentprofile();

            foreach ($examStudent as $key => $studentLoop){
                $photo = $lobjStudentprofile->getStudentPhoto($studentLoop['sp_id']);
                $app_photo = $app_photo_full = '';
                if (!empty($photo))
                {
                    $app_photo_full = DOCUMENT_URL.$photo['ad_filepath'];
                    $app_photo = $this->view->baseUrl().'/thumb.php?s=1&w=100&f='.DOCUMENT_PATH.urlencode($photo['ad_filepath']);
                }

                $examStudent[$key]['app_photo'] = $app_photo;
                $examStudent[$key]['app_photo_full'] = $app_photo_full;
            }
        }

        $this->view->examSchedule = $examSchedule;
        $this->view->examStudent = $examStudent;
    }

    public function textbookAction(){
        $this->view->title = $this->view->translate('Learning Material');

        $auth = Zend_Auth::getInstance();

        if($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form = new Schedule_Form_ClassScheduleSearch(array('schemeid'=>$formData['scheme']));
            $this->view->form = $form;

            $list = $this->model->getExam($auth->getIdentity()->IdStaff, $formData);

            if ($list){
                foreach ($list as $key => $loop){
                    $textbook = $this->model->getTextbook($loop['IdSubject'], $loop['IdSemester']);

                    if ($textbook) {
                        $list[$key]['textbook'] = $textbook;
                    }
                }
            }

            $this->view->list = $list;

            $form->populate($formData);
        }else{
            $form = new Schedule_Form_ClassScheduleSearch();
            $this->view->form = $form;
        }
    }

    private function aasort(&$array, $key){
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
    }
}