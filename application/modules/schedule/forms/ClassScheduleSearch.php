<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 4/11/2015
 * Time: 10:38 AM
 */
class Schedule_Form_ClassScheduleSearch extends Zend_Dojo_Form {

    public function init() {

        $schemeid = $this->getAttrib('schemeid');

        $model = new Schedule_Model_DbTable_Schedule();

        //Scheme
        $scheme = new Zend_Form_Element_Select('scheme');
        $scheme->removeDecorator("DtDdWrapper");
        $scheme->setAttrib('class', 'select');
        $scheme->setAttrib('required', true);
        $scheme->setAttrib('onchange', 'getSemester(this.value);');
        $scheme->removeDecorator("Label");

        $scheme->addMultiOption('', '-- Select --');

        $schemeList = $model->getScheme();

        if ($schemeList){
            foreach ($schemeList as $schemeLoop){
                $scheme->addMultiOption($schemeLoop['IdScheme'], $schemeLoop['SchemeCode'].' - '.$schemeLoop['EnglishDescription']);
            }
        }

        //Semester
        $semester = new Zend_Form_Element_Select('semester');
        $semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('class', 'select');
        $semester->setAttrib('required', true);
        //$scheme->setAttrib('onchange', 'getSemester(this.value);');
        $semester->removeDecorator("Label");

        $semester->addMultiOption('', '-- Select --');

        if ($schemeid){
            $semesterList = $model->getSemester($schemeid);

            if ($semesterList){
                foreach ($semesterList as $semesterLoop){
                    $semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName']);
                }
            }
        }

        //Course Code
        $subcode = new Zend_Form_Element_Text('subcode');
        $subcode->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //Course Name
        $coursename = new Zend_Form_Element_Text('coursename');
        $coursename->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $this->addElements(array(
            $scheme,
            $semester,
            $subcode,
            $coursename
        ));
    }
}