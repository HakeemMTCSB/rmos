/* common js functions */
function sure(msg)
{
	if (msg == undefined)
	{
		var msg = 'Are you sure?';
	}

	var ask = confirm(msg);
	if (ask == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function escapeHtml(string) 
{
	var entityMap = 
	{
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		'"': '&quot;',
		"'": '&#39;',
		"/": '&#x2F;'
	};
	return String(string).replace(/[&<>"'\/]/g, function (s) {
	  return entityMap[s];
	});
}

function initCap(id) 
{
	var str = $(id).val();
	var upper = str.toUpperCase();
	
	if ( str != upper )
	{
		$(id).val(upper);
	}
}

/* on load */
$(function(){

	$(".fancy").fancybox();

	var header = $("#main-menu");
	var headerY = header.offset().top;
	$(document).scroll(function () {
		var y = $(document).scrollTop();

		if (y >= headerY) {
			header.addClass('fixed');
		} else {
			header.removeClass('fixed');
		}
	});

	var toolbar = $("div.toolbar");
	var toolbarY = header.offset().top;
	$(document).scroll(function () {
		var y = $(document).scrollTop();

		if (y >= toolbarY) {
			//toolbar.addClass('fixed');
		} else {
			//toolbar.removeClass('fixed');
		}
	});

	//remove pesky info button
	if( $('.toolbar-button').find('span.info').length != 0 )
	{
		var info = $('.toolbar-button li a span.info');
		info.parent().parent().remove(); //begone!
	}

	//flashmessenger
	$(function(){
		setTimeout(function(){
			$(".success").slideUp();
		},2000);
	});

	//tooltips
	loadTooltip();

	$('.initcap').on('keyup', function()
	{
		initCap($(this));
	});
	
	//make sure content is always height >= sidebar
	checkHeight();

	$("#main-menu").prepend('<div id="togglemenu"><span class="icon icon-menu"></span></div>');
	$('#togglemenu').on('click', function() 
	{
		$(".navigation").slideToggle();
	});
	
	$( window ).resize(function() 
	{
		if ($(window).width()>800)
		{	
			if ( $(".navigation").css("display") == 'none' )
			{
				$(".navigation").show();
			}
		}
	});
	
});

function loadTooltip()
{
	$('.tooltip').each(function() {
         $(this).qtip({
             content: {
                 text: $(this).next('.tooltiptext')
             },style: {
				classes: 'qtip-light'
			},
			hide: {
                fixed: true,
                delay: 300
            },
			position: {
				my: 'bottom center',  // Position my top left...
				at: 'top center', // at the bottom right of...
			}
         });
     });

	 $('.tooltip2').each(function() {
         $(this).qtip({
             content: {
                 text: $(this).next('.tooltiptext')
             },style: {
				classes: 'qtip-dark'
			},
			hide: {
                fixed: true,
                delay: 300
            },
			position: {
				my: 'bottom center',  // Position my top left...
				at: 'top center', // at the bottom right of...
			}
         });
     });
}

function checkHeight()
{
	var sidebar = $("#sidebar").height();
	var content = $("#body-content").height();

	if ( sidebar > content )
	{
		$("#body-content").css('height', sidebar+'px');
	}
}

function togglemenu(what)
{
	var check = $(what).next().css('display');
	$('.nav-dropdown').hide();
	$('.navlink').removeClass('active');
	
	if (check == 'none')
	{
		 $(what).next().show();
		 $(what).addClass('active');
	}
	else
	{
		 $(what).next().hide();
	}
   
    return false;
}

(function( $ ) {
	 $.widget( "ui.combobox", {
		 _create: function() {
			 var self = this,
				 select = this.element.hide(),
				 selected = select.children( ":selected" ),
				 value = selected.val() ? selected.text() : "";
			 var input = this.input = $( "<input>" )
				 .insertAfter( select )
				 .val( value )
				 .autocomplete({
					 delay: 0,
					 minLength: 0,
					 source: function( request, response ) {
						 var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
						 response( select.children( "option" ).map(function() {
							 var text = $( this ).text();
							 if ( this.value && ( !request.term || matcher.test(text) ) )
								 return {
									 label: text.replace(
										 new RegExp(
											 "(?![^&;]+;)(?!<[^<>]*)(" +
											 $.ui.autocomplete.escapeRegex(request.term) +
											 ")(?![^<>]*>)(?![^&;]+;)", "gi"
										 ), "$1" ),
									 value: text,
									 option: this
								 };
						 }) );
					 },
					 select: function( event, ui ) {
						 ui.item.option.selected = true;
						 self._trigger( "selected", event, {
							 item: ui.item.option
						 });
						 select.trigger("change");                             
					 },
					 change: function( event, ui ) {
						 if ( !ui.item ) {
							 var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
								 valid = false;
							 select.children( "option" ).each(function() {
								 if ( $( this ).text().match( matcher ) ) {
									 this.selected = valid = true;
									 return false;
								 }
							 });
							 if ( !valid ) {
								 // remove invalid value, as it didn't match anything
								 $( this ).val( "" );
								 select.val( "" );
								 input.data( "autocomplete" ).term = "";
								 return false;
							 }
						 }
					 }
				 })
				 .addClass( "input-txt" );

			 input.data( "autocomplete" )._renderItem = function( ul, item ) {
				 return $( "<li></li>" )
					 .data( "item.autocomplete", item )
					 .append( "<a>" + item.label + "</a>" )
					 .appendTo( ul );
			 };

			 this.button = $( "<button type='button'>&nbsp;</button>" )
				 .attr( "tabIndex", -1 )
				 .attr( "title", "Show All Items" )
				 .insertAfter( input )
				 .button({
					 icons: {
						 primary: "ui-icon-triangle-1-s"
					 },
					 text: false
				 })
				 .removeClass( "ui-corner-all" )
				 .addClass( "ui-corner-right ui-button-icon" )
				 .click(function() {
					 // close if already visible
					 if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
						 input.autocomplete( "close" );
						 return;
					 }

					 // pass empty string as value to search for, displaying all results
					 input.autocomplete( "search", "" );
					 input.focus();
				 });
		 },

		 destroy: function() {
			 this.input.remove();
			 this.button.remove();
			 this.element.show();
			 $.Widget.prototype.destroy.call( this );
		 }
	 });
 })( jQuery );

function strip_tags(input, allowed) 
{
	allowed = (((allowed || '') + '')
	.toLowerCase()
	.match(/<[a-z][a-z0-9]*>/g) || [])
	.join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
	var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
	commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
	return input.replace(commentsAndPhpTags, '')
	.replace(tags, function ($0, $1) {
	  return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
	});
}